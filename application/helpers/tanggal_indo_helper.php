<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');
	function tanggal_display($date = null, $style = 3) {
		$ret = '';

		$month = array(
			'01' => 'Januari',
			'02' => 'Februari',
			'03' => 'Maret',
			'04' => 'April',
			'05' => 'Mei',
			'06' => 'Juni',
			'07' => 'Juli',
			'08' => 'Agustus',
			'09' => 'September',
			'10' => 'Oktober',
			'11' => 'November',
			'12' => 'Desember'
		);

		if ($date) {
			$data_arr = explode('-', $date);
			$ret = $data_arr[2] . ' ' . $month[$data_arr[1]] . ' ' . $data_arr[0];

		} else {
			$ret = date("d") . ' ' . $month[date("m")] . ' ' . date("Y");
		}

		if ($style == 1) {
			$ret = strtolower($ret);
		} else if ($style == 2) {
			$ret = strtoupper($ret);
		} else {
			$ret = ucwords($ret);
		}

		return $ret;

	}
	
	function tanggal_angka($date = null, $sparator = '/'){
		$ret = '';
		
		if ($date) {
			$data_arr = explode('-', $date);
			$ret = $data_arr[2] . $sparator . $data_arr[1] . $sparator . $data_arr[0];

		} else {
			$ret = date("d") . $sparator . $month[date("m")] . $sparator . date("Y");
		}
		
		return $ret;
	}

	function nama_hari($date = null, $style = 4) {
		$ret = '';

		$day_name = array(
			'0' => 'minggu',
			'1' => 'senin',
			'2' => 'selasa',
			'3' => 'rabu',
			'4' => 'kamis',
			'5' => 'jum\'at',
			'6' => 'sabtu'
		);

		if ($date) {
			$date = date_create($date); ;
			$ret = $day_name[date_format($date, "w")];

		} else {
			$ret = $day_name[date("w")];
		}

		if ($style == 1) {
			$ret = strtoupper($ret);
		} else if ($style == 2) {
			$ret = strtolower($ret);
		} else if ($style == 3) {
			$ret = ucwords($ret);
		} else {
			$ret = ucfirst($ret);
		}

		return $ret;
	}

	function nama_bulan($date = null, $style = 4) {
		$ret = '';

		$month_name = array(
			'01' => 'Januari',
			'02' => 'Februari',
			'03' => 'Maret',
			'04' => 'April',
			'05' => 'Mei',
			'06' => 'Juni',
			'07' => 'Juli',
			'08' => 'Agustus',
			'09' => 'September',
			'10' => 'Oktober',
			'11' => 'Nopember',
			'12' => 'Desember'
		);

		return $month_name[$date];
	}
    function x(){
        $a="1";
        return $a;
    }
    
?>