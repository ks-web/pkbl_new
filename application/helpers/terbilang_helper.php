<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');
	function terbilang_display($bil = null, $type = null) {
		$ret = '';
		if ($bil) {
			$CI = &get_instance();
			$CI->load->library('terbilang');
			$ret = $CI->terbilang->display($bil, $type);
		}

		return $ret;

	}
?>