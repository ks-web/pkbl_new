<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class PMailer {
		private $CI;

		function mail_sent($mailTo = array(), $subj = '', $content = '', $attch = array()) {
			if (!mail_db) {
				require_once (APPPATH . 'libraries/PHPMailerAutoload.php');
				$mail = new PHPMailer();

				// $mail->SMTPDebug = 3;

				$mail->IsSMTP();
				$mail->SMTPOptions = array('ssl' => array(
						'verify_peer' => false,
						'verify_peer_name' => false,
						'allow_self_signed' => true
					));
				$mail->SMTPAuth = true;
				$mail->SMTPSecure = mail_SMTPSecure;
				$mail->Port = mail_Port;
				$mail->Host = mail_Host;
				$mail->Username = mail_Username;
				$mail->Password = mail_Password;
				$mail->SetFrom(mail_Username, mail_AppName . PERUSAHAAN);
				$mail->Subject = $subj;
				$mail->IsHTML(TRUE);

				$mail->MsgHTML($content);

				if (sizeof($mailTo) > 0) {
					for ($i = 0; $i < sizeof($mailTo); $i++) {
						$mail->AddAddress($mailTo[$i]);
					}
				}

				if (sizeof($attch) > 0) {
					for ($i = 0; $i < sizeof($attch); $i++) {
						$dat_file = explode('#|#', $attch[$i]);
						if (isset($dat_file[1])) {
							$mail->addAttachment($dat_file[0], $dat_file[1]);
						} else {
							$mail->addAttachment($dat_file[0]);
						}

					}
				}

				if ($mail->Send()) {
					$data["status"] = true;
					$data["message"] = "Message sent correctly!";
				} else {
					$data["status"] = false;
					$data["message"] = "Error: " . $mail->ErrorInfo;
				}
				return $data;
			} else {
				$this->CI = &get_instance();
				$this->CI->load->database();

				$email_count = sizeof($mailTo);
				$email_processed = 0;

				foreach ($mailTo as $mail_address) {
					$data = array(
						'email_to' => $mail_address,
						'subject' => $subj,
						'content' => $content,
						'status' => '0'
					);

					$this->CI->db->insert('email_queue', $data);
					$id = $this->CI->db->insert_id();

					$affected = $this->CI->db->affected_rows();
					if ($affected > 0) {
						$email_processed++;
					}

					foreach ($attch as $file_path) {
						$data_att = array(
							'email_id' => $id,
							'attach_url' => $file_path
						);
						$this->CI->db->insert('email_queue_attachment', $data_att);
					}
				}

				if ($email_count == $email_processed) {
					$data["status"] = true;
					$data["message"] = "Message sent correctly!";
				} else {
					$data["status"] = false;
					$data["message"] = "Error: Some email cannot send";
				}
				return $data;
			}

		}

		function mail_sent_queue($mailTo = array(), $subj = '', $content = '', $attch = array()) {
			require_once (APPPATH . 'libraries/PHPMailerAutoload.php');
			$mail = new PHPMailer();

			// $mail->SMTPDebug = 4;

			$mail->IsSMTP();
			$mail->SMTPOptions = array('ssl' => array(
					'verify_peer' => false,
					'verify_peer_name' => false,
					'allow_self_signed' => true
				));
			$mail->SMTPAuth = true;
			$mail->SMTPSecure = mail_SMTPSecure;
			$mail->Port = mail_Port;
			$mail->Host = mail_Host;
			$mail->Username = mail_Username;
			$mail->Password = mail_Password;
			$mail->SetFrom(mail_Username, mail_AppName . PERUSAHAAN);
			$mail->Subject = $subj;
			$mail->IsHTML(TRUE);

			$mail->MsgHTML($content);

			if (sizeof($mailTo) > 0) {
				for ($i = 0; $i < sizeof($mailTo); $i++) {
					$mail->AddAddress($mailTo[$i]);
				}
			}

			if (sizeof($attch) > 0) {
				for ($i = 0; $i < sizeof($attch); $i++) {
					$dat_file = explode('#|#', $attch[$i]);
					if (isset($dat_file[1])) {
						$mail->addAttachment($dat_file[0], $dat_file[1]);
					} else {
						$mail->addAttachment($dat_file[0]);
					}

				}
			}

			if ($mail->Send()) {
				$data["status"] = true;
				$data["message"] = "Message sent correctly!";
			} else {
				$data["status"] = false;
				$data["message"] = "Error: " . $mail->ErrorInfo;
			}
			return $data;
		}

	}
