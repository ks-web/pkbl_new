<?php defined('BASEPATH') OR exit('No direct script access allowed');

class ArrayElement {
	/*
	 * getArrayElement() by @MmO12 modified by @fajar.isnandio
	 *
	 * SELECT an element of  an $array...
	 * *$array = array( 0 => array('index1'  => 'a', 'index2' => 'b', 'value' =>
	 * 'hello'),
	 1 => array('index1'  => 'a', 'index2' => 'c', 'value' => 'bye'));
	 *
	 * WHERE its $indexs values  are equal to...
	 * * $indexs = array('index1' => 'a', 'index2'=>'b');
	 */

	function get($array, $indexs, $justvalsplease = false) {
		$newarray = false;
		//verificamos el array
		if (is_array($array) && count($array) > 0) {

			//verify indexs and get # of indexs
			if (is_array($indexs) && count($indexs) > 0)
				$ninds = count($indexs);
			else
				return false;

			//search for coincidences
			foreach (array_keys($array) as $key) {

				//index value coincidence counter.
				$count = 0;

				//for each index we search
				foreach ($indexs as $indx => $val) {

					//if index value is equal then counts
					if ($array[$key][$indx] == $val) {
						$count++;
					}
				}
				//if indexes match, we get the array elements :)
				if ($count == $ninds) {

					//if you only need the vals of the first coincidence
					//witch was my case by the way...
					if ($justvalsplease)
						return $array[$key];
					else
						$newarray[$key] = $array[$key];
				}
			}
		}
		return $newarray;
	}

}
