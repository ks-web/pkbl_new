<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Terbilang {
	function ke_kata($angka) {
		$angka = abs($angka);
		$kata_angka = array(
			'',
			'satu',
			'dua',
			'tiga',
			'empat',
			'lima',
			'enam',
			'tujuh',
			'delapan',
			'sembilan',
			'sepuluh',
			'sebelas'
		);
		$kalimat = '';

		if ($angka < 12) {
			$kalimat = " " . $kata_angka[$angka];
		} elseif ($angka < 20) {
			$kalimat = $this->ke_kata($angka - 10) . " belas";
		} elseif ($angka < 100) {
			$kalimat = $this->ke_kata($angka / 10) . " puluh" . $this->ke_kata($angka % 10);
		} elseif ($angka < 200) {
			$kalimat = " seratus" . $this->ke_kata($angka - 100);
		} elseif ($angka < 1000) {
			$kalimat = $this->ke_kata($angka / 100) . " ratus" . $this->ke_kata($angka % 100);
		} elseif ($angka < 2000) {
			$kalimat = " seribu" . $this->ke_kata($angka - 1000);
		} elseif ($angka < 1000000) {
			$kalimat = $this->ke_kata($angka / 1000) . " ribu" . $this->ke_kata($angka % 1000);
		} elseif ($angka < 1000000000) {
			$kalimat = $this->ke_kata($angka / 1000000) . " juta" . $this->ke_kata($angka % 1000000);
		} elseif ($angka < 1000000000000) {
			$kalimat = $this->ke_kata($angka / 1000000000) . " milyar" . $this->ke_kata(fmod($angka, 1000000000));
		} elseif ($angka < 1000000000000000) {
			$kalimat = $this->ke_kata($angka / 1000000000000) . " trilyun" . $this->ke_kata(fmod($angka, 1000000000000));
		}

		return $kalimat;
	}

	function display($angka, $style = 4) {
		if ($angka < 0) {
			$hasil = "minus " . trim($this->ke_kata($angka));
		} else {
			$hasil = trim($this->ke_kata($angka));
		}
		
		if($style == 1) {
			$hasil=strtoupper($hasil);
		} else if($style == 2) {
			$hasil=strtolower($hasil);
		} else if($style == 3) {
			$hasil=ucwords($hasil);
		} else {
			$hasil=ucfirst($hasil);
		}
		
		return $hasil;
	}

}
