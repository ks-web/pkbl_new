<!DOCTYPE html>
<html>
	<head>
	<meta charset="utf-8" />
	<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="KES Modul PKBL" />
	<meta name="author" content="Fajar Isnandio" />
	<title>KS PKBL</title>
	<link rel="shortcut icon" href="<?= base_url('assets/images/kes.png')?>"/>
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/themes/bootstrap/easyui.css')?>"/>
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/font-awesome/css/font-awesome.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/bootstrap.css')?>"/>
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/style.css')?>"/>
    <style>
		body {
			display: none;
		}
	</style>
    <script type="text/javascript" src="<?= base_url('assets/lib/jquery.min.js')?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/lib/jquery.easyui.min.js')?>" ></script>
    <script type="text/javascript" src="<?= base_url('assets/lib/queryLoader.js')?>" ></script>
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="<?= base_url('assets/lib/html5shiv.js')?>"></script>
    <![endif]-->
    </head>
<body class="easyui-layout">
        <!--top--> 
        <!-- <div data-options="region:'north'" style="height:100px; overflow: hidden;">
        <span style="float: right; margin:10px">
            <a href="<?=base_url('dashboard')?>" style="border: 0px;"><img src="<?= base_url('assets/images/logo.png')?>" alt="logo" style="height:80px;width:auto;" /></a>
        </span>
        </div> -->
        <div data-options="region:'north'" style="height: 100px; overflow: hidden;" class="bg-dot">
        	<span style="float: left; margin:10px;">
                <a href="<?=base_url()?>" style="border: 0px;"><img src="<?= base_url('assets/images/logo-partner.png')?>" alt="logo-partner" style="height:80px;width: auto;" /></a>
            </span>
            <span style="float: right; margin:10px;">
                <a href="<?=base_url()?>" style="border: 0px;"><img src="<?= base_url('assets/images/logo.png')?>" alt="logo" style="height:80px;width: auto;" /></a>
            </span>
        </div>
        <!--bottom-->   
        <div class="bg-levis" data-options="region:'south'" style="height:20px; margin: 0px; padding: 0px; overflow: hidden; color: #fff;">
        <center>
				<small>
					&copy; 2016. KES Modul PKBL - <a href="http://www.krakatau-it.co.id/" target="_blank">PT. Krakatau Information Technology</a>
				</small>
			</center>
        </div> 
        <div class="bg-dot" data-options="region:'center',tools:'#tt',iconCls:'icon-globe icon-large'" title="PKBL">  
        <div class="content">
        <?php echo $contents; ?>
        </div>
        </div>
        <div id="mm" class="easyui-menu" style="width:150px;">  
        <div onclick="javascript:$.messager.alert('About','PKBL 2017','info')" data-options="iconCls:'icon-info-sign'">About</div>   
        <div onclick="javascript:window.location.reload(true)" data-options="iconCls:'icon-refresh'" >Reload</div>  
        <div  data-options="iconCls:'icon-print',disabled:true">Print</div>  
        <div class="menu-sep"></div>  
        <div onclick="javascript:$.messager.alert('About','&copy; PKBL 2017','info')" data-options="iconCls:'icon-user'">Author</div>  
        </div>  
        <script>
			$('body').fadeIn(1000);
		</script>
        <script>  
       $(function(){  
       $(document).bind('contextmenu',function(e){  
               e.preventDefault();  
               $('#mm').menu('show', {  
               left: e.pageX,  
               top: e.pageY  
                });  
            });  
       });           
       $.fn.datebox.defaults.formatter = function(date) {
			var y = date.getFullYear();
			var m = date.getMonth() + 1;
			var d = date.getDate();
			return y + '-' + (m < 10 ? '0' + m : m) + '-' + (d < 10 ? '0' + d : d);
		};
		
		$.fn.datebox.defaults.parser = function(s) {
			if (s) {
				var a = s.split('-');
				var y = new Number(a[0]);
				var m = new Number(a[1]);
				var d = new Number(a[2]);
				var dd = new Date(y, m-1, d);
				return dd;
			} else {
				return new Date();
			}
		};
        </script>    
</body>
</html>