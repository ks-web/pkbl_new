	<?php
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
date_default_timezone_set("Asia/Jakarta");
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8" />
  <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta name="description" content="KES Modul PKBL" />
  <meta name="author" content="Fajar Isnandio" />
  <title>KS PKBL</title>

  <link rel="shortcut icon" href="<?= base_url('assets/images/kes.png')?>"/>
  <link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/font-awesome/css/font-awesome.css')?>" />
  <link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/bootstrap.css')?>"/>

  <link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/style.css')?>"/>
  <link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/themes/bootstrap/easyui.css')?>"/>
	<style>
		body {
			display: none;
		}
	</style>


  <script type="text/javascript" src="<?= base_url('assets/lib/jquery.min.js')?>"></script>
  <script type="text/javascript" src="<?= base_url('assets/lib/jquery.easyui.min.js')?>" ></script>
  <!-- <script type="text/javascript" src="<?= base_url('assets/lib/plugins/jquery.edatagrid.js')?>"></script> -->

  <script type="text/javascript" src="<?= base_url('assets/lib/ckeditor/ckeditor.js')?>"></script>
  <script type="text/javascript" src="<?= base_url('assets/lib/ckeditor/samples/js/sample.js')?>"></script>
  <link rel="stylesheet" href="<?= base_url('assets/lib/ckeditor/samples/css/samples.css')?>">
  <link rel="stylesheet" href="<?= base_url('assets/lib/ckeditor/samples/toolbarconfigurator/lib/codemirror/neo.css')?>">

  <!-- Format Angka -->
  <script src="<?=base_url('assets/lib/format.20110630-1100.min.js')?>"></script>
  <script>
    function tgl_ind(val)
    {
     if(val){
        var d = val.split("-");
        return d[2] + "/" + d[1]+ "/" + d[0];
    }	
}
function formatPrice(val,row){
   var number = format( "#.##0,####", val)
   return number;
}

function format_numberdisp(value,row,index) {
	var disp = format( "###,###,###,##0.00", value);
	return disp;
}
// $.fn.datebox.defaults.formatter = function(date){
 // var y = date.getFullYear();
 // var m = date.getMonth()+1;
 // var d = date.getDate();
 // return y+'-'+m+'-'+d;
// }
$.fn.datebox.defaults.formatter = function(date) {
	var y = date.getFullYear();
	var m = date.getMonth() + 1;
	var d = date.getDate();
	return y + '-' + (m < 10 ? '0' + m : m) + '-' + (d < 10 ? '0' + d : d);
};

$.fn.datebox.defaults.parser = function(s) {
	if (s) {
		var a = s.split('-');
		var y = new Number(a[0]);
		var m = new Number(a[1]);
		var d = new Number(a[2]);
		var dd = new Date(y, m-1, d);
		return dd;
	} else {
		return new Date();
	}
};

var detik;
var menit;
var jam;

$(document).ready(function(){

    function clock()
    {
        if (detik!=0 && detik%60==0) {
            menit++;
            detik=0;
        }
        second = detik;
        
        if (menit!=0 && menit%60==0) {
            jam++;
            menit=0;
        }
        minute = menit;
        
        if (jam!=0 && jam%24==0) {
            jam=0;
        }
        hour = jam;
        
        if (detik<10){
            second='0'+detik;
        }
        if (menit<10){
            minute='0'+menit;
        }
        
        if (jam<10){
            hour='0'+jam;
        }
        waktu = hour+':'+minute+':'+second;
        $('#waktu').html(waktu);
        detik++;
    }

    $.post('<?=base_url("home/waktu")?>/',function(result){
		if (result.status){
			detik = parseInt(result.detik);
			menit = parseInt(result.menit);
			jam = parseInt(result.jam);
			setInterval(clock,1000);
		} else {
			$('#waktu').html('Error');
		}
	},'json');

});
</script>

</head>
<body class="easyui-layout">
    <div data-options="region:'north'" style="height: 100px; overflow: hidden;" class="bg-dot">
    	<span style="float: left; margin:10px;">
            <a href="<?=base_url()?>" style="border: 0px;"><img src="<?= base_url('assets/images/logo-partner.png')?>" alt="logo-partner" style="height:80px;width: auto;" /></a>
        </span>
        <span style="float: right; margin:10px;">
            <a href="<?=base_url()?>" style="border: 0px;"><img src="<?= base_url('assets/images/kes.png')?>" alt="logo" style="height:80px;width: auto;" /></a>
        </span>
    </div>
    <!--top-->

    <!--right -->

    <div data-options="region:'east',split:true,collapsed:false,title:'Informasi',iconCls:' icon-bullhorn'" style="width:200px;padding:10px;">
    <h5>Jam : <span id="waktu"></span></h5>


    </div>
    <!-- end right -->

    <!--left-->
    <div class="bg-dot" data-options="region:'west',split:true,collapsed:false,iconCls:'icon-th'" title="Menu" style="width:220px; overflow:hidden">
		 
        <?php include( 'accordion.php'); 
        //$this->load->view('nav'); ?>
    </div>
    
       <div class="bg-dot" data-options="region:'center',iconCls:'icon-globe'" title="Selamat Datang <?=$this->session->userdata('name_hello')? '- '.$this->session->userdata('name_hello'):'' ?> ">
           <?php echo $contents?>
       </div>
        <!--div id="tbc">
        <a href="javascript:void(0)" class="btn btn-mini btn-danger">
        <i class="icon-lock"></i></a>
    </div-->
    <!--bottom-->
    <div class="bg-levis" data-options="region:'south'" style="height:20px; margin: 0px; padding: 0px; overflow: hidden; color: #fff;">
       <center>
        <small>
         ©2015. KES Modul PKBL - <a href="http://www.krakatau-it.co.id/" style="color:white;" target="_blank">PT. Krakatau Information Technology</a>
     </small>
 </center>
</div>

<div id="mm" class="easyui-menu" style="width:150px;">  
    <div onclick="javascript:$.messager.alert('About','Copyright &copy; PT. Krakatau Information Technology 2015, - Development By KES For Administration Team','info')" data-options="iconCls:'icon-info-sign'">About</div>   
    <div onclick="javascript:window.location.reload(true)" data-options="iconCls:'icon-refresh'" >Reload</div>  
    <div  data-options="iconCls:'icon-print',disabled:true">Print</div>  
    <div onclick="javascript:change_pass(<?php echo $this->session->userdata('idUser') ?>)">Ubah Kata Sandi</div>  
    <div class="menu-sep"></div>  
    <div  onclick="javascript:window.location.href='<?=base_url('home/logout')?>'" data-options="iconCls:'icon-remove'">Keluar</div>  
</div>  
<script>

   $(function(){  
       $(document).bind('contextmenu',function(e){

           e.preventDefault();  
           $('#mm').menu('show', {  
               left: e.pageX,
               top: e.pageY  
           });

       });

   }); 
   function change_pass(idUser){
    url = "<?php echo base_url('home/change_pass') ?>/"+idUser;
    $('#dlg_change').dialog('open').dialog('setTitle','Change Password');
}
</script>
<div id="dlg_change" class="easyui-dialog" style="width:300px; height:180px; padding:10px" closed="true" buttons="#buttons_change" >
    <form id="fm_change" method="post" enctype="multipart/form-data" action="">
        <table width="100%" align="center" border="0">
            <tr>
                <td> Your Password</td>
                <td>: <input type="password" id="pwd_lama" name="pwd_lama" style="width: 100px ;"/>
                </td>
            </tr>
            <tr>
                <td> New Password</td>
                <td>: <input type="password" id="pwd_baru" name="pwd_baru" style="width: 100px ;"/>
                </td>
            </tr>
        </table>
    </form>
    <script type="text/javascript">
        var url;
        function save_change_pass(){
            $('#fm_change').form('submit',{
                url: url,
                onSubmit: function(){
                    return $(this).form('validate');
                },
                success: function(result){
                    var result = eval('('+result+')');
                    if (result.success){
                        $('#dlg_change').dialog('close');

                    } else {
             //$('#dlg_change').dialog('close');
             $.messager.alert('Error Message',result.msg,'error');
         }
     }
 });
        }
    </script>
    <!-- end tombol datagrid -->
    <!-- Tombol simpan & Batal Form -->
    <div id="buttons_change">
        <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:save_change_pass()"><i class="icon-save icon-large"></i>&nbsp;Simpan</a>
        <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:$('#dlg_change').dialog('close')"><i class="icon-remove icon-large"></i>&nbsp;Batal</a>      
    </div>
</div>

<script>
	$('body').fadeIn(1000);
</script>
</body>
</html>