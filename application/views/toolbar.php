<script type="text/javascript">
var url;
function save(){
    $('#fm').form('submit',{
    	url: url,
    	onSubmit: function(){
    		return $(this).form('validate');
    	},
    	success: function(result){
    		var result = eval('('+result+')');
    		if (result.success){
    			$('#dlg').dialog('close');		
    			$('#dg').datagrid('reload');
                $('#dg').treegrid('reload');
                
    		} else {
    		 //$('#dlg').dialog('close');
             $.messager.alert('Error Message',result.msg,'error');
    		}
    	}
    });
}

function doSearch(value){
    $('#dg').datagrid('load',{    
    q:value  
    });   
}
</script>

        <!-- Tombol Add, Edit, Hapus Datagrid -->
        <div id="toolbar">  
            <table align="center" style="padding: 0px; width: 99%;">
                <tr>
                    <td>
                        <a href="javascript:void(0)" class="btn btn-small btn-success" onclick="javascript:add()"><i class="icon-plus-sign icon-large"></i>&nbsp;Add</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:edit()"><i class="icon-edit"></i>&nbsp;Edit</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-danger" onclick="javascript:hapus()"><i class="icon-trash icon-large"></i>&nbsp;Delete</a>
                    </td>
                    <td>&nbsp;</td>
                    <td align="right">
                    	<input class="easyui-searchbox" prompt="Silahkan Input Field" searcher="doSearch"  style="width:300px"/>
                    </td>
                </tr>
            </table>  
        </div>  
        <!-- end tombol datagrid -->
        <!-- Tombol simpan & Batal Form -->
        <div id="t_dlg_dis-buttons">
            <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:save()"><i class="icon-save icon-large"></i>&nbsp;Simpan</a>
            <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:$('#dlg').dialog('close')"><i class="icon-remove icon-large"></i>&nbsp;Batal</a>      
        </div>
        <!-- end tombol Form -->
        