<div class="panel-header accordion-header accordion-header-selected" style="height: 50px; width: 203px;">
	<a href="javascript:void(0)" class="btn btn-mini btn-info">
		<i class="icon-user">
			&nbsp;<?=$this->session->userdata('group_name') ? $this->session->userdata('group_name'):'Guest'?> 
		</i>
	</a>
	<a href="<?=base_url('home/logout')?>" class="btn btn-mini btn-danger">
		<i class="icon-lock">&nbsp;
			<?php
			if(!$this->session->userdata('logged_in')==1){
				echo 'Login';
			}else{
				echo 'Keluar';
			}?>
		</i></a>
		<br>
		<a href='javascript:void(0)' onclick='set_homepage()' style="margin-top:5px;" class='btn btn-mini btn-success'><i class='icon-home'>&nbsp;Tetapkan sebagai Beranda</i></a>
	</div>

	<div class="easyui-accordion"  animate="true" style="width:215px;"> 
		<?=$menu;?>
	</div>
	<script type="text/javascript">
		function set_homepage() {
			var url = '<?php echo $_SERVER["REQUEST_URI"]; ?>';
			$.post('<?php echo base_url("home/set_homepage") ?>',{url:url},function(result){
				if (result.success){
					$.messager.alert('Info','Success','info');
				} else {
					$.messager.alert('Error',result.msg,'error');
				}
			},'json');
		}
	</script>