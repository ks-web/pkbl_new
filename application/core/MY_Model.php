<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class MY_Model extends CI_Model {

	var $_table = "";
	var $_view = "";
	var $_order = "asc";
	var $_sort = "";
	var $_page = 1;
	var $_rows = 10;
	var $_filter = array();
	var $_like = array();
	var $_data = array();
	var $_param = array();
	var $_create = false;
	var $_update = false;

	public function __construct() {
		parent::__construct();
	}

	public function read($get = array()) {
		$this->_view = $this->_view ? $this->_view : $this->_table;
		$order = $this->input->post('order') ? $this->input->post('order') : $this->_order;
		$sort = $this->input->post('sort') ? $this->input->post('sort') : $this->_sort;
		$page = $this->input->post('page') ? intval($this->input->post('page')) : $this->_page;
		$rows = $this->input->post('rows') ? intval($this->input->post('rows')) : $this->_rows;
		$offset = (($page - 1) * $rows);
		if (sizeof($get) > 0) {
			$this->db->where($get);
		}
		if (sizeof($this->_filter) > 0) {
			$this->db->where($this->_filter);
		}
		if (sizeof($this->_like) > 0) {
			$this->db->or_like($this->_like);
		}
		$this->db->order_by($sort, $order);
		$this->db->limit($rows, $offset);
		$result = $this->db->get($this->_view);

		$json['total'] = $this->_total($get);
		$json['rows'] = $result->result_array();

		return json_encode($json);
	}

	protected function _total($get = array()) {
	    $this->db->select('COUNT(*) as count');
		if (sizeof($get) > 0) {
			$this->db->where($get);
		}
		if (sizeof($this->_filter) > 0) {
			$this->db->where($this->_filter);
		}
		$this->_view = $this->_view ? $this->_view : $this->_table;
		return $this->db->get($this->_view)->row()->count;
	}

	public function read_one($get = array()) {
		$this->_view = $this->_view ? $this->_view : $this->_table;
		if (sizeof($get) > 0) {
			$this->db->where($get);
		}
		if (sizeof($this->_filter) > 0) {
			$this->db->where($this->_filter);
		}
		$result = $this->db->get($this->_view);
		return json_encode($result->row_array());
	}

	public function read_all($get = array()) {
		$this->_view = $this->_view ? $this->_view : $this->_table;
		if (sizeof($get) > 0) {
			$this->db->where($get);
		}
		if (sizeof($this->_filter) > 0) {
			$this->db->where($this->_filter);
		}
		$order = $this->input->post('order') ? $this->input->post('order') : $this->_order;
		$sort = $this->input->post('sort') ? $this->input->post('sort') : $this->_sort;
		$this->db->order_by($sort, $order);
		$result = $this->db->get($this->_view);
		return json_encode($result->result_array());
	}

	public function insert($data_add = array()) {
		if ($this->_create == true) {
			$create = array(
				// 'creator' => $this->session->userdata('id'),
				// 'tanggal_create' => date("Y-m-d H:i:s")
				// 'create_user' => $this->session->userdata('username') ?
				// $this->session->userdata('username') : gethostname(),
				// 'create_date' => date("Y-m-d H:i:s"),
				// 'create_ip' => $this->input->ip_address()
			);
		} else {
			$create = array();
		}
		$data = array_merge($this->_param, $this->_data, $create, $data_add);
		$this->db->insert($this->_table, $data);
		return $this->db->affected_rows();
	}

	public function update($get = array(), $data_add = array()) {
		if ($this->_update == true) {
			$update = array(
				// 'modifier' => $this->session->userdata('id'),
				// 'tanggal_modified' => date("Y-m-d H:i:s")
				// 'update_user' => $this->session->userdata('username') ?
				// $this->session->userdata('username') : gethostname(),
				// 'update_date' => date("Y-m-d H:i:s"),
				// 'update_ip' => $this->input->ip_address()
			);
		} else {
			$update = array();
		}
		if (sizeof($get) > 0) {
			$this->db->where($get);
		} else {
			$this->db->where($this->_param);
		}

		$data = array_merge($this->_data, $update, $data_add);
		$this->db->update($this->_table, $data);
		return $this->db->affected_rows();
	}

	public function delete($get = array()) {
		if (sizeof($get) > 0) {
			$this->db->where($get);
		} else {
			$this->db->where($this->_param);
		}
		$this->db->delete($this->_table);
		return $this->db->affected_rows();
	}

	/* -----------------------------------------------------------------------------------
	 * */

	public function getOtherDataFecth($table, $where = array()) {
		if (sizeof($where) > 0) {
			$this->db->where($where);
		}
		$query = $this->db->get($table);
		return json_encode($query->result_array());

	}

	public function getOtherData($table, $where = array()) {
		if (sizeof($where) > 0) {
			$this->db->where($where);
		}
		$query = $this->db->get($table);
		return json_encode($query->row_array());

	}

	public function insertOtherData($table, $data = array()) {
		$this->db->insert($table, $data);
		return $this->db->affected_rows();

	}

	public function updateOtherData($table, $where = array(), $data = array()) {
		if (sizeof($where) > 0) {
			$this->db->where($where);
		}
		$this->db->update($table, $data);
		return $this->db->affected_rows();

	}

	public function deleteOtherData($table, $where = array()) {
		if (sizeof($where) > 0) {
			$this->db->where($where);
		}
		$this->db->delete($table);
		return $this->db->affected_rows();

	}

}
