<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

	protected $models = array();
	protected $model_string = 'model_%';
	protected $helpers = array();

	// define parameter additional
	var $data_add = array();
	var $where_add = array();

	public function __construct() {
		parent::__construct();
		if (!$this->session->userdata('logged_in') == 1) {
			redirect(base_url('home'));
		}
		$this->_load_models();
		$this->_load_helpers();

		$this->load->model('model_menu');

		$this->load->helper('cookie');

		$exp = time() + $this->config->item('sess_expiration');
		$this->input->set_cookie(array(
			'name' => 'group_id',
			'value' => $this->session->userdata('group_id'),
			'expire' => $exp
		));
	}

	private function _load_models() {
		foreach ($this->models as $model) {
			$this->load->model($this->_model_name($model), $model);
		}
	}

	protected function _model_name($model) {
		return str_replace('%', $model, $this->model_string);
	}

	private function _load_helpers() {
		foreach ($this->helpers as $helper) {
			$this->load->helper($helper);
		}
	}

	// log vendor
	protected function _log_vendor($type = null, $ket = null) {
		$type_allowed = array(
			'create' => 'C',
			'update' => 'U',
			'delete' => 'D'
		);

		$ket = $ket == null ? $this->uri->segment(1, '') . '/' . $this->uri->segment(2, '') : $ket;
		$vendor_code = $this->session->userdata('vendor_code');
		if ($vendor_code && $type) {
			if (array_key_exists($type, $type_allowed)) {
				$data = array(
					'vendor_code' => $vendor_code,
					'tipe' => $type_allowed[$type],
					'keterangan' => $ket
				);
				$this->db->insert('vendor_act_log', $data);
			}

		}
	}

	function mail_sent($mailTo = array(), $subj = '', $content = '') {
		require_once (APPPATH . 'libraries/PHPMailerAutoload.php');
		$mail = new PHPMailer();

		// $mail->SMTPDebug = 3;

		$mail->IsSMTP();
		$mail->SMTPAuth = true;
		$mail->SMTPSecure = mail_SMTPSecure;
		$mail->Port = mail_Port;
		$mail->Host = mail_Host;
		$mail->Username = mail_Username;
		$mail->Password = mail_Password;
		$mail->SetFrom(mail_Username, mail_AppName . PERUSAHAAN);
		$mail->Subject = $subj;
		$mail->IsHTML(TRUE);

		$mail->MsgHTML($content);

		if (sizeof($mailTo) > 0) {
			for ($i = 0; $i < sizeof($mailTo); $i++) {
				$mail->AddAddress($mailTo[$i]);
			}
		}

		if ($mail->Send()) {
			$data["status"] = true;
			$data["message"] = "Message sent correctly!";
		} else {
			$data["status"] = false;
			$data["message"] = "Error: " . $mail->ErrorInfo;
		}
		return $data;
	}

	function mail_sent_with_attchment($mailTo = array(), $subj = '', $content = '', $attch = array()) {
		require_once (APPPATH . 'libraries/PHPMailerAutoload.php');
		$mail = new PHPMailer();

		// $mail->SMTPDebug = 3;

		$mail->IsSMTP();
		$mail->SMTPAuth = true;
		$mail->SMTPSecure = mail_SMTPSecure;
		$mail->Port = mail_Port;
		$mail->Host = mail_Host;
		$mail->Username = mail_Username;
		$mail->Password = mail_Password;
		$mail->SetFrom(mail_Username, mail_AppName . PERUSAHAAN);
		$mail->Subject = $subj;
		$mail->IsHTML(TRUE);

		$mail->MsgHTML($content);

		if (sizeof($mailTo) > 0) {
			for ($i = 0; $i < sizeof($mailTo); $i++) {
				$mail->AddAddress($mailTo[$i]);
			}
		}

		if (sizeof($attch) > 0) {
			for ($i = 0; $i < sizeof($attch); $i++) {
				$mail->addAttachment($attch[$i]);
			}
		}

		if ($mail->Send()) {
			$data["status"] = true;
			$data["message"] = "Message sent correctly!";
		} else {
			$data["status"] = false;
			$data["message"] = "Error: " . $mail->ErrorInfo;
		}
		return $data;
	}

}
