<style>
table {
	font-family: 'sans-serif'
}
.bold {
	font-weight: bold;
}
.fontred {
	color: red;
}
.font10 {
	font-size: 10px;
}
.font11 {
	font-size: 11px;
}
.font12 {
	font-size: 12px;
}
.font13 {
	font-size: 13px;
}
.col-1 {width: 8.33%;}
.col-2 {width: 16.66%;}
.col-3 {width: 25%;}
.col-4 {width: 33.33%;}
.col-5 {width: 41.66%;}
.col-6 {width: 50%;}
.col-7 {width: 58.33%;}
.col-8 {width: 66.66%;}
.col-9 {width: 75%;}
.col-10 {width: 83.33%;}
.col-11 {width: 91.66%;}
.col-12 {width: 100%;}

.col-h-1 {width: 15%;}
.col-h-2 {width: 2%;}
.col-h-3 {width: 56%;}
.col-h-4 {width: 15%;}
.col-h-5 {width: 2%;}
.col-h-6 {width: 10%;}
.center {
	text-align: center;
}
.right {
	text-align: right;
}
</style>
<?php //print_r($mitra); ?>
<table border="0" width="80%" align="center">
	<tr>
		<td></td>
		<td></td>
		<td align="center"><span style="font-size: 30px">Kartu Piutang<span></td>
		<td colspan="3" rowspan="2" align="left"><span style="font-size: 36px"><span></td>
	</tr>
	<tr>
		<td></td>
		<td></td>
		<td align="center">Posisi <?php echo bulan_indo(str_replace('0', '', date('m'))).' '.date('Y'); ?></td>
	</tr>
	<tr>
		<td class="col-h-1">No Kartu</td>
		<td class="col-h-2">:</td>
		<td class="col-h-3"><?php echo $mitra[0]['id_mitra']; ?></td>
		<td class="col-h-4">Pinjaman Pokok</td>
		<td class="col-h-5">:</td>
		<td class="col-h-6 right"><?php echo number_format($mitra[0]['angsuran_pokok']); ?></td>
	</tr>
	<tr>
		<td class="col-h-1">No Kontrak</td>
		<td class="col-h-2">:</td>
		<td class="col-h-3"><?php echo $mitra[0]['no_kontrak']; ?></td>
		<td class="col-h-4">Bunga <?php echo $mitra[0]['bunga']; ?>,00% x <?php echo $mitra[0]['tahun']; ?> Th</td>
		<td class="col-h-5">:</td>
		<td class="col-h-6 right"><?php echo number_format($mitra[0]['angsuran_bunga']); ?></td>
	</tr>
	<tr>
		<td class="col-h-1">Nama Usaha</td>
		<td class="col-h-2">:</td>
		<td class="col-h-3"><?php echo $mitra[0]['nama_perusahaan']; ?></td>
		<td class="col-h-4">Total Hutang</td>
		<td class="col-h-5">:</td>
		<td class="col-h-6 right"><?php echo number_format($mitra[0]['total_pinjaman_bunga']); ?></td>
	</tr>
	<tr>
		<td class="col-h-1">Nama Pemilik</td>
		<td class="col-h-2">:</td>
		<td class="col-h-3"><?php echo $mitra[0]['nama']; ?></td>
		<td class="col-h-4">Angsuran Pokok</td>
		<td class="col-h-5">:</td>
		<td class="col-h-6 right"><?php echo number_format($mitra[0]['angsuranpokok_perbln']); ?></td>
	</tr>
	<tr>
		<td class="col-h-1">Alamat</td>
		<td class="col-h-2">:</td>
		<td class="col-h-3"><?php echo $mitra[0]['alamat']; ?></td>
		<td class="col-h-4">Angsuran Bunga</td>
		<td class="col-h-5">:</td>
		<td class="col-h-6 right"><?php echo number_format($mitra[0]['angsuranbunga_perbln']); ?></td>
	</tr>
	<tr>
		<td class="col-h-1"></td>
		<td class="col-h-2">:</td>
		<td class="col-h-3"><?php echo $mitra[0]['kelurahan'].' '.$mitra[0]['kecamatan'].' '.$mitra[0]['kota'].' '.$mitra[0]['propinsi']; ?>, Telp. <?php echo $mitra[0]['telepon'].' '.$mitra[0]['handphone']; ?></td>
		<td class="col-h-4">Jumlah Angsuran</td>
		<td class="col-h-5">:</td>
		<td class="col-h-6 right"><?php echo number_format($mitra[0]['angsuran_bulanan']); ?></td>
	</tr>
	<tr>
		<td class="col-h-1">Sektor Usaha</td>
		<td class="col-h-2">:</td>
		<td class="col-h-3"><?php echo $mitra[0]['nama_sektor']; ?></td>
		<td class="col-h-4">Angs. Pertama</td>
		<td class="col-h-5">:</td>
		<td class="col-h-6 right"><?php if($mitra[0]['tanggal_angsuran'] != ''){ echo date("d/m/Y", strtotime($mitra[0]['tanggal_angsuran'])); } ?></td>
	</tr>
</table>
<table border="0" width="80%" align="center">
	<tr>
		<td colspan="2">Rincian Transaksi :</td>
	</tr>
	<tr>
		<td colspan="2"></td>
	</tr>
	<tr>
		<td class="col-12" colspan="2">
			<table border="1" width="100%">
				<tr>
					<td class="col-1 center" rowspan="2">No</td>
					<td class="col-1 center" rowspan="2">Kode</td>
					<td class="col-1 center" rowspan="2">Tanggal</td>
					<td class="center" colspan="2">Mutasi</td>
					<td class="center" colspan="3">Saldo</td>
				</tr>
				<tr>
					<td class="center col-2">Pokok</td>
					<td class="center col-1">Bunga</td>
					<td class="center col-2">Pokok</td>
					<td class="center col-1">Bunga</td>
					<td class="center col-2">Jumlah</td>
				</tr>
				<tr>
				<?php
					//$angsuran_pokok = $mitra[0]['angsuran_pokok']*(12*$mitra[0]['tahun']);
					$angsuran_pokok = $mitra[0]['angsuran_pokok'];
					//$angsuran_bunga = $mitra[0]['angsuran_bunga']*(12*$mitra[0]['tahun']);
					$angsuran_bunga = $mitra[0]['angsuran_bunga'];
				?>
					<td class="center"><?php echo '1'; ?></td>
					<td><?php echo $mitra[0]['id_cd']; ?></td>
					<td class="center"><?php echo date("d/m/Y", strtotime($mitra[0]['tanggal_cd'])); ?></td>
					<td class="right"><?php echo number_format($angsuran_pokok); ?></td>
					<td class="right"><?php echo number_format($angsuran_bunga); ?></td>
					<td class="right"><?php echo number_format($angsuran_pokok); ?></td>
					<td class="right"><?php echo number_format($angsuran_bunga); ?></td>
					<td class="right"><?php echo number_format($mitra[0]['total_pinjaman_bunga']); ?></td>
				</tr>
				<?php $i = 2; $aangsuran = 0; $abiaya = 0; 
					foreach ($angsuran as $key) { 
						$angsuran_pokok = $angsuran_pokok - $key['jumlah'];
						$angsuran_bunga = $angsuran_bunga - $key['biaya'];
						if($key['status_pelunasan'] == 1){
							$sisa_angsuran = 0;
							$angsuran_bunga = 0;
						} else {
							$sisa_angsuran = $key['sisa_angsuran'];
						}
					?>
				<tr>
					<td class="center"><?php echo $i; ?></td>
					<td><?php echo $key['id_angsuran']; ?></td>
					<td class="center"><?php echo date("d/m/Y", strtotime($key['tanggal_angsuran'])); ?></td>
					<td class="right">-<?php echo number_format($key['jumlah']); ?></td>
					<td class="right">-<?php echo number_format($key['biaya']); ?></td>
					<td class="right"><?php echo number_format($angsuran_pokok); ?></td>
					<td class="right"><?php echo number_format($angsuran_bunga); ?></td>
					<td class="right"><?php echo number_format($sisa_angsuran); ?></td>
				</tr>
				<?php $i++; 
					$aangsuran = $aangsuran + $key['jumlah']; 
					$abiaya = $abiaya + $key['biaya'];
				} ?>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			&nbsp;
		</td>
	</tr>
	<tr>
		<td class="col-6">
			Catatan :
		</td>
		<td class="col-6 center">
			Divisi Commmunity Development
		</td>
	</tr>
	<tr>
		<td class="col-6">
			<table>
				<tr>
					<td>Akumulasi Angsuran Pokok</td>
					<td>: Rp</td>
					<td>&nbsp;</td>
					<td class="right"><?php echo number_format($aangsuran); ?></td>
				</tr>
				<tr>
					<td>Akumulasi Angsuran Bunga</td>
					<td>: Rp</td>
					<td>&nbsp;</td>
					<td class="right"><?php echo number_format($abiaya); ?></td>
				</tr>
				<tr>
					<td><b>Jumlah</b></td>
					<td><b>: Rp</b></td>
					<td>&nbsp;</td>
					<td class="right"><b><?php echo number_format($aangsuran+$abiaya); ?></b></td>
				</tr>
			</table>
		</td>
		<td class="col-6">
			&nbsp;
		</td>
	</tr>
	<tr>
		<td class="col-6">
			&nbsp;
		</td>
		<td class="col-6 center">
			<?php echo $pegawai[0]['nama_pegawai']; ?>
		</td>
	</tr>
	<tr>
		<td class="col-6">
			Dicetak <?php echo date('d M Y'); ?>
		</td>
		<td class="col-6 center">
			Manager
		</td>
	</tr>
</table>

<?php if(count($angsuranP1) > 0) { ?>
<div style="page-break-before: always;"></div>

<table border="0" width="80%" align="center">
	<tr>
		<td></td>
		<td></td>
		<td align="center"><span style="font-size: 30px">Kartu Piutang<span></td>
		<td colspan="3" rowspan="2" align="left"><span style="font-size: 36px">P1<span></td>
	</tr>
	<tr>
		<td></td>
		<td></td>
		<td align="center">Posisi <?php echo bulan_indo(str_replace('0', '', date('m'))).' '.date('Y'); ?></td>
	</tr>
	<tr>
		<td class="col-h-1">No Kartu</td>
		<td class="col-h-2">:</td>
		<td class="col-h-3"><?php echo $mitraP1[0]['id_mitra']; ?></td>
		<td class="col-h-4">Pinjaman Pokok</td>
		<td class="col-h-5">:</td>
		<td class="col-h-6 right"><?php echo number_format($mitraP1[0]['angsuran_pokok']); ?></td>
	</tr>
	<tr>
		<td class="col-h-1">No Kontrak</td>
		<td class="col-h-2">:</td>
		<td class="col-h-3"><?php echo $mitraP1[0]['no_kontrak']; ?></td>
		<td class="col-h-4">Bunga <?php echo $mitraP1[0]['bunga']; ?>,00% x <?php echo $mitraP1[0]['tahun']; ?> Th</td>
		<td class="col-h-5">:</td>
		<td class="col-h-6 right"><?php echo number_format($mitraP1[0]['angsuran_bunga']); ?></td>
	</tr>
	<tr>
		<td class="col-h-1">Nama Usaha</td>
		<td class="col-h-2">:</td>
		<td class="col-h-3"><?php echo $mitraP1[0]['nama_perusahaan']; ?></td>
		<td class="col-h-4">Total Hutang</td>
		<td class="col-h-5">:</td>
		<td class="col-h-6 right"><?php echo number_format($mitraP1[0]['total_pinjaman_bunga']); ?></td>
	</tr>
	<tr>
		<td class="col-h-1">Nama Pemilik</td>
		<td class="col-h-2">:</td>
		<td class="col-h-3"><?php echo $mitraP1[0]['nama']; ?></td>
		<td class="col-h-4">Angsuran Pokok</td>
		<td class="col-h-5">:</td>
		<td class="col-h-6 right"><?php echo number_format($mitraP1[0]['angsuranpokok_perbln']); ?></td>
	</tr>
	<tr>
		<td class="col-h-1">Alamat</td>
		<td class="col-h-2">:</td>
		<td class="col-h-3"><?php echo $mitraP1[0]['alamat']; ?></td>
		<td class="col-h-4">Angsuran Bunga</td>
		<td class="col-h-5">:</td>
		<td class="col-h-6 right"><?php echo number_format($mitraP1[0]['angsuranbunga_perbln']); ?></td>
	</tr>
	<tr>
		<td class="col-h-1"></td>
		<td class="col-h-2">:</td>
		<td class="col-h-3"><?php echo $mitraP1[0]['kelurahan'].' '.$mitraP1[0]['kecamatan'].' '.$mitraP1[0]['kota'].' '.$mitraP1[0]['propinsi']; ?>, Telp. <?php echo $mitraP1[0]['telepon'].' '.$mitraP1[0]['handphone']; ?></td>
		<td class="col-h-4">Jumlah Angsuran</td>
		<td class="col-h-5">:</td>
		<td class="col-h-6 right"><?php echo number_format($mitraP1[0]['angsuran_bulanan']); ?></td>
	</tr>
	<tr>
		<td class="col-h-1">Sektor Usaha</td>
		<td class="col-h-2">:</td>
		<td class="col-h-3"><?php echo $mitraP1[0]['nama_sektor']; ?></td>
		<td class="col-h-4">Angs. Pertama</td>
		<td class="col-h-5">:</td>
		<td class="col-h-6 right"><?php if($mitraP1[0]['tanggal_angsuran'] != ''){ echo date("d/m/Y", strtotime($mitraP1[0]['tanggal_angsuran'])); } ?></td>
	</tr>
</table>

<table border="0" width="80%" align="center">
	<tr>
		<td colspan="2">Rincian Transaksi :</td>
	</tr>
	<tr>
		<td colspan="2"></td>
	</tr>
	<tr>
		<td class="col-12" colspan="2">
			<table border="1" width="100%">
				<tr>
					<td class="col-1 center" rowspan="2">No</td>
					<td class="col-1 center" rowspan="2">Kode</td>
					<td class="col-1 center" rowspan="2">Tanggal</td>
					<td class="center" colspan="2">Mutasi</td>
					<td class="center" colspan="3">Saldo</td>
				</tr>
				<tr>
					<td class="center col-2">Pokok</td>
					<td class="center col-1">Bunga</td>
					<td class="center col-2">Pokok</td>
					<td class="center col-1">Bunga</td>
					<td class="center col-2">Jumlah</td>
				</tr>
				<tr>
					<td class="center">1</td>
					<td>P1</td>
					<td class="center"><?php echo date("d/m/Y", strtotime($key['tanggal_angsuran'])); ?></td>
					<td class="right"><?php echo number_format(0); ?></td>
					<td class="right"><?php echo number_format(0); ?></td>
					<td class="right"><?php echo number_format($mitraP1[0]['angsuran_pokok']); ?></td>
					<td class="right"><?php echo number_format(0); ?></td>
					<td class="right"><?php echo number_format($mitraP1[0]['angsuran_pokok']); ?></td>
				</tr>
				<?php
					//$angsuran_pokok = $mitra[0]['angsuran_pokok']*(12*$mitra[0]['tahun']);
					$angsuran_pokok = $mitraP1[0]['angsuran_pokok'];
					//$angsuran_bunga = $mitra[0]['angsuran_bunga']*(12*$mitra[0]['tahun']);
					$angsuran_bunga = $mitraP1[0]['angsuran_bunga'];
				?>
				<?php $i = 2; $aangsuran = 0; $abiaya = 0; 
					foreach ($angsuranP1 as $key) { 
						$angsuran_pokok = $angsuran_pokok - $key['jumlah'];
						$angsuran_bunga = 0;
						if($key['status_pelunasan'] == 1){
							$sisa_angsuran = 0;
							$angsuran_bunga = 0;
						} else {
							$sisa_angsuran = $key['sisa_angsuran'];
						}
					?>
				<tr>
					<td class="center"><?php echo $i; ?></td>
					<td><?php echo $key['id_angsuran']; ?></td>
					<td class="center"><?php echo date("d/m/Y", strtotime($key['tanggal_angsuran'])); ?></td>
					<td class="right">-<?php echo number_format($key['jumlah']); ?></td>
					<td class="right"><?php echo number_format(0); ?></td>
					<td class="right"><?php echo number_format($angsuran_pokok); ?></td>
					<td class="right"><?php echo number_format(0); ?></td>
					<td class="right"><?php echo number_format($sisa_angsuran); ?></td>
				</tr>
				<?php $i++; 
					$aangsuran = $aangsuran + $key['jumlah']; 
					$abiaya = $abiaya + 0;
				} ?>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			&nbsp;
		</td>
	</tr>
	<tr>
		<td class="col-6">
			Catatan :
		</td>
		<td class="col-6 center">
			Divisi Commmunity Development
		</td>
	</tr>
	<tr>
		<td class="col-6">
			<table>
				<tr>
					<td>Akumulasi Angsuran Pokok</td>
					<td>: Rp</td>
					<td>&nbsp;</td>
					<td class="right"><?php echo number_format($aangsuran); ?></td>
				</tr>
				<tr>
					<td>Akumulasi Angsuran Bunga</td>
					<td>: Rp</td>
					<td>&nbsp;</td>
					<td class="right"><?php echo number_format($abiaya); ?></td>
				</tr>
				<tr>
					<td><b>Jumlah</b></td>
					<td><b>: Rp</b></td>
					<td>&nbsp;</td>
					<td class="right"><b><?php echo number_format($aangsuran+$abiaya); ?></b></td>
				</tr>
			</table>
		</td>
		<td class="col-6">
			&nbsp;
		</td>
	</tr>
	<tr>
		<td class="col-6">
			&nbsp;
		</td>
		<td class="col-6 center">
			<?php echo $pegawai[0]['nama_pegawai']; ?>
		</td>
	</tr>
	<tr>
		<td class="col-6">
			Dicetak <?php echo date('d M Y'); ?>
		</td>
		<td class="col-6 center">
			Manager
		</td>
	</tr>
</table>
<?php } ?>

<?php if(count($angsuranP2) > 0) { ?>
<div style="page-break-before: always;"></div>

<table border="0" width="80%" align="center">
	<tr>
		<td></td>
		<td></td>
		<td align="center"><span style="font-size: 30px">Kartu Piutang<span></td>
		<td colspan="3" rowspan="2" align="left"><span style="font-size: 36px">P2<span></td>
	</tr>
	<tr>
		<td></td>
		<td></td>
		<td align="center">Posisi <?php echo bulan_indo(str_replace('0', '', date('m'))).' '.date('Y'); ?></td>
	</tr>
	<tr>
		<td class="col-h-1">No Kartu</td>
		<td class="col-h-2">:</td>
		<td class="col-h-3"><?php echo $mitraP2[0]['id_mitra']; ?></td>
		<td class="col-h-4">Pinjaman Pokok</td>
		<td class="col-h-5">:</td>
		<td class="col-h-6 right"><?php echo number_format($mitraP2[0]['angsuran_pokok']); ?></td>
	</tr>
	<tr>
		<td class="col-h-1">No Kontrak</td>
		<td class="col-h-2">:</td>
		<td class="col-h-3"><?php echo $mitraP2[0]['no_kontrak']; ?></td>
		<td class="col-h-4">Bunga <?php echo $mitraP2[0]['bunga']; ?>,00% x <?php echo $mitraP2[0]['tahun']; ?> Th</td>
		<td class="col-h-5">:</td>
		<td class="col-h-6 right"><?php echo number_format($mitraP2[0]['angsuran_bunga']); ?></td>
	</tr>
	<tr>
		<td class="col-h-1">Nama Usaha</td>
		<td class="col-h-2">:</td>
		<td class="col-h-3"><?php echo $mitraP2[0]['nama_perusahaan']; ?></td>
		<td class="col-h-4">Total Hutang</td>
		<td class="col-h-5">:</td>
		<td class="col-h-6 right"><?php echo number_format($mitraP2[0]['total_pinjaman_bunga']); ?></td>
	</tr>
	<tr>
		<td class="col-h-1">Nama Pemilik</td>
		<td class="col-h-2">:</td>
		<td class="col-h-3"><?php echo $mitraP2[0]['nama']; ?></td>
		<td class="col-h-4">Angsuran Pokok</td>
		<td class="col-h-5">:</td>
		<td class="col-h-6 right"><?php echo number_format($mitraP2[0]['angsuranpokok_perbln']); ?></td>
	</tr>
	<tr>
		<td class="col-h-1">Alamat</td>
		<td class="col-h-2">:</td>
		<td class="col-h-3"><?php echo $mitraP2[0]['alamat']; ?></td>
		<td class="col-h-4">Angsuran Bunga</td>
		<td class="col-h-5">:</td>
		<td class="col-h-6 right"><?php echo number_format($mitraP2[0]['angsuranbunga_perbln']); ?></td>
	</tr>
	<tr>
		<td class="col-h-1"></td>
		<td class="col-h-2">:</td>
		<td class="col-h-3"><?php echo $mitraP2[0]['kelurahan'].' '.$mitraP2[0]['kecamatan'].' '.$mitraP2[0]['kota'].' '.$mitraP2[0]['propinsi']; ?>, Telp. <?php echo $mitraP2[0]['telepon'].' '.$mitraP2[0]['handphone']; ?></td>
		<td class="col-h-4">Jumlah Angsuran</td>
		<td class="col-h-5">:</td>
		<td class="col-h-6 right"><?php echo number_format($mitraP2[0]['angsuran_bulanan']); ?></td>
	</tr>
	<tr>
		<td class="col-h-1">Sektor Usaha</td>
		<td class="col-h-2">:</td>
		<td class="col-h-3"><?php echo $mitraP2[0]['nama_sektor']; ?></td>
		<td class="col-h-4">Angs. Pertama</td>
		<td class="col-h-5">:</td>
		<td class="col-h-6 right"><?php if($mitraP2[0]['tanggal_angsuran'] != ''){ echo date("d/m/Y", strtotime($mitraP2[0]['tanggal_angsuran'])); } ?></td>
	</tr>
</table>

<table border="0" width="80%" align="center">
	<tr>
		<td colspan="2">Rincian Transaksi :</td>
	</tr>
	<tr>
		<td colspan="2"></td>
	</tr>
	<tr>
		<td class="col-12" colspan="2">
			<table border="1" width="100%">
				<tr>
					<td class="col-1 center" rowspan="2">No</td>
					<td class="col-1 center" rowspan="2">Kode</td>
					<td class="col-1 center" rowspan="2">Tanggal</td>
					<td class="center" colspan="2">Mutasi</td>
					<td class="center" colspan="3">Saldo</td>
				</tr>
				<tr>
					<td class="center col-2">Pokok</td>
					<td class="center col-1">Bunga</td>
					<td class="center col-2">Pokok</td>
					<td class="center col-1">Bunga</td>
					<td class="center col-2">Jumlah</td>
				</tr>
				<tr>
					<td class="center">1</td>
					<td>P2</td>
					<td class="center"><?php echo date("d/m/Y", strtotime($key['tanggal_angsuran'])); ?></td>
					<td class="right"><?php echo number_format(0); ?></td>
					<td class="right"><?php echo number_format(0); ?></td>
					<td class="right"><?php echo number_format($mitraP2[0]['angsuran_pokok']); ?></td>
					<td class="right"><?php echo number_format(0); ?></td>
					<td class="right"><?php echo number_format($mitraP2[0]['angsuran_pokok']); ?></td>
				</tr>
				<?php
					//$angsuran_pokok = $mitra[0]['angsuran_pokok']*(12*$mitra[0]['tahun']);
					$angsuran_pokok = $mitraP2[0]['angsuran_pokok'];
					//$angsuran_bunga = $mitra[0]['angsuran_bunga']*(12*$mitra[0]['tahun']);
					$angsuran_bunga = $mitraP2[0]['angsuran_bunga'];
				?>
				<?php $i = 2; $aangsuran = 0; $abiaya = 0; 
					foreach ($angsuranP2 as $key) { 
						$angsuran_pokok = $angsuran_pokok - $key['jumlah'];
						$angsuran_bunga = 0;
						if($key['status_pelunasan'] == 1){
							$sisa_angsuran = 0;
							$angsuran_bunga = 0;
						} else {
							$sisa_angsuran = $key['sisa_angsuran'];
						}
					?>
				<tr>
					<td class="center"><?php echo $i; ?></td>
					<td><?php echo $key['id_angsuran']; ?></td>
					<td class="center"><?php echo date("d/m/Y", strtotime($key['tanggal_angsuran'])); ?></td>
					<td class="right">-<?php echo number_format($key['jumlah']); ?></td>
					<td class="right"><?php echo number_format(0); ?></td>
					<td class="right"><?php echo number_format($angsuran_pokok); ?></td>
					<td class="right"><?php echo number_format(0); ?></td>
					<td class="right"><?php echo number_format($sisa_angsuran); ?></td>
				</tr>
				<?php $i++; 
					$aangsuran = $aangsuran + $key['jumlah']; 
					$abiaya = $abiaya + 0;
				} ?>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			&nbsp;
		</td>
	</tr>
	<tr>
		<td class="col-6">
			Catatan :
		</td>
		<td class="col-6 center">
			Divisi Commmunity Development
		</td>
	</tr>
	<tr>
		<td class="col-6">
			<table>
				<tr>
					<td>Akumulasi Angsuran Pokok</td>
					<td>: Rp</td>
					<td>&nbsp;</td>
					<td class="right"><?php echo number_format($aangsuran); ?></td>
				</tr>
				<tr>
					<td>Akumulasi Angsuran Bunga</td>
					<td>: Rp</td>
					<td>&nbsp;</td>
					<td class="right"><?php echo number_format($abiaya); ?></td>
				</tr>
				<tr>
					<td><b>Jumlah</b></td>
					<td><b>: Rp</b></td>
					<td>&nbsp;</td>
					<td class="right"><b><?php echo number_format($aangsuran+$abiaya); ?></b></td>
				</tr>
			</table>
		</td>
		<td class="col-6">
			&nbsp;
		</td>
	</tr>
	<tr>
		<td class="col-6">
			&nbsp;
		</td>
		<td class="col-6 center">
			<?php echo $pegawai[0]['nama_pegawai']; ?>
		</td>
	</tr>
	<tr>
		<td class="col-6">
			Dicetak <?php echo date('d M Y'); ?>
		</td>
		<td class="col-6 center">
			Manager
		</td>
	</tr>
</table>
<?php } ?>
<?php if(count($angsuranPB) > 0) { ?>
<div style="page-break-before: always;"></div>

<table border="0" width="80%" align="center">
	<tr>
		<td></td>
		<td></td>
		<td align="center"><span style="font-size: 30px">Kartu Piutang<span></td>
		<td colspan="3" rowspan="2" align="left"><span style="font-size: 36px">PB<span></td>
	</tr>
	<tr>
		<td></td>
		<td></td>
		<td align="center">Posisi <?php echo bulan_indo(str_replace('0', '', date('m'))).' '.date('Y'); ?></td>
	</tr>
	<tr>
		<td class="col-h-1">No Kartu</td>
		<td class="col-h-2">:</td>
		<td class="col-h-3"><?php echo $mitraPB[0]['id_mitra']; ?></td>
		<td class="col-h-4">Pinjaman Pokok</td>
		<td class="col-h-5">:</td>
		<td class="col-h-6 right"><?php echo number_format($mitraPB[0]['angsuran_pokok']); ?></td>
	</tr>
	<tr>
		<td class="col-h-1">No Kontrak</td>
		<td class="col-h-2">:</td>
		<td class="col-h-3"><?php echo $mitraPB[0]['no_kontrak']; ?></td>
		<td class="col-h-4">Bunga <?php echo $mitraPB[0]['bunga']; ?>,00% x <?php echo $mitraPB[0]['tahun']; ?> Th</td>
		<td class="col-h-5">:</td>
		<td class="col-h-6 right"><?php echo number_format($mitraPB[0]['angsuran_bunga']); ?></td>
	</tr>
	<tr>
		<td class="col-h-1">Nama Usaha</td>
		<td class="col-h-2">:</td>
		<td class="col-h-3"><?php echo $mitraPB[0]['nama_perusahaan']; ?></td>
		<td class="col-h-4">Total Hutang</td>
		<td class="col-h-5">:</td>
		<td class="col-h-6 right"><?php echo number_format($mitraPB[0]['total_pinjaman_bunga']); ?></td>
	</tr>
	<tr>
		<td class="col-h-1">Nama Pemilik</td>
		<td class="col-h-2">:</td>
		<td class="col-h-3"><?php echo $mitraPB[0]['nama']; ?></td>
		<td class="col-h-4">Angsuran Pokok</td>
		<td class="col-h-5">:</td>
		<td class="col-h-6 right"><?php echo number_format($mitraPB[0]['angsuranpokok_perbln']); ?></td>
	</tr>
	<tr>
		<td class="col-h-1">Alamat</td>
		<td class="col-h-2">:</td>
		<td class="col-h-3"><?php echo $mitraPB[0]['alamat']; ?></td>
		<td class="col-h-4">Angsuran Bunga</td>
		<td class="col-h-5">:</td>
		<td class="col-h-6 right"><?php echo number_format($mitraPB[0]['angsuranbunga_perbln']); ?></td>
	</tr>
	<tr>
		<td class="col-h-1"></td>
		<td class="col-h-2">:</td>
		<td class="col-h-3"><?php echo $mitraPB[0]['kelurahan'].' '.$mitraPB[0]['kecamatan'].' '.$mitraPB[0]['kota'].' '.$mitraPB[0]['propinsi']; ?>, Telp. <?php echo $mitraPB[0]['telepon'].' '.$mitraPB[0]['handphone']; ?></td>
		<td class="col-h-4">Jumlah Angsuran</td>
		<td class="col-h-5">:</td>
		<td class="col-h-6 right"><?php echo number_format($mitraPB[0]['angsuran_bulanan']); ?></td>
	</tr>
	<tr>
		<td class="col-h-1">Sektor Usaha</td>
		<td class="col-h-2">:</td>
		<td class="col-h-3"><?php echo $mitraPB[0]['nama_sektor']; ?></td>
		<td class="col-h-4">Angs. Pertama</td>
		<td class="col-h-5">:</td>
		<td class="col-h-6 right"><?php if($mitraPB[0]['tanggal_angsuran'] != ''){ echo date("d/m/Y", strtotime($mitraPB[0]['tanggal_angsuran'])); } ?></td>
	</tr>
</table>

<table border="0" width="80%" align="center">
	<tr>
		<td colspan="2">Rincian Transaksi :</td>
	</tr>
	<tr>
		<td colspan="2"></td>
	</tr>
	<tr>
		<td class="col-12" colspan="2">
			<table border="1" width="100%">
				<tr>
					<td class="col-1 center" rowspan="2">No</td>
					<td class="col-1 center" rowspan="2">Kode</td>
					<td class="col-1 center" rowspan="2">Tanggal</td>
					<td class="center" colspan="2">Mutasi</td>
					<td class="center" colspan="3">Saldo</td>
				</tr>
				<tr>
					<td class="center col-2">Pokok</td>
					<td class="center col-1">Bunga</td>
					<td class="center col-2">Pokok</td>
					<td class="center col-1">Bunga</td>
					<td class="center col-2">Jumlah</td>
				</tr>
				<tr>
					<td class="center">1</td>
					<td>PB</td>
					<td class="center"><?php echo date("d/m/Y", strtotime($key['tanggal_angsuran'])); ?></td>
					<td class="right"><?php echo number_format(0); ?></td>
					<td class="right"><?php echo number_format(0); ?></td>
					<td class="right"><?php echo number_format($mitraPB[0]['angsuran_pokok']); ?></td>
					<td class="right"><?php echo number_format(0); ?></td>
					<td class="right"><?php echo number_format($mitraPB[0]['angsuran_pokok']); ?></td>
				</tr>
				<?php
					//$angsuran_pokok = $mitra[0]['angsuran_pokok']*(12*$mitra[0]['tahun']);
					$angsuran_pokok = $mitraPB[0]['angsuran_pokok'];
					//$angsuran_bunga = $mitra[0]['angsuran_bunga']*(12*$mitra[0]['tahun']);
					$angsuran_bunga = $mitraPB[0]['angsuran_bunga'];
				?>
				<?php $i = 2; $aangsuran = 0; $abiaya = 0; 
					foreach ($angsuranPB as $key) { 
						$angsuran_pokok = $angsuran_pokok - $key['jumlah'];
						$angsuran_bunga = 0;
						if($key['status_pelunasan'] == 1){
							$sisa_angsuran = 0;
							$angsuran_bunga = 0;
						} else {
							$sisa_angsuran = $key['sisa_angsuran'];
						}
					?>
				<tr>
					<td class="center"><?php echo $i; ?></td>
					<td><?php echo $key['id_angsuran']; ?></td>
					<td class="center"><?php echo date("d/m/Y", strtotime($key['tanggal_angsuran'])); ?></td>
					<td class="right">-<?php echo number_format($key['jumlah']); ?></td>
					<td class="right"><?php echo number_format(0); ?></td>
					<td class="right"><?php echo number_format($angsuran_pokok); ?></td>
					<td class="right"><?php echo number_format(0); ?></td>
					<td class="right"><?php echo number_format($sisa_angsuran); ?></td>
				</tr>
				<?php $i++; 
					$aangsuran = $aangsuran + $key['jumlah']; 
					$abiaya = $abiaya + 0;
				} ?>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			&nbsp;
		</td>
	</tr>
	<tr>
		<td class="col-6">
			Catatan :
		</td>
		<td class="col-6 center">
			Divisi Commmunity Development
		</td>
	</tr>
	<tr>
		<td class="col-6">
			<table>
				<tr>
					<td>Akumulasi Angsuran Pokok</td>
					<td>: Rp</td>
					<td>&nbsp;</td>
					<td class="right"><?php echo number_format($aangsuran); ?></td>
				</tr>
				<tr>
					<td>Akumulasi Angsuran Bunga</td>
					<td>: Rp</td>
					<td>&nbsp;</td>
					<td class="right"><?php echo number_format($abiaya); ?></td>
				</tr>
				<tr>
					<td><b>Jumlah</b></td>
					<td><b>: Rp</b></td>
					<td>&nbsp;</td>
					<td class="right"><b><?php echo number_format($aangsuran+$abiaya); ?></b></td>
				</tr>
			</table>
		</td>
		<td class="col-6">
			&nbsp;
		</td>
	</tr>
	<tr>
		<td class="col-6">
			&nbsp;
		</td>
		<td class="col-6 center">
			<?php echo $pegawai[0]['nama_pegawai']; ?>
		</td>
	</tr>
	<tr>
		<td class="col-6">
			Dicetak <?php echo date('d M Y'); ?>
		</td>
		<td class="col-6 center">
			Manager
		</td>
	</tr>
</table>
<?php } ?>
<?php
function Terbilang($x)
{
  $abil = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
  if ($x < 12)
    return " " . $abil[$x];
  elseif ($x < 20)
    return Terbilang($x - 10) . "belas";
  elseif ($x < 100)
    return Terbilang($x / 10) . " puluh" . Terbilang($x % 10);
  elseif ($x < 200)
    return " seratus" . Terbilang($x - 100);
  elseif ($x < 1000)
    return Terbilang($x / 100) . " ratus" . Terbilang($x % 100);
  elseif ($x < 2000)
    return " seribu" . Terbilang($x - 1000);
  elseif ($x < 1000000)
    return Terbilang($x / 1000) . " ribu" . Terbilang($x % 1000);
  elseif ($x < 1000000000)
    return Terbilang($x / 1000000) . " juta" . Terbilang($x % 1000000);
}

function tanggal_indo($tanggal)
{
	$bulan = array (1 =>   'Januari',
				'Februari',
				'Maret',
				'April',
				'Mei',
				'Juni',
				'Juli',
				'Agustus',
				'September',
				'Oktober',
				'November',
				'Desember'
			);
	$split = explode('-', $tanggal);
	return $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];
}
function bulan_indo($num)
{
	$bulan = array (1 =>   'Januari',
				'Februari',
				'Maret',
				'April',
				'Mei',
				'Juni',
				'Juli',
				'Agustus',
				'September',
				'Oktober',
				'November',
				'Desember'
			);
	//$split = explode('-', $tanggal);
	//return $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];
	return $bulan[$num];
}
?>