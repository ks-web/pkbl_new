<!DOCTYPE html>
<html>
	<head>
		<style>
			table {
				border-spacing: 0px;
				border-collapse: separate;
				font-size: 14px;
				page-break-inside: avoid;
			}

			table, th, td {
				border: 0px solid black;
			}
			
			.bordered, .bordered td {
				border: 1px solid black;
			}
			html {
				margin: 10px 50px
			}
			.pull-right {
				text-align: right;
			}
			.center {
				text-align: center;
			}
		</style>
	</head>
	<body>
		<table style="width: 100%;">
			<thead>
				<tr>
					<td class="center" style="width: 150px; font-size: 10px;">
						PT. Krakatau Steel <br /> Divisi Community Development
					</td>
					<td class="center">
						<h4>Usulan Perpanjangan Kontrak Usaha Kecil<br />
						No.: <?=$no_pkbl ?></h4>
					</td>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td colspan="2">
					<table style="width: 100%;" class="bordered">
						<thead>
							<tr style="font-weight: bold;">
								<td width="30px" class="center">No.</td>
								<td width="80px" class="center">No. Kontrak</td>
								<td width="120px" class="center">Nama Perusahaan</td>
								<td class="center">Alamat Perusahaan</td>
								<td width="80px" class="center">Kelurahan</td>
								<td width="80px" class="center">Kecamatan</td>
								<td width="80px" class="center">Kota</td>
								<td width="80px" class="center">Pinjaman</td>
								<td width="80px" class="center">Bayar</td>
								<td width="80px" class="center">Sisa Utang</td>
								<td width="80px" class="center">Keterangan</td>
							</tr>
						</thead>
						<tbody>
							<?php
							$no = 1;
							$tot_pinj = 0;
							$tot_baya = 0;
							$tot_sisa = 0;
							if($data) {
								foreach ($data as $row) {
									$tot_pinj += $row['nilai_pinjaman'];
									$tot_baya += $row['nilai_angsuran'];
									$tot_sisa += $row['sisa_angsuran'];
								?>
							<tr>
								<td width="30px" class="center"><?=$no?></td>
								<td width="80px"><?=$row['no_kontrak']?></td>
								<td width="120px"><?=$row['nama_perusahaan']?></td>
								<td><?=$row['alamat']?></td>
								<td width="80px" ><?=$row['kelurahan']?></td>
								<td width="80px" ><?=$row['kecamatan']?></td>
								<td width="80px" ><?=$row['kota']?></td>
								<td width="80px" class="pull-right"><?=number_format($row['nilai_pinjaman'], 0,",",".")?></td>
								<td width="80px" class="pull-right"><?=number_format($row['nilai_angsuran'], 0,",",".")?></td>
								<td width="80px" class="pull-right"><?=number_format($row['sisa_angsuran'], 0,",",".")?></td>
								<td width="80px"><?=$row['catatan_res']?></td>
							</tr>
								<?php
									$no++;
								}
							} else {
								?>
							<tr>
								<td width="30px" class="center"><br /></td>
								<td width="80px"></td>
								<td width="120px"></td>
								<td></td>
								<td width="80px" ></td>
								<td width="80px" ></td>
								<td width="80px" ></td>
								<td width="80px" class="pull-right"></td>
								<td width="80px" class="pull-right"></td>
								<td width="80px" class="pull-right"></td>
								<td width="80px"></td>
							</tr>
								<?php
							}
							?>
						</tbody>
						<tfoot>
							<tr>
								<td colspan="7" class="center"><strong>TOTAL</strong></td>
								<td width="80px" class="pull-right"><strong><?=number_format($tot_pinj, 0,",",".")?></strong></td>
								<td width="80px" class="pull-right"><strong><?=number_format($tot_baya, 0,",",".")?></strong></td>
								<td width="80px" class="pull-right"><strong><?=number_format($tot_sisa, 0,",",".")?></strong></td>
								<td width="80px"></td>
							</tr>
						</tfoot>
					</table>
					<!-- <hr  style="border: 1px solid black;"/> -->
					<br />
					<table style="width: 100%;">
						<tbody>
							<tr>
								<td width="300px" class="center"><strong></strong></td>
								<td><strong></strong></td>
								<td width="300px" class="center"><strong><?=$kota ?>, <?=$tanggal ?></strong></td>
							</tr>
							<tr>
								<td width="300px" class="center">Menyetujui,</td>
								<td><strong></strong></td>
								<td width="300px" class="center">Disiapkan Oleh,</td>
							</tr>
							<tr>
								<td width="300px" class="center">Divisi Community Development</td>
								<td><strong></strong></td>
								<td width="300px" class="center">a.n Divisi Community Development</td>
							</tr>
							<tr>
								<td width="300px" height="50px" class="center">TTD</td>
								<td><strong></strong></td>
								<td width="300px" height="50px" class="center">TTD</td>
							</tr>
							<tr>
								<td width="300px" class="center"><strong><u>Nama Lengkap</u></strong></td>
								<td><strong></strong></td>
								<td width="300px" class="center"><strong><u>Nama Lengkap</u></strong></td>
							</tr>
							<tr>
								<td width="300px" class="center">Manager</td>
								<td><strong></strong></td>
								<td width="300px" class="center">Supervisor</td>
							</tr>
						</tbody>
					</table></td>
				</tr>
			</tbody>
			<tfoot>
				<tr>
					<td colspan="2"></td>
				</tr>
			</tfoot>
		</table>
	</body>
</html>
