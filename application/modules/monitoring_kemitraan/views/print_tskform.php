<style>
table {
	font-family: 'sans-serif'
}
.bold {
	font-weight: bold;
}
.fontred {
	color: white;
}
.font10 {
	font-size: 10px;
}
.font11 {
	font-size: 11px;
}
.font12 {
	font-size: 12px;
}
.font13 {
	font-size: 13px;
}
</style>

<table border="1" width="70%">
	<tr>
		<td colspan="5"><img align="left" src="<?php echo base_url(); ?>assets/images/logo2.png">PT KRAKATAU STEEL (PERSERO) Tbk.<br>DINAS COMMUNITY DEVELOPMENT FINANCE<br>Telepon (0254) 372326, 372298</td>
    <?php 
    
    $a= count($angsuran)+1;
    
    ?>
        <td align="right">PENYETOR<br>No.: G <?php echo $datakosong[0]['id_mitra']." CR-".$a;?></td>
	</tr>
	<tr>
		<td colspan="6" align="center" class="bold">TANDA SETOR KAS (TSK) PKBL</td>
	</tr>
	<tr>
		<td>TERIMA DARI</td>
		<td colspan="5">: <?php echo $datakosong[0]['nama']; ?></td>
	</tr>
	<tr>
		<td>JUMLAH</td>
		<td colspan="5">: Rp</td>
	</tr>
	<tr>
		<td>TERBILANG</td>
		<td colspan="5">: </td>
	</tr>
	<tr>
		<td></td>
		<td colspan="5">  ..........................................</td>
	</tr>
	<tr>
		<td colspan="6">
			<table border="1" width="100%">
				<tr>
					<td>NO</td>
					<td>NOMOR KONTRAK</td>
					<td>KETERANGAN</td>
					<td>JUMLAH (Rp)</td>
				</tr>
				<tr>
					<td colspan="4">&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td></td>
					<td>Pembayaran Angsuran</td>
					<td></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>Pinjaman Modal Usaha</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td colspan="3">TOTAL</td>
					<td></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="6" align="right">Cilegon, Tgl. &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2" align="center">PENERIMA</td>
		<td colspan="2" rowspan="3">
			<table border="1" width="100%" style="background-color:#FFFFFF;">
				<tr align="center" class="font12 fontred">
					<td>PENYETORAN DAPAT MELALUI REKENING :<br>
					Bank Mandiri Cilegon No. 116-0091023789<br>
					Bank BRI Cilegon No. 0188-01-000095-306<br>
					atas nama PEGEL KOP PTKS</td>
				</tr>
				<tr align="center" class="font11 fontred">
					<td>Pembayaran dengan CEK/BG<br>
					dapat dianggap sah setelah uang diterima.</td>
				</tr>
			</table>
		</td>
		<td colspan="2" align="center">PENYETOR</td>
	</tr>
	<tr>
		<td colspan="2" align="center">________________</td>
		<td colspan="2" align="center">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2"></td>
		<td colspan="2" align="center" class="font10">(Tanda Tangan & Nama Lengkap)</td>
	</tr>
</table>
<?php
function Terbilang($x)
{
  $abil = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
  if ($x < 12)
    return " " . $abil[$x];
  elseif ($x < 20)
    return Terbilang($x - 10) . "belas";
  elseif ($x < 100)
    return Terbilang($x / 10) . " puluh" . Terbilang($x % 10);
  elseif ($x < 200)
    return " seratus" . Terbilang($x - 100);
  elseif ($x < 1000)
    return Terbilang($x / 100) . " ratus" . Terbilang($x % 100);
  elseif ($x < 2000)
    return " seribu" . Terbilang($x - 1000);
  elseif ($x < 1000000)
    return Terbilang($x / 1000) . " ribu" . Terbilang($x % 1000);
  elseif ($x < 1000000000)
    return Terbilang($x / 1000000) . " juta" . Terbilang($x % 1000000);
}

function tanggal_indo($tanggal)
{
	$bulan = array (1 =>   'Januari',
				'Februari',
				'Maret',
				'April',
				'Mei',
				'Juni',
				'Juli',
				'Agustus',
				'September',
				'Oktober',
				'November',
				'Desember'
			);
	$split = explode('-', $tanggal);
	return $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];
}
?>
