<?php
$app = 'Rescheduling Reconditioning'; // nama aplikasi
$module = 'monitoring_kemitraan';
$appLink = 'rescheduling_recon'; // controller
$idField  = 'id_persetujuan'; //field key table
?>

<script>
	var url;
	var app = "<?=$app?>";
	var appLink = '<?=$appLink?>';
	var module = '<?=$module?>';
	var idField = '<?=$idField?>';
	
    function add_comment(idx){
        $('#dlg').dialog('open').dialog('setTitle','Edit ' + app);
        row = $('#dg').datagrid('getRows')[idx];
		$('#fm').form('load', row);
	    $('#tbl').html('Save');
		url = '<?=base_url($module . '/' . $appLink . '/update')?>/' + row.id_persetujuan;
    }
    
    function kontrak(idx){
        row = $('#dg').datagrid('getRows')[idx];
		window.open('<?=base_url($module . '/' . $appLink . '/create_kontrak')?>/'+row.id_persetujuan, '_blank');
    }
    
    function prev_usulan(){
    	var rows = $('#dg').datagrid('getSelections');
    	if(rows.length > 0){
    		var ids_selected= [];
	    	rows.forEach(function(entry){
	    		ids_selected.push (entry.id_persetujuan);
	    	});
	    	
	    	var ids_selected_str = ids_selected.join('_');
	    	
	    	window.open('<?=base_url($module . '/' . $appLink . '/create_prev_usulan')?>/'+ids_selected_str, '_blank');
    	} else {
    		$.messager.alert('Error Message','Tidak ada data yang dipilih','error');
    	}
    }
    function save(){
	   $('#fm').form('submit',{
	    	url: url,
	    	onSubmit: function(){
	    		return $(this).form('validate');
	    	},
	    	success: function(result){
	    		var result = eval('('+result+')');
	    		if (result.success){
	    			$('#dlg').dialog('close');		
	    			$('#dg').datagrid('reload');
	                
	    		} else {
	    			$.messager.alert('Error Message',result.msg,'error');
	    		}
	    	}
	    });
	}
	function doSearch(value){
	    $('#dg').datagrid('load',{    
	    	q:value  
	    });
	}
	
	function aksi_button(value,row,index) {
		return '<button onclick="add_comment('+index+')">'+'<i class="icon-edit"></i>'+'</button>'
		+ '<button onclick="kontrak('+index+')">'+'<i class="icon-download-alt"></i>'+'</button>';
	}
</script>
 
<div class="tabs-container">                
	<table id="dg" class="easyui-datagrid" 
	data-options="  
	    url: '<?=base_url($module . '/' . $appLink . '/read')?>',
	    title:'<?=$app?>',
	    toolbar:'#toolbar',
	    iconCls:'icon-cog',
	    rownumbers:'true',  
	    idField:'<?=$idField?>', 
	    pagination:'true',
	    fitColumns:'true',
	    pageList: [10,20,30]
	    "
	>
	    <thead>
	    	<th data-options="checkbox:true"></th>
	        <th field="id_mitra" width="100" >ID Mitra</th>
	        <th field="no_kontrak" width="100" >No Kontrak</th>
            <th field="nama" width="100" >Nama Pemohon</th>
            <th field="nama_perusahaan" width="100" >Nama Instansi</th>
            <th field="nilai_pinjaman" formatter="format_numberdisp" width="100" align="right">Nilai Pinjaman</th>
            <th field="nilai_angsuran" formatter="format_numberdisp" width="100" align="right">Nilai Angsuran</th>
            <th field="sisa_angsuran" formatter="format_numberdisp" width="100" align="right">Sisa Angsuran</th>
	    </thead>
	</table>
	
	<!-- Model Start -->
	<div id="dlg" class="easyui-dialog" style="width:400px; height:200px; padding:10px" closed="true" buttons="#t_dlg_dis-buttons" >
	    <form id="fm" method="post" enctype="multipart/form-data" action="">
	        <table width="100%" align="center" border="0">
	            <tr>
	                <td>Catatan</td>
	                <td>:</td>
	                <td>
	                    <input class="easyui-textbox" id="catatan_res" name="catatan_res" required type="text" style="width: 100px;">
	                </td>
	            </tr>
	        </table>
	    </form>
	   
	   	<!-- Tombol Add, Edit, Hapus Datagrid -->
       	<div id="toolbar">  
            <table align="center" style="padding: 0px; width: 99%;">
                <tr>
                    <td>
                        <a href="javascript:void(0)" class="btn btn-small btn-success" onclick="javascript:prev_usulan()"><i class="icon-check"></i>&nbsp;Preview Usulan</a>
                    </td>
                    <td>&nbsp;</td>
                    <td align="right">
                    	<input class="easyui-searchbox" prompt="Silahkan Input Field" searcher="doSearch"  style="width:300px"/>
                    </td>
                </tr>
            </table>  
        </div>  
        <!-- end tombol datagrid -->
        <!-- Tombol simpan & Batal Form -->
        <div id="t_dlg_dis-buttons">
            <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:save()"><i class="icon-save icon-large"></i>&nbsp;Simpan</a>
            <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:$('#dlg').dialog('close')"><i class="icon-remove icon-large"></i>&nbsp;Batal</a>      
        </div>
	</div>
	<!-- Model end -->
</div>