<?php
$app1 = 'Monitoring Penagihan Detail'; // nama aplikasi
$module1 = 'monitoring_kemitraan';
$appLink1 = 'monitoring_penagihan_detail'; // controller
$idField1  = 'id_monitoring'; //field key table
?>

<script>
	var url1;
	var app1 = "<?=$app1?>";
	var appLink1 = '<?=$appLink1?>';
	var module1 = '<?=$module1?>';
	var idField1 = '<?=$idField1?>';
	var SisaAngsuran = 0;
	
    function add1(){
        $('#dlg1').dialog('open').dialog('setTitle','Tambah ' + idParent);
		$('#fm1').form('clear');
	    $('#tbl1').html('Save');
		url1 = '<?=base_url($module1 . '/' . $appLink1 . '/create')?>/'+idParent;
		//$('#jumlah').numberbox('disable');
		/*$.post( '<?=base_url($module1 . '/' . $appLink1 . '/getSisaAngsuran')?>/', { id_mitra: idParent })
  			.done(function( data ) {
  			$('#sisa_angsuran').numberbox('setValue', data[0]['sisa_angsuran']);
  			SisaAngsuran = parseInt(data[0]['sisa_angsuran']);
  			$('#jumlah').numberbox('enable');
    		//alert( "Data Loaded: " + data );
  		});*/
    }
    function edit1(){
        var row = $('#dg1').datagrid('getSelected');
		if (row){
			$('#tbl1').html('Simpan');
			$('#dlg1').dialog('open').dialog('setTitle','Edit '+ app1);
			$('#fm1').form('load',row);
			url1 = '<?=base_url($module1 . '/' . $appLink1 . '/update')?>/'+row[idField1];
	    }
    }
    function hapus1(){
        var row = $('#dg1').datagrid('getSelected');		
	    if (row){
			$.messager.confirm('Confirm','Apakah Anda Yakin Akan Menghapus ini?',function(r){
				if (r){
					$.post('<?=base_url($module1 . '/' . $appLink1 . '/delete')?>/'+row[idField1],function(result){
						if (result.success){
							$('#dg1').datagrid('reload');	// reload the user data
						} else {
							$.messager.show({	// show error message
								title: 'Error',
								msg:result.msg
							});
						}
					},'json');
				}
			});
		}
    }
    function save1(){
	   $('#fm1').form('submit',{
	    	url: url1,
	    	onSubmit: function(){
	    		return $(this).form('validate');
	    	},
	    	success: function(result){
	    		var result = eval('('+result+')');
	    		if (result.success){
	    			$('#dlg1').dialog('close');		
	    			$('#dg1').datagrid('reload');
	                
	    		} else {
	             $.messager.alert('Error Message',result.msg,'error');
	    		}
	    	}
	    });
	}
	function doSearch1(value){
	    $('#dg1').datagrid('load',{    
	    	q:value  
	    });
	}
	function reload1(){
		$('#dg1').datagrid({    
	    	url: '<?=base_url($module1 . '/' . $appLink1 . '/read/')?>/'+idParent  
	    });
	}
	/*function print1(){
		var row = $('#dg1').datagrid('getSelected');
		if (row){
			url1 = '<?=base_url($module1 . '/' . $appLink1 . '/printAngsuran')?>/'+row[idField1];
			window.open(url1, '_blank');
			//window.location = url1;
	    }
	}
	function totalSisaOngkir(){
		var total = 0;
		var bayar = $('#fm1 #jumlah').numberbox('getValue');
		total = SisaAngsuran - bayar;
		$('#sisa_angsuran').numberbox('setValue', total);
	}*/
</script>
 
<div class="tabs-container">                
	<table id="dg1" class="easyui-datagrid" 
	data-options="  
	    url: '<?=base_url($module1 . '/' . $appLink1 . '/read')?>',
		singleSelect:'true', 
	    title:'<?=$app1?>',
	    toolbar:'#toolbar1',
	    iconCls:'icon-cog',
	    rownumbers:'true',  
	    idField:'<?=$idField1?>', 
	    pagination:'true',
	    fitColumns:'true',
	    pageList: [10,20,30]
	    "
	>
	    <thead>
	        <th field="id_monitoring" width="100" sortable="true">ID</th>
	        <th field="id_mitra" width="200" sortable="true">ID Mitra</th>
            <th field="tanggal_kunjungan" width="200" sortable="true">Tanggal Kunjungan</th>
            <th field="asset" width="200" sortable="true">Asset</th>
            <th field="omzet" width="200" sortable="true">Omzet</th>
            <th field="tenaga_kerja" width="200" sortable="true">Tenaga Kerja</th>
            <th field="aspek_pengelolaan" width="200" sortable="true">Aspek Pengelolaan</th>
            <th field="aspek_pemasaran" width="200" sortable="true">Aspek Pemasaran</th>
            <th field="aspek_teknis" width="200" sortable="true">Aspek Teknis/Produksi</th>
            <th field="aspek_keuangan" width="200" sortable="true">Aspek Keuangan</th>
            <th field="progres_mitra" width="200" sortable="true">Progres Mitra</th>
            <th field="status_mitra" width="200" sortable="true">Status Mitra</th>
	    </thead>
	</table>
	
	<!-- Model Start -->
	<div id="dlg1" class="easyui-dialog" style="width:500px; height:auto; padding:10px" closed="true" buttons="#t_dlg_dis-buttons1" >
	    <form id="fm1" method="post" enctype="multipart/form-data" action="">
	        <table width="100%" align="center" border="0">
	            <tr>
	                <td>Tanggal Kunjungan</td>
	                <td>:</td>
	                <td>
	                    <input class="easyui-datebox" label="Start Date:" labelPosition="top" name="tanggal_kunjungan" id="tanggal_kunjungan" required style="width: 120px;">
	                </td>
	            </tr>
	            <tr>
	                <td>Asset</td>
	                <td>:</td>
	                <td>
	                    <input type="text" name="asset" id="asset" required style="width: 250px;">
	                </td>
	            </tr>
	            <tr>
	                <td>Omzet</td>
	                <td>:</td>
	                <td>
	                    <input class="easyui-numberbox" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%',onChange: function(){ totalSisaOngkir(); }" name="omzet" id="omzet" required style="width: 250px;">
	                </td>
	            </tr>
	            <tr>
	                <td>Tenaga Kerja</td>
	                <td>:</td>
	                <td>
	                    <input type="text" name="tenaga_kerja" id="tenaga_kerja" required style="width: 250px;">
	                </td>
	            </tr>
	            <tr>
	                <td>Aspek Pengelolaan</td>
	                <td>:</td>
	                <td>
	                    <input type="text" name="aspek_pengelolaan" id="aspek_pengelolaan" required style="width: 250px;">
	                </td>
	            </tr>
	            <tr>
	                <td>Aspek Pemasaran</td>
	                <td>:</td>
	                <td>
	                    <input type="text" name="aspek_pemasaran" id="aspek_pemasaran" required style="width: 250px;">
	                </td>
	            </tr>
	            <tr>
	                <td>Aspek Teknis/Produksi</td>
	                <td>:</td>
	                <td>
	                    <input type="text" name="aspek_teknis" id="aspek_teknis" required style="width: 250px;">
	                </td>
	            </tr>
	            <tr>
	                <td>Aspek Keuangan</td>
	                <td>:</td>
	                <td>
	                    <input type="text" name="aspek_keuangan" id="aspek_keuangan" required style="width: 250px;">
	                </td>
	            </tr>
	            <tr>
	                <td>Progres Mitra</td>
	                <td>:</td>
	                <td>
	                    <select  class="easyui-combobox" type="text" name="progres_mitra" id="progres_mitra" required style="width: 250px;">
	                    	<option value="Sangat Baik">Sangat Baik</option>
	                    	<option value="Baik">Baik</option>
	                    	<option value="Sedang">Sedang</option>
	                    	<option value="Buruk">Buruk</option>
	                    	<option value="Sangat Buruk">Sangat Buruk</option>
	                    </select>
	                </td>
	            </tr>
	            <tr>
	                <td>Status Mitra</td>
	                <td>:</td>
	                <td>
	                    <select  class="easyui-combobox" type="text" name="status_mitra" id="status_mitra" required style="width: 250px;">
	                    	<option value="UK Unggulan">UK Unggulan</option>
	                    	<option value="UK Bermasalah">UK Bermasalah</option>
	                    </select>
	                </td>
	            </tr>
	        </table>
	    </form>
	   
	   	<!-- Tombol Add, Edit, Hapus Datagrid -->
       	<div id="toolbar1">  
            <table align="center" style="padding: 0px; width: 99%;">
                <tr>
                    <td>
                        <a href="javascript:void(0)" class="btn btn-small btn-success" onclick="javascript:add1()"><i class="icon-plus-sign icon-large"></i>&nbsp;Add</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:edit1()"><i class="icon-edit"></i>&nbsp;Edit</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-danger" onclick="javascript:hapus1()"><i class="icon-trash icon-large"></i>&nbsp;Delete</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:print1()"><i class="icon-print"></i>&nbsp;Laporan Monitoring</a>
                    </td>
                    <td>&nbsp;</td>
                    <td align="right">
                    	<input class="easyui-searchbox" prompt="Silahkan Input Field" searcher="doSearch1"  style="width:300px"/>
                    </td>
                </tr>
            </table>  
        </div>  
        <!-- end tombol datagrid -->
        <!-- Tombol simpan & Batal Form -->
        <div id="t_dlg_dis-buttons1">
            <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:save1()"><i class="icon-save icon-large"></i>&nbsp;Simpan</a>
            <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:$('#dlg1').dialog('close')"><i class="icon-remove icon-large"></i>&nbsp;Batal</a>      
        </div>
	</div>
	<!-- Model end -->
</div>