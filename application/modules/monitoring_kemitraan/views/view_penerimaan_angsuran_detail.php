<?php
$app1 = 'Penerimaan Angsuran Detail'; // nama aplikasi
$module1 = 'monitoring_kemitraan';
$appLink1 = 'penerimaan_angsuran_detail'; // controller
$idField1  = 'id_angsuran'; //field key table
?>

<script>
	var url1;
	var app1 = "<?=$app1?>";
	var appLink1 = '<?=$appLink1?>';
	var module1 = '<?=$module1?>';
	var idField1 = '<?=$idField1?>';
	var SisaAngsuran = 0;
	var gSisaPokok = 0;
	var gSisaBiaya = 0;
	var gSisaBiayaBulan = 0;
	var gAngsuranPokok = 0;
	var gAngsuranBiaya = 0;
	var gEditEvent = 0;
	
    function add1(){
        $('#dlg1').dialog('open').dialog('setTitle','Tambah ' + app1);
		$('#fm1').form('clear');
	    $('#tbl1').html('Save');
		url1 = '<?=base_url($module1 . '/' . $appLink1 . '/create')?>/'+idParent;
		$('#jumlah').numberbox('disable');
		gEditEvent = 0;
		$.post( '<?=base_url($module1 . '/' . $appLink1 . '/getSisaAngsuran')?>/', { id_mitra: idParent })
  			.done(function( data ) {
  			$(".kasir").hide();
			$(".rekening").hide();
  			//$('#sisa_angsuran').numberbox('setValue', data[0]['sisa_angsuran']);
  			//SisaAngsuran = parseInt(data[0]['sisa_angsuran']);
  			$('#jumlah').numberbox('enable');
    		//alert( "Data Loaded: " + data );
    		console.log(data);

    		var pokok = data[0]['angsuran'];
			var biaya = data[0]['bunga'];
			
			var tahun = data[0]['tahun'];
			$('#fm1 #tahun').val(tahun);
			
			
			var total = data[0]['total_pinjaman_bunga'];
    		var angsuran_bayar = data[0]['angsuran_bayar'];
    		if(angsuran_bayar == null){
    			angsuran_bayar = 0;
    		}
    		var biaya_bayar = data[0]['biaya_bayar'];
    		if(biaya_bayar == null){
    			biaya_bayar = 0;
    		}

    		var sisa_pokok = parseInt(pokok)-parseInt(angsuran_bayar);
    		var sisa_biaya = parseInt(biaya)-parseInt(biaya_bayar);
    		total = total - (parseInt(angsuran_bayar)+parseInt(biaya_bayar));

    		$('#fm1 #sisa_pokok').numberbox('setValue', sisa_pokok);
    		$('#fm1 #sisa_biaya').numberbox('setValue', sisa_biaya);
    		$('#fm1 #sisa_angsuran').numberbox('setValue', total);

    		gSisaPokok = sisa_pokok;
    		gSisaBiaya = sisa_biaya;
    		gSisaBiayaBulan = parseInt(data[0]['angsuran_bunga']);
  
    		gAngsuranPokok = parseInt(data[0]['angsuran_pokok']);
    		gAngsuranBiaya = parseInt(data[0]['angsuran_bunga']);
    		$('#fm1 #pokok_perbulan').numberbox('setValue', gAngsuranPokok);
    		$('#fm1 #biaya_perbulan').numberbox('setValue', gAngsuranBiaya);
    		$('#fm1 #angsuran_perbulan').numberbox('setValue', gAngsuranPokok+gAngsuranBiaya);
    		if(data[0]['angsuran_ke'] == null){
    			$("#fm1 #angsuran_ke").numberbox('setValue', 1);
    		} else {
	    		$("#fm1 #angsuran_ke").numberbox('setValue', data[0]['angsuran_ke']);	
    		}

    		if(gSisaPokok == 0 && gSisaBiaya <= 10000){
    			alert('Maaf Angsuran Sudah Lunas');
    			$('#dlg1').dialog('close');
    		}

    		if(data[0]['status_angsuran'] == ''){
    			$('#fm1 #status_angsuran').text('Normal');
    		} else {
    			$('#fm1 #status_angsuran').text(data[0]['status_angsuran']);
    		}
  		});
    }
    function edit1(){
        var row = $('#dg1').datagrid('getSelected');
		if (row){
			if(row.status_posted == 0){
				$('#tbl1').html('Simpan');
				$('#dlg1').dialog('open').dialog('setTitle','Edit '+ app1);
				gEditEvent = 1;
				//$('#dlg1').dialog('width', '800');
				$('#fm1').form('load',row);
				url1 = '<?=base_url($module1 . '/' . $appLink1 . '/update')?>/'+row[idField1];
				$(".kasir").hide();
				$(".rekening").hide();
				//console.log(row);
				onSelectPembayaran(row.id_pembayaran);
			} else {
				alert('Data tidak dapat diedit, karena sudah di posting');
			}
	    }
    }
    function hapus1(){
        var row = $('#dg1').datagrid('getSelected');		
	    if (row){
	    	if(row.status_posted == 0){
				$.messager.confirm('Confirm','Apakah Anda Yakin Akan Menghapus ini?',function(r){
					if (r){
						$.post('<?=base_url($module1 . '/' . $appLink1 . '/delete')?>/'+row[idField1],function(result){
							if (result.success){
								$('#dg1').datagrid('reload');	// reload the user data
							} else {
								$.messager.show({	// show error message
									title: 'Error',
									msg:result.msg
								});
							}
						},'json');
					}
				});
			} else {
				alert('Data tidak dapat diedit, karena sudah di posting');
			}
		}
    }
    function save1(){
	   $('#fm1').form('submit',{
	    	url: url1,
	    	onSubmit: function(){
	    		return $(this).form('validate');
	    	},
	    	success: function(result){
	    		var result = eval('('+result+')');
	    		if (result.success){
	    			$('#dlg1').dialog('close');		
	    			$('#dg1').datagrid('reload');
	                
	    		} else {
	             $.messager.alert('Error Message',result.msg,'error');
	    		}
	    	}
	    });
	}
	function doSearch1(value){
	    $('#dg1').datagrid('load',{    
	    	q:value  
	    });
	}
	function edit1Account(id){
		//var row = $('#dg1').datagrid('getSelected');
		//console.log(row);
		//if (row){
			//$('#tbl').html('Simpan');
			$('#dlgx').dialog('open').dialog('setTitle','Edit '+ app);
			//idParent = row.id_angsuran;
			idParent2 = id;
			reload2();
	    //}
	}
	function reload1(){
		$('#dg1').datagrid({    
	    	url: '<?=base_url($module1 . '/' . $appLink1 . '/read/')?>/'+idParent  
	    });
	}
	function print1(){
		var row = $('#dg1').datagrid('getSelected');
		if (row){
			url1 = '<?=base_url($module1 . '/' . $appLink1 . '/printAngsuran')?>/'+row[idField1];
			window.open(url1, '_blank');
			//window.location = url1;
	    }
	}
	function totalSisaAngsuran(){
		var total = 0;
		var persen = 3;
		var jumlah_biaya = 0;
		var jumlah_bayar = $('#fm1 #jumlah_bayar').numberbox('getValue');
		var sisa_biaya = $('#fm1 #sisa_biaya').numberbox('getValue');
		var tahun = $('#fm1 #tahun').val();
		if(jumlah_bayar == ''){
			jumlah_bayar = 0;
		}

		if (tahun>=2) {
			persen=6;
		}
		//var jumlah_biaya = parseInt(jumlah_bayar)-parseInt(gAngsuranBiaya);
		//var jumlah_biaya = parseInt(jumlah_bayar)-parseInt(gSisaBiaya);
		// var jumlah_biaya = parseInt(jumlah_bayar)-parseInt(gSisaBiayaBulan);
		//jumlah_biaya = parseInt(jumlah_bayar) - (parseInt(jumlah_bayar)* (persen/100));
		//sisa_biaya_bayar = (parseInt(jumlah_bayar)* (persen/100));

		jumlah_biaya =  (parseInt(jumlah_bayar)* (100/(100+persen)));
		sisa_biaya_bayar = (parseInt(jumlah_bayar)* (persen/(100+persen)));

		if(jumlah_biaya== 0 ){
			$('#fm1 #jumlah').numberbox('setValue', 0);
			$('#fm1 #biaya').numberbox('setValue', jumlah_bayar);
		} else if(sisa_biaya!=0 && jumlah_biaya!=0){
			$('#fm1 #jumlah').numberbox('setValue', jumlah_biaya);
			$('#fm1 #biaya').numberbox('setValue', sisa_biaya_bayar);
		}else if (sisa_biaya==0 && jumlah_biaya!=0){
			$('#fm1 #jumlah').numberbox('setValue', jumlah_bayar);
			$('#fm1 #biaya').numberbox('setValue', 0);
		}

		var bayar = $('#fm1 #jumlah').numberbox('getValue');
		if(bayar == ''){
			bayar = 0;
		}
		var biaya = $('#fm1 #biaya').numberbox('getValue');
		if(biaya == ''){
			biaya = 0;
		}

		$('#fm1 #sisa_pokok').numberbox('setValue', (gSisaPokok - bayar));
		$('#fm1 #sisa_biaya').numberbox('setValue', (gSisaBiaya - biaya));
		$('#fm1 #sisa_angsuran').numberbox('setValue', (gSisaPokok+gSisaBiaya-(parseInt(bayar)+parseInt(biaya))));
		$('#fm1 #total_bayar').numberbox('setValue', (parseInt(bayar)+parseInt(biaya)));

		//var sisa_pokok = parseInt($('#fm1 #sisa_pokok').numberbox('getValue'));
		//var sisa_biaya = parseInt($('#fm1 #sisa_biaya').numberbox('getValue'));

		//var bayar_biaya = parseInt(bayar) + parseInt(biaya);
		//alert(bayar_biaya);
		//total = SisaAngsuran - bayar_biaya;
		//$('#sisa_angsuran').numberbox('setValue', total);
	}
	function onSelectPembayaran(id_pembayaran){
		if(id_pembayaran == 1 || id_pembayaran == 4){ //kasir
			//alert("1");
			$(".kasir").show();
			$(".rekening").hide();
			$("#no_rek").val('');
			$("#nama_rek").val('');
		} else if(id_pembayaran == 2 || id_pembayaran == 3){ //transfer
			//alert("2");
			$(".kasir").hide();
			$(".rekening").show();
			$('#fm1 #nip').combogrid('setValue', '');
		} else {
			alert("Silahkan Pilih Jenis Pembayaran")
		}
	}
	function accountlist(val, row){
		var result = '<a href="javascript:void(0)" title="Daftar Akun" class="btn btn-small btn-default" onclick="javascript:edit1Account(\''+row.id_angsuran+'\')"><i class="icon-tasks icon-large"></i></a> ';

		var icon = '';
		var color = '';
		if(val == 0){
			icon = 'remove';
			color = 'red';
		} else {
			icon = 'ok';
			color = 'green';
		}
		var result2 = ' <a href="javascript:void(0)" title="Status Posting" style="color:'+color+'" class="btn btn-small btn-default"><i class="icon-'+icon+' icon-large"></i></a>';

		return result+result2;
	}
	function statusposted(val, row){
		var icon = '';
		var color = '';
		if(val == 0){
			icon = 'remove';
			color = 'red';
		} else {
			icon = 'ok';
			color = 'green';
		}
		return '<a href="javascript:void(0)" title="Status Posting" style="color:'+color+'" class="btn btn-small btn-default"><i class="icon-'+icon+' icon-large"></i></a>'; 
		
	}
	function onSelectTglBayar(tgl){
		var tglBayar = tgl.getFullYear()+"-"+(tgl.getMonth()+1)+"-"+tgl.getDate();
		//alert(tglBayar);
		$.post( '<?=base_url($module1 . '/' . $appLink1 . '/getJatuhTempo')?>/', { id_mitra: idParent, tgl_bayar: tglBayar })
  			.done(function( data ) {
  				//console.log(data);
  				if(gEditEvent == 0){
  					$('#fm1 #jumlah_bayar').numberbox('setValue', 0);
  				}
  				gSisaBiayaBulan = (gAngsuranBiaya*parseInt(data[0]['jumlahBayar']));
  				//alert(gSisaBiayaBulan);
  				$("#id_jatuh_tempo").val(data[0]['idJatuhTempo']);
  				$("#status").val(data[0]['statusBayar']);
  		});
	}
</script>
 <style>
 .numberbox-currency {
 	text-align: right;
 }
 </style>


<div class="tabs-container">                
	<table id="dg1" class="easyui-datagrid" 
	data-options="
		singleSelect:'true', 
	    title:'<?=$app1?>',
	    toolbar:'#toolbar1',
	    iconCls:'icon-cog',
	    rownumbers:'true',  
	    idField:'<?=$idField1?>', 
	    pagination:'true',
	    fitColumns:'true',
	    pageList: [10,20,30]
	    "
	>
	    <thead>
	        <th field="id_angsuran" width="100" sortable="true">ID</th>
			<th field="tgl_jatuh_tempo" width="100" sortable="true">Jatuh Tempo</th>
	        <th field="tanggal_angsuran" width="100" sortable="true">Tgl<br>Angsuran</th>
			<th field="status_pembayaran" width="50" sortable="true">Status</th>
	        <th field="pembayaran" width="100" sortable="true">Jenis<br>Pembayaran</th>
            <th field="angsuran_ke" width="50" sortable="true">Angsuran<br>Ke</th>
            <!--<th field="nama" width="200" sortable="true">Nama Pemohon</th>
            <th field="no_ktp" width="200" sortable="true">KTP Pemohon</th>
            <th field="nama_perusahaan" width="200" sortable="true">Nama Instansi</th>
            <th field="nilai_disetujui" width="200" sortable="true">Nilai Pengajuan</th>-->
            <th field="total" width="100" sortable="true" formatter="format_numberdisp">Jumlah<br>Pembayaran</th>
            <th field="sisa_angsuran" width="100" sortable="true" formatter="format_numberdisp">Sisa<br>Hutang</th>
         	<th field="tgl_posting" width="100" sortable="true">Tgl<br>Posting</th>
            <th field="status_posted" width="100" sortable="true" formatter="accountlist">Action</th>
            <!--<th field="status_posted" width="100" sortable="true" formatter="statusposted">Posting</th>-->
	    </thead>
	</table>
	
	<!-- Model Start -->
	
	<div id="dlg1" class="easyui-dialog" style="width:800px; height:auto; padding:10px" closed="true" buttons="#t_dlg_dis-buttons1" >
	    <form id="fm1" method="post" enctype="multipart/form-data" action="">
	        <table width="100%" align="center" border="0">
	            <tr>
	                <td>Jenis Pembayaran</td>
	                <td>:</td>
	                <td>
	                	<input type="hidden" id="status" name="status" value="1">
	                	<input type="hidden" id="id_jatuh_tempo" name="id_jatuh_tempo" value="0">
	                    <input name="id_pembayaran" id="id_pembayaran"  class="easyui-combogrid" required style="width: 200px"
						data-options="panelHeight:200, panelWidth: 250,
						valueField:'',
						idField:'id_pembayaran',
						textField:'pembayaran',
						mode:'remote',
						url:'<?=base_url($module1 . '/' . $appLink1 . '/getpembayaran') ?>',
						onSelect :function(indek,row)
						{ onSelectPembayaran(row.id_pembayaran); },
						columns:[[
                       		{field:'id_pembayaran',title:'ID Pembayaran',width:80},
                        	{field:'pembayaran',title:'Pembayaran',width:150}
                        ]]

						" >
	                </td>
	            </tr>
	            <tr class="kasir">
	                <td>Pegawai</td>
	                <td>:</td>
	                <td>
	                    <input name="nip" id="nip"  class="easyui-combogrid" style="width: 200px"
						data-options="panelHeight:200, panelWidth: 250,
						valueField:'',
						idField:'nip',
						textField:'nama_pegawai',
						mode:'remote',
						url:'<?=base_url($module1 . '/' . $appLink1 . '/getpegawai') ?>',
						onSelect :function(indek,row)
						{},
						columns:[[
                       		{field:'idPegawai',title:'NIP',width:80},
                        	{field:'nama_pegawai',title:'Nama',width:150}
                        ]]

						" >
	                </td>
	            </tr>
	            <tr class="rekening">
	               <td>No Rekening</td>
	               <td>:</td>
	               <td>
	               <input type="text" name="no_rek" id="no_rek" style="width: 150px;">
	               </td>
	            </tr>
	            <tr class="rekening">
	               <td>Atas Nama</td>
	               <td>:</td>
	               <td>
	               <input type="text" name="nama_rek" id="nama_rek" style="width: 150px;">
	               </td>
	            </tr>
	            <tr>
	                <td>Tgl Bayar</td>
	                <td>:</td>
	                <td>
	                    <input class="easyui-datebox" label="Start Date:" labelPosition="top" name="tanggal_angsuran" id="tanggal_angsuran" required style="width: 120px;"
							data-options="
							onSelect:function(date){
								onSelectTglBayar(date);
							}
						">
	                </td>
	            </tr>
				<tr>
	                <td>Angsuran ke</td>
	                <td>:</td>
	                <td>
	                    <input class="easyui-numberbox" data-options="precision:0,groupSeparator:'.',decimalSeparator:',',width:'100%'" name="angsuran_ke" id="angsuran_ke" required style="width: 150px;" >
	                </td>
	                <td align="right">Status Angsuran : <b id="status_angsuran">Normal</b>
					<input type="hidden" id="tahun" name="tahun" >
					</td>
	            </tr>
	            <tr>
	                <td>Jumlah yang Dibayarkan</td>
	                <td>:</td>
	                <td>
	                    <input class="easyui-numberbox numberbox-currency" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%',onChange: function(){ totalSisaAngsuran(); }" name="jumlah_bayar" id="jumlah_bayar" required style="width: 150px;">
	                </td>
	                <td align="right">Pokok / Bulan <input class="easyui-numberbox numberbox-currency" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%'" name="pokok_perbulan" id="pokok_perbulan" style="width: 150px;" readonly="readonly"></td>
	            </tr>
	            <tr>
	                <td>Pokok Dibayarkan</td>
	                <td>:</td>
	                <td>
	                    <input class="easyui-numberbox numberbox-currency" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%',onChange: function(){ totalSisaAngsuran(); }" name="jumlah" id="jumlah" required style="width: 150px;" readonly="readonly">
	                </td>
	                <td align="right">Biaya / Bulan <input class="easyui-numberbox numberbox-currency" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%'" name="biaya_perbulan" id="biaya_perbulan" style="width: 150px;" readonly="readonly"></td>
	            </tr>
	            <tr>
	                <td>Biaya Adm Dibayarkan</td>
	                <td>:</td>
	                <td>
	                    <input class="easyui-numberbox numberbox-currency" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%',onChange: function(){ totalSisaAngsuran(); }" name="biaya" id="biaya" required style="width: 150px;" readonly="readonly">
	                </td>
	                <td align="right">Angsuran / Bulan <input class="easyui-numberbox numberbox-currency" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%'" name="angsuran_perbulan" id="angsuran_perbulan" style="width: 150px;" readonly="readonly"></td>
	            </tr>
	            <tr>
	                <td>Sisa Pokok</td>
	                <td>:</td>
	                <td>
	                    <input class="easyui-numberbox numberbox-currency" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%'" name="sisa_pokok" id="sisa_pokok" readonly="readonly" style="width: 150px;">
	                </td>
	            </tr>
	            <tr>
	                <td>Sisa Biaya Adm</td>
	                <td>:</td>
	                <td>
	                    <input class="easyui-numberbox numberbox-currency" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%'" name="sisa_biaya" id="sisa_biaya" readonly="readonly" style="width: 150px;">
	                </td>
	            </tr>
	            <tr>
	                <td>Total Pembayaran</td>
	                <td>:</td>
	                <td>
	                    <input class="easyui-numberbox numberbox-currency" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%'" name="total_bayar" id="total_bayar" readonly="readonly" style="width: 150px;">
	                </td>
	                <td>&nbsp;</td>
	            </tr>
	            
	            <tr>
	                <td>Sisa Total Angsuran</td>
	                <td>:</td>
	                <td>
	                    <input class="easyui-numberbox numberbox-currency" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%'" name="sisa_angsuran" id="sisa_angsuran" required style="width: 150px;" readonly="readonly">
	                </td>
	            </tr>

	            <tr>
	                <td>Keterangan</td>
	                <td>:</td>
	                <td>
	                    <textarea placeholder="Keterangan" name="keterangan" id="keterangan"></textarea>
	                </td>
	            </tr>
	            
	        </table>
	    </form>
	   
	   	<!-- Tombol Add, Edit, Hapus Datagrid -->
       	<div id="toolbar1">  
            <table align="center" style="padding: 0px; width: 99%;">
                <tr>
                    <td>
                        <a href="javascript:void(0)" class="btn btn-small btn-success" onclick="javascript:add1()"><i class="icon-plus-sign icon-large"></i>&nbsp;Add</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:edit1()"><i class="icon-edit"></i>&nbsp;Edit</a>
                        <!--<a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:edit1Account()"><i class="icon-edit"></i>&nbsp;Edit Akun</a>-->
                        <a href="javascript:void(0)" class="btn btn-small btn-danger" onclick="javascript:hapus1()"><i class="icon-trash icon-large"></i>&nbsp;Delete</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:print1()"><i class="icon-print"></i>&nbsp;Print TSK</a>
                    </td>
                    <td>&nbsp;</td>
                    <td align="right">
                    	<input class="easyui-searchbox" prompt="Silahkan Input Field" searcher="doSearch1"  style="width:300px"/>
                    </td>
                </tr>
            </table>  
        </div>  
        <!-- end tombol datagrid -->
        <!-- Tombol simpan & Batal Form -->
        <div id="t_dlg_dis-buttons1">
            <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:save1()"><i class="icon-save icon-large"></i>&nbsp;Simpan</a>
            <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:$('#dlg1').dialog('close')"><i class="icon-remove icon-large"></i>&nbsp;Batal</a>      
        </div>
	</div>
	<!-- Model end -->
</div>
