<!DOCTYPE html>
<html>
	<head>
		<style>
			table {
				border-spacing: 0px;
				border-collapse: separate;
				font-size: 14px;
				page-break-inside: avoid;
			}

			table, th, td {
				border: 0px solid black;
			}
			
			.bordered, .bordered td {
				border: 1px solid black;
			}
			html {
				margin: 50px 100px
			}
			.pull-right {
				text-align: right;
			}
			.center {
				text-align: center;
			}
		</style>
	</head>
	<body>
		<table style="width: 100%;">
			<thead>
				<tr>
					<td></td>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>
						<h3 class="center">
							Perjanjian<br />
							Antara<br />
							PT Krakatau Steel (Persero) Tbk.<br />
							dengan<br />
							Usaha Kecil <?=isset($data['nama']) ? $data['nama'] : null?><br />
							Tentang<br />
							Pinjaman Modal Usaha<br />
							Nomor Kontrak : <?=isset($data['no_kontrak']) ? $data['no_kontrak'] : null?><br />
							Usulan Perpanjangan<br />
							Nomor Usulan Perpanjangan : <?=isset($data['no_kontrak']) ? $data['no_kontrak'] : null?>
						</h3>
					</td>
				</tr>
				<tr>
					<td>
						<?php
							$tgl_arr = explode(' ', tanggal_display($data['tgl_lahir']));
						?>
						<p>
							Perjanjian Modal Usaha ini (selanjutnya disebut Perjanjian) ditandatangani pada hari ini 
							tanggal <?=isset($tgl_arr[0]) ? $tgl_arr[0] : null?>, 
							bulan <?=isset($tgl_arr[1]) ? $tgl_arr[1] : null?>, 
							tahun <?=isset($tgl_arr[2]) ? $tgl_arr[2] : null?> (<?=terbilang_display(isset($tgl_arr[2]) ? $tgl_arr[2] : null)?>)
							di Cilegon, oleh dan antara :
						</p>
						<table style="width: 100%;">
							<tr>
								<td width="10px" style="vertical-align: top;">1.</td>
								<td width="100px" style="vertical-align: top;">PT Krakatau Steel <br />(Persero) Tbk.</td>
								<td width="10px" style="vertical-align: top;">:</td>
								<td>Berkedudukan di Cilegon, di Jalan Industri Nomor 5, Provinsi Banten, sesuai anggaran dasar yang tercantum dalam akta nomor 51 Tanggal 27 April 2015 yang dibuat oleh Jose Dima Satria, S.H., M.Kn.. Notaris di Jakarta, dalam hal ini diwakili oleh <strong>Syarif Rahman</strong>, Manager Community Development berdasarkan Surat Keputusan Direksi PT Krakatau Steel (Persero) Tbk. Nomor  59/C/DU-KS/Kpts/2015 tanggal 29  Mei 2015, dan Pengaturan Kewenangan di Divisi Program Kemitraan & Bina Lingkungan No. 49 A/C/DU- KS/Kpts/2006 tanggal 22 Agustus 2006, yang bertindak mewakili Direksi, dari dan oleh sebab itu bertindak untuk dan atas nama Krakatau Steel (Persero) Tbk. selanjutnya disebut <strong>PIHAK PERTAMA</strong>.</td>
							</tr>
							<tr>
								<td width="10px" style="vertical-align: top;">2.</td>
								<td width="100px" style="vertical-align: top;">Usaha Kecil <br /><?=isset($data['nama']) ? $data['nama'] : null?></td>
								<td width="10px" style="vertical-align: top;">:</td>
								<td>Berkedudukan di <?=isset($data['alamat']) ? $data['alamat'] : null?>, 
									<?=isset($data['kecamatan']) ? $data['kecamatan'] : null?>, 
									<?=isset($data['kota']) ? $data['kota'] : null?>, dalam hal ini diwakili oleh 
									<?=isset($data['nama']) ? $data['nama'] : null?>, Nomor KTP 
									<?=isset($data['no_ktp']) ? $data['no_ktp'] : null?>, Lahir di 
									<?=isset($data['tempat_lahir']) ? $data['tempat_lahir'] : null?> tanggal 
									<?=isset($data['tgl_lahir']) ? tanggal_display($data['tgl_lahir']) : null?>, alamat rumah di 
									<?=isset($data['alamat']) ? $data['alamat'] : null?>, Desa 
									<?=isset($data['kelurahan']) ? $data['kelurahan'] : null?>, Kecamatan 
									<?=isset($data['kecamatan']) ? $data['kecamatan'] : null?>, 
									<?=isset($data['kota']) ? $data['kota'] : null?> dalam hal ini bertindak atas nama Pimpinan Usaha Kecil 
									<?=isset($data['nama']) ? $data['nama'] : null?>, yang bergerak pada 
									<?=isset($data['sektor_usaha']) ? $data['sektor_usaha'] : null?>, dari dan oleh sebab itu bertindak untuk dan atas nama Usaha Kecil 
									<?=isset($data['nama']) ? $data['nama'] : null?>, selanjutnya disebut PIHAK KEDUA.</td>
							</tr>
							<tr>
								<td colspan="4">
									PIHAK PERTAMA dan PIHAK KEDUA selanjutnya disebut Kedua Pihak.Kedua Pihak terlebih dahulu menerangkan :
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</tbody>
			<tfoot>
				<tr>
					<td></td>
				</tr>
			</tfoot>
		</table>
	</body>
</html>
