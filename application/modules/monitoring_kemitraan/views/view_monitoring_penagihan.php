<?php
$app = 'Monitoring Penagihan'; // nama aplikasi
$module = 'monitoring_kemitraan';
$appLink = 'monitoring_penagihan'; // controller
$idField  = 'id_angsuran'; //field key table
?>

<script>
	var url;
	var app = "<?=$app?>";
	var appLink = '<?=$appLink?>';
	var module = '<?=$module?>';
	var idField = '<?=$idField?>';
	var idParent = '';
	
    function add(){
    	var row = $('#dg').datagrid('getSelected');
		if (row){
			$('#dlg').dialog('open').dialog('setTitle','Tambah ' + app + row.id_mitra);
        
       	 	idParent = row.id_mitra;
        	reload1();
	    }
    }
	function doSearch(value){
	    $('#dg').datagrid('load',{    
	    	q:value  
	    });
	}
</script>
 
<div class="tabs-container">                
	<table id="dg" class="easyui-datagrid" 
	data-options="  
	    url: '<?=base_url($module . '/' . $appLink . '/read')?>',
		singleSelect:'true', 
	    title:'<?=$app?>',
	    toolbar:'#toolbar',
	    iconCls:'icon-cog',
	    rownumbers:'true',  
	    idField:'<?=$idField?>', 
	    pagination:'true',
	    fitColumns:'true',
	    pageList: [10,20,30]
	    "
	>
	    <thead>
	        <th field="id_mitra" width="200" sortable="true">ID Mitra</th>
	        <th field="tanggal_input" width="200" sortable="true">No Kontrak</th>
            <th field="tanggal_kunjungan" width="200" sortable="true">Tgl Kunjungan</th>
            <th field="nama" width="200" sortable="true">Nama Pemohon</th>
            <th field="nama_perusahaan" width="200" sortable="true">Nama Instansi</th>
            <th field="alamat" width="200" sortable="true">Alamat</th>
            <th field="kota" width="200" sortable="true">Kota</th>
            <th field="sektor_usaha" width="200" sortable="true">Sektor Usaha</th>
            <th field="progres_mitra" width="200" sortable="true">Progres Mitra</th>
	    </thead>
	</table>
	
	<!-- Model Start -->
	<div id="dlg" class="easyui-dialog" style="width:900px; height:auto; padding:10px" closed="true" buttons="#t_dlg_dis-buttons" >
	    
	    <?php $this->load->view('view_monitoring_penagihan_detail'); ?>

	    <!-- Tombol Add, Edit, Hapus Datagrid -->
       	<div id="toolbar">  
            <table align="center" style="padding: 0px; width: 99%;">
                <tr>
                    <td>
                        <a href="javascript:void(0)" class="btn btn-small btn-success" onclick="javascript:add()"><i class="icon-plus-sign icon-large"></i>&nbsp;Add</a>
                    </td>
                    <td>&nbsp;</td>
                    <td align="right">
                    	<input class="easyui-searchbox" prompt="Silahkan Input Field" searcher="doSearch"  style="width:300px"/>
                    </td>
                </tr>
            </table>  
        </div>  
        <!-- end tombol datagrid -->
	   
        <!-- Tombol simpan & Batal Form -->
        <div id="t_dlg_dis-buttons">
            <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:$('#dlg').dialog('close')"><i class="icon-remove icon-large"></i>&nbsp;Batal</a>      
        </div>
	</div>
	<!-- Model end -->

</div>