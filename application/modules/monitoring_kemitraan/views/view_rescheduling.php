<?php
$app = 'Rescheduling'; // nama aplikasi
$module = 'monitoring_kemitraan';
$appLink = 'rescheduling'; // controller
$idField  = 'id_res'; //field key table
?>

<script>
	var url;
	var app = "<?=$app?>";
	var appLink = '<?=$appLink?>';
	var module = '<?=$module?>';
	var idField = '<?=$idField?>';
	
    function add(){
		$('#id_mitra').combogrid('enable');
        $('#dlg').dialog('open').dialog('setTitle','Tambah ' + app);
		$('#fm').form('clear');
	    $('#tbl').html('Save');
		url = '<?=base_url($module . '/' . $appLink . '/create')?>';
    }
    function edit(){
        var row = $('#dg').datagrid('getSelected');
		if (row){
			$('#id_mitra').combogrid('disable');
			$('#tbl').html('Simpan');
			$('#dlg').dialog('open').dialog('setTitle','Edit '+ app);
			$('#fm').form('load',row);
			url = '<?=base_url($module . '/' . $appLink . '/update')?>/'+row[idField];
	    }
    }
    function hapus(){
        var row = $('#dg').datagrid('getSelected');		
	    if (row){
			$.messager.confirm('Confirm','Apakah Anda Yakin Akan Menghapus ini?',function(r){
				if (r){
					$.post('<?=base_url($module . '/' . $appLink . '/delete')?>/'+row[idField],function(result){
						if (result.success){
							$('#dg').datagrid('reload');	// reload the user data
						} else {
							$.messager.show({	// show error message
								title: 'Error',
								msg:result.msg
							});
						}
					},'json');
				}
			});
		}
    }
    function save(){
	   $('#fm').form('submit',{
	    	url: url,
	    	onSubmit: function(){
	    		return $(this).form('validate');
	    	},
	    	success: function(result){
	    		var result = eval('('+result+')');
	    		if (result.success){
	    			$('#dlg').dialog('close');		
	    			$('#dg').datagrid('reload');
	                
	    		} else {
	             $.messager.alert('Error Message',result.msg,'error');
	    		}
	    	}
	    });
	}
	function doSearch(value){
	    $('#dg').datagrid('load',{    
	    	q:value  
	    });
	}

	function repopulate_form(row){
		if(row){
			$('#nama').val(row.nama);
			$('#nama_perusahaan').val(row.nama_perusahaan);
			$('#nilai_dibayar').numberbox('setValue', row.nilai_dibayar);
			$('#sisa_angsuran').numberbox('setValue', row.sisa_angsuran);
		}
	}

	function calculate_nilai_angsuran(val){
		var sisa_angsuran = $('#sisa_angsuran').numberbox('getValue');
		var bulan = val*12;

		var nilai_angsuran = Math.round(sisa_angsuran/bulan);

		$('#nilai_angsuran').numberbox('setValue', nilai_angsuran);
	}

	function action(value,row,index){
		return '<button onclick="kontrak('+index+')">'+'<i class="icon-download-alt"></i>'+'</button>';
	}
	
	function kontrak(idx){
        row = $('#dg').datagrid('getRows')[idx];
		window.open('<?=base_url($module . '/' . $appLink . '/create_kontrak')?>/'+row.id_res, '_blank');
    }
</script>
 
<div class="tabs-container">                
	<table id="dg" class="easyui-datagrid" 
	data-options="  
	    url: '<?=base_url($module . '/' . $appLink . '/read')?>',
		singleSelect:'true', 
	    title:'<?=$app?>',
	    toolbar:'#toolbar',
	    iconCls:'icon-cog',
	    rownumbers:'true',  
	    idField:'<?=$idField?>', 
	    pagination:'true',
	    fitColumns:'true',
	    pageList: [10,20,30]
	    "
	>
	    <thead>
	        <th field="id_res" width="100">No Reschedule</th>
	        <th field="id_mitra" width="100">ID Mitra</th>
            <th field="nama" width="150">Nama<br>Pemohon</th>
            <th field="nama_perusahaan" width="150">Nama<br>Instansi</th>
            <th field="nilai_dibayar" formatter="format_numberdisp" width="100" align="right">Nilai<br>Dibayar</th>
            <th field="nilai_angsuran" formatter="format_numberdisp" width="100" align="right">Nilai<br>Angsuran</th>
            <th field="status_angsuran" width="60" align="center">Status<br>Angsuran</th>
            <th field="action" width="40" formatter="action">Action</th>
	    </thead>
	</table>
	
	<!-- Model Start -->
	<div id="dlg" class="easyui-dialog" style="width:500px; height:auto; padding:10px" closed="true" buttons="#t_dlg_dis-buttons" >
	    <form id="fm" method="post" enctype="multipart/form-data" action="">
	        <table width="100%" align="center" border="0">
	            <tr>
	                <td>No Mitra</td>
	                <td>:</td>
	                <td>
	                    <input id="id_mitra" name="id_mitra" class="easyui-combogrid" style="width:250px;" data-options="
	                    	delay: 500,
    						mode: 'remote',
                            panelWidth: 600,
                            idField: 'id_mitra',
                            textField: 'id_mitra',
                            url: '<?=base_url($module . '/' . $appLink . '/search_no')?>',
                            method: 'get',
                            columns: [[
                                {field:'id_mitra',title:'ID Mitra',width:100},
                                {field:'nama',title:'Nama Pemohon',width:100},
                                {field:'nama_perusahaan',title:'Nama Instansi',width:100},
                                {field:'nilai_dibayar',title:'Nilai Dibayar',width:100,align:'right',formatter:format_numberdisp},
                                {field:'sisa_angsuran',title:'Sisa Angsuran',width:100,align:'right',formatter:format_numberdisp}
                            ]],
                            fitColumns: true,
                            onChange: function(newVal, oldVal){
                            	var g = $(this).combogrid('grid');	// get datagrid object
                                var row = g.datagrid('getSelected');	// get the selected row
                                repopulate_form(row);
                            }
                        ">
	                </td>
	            </tr>
	            <tr>
	                <td>Nama Mitra</td>
	                <td>:</td>
	                <td>
	                    <input type="text" name="nama" id="nama" style="width: 250px;" readonly>
	                </td>
	            </tr>
	            <tr>
	                <td>Nama Instansi</td>
	                <td>:</td>
	                <td>
	                    <input type="text" name="nama_perusahaan" id="nama_perusahaan" style="width: 250px;" readonly>
	                </td>
	            </tr>
	            <tr>
	                <td>Nilai Dibayar</td>
	                <td>:</td>
	                <td>
	                	<input type="text" name="nilai_dibayar" id="nilai_dibayar" class="easyui-numberbox" value="100" data-options="min:0,precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. '" readonly>
	                </td>
	            </tr>
	            <tr>
	                <td>Total Sisa Angsuran</td>
	                <td>:</td>
	                <td>
	                    <input type="text" name="sisa_angsuran" id="sisa_angsuran" class="easyui-numberbox" value="100" data-options="min:0,precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. '" readonly>
	                </td>
	            </tr>
	            <tr>
	                <td colspan="3"><hr></td>
	            </tr>
	            <tr>
	                <td>Status</td>
	                <td>:</td>
	                <td>
	                	<select class="easyui-combobox" name="status_angsuran" id="status_angsuran" data-options="panelHeight: 'auto'" style="width:100%;" >
                        	<option value="P1">P1</option>
                        	<option value="P2">P2</option>
                        	<option value="PB">PB</option>
                        </select>
	                </td>
	            </tr>
	            <tr>
	                <td>Jangka Waktu</td>
	                <td>:</td>
	                <td>
	                    <select class="easyui-combobox" name="tahun_angsuran" id="tahun_angsuran" data-options="panelHeight: 'auto',onChange:function(newVal, oldVal){calculate_nilai_angsuran(newVal);}" style="width:100%;">
                        	<option value="1">1</option>
                        	<option value="2">2</option>
                        </select> Tahun
	                </td>
	            </tr>
	            <tr>
	                <td>Nilai Angsuran Bulanan</td>
	                <td>:</td>
	                <td>
	                	<input type="text" name="nilai_angsuran" id="nilai_angsuran" class="easyui-numberbox" value="100" data-options="min:0,precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. '" readonly>
	                </td>
	            </tr>
	            <tr>
	                <td>Keterangan</td>
	                <td>:</td>
	                <td>
	                    <textarea name="keterangan" id="keterangan"></textarea>
	                </td>
	            </tr>
	        </table>
	    </form>
	   
	   	<!-- Tombol Add, Edit, Hapus Datagrid -->
       	<div id="toolbar">  
            <table align="center" style="padding: 0px; width: 99%;">
                <tr>
                    <td>
                        <a href="javascript:void(0)" class="btn btn-small btn-success" onclick="javascript:add()"><i class="icon-plus-sign icon-large"></i>&nbsp;Add</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:edit()"><i class="icon-edit"></i>&nbsp;Edit</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-danger" onclick="javascript:hapus()"><i class="icon-trash icon-large"></i>&nbsp;Delete</a>
                    </td>
                    <td>&nbsp;</td>
                    <td align="right">
                    	<input class="easyui-searchbox" prompt="Silahkan Input Field" searcher="doSearch"  style="width:300px"/>
                    </td>
                </tr>
            </table>  
        </div>  
        <!-- end tombol datagrid -->
        <!-- Tombol simpan & Batal Form -->
        <div id="t_dlg_dis-buttons">
            <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:save()"><i class="icon-save icon-large"></i>&nbsp;Simpan</a>
            <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:$('#dlg').dialog('close')"><i class="icon-remove icon-large"></i>&nbsp;Batal</a>      
        </div>
	</div>
	<!-- Model end -->
</div>