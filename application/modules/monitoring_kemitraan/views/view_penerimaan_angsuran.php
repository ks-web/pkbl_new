<?php
$app = 'Penerimaan Angsuran'; // nama aplikasi
$module = 'monitoring_kemitraan';
$appLink = 'penerimaan_angsuran'; // controller
$idField  = 'id_angsuran'; //field key table
?>

<script>
	var url;
	var app = "<?=$app?>";
	var appLink = '<?=$appLink?>';
	var module = '<?=$module?>';
	var idField = '<?=$idField?>';
	var idParent = '';
	var tgl_kontrak = '';
	
    function add(){
    	var row = $('#dg').datagrid('getSelected');
		if (row){
			$('#dlg').dialog('open').dialog('setTitle','Tambah ' + app);
        
       	 	idParent = row['id_mitra'];
        	reload1();
	    }
	}
	
	function printformtsk(){
		var row = $('#dg').datagrid('getSelected');
		if (row){
			url1 = '<?=base_url($module . '/' . $appLink . '/printAngsuranForm')?>/'+row['id_mitra'];
			window.open(url1, '_blank');
			//window.location = url1;
	    }
	}
    function preview(){
		var row = $('#dg').datagrid('getSelected');
		if (row){
			$.post( '<?=base_url($module . '/' . $appLink . '/getStatusPosted')?>/', { id_mitra: row['id_mitra'] })
	  			.done(function( data ) {
	  				console.log(data.status);
	  				if(data.status == 0){
	  					url1 = '<?=base_url($module . '/' . $appLink . '/printKartuPiutangAkumulasi')?>/'+row['id_mitra'];
						window.open(url1, '_blank');
	  				} else {
	  					$.messager.alert('Error Message','Terdapat pembayaran penerimaan angsuran yang belum di posting, silahkan cek pada tabel pembayaran','error');
	  					
// 	  					alert();
// 	  					$('#dlg').dialog('open').dialog('setTitle','Tambah ' + app);
        
// 			       	 	idParent = row['id_mitra'];
// 			        	reload1();
	  				}
	  		});
	    }
	}
	function doSearch(value){
	    $('#dg').datagrid('load',{    
	    	q:value  
	    });
	}
	function format_saldo_piutang(value,row,index) {
		var saldo = row.nilai_disetujui-row.nilai_angsuran;
		var disp = format( "###,###,###,##0.00", saldo);
		return disp;
	}
</script>
 
<div class="tabs-container">                
	<table id="dg" class="easyui-datagrid" 
	data-options="  
	    url: '<?=base_url($module . '/' . $appLink . '/read')?>',
		singleSelect:'true', 
	    title:'<?=$app?>',
	    toolbar:'#toolbar',
	    iconCls:'icon-cog',
	    rownumbers:'true',  
	    idField:'<?=$idField?>', 
	    pagination:'true',
	    fitColumns:'true',
	    pageList: [10,20,30]
	    "
	>
	    <thead>
	        <th field="id_mitra" width="200" sortable="true">ID Mitra</th>
	        <th field="tanggal_input" width="200" sortable="true">Tgl Daftar</th>
            <th field="tanggal_disetujui" width="200" sortable="true">Tgl Kontrak</th>
            <th field="nama" width="200" sortable="true">Nama Pemohon</th>
            <th field="nama_perusahaan" width="200" sortable="true">Nama Instansi</th>
            <th field="tgl_jatuh_tempo" width="200" sortable="true">Tgl Akhir<br>Jatuh Tempo</th>
            <th field="nilai_disetujui" width="200" sortable="true" formatter="format_numberdisp">Nilai Penyaluran</th>
            <th field="nilai_angsuran" width="200" sortable="true" formatter="format_numberdisp">Nilai Dibayar</th>
            <th field="saldo_piutang" width="200" sortable="true" formatter="format_saldo_piutang">Nilai Saldo Piutang</th>
	    </thead>
	</table>
	
	<!-- Model Start -->
	<div id="dlg" class="easyui-dialog" style="width:900px; height:auto; padding:10px" closed="true" buttons="#t_dlg_dis-buttons" >
	    
	    <?php $this->load->view('view_penerimaan_angsuran_detail'); ?>

	    <!-- Tombol Add, Edit, Hapus Datagrid -->
       	<div id="toolbar">  
            <table align="center" style="padding: 0px; width: 99%;">
                <tr>
                    <td>
                        <a href="javascript:void(0)" class="btn btn-small btn-success" onclick="javascript:add()"><i class="icon-plus-sign icon-large"></i>&nbsp;Pembayaran</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:preview()"><i class="icon-print icon-large"></i>&nbsp;Kartu Piutang</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:printformtsk()"><i class="icon-print icon-large"></i>&nbsp;Form TSK</a>
                    </td>
                    <td>&nbsp;</td>
                    <td align="right">
                    	<input class="easyui-searchbox" prompt="Silahkan Input Field" searcher="doSearch"  style="width:300px"/>
                    </td>
                </tr>
            </table>  
        </div>  
        <!-- end tombol datagrid -->
	   
        <!-- Tombol simpan & Batal Form -->
        <div id="t_dlg_dis-buttons">
            <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:$('#dlg').dialog('close')"><i class="icon-remove icon-large"></i>&nbsp;Batal</a>      
        </div>
	</div>
	<!-- Model end -->

	<!-- Model Start -->
	<div id="dlgx" class="easyui-dialog" style="width:800px; height:auto; padding:10px" closed="true" buttons="#t_dlg_dis-buttonsx" >
		<?php $this->load->view('view_penerimaan_angsuran_detail_akun'); ?>
		<!-- Tombol simpan & Batal Form -->
        <div id="t_dlg_dis-buttonsx">
            <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:$('#dlgx').dialog('close')"><i class="icon-remove icon-large"></i>&nbsp;Batal</a>      
        </div>
	</div>
	<!-- Model end -->

</div>