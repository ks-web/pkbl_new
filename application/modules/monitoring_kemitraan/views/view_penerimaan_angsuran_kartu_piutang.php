<style>
table {
	font-family: 'sans-serif'
}
.bold {
	font-weight: bold;
}
.fontred {
	color: red;
}
.font10 {
	font-size: 10px;
}
.font11 {
	font-size: 11px;
}
.font12 {
	font-size: 12px;
}
.font13 {
	font-size: 13px;
}
.col-1 {width: 8.33%;}
.col-2 {width: 16.66%;}
.col-3 {width: 25%;}
.col-4 {width: 33.33%;}
.col-5 {width: 41.66%;}
.col-6 {width: 50%;}
.col-7 {width: 58.33%;}
.col-8 {width: 66.66%;}
.col-9 {width: 75%;}
.col-10 {width: 83.33%;}
.col-11 {width: 91.66%;}
.col-12 {width: 100%;}
.center {
	text-align: center;
}
.right {
	text-align: right;
}
</style>
<?php //print_r($angsuran); ?>
<table border="0" width="80%" align="center">
	<tr>
		<td colspan="4" align="center"><span style="font-size: 30px">Kartu Piutang<span></td>
	</tr>
	<tr>
		<td colspan="4" align="center">Posisi <?php echo bulan_indo(str_replace('0', '', date('m'))).' '.date('Y'); ?></td>
	</tr>
	<tr>
		<td class="col-2">No Kontrak</td>
		<td class="col-5">: aaa</td>
		<td class="col-3">Pinjaman Pokok</td>
		<td class="col-2">: <?php echo number_format($mitra[0]['total_pinjaman_bunga']-$mitra[0]['total_bunga']); ?></td>
	</tr>
	<tr>
		<td class="col-2">Nama Usaha</td>
		<td class="col-5">: <?php echo $mitra[0]['nama_perusahaan']; ?></td>
		<td class="col-3">Bunga <?php echo $mitra[0]['bunga']; ?>,00% x <?php echo $mitra[0]['tahun']; ?> Th</td>
		<td class="col-2">: <?php echo number_format($mitra[0]['total_bunga']); ?></td>
	</tr>
	<tr>
		<td class="col-2">Nama Pemilik</td>
		<td class="col-5">: <?php echo $mitra[0]['nama']; ?></td>
		<td class="col-3">Total Hutang</td>
		<td class="col-2">: <?php echo number_format($mitra[0]['total_pinjaman_bunga']); ?></td>
	</tr>
	<tr>
		<td class="col-2">Alamat</td>
		<td class="col-5">: <?php echo $mitra[0]['alamat']; ?></td>
		<td class="col-3">Angsuran/Bulan</td>
		<td class="col-2">: <?php echo number_format($mitra[0]['angsuran_bulanan']); ?></td>
	</tr>
	<tr>
		<td class="col-2"></td>
		<td class="col-5"><?php echo $mitra[0]['kelurahan'].' '.$mitra[0]['kecamatan'].' '.$mitra[0]['kota'].' '.$mitra[0]['propinsi']; ?></td>
		<td class="col-3">Angsuran Pertama</td>
		<td class="col-2">: <?php echo $mitra[0]['tanggal_angsuran']; ?></td>
	</tr>
	<tr>
		<td class="col-2">Telp/HP</td>
		<td class="col-5">: <?php echo $mitra[0]['telepon'].' '.$mitra[0]['handphone']; ?></td>
		<td class="col-3">Surveyor</td>
		<td class="col-2">: -</td>
	</tr>
	<tr>
		<td class="col-2">Sektor Usaha</td>
		<td class="col-5">: <?php echo $mitra[0]['nama_sektor']; ?></td>
		<td class="col-3">Pembina</td>
		<td class="col-2">: -</td>
	</tr>
</table>
<table border="0" width="80%" align="center">
	<tr>
		<td colspan="2">Rincian Transaksi :</td>
	</tr>
	<tr>
		<td colspan="2"></td>
	</tr>
	<tr>
		<td class="col-6">
			<table border="1" width="100%">
				<tr>
					<td class="col-1">No</td>
					<td class="col-2">Kode</td>
					<td class="col-3">Tanggal</td>
					<td class="col-3">Mutasi</td>
					<td class="col-3">Saldo</td>
				</tr>
				<tr>
					<td class="col-1"><?php echo '1'; ?></td>
					<td class="col-2"><?php echo $mitra[0]['id_cd']; ?></td>
					<td class="col-3"><?php echo $mitra[0]['tanggal_cd']; ?></td>
					<td class="col-3 right"><?php echo number_format($mitra[0]['total_pinjaman_bunga']); ?></td>
					<td class="col-3 right"><?php echo number_format($mitra[0]['total_pinjaman_bunga']); ?></td>
				</tr>
				<?php $i = 2; $sisa_angsuran = 0; foreach ($angsuran as $key) { ?>
				<tr>
					<td class="col-1"><?php echo $i; ?></td>
					<td class="col-2"><?php echo $key['id_angsuran']; ?></td>
					<td class="col-3"><?php echo $key['tanggal_angsuran']; ?></td>
					<td class="col-3 right"><?php echo number_format($key['jumlah']+$key['biaya']); ?></td>
					<td class="col-3 right"><?php echo number_format($key['sisa_angsuran']); ?></td>
				</tr>
				<?php $i++; $sisa_angsuran = $key['sisa_angsuran']; } ?>
			</table>
		</td>
		<td class="col-6" valign="top">
			<table border="1" width="100%">
				<tr>
					<td class="col-1">No</td>
					<td class="col-2">Kode</td>
					<td class="col-3">Tanggal</td>
					<td class="col-3">Mutasi</td>
					<td class="col-3">Saldo</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			&nbsp;
		</td>
	</tr>
	<tr>
		<td class="col-6">
			Catatan :
		</td>
		<td class="col-6 center">
			Divisi Commmunity Development
		</td>
	</tr>
	<tr>
		<td class="col-6">
			<table>
				<tr>
					<td>Angsuran Dibayar</td>
					<td>: Rp</td>
					<td>&nbsp;</td>
					<td class="right"><?php echo number_format($mitra[0]['total_pinjaman_bunga']-$sisa_angsuran); ?></td>
				</tr>
				<tr>
					<td>Sisa Hutang</td>
					<td>: Rp</td>
					<td>&nbsp;</td>
					<td class="right"><?php echo number_format($sisa_angsuran); ?></td>
				</tr>
				<tr>
					<td class="bold">Hutang Jatuh Tempo</td>
					<td class="bold">: Rp</td>
					<td>&nbsp;</td>
					<td class="right"><?php echo number_format($mitra[0]['angsuran_bulanan']); ?></td>
				</tr>
			</table>
		</td>
		<td class="col-6">
			&nbsp;
		</td>
	</tr>
	<tr>
		<td class="col-6">
			&nbsp;
		</td>
		<td class="col-6 center">
			Adriana Peris
		</td>
	</tr>
	<tr>
		<td class="col-6">
			Dicetak <?php echo date('d M Y'); ?>
		</td>
		<td class="col-6 center">
			Manager
		</td>
	</tr>
</table>

<?php
function Terbilang($x)
{
  $abil = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
  if ($x < 12)
    return " " . $abil[$x];
  elseif ($x < 20)
    return Terbilang($x - 10) . "belas";
  elseif ($x < 100)
    return Terbilang($x / 10) . " puluh" . Terbilang($x % 10);
  elseif ($x < 200)
    return " seratus" . Terbilang($x - 100);
  elseif ($x < 1000)
    return Terbilang($x / 100) . " ratus" . Terbilang($x % 100);
  elseif ($x < 2000)
    return " seribu" . Terbilang($x - 1000);
  elseif ($x < 1000000)
    return Terbilang($x / 1000) . " ribu" . Terbilang($x % 1000);
  elseif ($x < 1000000000)
    return Terbilang($x / 1000000) . " juta" . Terbilang($x % 1000000);
}

function tanggal_indo($tanggal)
{
	$bulan = array (1 =>   'Januari',
				'Februari',
				'Maret',
				'April',
				'Mei',
				'Juni',
				'Juli',
				'Agustus',
				'September',
				'Oktober',
				'November',
				'Desember'
			);
	$split = explode('-', $tanggal);
	return $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];
}
function bulan_indo($num)
{
	$bulan = array (1 =>   'Januari',
				'Februari',
				'Maret',
				'April',
				'Mei',
				'Juni',
				'Juli',
				'Agustus',
				'September',
				'Oktober',
				'November',
				'Desember'
			);
	//$split = explode('-', $tanggal);
	//return $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];
	return $bulan[$num];
}
?>