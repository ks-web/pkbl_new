<?php
$app2 = 'Penerimaan Angsuran Akun'; // nama aplikasi
$module2 = 'monitoring_kemitraan';
$appLink2 = 'penerimaan_angsuran_detail_akun'; // controller
$idField2  = 'id_akun'; //field key table
?>

<script>
	var url2;
	var app2 = "<?=$app2?>";
	var appLink2 = '<?=$appLink2?>';
	var module2 = '<?=$module2?>';
	var idField2 = '<?=$idField2?>';
	//var idParent = 1;
	
    function add2(){
        $('#dlg2').dialog('open').dialog('setTitle','Tambah ' + app2);
		$('#fm2').form('clear');
	    $('#tbl2').html('Save');
		url2 = '<?=base_url($module2 . '/' . $appLink2 . '/create')?>/'+idParent2;
    }
    function edit2(){
        var row = $('#dg2').datagrid('getSelected');
		if (row){
			$('#tbl2').html('Simpan');
			$('#dlg2').dialog('open').dialog('setTitle','Edit '+ app2);
			$('#fm2').form('load',row);
			url2 = '<?=base_url($module2 . '/' . $appLink2 . '/update')?>/'+row[idField2];
	    }
    }
    function hapus2(){
        var row = $('#dg2').datagrid('getSelected');		
	    if (row){
			$.messager.confirm('Confirm','Apakah Anda Yakin Akan Menghapus ini?',function(r){
				if (r){
					$.post('<?=base_url($module2 . '/' . $appLink2 . '/delete')?>/'+row[idField2],function(result){
						if (result.success){
							$('#dg2').datagrid('reload');	// reload the user data
						} else {
							$.messager.show({	// show error message
								title: 'Error',
								msg:result.msg
							});
						}
					},'json');
				}
			});
		}
    }
    function posting2(){
    	$.messager.confirm('Confirm','Apakah Anda Yakin Akan Melakukan Posting?',function(r){
			if (r){
				$.post('<?=base_url($module2 . '/' . $appLink2 . '/posting')?>/'+idParent2,function(result){
					if (result.success){
						$('.btnOptions1').hide();
						$('.btnOptions2').show();
						$('#dg1').datagrid('reload');	// reload the user data
						//$('#dg1').datagrid('reload');
					} else {
						$.messager.show({	// show error message
							title: 'Error',
							msg:result.msg
						});
					}
				},'json');
			}
		});
    }
    function save2(){
    	//alert(idParent);
	   $('#fm2').form('submit',{
	    	url: url2,
	    	onSubmit: function(){
	    		return $(this).form('validate');
	    	},
	    	success: function(result){
	    		var result = eval('('+result+')');
	    		if (result.success){
	    			$('#dlg2').dialog('close');		
	    			reload2();
	    			//$('#dg2').datagrid('reload');
	                
	    		} else {
	             $.messager.alert('Error Message',result.msg,'error');
	    		}
	    	}
	    });
	}
	function doSearch2(value){
	    $('#dg2').datagrid('load',{    
	    	q:value  
	    });
	}
	function reload2(){
		$.post( '<?=base_url($module2 . '/' . $appLink2 . '/getStatusPosting/')?>/'+idParent2 )
  		.done(function( data ) {
  			console.log(data[0]['status_posted']);
    		if(data[0]['status_posted'] == 1){
				$('.btnOptions1').hide();
				$('.btnOptions2').show();
			} else {
				$('.btnOptions1').show();
				$('.btnOptions2').hide();
			}
    		$('#dg2').datagrid({    
		    	url: '<?=base_url($module2 . '/' . $appLink2 . '/read/')?>/'+idParent2  
		    });
  		});

		/*$('#dg2').datagrid({    
	    	url: '<?=base_url($module2 . '/' . $appLink2 . '/read/')?>/'+idParent2  
	    });*/
	}

	$("input[name='mode']").change(function(){
                var mode = $(this).val();
                $('#account_no').combogrid({
                    mode: mode
                });
            });
</script>
 
<div class="tabs-container">                
	<table id="dg2" class="easyui-datagrid" 
	data-options="  
		singleSelect:'true', 
	    title:'<?=$app2?>',
	    toolbar:'#toolbar2',
	    iconCls:'icon-cog',
	    rownumbers:'true',  
	    idField:'<?=$idField2?>', 
	    pagination:'true',
	    fitColumns:'true',
	    pageList: [10,20,30]
	    "
	>
	    <thead>
	    	<!-- <th field="id_asset" width=100" sortable="true">ID Asset</th> -->
	        <th field="account_no" width=100" sortable="true">Account No</th>
	        <th field="nama_akun" width=100" sortable="true">Nama Akun</th>
			<th field="debet" width="100" sortable="true" formatter="format_numberdisp">Debet</th>
			<th field="kredit" width="100" sortable="true" formatter="format_numberdisp">Credit</th>
	    </thead>
	</table>
	
	<!-- Model Start -->
	<div id="dlg2" class="easyui-dialog" style="width:500px; height:auto; padding:10px" closed="true" buttons="#t_dlg_dis-buttons2" >
	    <form id="fm2" method="post" enctype="multipart/form-data" action="">
	       <table width="100%" align="center" border="0">
	            <tr>
	                <td>Account</td>
	                <td>:</td>
	                <td><input name="account_no" id="account_no"  class="easyui-combogrid" required style="width: 203px"
						data-options="panelHeight:200, panelWidth: 400,
						valueField:'',
						idField:'kode_account',
						textField:'kode_account',
						mode:'remote',
						url:'<?=base_url($module2 . '/' . $appLink2 . '/getAccount') ?>',
						onSelect :function(indek,row)
						{
							//console.log(row);
							$('#nama_akun').val(row.uraian);
						},
						columns:[[
                       		{field:'kode_account',title:'Account',width:80},
                        	{field:'uraian',title:'Uraian',width:150}
                        ]]

						" >
	                </td>
	            </tr>
	            <tr>
	                <td>Debet</td>
	                <td>:</td>
	                <td>
	                    <input type="text" name="nama_akun" id="nama_akun" style="width: 200px;" readonly="readonly">
	                </td>
	            </tr>
	            <tr>
	                <td>Debet</td>
	                <td>:</td>
	                <td>
	                    <input class="easyui-numberbox" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%',onChange:function(a, b){ if(a != 0){$('#kredit').numberbox('setValue', 0)} }" name="debet" id="debet" required style="width: 200px;" readonly="readonly">
	                </td>
	            </tr>
	             <tr>
	                <td>Credit</td>
	                <td>:</td>
	                <td>
	                    <input class="easyui-numberbox" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%',onChange:function(a, b){ if(a != 0){$('#debet').numberbox('setValue', 0)} }" name="kredit" id="kredit" required style="width: 200px;" readonly="readonly">
	                </td>
	            </tr>	            
	        </table>
	    </form>
	   
	   	<!-- Tombol Add, Edit, Hapus Datagrid -->
       	<div id="toolbar2">  
            <table align="center" style="padding: 0px; width: 99%;">
                <tr>
                    <td>
                        <!--<a href="javascript:void(0)" class="btn btn-small btn-success btnOptions1" onclick="javascript:add2()"><i class="icon-plus-sign icon-large"></i>&nbsp;Add</a>-->
                        <a href="javascript:void(0)" class="btn btn-small btn-primary btnOptions1" onclick="javascript:edit2()"><i class="icon-edit"></i>&nbsp;Edit</a>
                        <!--<a href="javascript:void(0)" class="btn btn-small btn-danger btnOptions1" onclick="javascript:hapus2()"><i class="icon-trash icon-large"></i>&nbsp;Delete</a>-->
                        <a href="javascript:void(0)" class="btn btn-small btn-primary btnOptions1" onclick="javascript:posting2()"><i class="icon-check icon-large"></i>&nbsp;Posting</span></a>
                        <a href="javascript:void(0)" class="btn btn-small btn-success btnOptions2"><i class="icon-check icon-large" data-options="disabled:true"></i>&nbsp;Posted</span></a>
                    </td>
                    <td>&nbsp;</td>
                    <td align="right">
                    	<input class="easyui-searchbox" prompt="Silahkan Input Field" searcher="doSearch2"  style="width:100px"/>
                    </td>
                </tr>
            </table>  
        </div>  
        <!-- end tombol datagrid -->
        <!-- Tombol simpan & Batal Form -->
        <div id="t_dlg_dis-buttons2">
            <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:save2()"><i class="icon-save icon-large"></i>&nbsp;Simpan</a>
            <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:$('#dlg2').dialog('close')"><i class="icon-remove icon-large"></i>&nbsp;Batal</a>      
        </div>
	</div>
	<!-- Model end -->
</div>