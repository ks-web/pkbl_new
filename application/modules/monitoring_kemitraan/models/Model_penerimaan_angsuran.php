<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	Class Model_penerimaan_angsuran extends MY_Model {
		function __construct() {
			parent::__construct();
			//$this->_table = "tbl_angsuran_header"; // for insert, update, delete
			$this->_view = "tbl_angsuran_header_vd"; // for call view
			$this->_order = 'asc';
			$this->_sort = 'id_mitra';
			$this->_page = 1;
			$this->_rows = 10;

			$this->_create = true;
			$this->_update = true;

			if ($this->uri->segment(4)) {
				$this->_filter = array('id_mitra' => $this->uri->segment(4));
			}

			//parameter from post/get - search function
			if ($this->input->post('q')) {
				$this->_like = array(
					'id_mitra' => $this->input->post('q'),
					'nama' => $this->input->post('q'),
					'nama_perusahaan' => $this->input->post('q')
				);
			}

			$this->_param = array('id_mitra' => $this->input->post('id_mitra'));

			//data array for input to database
			$this->_data = array(
				//'col1' => $this->input->post('col1'),
				//'col2' => $this->input->post('col2'),
			);
			
		}

		public function kartuPiutangMitraInit($filter)
		{
			$this->db->select('A.*');
			$this->db->from('kartu_piutang_init_vd A');
			$this->db->where($filter);

			$query = $this->db->get();

			//echo $this->db->last_query();

			return $query->result_array();
		}

		public function kartuPiutangMitra($filter)
		{
			/*$this->db->select('A.*, B.nama_sektor, C.*, D.tanggal_angsuran');
			$this->db->select('E.bunga, E.tahun, E.angsuran_pokok, E.angsuran_bunga, E.total_pinjaman_bunga');
			$this->db->select('E.angsuran_bulanan, E.angsuranbunga_perbln, E.angsuranpokok_perbln');
			$this->db->select('F.no_kontrak');
			$this->db->from('tmitra A');
			$this->db->join('sektor_kemitraan B', 'B.id_sektor = A.sektor_usaha');
			//$this->db->join('tcd_mitra C', 'C.id_mitra = A.id_mitra');
			$this->db->join('tcd_all C', 'C.no = A.id_mitra', 'left');
			$this->db->join('tbl_angsuran D', 'D.id_mitra = A.id_mitra AND D.angsuran_ke = 1');
			$this->db->join('tpenilaian_survey_pengusaha_evaluator E', 'E.id_mitra = A.id_mitra');
			$this->db->join('tpersetujuan_mitra F', 'F.id_mitra = A.id_mitra');
			$this->db->join('tpenilaian_pengusaha_evaluator_res G', 'G.id_mitra = A.id_mitra', 'left');
			$this->db->where($filter);*/

			$this->db->select('A.*');
			$this->db->from('kartu_piutang_vd A');
			$this->db->where($filter);

			$query = $this->db->get();

			//echo $this->db->last_query();

			return $query->result_array();
		}

		public function kartuPiutang($filter)
		{
			$this->db->select('A.*');
			$this->db->from('tbl_angsuran_vd A');
			$this->db->where($filter);
			$this->db->order_by('tanggal_angsuran');

			$query = $this->db->get();

			//echo $this->db->last_query();

			return $query->result_array();
		}

		public function getPegawai($filter)
		{
			$this->db->select('A.*');
			$this->db->from('mas_pegawai A');
			$this->db->where($filter);

			$query = $this->db->get();

			return $query->result_array();
		}

		public function getStatusPosted($filter)
		{
			$this->db->select('*');
			$this->db->from('tbl_angsuran A');
			$this->db->where($filter);

			$query = $this->db->get();

			return $query->num_rows();
		}

		public function printAngsuranForm($filter)
		{
			$this->db->select('*');
			$this->db->from('tbl_angsuran_print_vd');
			$this->db->where($filter);

			$query = $this->db->get();

			return $query->result_array();
		}
		public function datakosong($filter)
		{
			$this->db->select('*');
			$this->db->from('tbl_angsuran_header_vd');
			$this->db->where($filter);

			$query = $this->db->get();

			return $query->result_array();
		}

	}
