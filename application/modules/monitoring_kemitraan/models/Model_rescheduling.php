<?php
if (! defined('BASEPATH'))
    exit('No direct script access allowed');

class Model_rescheduling extends MY_Model
{

    function __construct()
    {
        parent::__construct();
        $this->_table = "tpenilaian_pengusaha_evaluator_res"; // for insert, update, delete
        $this->_view = "tpenilaian_pengusaha_evaluator_res_vd"; // for call view
        $this->_order = 'asc';
        $this->_sort = 'id_res';
        $this->_page = 1;
        $this->_rows = 10;
        
        $this->_create = true;
        $this->_update = true;
        
        if ($this->uri->segment(4)) {
            $this->_filter = array(
                'id_res' => $this->uri->segment(4)
            );
        }
        
        // parameter from post/get - search function
        if ($this->input->post('q')) {
            $this->_like = array(
                'id_mitra' => $this->input->post('q')
            );
        }
        
        // $this->_param = array('node' => $this->input->post('node'));
        
        // data array for input to database
        $this->_data = array(
            'id_mitra' => $this->input->post('id_mitra'),
            'nilai_dibayar' => $this->input->post('nilai_dibayar'),
            'total_sisa' => $this->input->post('sisa_angsuran'),
            'pokok_angsuran' => $this->input->post('pokok_angsuran'),
            'nilai_angsuran' => $this->input->post('nilai_angsuran'),
            'status_angsuran' => $this->input->post('status_angsuran'),
            'tahun_angsuran' => $this->input->post('tahun_angsuran'),
            'keterangan' => $this->input->post('keterangan')
        );
    }

    function search_no()
    {
        if ($this->input->get('q')) {
            $like = array(
                'id_mitra' => $this->input->get('q')
            );
            $this->db->or_like($like);
        }
        $this->db->where('status_pelunasan is null');
        $this->db->limit(5);
        $res = $this->db->get('rescheduling_list_mitra_vd');
        
        return json_encode($res->result_array());
    }

    public function createJatuhTempo($id)
    {
        $this->db->select('A.id_mitra, A.tahun_angsuran, YEAR(A.tanggal_create) tahun, MONTH(A.tanggal_create) bulan, DAY(A.tanggal_create) hari, fc_get_status_sektor_usaha(A.id_mitra) AS status_sektor');
        $this->db->from('tpenilaian_pengusaha_evaluator_res A');
        $this->db->where('A.id_res', $id);
        $query = $this->db->get();
        
        // echo $this->db->last_query();
        
        $result = $query->row();
        
        if ($result->status_sektor == 1) {
            $jumlah_angsuran = $result->tahun_angsuran * 2;
            $bulan_step = 6;
        } else {
            $jumlah_angsuran = $result->tahun_angsuran * 12;
            $bulan_step = 1;
        }
        
        $tahun = $result->tahun;
        $bulan = $result->bulan;
        $hari = $result->hari;
        // if($hari == 31){
        // $hari = 1;
        // $bulan = $bulan + 1;
        // } else if($hari == 28 && $bulan == 2) {
        // $hari = 1;
        // $bulan = $bulan + 1;
        // }
        if ($hari > 28) {
            $hari = 1;
            $bulan = $bulan + 1;
        }
        
        for ($i = 1; $i <= $jumlah_angsuran; $i ++) {
            // $bulan = $bulan + 1;
            $bulan = $bulan + $bulan_step;
            if ($bulan == 13) {
                $bulan = $bulan - 12;
                $tahun = $tahun + 1;
            }
            
            $data = array(
                'id_mitra' => $result->id_mitra,
                'tgl_jatuh_tempo' => $tahun . '-' . $bulan . '-' . $hari,
                'angsuran_ke' => $i
            );
            $this->db->insert('tbl_jatuhtempo', $data);
        }
    }
}
