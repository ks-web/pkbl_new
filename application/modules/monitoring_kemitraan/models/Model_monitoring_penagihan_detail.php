<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	Class Model_monitoring_penagihan_detail extends MY_Model {
		function __construct() {
			parent::__construct();
			$this->_table = "tbl_monitoring_kemitraan"; // for insert, update, delete
			$this->_view = "tbl_monitoring_kemitraan_vd"; // for call view
			$this->_order = 'asc';
			$this->_sort = 'id_monitoring';
			$this->_page = 1;
			$this->_rows = 10;

			$this->_create = true;
			$this->_update = true;

			if ($this->uri->segment(4)) {
				$this->_filter = array('id_mitra' => $this->uri->segment(4));
			}

			//parameter from post/get - search function
			if ($this->input->post('q')) {
				$this->_like = array(
					'id_monitoring' => $this->input->post('q'),
					'id_mitra' => $this->input->post('q'),
					'id_persetujuan' => $this->input->post('q'),
					'tanggal_kunjungan' => $this->input->post('q')
				);
			}

			$this->_param = array('id_monitoring' => $this->input->post('id_monitoring'));

			//data array for input to database
			$this->_data = array(
				'tanggal_kunjungan' => $this->input->post('tanggal_kunjungan'),
				'asset' => $this->input->post('asset'),
				'omzet' => $this->input->post('omzet'),
				'tenaga_kerja' => $this->input->post('tenaga_kerja'),
				'aspek_pengelolaan' => $this->input->post('aspek_pengelolaan'),
				'aspek_pemasaran' => $this->input->post('aspek_pemasaran'),
				'aspek_teknis' => $this->input->post('aspek_teknis'),
				'aspek_keuangan' => $this->input->post('aspek_keuangan'),
				'progres_mitra' => $this->input->post('progres_mitra'),
				'status_mitra' => $this->input->post('status_mitra'),
			);
			
		}

	}
