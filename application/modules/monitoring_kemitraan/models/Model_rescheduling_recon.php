<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	Class Model_rescheduling_recon extends MY_Model {
		function __construct() {
			parent::__construct();
			$this->_table = "tpersetujuan_mitra"; // for insert, update, delete
			$this->_view = "rescheduling_recon_vd"; // for call view
			$this->_order = 'desc';
			$this->_sort = 'tanggal_angsuran';
			$this->_page = 1;
			$this->_rows = 10;

			$this->_create = true;
			$this->_update = true;

			if ($this->uri->segment(4)) {
				$this->_filter = array('id_persetujuan' => $this->uri->segment(4));
			}

			//parameter from post/get - search function
			if ($this->input->post('q')) {
				$this->_like = array(
					'id_mitra' => $this->input->post('q'),
					'no_kontrak' => $this->input->post('q'),
					'nama' => $this->input->post('q')
				);
			}

			$this->_param = array('id_persetujuan' => $this->input->post('id_persetujuan'));

			//data array for input to database
			$this->_data = array(
				'catatan_res' => $this->input->post('catatan_res'),
				'tanggal_catatan_res' => date("Y-m-d"),
			);
			
		}

	}
