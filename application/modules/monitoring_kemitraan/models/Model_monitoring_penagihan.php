<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	Class Model_monitoring_penagihan extends MY_Model {
		function __construct() {
			parent::__construct();
			//$this->_table = "tbl_angsuran_header"; // for insert, update, delete
			$this->_view = "tbl_monitoring_kemitraan_header_vd"; // for call view
			$this->_order = 'asc';
			$this->_sort = 'id_mitra';
			$this->_page = 1;
			$this->_rows = 10;

			$this->_create = true;
			$this->_update = true;

			if ($this->uri->segment(4)) {
				$this->_filter = array('id_mitra' => $this->uri->segment(4));
			}

			//parameter from post/get - search function
			if ($this->input->post('q')) {
				$this->_like = array(
					'id_mitra' => $this->input->post('q'),
					'nama' => $this->input->post('q'),
					'nama_perusahaan' => $this->input->post('q')
				);
			}

			$this->_param = array('id_mitra' => $this->input->post('id_mitra'));

			//data array for input to database
			$this->_data = array(
				//'col1' => $this->input->post('col1'),
				//'col2' => $this->input->post('col2'),
			);
			
		}

	}
