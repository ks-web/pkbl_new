<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	Class Model_penerimaan_angsuran_detail extends MY_Model {
		function __construct() {
			parent::__construct();
			$this->_table = "tbl_angsuran"; // for insert, update, delete
			$this->_view = "tbl_angsuran_vd"; // for call view
			$this->_order = 'asc';
			$this->_sort = 'id_angsuran';
			$this->_page = 1;
			$this->_rows = 10;

			$this->_create = true;
			$this->_update = true;

			if ($this->uri->segment(4)) {
				$this->_filter = array('id_mitra' => $this->uri->segment(4));
			}

			//parameter from post/get - search function
			if ($this->input->post('q')) {
				$this->_like = array(
					'id_angsuran' => $this->input->post('q'),
					'angsuran_ke' => $this->input->post('q'),
					'jumlah' => $this->input->post('q'),
					'tanggal_angsuran' => $this->input->post('q')
				);
			}

			$this->_param = array('id_angsuran' => $this->input->post('id_angsuran'));
			$jatuh_tempo = $this->input->post('jatuh_tempo');
			$status = $this->input->post('status');
			
			//data array for input to database
			$this->_data = array(
				'tanggal_angsuran' 	=> $this->input->post('tanggal_angsuran'),
				'jumlah' 			=> $this->input->post('jumlah'),
				'biaya' 			=> $this->input->post('biaya'),
				'angsuran_ke' 		=> $this->input->post('angsuran_ke'),
				'sisa_angsuran' 	=> $this->input->post('sisa_angsuran'),
				'id_pembayaran' 	=> $this->input->post('id_pembayaran'),
				'nip' 				=> $this->input->post('nip'),
				'nama_rek' 			=> $this->input->post('nama_rek'),
				'no_rek' 			=> $this->input->post('no_rek'),
				'status'			=> $this->input->post('status'),
				'keterangan'		=> $this->input->post('keterangan')
				//'jatuh_tempo' 		=> $jatuh_tempo,
				//'status' 			=> $status
			);
		}

		public function getSisaAngsuran($filter)
		{
			/*$this->db->select('MIN(sisa_angsuran) sisa_angsuran');
			$this->db->from('tbl_angsuran');
			$this->db->where($filter);

			$query = $this->db->get();
			$result = $query->result_array();

			if($result[0]['sisa_angsuran'] == ''){
				$this->db->select('nilai_disetujui sisa_angsuran');
				$this->db->from('tpersetujuan_mitra');
				$this->db->where($filter);

				$query = $this->db->get();
				$result = $query->result_array();
			}

			return $result;*/
			
			$this->db->select('*');
			$this->db->from('tbl_angsuran_sisa_vd');
			$this->db->where($filter);

			$query = $this->db->get();

			return $query->result_array();
		}

		public function getPembayaran($filter)
		{
			$this->db->select('*');
			$this->db->from('mas_pembayaran');
			$this->db->where($filter);

			$query = $this->db->get();

			return $query->result_array();
		}

		public function getPegawai($filter)
		{
			$this->db->select('*');
			$this->db->from('mas_pegawai');
			$this->db->where($filter);

			$query = $this->db->get();

			return $query->result_array();
		}

		public function printAngsuranForm($filter)
		{
			$this->db->select('*');
			$this->db->from('tbl_angsuran_print_vd');
			$this->db->where($filter);

			$query = $this->db->get();

			return $query->result_array();
		}

		public function printAngsuran($filter)
		{
			$this->db->select('*');
			$this->db->from('tbl_angsuran_print_vd');
			$this->db->where($filter);

			$query = $this->db->get();

			return $query->result_array();
		}

		public function getAccount($filter)
		{
			$this->db->select('A.*');
			$this->db->from('account_pk A');
			//$this->db->where($filter);

			$query = $this->db->get();

			return $query->result_array();
		}
		
		public function get_jatuh_tempo($filter)
		{
			$this->db->select('*');
			$this->db->from('tbl_angsuran_vd');
			$this->db->where('id_mitra', $filter);
			$query = $this->db->get();
			$baris = $query->num_rows();
			
			if($baris > 0)
			{
				$this->db->select_max('jatuh_tempo');
				$this->db->from('tbl_angsuran_vd');
				$this->db->where('id_mitra', $filter);
				$query = $this->db->get()->row();
				$jatuh_tempo_prev = $query->jatuh_tempo;
				$date_now = date('Y-m-d');
				
				if($date_now > $jatuh_tempo_prev)
				{
					$pecah = explode('-', $jatuh_tempo_prev);
					$month_jatuh_tempo = $pecah[1] + 1;
					return $pecah[0].'-'.$month_jatuh_tempo.'-'.$pecah[2];
				}
				
				else
				{
					return $jatuh_tempo_prev;
				}
				/*
				$pecah = explode('-', $query->jatuh_tempo);
				return $pecah[0].'-'.date('m').'-'.$pecah[2];
				*/
			}
			else
			{
				$this->db->select('*');
				$this->db->from('tpersetujuan_mitra');
				$this->db->where('id_mitra', $filter);
				$query = $this->db->get()->row();
				$pecah = explode('-', $query->tanggal_disetujui);
				$next_month = $pecah[1] + 1;
				if(strlen($next_month) > 1)
					$month = $next_month;
				else
					$month = '0'.$next_month;
					
				return $pecah[0].'-'.$month.'-'.$pecah[2];
			}
		}
		
		public function get_status($tgl_angsuran, $tgl_jatuh_tempo)
		{
			$pecah2 = explode('-', $tgl_jatuh_tempo);
			$pecah3	= explode('-', $tgl_angsuran);
			$jd1 = GregorianToJD($pecah2[1],$pecah2[2],$pecah2[0]);
			$jd2 = GregorianToJD($pecah3[1],$pecah3[2],$pecah3[0]);
				
			$selisih = $jd2 - $jd1;
			
			if($selisih <= 0)
				return '1';
			else if($selisih > 0 || $selisih <= 180)
				return 2;
			else if($selisih >= 181 || $selisih <= 270)
				return '3';
			else	
				return '4';
				
			
		}

		public function createAccount($id){
        	$this->db->select('sektor_usaha, jumlah, biaya, total');
        	$this->db->from('tbl_angsuran_vd');
        	$this->db->where('id_angsuran', $id);
        	$query = $this->db->get();

        	$result = $query->row();
        	//print_r($result);
        	//echo $this->db->last_query();

        	/*$this->db->select('A.*, B.uraian debet_account, C.uraian kredit_account');
        	$this->db->from('mas_account_sektor A');
        	$this->db->join('mas_account B', 'B.kode_account = A.debet');
        	$this->db->join('mas_account C', 'C.kode_account = A.kredit');
        	$this->db->where(array('menu' => 2, 'id_sektor' => $result->sektor_usaha));
        	$query2 = $this->db->get();

        	$result2 = $query2->row();
        	//echo $this->db->last_query();

        	$data = array('id_cd_um' => $id, 'account_no' => $result2->debet, 'nama_akun' => $result2->debet_account, 'kredit' => 0, 'debet' => $result->total, 'status' => 'KM', 'transaksi' => 'CR');
        	$this->db->insert('detail_akun_temp', $data);

        	$data = array('id_cd_um' => $id, 'account_no' => $result2->kredit, 'nama_akun' => $result2->kredit_account, 'kredit' => $result->jumlah, 'debet' => 0, 'status' => 'KM', 'transaksi' => 'CR');
        	$this->db->insert('detail_akun_temp', $data);

        	$data = array('id_cd_um' => $id, 'account_no' => $result2->kredit, 'nama_akun' => $result2->kredit_account, 'kredit' => $result->biaya, 'debet' => 0, 'status' => 'KM', 'transaksi' => 'CR');
        	$this->db->insert('detail_akun_temp', $data);*/

        	$this->db->select('A.*, B.uraian');
        	$this->db->from('mas_account_sektor A');
        	$this->db->join('mas_account B', 'B.kode_account = A.account_no');
        	$this->db->where(array('status' => 'KM', 'transaksi' => 'CR', 'id_sektor' => $result->sektor_usaha));
        	$query2 = $this->db->get();
        	$result2 = $query2->result_array();
        	//print_r($result2);
        	foreach ($result2 as $key) {
        		if($key['posisi'] == 'C'){
        			if($key['biaya'] == 'Y'){
        				$credit = $result->biaya;
        			} else {
        				$credit = $result->jumlah;
        			}
        			$debet = 0;
        		} else {
        			$debet = $result->total;
        			$credit = 0;
        		}
        		$data = array('id_cd_um' => $id, 'account_no' => $key['account_no'], 'nama_akun' => $key['uraian'], 'kredit' => $credit, 'debet' => $debet, 'status' => 'KM', 'transaksi' => 'CR');
        		//print_r($data);
        		//echo "<br>";
        		$this->db->insert('detail_akun_temp', $data);
        	}
        }

        public function deleteAccount($filter)
        {
        	$this->db->where($filter);
			$this->db->delete('detail_akun_temp');
        }

        public function getJatuhTempo($id_mitra, $tgl_bayar)
        {
        	$data = $this->db->query("CALL tbl_jatuhtempo_sp('".$id_mitra."','".$tgl_bayar."')");
			//$result = $data->result();
			
			return $data->result();
		}

		public function setStatusJatuhTempo($filter, $data)
		{
			$this->db->where($filter);
			$this->db->update('tbl_jatuhtempo', $data);
		}

		public function getStatusAngsuran($filter)
		{
			$this->db->select('A.status_angsuran');
			$this->db->from('tpenilaian_pengusaha_evaluator_res A');
			$this->db->where($filter);
			$this->db->order_by('A.status_angsuran', 'DESC');
			$this->db->limit(1);

			$query = $this->db->get();
			//echo $this->db->last_query();
			if($query->num_rows() > 0){
				return $query->row()->status_angsuran;
			} else {
				return 'N';
			}
        	
		}


}
