<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');
	class Penerimaan_angsuran extends MY_Controller {
		public $models = array('penerimaan_angsuran');
		
		public function __construct() {
			parent::__construct();
		}

		public function index() {
			$data = array();
			$data['menu'] = $this->model_menu->getAllMenu();

			$this->template->load('template', 'view_penerimaan_angsuran', $data);
		}

		public function read() {
			$this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read());
		}

		public function read_all() {
			$this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read_all());
		}

		public function create() {
			$param = $this->input->post('param'); // parametter key

			// additional block

			// addtional get
			$result = $this->{$this->models[0]}->insert($this->data_add);
			if ($result == 1) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => $this->db->_error_message())));
			}
		}

		public function update() {
			$param = $this->uri->segment(4); // parameter key

			// additional block

			// addtional data

			// additional where
			$this->where_add['param'] = $param;

			$result = $this->{$this->models[0]}->update($this->where_add, $this->data_add);
			if ($result == 1) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'Data Gagal Di Update !')));
			}
		}

		public function delete() {
			$param = $this->uri->segment(4); // parameter key

			// additional block

			// additional where
			$this->where_add['param'] = $param;

			$result = $this->{$this->models[0]}->delete($this->where_add);
			if ($result == 1) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'Data Gagal Di Hapus !')));
			}
		}

		public function printKartuPiutang()
		{
			$param = $this->uri->segment(4); // parameter key
			$filter = array('A.id_mitra' => $param);

			$data['mitra'] = $this->{$this->models[0]}->kartuPiutangMitra($filter);
			$data['angsuran'] = $this->{$this->models[0]}->kartuPiutang($filter);

			$this->load->view('view_penerimaan_angsuran_kartu_piutang', $data);
		}

		public function printKartuPiutangAkumulasi()
		{
			$param = $this->uri->segment(4); // parameter key
			$filter = array('A.id_mitra' => $param);
			$filter2 = array('A.id_mitra' => $param, 'status_angsuran' => 'N');
			$filter3 = array('A.id_mitra' => $param, 'status_angsuran' => 'P1');
			$filter4 = array('A.id_mitra' => $param, 'status_angsuran' => 'P2');
			$filter5 = array('A.id_mitra' => $param, 'status_angsuran' => 'PB');

			//$data['mitra'] = $this->{$this->models[0]}->kartuPiutangMitra($filter);
			//$data['mitraInit'] = $this->{$this->models[0]}->kartuPiutangMitraInit($filter);
			$data['mitra'] = $this->{$this->models[0]}->kartuPiutangMitraInit($filter);
			$data['angsuran'] = $this->{$this->models[0]}->kartuPiutang($filter2);
			$data['mitraP1'] = $this->{$this->models[0]}->kartuPiutangMitra($filter3);
			$data['angsuranP1'] = $this->{$this->models[0]}->kartuPiutang($filter3);
			$data['mitraP2'] = $this->{$this->models[0]}->kartuPiutangMitra($filter4);
			$data['angsuranP2'] = $this->{$this->models[0]}->kartuPiutang($filter4);
			$data['mitraPB'] = $this->{$this->models[0]}->kartuPiutangMitra($filter5);
			$data['angsuranPB'] = $this->{$this->models[0]}->kartuPiutang($filter5);
			//echo $this->db->last_query();
			

			$data['pegawai'] = $this->{$this->models[0]}->getPegawai(array('A.id_jabatan' => 1));

			$html = $this->load->view('view_penerimaan_angsuran_kartu_piutang_akumulasi', $data, true);
			//echo $html;
			
			// create pdf using dompdf
  			$this->load->library('dompdf_gen'); // Load library
			$this->dompdf->set_paper('A4', 'LANDSCAPE'); // Setting Paper
			// Convert to PDF
			$this->dompdf->load_html($html);
			$this->dompdf->render();
			$this->dompdf->stream("Kartu_Piutang_".$param.".pdf");
		}

		public function getStatusPosted()
		{
			$id_mitra = $this->input->post('id_mitra');
			$filter = array('A.id_mitra' => $id_mitra, 'A.status_posted' => 0);
			$status = $this->{$this->models[0]}->getStatusPosted($filter);

			$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true, 'status' => $status)));
		}

		public function printAngsuranForm()
		{
			$param = $this->uri->segment(4); // parameter key
			$filter = array('id_mitra' => $param);
			$data['datakosong'] = $this->{$this->models[0]}->datakosong($filter);
			$data['angsuran'] = $this->{$this->models[0]}->printAngsuranForm($filter);
			$this->load->view('print_tskform.php', $data);
		}

	}
