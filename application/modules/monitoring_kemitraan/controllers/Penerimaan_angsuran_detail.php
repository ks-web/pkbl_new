<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');
	class Penerimaan_angsuran_detail extends MY_Controller {
		public $models = array('penerimaan_angsuran_detail');
		
		public function __construct() {
			parent::__construct();
		}

		public function index() {
			$data = array();
			$data['menu'] = $this->model_menu->getAllMenu();

			$this->template->load('template', 'view_penerimaan_angsuran_detail', $data);
		}

		public function read() {
			$this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read());
		}

		public function read_all() {
			$this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read_all());
		}

		public function create() {
			$param = $this->uri->segment(4); // parameter key

			// additional block
			$this->data_add['id_mitra'] = $param;
			
			$tanggal_angsuran = explode('-', $this->input->post('tanggal_angsuran'));
			$thn_bln = $tanggal_angsuran[0] . '-' . $tanggal_angsuran[1];
			
			//$jatuh_tempo = $this->get_jatuh_tempo($this->uri->segment(4));
			//$status		 = $this->get_status($this->input->post('tanggal_angsuran'), $jatuh_tempo);
			$id_angsuran = $this->get_no($thn_bln);
			$this->data_add['id_angsuran'] = $id_angsuran;
			if($this->input->post('sisa_pokok') == 0){
				$this->data_add['status_pelunasan'] = 1;
			}
			//$this->data_add['jatuh_tempo'] = $jatuh_tempo;
			//$this->data_add['status'] = $status;
			$idJatuhTempo = $this->input->post('id_jatuh_tempo');
			$this->data_add['status_angsuran'] = $this->{$this->models[0]}->getStatusAngsuran(array('A.id_mitra' => $param));
			//echo $this->data_add['status_angsuran'];
			//echo $idJatuhTempo;
			//$this->{$this->models[0]}->setStatusJatuhTempo(array('id' => $idJatuhTempo), array('id_angsuran' => $id_angsuran, 'flag' => 1));

			// addtional get
			$result = $this->{$this->models[0]}->insert($this->data_add);
			if ($result == 1) {
				//$this->{$this->models[0]}->setStatusJatuhTempo(array('id' => idJatuhTempo));
				//$id = $id_angsuran;
				$this->{$this->models[0]}->setStatusJatuhTempo(array('id' => $idJatuhTempo), array('id_angsuran' => $id_angsuran, 'flag' => 1));
				$this->{$this->models[0]}->createAccount($id_angsuran);
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => $this->db->_error_message())));
			}
		}

		public function update() {
			$param = $this->uri->segment(4); // parameter key

			// additional block

			// addtional data
			/*$jatuh_tempo = $this->get_jatuh_tempo($this->uri->segment(4));
			$status		 = $this->get_status($this->input->post('tanggal_angsuran'), $jatuh_tempo);
			$this->data_add['jatuh_tempo'] = $jatuh_tempo;
			$this->data_add['status'] = $status;*/

			// additional where
			$this->where_add['id_angsuran'] = $param;
			$idJatuhTempo = $this->input->post('id_jatuh_tempo');
			$this->{$this->models[0]}->setStatusJatuhTempo(array('id_angsuran' => $param), array('id_angsuran' => 0, 'flag' => 0));

			$result = $this->{$this->models[0]}->update($this->where_add, $this->data_add);
			if ($result == 1) {
				$this->{$this->models[0]}->deleteAccount(array('id_cd_um' => $param, 'status' => 'KM'));
				$this->{$this->models[0]}->createAccount($param);
				$this->{$this->models[0]}->setStatusJatuhTempo(array('id' => $idJatuhTempo), array('id_angsuran' => $param, 'flag' => 1));
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'Data Gagal Di Update !')));
			}
		}

		public function delete() {
			$param = $this->uri->segment(4); // parameter key

			// additional block

			// additional where
			$this->where_add['id_angsuran'] = $param;

			$result = $this->{$this->models[0]}->delete($this->where_add);
			if ($result == 1) {
				$this->{$this->models[0]}->deleteAccount(array('id_cd_um' => $param, 'status' => 'KM', 'transaksi' => 'CR'));
				$this->{$this->models[0]}->setStatusJatuhTempo(array('id_angsuran' => $param), array('id_angsuran' => 0, 'flag' => 0));
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'Data Gagal Di Hapus !')));
			}
		}

		public function getSisaAngsuran()
		{
			$param = $this->input->post('id_mitra');
			$filter = array('id_mitra' => $param);

			$this->output->set_content_type('application/json')->set_output(json_encode($this->{$this->models[0]}->getSisaAngsuran($filter)));
		}

		public function getPembayaran()
		{
			$filter = array('id_pembayaran !=' => 0);

			$this->output->set_content_type('application/json')->set_output(json_encode($this->{$this->models[0]}->getPembayaran($filter)));
		}

		public function getPegawai()
		{
			$filter = array('idPegawai !=' => 0);

			$this->output->set_content_type('application/json')->set_output(json_encode($this->{$this->models[0]}->getPegawai($filter)));
		}

		public function printAngsuran()
		{
			$param = $this->uri->segment(4); // parameter key
			$filter = array('id_angsuran' => $param);

			$data['angsuran'] = $this->{$this->models[0]}->printAngsuran($filter);

			$this->load->view('view_penerimaan_angsuran_detail_print', $data);
		}
		
		public function getAccount()
		{
			$filter = array();
			$this->output->set_content_type('application/json')->set_output(json_encode($this->{$this->models[0]}->getAccount($filter)));
		}
		
		private function get_no($thn_bln) {
			$thn_bln_arr = explode('-', $thn_bln);

			$no = '000';

			$sql = "
				SELECT
					RIGHT(max(a.id_angsuran),4)*1 AS last_seq
				FROM
					tbl_angsuran a
				WHERE
					DATE_FORMAT(a.tanggal_angsuran, '%Y-%m') = ?
			";
			$query = $this->db->query($sql, array($thn_bln));
			$row = $query->row_array();

			$last_seq = (int)$row['last_seq'];
			$next_seq = $last_seq + 1;

			$leng_temp_no = strlen($next_seq);
			$no = 'CR'.implode('', $thn_bln_arr) . substr($no, $leng_temp_no) . $next_seq;

			return $no;

		}
		
		public function get_jatuh_tempo($filter)
		{
			//$filter = $this->uri->segment(4);
			return $this->{$this->models[0]}->get_jatuh_tempo($filter);
		}
		
		public function get_status($tgl_angsuran, $tgl_jatuh_tempo)
		{
			//$filter = $this->uri->segment(4);
			return $this->{$this->models[0]}->get_status($tgl_angsuran, $tgl_jatuh_tempo);
		}

		public function getJatuhTempo()
		{
			$id_mitra = $this->input->post('id_mitra');
			$tgl_bayar = $this->input->post('tgl_bayar');
			//$filter = array('id_mitra' => $param);

			$this->output->set_content_type('application/json')->set_output(json_encode($this->{$this->models[0]}->getJatuhTempo($id_mitra, $tgl_bayar)));
		}

		public function acccount()
		{
			$this->{$this->models[0]}->createAccount(201711002);
		}
		
	}
