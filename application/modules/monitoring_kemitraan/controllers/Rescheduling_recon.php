<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');
	class Rescheduling_recon extends MY_Controller {
		public $models = array('rescheduling_recon');

		public function __construct() {
			parent::__construct();
		}

		public function index() {
			$data = array();
			$data['menu'] = $this->model_menu->getAllMenu();

			$this->template->load('template', 'view_rescheduling_recon', $data);
		}

		public function read() {
			$this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read());
		}

		public function read_all() {
			$this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read_all());
		}

		public function create() {
			NULL;
		}

		public function create_prev_usulan($str_id = null) {

			if ($str_id) {
				$arr_id = explode('_', $str_id);
				$this->db->where_in('id_persetujuan', $arr_id);

				$check = $this->db->get('rescheduling_recon_vd');
				if ($check->num_rows() > 0) {

					$data['no_pkbl'] = '001/USL/PKBL-KS/'.date("Y");
					$data['data'] = $check->result_array();

					// kota tanggal
					$data['kota'] = 'Cilegon';
					$data['tanggal'] = '25 Oktober 2016';

					$html = $this->load->view('rep_resc_prev_usulan', $data);

					// $this->load->library('dompdf_gen');
					// // Load library
					// $this->dompdf->set_paper('A4', 'landscape');
					// // Setting Paper
					// // // Convert to PDF
					// $this->dompdf->load_html($html);
					// $this->dompdf->render();
					// $this->dompdf->stream("Preview_Usualn-" . $str_id . ".pdf");
				} else {
					show_404();
				}
			} else {
				show_404();
			}
		}

		public function create_kontrak($str_id = null) {

			if ($str_id) {
				$arr_id = explode('_', $str_id);
				$this->db->where_in('id_persetujuan', $arr_id);

				$check = $this->db->get('rescheduling_recon_vd');
				if ($check->num_rows() > 0) {

					$data['no_pkbl'] = '001/USL/PKBL-KS/'.date("Y");
					$data['data'] = $check->row_array();
					

					$this->load->helper('terbilang');
					$this->load->helper('tanggal_indo');
					$html = $this->load->view('rep_resc_kontrak', $data, true);

					$this->load->library('dompdf_gen');
					// Load library
					$this->dompdf->set_paper('A4', 'potrait');
					// Setting Paper
					// // Convert to PDF
					$this->dompdf->load_html($html);
					$this->dompdf->render();
					$this->dompdf->stream("Preview_Usualn-" . $str_id . ".pdf");
				} else {
					show_404();
				}
			} else {
				show_404();
			}
		}

		public function update() {
			$param = $this->uri->segment(4);
			// parameter key

			// additional block

			// addtional data

			// additional where
			$this->where_add['id_persetujuan'] = $param;
			$result = $this->{$this->models[0]}->update($this->where_add, $this->data_add);
			if ($result == 1) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'Data Gagal Di Update !')));
			}
		}

		public function delete() {
			NULL;
		}

	}
