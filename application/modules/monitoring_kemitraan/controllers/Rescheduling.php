<?php
if (! defined('BASEPATH'))
    exit('No direct script access allowed');

include_once (APPPATH . "third_party/PhpWord/Autoloader.php");
// include_once(APPPATH."core/Front_end.php");

use PhpOffice\PhpWord\Autoloader;
use PhpOffice\PhpWord\Settings;
Autoloader::register();
Settings::loadConfig();

class Rescheduling extends MY_Controller
{

    public $models = array(
        'rescheduling'
    );

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $data = array();
        $data['menu'] = $this->model_menu->getAllMenu();
        
        $this->template->load('template', 'view_rescheduling', $data);
    }

    public function read()
    {
        $this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read());
    }

    public function read_all()
    {
        $this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read_all());
    }

    public function create()
    {
        // additional block
        $thn_bln = date("Y-m");
        $this->data_add['id_res'] = $this->get_no($thn_bln);
        $this->data_add['creator'] = $this->session->userdata('id');
        $this->data_add['tanggal_create'] = date("Y-m-d H:i:s");
        
        $this->db->where('id_mitra', $this->input->post('id_mitra'));
        $this->db->where('status_angsuran', $this->input->post('status_angsuran'));
        
        $check = $this->db->get('tpenilaian_pengusaha_evaluator_res');
        
        if ($check->num_rows() > 0) {
            $this->output->set_content_type('application/json')->set_output(json_encode(array(
                'msg' => 'Status ' . $this->input->post('status_angsuran') . ' sudah ada.'
            )));
            return false;
        }
        
        // addtional get
        $result = $this->{$this->models[0]}->insert($this->data_add);
        if ($result == 1) {
            $this->{$this->models[0]}->createJatuhTempo($this->data_add['id_res']);
            $this->output->set_content_type('application/json')->set_output(json_encode(array(
                'success' => true
            )));
        } else {
            $this->output->set_content_type('application/json')->set_output(json_encode(array(
                'msg' => $this->db->_error_message()
            )));
        }
    }

    public function update()
    {
        $param = $this->uri->segment(4); // parameter key
                                         
        // additional block
                                         
        // addtional data
        $this->data_add['modifier'] = $this->session->userdata('id');
        $this->data_add['tanggal_modified'] = date("Y-m-d H:i:s");
        
        // additional where
        $this->where_add['id_res'] = $param;
        
        $result = $this->{$this->models[0]}->update($this->where_add, $this->data_add);
        if ($result == 1) {
            $this->output->set_content_type('application/json')->set_output(json_encode(array(
                'success' => true
            )));
        } else {
            $this->output->set_content_type('application/json')->set_output(json_encode(array(
                'msg' => 'Data Gagal Di Update !'
            )));
        }
    }

    public function delete()
    {
        $param = $this->uri->segment(4); // parameter key
                                         
        // additional block
                                         
        // additional where
        $this->where_add['id_res'] = $param;
        
        $result = $this->{$this->models[0]}->delete($this->where_add);
        if ($result == 1) {
            $this->output->set_content_type('application/json')->set_output(json_encode(array(
                'success' => true
            )));
        } else {
            $this->output->set_content_type('application/json')->set_output(json_encode(array(
                'msg' => 'Data Gagal Di Hapus !'
            )));
        }
    }

    public function search_no()
    {
        $this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->search_no());
    }

    public function create_kontrak($no = null)
    {
        if ($no) {
            $this->db->where('id_res', $no);
            
            $check = $this->db->get('rescheduling_kontrak_vd');
            if ($check->num_rows() > 0) {
                $kontrak = $check->row_array();
                $this->load->helper('tanggal_indo');
                $this->load->helper('terbilang');
                
                $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor(APPPATH . 'template_word/kontrak-reschedule.docx');
                
                $templateProcessor->setValue('nama_manager', managerName);
                $templateProcessor->setValue('nama_perusahaan', $kontrak['nama_perusahaan']);
                $templateProcessor->setValue('no_kontrak', $kontrak['no_kontrak']);
                $templateProcessor->setValue('id_mitra', $kontrak['id_mitra']);
                $templateProcessor->setValue('nama', $kontrak['nama']);
                $templateProcessor->setValue('propinsi', $kontrak['propinsi']);
                $templateProcessor->setValue('alamat', $kontrak['alamat']);
                $templateProcessor->setValue('kota', $kontrak['kota']);
                $templateProcessor->setValue('kecamatan', $kontrak['kecamatan']);
                $templateProcessor->setValue('kelurahan', $kontrak['kelurahan']);
                
                $angsuran_pertama_arr = explode('-', $kontrak['angsuran_pertama']);
                $templateProcessor->setValue('angsuran_pertama', nama_bulan($angsuran_pertama_arr[1]) . ' ' . $angsuran_pertama_arr[0]);
                
                $templateProcessor->setValue('id_res', $kontrak['id_res']);
                $templateProcessor->setValue('total_sisa', number_format($kontrak['total_sisa'], 0, ",", "."));
                $templateProcessor->setValue('total_sisa_terbilang', terbilang_display($kontrak['total_sisa']));
                $templateProcessor->setValue('bulan_angsuran', $kontrak['bulan_angsuran']);
                $templateProcessor->setValue('bulan_angsuran_terbilang', terbilang_display($kontrak['bulan_angsuran']));
                $templateProcessor->setValue('nilai_angsuran', number_format($kontrak['nilai_angsuran'], 0, ",", "."));
                $templateProcessor->setValue('nilai_angsuran_terbilang', terbilang_display($kontrak['nilai_angsuran']));
                $templateProcessor->setValue('hari_jatuh_tempo', $kontrak['hari_jatuh_tempo']);
                $templateProcessor->setValue('hari_jatuh_tempo_terbilang', terbilang_display($kontrak['hari_jatuh_tempo']));
                
                $filename = "kontrak-reschedule-" . $kontrak['id_res'] . ".docx";
                $filepath = FCPATH . "results_word\\";
                $fullpath = $filepath . $filename;
                $templateProcessor->saveAs($fullpath);
                
                header("Content-Disposition: attachment; filename='" . $filename . "'");
                readfile($fullpath); // or echo file_get_contents($temp_file);
                unlink($fullpath); // remove temp file
            } else {
                show_404();
            }
        } else {
            show_404();
        }
    }

    private function get_no($thn_bln)
    {
        $thn_bln_arr = explode('-', $thn_bln);
        
        $no = '0000';
        
        $sql = "
				SELECT
					RIGHT(max(a.id_res),4)*1 AS last_seq
				FROM
					tpenilaian_pengusaha_evaluator_res a
				WHERE
					DATE_FORMAT(a.tanggal_create, '%Y-%m') = ?
			";
        $query = $this->db->query($sql, array(
            $thn_bln
        ));
        $row = $query->row_array();
        
        $last_seq = (int) $row['last_seq'];
        $next_seq = $last_seq + 1;
        
        $leng_temp_no = strlen($next_seq);
        $no = 'RC' . implode('', $thn_bln_arr) . substr($no, $leng_temp_no) . $next_seq;
        
        return $no;
    }

    function test()
    {
        echo "adads";
        if (false) {
            exit();
        }
        echo "lala";
    }
}
