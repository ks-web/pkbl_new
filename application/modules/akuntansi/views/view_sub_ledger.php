<?php
$app 	= 'Sub Ledger'; // nama aplikasi
$module = 'akuntansi';
$appLink = 'Sub_ledger'; // controller
$idField  = ''; //field key table
?>

<script>
	var url;
	var app = "<?=$app?>";
	var appLink = '<?=$appLink?>';
	var module = '<?=$module?>';
	var idField = '<?=$idField?>';
	

	function doDownloadExcel(){
		var dari = $('#tgl_awal').datebox('getValue');
		var sampai = $('#tgl_akhir').datebox('getValue');
		// var akun = $('#filter-akun').combogrid('getValue');
		
		if(dari && sampai){
			window.open('<?=base_url($module . '/' . $appLink . '/read_preview_excel')?>/' + dari + '/' + sampai, '_blank');
		} else {
			$.messager.alert('Tanggal Belum Dipilih','Tolong pilih tanggal!','error');
		}
	}
	function Exceldetail(){
		var dari = $('#tgl_awal').datebox('getValue');
		var sampai = $('#tgl_akhir').datebox('getValue');
		// var akun = $('#filter-akun').combogrid('getValue');
		
		if(dari && sampai){
			window.open('<?=base_url($module . '/' . $appLink . '/read_preview_excel2')?>/' + dari + '/' + sampai, '_blank');
		} else {
			$.messager.alert('Tanggal Belum Dipilih','Tolong pilih tanggal!','error');
		}
	}
	

	function prev_sub_ledger()
	{
		var tgl_awal 	= $('#tgl_awal').datebox('getValue');
		var tgl_akhir 	= $('#tgl_akhir').datebox('getValue');
		if(tgl_awal == '' || tgl_akhir == '')
		{
			alert('Tanggal belum diisi');
		}
		else
		{
			url  = '<?=base_url($module . '/' . $appLink . '/prev_sub_ledger')?>/'+tgl_awal +'/'+ tgl_akhir;
			window.open(url,'_blank');
		}
	}

	function cari()
	{
		var tgl_awal 	= $('#tgl_awal').datebox('getValue');
		var tgl_akhir 	= $('#tgl_akhir').datebox('getValue');
		if(tgl_awal == '' || tgl_akhir == '')
		{
			alert('Tanggal belum diisi');
		}
		else
		{
			url  = '<?=base_url($module . '/' . $appLink . '/get_sub_ledger')?>/'+tgl_awal +'/'+ tgl_akhir;
			
			$('#dg-sub-ledger').datagrid({
				url:url
			});
		}
	}
	
</script>
 
<div class="tabs-container">                
	<table id="dg-sub-ledger" class="easyui-datagrid" 
	data-options="  
		singleSelect:'true', 
	    title:'<?=$app?>',
	    toolbar:'#toolbar',
	    iconCls:'icon-cog',
	    rownumbers:'true',  
	    idField:'<?=$idField?>', 
	    pagination:'true',
	    fitColumns:'true',
	    pageList: [10,20,30]
	    "
	>
	    <thead>
            <th field="id_mitra" width="100" sortable="true">No Dokumen</th>
            <th field="tgl_kontrak" width="100" sortable="true">Tgl Kontrak</th>
            <th field="nama" width="100" sortable="true">Nama Pemohon</th>
            <th field="nama_perusahaan" width="100" sortable="true">Nama Istansi</th>
            <th field="kota" width="100" sortable="true">Kota</th>
            <th field="angsuran_pokok" width="100" sortable="true" formatter="format_numberdisp" align="right">Nilai Pengajuan</th>
            <th field="nilai_bayar" width="100" sortable="true" formatter="format_numberdisp" align="right">Nilai Bayar</th>
            <th field="saldo_akhir" width="100" sortable="true" formatter="format_numberdisp" align="right">Saldo Akhir</th>
	    </thead>
	</table>
	
    <!-- Tombol Add, Edit, Hapus Datagrid -->
<div id="toolbar">  
    <table  align="center" style="padding: 0px; width: 99%;">
		
        <tr>
			<td>
			Dari tanggal : 
			<input class="easyui-datebox" name="tgl_awal" id="tgl_awal" style="width:150px;"> 
			Sampai tanggal : 
			<input class="easyui-datebox" name="tgl_akhir" id="tgl_akhir" style="width:150px;">
			<a href="javascript:void(0)" class="btn btn-small" onclick=" javascript:cari()"><i class="icon-search"></i>&nbsp;Cari</a>
			</td>
		</tr>
		<tr>
			<td><!-- <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick=" javascript:prev_sub_ledger()"><i class="icon-print"></i>&nbsp;Export Sub Ledger</a>&nbsp; --><a href="javascript:void(0)" class="btn btn-small btn-primary" iconCls: 'icon-search' onclick="doDownloadExcel()">&nbsp;Excel</a>&nbsp;<a href="javascript:void(0)" class="btn btn-small btn-primary" iconCls: 'icon-search' onclick="Exceldetail()">&nbsp;Excel Detail</a></td>
		</tr>
    </table>
</div>
        <!-- end tombol datagrid -->
</div>