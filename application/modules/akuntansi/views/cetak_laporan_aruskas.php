<!DOCTYPE html>
<html>
<head>

	<title>Laporan Keuangan</title>
	<style>
		div.content{
			width: 100%;
			/*height: 9;*/
			padding: 1px;
			border: 1px solid white;
			margin: 1px;

			/*background-color: yellow;*/
		}
		.bar{
			border-top: 1px solid black;
			
		}
		table.bar{
			border-top: 1px solid black;
			border-bottom: 1px solid black;
			font-size: 10;
			font-weight: bold;
		}
		ol{
			font-size: 9;
		}
		span{
			font-size: 10;
			font-weight: bold;
		}
	</style>
</head>
<body>
	<div class="content">
		<center>
			<b>PROGRAM  KEMITRAAN DAN BINA LINGKUNGAN</b> <br/>
			<b>LAPORAN ARUS KAS</b> <br/>
			<b>Periode <?=$tgl?></b>
			<h5> 
				(Disajikan dalam Rupiah, kecuali dinyatakan lain) 
				<p style="border-bottom: 1px solid black;"></p>
			</h5>
		</center>
		<?php
			$jumlah_km = array();
			$jumlah_bl = array();
			$total_km = array();
			$total_bl = array();
			function laporan_aruskas($data, $parent_group=0){
				$res = array();
				$data_satu =array();
				

				foreach ($data as $key => $value) {
					if ($value['id_laporan'] == 2 and $value['parent_laporan'] == $parent_group) {
						array_push($data_satu,$value);
					}
				}

				foreach ($data_satu as $key => $value) {
					$row['parent_laporan'] = $value['parent_laporan'];
					$row['node_laporan'] = $value['node_laporan'];
					$row['kode_account'] = $value['kode_account'];
	                $row['keterangan'] = $value['keterangan'];
	                $row['KM'] = $value['KM'];
	                $row['BL'] = $value['BL'];
	                $row['grup'] = $value['grup'];
	                $row['is_value'] = $value['is_value'];
	                $row['order_number'] = $value['order_number'];
	                $row['grup_subtotal'] = $value['grup_subtotal'];

	                $children = laporan_aruskas($data,$value['node_laporan']);
	                if (sizeof($children) > 0) {
	                    $row['children'] = $children;
	                }
	                array_push($res, $row);
				}
				
				return $res;

			}
			for ($i=1; $i < $split+1; $i++) { 
				foreach (laporan_aruskas($data) as $key => $grup) {
					foreach ($grup['children'] as $key => $parent){
						if($parent['order_number'] == $i){
							echo "<table width='100%'>";
								echo "<tr>";
									echo "<td>";
									echo "<span>".$parent['keterangan']."</span><br>";
									echo "</td>";

									echo "<td width='16%' align='right'>";
									echo "<span><u>KM</u></span>";
									echo "</td>";

									echo "<td  width='16%' align='right'>";
									echo "<span><u>BL</u></span>";
									echo "</td>";

									echo "<td  width='16%' align='right'>";
									echo "<span><u>Total</u></span>";
									echo "</td>";
								echo "</tr>";
							echo "</table>";
							echo "<ol>";
							foreach ($parent['children'] as $key => $header) {
								if($header['is_value'] != 1){
									echo "<li style='font-weight: bold;'>".$header['keterangan']."</li>";
									foreach ($header['children'] as $key => $akun) {
										echo "<table border='0' width='100%'>";
											echo "<tr>";
												echo "<td width='5%'>";
												echo $akun['kode_account'];
												echo "</td>";
												echo "<td>";
												echo $akun['keterangan'];
												echo "</td>";
												echo "<td width='17%' align='right'>";
												echo number_format($akun['KM'],2);
												echo "</td>";
												echo "<td width='17%' align='right'>";
												echo number_format($akun['BL'],2);
												echo "</td>";
												echo "<td width='17%' align='right'>";
												echo number_format($akun['KM']+$akun['BL'],2);
												echo "</td>";
											echo "</tr>";
										echo "</table>";
										array_push($jumlah_km, $akun['KM']);
										array_push($jumlah_bl, $akun['BL']);
									}
									echo "<br><br>";
								}else{	
								echo "<table border='0' width='100%'>";
									echo "<tr>";
										echo "<td width='5%'>";
										echo $header['kode_account'];
										echo "</td>";
										echo "<td>";
										echo $header['keterangan'];
										echo "</td>";
										echo "<td width='17%' align='right'>";
										echo number_format($header['KM'],2);
										echo "</td>";
										echo "<td width='17%' align='right'>";
										echo number_format($header['BL'],2);
										echo "</td>";
										echo "<td width='17%' align='right'>";
										echo number_format($header['KM']+$header['BL'],2);
										echo "</td>";
									echo "</tr>";
								echo "</table>";
								array_push($jumlah_km, $header['KM']);
								array_push($jumlah_bl, $header['BL']);
								}
							}
							echo "</ol><br/>";

						}
						
					}

				}
				echo "<table width='100%' class='bar'>";
					echo "<tr>";
						echo "<td>";
						echo 'Jumlah ';
						echo "</td>";

						echo "<td width='16%' align='right'>";
						echo number_format(array_sum($jumlah_km),2);
						echo "</td>";

						echo "<td  width='16%' align='right'>";
						echo number_format(array_sum($jumlah_bl),2);
						echo "</td>";

						echo "<td  width='16%' align='right'>";
						echo number_format(array_sum($jumlah_km)+array_sum($jumlah_bl),2);
						echo "</td>";
					echo "</tr>";
				echo "</table><br><br><br><br>";
				
				array_push($total_km,$jumlah_km);
				array_push($total_bl,$jumlah_bl);
				$jumlah_km = array();
				$jumlah_bl = array();
			}
			echo "<table width='100%' class='bar'>";
				echo "<tr>";
					echo "<td'>";
					echo 'Saldo Akhir Kas ';
					echo "</td>";

					echo "<td width='16%' align='right'>";
					echo number_format(array_sum($total_km[0])-array_sum($total_km[1]),2);
					echo "</td>";

					echo "<td  width='16%' align='right'>";
					echo number_format(array_sum($total_bl[0])-array_sum($total_bl[1]),2);
					echo "</td>";

					echo "<td  width='16%' align='right'>";
					echo number_format( (array_sum($total_km[0])-array_sum($total_km[1])) + (array_sum($total_bl[0])-array_sum($total_bl[1])),2);
					echo "</td>";
				echo "</tr>";
			echo "</table>";
		?>
	</div>
</body>
</html>