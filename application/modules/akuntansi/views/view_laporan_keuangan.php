<?php
$app = 'Laporan Keuangan'; // nama aplikasi
$module = 'akuntansi';
$appLink = 'laporan_keuangan'; // controller
$idField  = 'id_laporan'; //field key table
// $moduleref = '';
// $appLinkref = ''; // controller
?>

<script>
    var url;
    var app = "<?=$app?>";
    var appLink = '<?=$appLink?>';
    var module = '<?=$module?>';
    var idField = '<?=$idField?>';
    
    function add(){
        $('#dlg').dialog('open').dialog('setTitle','Tambah ' + app);
        $('#tbl').html('Save');
        $('#fm').form('clear');
        url = '<?=base_url($module . '/' . $appLink . '/create')?>';
        $('#group_laporan').combogrid({
            url:'<?=base_url('master/laporan_keuangan/getGroupLaporan')?>',
       });
        $('#parent_2').combogrid('disable');
        $('#parent_3').combogrid('disable');
    }

    function save(){
       $('#fm').form('submit',{
            url: url,
            onSubmit: function(){
                return $(this).form('validate');
            },
            success: function(result){
                var result = eval('('+result+')');
                if (result.success){
                    $('#dg').treegrid('reload');
                    $('#dlg').dialog('close');
                } else {
                 $.messager.alert('Error Message',result.msg,'error');
                }
            }
        });
    }

    function doSearch(value){
        $('#dg').treegrid('load',{    
            q:value  
        });
    }
    function tiktok(){
        var disable = $('#parent_2').combogrid('disable');
        var enable = $('#parent_2').combogrid('enable');
        if(disable){
            enable
        }else{
            disable
        }
           
    }
    function hapus(){
        var node = $('#dg').treegrid('getSelected');
        console.log(node);
    }
    function cetak(){
        $('#dlg').dialog('open');


    }
    $(document).ready(function(){

    }); 
    function bulanan(){
        $('#tahun').show();
        $('#bulan').show();
    } 
    function triwulan(){
        $('#tahun').show();
        $('#bulan').show();
    }       
    function tahunan(){
        $('#tahun').show();
        $('#bulan').hide();
    }
    function goToURL() {
        var radio = $("input[type='radio'][name='pilih']:checked");
      window.open(
          '<?=base_url($module . '/' . $appLink . '/cetak')?>/'+radio.val()+'/'+$('#bulan').val()+'/'+$('#tahun').val(),
          '_blank' // <- This is what makes it open in a new window.
        );
      
    }
</script>
 
<div class="tabs-container">                
    <table id="dg" title="Laporan Keuangan" class="easyui-treegrid" style="height: 500px"
            data-options="
                url: '<?=base_url($module . '/' . $appLink . '/getTree')?>',
                title:'<?=$app?>',
                toolbar:'#toolbar',
                rownumbers:'true',  
                idField:'node_laporan', 
                treeField:'keterangan',
                fitColumns:'true',
                state:'closed'
          
            ">
        <thead>
            <tr>
                <th field="keterangan" width="100" sortable="true">Keterangan</th>
                <th field="kode_account" width="20" sortable="true">Account</th>
            </tr>
        </thead>
    </table>
    <div id="toolbar">  
        <table align="center" style="padding: 0px; width: 99%;">
            <tr>
                <td>
                     <a href="#" onclick="cetak();" class="btn btn-small btn-primary"><i class="icon-search icon-large"></i>&nbsp;Cari Periode</a>
                </td>
                
                <td align="right">
                    <input class="easyui-searchbox" prompt="Silahkan Input Field" searcher="doSearch"  style="width:300px"/>
                </td>
            </tr>
        </table>  
    </div>
    <div id="btn">  
        <a href="javascript:void(0)" onclick="goToURL(); return false;" class="btn btn-small btn-success"><i class="icon-file icon-large"></i>&nbsp;Unduh</a>
    </div>  

    <div id="dlg" title="Pilih Periode" class="easyui-dialog" style="width:400px; height:300px; padding:10px" closed="true" buttons="#btn" >
        <form method="post">
          <label><input style="width: 50px" onclick="bulanan()" type="radio" name="pilih" value="1" checked> Bulanan </label>
          <label><input style="width: 50px" onclick="tahunan()" type="radio" name="pilih" value="2"> Tahunan</label>
          <label><input style="width: 50px" onclick="triwulan()" type="radio" name="pilih" value="3"> Triwulan</label>
          <br/>
          <table>
              <tr>
                  <td>Periode Laporan</td>
                  <td> : </td>
                  <td>  
                    <select style="width: 100px" name="bulan" id="bulan">
                      <option value="1">Januari</option>
                      <option value="2">Februari</option>
                      <option value="3">Maret</option>
                      <option value="4">April</option>
                      <option value="5">Mei</option>
                      <option value="6">Juni</option>
                      <option value="7">Juli</option>
                      <option value="8">Agustus</option>
                      <option value="9">September</option>
                      <option value="10">Oktober</option>
                      <option value="11">November</option>
                      <option value="12">Desember</option>
                    </select>
                  </td>
                  <td >  
                    <input id="tahun" style="width: 100px;height: 24px " type="number" name="thn" value="2017">
                  </td>
              </tr>
          </table>
        </form>

    </div>
</div>
