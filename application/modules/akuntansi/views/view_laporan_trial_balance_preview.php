<!DOCTYPE html>
<html>
	<head>
		<style>
			table {
				border-spacing: 0px;
				border-collapse: separate;
				font-size: 12px;
				/*page-break-inside: avoid;*/
			}

			table, th, td {
				border: 0px solid black;
			}
			html {
				margin: 30px 50px
			}
			.bordered, .bordered td {
				border: 1px solid black;
				font-size: 12px;
			}
			
			.bordered-catat{
				border: 1px solid black;
				font-size: 12px;
			}
			td.no_border_bot {
				border-top: 0px solid black;
				border-bottom: 0px solid black;
			}

			.pull-right {
				text-align: right;
			}
			.center {
				text-align: center;
			}

			.bold {
				font-weight: bold;
			}
			.corporate {
				color: red;
				font-size: 24px;
			}
			.corporate_sub {
				font-size: 12px;
			}
			.title {
				font-size: 18px;
			}
			.subtitle {
				font-size: 12px;
			}
			.red {
				color: red;
			}
		</style>
	</head>
	<body>
		<div style="width: 100%">
			<p class="center bold">
				<span class="title">LAPORAN TRIAL BALANCE</span>
			</p>
			<p class="bold">
				<span class="subtitle">PERIODE : <?=tanggal_angka($dari)?> - <?=tanggal_angka($sampai)?></span>
			</p>
		</div>
		<table style="width: 100%" class="bordered">
			<tr>
				<td rowspan="2" class="center" width="20">No.</td>
				<td rowspan="2" class="center" width="40">Kode Akun</td>
				<td rowspan="2" class="center">Nama Akun</td>
				<td colspan="2" class="center">Transaksi</td>
				<td colspan="2" class="center">Saldo</td>
			</tr>
			<tr>
				<td class="center" width="70">Debet</td>
				<td class="center" width="70">Credit</td>
				<td class="center" width="80">Nilai</td>
				<td class="center" width="20">D/C</td>
			</tr>
			<?php 
			if(count($data_detail) >0){
				$no = 1;
				$total_debet = 0;
				$total_kredit = 0;
				foreach ($data_detail as $row) {
					$total_debet += $row['debet'];
					$total_kredit += $row['kredit'];
					$saldo = $row['debet'] - $row['kredit'];
					
					if($saldo < 0){
						$saldo = $saldo * -1;
						$tipe_saldo = "C";
					} else if($saldo > 0){
						$tipe_saldo = "D";
					}
					else {
						$tipe_saldo = "<br>";
					}
					?>
			<tr>
				<td class="center" width="20"><?=$no?></td>
				<td class="center" width="40"><?=$row['kode_account']?></td>
				<td><?=$row['uraian']?></td>
				<td class="pull-right" width="70"><?=number_format($row['debet'], 0,",",".")?></td>
				<td class="pull-right" width="70"><?=number_format($row['kredit'], 0,",",".")?></td>
				<td class="pull-right" width="80"><?=number_format($saldo, 0,",",".")?></td>
				<td class="center"width="20"><?=$tipe_saldo?></td>
			</tr>
					<?php
					$no++;
				}
			}
			?>
			<tr>
				<td colspan="3" class="bold pull-right">Jumlah</td>
				<td class="pull-right" width="70"><?=number_format($total_debet, 0,",",".")?></td>
				<td class="pull-right" width="70"><?=number_format($total_kredit, 0,",",".")?></td>
				<td class="pull-right" width="80"></td>
				<td class="pull-right" width="20"></td>
			</tr>
		</table>
		<!-- <table style="width: 100%;">
			<tbody>
				<tr>
					<td>
					<!-- <table style="width: 100%;">
						<tr>
							<td width="70px"><img  src='<?=base_url('assets/images/logo_KS.png') ?>' style="width: 50%;"></td>
							<td><span class="corporate bold">PT. KRAKATAU STEEL</span>
							<br />
							<span class="corporate_sub bold">DIVISI COMMUNITY DEVELOPMENT</span></td>
							<td width="200px"><span class="bold"><u>PERMINTAAN UANG MUKA</u></span>
							<br />
							<span class="bold">NOMOR:</span></td>
						</tr>
					</table> --
					<table style="width: 100%;">
						<tr>
							<td class="title bold center">LAPORAN TRIAL BALANCE</td>
						</tr>
						<tr>
							<td class="subtitle bold">PERIODE : <?=tanggal_angka($dari)?> - <?=tanggal_angka($sampai)?></td>
						</tr>
					</table><br /><br /><br />
					
					</td>
				</tr>
			</tbody>
		</table> -->
	</body>
</html>
