<?php
$app = 'Laporan Arus Kas'; // nama aplikasi
$module = 'akuntansi';
$appLink = 'laporan_aruskas'; // controller
$idField  = 'id_laporan'; //field key table
// $moduleref = '';
// $appLinkref = ''; // controller
?>

<script>
    var url;
    var app = "<?=$app?>";
    var appLink = '<?=$appLink?>';
    var module = '<?=$module?>';
    var idField = '<?=$idField?>';

    function doSearch(value){
        $('#dg').treegrid('load',{    
            q:value  
        });
    }

    $(function(){
      $('#filter-dari').datebox({
    
      });
    });

    function doFilter(){
      var dari = $('#filter-dari').datebox('getValue');
      
      if(dari){
        $('#dg').datagrid({    
          url: '<?=base_url($module . '/' . $appLink . '/aruskas')?>/'+dari
        });
      } else {
        $.messager.alert('Tanggal Belum Dipilih','Tolong pilih tanggal!','error');
      }
    }
    function doDownload(){
      var dari = $('#filter-dari').datebox('getValue');
      
      if(dari){
        window.open('<?=base_url($module . '/' . $appLink . '/read_preview')?>/' + dari, '_blank');
      } else {
        $.messager.alert('Tanggal Belum Dipilih','Tolong pilih tanggal!','error');
      }
    }
    function doDownloadExcel(){
      var dari = $('#filter-dari').datebox('getValue');
      
      if(dari){
        window.open('<?=base_url($module . '/' . $appLink . '/cetak_excel')?>/' + dari, '_blank');
      } else {
        $.messager.alert('Tanggal Belum Dipilih','Tolong pilih tanggal!','error');
      }
    }
</script>
 
<div class="tabs-container">                
    <table id="dg" class="easyui-datagrid" 
	data-options="  
	   
		singleSelect:'true', 
	    title:'<?=$app?>',
	    toolbar:'#toolbar',
	    iconCls:'icon-cog',
	    rownumbers:'true',  
	    idField:'<?=$idField?>', 
	    
	    fitColumns:'true',
	    pageList: [10,20,30]
	    "
	>
	    <thead>
        <th field="kode_account" width="50">Kode Akun</th>
        <th field="keterangan" width="200">Keterangan</th>
        <th align="right" field="BL" width="100" formatter="format_numberdisp">Bina Lingkungan</th>
        <th align="right" field="KM" width="100" formatter="format_numberdisp">Kemitraan</th>
	    </thead>
	</table>
    <div id="toolbar">  
        Tanggal : <input id="filter-dari" style="width:110px">
        <a href="javascript:void(0)" class="easyui-linkbutton" onclick="doFilter()"><i class="icon-search"></i>&nbsp;Cari</a>
        <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="doDownload()"><i class="icon-download-alt"></i>&nbsp;Unduh</a>
        <a href="javascript:void(0)" class="btn btn-small btn-success" onclick="doDownloadExcel()"><i class="icon-download-alt"></i>&nbsp;Excel</a>      
    </div>
    
</div>
