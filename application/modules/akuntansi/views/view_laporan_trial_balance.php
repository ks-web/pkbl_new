<?php
$app = 'Laporan Trial Balance'; // nama aplikasi
$module = 'akuntansi';
$appLink = 'laporan_trial_balance'; // controller
$idField  = 'id_akun'; //field key table
?>

<script>
	var url;
	var app = "<?=$app?>";
	var appLink = '<?=$appLink?>';
	var module = '<?=$module?>';
	var idField = '<?=$idField?>';
	
	function doSearch(value){
	    $('#dg').datagrid('load',{    
	    	q:value  
	    });
	}
	
	$(function(){
		$('#filter-dari').datebox({
			onSelect : function(date){
				$('#filter-sampai').datebox().datebox('calendar').calendar({
		            validator: function(date1){
		                return date1>=date;
		            }
		        });
			}
		});
		$('#filter-sampai').datebox({
			
		});
    });
	
	function doFilter(){
		var dari = $('#filter-dari').datebox('getValue');
		var sampai = $('#filter-sampai').datebox('getValue');
		var akun = $('#filter-akun').combogrid('getValue');
		
		if(dari && sampai){
			$('#dg').datagrid('load',{    
		    	dari:dari, 
		    	sampai:sampai,
		    	akun:akun
		    });
		} else {
			$.messager.alert('Tanggal Belum Dipilih','Tolong pilih tanggal!','error');
		}
	}
	
	function doDownload(){
		var dari = $('#filter-dari').datebox('getValue');
		var sampai = $('#filter-sampai').datebox('getValue');
		
		if(dari && sampai){
			window.open('<?=base_url($module . '/' . $appLink . '/read_preview')?>/' + dari + '/' + sampai, '_blank');
		} else {
			$.messager.alert('Tanggal Belum Dipilih','Tolong pilih tanggal!','error');
		}
	}
	
	function doDownloadExcelLaporan(){
		var dari = $('#filter-dari').datebox('getValue');
		var sampai = $('#filter-sampai').datebox('getValue');
		
		if(dari && sampai){
			window.open('<?=base_url($module . '/' . $appLink . '/read_preview_excel_laporan')?>/' + dari + '/' + sampai, '_blank');
		} else {
			$.messager.alert('Tanggal Belum Dipilih','Tolong pilih tanggal!','error');
		}
	}
	
	function doDownloadExcel(){
		var dari = $('#filter-dari').datebox('getValue');
		var sampai = $('#filter-sampai').datebox('getValue');
		var akun = $('#filter-akun').combogrid('getValue');
		
		if(dari && sampai){
			window.open('<?=base_url($module . '/' . $appLink . '/read_preview_excel')?>/' + dari + '/' + sampai + '/' + akun, '_blank');
		} else {
			$.messager.alert('Tanggal Belum Dipilih','Tolong pilih tanggal!','error');
		}
	}
</script>
 
<div class="tabs-container">                
	<table id="dg" class="easyui-datagrid" 
	data-options="  
	    url: '<?=base_url($module . '/' . $appLink . '/read')?>',
		singleSelect:'true', 
	    title:'<?=$app?>',
	    toolbar:'#toolbar',
	    iconCls:'icon-cog',
	    rownumbers:'true',  
	    idField:'<?=$idField?>', 
	    pagination:'true',
	    fitColumns:'true',
	    pageList: [10,20,30]
	    "
	>
	    <thead>
	    	<th field="tanggal_cd" width="60">Tanggal</th>
	        <th field="id_cd_um" width="80">ID</th>
	        <th field="account_no" width="60">No Akun</th>
            <th field="uraian">Nama Akun</th>
            <th field="debet" width="100" align="right" formatter="format_numberdisp">Debet</th>
            <th field="kredit" width="100" align="right" formatter="format_numberdisp">Kredit</th>
	    </thead>
	</table>
	
	<!-- Toolbar Datagrid -->
   	<div id="toolbar" style="padding:2px 5px;">  
        Dari : <input id="filter-dari" style="width:110px">
        Sampai : <input id="filter-sampai" style="width:110px">
        Akun : 
        <input id="filter-akun" class="easyui-combogrid" style="width: 110px"
						data-options="panelHeight:200, panelWidth: 400,
						valueField:'',
						url:'<?=base_url($module . '/' . $appLink . '/get_akun')?>',
						idField:'kode_account',
						textField:'kode_account',
						mode:'remote',
						onSelect :function(indek,row)
						{},
						columns:[[
                       		{field:'kode_account',title:'Kode Akun',width:80},
                        	{field:'uraian',title:'Uraian',width:250}
                        ]]"/>
        <a href="javascript:void(0)" class="easyui-linkbutton" onclick="doFilter()"><i class="icon-search"></i>&nbsp;Cari</a>
        <a href="javascript:void(0)" class="btn btn-small btn-warning" iconCls: 'icon-search' onclick="doDownload()">&nbsp;PDF Laporan</a>
        <a href="javascript:void(0)" class="btn btn-small btn-primary" iconCls: 'icon-search' onclick="doDownloadExcelLaporan()">&nbsp;Excel Laporan</a>
        <a href="javascript:void(0)" class="btn btn-small btn-primary" iconCls: 'icon-search' onclick="doDownloadExcel()">&nbsp;Excel</a>
    </div>
    <!-- End Toolbar Datagrid -->
</div>