<!DOCTYPE html>
<html>
<head>

	<title>Laporan Keuangan</title>
	<style>
		div.content{
			width: 100%;
			/*height: 9;*/
			padding: 1px;
			border: 1px solid white;
			margin: 1px;

			/*background-color: yellow;*/
		}
		.bar{
			border-top: 1px solid black;
			
		}
		ul{
			font-size: 9;
		}
	</style>
</head>
<body>
	<div class="content">
		<center>
			<b> PROGRAM  KEMITRAAN DAN BINA LINGKUNGAN</b> <br/>
			<b>LAPORAN POSISI KEUANGAN</b> <br/>
			<b>Periode  
				<?php
					echo tanggal_display(date('Y-m-d', strtotime($tgl[0])),'3');
				?>
			</b>
			<h5> 
				(Disajikan dalam Rupiah, kecuali dinyatakan lain) 
				<p style="border-bottom: 1px solid black;"></p>
			</h5>
		</center>

		<table style="width: 100%;font-size: 12px;border-bottom: 1px solid black;" >
			<tr>
				<td><?php echo $tgl[2];?></td>
				<td style="width: 23%;text-align: right;">
					<?php echo date('d-m-Y', strtotime($tgl[0])); ?>
				</td>
				<td style="width: 17%;text-align: right;" >
					<?php echo date('d-m-Y', strtotime($tgl[1])); ?>
				</td>
			</tr>
		</table>
		<br>
		<br>
		<?php
		function laporan_poskeu($data, $parent_group=0){
			$res = array();
			$data_satu =array();

			foreach ($data as $key => $value) {
				if ($value['id_laporan'] == 1 and $value['parent_laporan'] == $parent_group) {
					array_push($data_satu,$value);
				}
			}

			foreach ($data_satu as $key => $value) {
				$row['parent_laporan'] = $value['parent_laporan'];
				$row['node_laporan'] = $value['node_laporan'];
				$row['kode_account'] = $value['kode_account'];
                $row['keterangan'] = $value['keterangan'];
                $row['debet'] = $value['debet'];
                $row['kredit'] = $value['kredit'];
                $row['saldo'] = $value['saldo'];
                $row['subtotal'] = $value['subtotal'];
                $row['grup'] = $value['grup'];

                $children = laporan_poskeu($data,$value['node_laporan']);
                if (sizeof($children) > 0) {
                    $row['children'] = $children;
                }
                array_push($res, $row);
			}
			
			return $res;

		}
		?>

		<?php
		$banding = laporan_poskeu($banding);
		$saldo2 =array();
		$subtotal2 =array();
		$urut = 0;
		$urut2 = 0;
		$total = 0;
		$total2 = 0;

		foreach ($banding as $key => $grup) {
			foreach ($grup['children'] as $key => $parent) {
				foreach ($parent['children'] as $key => $header) {
					array_push($subtotal2,$header);
					foreach ($header['children'] as $key => $akun2) {
						array_push($saldo2, $akun2);
					}
				}
			}
		}

		for ($i=1; $i < $split+1; $i++) { 
			foreach (laporan_poskeu($data) as $key => $grup) {
				foreach ($grup['children'] as $key => $parent) {
					if($parent['grup'] == $i){
						// echo $parent['grup'];
					
					echo $parent['keterangan'];
					echo "<ul>";
						foreach ($parent['children'] as $key => $header) {
							echo "<li>".$header['keterangan']."</li>";
							
							foreach ($header['children'] as $key => $akun) {
									echo "<table style='width:100%'><tr>";

									echo "<td style='width:10%'>".$akun['kode_account']."</td><td>"
											   .$akun['keterangan']."</td><td style='width:20%' align='right'>"
											   .number_format($akun['saldo'],2).
										 "</td>";

									echo "<td style='width:20%' align='right'>";
										  	echo $saldo2 ? number_format($saldo2[$urut]['saldo'],2) : number_format(0,2);
									echo "</td></tr></table>";

									$urut++;
							}
							echo "<table style='width:100%' class='bar'>
									<tr><td align='right'>".number_format($header['subtotal'],2)."</td>";
							echo "<td align='right' style='width:19.9%'>";
							echo $subtotal2 ? number_format($subtotal2[$urut2]['subtotal'],2) : number_format(0,2);
							echo "</td></tr></table>";
							
							$urut2++;
							$total +=$header['subtotal'];
						}
					echo "</ul>";
					}
				}
			}
		

			foreach ($subtotal2 as $key => $value) {
				
				if($value['grup'] == $i){
					$total2 +=$value['subtotal'];
				}
				
			}
			echo "<table style='width:100%;border-top: 1px solid black;border-bottom: 1px solid black;'>";
				echo "<tr>";
					echo "<td><b>Jumlah </b></td>";
					echo "<td style='width:20%;text-align:right;'><b>";
						echo number_format($total,2);
						$total =0;
					echo "</b></td>";

					echo "<td style='width:20%;text-align:right;'><b>";
						echo number_format($total2,2);
						$total2 =0;
					echo "</b></td>";

				echo "</tr>";
			echo "</table>";
			echo "<div style='height: 100px'></div>";
		}
		?>
	</div>
</body>
</html>