<?php
	function format_number_add_mod_minus($value) {
		$str = '';
		$minus = false;
		if ($value < 0) {
			$minus = true;
			$value = $value * -1;
		}

		if ($value != '') {
			$str = number_format($value, 0, ",", ".");
		} else {
			$str = '&nbsp;';
		}
		if ($minus) {
			$str = '(' . $str . ')';
		}
		return $str;
	}

	function draw($tree, $first = 0) {
		if ($first == 0) {
			$first = 1;
			echo "<ul style='padding-left:0px;'>";
		} else if ($first == 1) {
			$first = 2;
			echo "<ul style='padding-left:5px;'>";
		} else {
			$first = 2;
			echo "<ul style='padding-left:18px;'>";
		}
		foreach ($tree as $key => $value) {
			if ($value['is_value'] == '1') {
				$nilai = format_number_add_mod_minus($value['nilai']);
				$nilai_netto = format_number_add_mod_minus($value['nilai_netto']);

				echo "<table class='' width='100%'>";
				echo "<tr>";
				echo "<td width='50px' valign='top'>" . $value['kode_account'] . "</td>";
				echo "<td>" . $value['keterangan'] . "</td>";
				echo "<td width='130px' align='right'>" . $nilai . "</td>";
				echo "<td width='130px' align='right'>" . $nilai_netto . "</td>";
				echo "</tr>";
				echo "</table>";
			} else if ($value['grup_subtotal'] !== NULL) {
				$nilai = format_number_add_mod_minus($value['nilai']);
				$nilai_netto = format_number_add_mod_minus($value['nilai_netto']);

				echo "<table class='' width='100%'>";
				echo "<tr>";
				echo "<td>" . $value['keterangan'] . "</td>";
				echo "<td width='130px' align='right'>" . $nilai . "</td>";
				echo "<td width='130px' align='right'>" . $nilai_netto . "</td>";
				echo "</tr>";
				echo "</table>";
			} else {
				if ($value['parent_laporan'] != '0') {
					echo "<li style='list-style:none;'>";
					echo $value['keterangan'];
					if ($value['kode_account'])
						echo " -- " . $value['kode_account'];
					echo "</li>";
				}
			}
			if (isset($value['child'])) {
				draw($value['child'], $first);
			}
		}
		echo "</ul>";
	}
?>
<!DOCTYPE html>
<html>
<head>

	<title>Laporan Aktivitas</title>
	<style>
		table {
			border-spacing: 0px;
			border-collapse: separate;
			font-size: 12px;
			/*page-break-inside: avoid;*/
		}

		table, th, td {
			border: 0px solid black;
		}
		html {
			margin: 30px 50px
		}
		.bordered {
			font-size: 12px;
		}
		.bordered, .bordered td{
			border: 1px solid black;
			
		}
		.bordered-mod {
			/*border: 4px solid black;*/
			border-top: 4px solid black;
			border-top-style: double;
			
			
			border-bottom: 4px solid black;
			border-bottom-style: double;
		}
		div.content{
			width: 100%;
			/*height: 9;*/
			padding: 1px;
			border: 1px solid white;
			margin: 1px;

			/*background-color: yellow;*/
		}
		.bar{
			border-top: 1px solid black;
			
		}
		ul{
			font-size: 12px;
		}
		.pull-right {
			text-align: right;
		}
		.center {
			text-align: center;
		}

		.bold {
			font-weight: bold;
		}
		.corporate {
			color: red;
			font-size: 24px;
		}
		.corporate_sub {
			font-size: 12px;
		}
		.title {
			font-size: 18px;
		}
		.subtitle {
			font-size: 12px;
		}
		.red {
			color: red;
		}
	</style>
</head>
<body>
	<div class="content">
		<center>
			<b>PROGRAM  KEMITRAAN DAN BINA LINGKUNGAN</b><br/>
			<b>LAPORAN AKTIVITAS</b> <br/>
			<b>Periode yang berakhir pada tanggal <?=tanggal_display(date('Y-m-d', strtotime($sampai)),'3')?></b>
			<h5>
				(Disajikan dalam Rupiah, kecuali dinyatakan lain)
			</h5>
		</center>
		<table class="bordered-mod" style="width: 100%;">
			<tr>
				<td colspan="2">Keterangan</td>
				<td style="width: 130px;" align="center">Detail<br />Rupiah</td>
				<td style="width: 130px;" align="center">Netto<br />Rupiah</td>
			</tr>
		</table>
		<?php
		draw($data_set);
		?>
	</div>
</body>
</html>