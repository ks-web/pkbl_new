<?php
$app = 'Laporan Aktivitas'; // nama aplikasi
$module = 'akuntansi';
$appLink = 'laporan_aktivitas'; // controller
$idField  = 'node_laporan'; //field key table
?>

<script>
	var url;
	var app = "<?=$app?>";
	var appLink = '<?=$appLink?>';
	var module = '<?=$module?>';
	var idField = '<?=$idField?>';
	
	function doSearch(value){
	    $('#dg').datagrid('load',{    
	    	q:value  
	    });
	}
	
	$(function(){
		$('#filter-tanggal').datebox({
			onSelect : function(date){
				// $('#filter-sampai').datebox().datebox('calendar').calendar({
		            // validator: function(date1){
		                // return date1>=date;
		            // }
		        // });
			}
		});
    });
	
	function doDownload(){
		var tanggal = $('#filter-tanggal').datebox('getValue');
		if(tanggal){
			window.open('<?=base_url($module . '/' . $appLink . '/read_preview')?>/' + tanggal, '_blank');
		} else {
			$.messager.alert('Tanggal Belum Dipilih','Tolong pilih tanggal!','error');
		}
	}
</script>
 
<div class="tabs-container">                
	<table id="dg" class="easyui-treegrid" style="height: 500px"
	data-options="  
	    url: '<?=base_url($module . '/' . $appLink . '/getTree')?>',
	    title:'<?=$app?>',
	    toolbar:'#toolbar',
	    rownumbers:'true',
	    idField:'<?=$idField?>',
	    fitColumns:'true', 
	    treeField:'keterangan'
	    "
	>
	    <thead>
	    	<th field="keterangan" width="100" sortable="true">Keterangan</th>
            <th field="kode_account" width="20" sortable="true">Account</th>
	    </thead>
	</table>
	
	<!-- Toolbar Datagrid -->
   	<div id="toolbar" style="padding:2px 5px;">  
        Tanggal : <input id="filter-tanggal" style="width:110px">
        <a href="javascript:void(0)" class="btn btn-small btn-warning" iconCls: 'icon-search' onclick="doDownload()">&nbsp;Unduh</a>
    </div>
    <!-- End Toolbar Datagrid -->
</div>