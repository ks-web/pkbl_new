
<!DOCTYPE html>
<html>
<head>
	<title>BAST Jaminan Mitra<br>PT Krakatau Steel Tbk</title>
	<style>

	.judul 
	{
		font-family: Times;
		font-size: 28px;
		font-style: normal;
		font-weight: bold;
		text-decoration: underline;
		text-transform: none;
		color: #000000;
		background-color: #ffffff;
	}
		
	.dibawah_judul
	{
		font-family: Arial;
		font-size: 16px;
		font-style: normal;
		font-weight: normal;
		text-decoration: none;
		text-transform: none;
		color: #000000;
		background-color: #ffffff;
	}
	
	.teks
	{
		font-family: Arial;
		font-size: 14px;
		font-style: normal;
		font-weight: normal;
		text-decoration: none;
		text-transform: none;
		color: #000000;
		background-color: #ffffff;
	}
	
	u {    
    border-bottom: 1px dotted #000;
    text-decoration: none;
}
		div, .hr{

		    background-color: white;
		    /*width: 100%;*/
		    border: 0px solid black;
		    padding: 25px;
		    margin: 1px;
		    font-size: 10;
			padding-bottom:2px;
			padding-top: 0px;

		}
		hr{
		    background-color: white;
		    /*width: 100%;*/
		    border: 0px solid black;
		    padding: 0px;
		    margin: 0px;
		}
		h3, .title{
			text-align: center;
		}	
		table{
		    /*width: 100%;*/
		    border: 0px solid black;
		    border-collapse: collapse;
		    padding: 5px;
		    /*padding-bottom: 5px;*/
		}
		table,td,tr{
		    border: 0px solid black;
		    border-collapse: collapse;
		    padding: 1px;
			/*padding-bottom:2px;*/

		}	
					
	</style>
</head>
<body>
	PT. Krakatau Steel<br>
	Dinas Keuangan PKBL
	<center><h2 style="text-decoration:underline;">Sub Ledger Setoran</h2><br><br>
	<table style="border-collapse:collapse;border-spacing:0;width: 100%; margin-left: auto; margin-right: auto; ">
		<tr>
			<th style="font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;vertical-align:center" ><strong>NO</strong></th>
			<th style="font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;vertical-align:center" ><strong>TANGGAL CR</strong></th>
			<th style="font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;vertical-align:center" ><strong>NO DOKUMEN</strong></th>
			<th style="font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;vertical-align:center" ><strong>NO CR</strong></th>
			<th style="font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;vertical-align:center" ><strong>KETERANGAN</strong></th>
			<th style="font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;vertical-align:center" ><strong>SALDO<br>AWAL</strong></th>
			<th style="font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;vertical-align:center" ><strong>DEBET</strong></th>
			<th style="font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;vertical-align:center" ><strong>CREDIT</strong></th>
			<th style="font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;vertical-align:center" ><strong>SALDO<br>AKHIR</strong></th>
		</tr>
		<?php 
		$no = 1;
		foreach($data_ledger as $row)
        { 
		?>
		<tr>
			<td style="font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;vertical-align:center" ><?php echo $no++; ?></th>
			<td style="font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;vertical-align:center" ><?php echo $row->tanggal_angsuran; ?></th>
			<td style="font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;vertical-align:center" ><?php echo $row->nama; ?></th>
			<td style="font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;vertical-align:center" ><?php echo $row->id_angsuran; ?></th>
			<td style="font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;vertical-align:center" ><?php echo $row->nama; ?></th>
			<td style="font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;vertical-align:center" ><?php echo "Rp. ". number_format($row->jumlah,0,'.',','); ?></th>
			<td style="font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;vertical-align:center" ><?php echo "Rp. ". number_format('0',0,'.',',') ?></th>
			<td style="font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;vertical-align:center" ><?php echo "Rp. ". number_format('0',0,'.',',') ?></th>
			<td style="font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;vertical-align:center" ><?php echo "Rp. ".number_format($row->jumlah,0,'.',','); ?></th>
		</tr>
		<?php
		}
		?>
	</table>
	</center>
	<?php //echo $data_ledger->nama; ?>
</body>
</html>