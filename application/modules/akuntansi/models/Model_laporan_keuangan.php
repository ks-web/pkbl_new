<?php
    if (!defined('BASEPATH'))
        exit('No direct script access allowed');

    Class Model_laporan_keuangan extends MY_Model {
        function __construct() {
            parent::__construct();
            $this->_table = "sys_group_link_laporan"; // for insert, update, delete
            $this->_view = "akuntansi_laporan_keuangan_vd"; // for call view akuntansi_laporan_keuangan_vd
            $this->_order = 'ASC';
            $this->_sort = 'kode_account';
            $this->_page = 1;
            $this->_rows = 10;

            $this->_create = true;
            $this->_update = true;

            if ($this->uri->segment(4)) {
                $this->_filter = array('Node_group' => $this->uri->segment(4));
            }

            //parameter from post/get - search function
            if ($this->input->post('q')) {
                $this->_like = array(
                    'Node_group' => $this->input->post('q')
                );
            }

            // $this->_param = array('Node_group' => $this->input->post('Node_group'));

            //data array for input to database
           
            $this->_data = array(

            );
            
        }
       function getTree($parent_group=0)
        {
            $res = array();
            $where_add['id_laporan'] = 1;
            $where_add['parent_laporan'] = $parent_group;
            $this->db->group_by('node_laporan');
            $read = $this->read($where_add);
            $tmp = json_decode($read);

            foreach ($tmp->rows as $row) {
                $row->id = $row->node_laporan;
                $row->text = $row->keterangan;

                $children = $this->getTree($row->node_laporan);
                if (sizeof($children) > 0) {
                    $row->children = $children;
                    $row->state = 'opened';
                }
                array_push($res, $row);
            }

            return $res;
        }
    }
