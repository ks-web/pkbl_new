<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	Class Model_sub_ledger extends MY_Model {
		function __construct() {
			parent::__construct();
			$this->_table = "tblingkungan"; // for insert, update, delete
			$this->_view = "tblingkungan_vd"; // for call view
			$this->_order = 'asc';
			$this->_sort = 'id_bl';
			$this->_page = 1;
			$this->_rows = 10;

			$this->_create = true;
			$this->_update = true;

			if ($this->uri->segment(4)) {
				$this->_filter = array('id_bl' => $this->uri->segment(4));
			}

			//parameter from post/get - search function
			if ($this->input->post('q')) {
				$this->_like = array(
					'id_bl' => $this->input->post('q'),
					'tanggal_input' => $this->input->post('q'),
					'nama' => $this->input->post('q')
				);
			}

			$this->_param = array('id_bl' => $this->input->post('id_bl'));

			//data array for input to database
			$this->_data = array(
				
			);
			
		}
		
		function get_sub_ledger($tgl_awal, $tgl_akhir)
		{
			// $this->db->select('tbl_angsuran.*, tmitra.nama');
			$this->db->where('tanggal_angsuran >= ', $tgl_awal);
			$this->db->where('tanggal_angsuran <= ', $tgl_akhir);
			// $this->db->join('tmitra', 'tmitra.id_mitra = tbl_angsuran.id_mitra');
			return $this->db->get('get_sub_ledger_vd')->result();
		}
		
		function prev_sub_ledger($tgl_awal, $tgl_akhir)
		{
			
			$this->db->select('tbl_angsuran.*, tmitra.nama');
			$this->db->where('tanggal_angsuran >= ', $tgl_awal);
			$this->db->where('tanggal_angsuran <= ', $tgl_akhir);
			$this->db->join('tmitra', 'tmitra.id_mitra = tbl_angsuran.id_mitra');
			return $this->db->get("tbl_angsuran");
			
		}
		public function get_data_for_excel($get) {
			if (sizeof($get) > 0) {
				$this->db->where($get);
			}
			
			$result = $this->db->get('get_sub_ledger_vd');
			$data = $result->result_array();
			return $data;
		}
		public function get_data_for_excel2($get) {
			if (sizeof($get) > 0) {
				$this->db->where($get);
			}
			
			$result = $this->db->get('get_detail_sub_ledger_vd');
			$data = $result->result_array();
			return $data;
		}
		
	}
