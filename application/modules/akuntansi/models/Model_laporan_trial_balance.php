<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	Class Model_laporan_trial_balance extends MY_Model {
		function __construct() {
			parent::__construct();
			$this->_table = "laporan_trial_balance_akun_vd";
			// for insert, update, delete
			// $this->_view = "view_dummy"; // for call view
			$this->_order = 'desc';
			$this->_sort = 'tanggal_cd';
			$this->_page = 1;
			$this->_rows = 10;

			$this->_create = true;
			$this->_update = true;

			if ($this->uri->segment(4)) {
				$this->_filter = array('id_akun' => $this->uri->segment(4));
			}

			//parameter from post/get - search function
			// if ($this->input->post('q')) {
			// $this->_like = array('id_cd_um' => $this->input->post('q'));
			// }

			// $this->_param = array('node' => $this->input->post('node'));

			//data array for input to database
			$this->_data = array();

		}

		public function get_akun($like = null) {
			$this->db->select('kode_account, uraian, tipe');
			$this->db->from('mas_account');

			$this->db->where('tipe IS NOT NULL');
			$this->db->group_start();
			$this->db->or_like(array(
				'kode_account' => $like,
				'uraian' => $like
			));
			$this->db->group_end();

			$query = $this->db->get();
			return $query->result_array();
		}

		public function get_data_for_excel($get) {
			if (sizeof($get) > 0) {
				$this->db->where($get);
			}
			
			$result = $this->db->get('laporan_trial_balance_akun_vd');
			$data = $result->result_array();
			return $data;
		}

	}
