<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	Class Model_laporan_aktivitas extends MY_Model {
		function __construct() {
			parent::__construct();
			$this->_table = "laporan_aktivitas_vd";
			// for insert, update, delete
			// $this->_view = "view_dummy"; // for call view
			$this->_order = 'desc';
			$this->_sort = 'tanggal_cd';
			$this->_page = 1;
			$this->_rows = 10;

			$this->_create = true;
			$this->_update = true;

			if ($this->uri->segment(4)) {
				$this->_filter = array('id_akun' => $this->uri->segment(4));
			}

			//parameter from post/get - search function
			// if ($this->input->post('q')) {
			// $this->_like = array('id_cd_um' => $this->input->post('q'));
			// }

			// $this->_param = array('node' => $this->input->post('node'));

			//data array for input to database
			$this->_data = array();

		}

		public function getTree($parent_group = 0) {
			$res = array();
			$where_add['id_laporan'] = 3;
			$where_add['parent_laporan'] = $parent_group;
			
			$this->db->where($where_add);
			$this->db->order_by('order_number', 'asc');
			$read = $this->db->get('sys_group_link_laporan');
			
			foreach ($read->result() as $row) {
				$temp_row = array();
				$temp_row['parent_laporan'] = $row->parent_laporan;
				$temp_row['node_laporan'] = $row->node_laporan;
				$temp_row['keterangan'] = $row->keterangan;
				$temp_row['kode_account'] = $row->kode_account;
				$temp_row['is_value'] = $row->is_value;

				$children = $this->getTree($row->node_laporan);
				if (sizeof($children) > 0) {
					$temp_row['children'] = $children;
					$temp_row['state'] = 'opened';
				}

				array_push($res, $temp_row);
			}
			// die();

			return $res;
		}
	}
