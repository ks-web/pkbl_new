<?php
    if (!defined('BASEPATH'))
        exit('No direct script access allowed');
    class Laporan_keuangan extends MY_Controller {
        public $models = array('laporan_keuangan');
        
        public function __construct() {
            parent::__construct();
        }

        public function index() {
            $data = array();
            $data['menu'] = $this->model_menu->getAllMenu();

            $this->template->load('template', 'view_laporan_keuangan', $data);
        }

        public function read() {
            $this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read());
        }

        public function read_all() {
            $this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read_all());
        }
        
        function getTree()
        {
            $data = $this->{$this->models[0]}->getTree();
            $this->output->set_output(json_encode($data));  
        }
        function cetak($period,$bulan,$tahun){    

          $date = $tahun.'-'.$bulan; 

          $d = date_create_from_format('Y-m',$date); 
          $last_day = date_format($d, 't');
          
          if($period == 1){ // bulanan
              $awal = $tahun.'-'.$bulan.'-'.$last_day;

              $date = $tahun.'-'.($bulan-1);
              $d = date_create_from_format('Y-m',$date); 
              $last_day = date_format($d, 't');
              $akhir = $tahun.'-'.($bulan-1).'-'.$last_day;
              $periode = "Bulanan";
          }
          if($period == 2){ // tahunan

              $date = $tahun.date('-m');
              $d = date_create_from_format('Y-m',$date); 
              $last_day = date_format($d, 't');

              $awal = $tahun.date('-m-').$last_day ;
              $akhir = ($tahun-1).'-12-31';
              $periode = "Tahunan";
          }
          if($period == 3){ // triwulan
              $awal = $tahun.'-'.$bulan.'-'.$last_day;

              $date = $tahun.'-'.($bulan-3);
              $d = date_create_from_format('Y-m',$date); 
              $last_day = date_format($d, 't');

               $akhir = $tahun.'-'.($bulan-3).'-'.$last_day;
              if( ($bulan-2) < 0 ){
                  $tahun_x = $tahun-1;
                  $bulan_x = 12+($bulan-3);
                  
                  $date = $tahun_x.'-'.($bulan_x-3);
                  $d = date_create_from_format('Y-m',$date); 
                  $last_day = date_format($d, 't');

                  $akhir = $tahun_x.'-'.$bulan_x.'-'.$last_day;
              }
             
              $periode = "Triwulan";
          }

          //load content html
          $data['data']     = $this->test($awal);
          $data['banding']  = $this->test($akhir);
          $data['split']  = $this->split();
          $data['tgl'] = array($awal,$akhir,$periode);

          $this->load->helper('tanggal_indo');
          $html = $this->load->view('cetak_laporan_keuangan', $data,true);
          // echo $html;
          // die();
          // create pdf using dompdf
          $this->load->library('dompdf_gen'); // Load library
          $this->dompdf->set_paper('A4', 'portrait'); // Setting Paper landscape portrait
          // Convert to PDF
          $this->dompdf->load_html($html);
          $this->dompdf->render();
          $canvas = $this->dompdf->get_canvas(); 
          $font = Font_Metrics::get_font("helvetica", ""); 
          $canvas->page_text(20, 830, "Halaman : {PAGE_NUM} dari {PAGE_COUNT}",$font, 8, array(0,0,0));

          $this->dompdf->stream("laporan_keuangan.pdf");
        }

        function split(){
          $this->db->reconnect();
          $data = $this->db->query('SELECT
                              COUNT(*) as split
                            FROM
                              (
                                SELECT
                                  A.grup
                                FROM
                                  sys_group_link_laporan A
                                WHERE
                                  A.id_laporan = 1
                                GROUP BY
                                  A.grup
                              ) as t')->result_array();
          return $data[0]['split'];
        }

        function test($tgl){
          //Error Number: 2014
          // Commands out of sync; you can't run this command now
          $this->db->reconnect();
          $data = $this->db->query('call sp_laporan_posekeu(\''.$tgl.'\')' )->result_array();

          return $data;
        }
        

    }
