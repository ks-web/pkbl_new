<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');
	class Sub_ledger extends MY_Controller {
		public $models = array('sub_ledger');
		
		public function __construct() {
			parent::__construct();
		}

		public function index() {
			$data = array();
			$data['menu'] = $this->model_menu->getAllMenu();

			$this->template->load('template', 'view_sub_ledger', $data);
		}


		public function get_sub_ledger($tgl_awal, $tgl_akhir)
		{
			// header("Content-Type: application/json");
			echo json_encode($this->{$this->models[0]}->get_sub_ledger($tgl_awal, $tgl_akhir));
		}
        
		function prev_sub_ledger($tgl_awal,$tgl_akhir)
		{   
			//echo $tgl_awal.$tgl_akhir;
			//echo json_encode($this->{$this->models[0]}->prev_sub_ledger($tgl_awal,$tgl_akhir)->result());
			
		    $data['data_ledger'] = $this->{$this->models[0]}->prev_sub_ledger($tgl_awal,$tgl_akhir)->result();
		    $html = $this->load->view('prev_sub_ledger',$data, true);
			
  			$this->load->library('dompdf_gen'); 
			$this->dompdf->set_paper('A4', 'landscape'); 
			$this->dompdf->load_html($html);
			$this->dompdf->render();
			$this->dompdf->stream("Report Sub Ledger.pdf");
			
    	}
    	public function read_preview_excel($dari = null, $sampai = null) {
			//load our new PHPExcel library
			$this->load->library('excel');
			//activate worksheet number 1
			$this->excel->setActiveSheetIndex(0);
			//name the worksheet
			$this->excel->getActiveSheet()->setTitle('Data Jurnal');
			//set cell A1 content with some text

			//Loop Heading
			$heading = array(
				'No Dokumen',
				'Tgl Kontrak',
				'Nama Pemohon',
				'Nama Istansi',
				'Kota',
				'Nilai Disetujui',
				'Nilai Bayar',
				'Saldo Akhir'

			);
			$rowNumberH = 1;
			$colH = 'A';
			foreach ($heading as $h) {
				$this->excel->getActiveSheet()->setCellValue($colH . $rowNumberH, $h);
				$colH++;
			}

			// load data
			if ($dari && $sampai) {
				$get['tanggal_angsuran >='] = $dari;
				$get['tanggal_angsuran <='] = $sampai;
			}
			
			$data = $this->{$this->models[0]}->get_data_for_excel($get);

			//Loop Result
			$totn = count($data);
			$maxrow = $totn + 1;
			$row = 2;
			$no = 1;
			foreach ($data as $n) {
				// echo var_dump($n);
			 // die();

				//$numnil = (float) str_replace(',','.',$n->nilai);
				$this->excel->getActiveSheet()->setCellValue('A'.$row,$n['id_mitra']);
				$this->excel->getActiveSheet()->setCellValue('B'.$row,$n['tgl_kontrak']);
				$this->excel->getActiveSheet()->setCellValue('C'.$row,$n['nama']);
				$this->excel->getActiveSheet()->setCellValue('D'.$row,$n['nama_perusahaan']);
				$this->excel->getActiveSheet()->setCellValue('E'.$row,$n['kota']);
				
				$this->excel->getActiveSheet()->setCellValueExplicit('F'.$row,$n['angsuran_pokok'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
				$this->excel->getActiveSheet()->setCellValueExplicit('G'.$row,$n['nilai_bayar'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
				$this->excel->getActiveSheet()->setCellValueExplicit('H'.$row,$n['saldo_akhir'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
				$this->excel->getActiveSheet()->getStyle('I' . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
				// $this->excel->getActiveSheet()->setCellValueExplicit('F' . $row, $n['kredit'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
				$row++;
				$no++;

	         // $this->excel->getActiveSheet()->setCellValue('G' . $row_jumlah, '=sum(G2:G'. $last_row .')');
			}
			$toreturn = array();
			$last_row = $row-1;
	         $row_jumlah = $row+2;
	         // $this->excel->getActiveSheet()->mergeCells('"A'.$row_jumlah':F".$row_jumlah');
	         $this->excel->getActiveSheet()->setCellValue('A' . $row_jumlah, 'Subtotal  ');
	         $this->excel->getActiveSheet()->setCellValue('F' . $row_jumlah, '=sum(F2:F'. $last_row .')');
	         $this->excel->getActiveSheet()->setCellValue('G' . $row_jumlah, '=sum(G2:G'. $last_row .')');
	         $this->excel->getActiveSheet()->setCellValue('H' . $row_jumlah, '=sum(H2:H'. $last_row .')');
           //Freeze pane
			//Freeze pane
			$this->excel->getActiveSheet()->freezePane('A2');

			$filename = 'Sub Ledger.xls';
			//save our workbook as this file name
			header('Content-Type: application/vnd.ms-excel');
			//mime type
			header('Content-Disposition: attachment;filename="' . $filename . '"');
			//tell browser what's the file name
			header('Cache-Control: max-age=0');
			//no cache

			//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007'
			// (and adjust the filename extension, also the header mime type)
			//if you want to save it as .XLSX Excel 2007 format
			$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
			//force user to download the Excel file without writing it to server's HD
			$objWriter->save('php://output');
		}

		///detail excel
		public function read_preview_excel2($dari = null, $sampai = null) {
			//load our new PHPExcel library
			$this->load->library('excel');
			//activate worksheet number 1
			$this->excel->setActiveSheetIndex(0);
			//name the worksheet
			$this->excel->getActiveSheet()->setTitle('Data Sub Ledger');
			//set cell A1 content with some text

			//Loop Heading
			$heading = array(
				'No Dokumen',
			    'Tgl Kontrak',
			    'Tgl Pembayaran',
				'Nama Pemohon',
				'Nama Istansi',
				'Kota',
				'Nilai Disetujui',
				'Nilai Bayar'

			);
			$rowNumberH = 1;
			$colH = 'A';
			foreach ($heading as $h) {
				$this->excel->getActiveSheet()->setCellValue($colH . $rowNumberH, $h);
				$colH++;
			}

			// load data
			if ($dari && $sampai) {
				$get['tanggal_angsuran >='] = $dari;
				$get['tanggal_angsuran <='] = $sampai;
			}
			
			$data = $this->{$this->models[0]}->get_data_for_excel2($get);

			//Loop Result
			$totn = count($data);
			$maxrow = $totn + 1;
			$row = 2;
			$no = 1;
			foreach ($data as $n) {
				//$numnil = (float) str_replace(',','.',$n->nilai);
			    $this->excel->getActiveSheet()->setCellValue('A'.$row,$n['idmitra']);
			    $this->excel->getActiveSheet()->setCellValue('B'.$row,$n['tanggal_kontrak']);
			    $this->excel->getActiveSheet()->setCellValue('C'.$row,$n['tanggal_angsuran']);
				$this->excel->getActiveSheet()->setCellValue('D'.$row,$n['nama']);
				$this->excel->getActiveSheet()->setCellValue('E'.$row,$n['nama_perusahaan']);
				$this->excel->getActiveSheet()->setCellValue('F'.$row,$n['kota']);
				
				$this->excel->getActiveSheet()->setCellValueExplicit('G'.$row,$n['angsuran_pokok'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
				$this->excel->getActiveSheet()->setCellValueExplicit('H'.$row,$n['jumlah'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
				$this->excel->getActiveSheet()->getStyle('I' . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
				$row++;
				$no++;
			}
			$toreturn = array();
			$last_row = $row-1;
	         $row_jumlah = $row+2;
	         // $this->excel->getActiveSheet()->mergeCells('"A'.$row_jumlah':F".$row_jumlah');
	         $this->excel->getActiveSheet()->setCellValue('A' . $row_jumlah, 'Subtotal  ');
	         $this->excel->getActiveSheet()->setCellValue('H' . $row_jumlah, '=sum(H2:H'. $last_row .')');
           //Freeze pane
			$this->excel->getActiveSheet()->freezePane('A2');

			$filename = 'Sub Ledger Detail.xls';
			//save our workbook as this file name
			header('Content-Type: application/vnd.ms-excel');
			//mime type
			header('Content-Disposition: attachment;filename="' . $filename . '"');
			//tell browser what's the file name
			header('Cache-Control: max-age=0');
			//no cache

			//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007'
			// (and adjust the filename extension, also the header mime type)
			//if you want to save it as .XLSX Excel 2007 format
			$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
			//force user to download the Excel file without writing it to server's HD
			$objWriter->save('php://output');
		}

	}
