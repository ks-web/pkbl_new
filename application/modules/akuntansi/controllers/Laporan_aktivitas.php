<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');
	class Laporan_aktivitas extends MY_Controller {
		public $models = array('laporan_aktivitas');

		public function __construct() {
			parent::__construct();
		}

		public function index() {
			$data = array();
			$data['menu'] = $this->model_menu->getAllMenu();

			$this->template->load('template', 'view_laporan_aktivitas', $data);
		}

		public function read() {
			// $dari = $this->input->post('dari');
			// $sampai = $this->input->post('sampai');
			// $akun = $this->input->post('akun');
			//
			// $get = array();
			// if ($dari && $sampai) {
			// $get['tanggal_cd >='] = $dari;
			// $get['tanggal_cd <='] = $sampai;
			// }
			// if ($akun) {
			// $get['account_no'] = $akun;
			// }
			//
			// $this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read($get));
		}

		public function read_all() {
			// $dari = $this->input->post('dari');
			// $sampai = $this->input->post('sampai');
			//
			// $get = array();
			// if ($dari && $sampai) {
			// $get['tanggal_cd >='] = $dari;
			// $get['tanggal_cd <='] = $sampai;
			// }
			// if ($akun) {
			// $get['account_no'] = $akun;
			// }
			//
			// $this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read_all($get));
		}

		public function read_preview($sampai = null) {
			if ($sampai) {
				$data_head = $this->{$this->models[0]}->getTree(0, $sampai);

				if (count($data_head) > 0) {
					$temp_date = explode('-', $sampai);
					$dari = $temp_date[0] . '-01-01';

					$data['dari'] = $dari;
					$data['sampai'] = $sampai;
					$data['data_head'] = $data_head;
					$data['data_detail'] = null;
					$data['data_set'] = null;

					$q = $this->db->query('call sp_laporan_aktivitas(\'' . $dari . '\',\'' . $sampai . '\')');
					if ($q->num_rows() > 0) {
						$data['data_detail'] = $q->result_array();
						// isi nilai netto
						$data['data_detail'] = $this->set_nilai_netto_laporan($data['data_detail']);
						// isi subtotal
						$data['data_detail'] = $this->set_subtotal_laporan($data['data_detail']);

						$data['data_set'] = $this->get_tree_laporan($data['data_detail']);
					}

					$this->load->helper('tanggal_indo');
					$this->load->helper('terbilang');

					$html = $this->load->view('view_laporan_aktivitas_preview', $data, true);

					$this->load->library('dompdf_gen');
					// Load library
					$this->dompdf->set_paper('A4', 'potrait');
					// Setting Paper
					// // Convert to PDF
					$this->dompdf->load_html($html);
					$this->dompdf->render();

					// setting footer pagenumber
					$canvas = $this->dompdf->get_canvas();
					$font = Font_Metrics::get_font("helvetica", "");
					$canvas->page_text(10, 830, "Halaman : {PAGE_NUM} dari {PAGE_COUNT}", $font, 8, array(
						0,
						0,
						0
					));

					$this->dompdf->stream("Laporan_Aktivitas-" . $dari . "_" . $sampai . ".pdf");
				} else {
					show_404();
				}
			}
		}

		public function create() {

		}

		public function update() {

		}

		public function delete() {

		}

		function getTree() {
			$data = $this->{$this->models[0]}->getTree();

			$this->output->set_output(json_encode($data));
		}

		public function get_tree_laporan($data, $parent = 0) {
			$this->load->library('arrayelement');
			
			$res = array();
			$indexs = array('parent_laporan' => $parent);
			$arr = $this->arrayelement->get($data, $indexs);
			if ($arr) {
				foreach ($arr as $row) {
					$temp_row = array();
					$temp_row['parent_laporan'] = $row['parent_laporan'];
					$temp_row['node_laporan'] = $row['node_laporan'];
					$temp_row['keterangan'] = $row['keterangan'];
					$temp_row['kode_account'] = $row['kode_account'];
					$temp_row['is_value'] = $row['is_value'];
					$temp_row['grup_subtotal'] = $row['grup_subtotal'];
					$temp_row['nilai'] = $row['nilai'];
					$temp_row['nilai_netto'] = $row['nilai_netto'];

					$children = $this->get_tree_laporan($data, $row['node_laporan']);
					if (sizeof($children) > 0) {
						$temp_row['child'] = $children;
					}

					array_push($res, $temp_row);
				}
			}

			return $res;
		}

		public function set_subtotal_laporan($data) {
			$this->load->library('arrayelement');
			
			$indexs = array('grup_subtotal' => '1');
			$arr = $this->arrayelement->get($data, $indexs);

			if ($arr) {
				foreach ($arr as $key => $value) {
					$indexs_nilai = array(
						'is_value' => '1',
						'grup' => $value['grup']
					);
					$arr_nilai = $this->arrayelement->get($data, $indexs_nilai);

					$nilai = 0;
					$nilai_netto = 0;
					if ($arr_nilai) {
						foreach ($arr_nilai as $row) {
							$nilai += $row['nilai'];
							$nilai_netto += $row['nilai_netto'];
						}
					}

					$data[$key]['nilai'] = (string)$nilai;
					$data[$key]['nilai_netto'] = (string)$nilai_netto;

				}
			}

			$indexs = array('grup_subtotal' => '2');
			$arr = $this->arrayelement->get($data, $indexs);

			if ($arr) {
				foreach ($arr as $key => $value) {
					$temp = explode(',', $value['nilai']);
					$nilai = 0;
					$nilai_netto = 0;

					foreach ($temp as $row_temp) {
						$indexs_temp = array('node_laporan' => $row_temp);
						$arr_temp = $this->arrayelement->get($data, $indexs_temp);

						if ($arr_temp) {
							foreach ($arr_temp as $key_temp => $value_temp) {
								$nilai += $value_temp['nilai'];
								$nilai_netto += $value_temp['nilai_netto'];
							}
						}
					}

					$data[$key]['nilai'] = (string)$nilai;
					$data[$key]['nilai_netto'] = (string)$nilai_netto;

				}
			}

			$indexs = array('grup_subtotal' => '3');
			$arr = $this->arrayelement->get($data, $indexs);

			if ($arr) {
				foreach ($arr as $key => $value) {
					$temp = explode(',', $value['nilai']);
					$nilai = null;
					$nilai_netto = null;

					foreach ($temp as $row_temp) {
						$indexs_temp = array('node_laporan' => $row_temp);
						$arr_temp = $this->arrayelement->get($data, $indexs_temp);

						if ($arr_temp) {
							foreach ($arr_temp as $key_temp => $value_temp) {
								if ($nilai === null) {
									$nilai = $value_temp['nilai'];
								} else {
									$nilai -= $value_temp['nilai'];
								}

								if ($nilai_netto === null) {
									$nilai_netto = $value_temp['nilai_netto'];
								} else {
									$nilai_netto -= $value_temp['nilai_netto'];
								}
							}
						}
					}

					$data[$key]['nilai'] = (string)$nilai;
					$data[$key]['nilai_netto'] = (string)$nilai_netto;
				}
			}
			return $data;
		}

		public function set_nilai_netto_laporan($data) {
			$this->load->library('arrayelement');
			
			foreach ($data as $key => $value) {
				if ($value['is_value'] == '1') {

					if ($value['nilai_netto'] != NULL) {

						$indexs = array('node_laporan' => $value['nilai_netto']);
						$arr = $this->arrayelement->get($data, $indexs);
						$key_data = null;
						if ($arr) {
							foreach ($arr as $key_net => $value_net) {
								if ($key_data == null) {
									$key_data = $key_net;
									break;
								}
							}
						}

						$temp = $value['nilai'] - $data[$key_data]['nilai'];
						$data[$key]['nilai_netto'] = (string)$temp;
						// set kosong untuk yang sudah di pakai
						$data[$key_data]['nilai_netto'] = '';
					}
				}
			}

			foreach ($data as $key => $value) {
				if ($value['is_value'] == '1') {
					if ($value['nilai_netto'] === NULL) {
						$data[$key]['nilai_netto'] = $value['nilai'];
					}
				}
			}
			return $data;
		}

	}
