<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');
	class Laporan_trial_balance extends MY_Controller {
		public $models = array('laporan_trial_balance');

		public function __construct() {
			parent::__construct();
		}

		public function index() {
			$data = array();
			$data['menu'] = $this->model_menu->getAllMenu();

			$this->template->load('template', 'view_laporan_trial_balance', $data);
		}

		public function read() {
			$dari = $this->input->post('dari');
			$sampai = $this->input->post('sampai');
			$akun = $this->input->post('akun');

			$get = array();
			if ($dari && $sampai) {
				$get['tanggal_cd >='] = $dari;
				$get['tanggal_cd <='] = $sampai;
			}
			if ($akun) {
				$get['account_no'] = $akun;
			}

			$this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read($get));
		}

		public function read_all() {
			$dari = $this->input->post('dari');
			$sampai = $this->input->post('sampai');

			$get = array();
			if ($dari && $sampai) {
				$get['tanggal_cd >='] = $dari;
				$get['tanggal_cd <='] = $sampai;
			}
			if ($akun) {
				$get['account_no'] = $akun;
			}

			$this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read_all($get));
		}

		public function read_preview($dari = null, $sampai = null) {
			if ($dari && $sampai) {
				$this->db->where('tanggal_cd >= ', $dari);
				$this->db->where('tanggal_cd <= ', $sampai);
				$check = $this->db->get('laporan_trial_balance_akun_vd');
				if ($check->num_rows() > 0) {
					$data['dari'] = $dari;
					$data['sampai'] = $sampai;

					$data['data_head'] = $check->row_array();
					$data['data_detail'] = null;

					$q = $this->db->query('call sp_laporan_trial_balance(\'' . $dari . '\',\'' . $sampai . '\')');
					if ($q->num_rows() > 0) {
						$data['data_detail'] = $q->result_array();
					}

					// kota tanggal
					$data['kota'] = 'Cilegon';
					$this->load->helper('tanggal_indo');
					$this->load->helper('terbilang');
					$data['tanggal'] = tanggal_display();

					$html = $this->load->view('view_laporan_trial_balance_preview', $data, true);

					$this->load->library('dompdf_gen');
					// Load library
					$this->dompdf->set_paper('A4', 'potrait');
					// Setting Paper
					// // Convert to PDF
					$this->dompdf->load_html($html);
					$this->dompdf->render();

					// setting footer pagenumber
					$canvas = $this->dompdf->get_canvas();
					$font = Font_Metrics::get_font("helvetica", "");
					$canvas->page_text(10, 830, "Halaman : {PAGE_NUM} dari {PAGE_COUNT}", $font, 8, array(
						0,
						0,
						0
					));

					$this->dompdf->stream("Laporan_Trial_Balance-" . $dari . "_" . $sampai . ".pdf");
				} else {
					show_404();
				}
			}
		}

		public function read_preview_excel_laporan($dari = null, $sampai = null) {
			if ($dari && $sampai) {
				$this->db->where('tanggal_cd >= ', $dari);
				$this->db->where('tanggal_cd <= ', $sampai);
				$check = $this->db->get('laporan_trial_balance_akun_vd');
				if ($check->num_rows() > 0) {
					$data['dari'] = $dari;
					$data['sampai'] = $sampai;

					$data['data_head'] = $check->row_array();
					$data['data_detail'] = null;

					$q = $this->db->query('call sp_laporan_trial_balance(\'' . $dari . '\',\'' . $sampai . '\')');
					if ($q->num_rows() > 0) {
						$data['data_detail'] = $q->result_array();
					}

					// kota tanggal
					$data['kota'] = 'Cilegon';
					$this->load->helper('tanggal_indo');
					$this->load->helper('terbilang');
					$data['tanggal'] = tanggal_display();

					//load our new PHPExcel library
					$this->load->library('excel');
					//activate worksheet number 1
					$this->excel->setActiveSheetIndex(0);
					//name the worksheet
					$this->excel->getActiveSheet()->setTitle('Data Jurnal');
					//set cell A1 content with some text

					$this->excel->getActiveSheet()->mergeCells('A1:G1');
					$this->excel->getActiveSheet()->setCellValue('A1', 'LAPORAN TRIAL BALANCE');
					$style = array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, ));
					$this->excel->getActiveSheet()->getStyle("A1:G1")->applyFromArray($style);

					$this->excel->getActiveSheet()->mergeCells('A2:G2');
					$this->excel->getActiveSheet()->setCellValue('A2', 'PERIODE : ' . tanggal_angka($dari) . ' - ' . tanggal_angka($sampai));

					$this->excel->getActiveSheet()->mergeCells('A4:A5');
					$this->excel->getActiveSheet()->setCellValue('A4', 'No.');

					$this->excel->getActiveSheet()->mergeCells('B4:B5');
					$this->excel->getActiveSheet()->setCellValue('B4', 'Kode Akun');

					$this->excel->getActiveSheet()->mergeCells('C4:C5');
					$this->excel->getActiveSheet()->setCellValue('C4', 'Nama Akun');

					$this->excel->getActiveSheet()->mergeCells('D4:E4');
					$this->excel->getActiveSheet()->setCellValue('D4', 'Transaksi');

					$this->excel->getActiveSheet()->mergeCells('F4:G4');
					$this->excel->getActiveSheet()->setCellValue('F4', 'Saldo');

					$this->excel->getActiveSheet()->setCellValue('D5', 'Debet');
					$this->excel->getActiveSheet()->setCellValue('E5', 'Kredit');
					$this->excel->getActiveSheet()->setCellValue('F5', 'Nilai');
					$this->excel->getActiveSheet()->setCellValue('G5', 'D/C');

					$row = 6;
					$no = 1;
					$total_debet = 0;
					$total_kredit = 0;
					foreach ($data['data_detail'] as $row_data) {
						$total_debet += $row_data['debet'];
						$total_kredit += $row_data['kredit'];
						$saldo = $row_data['debet'] - $row_data['kredit'];

						if ($saldo < 0) {
							$saldo = $saldo * -1;
							$tipe_saldo = "C";
						} else if ($saldo > 0) {
							$tipe_saldo = "D";
						} else {
							$tipe_saldo = "";
						}

						$this->excel->getActiveSheet()->setCellValue('A' . $row, $no);
						// $objPHPExcel->getActiveSheet()->setCellValueExplicit('A1', '0029', PHPExcel_Cell_DataType::TYPE_STRING);
						$this->excel->getActiveSheet()->setCellValueExplicit('B' . $row, $row_data['kode_account'], PHPExcel_Cell_DataType::TYPE_STRING);
						$this->excel->getActiveSheet()->setCellValue('C' . $row, $row_data['uraian']);
						$this->excel->getActiveSheet()->getStyle('D' . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
						$this->excel->getActiveSheet()->setCellValueExplicit('D' . $row, $row_data['debet'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
						$this->excel->getActiveSheet()->getStyle('E' . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
						$this->excel->getActiveSheet()->setCellValueExplicit('E' . $row, $row_data['kredit'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
						$this->excel->getActiveSheet()->getStyle('F' . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
						$this->excel->getActiveSheet()->setCellValueExplicit('F' . $row, $saldo, PHPExcel_Cell_DataType::TYPE_NUMERIC);
						$this->excel->getActiveSheet()->setCellValue('G' . $row, $tipe_saldo);
						$row++;
						$no++;
					}

					$this->excel->getActiveSheet()->mergeCells('A' . $row . ':C' . $row);
					$this->excel->getActiveSheet()->setCellValue('A' . $row, 'Jumlah');
					$style = array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT, ));
					$this->excel->getActiveSheet()->getStyle('A' . $row . ':C' . $row . '')->applyFromArray($style);
					$this->excel->getActiveSheet()->getStyle('D' . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
					$this->excel->getActiveSheet()->setCellValueExplicit('D' . $row, $total_debet, PHPExcel_Cell_DataType::TYPE_NUMERIC);
					$this->excel->getActiveSheet()->getStyle('E' . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
					$this->excel->getActiveSheet()->setCellValueExplicit('E' . $row, $total_kredit, PHPExcel_Cell_DataType::TYPE_NUMERIC);

					// $this->excel->getActiveSheet()->setCellValue('D'.$row, $total_debet);
					// $this->excel->getActiveSheet()->setCellValue('E'.$row, $total_kredit);

					// die();

					$filename = 'Trial_Balance.xls';
					//save our workbook as this file name
					header('Content-Type: application/vnd.ms-excel');
					//mime type
					header('Content-Disposition: attachment;filename="' . $filename . '"');
					//tell browser what's the file name
					header('Cache-Control: max-age=0');
					//no cache

					//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007'
					// (and adjust the filename extension, also the header mime type)
					//if you want to save it as .XLSX Excel 2007 format
					$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
					//force user to download the Excel file without writing it to server's HD
					$objWriter->save('php://output');
				} else {
					show_404();
				}
			}
		}

		public function read_preview_excel($dari = null, $sampai = null, $akun = null) {
			//load our new PHPExcel library
			$this->load->library('excel');
			//activate worksheet number 1
			$this->excel->setActiveSheetIndex(0);
			//name the worksheet
			$this->excel->getActiveSheet()->setTitle('Data Jurnal');
			//set cell A1 content with some text

			//Loop Heading
			$heading = array(
				'Tanggal',
				'ID',
				'No Akun',
				'Nama Akun',
				'Debet',
				'Kredit'
			);
			$rowNumberH = 1;
			$colH = 'A';
			foreach ($heading as $h) {
				$this->excel->getActiveSheet()->setCellValue($colH . $rowNumberH, $h);
				$colH++;
			}

			// load data
			if ($dari && $sampai) {
				$get['tanggal_cd >='] = $dari;
				$get['tanggal_cd <='] = $sampai;
			}
			if ($akun) {
				$get['account_no'] = $akun;
			}
			$data = $this->{$this->models[0]}->get_data_for_excel($get);

			//Loop Result
			$totn = count($data);
			$maxrow = $totn + 1;
			$row = 2;
			$no = 1;
			foreach ($data as $n) {
				//$numnil = (float) str_replace(',','.',$n->nilai);
				$this->excel->getActiveSheet()->setCellValue('A' . $row, $n['tanggal_cd']);
				$this->excel->getActiveSheet()->setCellValue('B' . $row, $n['id_cd_um']);
				$this->excel->getActiveSheet()->setCellValue('C' . $row, $n['account_no']);
				$this->excel->getActiveSheet()->setCellValueExplicit('D' . $row, $n['uraian']);
				$this->excel->getActiveSheet()->getStyle('E' . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
				$this->excel->getActiveSheet()->setCellValueExplicit('E' . $row, $n['debet'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
				$this->excel->getActiveSheet()->getStyle('F' . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
				$this->excel->getActiveSheet()->setCellValueExplicit('F' . $row, $n['kredit'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
				$row++;
				$no++;
			}

			//Freeze pane
			$this->excel->getActiveSheet()->freezePane('A2');

			$filename = 'Data_jurnal.xls';
			//save our workbook as this file name
			header('Content-Type: application/vnd.ms-excel');
			//mime type
			header('Content-Disposition: attachment;filename="' . $filename . '"');
			//tell browser what's the file name
			header('Cache-Control: max-age=0');
			//no cache

			//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007'
			// (and adjust the filename extension, also the header mime type)
			//if you want to save it as .XLSX Excel 2007 format
			$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
			//force user to download the Excel file without writing it to server's HD
			$objWriter->save('php://output');
		}

		public function get_akun() {
			$q = $this->input->post('q');

			$ret = $this->{$this->models[0]}->get_akun($q);

			echo json_encode($ret);
		}

		public function create() {
			// // additional block
			//
			// // addtional get
			// $result = $this->{$this->models[0]}->insert($this->data_add);
			// if ($result == 1) {
			// $this->output->set_content_type('application/json')->set_output(json_encode(array('success'
			// => true)));
			// } else {
			// $this->output->set_content_type('application/json')->set_output(json_encode(array('msg'
			// => $this->db->_error_message())));
			// }
		}

		public function update() {
			// $param = $this->uri->segment(4); // parameter key
			//
			// // additional block
			//
			// // addtional data
			//
			// // additional where
			// $this->where_add['param'] = $param;
			//
			// $result = $this->{$this->models[0]}->update($this->where_add,
			// $this->data_add);
			// if ($result == 1) {
			// $this->output->set_content_type('application/json')->set_output(json_encode(array('success'
			// => true)));
			// } else {
			// $this->output->set_content_type('application/json')->set_output(json_encode(array('msg'
			// => 'Data Gagal Di Update !')));
			// }
		}

		public function delete() {
			// $param = $this->uri->segment(4); // parameter key
			//
			// // additional block
			//
			// // additional where
			// $this->where_add['param'] = $param;
			//
			// $result = $this->{$this->models[0]}->delete($this->where_add);
			// if ($result == 1) {
			// $this->output->set_content_type('application/json')->set_output(json_encode(array('success'
			// => true)));
			// } else {
			// $this->output->set_content_type('application/json')->set_output(json_encode(array('msg'
			// => 'Data Gagal Di Hapus !')));
			// }
		}

	}
