<?php
    if (!defined('BASEPATH'))
        exit('No direct script access allowed');
    class Laporan_aruskas extends MY_Controller {
        public $models = array('laporan_aruskas');
        
        public function __construct() {
            parent::__construct();
        }

        public function index() {
            $data = array();
            $data['menu'] = $this->model_menu->getAllMenu();

            $this->template->load('template', 'view_laporan_aruskas', $data);
        }

        public function read(){
            // $where = ('KM <> 0 OR BL <> 0');   
            $this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read());
        }

        public function read_all() {
            $this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read_all());
        }
        
        function getTree()
        {
            $data = $this->{$this->models[0]}->getTree();
            $this->output->set_output(json_encode($data));  
        }
        function read_preview($dari){    
          //load content html
          $tgl = date('Y-m-01', strtotime("$dari") );
          $data['data'] = $this->aruskas_cetak($tgl);
          $data['split']  = $this->split();
          $data['tgl']  = date('t-m-Y', strtotime("$dari") );
          $html = $this->load->view('cetak_laporan_aruskas', $data,true);
          // echo $html;
          // die();
          // create pdf using dompdf
          $this->load->library('dompdf_gen'); // Load library
          $this->dompdf->set_paper('A4', 'portrait'); // Setting Paper landscape portrait
          // Convert to PDF
          $this->dompdf->load_html($html);
          $this->dompdf->render();
          $canvas = $this->dompdf->get_canvas(); 
          $font = Font_Metrics::get_font("helvetica", ""); 
          $canvas->page_text(500, 830, "Halaman : {PAGE_NUM} dari {PAGE_COUNT}",$font, 8, array(0,0,0));

          $this->dompdf->stream("laporan_keuangan.pdf");
        }
        function split(){
          $this->db->reconnect();
          $data = $this->db->query('SELECT
                              COUNT(*) as split
                            FROM
                              (
                                SELECT
                                  A.order_number
                                FROM
                                  sys_group_link_laporan A
                                WHERE
                                  A.id_laporan = 2
                                GROUP BY
                                  A.order_number
                              ) as t')->result_array();
          return $data[0]['split'];
        }
        function aruskas($tgl_A = null){
          $tgl = date('Y-m-01', strtotime("$tgl_A") );
          
          $this->db->reconnect();
          $data = $this->db->query('call sp_laporan_aruskas(\''.$tgl.'\')' )->result_array();
          echo json_encode( $data );
        }
        function aruskas_cetak($tgl_A = null){
          $this->db->reconnect();
          $data = $this->db->query('call sp_laporan_aruskas(\''.$tgl_A.'\')' )->result_array();
          return $data;
        }

        function cetak_excel($tgl_A){
          
          //load our new PHPExcel library
          $this->load->library('excel');
          //activate worksheet number 1
          $this->excel->setActiveSheetIndex(0);
          //name the worksheet
          $this->excel->getActiveSheet()->setTitle('Data Jurnal');
          //set cell A1 content with some text

          //Loop Heading
          $heading = array(
            'ID Laporan',
            'Keterangan',
            'Bina Lingkungan',
            'Kemitraan'
          );
          $rowNumberH = 1;
          $colH = 'A';
          foreach ($heading as $h) {
            $this->excel->getActiveSheet()->setCellValue($colH . $rowNumberH, $h);
            $colH++;
          }

          
          $data = $this->aruskas_cetak($tgl_A);

          //Loop Result
          $totn = count($data);
          $maxrow = $totn + 1;
          $row = 2;
          $no = 1;
          foreach ($data as $n) {
            //$numnil = (float) str_replace(',','.',$n->nilai);
            $this->excel->getActiveSheet()->setCellValue('A' . $row, $n['id_laporan']);
            $this->excel->getActiveSheet()->setCellValueExplicit(
              'B' . $row,
              $n['kode_account'],
              PHPExcel_Cell_DataType::TYPE_STRING
            );
            $this->excel->getActiveSheet()->setCellValue(
              'C' . $row, 
              $n['kode_account'] ? '                '.$n['keterangan'] : $n['keterangan']
            );
            $this->excel->getActiveSheet()->setCellValue('D' . $row, $n['BL']);
            $this->excel->getActiveSheet()->setCellValue('E' . $row, $n['KM']);
            // $this->excel->getActiveSheet()->setCellValue('B' . $row, $n['id_cd_um']);
            // $this->excel->getActiveSheet()->setCellValue('C' . $row, $n['account_no']);
            // $this->excel->getActiveSheet()->setCellValueExplicit('D' . $row, $n['uraian']);
            // $this->excel->getActiveSheet()->getStyle('E' . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            // $this->excel->getActiveSheet()->setCellValueExplicit('E' . $row, $n['debet'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            // $this->excel->getActiveSheet()->getStyle('F' . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            // $this->excel->getActiveSheet()->setCellValueExplicit('F' . $row, $n['kredit'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $row++;
            $no++;
          }
          // $last_row = $row-1;
          // $row_jumlah = $row+2;
          // $this->excel->getActiveSheet()->setCellValue('B' . $row_jumlah, '="Jumlah = "&sum(A2:A'. $last_row .')');

          //Freeze pane
          $this->excel->getActiveSheet()->freezePane('A2');

          $filename = 'Data_Arus_Kas.xls';
          //save our workbook as this file name
          header('Content-Type: application/vnd.ms-excel');
          //mime type
          header('Content-Disposition: attachment;filename="' . $filename . '"');
          //tell browser what's the file name
          header('Cache-Control: max-age=0');
          //no cache

          //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007'
          // (and adjust the filename extension, also the header mime type)
          //if you want to save it as .XLSX Excel 2007 format
          $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
          //force user to download the Excel file without writing it to server's HD
          $objWriter->save('php://output');

        }
        

    }
