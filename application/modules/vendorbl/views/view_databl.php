<?php
$app = 'Data BL'; // nama aplikasi
$module = 'vendorbl';
$appLink = 'databl'; // controller
$idField  = 'id_bl'; //field key table
$moduleref = 'kemitraan';
$appLinkref = 'datamitra'; // controller
?>

<script>
	var url;
	var app = "<?=$app?>";
	var appLink = '<?=$appLink?>';
	var module = '<?=$module?>';
	var idField = '<?=$idField?>';
	//begin file
	var file_type = JSON.parse('<?=fileUploadTipe5?>');
	var file_upload = true;
		
	function validate_file(obj){
        var file_name = $(obj).val().replace('C:\\fakepath\\', '');
        var file_name_attr = file_name.split('.');
        file_name_attr[2] = obj.files[0].size/1024;

       
        if(file_type.indexOf(file_name_attr[1]) == -1 || (file_name_attr[2] > <?=fileUploadTipe5?>)){
        	$.messager.alert('Error Message', 'File upload harus (' + file_type.join('|') + ') dan size dibawah <?=fileUploadTipe5?>KB', 'error');
        	$(obj).wrap('<form>').closest('form').get(0).reset();
        	$(obj).unwrap();
        }
    }
    //end file
    function add(){
        $('#dlg').dialog('open').dialog('setTitle','Tambah ' + app);
		$('#fm').form('clear');
        $('#nilai').numberbox('setValue', 0);
        $('#tanggal_input').datebox('setValue','<?=date("Y-m-d")?>');
	    $('#tbl').html('Save');
        file_upload=true;
		url = '<?=base_url($module . '/' . $appLink . '/create')?>';
    }
    function edit(){
        var row = $('#dg').datagrid('getSelected');
		if (row){
			$('#tbl').html('Simpan');
			$('#dlg').dialog('open').dialog('setTitle','Edit '+ app);
            $('.edit').hide();
          /*  file_upload=false;
            //lampiran foto
            if(row.lampiran_foto != ''){
            	var lampiran_foto = '<?=base_url()?>'+row.lampiran_foto;
                $('#current_foto').attr('href', lampiran_foto).show();
                $('#current_foto_inp').attr('value', row.lampiran_foto);
            } 
            //lampiran ktp
            if(row.lampiran_ktp != ''){
            	var lampiran_ktp = '<?=base_url()?>'+row.lampiran_ktp;
                $('#current_ktp').attr('href', lampiran_ktp).show();
                $('#current_ktp_inp').attr('value', row.lampiran_ktp);
            } 
            //lampiran proposal
              if(row.lampiran_proposal != ''){
            	var lampiran_proposal = '<?=base_url()?>'+row.lampiran_proposal;
                $('#current_proposal').attr('href', lampiran_proposal).show();
                $('#current_proposal_inp').attr('value', row.lampiran_proposal);
            } 
            //lampiran rab
             if(row.lampiran_rab != ''){
            	var lampiran_rab = '<?=base_url()?>'+row.lampiran_rab;
                $('#current_rab').attr('href', lampiran_rab).show();
                $('#current_rab_inp').attr('value', row.lampiran_rab);
            } */
            $('#fm').form('clear');
			$('#fm').form('load',//row
             {
	 		 id_bl:row.id_bl,
             tanggal_input:row.tanggal_input,
             status_penerimaan:row.status_penerimaan,
             no_dokumen_proposal:row.no_dokumen_proposal,
             tanggal_dokumen_proposal:row.tanggal_dokumen_proposal,
             no_ktp:row.no_ktp,
             nama:row.nama,
             instansi:row.instansi,
             propinsi:row.propinsi,
             alamat:row.alamat,
             kota:row.kota,
             kecamatan:row.kecamatan,
             kelurahan:row.kelurahan,
             kode_pos:row.kode_pos,
             telepon:row.telepon,
             handphone:row.handphone,
             email:row.email,
             jenis_bl:row.jenis_bl,
             jenis_pengajuan:row.jenis_pengajuan,
             barang:row.barang,
             kegiatan:row.kegiatan,
             nilai:row.nilai,
             keterangan:row.keterangan
			});
                var a=$('#jenis_pengajuan').combobox('getValue');
                pengajuan(a);
        
			url = '<?=base_url($module . '/' . $appLink . '/update')?>/'+row[idField];
	    }
    }
    function hapus(){
        var row = $('#dg').datagrid('getSelected');		
	    if (row){
			$.messager.confirm('Confirm','Apakah Anda Yakin Akan Menghapus ini?',function(r){
				if (r){
					$.post('<?=base_url($module . '/' . $appLink . '/delete')?>/'+row[idField],function(result){
						if (result.success){
							$('#dg').datagrid('reload');	// reload the user data
						} else {
							$.messager.show({	// show error message
								title: 'Error',
								msg:result.msg
							});
						}
					},'json');
				}
			});
		}
    }
    function save(){
	   $('#fm').form('submit',{
	    	url: url,
            
	    	onSubmit: function(){
	    		//return $(this).form('validate');
                var ret = $(this).form('validate');
                ret = true;
                /*if(file_upload){
                    if($('#lampiran_foto').get(0).files.length === 0) {
                        ret = false;
                    }
                }
                if(file_upload){
                    if($('#lampiran_ktp').get(0).files.length === 0) {
                        ret = false;
                    }
                }
                if(file_upload){
                    if($('#lampiran_proposal').get(0).files.length === 0) {
                        ret = false;
                    }
                }
                if(file_upload){
                    if($('#lampiran_rab').get(0).files.length === 0) {
                        ret = false;
                    }
                }*/
                if(!ret){
                    $.messager.alert('Error Message', 'Mohon lengkapi data', 'error');
                }
                return ret;
	    	},
	    	success: function(result){
	    		var result = eval('('+result+')');
	    		if (result.success){
	    			$('#dlg').dialog('close');		
	    			$('#dg').datagrid('reload');
	                
	    		} else {
	             $.messager.alert('Error Message',result.msg,'error');
	    		}
	    	}
	    });
	}
	function doSearch(value){
	    $('#dg').datagrid('load',{    
	    	q:value  
	    });
	}
   function lamp_foto(val,row) {
            var e="foto";
            if(val==''||val==null){
                return '<a href="javascript:void(0)" title="Upload Foto" class="btn btn-small btn-default" onclick="javascript:upload_file(\''+e+'\')"><i class="icon-arrow-up icon-large"></i></a>';
            }else{
                return '<a href="javascript:void(0)" onclick="window.open(\'<?=base_url()?>'+row.lampiran_foto+'\',\'_blank\');" class="btn btn-small btn-default"><i class="icon-search"></i></a>';    
            }
            
        }
   function lamp_ktp(val,row) {
            var e='ktp';
            if(val==''||val==null){
                //return '<a href="javascript:void(0)" title="Upload KTP" class="btn btn-small btn-default" onclick="javascript:upload_file(\''+e+'\')"><i class="icon-arrow-up icon-large"></i></a>';
                return '';
            }else{
                return '<a href="javascript:void(0)" onclick="window.open(\'<?=base_url()?>'+row.lampiran_ktp+'\',\'_blank\');" class="btn btn-small btn-default"><i class="icon-search"></i></a>';    
            }
            
        }
   function lamp_proposal(val,row) {
            var e='proposal';
            if(val==''||val==null){
                //return '<a href="javascript:void(0)" title="Upload Proposal" class="btn btn-small btn-default" onclick="javascript:upload_file(\''+e+'\')"><i class="icon-arrow-up icon-large"></i></a>';
                return '';
            }else{
                return '<a href="javascript:void(0)" onclick="window.open(\'<?=base_url()?>'+row.lampiran_proposal+'\',\'_blank\');" class="btn btn-small btn-default"><i class="icon-search"></i></a>';    
            }
            
        }
    function lamp_rab(val,row) {
            var e='rab';
            if(val==''||val==null){
                //return '<a href="javascript:void(0)" title="Upload RAB" class="btn btn-small btn-default" onclick="javascript:upload_file(\''+e+'\')"><i class="icon-arrow-up icon-large"></i></a>';
                return '';
            }else{
                return '<a href="javascript:void(0)" onclick="window.open(\'<?=base_url()?>'+row.lampiran_rab+'\',\'_blank\');" class="btn btn-small btn-default"><i class="icon-search"></i></a>';    
            }
            
        }
    function upload_file(e){
        var row = $('#dg').datagrid('getSelected');
        if(row){
            $('#dlg_upload').dialog('open').dialog('setTitle','Upload '+e);
            $("#id_bl").val(row.id_bl);
            $("#jenis").val(e);
            
        }
        
    }
    function upload_lampiran() {
            $('#fm_upload_file').form('submit',{
                url: '<?=base_url($module . '/' . $appLink . '/upload_lampiran')?>',
                success: function(result){
                    var result = eval('('+result+')');
                    if (result.success){
                        $('#dlg_upload').dialog('close');
                        $('#dg').datagrid('reload');
                    } else {
                        $.messager.alert('Error Message',result.msg,'error');
                    }
                }
            });
        }
    function preview(){
    var row = $('#dg').datagrid('getSelected');
    url  = '<?=base_url($module . '/' . $appLink . '/permohonan')?>/'+row[idField];
    if (row)
    {
        window.open(url,'_blank');
    }
}
    function action(index,value) {

        /*var idbl = value.id_bl;
        disposisi = '<center> <a onclick="javascript:disposisi_view(\''+idbl+'\')" title="Disposisi" class="btn btn-small btn-default easyui-tooltip" ><i class="icon-tasks icon-large"></i></a> </center>';
        return disposisi;*/
        var idbl = value.id_bl;
        var tooltip = 'Belum ada disposisi yang di-approve';
        var style = ' style="color:red" ';
        if(value.approve_disposisi > 0){
            //if(value.total_approve == value.approve_disposisi){
                tooltip = 'Sudah ada disposisi yang di-approve';
                style = ' style="color:green" ';
            //}
        }
        disposisi = '<center> <a onclick="javascript:disposisi_view(\''+idbl+'\')" title="Disposisi" class="btn btn-small btn-default easyui-tooltip"'+style+' '+tooltip+'><i class="icon-tasks icon-large"></i></a> </center>';
        return disposisi;
    }
    function disposisi_view(idbl) {
        $('#dlg_disposisi').dialog('open').dialog('setTitle','List Disposisi');
        // console.log(idbl);
        $('#id_doc').val(idbl);

        $('#dg-disposisi').datagrid({
            url:'<?=base_url($module . '/' . $appLink . '/disposisi_affterProses')?>/'+idbl,
        });
    }
    function add_disposisi(){
        $('#fm_disposisi').form('submit',{
            url: '<?=base_url($module . '/' . $appLink . '/add_disposisi')?>',
            success: function(result){
                var result = eval('('+result+')');
                if (result.success){
                    $('#fm_disposisi').form('reset');
                    $('#dg-disposisi').datagrid('reload');
                } else {
                    $.messager.alert('Error Message',result.msg,'error');
                }
            }
        });
    }
    
    function add_notif(){
        var row = $('#dg-disposisi').datagrid('getSelected');
        if(row){
        	$.messager.confirm('Konfirmasi','Apakah anda yakin ingin mengirim notifikasi?',function(r){
				if (r){
					$.post('<?=base_url($module . '/' . $appLink . '/create_notif')?>', row, function(result){
						if (result.success){
							// $('#dg').datagrid('reload');	// reload the user data
						} else {
							$.messager.show({	// show error message
								title: 'Error',
								msg:result.msg
							});
						}
					},'json');
				}
			});
        }
    }

    function action_disposisi(val, row, index) 
    {
        //var idUser = "<?=$this->session->userdata('idUser')?>";
        baris = $('#dg-disposisi').datagrid('getRows')[index];
        // console.log(baris.approve);
        if(baris.approve == '1')
        {
            disposisi = '<center><span style="color:green;"><i class="icon-check icon-large"></i><span><center>';
        }
        else
        {
            disposisi = '<a href="javascript:disposisi_selesai('+index+')" title="Disposisi" class="btn btn-small btn-default easyui-tooltip" ><i class="icon-ok icon-large"></i></a>';         
        }
        return disposisi;
    }

    function disposisi_selesai(index)
    {
        row = $('#dg-disposisi').datagrid('getRows')[index];
        // console.log(row.id_doc);

        $.post('<?=base_url('vendorbl/databl/disposisi_selesai')?>/'+row.nip_tujuan+'/'+row.id_doc,function(result)
        {
            if (result.success)
            {
                $.messager.show({   
                    title: 'Success',
                    msg:'Disposisi sudah selesai'
                });
                $('#dg-disposisi').datagrid('reload');    
            } 
            else 
            {
                $.messager.show({   
                    title: 'Error',
                    msg:result.msg
                });
                $('#dg-disposisi').datagrid('reload');   
            }
        },'json');
        $('#dg-disposisi').datagrid('reload');  
    }
    function hapus_disposisi(){
        var data = $('#dg-disposisi').datagrid('getSelected');
        $.messager.confirm('Confirm','Apakah Anda Yakin Akan Menghapus ini?',function(r){
            if (r){
                $.post('<?=base_url('vendorbl/databl/hapus_disposisi')?>/'+data.id_doc+'/'+data.nip_tujuan+'/'+data.creator,function(result){
                    if (result.success){
                        $.messager.show({   
                            title: 'Success',
                            msg:'Hapus Disposisi Berhasil'
                        });
                        $('#dg-disposisi').datagrid('reload');    
                    }else{
                        $.messager.show({   
                            title: 'Error',
                            msg:result.msg
                        });
                        $('#dg-disposisi').datagrid('reload');   
                    }
                },'json');
                $('#dg-disposisi').datagrid('reload');
            }
        });
    }
    
    function cetak_disposisi(){
		var id_doc = $('#id_doc').val();
		if (id_doc){
			window.open('<?=base_url($module . '/' . $appLink . '/cetak_disposisi')?>/'+id_doc, '_blank');
	    }
    }
    
    function pilih_mitra(value,row,index){
        // console.log(value);
        var cek = ""
        if(value){
            cek = "checked disabled"
        }

        return "<input style='width: 20px' "+cek+" type='checkbox' id='id_bl' name='id_bl[]' value='"+row.id_bl+"'/>" ;
    }
    function survey(){
         $('#dlg_survey').dialog('open');
         $('#p').show();
    }

    function kirim_survey(){
       $('#fm_survey').form('submit',{
            url: "<?=base_url($module . '/' . $appLink . '/kirim_survey')?>",
            onSubmit: function() {
                return $(this).form('validate');       
            },
            success: function(result){
                var result = eval('('+result+')');
                if (result.success){
                    $('#dlg').dialog('close');      
                    $('#dg').datagrid('reload');
                    $('#fm_survey').form('reset');
                    $('#p').hide();
                    
                } else {
                 $.messager.alert('Error Message',result.msg,'error');
                }
            }
        });
       // $('#p').hide();
    }
    function kirim_batal(){
          $('#fm_survey').form('reset');
         $('#p').hide();
    }
    function upload_file_all(){
        var row = $('#dg').datagrid('getSelected');
		if (row){
		  $('#dlg_upload_all').dialog('open').dialog('setTitle','Upload File');
          $('.edit').hide();
          file_upload=false;
            //lampiran foto
            if(row.lampiran_foto != ''){
            	var lampiran_foto = '<?=base_url()?>'+row.lampiran_foto;
                $('#current_foto').attr('href', lampiran_foto).show();
                $('#current_foto_inp').attr('value', row.lampiran_foto);
            } 
            //lampiran ktp
            if(row.lampiran_ktp != ''){
            	var lampiran_ktp = '<?=base_url()?>'+row.lampiran_ktp;
                $('#current_ktp').attr('href', lampiran_ktp).show();
                $('#current_ktp_inp').attr('value', row.lampiran_ktp);
            } 
            //lampiran proposal
              if(row.lampiran_proposal != ''){
            	var lampiran_proposal = '<?=base_url()?>'+row.lampiran_proposal;
                $('#current_proposal').attr('href', lampiran_proposal).show();
                $('#current_proposal_inp').attr('value', row.lampiran_proposal);
            } 
            //lampiran rab
             if(row.lampiran_rab != ''){
            	var lampiran_rab = '<?=base_url()?>'+row.lampiran_rab;
                $('#current_rab').attr('href', lampiran_rab).show();
                $('#current_rab_inp').attr('value', row.lampiran_rab);
            }
            /*$('#fm_upload_file_all').form('clear');
			$('#fm_upload_file_all').form('load',//row
             {
	 		 id_bl:row.id_bl,
             tanggal_input:row.tanggal_input,
             status_penerimaan:row.status_penerimaan,
             no_dokumen_proposal:row.no_dokumen_proposal,
             tanggal_dokumen_proposal:row.tanggal_dokumen_proposal,
             no_ktp:row.no_ktp,
             nama:row.nama,
             instansi:row.instansi,
             propinsi:row.propinsi,
             alamat:row.alamat,
             kota:row.kota,
             kecamatan:row.kecamatan,
             kelurahan:row.kelurahan,
             kode_pos:row.kode_pos,
             telepon:row.telepon,
             handphone:row.handphone,
             email:row.email,
             jenis_bl:row.jenis_bl,
             kegiatan:row.kegiatan,
             nilai:row.nilai,
             keterangan:row.keterangan
			}); */
            url = '<?=base_url($module . '/' . $appLink . '/upload_lampiran_all')?>/'+row[idField];
          }
    }
    function upload_lampiran_all(){
        var row = $('#dg').datagrid('getSelected');
        if(row){
            
            $('#fm_upload_file_all').form('submit',{
                url: url,
                success: function(result){
                    var result = eval('('+result+')');
                    if (result.success){
                        $('#dlg_upload_all').dialog('close');
                        $('#dg').datagrid('reload');
                        
                    } else {
                        $.messager.alert('Error Message',result.msg,'error');
                    }
                }
            });
        }
         
    }
    
    $(document).ready(function(){
        $.post('<?=base_url('vendorbl/databl/get_approval')?>',function(result)
        {
           
            // var res = result[0].id_jabatan;
            // console.log(result.id_jabatan);
            //alert(result.disposisi);
            if(result.disposisi == null){
                $('#dg').datagrid('hideColumn', 'asd');
            }else{
                $('#dg').datagrid('showColumn', 'asd');
                if(result.disposisi != 1 ){
                    $('#fm_disposisi').hide();
                }else{
                    $('#fm_disposisi').show();
                } 
                
            }
            //alert(result.kirim_survey);
            if (result.kirim_survey == null||result.kirim_survey =='' ) {
                $('#dg').datagrid('hideColumn', 'pilih');
                $('#survey').hide();
            }else{
                $('#dg').datagrid('showColumn', 'pilih');
                $('#survey').show();
            }
            if (result.upload_file == null||result.upload_file =='' ) {
                $('#upload_file').hide();
            }else{
                $('#upload_file').show();
            }
            
        },'json');

        $('#p').hide();
    })

    function pengajuan(val){
        // alert(val);
        if (val=='1') {
            $('.uang').show();
            $('.barang').hide();
        }else{
            $('.barang').show();
            $('.uang').hide();
        }
    }
</script>
 
<div class="tabs-container">   
<form id="fm_survey" method="post" enctype="multipart/form-data" action=""> <!-- form Buka -->                      
	<table id="dg" class="easyui-datagrid" 
	data-options="  
	    url: '<?=base_url($module . '/' . $appLink . '/get_BL')?>',
		singleSelect:'true', 
	    title:'<?=$app?>',
	    toolbar:'#toolbar',
	    iconCls:'icon-cog',
	    rownumbers:'true',  
        nowrap:false, 
	    idField:'<?=$idField?>', 
	    pagination:'true',
	    pageList: [10,20,30]
	    "
	>
        <thead  data-options="frozen:true">  
            <tr>  	
                <th data-options="field:'pilih',width:100" width="50" sortable="true" formatter="pilih_mitra">Pilih</th>
                <th data-options="field:'id_bl',width:100">ID BL</th>
                <th data-options="field:'nokon_old',width:100">Nokon old</th>
            </tr>
        </thead>
	    <thead>
            <tr>
            	<th data-options="field:'propinsi',width:150">propinsi</th>
            	<th data-options="field:'kota',width:150">Kota</th>
            	<th data-options="field:'kecamatan',width:150">Kecamatan</th>
            	<th data-options="field:'kelurahan',width:150">Kelurahan</th>
                <th data-options="field:'tanggal_input',width:130">Tanggal Pengajuan</th>
                <th data-options="field:'nama',width:150">Nama Pemohon</th>
                <th data-options="field:'instansi',width:200">Instansi Pemohon</th>
                <th data-options="field:'alamat',width:300">Alamat</th>
                <th data-options="field:'asd',width:60" formatter="action" id="test">Disposisi</th>
                <th data-options="field:'lampiran_foto',width:60" formatter="lamp_foto">Lampiran<br />Foto</th>
                <th data-options="field:'lampiran_ktp',width:60" formatter="lamp_ktp">Lampiran<br />KTP</th>
                <th data-options="field:'lampiran_proposal',width:60" formatter="lamp_proposal">Lampiran<br />Proposal</th>
                <th data-options="field:'lampiran_rab',width:60" formatter="lamp_rab">Lampiran<br />RAB</th>
            </tr>
     	   <!-- field="id_bl" width="100" sortable="true">ID</th>
	        <th field="tanggal_input" width="200" sortable="true">Tanggal Pengajuan</th>
            <th field="nama" width="200" sortable="true">Nama Pemohon</th>
            <th field="instansi" width="200" sortable="true">Instansi Pemohon</th>
            <th field="alamat" width="200" sortable="true">Alamat</th-->
	    </thead>
	</table>


	<!-- Tombol Add, Edit, Hapus Datagrid -->
    <div id="toolbar">  
        <table align="center" style="padding: 0px; width: 99%;">
            <tr>
                <td>
                    <a href="javascript:void(0)" class="btn btn-small btn-success" onclick="javascript:add()"><i class="icon-plus-sign icon-large"></i>&nbsp;Add</a>
                    <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:edit()"><i class="icon-edit"></i>&nbsp;Edit</a>
                    <a href="javascript:void(0)" class="btn btn-small btn-danger" onclick="javascript:hapus()"><i class="icon-trash icon-large"></i>&nbsp;Delete</a>
                    <a href="javascript:void(0)" class="btn btn-small btn-warning" iconCls: 'icon-search' onclick="javascript:preview()"><i class="icon-download-alt"></i>&nbsp;Preview</a>
                    <a id="upload_file" href="javascript:void(0)" class="btn btn-small btn-success" iconCls: 'icon-search' onclick="javascript:upload_file_all()"><i class="icon-arrow-up icon-large"></i>&nbsp;Uplod File</a>
                    <a id="survey" href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:survey()"><i class="icon-bullhorn icon-large"></i>&nbsp;Survey</a>
                </td>
                <td>&nbsp;</td>
                <td align="right">
                    <input class="easyui-searchbox" prompt="Silahkan Input Field" searcher="doSearch"  style="width:300px"/>
                </td>
            </tr>
        </table>  

        <div id="p" class="easyui-panel" title=" " style="width: 1000%;height:180px;">
            <table style="margin: 10px;" border="0">
                <tr>
                    <td>Tanggal Survey</td>
                    <td style="width: 30px;height: 35px "></td>
                    <td> : <input type="date" name="tgl_survey" class="easyui-datebox" required></td>
                </tr>

                <tr>
                    <td>Petugas Survey</td>
                    <td style="width: 30px;height: 35px "></td>
                    <td> : <select id="tujuan_disposisi" name="petugas" class="easyui-combogrid" required style="width: 150px"
                            data-options="
                                panelWidth:450,
                                mode:'remote',
                                url:'<?=base_url('kemitraan/Datamitra/getPegawai')?>',
                                idField:'idPegawai',
                                textField:'nama_pegawai',
                                columns:[[
                                {field:'idPegawai',title:'NIP',width:50},
                                {field:'nama_pegawai',title:'Nama Pegawai',width:200},
                                {field:'jabatan',title:'jabatan',width:180},
                                ]]
                                ">
                            </select>
                    </td>
                </tr>

                <tr>
                    <td>Keterangan</td>
                    <td style="width: 30px;height: 35px "></td>
                    <td> : <input type="text" name="ket" class="easyui-textbox"></td>
                </tr>
                <tr>
                    <td colspan="3">
                       <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:kirim_survey()"><i class="icon-save icon-large"></i>&nbsp;Simpan</a> 
                       <a href="javascript:void(0)" class="btn btn-small btn-danger" onclick="javascript:kirim_batal()"><i class="icon-remove"></i>&nbsp;Batal</a> 
                    </td>
                </tr>
            </table>
                
    </form> <!-- form tutup --> 
            <br/>
        </div>


    </div>  
    <!-- end tombol datagrid -->


	<!-- Model Start -->
	<div id="dlg" class="easyui-dialog" style="width:500px; height:600px; padding:10px" closed="true" buttons="#t_dlg_dis-buttons" >
	    
        <form id="fm" method="post" enctype="multipart/form-data" action="">
            <div id="tt" class="easyui-tabs" style="width:440px;height:auto;">
                <div title="Data Pribadi" style="padding:20px;">
                        <table width="100%" align="center" border="0">
        	            
        	            <tr>
        	                <td>Nomor KTP</td>
        	                <td>:</td>
        	                <td>
        	                    <input class="easyui-numberbox" type="text" name="no_ktp" id="no_ktp" required style="width: 250px;" maxlength="16">
        	                </td>
        	            </tr>
                        <tr>
        	                <td>Nama Pemohon</td>
        	                <td>:</td>
        	                <td>
        	                    <input type="text" name="nama" id="nama" required style="width: 250px;">
        	                </td>
        	            </tr>
                        
                        <tr>
        	                <td>Alamat</td>
        	                <td>:</td>
        	                <td>
        	                    <textarea id="alamat" name="alamat" style="width: 300px ;" required></textarea>
        	                </td>
        	            </tr>
                        
                        <tr>
        	                <td>Kode Pos</td>
        	                <td>:</td>
        	                <td>
        	                    <input class="easyui-numberbox" type="text" name="kode_pos" id="kode_pos"  style="width: 100px;" maxlength="5">
        	                </td>
        	            </tr>
                        <tr>
        	                <td>Telepon</td>
        	                <td>:</td>
        	                <td>
        	                    <input  type="text" name="telepon" id="telepon"  style="width: 150px;" maxlength="15" onkeypress="return event.charCode >= 48 && event.charCode <= 57">
        	                </td>
        	            </tr>
                        <tr>
        	                <td>Handphone</td>
        	                <td>:</td>
        	                <td>
        	                    <input  type="text" name="handphone" id="handphone"  style="width: 150px;" maxlength="12" onkeypress="return event.charCode >= 48 && event.charCode <= 57">
        	                </td>
        	            </tr>
                        <tr>
        	                <td>Email</td>
        	                <td>:</td>
        	                <td>
        	                    <input type="email" name="email" id="email" required style="width: 250px;">
        	                </td>
        	            </tr>
        	        </table>
                </div>
                <div title="Data Lembaga" style="padding:20px;">
                    <table>
                        <tr>
            	                <td>Tanggal Daftar</td>
            	                <td>:</td>
            	                <td>
            	                    <input id="tanggal_input" name="tanggal_input" required="true" class="easyui-datebox" style="width:100px;"/></td>
            	                </td>
            	            </tr>
                            <tr>
                                <td>Status Penerimaan</td>
                                <td>:</td>
                                <td> <select id="status_penerimaan" name="status_penerimaan">
                                    <option value="1">Internal</option>
                                    <option value="2">Eksternal</option>
                                </select></td>
                            </tr>
                            <tr>
            	                <td>Nomor Dokumen</td>
            	                <td>:</td>
            	                <td>
            	                    <input type="text" name="no_dokumen_proposal" id="no_dokumen_proposal" required style="width: 250px;">
            	                </td>
            	            </tr>
                            <tr>
            	                <td>Tanggal Dokumen</td>
            	                <td>:</td>
            	                <td>
            	                    <input id="tanggal_dokumen_proposal" name="tanggal_dokumen_proposal" required="true" class="easyui-datebox" style="width:100px;"/></td>
            	                </td>
            	            </tr>
                            <tr>
            	                <td>Instansi/Lembaga</td>
            	                <td>:</td>
            	                <td>
            	                    <input type="text" name="instansi" id="instansi" required style="width: 250px;">
            	                </td>
        	               </tr>
                           <tr>
        	                <td>Provinsi</td>
        	                <td>:</td>
        	                <td>
                            <input class="easyui-combobox" name="propinsi" id="propinsi" required style="width: 250px;" data-options="
        	                    url:'<?=base_url($module . '/' . $appLink . '/getPropinsiall')?>',
        	                    method:'get',
        	                    valueField:'name',
        	                    textField:'name',
        	                    panelHeight:'auto',
        	                    label: 'Language:',
        	                    labelPosition: 'top',
        	                    onSelect: function(param){
        	                    	$('#kota').combobox('clear');
        	                    	$('#kota').combobox('reload','<?=base_url($moduleref . '/' . $appLinkref . '/getKota/')?>/'+param.id);
        	                    }
        	                ">
        	                </td>
        	            </tr>
                        <tr>
        	                <td>Kota</td>
        	                <td>:</td>
        	                <td>
                            <input class="easyui-combobox" name="kota" id="kota" required style="width: 250px;" data-options="
        	                    url:'<?=base_url($moduleref . '/' . $appLinkref . '/getKota/')?>',
        	                    method:'get',
        	                    valueField:'name',
        	                    textField:'name',
        	                    panelHeight:'auto',
        	                    label: 'Language:',
        	                    labelPosition: 'top',
        	                    onSelect: function(param){
        	                    	$('#kecamatan').combobox('clear');
        	                    	$('#kecamatan').combobox('reload','<?=base_url($moduleref . '/' . $appLinkref . '/getKecamatan/')?>/'+param.id);
        	                    }
        	                ">
        	                </td>
        	            </tr>
                        <tr>
        	                <td>Kecamatan</td>
        	                <td>:</td>
        	                <td>
                            <input class="easyui-combobox" name="kecamatan" id="kecamatan" required style="width: 250px;" data-options="
        	                    url:'<?=base_url($moduleref . '/' . $appLinkref . '/getKecamatan')?>',
        	                    method:'get',
        	                    valueField:'name',
        	                    textField:'name',
        	                    panelHeight:'auto',
        	                    label: 'Language:',
        	                    labelPosition: 'top',
        	                    onSelect: function(param){
        	                    	$('#kelurahan').combobox('clear');
        	                    	$('#kelurahan').combobox('reload','<?=base_url($moduleref . '/' . $appLinkref . '/getKelurahan/')?>/'+param.id);
        	                    }
        	                ">
        	                </td>
        	            </tr>
                        <tr>
        	                <td>Kelurahan/Desa</td>
        	                <td>:</td>
        	                <td>
                            <input class="easyui-combobox" name="kelurahan" id="kelurahan" required style="width: 250px;" data-options="
        	                    url:'<?=base_url($moduleref . '/' . $appLinkref . '/getKelurahan')?>',
        	                    method:'get',
        	                    valueField:'name',
        	                    textField:'name',
        	                    panelHeight:'auto',
        	                    label: 'Language:',
        	                    labelPosition: 'top'
        	                ">
        	                </td>
        	            </tr>
                        <tr>
        	                <td>Keterangan</td>
        	                <td>:</td>
        	                <td>
        	                    <textarea id="keterangan" name="keterangan" style="width: 300px ;" required></textarea>
        	                </td>
        	            </tr>
                        <tr>
        	                <td>Jenis Bina Lingkungan</td>
        	                <td>:</td>
        	                <td>
                            <select name="jenis_bl" id="jenis_bl" class="easyui-combogrid" style="width:300px;" data-options="
                                required:true,
                                panelWidth:300,
                                value:'',
                                idField:'id_sektor_bl',
                                textField:'nama_bl',
                                mode:'remote',
                                url:'<?=base_url($module . '/' . $appLink . '/sektor_bl')?>',
                                columns:[[
                                {field:'id_sektor_bl',title:'ID',width:100},
                                {field:'nama_bl',title:'Nama Bina Lingkungan',width:195}
                                ]]
                                "></select>
        	                </td>
        	            </tr>
                        <tr>
        	                <td>Bentuk Kegiatan</td>
        	                <td>:</td>
        	                <td>
        	                    <input type="text" name="kegiatan" id="kegiatan" required style="width: 200px;">
        	                </td>
        	            </tr>
                        <tr>
                            <td>Jenis Pengajuan</td>
                            <td>:</td>
                            <td>
                            <input class="easyui-combobox" style="width: 100px;" name="jenis_pengajuan" id="jenis_pengajuan" data-options="
                                valueField: 'value',
                                textField: 'label',
                                data: [{
                                    label: 'Uang',
                                    value: '1'
                                },{
                                    label: 'Barang',
                                    value: '0'
                                }],
                                onSelect:function(param){
                                
                                pengajuan(param.value);

                                }
                                " />
                            </td>
                        </tr>
                        <tr class="barang" style="display: none;">
                            <td>Deskripsi Barang</td>
                            <td>:</td>
                            <td><input class="easyui-textbox" name="barang" id="barang"></td>
                        </tr>
                        <tr class="uang" style="display: none;">
        	                <td>Nilai Pengajuan</td>
        	                <td>:</td>
        	                <td>
                                <input class="easyui-numberbox" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%'" name="nilai" id="nilai" required style="width: 250px;">
        	                </td>
        	            </tr>
                    </table>
                </div>
                <!--div title="Data Lampiran" style="padding:20px;">
                    <table>
                        <tr>
    						<td>File Foto</td>
    						<td>:</td>
    						<td><a href="" id="current_foto" class="edit">File</a><input type="hidden" name="current_ktp" id="current_foto_inp" />
    						<input type="file" id="lampiran_foto" name="lampiran_foto" size="20" onchange="validate_file(this)" />
    						</td>
					   </tr>
                        <tr>
    						<td>File KTP</td>
    						<td>:</td>
    						<td><a href="" id="current_ktp" class="edit">File</a><input type="hidden" name="current_ktp" id="current_ktp_inp" />
    						<input type="file" id="lampiran_ktp" name="lampiran_ktp" size="20" onchange="validate_file(this)" />
    						</td>
					   </tr>
                       <tr>
    						<td>File Proposal</td>
    						<td>:</td>
    						<td><a href="" id="current_proposal" class="edit">File</a><input type="hidden" name="current_proposal" id="current_proposal_inp" />
    						<input type="file" id="lampiran_proposal" name="lampiran_proposal" size="20" onchange="validate_file(this)" />
    						</td>
					   </tr>
                        <tr>
    						<td>File RAB</td>
    						<td>:</td>
    						<td><a href="" id="current_rab" class="edit">File</a><input type="hidden" name="current_rab" id="current_rab_inp" />
    						<input type="file" id="lampiran_rab" name="lampiran_rab" size="20" onchange="validate_file(this)" />
    						</td>
					   </tr>
                    </table>
                </div-->
                
	        
            </div>
	    </form>
	   
        <!-- Tombol simpan & Batal Form -->
        <div id="t_dlg_dis-buttons">
            <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:save()"><i class="icon-save icon-large"></i>&nbsp;Simpan</a>
            <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:$('#dlg').dialog('close')"><i class="icon-remove icon-large"></i>&nbsp;Batal</a>      
        </div>
	</div>
	<!-- Model end -->
    <div id="dlg_upload" class="easyui-dialog" title="Upload File" data-options="iconCls:'icon-file'" closed="true" style="width:400px;height:110px;padding:10px">
        <form id="fm_upload_file" method="POST" enctype="multipart/form-data">
            <input type="hidden" name="id_bl" id="id_bl" />
            <input type="hidden" name="jenis" id="jenis" />
            <input type="file" id="file" name="file" >
            <a href="javascript:void(0)" onclick="upload_lampiran()" class="btn btn-small btn-default">Upload</a>
        </form>
    </div>
</div>

<div id="dlg_disposisi" class="easyui-dialog" style="width:1050px; height:400px; padding:10px" closed="true" buttons="#t_dlg_dis_view-buttons" >
    <form id="fm_disposisi" method="post" enctype="multipart/form-data" action="">
        <table align="left">
            <tr>
                <td>
                    Tujuan
                </td>
                <td> : </td>
                <td>
                    <input type="hidden" id="id_doc" class="easyui-textbox" name="id_doc" style="width: 750px;" >
                    <select id="tujuan_disposisi" name="tujuan_disposisi" class="easyui-combogrid" style="width:215px;height:25px;"
                    data-options="
                        panelWidth:350,
                        mode:'remote',
                        url:'<?=base_url('vendorbl/Databl/getPegawai')?>',
                        idField:'idPegawai',
                        textField:'nama_pegawai',
                        columns:[[
                        {field:'idPegawai',title:'NIP',width:50},
                        {field:'nama_pegawai',title:'Nama Pegawai',width:150},
                        {field:'jabatan',title:'jabatan',width:130},
                        ]]
                        ">
                    </select>
                </td>
            </tr>
            
            <tr>
                <td>
                    Pesan
                </td>
                <td> : </td>
                <td>
                    <input class="easyui-combobox" id="pesan_disposisi" name="pesan_disposisi" style="width:215px;height:25px;" data-options="valueField:'id',textField:'pesan_disposisi',url:'<?=base_url('vendorbl/Databl/getPesan')?>'">
                </td>
            </tr>
            
            <tr>
                <td>
                    Catatan
                </td>
                <td> : </td>
                <td>
                    <input type="text" id="catatan_disposisi" name="catatan_disposisi" maxlength="255" style="width: 950px;">
                </td>
                
            </tr>

            <tr >
                <td></td>
                <td></td>
                <td align="left">
                    <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:add_disposisi()"><i class="icon-plus icon-large"></i>&nbsp;Add</a>
                    <a href="javascript:void(0)" class="btn btn-small btn-danger" onclick="javascript:hapus_disposisi()"><i class="icon-remove icon-large"></i>&nbsp;Delete</a>
                    <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:cetak_disposisi()"><i class="icon-large"></i>&nbsp;Cetak</a>
                </td>
            </tr>
        </table>
    </form>
    <table id="dg-disposisi" class="easyui-datagrid" style="height:200px;width:1000px;"
        data-options="
        singleSelect:'true', 
        title:'List',
        iconCls:'icon-tasks',
        rownumbers:'true', 
        fitColumns:'true'
        "
    >
        <thead>
            <th field="id_doc" sortable="true" >ID DOC</th>
            <th field="pemberi" width="80">Pemberi Disposisi</th>
            <th field="penerima" width="80">Ditujukan Untuk</th>
            <th field="pesan_disposisi" width="100">Pesan</th>
            <th field="catatan_disposisi" width="200">Catatan</th>
            <th field="approve_date_time" width="80">Tanggal Approve</th>
            <th field="asd" width="40" formatter="action_disposisi">Action</th>
        </thead>
    </table>
    <a href="javascript:void(0)" class="btn btn-small" onclick="javascript:add_notif()">&nbsp;Kirim Notifikasi</a>
</div>  

<!--Dialog upload file-->
<div id="dlg_upload_all" class="easyui-dialog" style="width:500px; height:200px; padding:10px" closed="true" buttons="#t_dlg_upload_view-buttons" >
    <form id="fm_upload_file_all" method="POST" enctype="multipart/form-data">
        <table>
            <tr>
				<td>File Foto</td>
				<td>:</td>
				<td><a href="" id="current_foto" class="edit">File</a><input type="hidden" name="current_ktp" id="current_foto_inp" />
				<input type="file" id="lampiran_foto" name="lampiran_foto" size="20" onchange="validate_file(this)" />
				</td>
		   </tr>
            <tr>
				<td>File KTP</td>
				<td>:</td>
				<td><a href="" id="current_ktp" class="edit">File</a><input type="hidden" name="current_ktp" id="current_ktp_inp" />
				<input type="file" id="lampiran_ktp" name="lampiran_ktp" size="20" onchange="validate_file(this)" />
				</td>
		   </tr>
           <tr>
				<td>File Proposal</td>
				<td>:</td>
				<td><a href="" id="current_proposal" class="edit">File</a><input type="hidden" name="current_proposal" id="current_proposal_inp" />
				<input type="file" id="lampiran_proposal" name="lampiran_proposal" size="20" onchange="validate_file(this)" />
				</td>
		   </tr>
            <tr>
				<td>File RAB</td>
				<td>:</td>
				<td><a href="" id="current_rab" class="edit">File</a><input type="hidden" name="current_rab" id="current_rab_inp" />
				<input type="file" id="lampiran_rab" name="lampiran_rab" size="20" onchange="validate_file(this)" />
				</td>
		   </tr>
        </table>
    </form>
    <!--Tombol upload file all-->
    <div id="t_dlg_upload_view">
        <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:upload_lampiran_all()"><i class="icon-arrow-up icon-large"></i>&nbsp;Upload</a>
        <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:$('#dlg_upload_all').dialog('close')"><i class="icon-remove icon-large"></i>&nbsp;Batal</a>      
    </div>
    <!--End Tombol upload file all-->
</div>
<!--End Dialog upload file-->
