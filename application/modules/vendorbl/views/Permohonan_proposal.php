<?php
    date_default_timezone_set("Asia/Jakarta");
    $tanggal = date('d F Y');
    $tgl=$vendor['tanggal_dokumen_proposal'];
    $hari=date('d',strtotime($tgl));
    $bulan=date('m',strtotime($tgl));
    // echo $bulan;
    $year=date('Y',strtotime($tgl));
    $day = date('D', strtotime($tanggal));
    $dayList = array(
        'Sun' => 'Minggu',
        'Mon' => 'Senin',
        'Tue' => 'Selasa',
        'Wed' => 'Rabu',
        'Thu' => 'Kamis',
        'Fri' => 'Jumat',
        'Sat' => 'Sabtu'
    );
    $bulanList = array('01' => 'Januari', '02' => 'Februari', '03' => 'Maret', '04' => 'April', '05' => 'Mei', '06' => 'Juni', '07' => 'Juli', '08' => 'Augustus', '09' => 'September', '10' => 'October.', '11' => 'Nopember', '12' => 'Desember');
   

function kekata($x) {
        $x = abs($x);
        $angka = array("", "satu", "dua", "tiga", "empat", "lima",
        "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
        $temp = "";
        if ($x <12) {
            $temp = " ". $angka[$x];
        } else if ($x <20) {
            $temp = kekata($x - 10). " belas";
        } else if ($x <100) {
            $temp = kekata($x/10)." puluh". kekata($x % 10);
        } else if ($x <200) {
            $temp = " seratus" . kekata($x - 100);
        } else if ($x <1000) {
            $temp = kekata($x/100) . " ratus" . kekata($x % 100);
        } else if ($x <2000) {
            $temp = " seribu" . kekata($x - 1000);
        } else if ($x <1000000) {
            $temp = kekata($x/1000) . " ribu" . kekata($x % 1000);
        } else if ($x <1000000000) {
            $temp = kekata($x/1000000) . " juta" . kekata($x % 1000000);
        } else if ($x <1000000000000) {
            $temp = kekata($x/1000000000) . " milyar" . kekata(fmod($x,1000000000));
        } else if ($x <1000000000000000) {
            $temp = kekata($x/1000000000000) . " trilyun" . kekata(fmod($x,1000000000000));
        }     
            return $temp;
    }
   /* function terbilang($x, $style=4) {
        if($x<0) {
            $hasil = "minus ". trim(kekata($x));
        } else {
            $hasil = trim(kekata($x));
        }     
        switch ($style) {
            case 1:
                $hasil = strtoupper($hasil);
                break;
            case 2:
                $hasil = strtolower($hasil);
                break;
            case 3:
                $hasil = ucwords($hasil);
                break;
            default:
                $hasil = ucfirst($hasil);
                break;
        }     
        return $hasil;
    }*/
    //$nilai_pengajuan = number_format($vendor['nilai_pengajuan']);
    // $harga_penawaran_vendor = number_format($vendor['harga_penawaran_vendor']);
?>
<!DOCTYPE html>
<html>
<head>
	<title>Surat Tanda Terima Proposal</title>
	<style>

	u {    
    border-bottom: 1px dotted #000;
    text-decoration: none;
}
		div, .hr{

		    background-color: white;
		    /*width: 100%;*/
		    border: 0px solid black;
		    padding: 25px;
		    margin: 1px;
		    font-size: 10;
			padding-bottom:2px;
			padding-top: 0px;

		}
		hr{
		    background-color: white;
		    /*width: 100%;*/
		    border: 0px solid black;
		    padding: 0px;
		    margin: 0px;
		}
		h3, .title{
			text-align: center;
		}	
		table{
		    /*width: 100%;*/
		    border: 1px solid black;
		    border-collapse: collapse;
		    padding: 5px;
		    /*padding-bottom: 5px;*/
		}
		table,td,tr{
		    border: 1px solid black;
		    border-collapse: collapse;
		    padding: 1px;
			/*padding-bottom:2px;*/

		}	
					
	</style>
</head>
<body>
    <div style="padding-top: 10px;padding-bottom: 0px;padding-right: 50px;padding-left:50px;">
        <table  style=" padding-top: 10px;" width="100%" height="40%" border="0" >
            <tr style="padding: 10px;">
                <td style="padding-bottom: 0px;padding-top: 0px;padding-left: 0px;padding-right: 0px;"><center><img  src='<?=base_url('assets/images/logo2.png')?>' style="width:80px; height:100px;"></center></td>
                <td align="center"><strong>PROGRAM KEMITRAAN BINA LINGKUNGAN<br />PT.Krakatau Steel (Persero)Tbk.</strong><br />Jl.Industri No. 5 P.O.Box 14<br />Cilegon - Banten 42435<br />Indonesia</td>
            </tr>
            <tr style="padding: 10px;">
                <td align="center" colspan="2" style="padding: 20px;"><strong><font style=" font-size: 25px;">SURAT TANDA TERIMA PROPOSAL</font></strong><br /><?php echo $vendor['id_bl'];?></td>
            </tr>
            <tr style="padding: 10px;">
                <td colspan="2" style="padding: 10px;">Saya yang bertanda tangan di bawah ini :</td>
            </tr>
            <tr style="padding: 10px;">
                <td style="padding: 10px;">Proposal Dari </td><td style="padding: 10px;">: <?php echo $vendor['nama'];?></td>
            </tr>
            <tr style="padding: 10px;">
                <td style="padding: 10px;">Instansi Dari </td><td style="padding: 10px;">: <?php echo $vendor['instansi'];?></td>
            </tr>
            <tr style="padding: 10px;">
                <td style="padding: 10px;">Tanggal Proposal</td><td style="padding: 10px;">: <?php echo tanggal_display($vendor['tanggal_dokumen_proposal']);?></td>
            </tr>
            <tr style="padding: 10px;">
                <td colspan="2" style="padding: 10px;">Telah terima proposal dana untuk <?php echo $vendor['kegiatan'];?> dengan nomor proposal <?php echo $vendor['no_dokumen_proposal'];?>
                 yang diserahkan kepada :</td>
            </tr>
            <tr style="padding: 10px;">
                <td style="padding: 10px;">Nama</td><td style="padding: 10px;">: <?php echo managerName;?></td>
            </tr>
            <tr style="padding: 10px;">
                <td style="padding: 10px;">Jabatan</td><td style="padding: 10px;">: <?php echo managerTitle;?> </td>
            </tr>
            <tr style="padding: 10px;">
                <td style="padding: 10px;">Instansi/Perusahaan</td><td style="padding: 10px;">: PT. Krakatau Steel(Persero) Tbk.</td>
            </tr>
             <tr style="padding: 10px;">
                <td style="padding: 10px;">Tanggal</td><td style="padding: 10px;">: <?php echo tanggal_display($vendor['tanggal_input']);?></td>
            </tr>
            <tr style="padding: 10px;">
                <td style="padding: 10px;"><center>Yang Menerima <br /><br /><br /><br />(<?php echo managerName; ?>)</center></td>
                <td style="padding: 10px;"><center>Cilegon , <?php echo tanggal_display(); ?><br />Yang Menyerahkan<br /><br /><br />(<?php echo $vendor['nama']; ?>)</center></td>
            </tr>
    	</table>
    </div>
</body>
</html>