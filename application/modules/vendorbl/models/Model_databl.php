<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	Class Model_databl extends MY_Model {
		function __construct() {
			parent::__construct();
			$this->_table = "tblingkungan"; // for insert, update, delete
			$this->_view = "tblingkungan_vd"; // for call view
			$this->_order = 'desc';
			$this->_sort = 'tanggal_input';
			$this->_page = 1;
			$this->_rows = 10;

			$this->_create = true;
			$this->_update = true;

			if ($this->uri->segment(4)) {
				$this->_filter = array('id_bl' => $this->uri->segment(4));
			}

			//parameter from post/get - search function
			if ($this->input->post('q')) {
				$this->_like = array(
					'id_bl' => $this->input->post('q'),
					'tanggal_input' => $this->input->post('q'),
					'nama' => $this->input->post('q')
				);
			}

			$this->_param = array('id_bl' => $this->input->post('id_bl'));

			//data array for input to database
			$this->_data = array(
				'tanggal_input' => $this->input->post('tanggal_input'),
                'status_penerimaan' => $this->input->post('status_penerimaan'),
                'no_dokumen_proposal' => $this->input->post('no_dokumen_proposal'),
                'tanggal_dokumen_proposal' => $this->input->post('tanggal_dokumen_proposal'),
				'no_ktp' => $this->input->post('no_ktp'),
                'nama' => $this->input->post('nama'),
                'instansi' => $this->input->post('instansi'),
                'alamat' => $this->input->post('alamat'),
                'propinsi' => $this->input->post('propinsi'),
                'kota' => $this->input->post('kota'),
                'kecamatan' => $this->input->post('kecamatan'),
                'kelurahan' => $this->input->post('kelurahan'),
                'kode_pos' => $this->input->post('kode_pos'),
                'telepon' => $this->input->post('telepon'),
                'handphone' => $this->input->post('handphone'),
                'email' => $this->input->post('email'),
                'jenis_bl' => $this->input->post('jenis_bl'),
                'kegiatan' => $this->input->post('kegiatan'),
                'nilai' => $this->input->post('nilai'),
                'jenis_pengajuan'=> $this->input->post('jenis_pengajuan'),
                'barang'=> $this->input->post('barang'),
                'keterangan' => $this->input->post('keterangan'),
                'status'=>'2'
			);

		}
        function get_idbl(){
        $q = $this->db->query("SELECT LPAD(RIGHT(id_bl,3)+1,3,0) as id_bl FROM tblingkungan WHERE id_bl LIKE DATE_FORMAT(NOW(),'BL%Y%m%') ORDER BY id_bl DESC limit 1");
        return $q;
        }
        function sektor_bl($cari) {
			$this->db->select('id_sektor_bl, nama_bl');
			$this->db->like('CONCAT(id_sektor_bl,nama_bl)', $cari);
			$data = $this->db->get('sektor_bl')->result();
			$this->output->set_output(json_encode($data));
		}
        function update_file($id,$data)
    {
        $this->db->where('id_bl',$id);
        $update = $this->db->update($this->_table,$data);
        if($update){
            $hasil=1;
            return $hasil;
        }else{
            $hasil=0;
            return $hasil;
        }

    }
    	public function permohonan(){
			$param = $this->uri->segment(4);
			$this->db->where("id_bl", $param);
			$vendor = $this->db->get("tblingkungan_vd");
			return $vendor;
		}
// DISPOSISI start -------------------------------------------------------------------------

		public function getPegawai()
		{
			$this->db->select('*');
			$this->db->like( 'idPegawai',$this->input->post('q'));
			$this->db->or_like( 'nama_pegawai',$this->input->post('q'));
			$query = $this->db->get('mas_approval_vd');
			return $query->result_array();
		}
		public function getPesan()
		{
			$this->db->select('*');
			$query = $this->db->get('mas_pesan_disposisi');
			return $query->result_array();
		}

		public function disposisi_affterProses(){
			$id_doc = $this->uri->segment(4);
			$ses_id = $this->session->userdata('idPegawai');

			$this->db->where('node_menu',29);
			$this->db->where('id_doc',$id_doc);
			$data = $this->db->get('disposisi_transaksi_vd')->result_array();

			return $data;
		}
		public function disposisi_selesai($tujuan,$id_doc, $data){
			$ses_id = $this->session->userdata('idPegawai');

			$this->db->select('nip_tujuan');
			$this->db->where('id_doc',$id_doc);
			$this->db->where('nip_tujuan',$tujuan);
			$nip = $this->db->get('disposisi_transaksi')->result_array();
			$nip_tujuan = $nip[0]['nip_tujuan'];

			if($nip_tujuan == $ses_id){
					$this->db->where('id_doc',$id_doc);
					$this->db->where('nip_tujuan',$ses_id);
					$q	= $this->db->update('disposisi_transaksi', $data);

					if ($q)
					{
						$this->output->set_output(json_encode(array('success'=>TRUE)));
					}
					else
					{
						$this->output->set_output(json_encode(array('msg'=>'Gagal update disposisi selesai')));
					}
			}else{
				$this->output->set_output(json_encode(array('msg'=>'User yang Aktif tidak Bisa Melakukan Approve')));
			}

		}

		public function get_BL(){
			$order = $this->input->post('order') ? $this->input->post('order') : 'asc';
			$sort = $this->input->post('sort') ? $this->input->post('sort') : 'id_bl';
			$page = $this->input->post('page') ? intval($this->input->post('page')) : 1;
			$rows = $this->input->post('rows') ? intval($this->input->post('rows')) : 10;
			$offset = (($page - 1) * $rows);

			$id_peg = $this->session->userdata('idPegawai');
			$allow_data = $this->get_userAllow($id_peg);
			
			$like = array();
			if ($this->input->post('q')) {
			    $like = array(
			        'id_bl' => $this->input->post('q')
			    );
			}
			

			if($allow_data){
				//$this->db->group_by("id_bl");
				if (sizeof($like) > 0) {
				    $this->db->or_like($like);
				}
				$json['total'] = $this->db->get('tblingkungan_vd')->num_rows();

				$this->db->order_by($sort, $order);
				$this->db->limit($rows, $offset);
				if (sizeof($like) > 0) {
				    $this->db->or_like($like);
				}

				//$this->db->group_by("id_bl");
				$json['rows'] = $this->db->get("tblingkungan_vd")->result_array();
				return $json;
			}else{
				//$this->db->where("creator_dis", $id_peg);
			    if (sizeof($like) > 0) {
			        $this->db->or_like($like);
			    }
               	$this->db->where("creator", $id_peg);
				$this->db->or_where("nip_tujuan", $id_peg);
               // $this->db->or_where("idPegawai", $id_peg);
				//$this->db->group_by("id_bl");
				$json['total'] = $this->db->get('tblingkungan_vd')->num_rows();

				$this->db->order_by($sort, $order);
				$this->db->limit($rows, $offset);
			//	$this->db->where("creator_dis", $id_peg);
                $this->db->where("creator", $id_peg);
				$this->db->or_where("nip_tujuan", $id_peg);
                //$this->db->or_where("idPegawai", $id_peg);
                if (sizeof($like) > 0) {
                    $this->db->or_like($like);
                }
				//$this->db->group_by("id_bl");
				$json['rows'] = $this->db->get("tblingkungan_vd")->result_array();
				return $json;
			}

		}

        public function getPropinsiall($filter)
		{
			$this->db->select('A.*');
			$this->db->from('provinces A');
			//$this->db->where('id','36');
			//$this->db->or_where('id','32');
			$this->db->where($filter);

			$query = $this->db->get();

			return $query->result_array();
		}

		private function get_userAllow( $idpegawai = ''){
			$this->db->where('idpegawai',$idpegawai);
			$this->db->where('nodemenu',29);
			$this->db->where('allow',1);
			$data = $this->db->get('t_allowdata')->row_array();
			$allow = $data['idpegawai'];
			return $allow;
		}
// DISPOSISI END -------------------------------------------------------------------------
	}
