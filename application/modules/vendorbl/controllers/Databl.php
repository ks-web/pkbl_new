<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');
	class Databl extends MY_Controller {
		public $models = array('databl');
		
		public function __construct() {
			parent::__construct();
		}

		public function index() {
			$data = array();
			$data['menu'] = $this->model_menu->getAllMenu();

			$this->template->load('template', 'view_databl', $data);
		}

		public function read() {
			$this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read());
		}

		public function read_all() {
			$this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read_all());
		}

		public function create() {
			//$param = $this->input->post('id_bl'); // parametter key

			// additional block
            // addtional get
            $this->data_add['id_bl'] =$this->get_idbl();
            $this->data_add['creator'] = $this->session->userdata('id');
			$this->data_add['tanggal_create'] = date("Y-m-d H:i:s");
            // Upload foto
		/*	$bl_dir = 'assets/upload/BL/' . $this->data_add['id_bl'].'/foto';
			if (!is_dir($bl_dir)) {
				mkdir($bl_dir, folderUploadPermission, TRUE);
			}
			$dir = 'assets/upload/BL/' . $this->data_add['id_bl'] . '/foto';
			if (!is_dir($dir)) {
				mkdir($dir, folderUploadPermission, TRUE);
			}

			$config['upload_path'] = $dir . '/';
			$config['allowed_types'] = implode('|', json_decode(fileUploadTipe5));
			$config['max_size'] = fileUploadTipe5;
			$config['encrypt_name'] = true;
			$this->upload->initialize($config);

			if (!$this->upload->do_upload('lampiran_foto')) {
				$this->data_add['lampiran_foto'] = '';
			} else {
				$file = $this->upload->data();
				$this->data_add['lampiran_foto'] = $config['upload_path'] . $file['file_name'];
			}
            // Upload ktp
			$bl_dir = 'assets/upload/BL/' . $this->data_add['id_bl'].'/ktp';
			if (!is_dir($bl_dir)) {
				mkdir($bl_dir, folderUploadPermission, TRUE);
			}
			$dir = 'assets/upload/BL/' . $this->data_add['id_bl'] . '/ktp';
			if (!is_dir($dir)) {
				mkdir($dir, folderUploadPermission, TRUE);
			}

			$config['upload_path'] = $dir . '/';
			$config['allowed_types'] = implode('|', json_decode(fileUploadTipe5));
			$config['max_size'] = fileUploadTipe5;
			$config['encrypt_name'] = true;
			$this->upload->initialize($config);

			if (!$this->upload->do_upload('lampiran_ktp')) {
				$this->data_add['lampiran_ktp'] = '';
			} else {
				$file = $this->upload->data();
				$this->data_add['lampiran_ktp'] = $config['upload_path'] . $file['file_name'];
			}
            //upload proposal
			$bl_dir = 'assets/upload/BL/' . $this->data_add['id_bl'].'/proposal';
			if (!is_dir($bl_dir)) {
				mkdir($bl_dir, folderUploadPermission, TRUE);
			}
			$dir = 'assets/upload/BL/' . $this->data_add['id_bl'] . '/proposal';
			if (!is_dir($dir)) {
				mkdir($dir, folderUploadPermission, TRUE);
			}

			$config['upload_path'] = $dir . '/';
			$config['allowed_types'] = implode('|', json_decode(fileUploadTipe5));
			$config['max_size'] = fileUploadTipe5;
			$config['encrypt_name'] = true;
			$this->upload->initialize($config);

			if (!$this->upload->do_upload('lampiran_proposal')) {
				$this->data_add['lampiran_proposal'] = '';
			} else {
				$file = $this->upload->data();
				$this->data_add['lampiran_proposal'] = $config['upload_path'] . $file['file_name'];
			}
            // upload rab
            $bl_dir = 'assets/upload/BL/' . $this->data_add['id_bl'].'/rab';
			if (!is_dir($bl_dir)) {
				mkdir($bl_dir, folderUploadPermission, TRUE);
			}
			$dir = 'assets/upload/BL/' . $this->data_add['id_bl'] . '/rab';
			if (!is_dir($dir)) {
				mkdir($dir, folderUploadPermission, TRUE);
			}

			$config['upload_path'] = $dir . '/';
			$config['allowed_types'] = implode('|', json_decode(fileUploadTipe5));
			$config['max_size'] = fileUploadTipe5;
			$config['encrypt_name'] = true;
			$this->upload->initialize($config);

			if (!$this->upload->do_upload('lampiran_rab')) {
				$this->data_add['lampiran_rab'] = '';
			} else {
				$file = $this->upload->data();
				$this->data_add['lampiran_rab'] = $config['upload_path'] . $file['file_name'];
			}
            */
			$result = $this->{$this->models[0]}->insert($this->data_add);
			if ($result == 1) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => $this->db->_error_message())));
			}
		}

		public function create_notif() {
			$param = $this->uri->segment(4); // parameter key

			// additional block

			// additional where
			
			$this->db->where('idPegawai', $this->input->post('nip_tujuan'));
			$query = $this->db->get('mas_pegawai');
			if($query->num_rows()){
				$row_pegawai = $query->row_array();
				
				$this->load->library('pmailer');
				
				$content = "
				<html>
					<body>
						Diberitahukan bahwa ada disposisi mengenai transaksi bina lingkungan dengan No " . $this->input->post('id_doc') . " yang harus anda validasi segera.
						<br/><br/>
						Terima kasih.
					</body>
				</html>
				";
				$mail = $this->pmailer->mail_sent(array('demo1@cahaya-abadi.net'), 'Pemberitahuan Disposisi Bina Lingkungan', $content);
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'Data Gagal Di Hapus !')));
			}
		}
        function get_idbl()
    	{
    		$maxbl =$this->{$this->models[0]}->get_idbl();
    		if ($maxbl->num_rows() > 0)
            {
                $q = $maxbl->result_array();
                $a = $q[0]['id_bl'];
            } else {
                $a = "001";
            }
                $hasil= "BL".date('Ym').$a;
                return $hasil;
    	}

		public function update() {
		    $param = $this->uri->segment(4); // parameter key
		    $this->where_add['id_bl'] = $param;
            // Upload foto
		/*	$bl_dir = 'assets/upload/BL/' . $param.'/foto';
			if (!is_dir($bl_dir)) {
				mkdir($bl_dir, folderUploadPermission, TRUE);
			}
			$dir = 'assets/upload/BL/' . $param . '/foto';
			if (!is_dir($dir)) {
				mkdir($dir, folderUploadPermission, TRUE);
			}

			$config['upload_path'] = $dir . '/';
			$config['allowed_types'] = implode('|', json_decode(fileUploadTipe5));
			$config['max_size'] = fileUploadTipe5;
			$config['encrypt_name'] = true;
			$this->upload->initialize($config);

			if (!$this->upload->do_upload('lampiran_foto')) {
				//$this->data_add['lampiran_foto'] = '';
			} else {
				$file = $this->upload->data();
				$this->data_add['lampiran_foto'] = $config['upload_path'] . $file['file_name'];
			}
            // Upload ktp
			$bl_dir = 'assets/upload/BL/' . $param.'/ktp';
			if (!is_dir($bl_dir)) {
				mkdir($bl_dir, folderUploadPermission, TRUE);
			}
			$dir = 'assets/upload/BL/' . $param . '/ktp';
			if (!is_dir($dir)) {
				mkdir($dir, folderUploadPermission, TRUE);
			}

			$config['upload_path'] = $dir . '/';
			$config['allowed_types'] = implode('|', json_decode(fileUploadTipe5));
			$config['max_size'] = fileUploadTipe5;
			$config['encrypt_name'] = true;
			$this->upload->initialize($config);

			if (!$this->upload->do_upload('lampiran_ktp')) {
				//$this->data_add['lampiran_ktp'] = '';
			} else {
				$file = $this->upload->data();
				$this->data_add['lampiran_ktp'] = $config['upload_path'] . $file['file_name'];
			}
            //upload proposal
			$bl_dir = 'assets/upload/BL/' . $param.'/proposal';
			if (!is_dir($bl_dir)) {
				mkdir($bl_dir, folderUploadPermission, TRUE);
			}
			$dir = 'assets/upload/BL/' . $param . '/proposal';
			if (!is_dir($dir)) {
				mkdir($dir, folderUploadPermission, TRUE);
			}

			$config['upload_path'] = $dir . '/';
			$config['allowed_types'] = implode('|', json_decode(fileUploadTipe5));
			$config['max_size'] = fileUploadTipe5;
			$config['encrypt_name'] = true;
			$this->upload->initialize($config);

			if (!$this->upload->do_upload('lampiran_proposal')) {
				//$this->data_add['lampiran_proposal'] = '';
			} else {
				$file = $this->upload->data();
				$this->data_add['lampiran_proposal'] = $config['upload_path'] . $file['file_name'];
			}
            // upload rab
            $bl_dir = 'assets/upload/BL/' . $param.'/rab';
			if (!is_dir($bl_dir)) {
				mkdir($bl_dir, folderUploadPermission, TRUE);
			}
			$dir = 'assets/upload/BL/' . $param . '/rab';
			if (!is_dir($dir)) {
				mkdir($dir, folderUploadPermission, TRUE);
			}

			$config['upload_path'] = $dir . '/';
			$config['allowed_types'] = implode('|', json_decode(fileUploadTipe5));
			$config['max_size'] = fileUploadTipe5;
			$config['encrypt_name'] = true;
			$this->upload->initialize($config);

			if (!$this->upload->do_upload('lampiran_rab')) {
				//$this->data_add['lampiran_rab'] = '';
			} else {
				$file = $this->upload->data();
				$this->data_add['lampiran_rab'] = $config['upload_path'] . $file['file_name'];
			}
			*/

			// additional block

			// addtional data

			// additional where
			

			$result = $this->{$this->models[0]}->update($this->where_add, $this->data_add);
			if ($result == 1) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => $result)));
			}
		}

		public function delete() {
			$param = $this->uri->segment(4); // parameter key

			// additional block

			// additional where
			$this->where_add['id_bl'] = $param;

			$result = $this->{$this->models[0]}->delete($this->where_add);
			if ($result == 1) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'Data Gagal Di Hapus !')));
			}
		}
        function sektor_bl() {
			header("Content-Type: application/json");
			$cari = $this->input->post('q') ? $this->input->post('q') : '';
			$this->{$this->models[0]}->sektor_bl($cari);
		}
        function upload_lampiran() {
 
                  //$this->data_add['Jenis_ijin'] = $this->input->post('Jenis_ijin');
                  //$this->data_add['vendor_code'] = $this->session->userdata('vendor_code');
                  $this->data_add['jenis'] =$this->input->post('jenis');
                  $this->data_add['id_bl'] =$this->input->post('id_bl');
 
                  // Upload file
                  $bl_dir = 'assets/upload/BL/' . $this->data_add['id_bl'];
                  if (!is_dir($bl_dir)) {
                        mkdir($bl_dir, folderUploadPermission, TRUE);
                  }
                  $dir = 'assets/upload/BL/' . $this->data_add['id_bl'] .'/'. $this->data_add['jenis'] ;
                  if (!is_dir($dir)) {
                        mkdir($dir, folderUploadPermission, TRUE);
                  }
 
                  $config['upload_path'] = $dir . '/';
                  $config['allowed_types'] = implode('|', json_decode(fileUploadTipe5));
                  $config['max_size'] = fileUploadSize5;
                  $config['encrypt_name'] = true;
                  $this->upload->initialize($config);
 
                  if (!$this->upload->do_upload('file')) {
                        $this->data_add['lampiran_'.$this->data_add['jenis']] = '';
                  } else {
                        $file = $this->upload->data();
                        $this->data_add['lampiran_'.$this->data_add['jenis']] = $config['upload_path'] . $file['file_name'];
                  }
                  $id=$this->data_add['id_bl'];
                  $data=array('lampiran_'.$this->data_add['jenis']=>$this->data_add['lampiran_'.$this->data_add['jenis']]);
                  $result = $this->{$this->models[0]}->update_file($id, $data);
                  
                  if ($result) {
                        //$this->_log_vendor('create', 'Ijin Usaha');
                        $this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
                  } else {
                        $this->output->set_content_type('application/json')->set_output(json_encode(array('msg' =>'Data Gagal Diupdate !')));
                  }
            }
            function permohonan(){    // load dompdf
    
		    //load content html
		    $data['vendor'] = $this->{$this->models[0]}->permohonan()->row_array();
		    // $html = $this->load->view('sph3.php', $data, true);
            $this->load->helper('tanggal_indo');
		    $html = $this->load->view('Permohonan_proposal.php', $data,true);
		    // create pdf using dompdf
  			$this->load->library('dompdf_gen'); // Load library
			$this->dompdf->set_paper('A4', 'portrait'); // Setting Paper
			// Convert to PDF
			$this->dompdf->load_html($html);
			$this->dompdf->render();
			$this->dompdf->stream("Penerimaan Proposal Bina Lingkungan.pdf");
		}
// DISPOSISI start -------------------------------------------------------------------------

		public function getPegawai(){
			//$filter = array('A.id !=' => 0);
			$this->output->set_content_type('application/json')->set_output(json_encode($this->{$this->models[0]}->getPegawai()));		
		}
		public function getPesan()
		{
			$this->output->set_content_type('application/json')->set_output(json_encode($this->{$this->models[0]}->getPesan()));		
		}

		public function add_disposisi(){
			date_default_timezone_set('Asia/Jakarta');

			$id_doc 			= $this->input->post('id_doc');
			$tujuan_disposisi 	= $this->input->post('tujuan_disposisi');
			$pesan_disposisi 	= $this->input->post('pesan_disposisi');
			$catatan_disposisi 	= $this->input->post('catatan_disposisi');

			$data = array(
			        'id_doc' => $id_doc,
			        'node_menu' => 29,
			        'nip_tujuan' => $tujuan_disposisi,
			        'pesan' => $pesan_disposisi,
			        'catatan_disposisi' => $catatan_disposisi,
			        'creator' => $this->session->userdata('idPegawai'),
			        'tanggal_disposisi' => date("Y-m-d H:i:s")

			);

			$res = $this->db->insert('disposisi_transaksi', $data);

			if ($res) {
            	$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
          	} else {
                $this->output->set_content_type('application/json')->set_output(json_encode(array('msg' =>'Gagal Kirim Disposisi!')));
          	}
		}

		public function disposisi_affterProses(){
			$this->output
				->set_content_type('application/json')
				->set_output(json_encode($this->{$this->models[0]}->disposisi_affterProses()));	
		}

		public function disposisi_selesai(){
			$tujuan = $this->uri->segment(4);
			$id_doc = $this->uri->segment(5);
			header("Content-Type: application/json");
			date_default_timezone_set('Asia/Jakarta');
			
			$data = array
					(		
						'approve' => '1',
						'approve_date_time' => date("Y-m-d H:i:s")
					);
					
			$this->{$this->models[0]}->disposisi_selesai($tujuan,$id_doc, $data);

		}

		public function get_approval(){
			$ses_id = $this->session->userdata('idPegawai');

			$this->db->where('idPegawai', $ses_id);
			$data = $this->db->get('mas_approval_vd')->row_array();
			
			// echo var_dump($data);
			$this->output->set_content_type('application/json')->set_output(json_encode($data));	
		}

		public function hapus_disposisi(){
			$id_doc  = $this->uri->segment(4);
			$tujuan  = $this->uri->segment(5);
			$creator  = $this->uri->segment(6);
			$creator_aktiv = $this->session->userdata('idPegawai');

			if($creator_aktiv != $creator){
				$this->output->set_output(json_encode(array('msg'=>'User yang Aktif tidak Bisa Menghapus')));
			}else{

				$where = array(
					'id_doc' 	 => $id_doc,
					'nip_tujuan' => $tujuan,
					'creator'	 => $creator
				);
				$this->db->where($where);
				$data = $this->db->delete('disposisi_transaksi');
				
				if ($data) {
					$this->output->set_output(json_encode(array('success'=>TRUE)));
				}
				else{
					$this->output->set_output(json_encode(array('msg'=>'Hapus Gagal')));
				}

			}
		}
		
		function cetak_disposisi($id = null) {// load dompdf
			$this->db->where('id_bl', $id);
			$check = $this->db->get('tblingkungan_cetak_disp_vd');
			if ($check->num_rows() > 0) {
				$data['data_head'] = $check->row_array();
				$data['data_detail'] = null;

				$this->db->where('id_doc', $id);
				$q = $this->db->get('tblingkungan_cetak_disp_detail_vd');
				if ($q->num_rows() > 0) {
					$data['data_detail'] = $q->result_array();
				}

				// kota tanggal
				$data['kota'] = 'Cilegon';
				$this->load->helper('tanggal_indo');
				$this->load->helper('terbilang');
				$data['tanggal'] = tanggal_display();

				$html = $this->load->view('cetak_disposisi_databl', $data, true);

				$this->load->library('dompdf_gen');
				// Load library
				$this->dompdf->set_paper('A4', 'potrait');
				// Setting Paper
				// // Convert to PDF
				$this->dompdf->load_html($html);
				$this->dompdf->render();
				$this->dompdf->stream("Lembar_Disposisi-" . $id . ".pdf");
			} else {
				show_404();
			}

		}

		public function get_BL(){
			$this->output
				->set_content_type('application/json')
				->set_output(json_encode($this->{$this->models[0]}->get_BL()));	
		}
        
// DISPOSISI END -------------------------------------------------------------------------
		function kirim_survey(){
			$id_bl				= $this->input->post('id_bl');
			$tgl_survey 		= $this->input->post('tgl_survey');
			$petugas 			= $this->input->post('petugas');
			$ket 				= $this->input->post('ket');

			if($id_bl){
				foreach ($id_bl as $key => $value) {
					$data = array(
							'id_bl' 			=> $id_bl[$key],
							'tanggal_survey'	=> $tgl_survey,
							'petugas_survey'	=> $petugas,
							'keterangan'		=> $ket,
							'pemberi_tugas'		=> $this->session->userdata('idPegawai')
						);

					$res = $this->db->insert('tpenugasan_survey_bl',$data);
				}
				if ($res){
					
					$this->output->set_output(json_encode(array('success'=>TRUE)));

					foreach ($id_bl as $key => $value) {
						$data = array(
								'status' 			=> 3
							);

						$where = array(
								'id_bl' 			=> $id_bl[$key]
							);

						$res = $this->db->update('tblingkungan',$data,$where);
					}

				}else{
					$this->output->set_output(json_encode(array('msg'=>'Gagal')));
				}
			}else{
				$this->output->set_output(json_encode(array('msg'=>'Pilih Minimal Satu Mitra')));
			}
			
		}
        public function getPropinsiall()
		{
			$filter = array('A.id !=' => 0);
			$this->output->set_content_type('application/json')->set_output(json_encode($this->{$this->models[0]}->getPropinsiall($filter)));
		}
        function upload_lampiran_all($idbl){
            $param=$idbl;
            //$this->where_add['id_bl'] = $param;
            // Upload foto
			$bl_dir = 'assets/upload/BL/' . $param.'/foto';
			if (!is_dir($bl_dir)) {
				mkdir($bl_dir, folderUploadPermission, TRUE);
			}
			$dir = 'assets/upload/BL/' . $param . '/foto';
			if (!is_dir($dir)) {
				mkdir($dir, folderUploadPermission, TRUE);
			}

			$config['upload_path'] = $dir . '/';
			$config['allowed_types'] = implode('|', json_decode(fileUploadTipe5));
			$config['max_size'] = fileUploadTipe5;
			$config['encrypt_name'] = true;
			$this->upload->initialize($config);

			if (!$this->upload->do_upload('lampiran_foto')) {
				//$this->data_add['lampiran_foto'] = '';
			} else {
				$file = $this->upload->data();
			//	$this->data_add['lampiran_foto'] = $config['upload_path'] . $file['file_name'];
                $data=array('lampiran_foto'=>$config['upload_path'] . $file['file_name']);
			}
            // Upload ktp
			$bl_dir = 'assets/upload/BL/' . $param.'/ktp';
			if (!is_dir($bl_dir)) {
				mkdir($bl_dir, folderUploadPermission, TRUE);
			}
			$dir = 'assets/upload/BL/' . $param . '/ktp';
			if (!is_dir($dir)) {
				mkdir($dir, folderUploadPermission, TRUE);
			}

			$config['upload_path'] = $dir . '/';
			$config['allowed_types'] = implode('|', json_decode(fileUploadTipe5));
			$config['max_size'] = fileUploadTipe5;
			$config['encrypt_name'] = true;
			$this->upload->initialize($config);

			if (!$this->upload->do_upload('lampiran_ktp')) {
				//$this->data_add['lampiran_ktp'] = '';
			} else {
				$file = $this->upload->data();
				//$this->data_add['lampiran_ktp'] = $config['upload_path'] . $file['file_name'];
                $data=array('lampiran_ktp'=>$config['upload_path'] . $file['file_name']);
			}
            //upload proposal
			$bl_dir = 'assets/upload/BL/' . $param.'/proposal';
			if (!is_dir($bl_dir)) {
				mkdir($bl_dir, folderUploadPermission, TRUE);
			}
			$dir = 'assets/upload/BL/' . $param . '/proposal';
			if (!is_dir($dir)) {
				mkdir($dir, folderUploadPermission, TRUE);
			}

			$config['upload_path'] = $dir . '/';
			$config['allowed_types'] = implode('|', json_decode(fileUploadTipe5));
			$config['max_size'] = fileUploadTipe5;
			$config['encrypt_name'] = true;
			$this->upload->initialize($config);

			if (!$this->upload->do_upload('lampiran_proposal')) {
				//$this->data_add['lampiran_proposal'] = '';
			} else {
				$file = $this->upload->data();
				//$this->data_add['lampiran_proposal'] = $config['upload_path'] . $file['file_name'];
                $data=array('lampiran_proposal'=>$config['upload_path'] . $file['file_name']);
			}
            // upload rab
            $bl_dir = 'assets/upload/BL/' . $param.'/rab';
			if (!is_dir($bl_dir)) {
				mkdir($bl_dir, folderUploadPermission, TRUE);
			}
			$dir = 'assets/upload/BL/' . $param . '/rab';
			if (!is_dir($dir)) {
				mkdir($dir, folderUploadPermission, TRUE);
			}

			$config['upload_path'] = $dir . '/';
			$config['allowed_types'] = implode('|', json_decode(fileUploadTipe5));
			$config['max_size'] = fileUploadTipe5;
			$config['encrypt_name'] = true;
			$this->upload->initialize($config);

			if (!$this->upload->do_upload('lampiran_rab')) {
				//$this->data_add['lampiran_rab'] = '';
			} else {
				$file = $this->upload->data();
				//$this->data_add['lampiran_rab'] = $config['upload_path'] . $file['file_name'];
                $data=array('lampiran_rab'=>$config['upload_path'] . $file['file_name']);
			}
            $result = $this->{$this->models[0]}->update_file($param, $data);
			if ($result == 1) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => $result)));
			}
        }

	}
