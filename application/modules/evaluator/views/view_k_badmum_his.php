<?php
$app9 = 'Biaya Administrasi Umum'; // nama aplikasi
$module9 = 'evaluator';
$appLink9= 'K_badmum_his'; // controller
$idField9  = 'id_admum'; //field key table
?>


<script>
	var url9;
	var app9 = "<?=$app9?>";
	var appLink9 = '<?=$appLink9?>';
	var module9 = '<?=$module9?>';
	var idField9 = '<?=$idField9?>';
	
	function hit5(){
		var harga=$("#fm9 #harga_satuan").numberbox('getValue');
		var prod_akt=$("#fm9 #unit_akt").numberbox('getValue');
		var prod_proy=$("#fm9 #unit_proy").numberbox('getValue');

		var sumharga=harga*prod_akt;
		$("#fm9 #produksi_akt").numberbox('setValue', sumharga);

		var sumharga2=harga*prod_proy;
		$("#fm9 #produksi_proy").numberbox('setValue', sumharga2);
	// console.log(harga);
	}	

    function add9(){
        $('#dlg9').dialog('open').dialog('setTitle','Tambah ' + app9);
		$('#fm9').form('clear');
	    $('#tbl9').html('Save9');
		url9 = '<?=base_url($module9 . '/' . $appLink9 . '/create')?>/'+idParent;
    }
    function edit9(){
        var row = $('#dg9').datagrid('getSelected');
		if (row){
			$('#tbl9').html('Simpan9');
			$('#dlg9').dialog('open').dialog('setTitle','Edit '+ app9);
			$('#fm9').form('load',row);
			url9 = '<?=base_url($module9 . '/' . $appLink9 . '/update')?>/'+row[idField9];
	    }
    }
    function hapus9(){
        var row = $('#dg9').datagrid('getSelected');		
	    if (row){
			$.messager.confirm('Confirm','Apakah Anda Yakin Akan Menghapus ini?',function(r){
				if (r){
					$.post('<?=base_url($module9 . '/' . $appLink9 . '/delete')?>/'+row[idField9],function(result){
						if (result.success){
							$('#dg9').datagrid('reload');	// reload the user data
							$('#dg9').datagrid('reload');
						} else {
							$.messager.show({	// show error message
								title: 'Error',
								msg:result.msg
							});
						}
					},'json');
				}
			});
		}
    }
    function save9(){
	   $('#fm9').form('submit',{
	    	url: url9,
	    	onSubmit: function(){
	    		return $(this).form('validate');
	    	},
	    	success: function(result){
	    		var result = eval('('+result+')');
	    		if (result.success){
	    			$('#dlg9').dialog('close');		
	    			$('#dg9').datagrid('reload');
	                
	    		} else {
	             $.messager.alert('Error Message',result.msg,'error');
	    		}
	    	}
	    });
	}
	function doSearch9(value){
	    $('#dg9').datagrid('load',{    
	    	q:value  
	    });
	}
	function reload9(value){
		$('#dg9').datagrid({    
	    	url: '<?=base_url($module9 . '/' . $appLink9 . '/read/')?>/'+idParent  
	    });
	}
</script>
 
<div class="tabs-container">                
	<table id="dg9" class="easyui-datagrid" 
	data-options="  
	    url: '<?=base_url($module9 . '/' . $appLink9 . '/read')?>',
		singleSelect:'true', 
	    title:'<?=$app9?>',
	    toolbar:'#toolbar9',
	    iconCls:'icon-cog',
	    rownumbers:'true',  
	    idField:'<?=$idField9?>', 
	    pagination:'true',
	    fitColumns:'true',
	    pageList: [10,20,30]
	    "
	>
	    <thead>
	    <!-- <tr> -->
	    	<th field="id_admum" hidden width="100" sortable="true">ID</th>
			<th field="jenis_biaya" width="100" sortable="true">Jenis Biaya</th>
			<th field="biaya_periode" width="100" sortable="true">Biaya / Periode</th>

		<!-- </tr> -->
		</thead>
	</table>
	
	<!-- Model Start -->
	<div id="dlg9" class="easyui-dialog" style="width:500px; height:auto; padding:10px" closed="true" buttons="#t_dlg_dis-buttons9" >
	    <form id="fm9" method="post" enctype="multipart/form-data" action="">
	        <table width="100%" align="center" border="0">
	            <tr>
	               <td>Jenis Biaya</td>
	               <td>:</td>
	               <td>
	               <input type="text" name="jenis_biaya" id="jenis_biaya" required="required" style="width: 150px;">
	               </td>
	            </tr>
	           
	           <tr>
	               <td>Biaya Perperiode</td>
	               <td>:</td>
	               <td>
	               <input class="easyui-numberbox" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%'" name="biaya_periode" id="biaya_periode" required style="width: 150px;">
	               </td>
	            </tr>
	           
	              </tr>

	            
	        </table>
	    </form>
	   
	   	<!-- Tombol Add, Edit, Hapus Datagrid -->
       	<div id="toolbar9">  
            <table align="center" style="padding: 0px; width: 99%;">
                <tr>
                    <td>
                        <a href="javascript:void(0)" class="btn btn-small btn-success" onclick="javascript:add9()"><i class="icon-plus-sign icon-large"></i>&nbsp;Add</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:edit9()"><i class="icon-edit"></i>&nbsp;Edit</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-danger" onclick="javascript:hapus9()"><i class="icon-trash icon-large"></i>&nbsp;Delete</a>
                    </td>
                    <td>&nbsp;</td>
                    <td align="right">
                    	<input class="easyui-searchbox" prompt="Silahkan Input Field" searcher="doSearch9"  style="width:300px"/>
                    </td>
                </tr>
            </table>  
        </div>  
        <!-- end tombol datagrid -->
        <!-- Tombol simpan & Batal Form -->
        <div id="t_dlg_dis-buttons9">
            <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:save9()"><i class="icon-save icon-large"></i>&nbsp;Simpan</a>
            <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:$('#dlg9').dialog('close')"><i class="icon-remove icon-large"></i>&nbsp;Batal</a>      
        </div>
	</div>
	<!-- Model end -->
</div>