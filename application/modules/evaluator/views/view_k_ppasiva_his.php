<?php
$app12 = 'Posisi Pasiva'; // nama aplikasi
$module12 = 'evaluator';
$appLink12 = 'K_ppasiva_his'; // controller
$idField12  = 'id_ppasiva'; //field key table
?>

<script>
	var url12;
	var app12 = "<?=$app12?>";
	var appLink12 = '<?=$appLink12?>';
	var module12 = '<?=$module12?>';
	var idField12 = '<?=$idField12?>';
	
	// function hit10(){
	// 	var harga=$("#fm12 #harga_satuan").numberbox('getValue');
	// 	var prod_akt=$("#fm12 #junit_akt").numberbox('getValue');
	// 	var prod_proy=$("#fm12 #junit_proy").numberbox('getValue');

	// 	var sumharga=harga*prod_akt;
	// 	$("#fm12 #biaya_akt").numberbox('setValue', sumharga);

	// 	var sumharga2=harga*prod_proy;
	// 	$("#fm12 #biaya_proy").numberbox('setValue', sumharga2);
	// // console.log(harga);
	// }	

    function add12(){
        $('#dlg12').dialog('open').dialog('setTitle','Tambah ' + app12);
		$('#fm12').form('clear');
	    $('#tbl12').html('Save12');
		url12 = '<?=base_url($module12 . '/' . $appLink12 . '/create')?>/'+idParent;
    }
    function edit12(){
        var row = $('#dg12').datagrid('getSelected');
		if (row){
			$('#tbl12').html('Simpan');
			$('#dlg12').dialog('open').dialog('setTitle','Edit '+ app12);
			$('#fm12').form('load',row);
			url12 = '<?=base_url($module12 . '/' . $appLink12 . '/update')?>/'+row[idField12];
	    }
    }
    function hapus12(){
        var row = $('#dg12').datagrid('getSelected');		
	    if (row){
			$.messager.confirm('Confirm','Apakah Anda Yakin Akan Menghapus ini?',function(r){
				if (r){
					$.post('<?=base_url($module12. '/' . $appLink12 . '/delete')?>/'+row[idField12],function(result){
						if (result.success){
							$('#dg12').datagrid('reload');	// reload the user data
							$('#dg12').datagrid('reload');
						} else {
							$.messager.show({	// show error message
								title: 'Error',
								msg:result.msg
							});
						}
					},'json');
				}
			});
		}
    }
    function save12(){
	   $('#fm12').form('submit',{
	    	url: url12,
	    	onSubmit: function(){
	    		return $(this).form('validate');
	    	},
	    	success: function(result){
	    		var result = eval('('+result+')');
	    		if (result.success){
	    			$('#dlg12').dialog('close');		
	    			$('#dg12').datagrid('reload');
	                
	    		} else {
	             $.messager.alert('Error Message',result.msg,'error');
	    		}
	    	}
	    });
	}
	function doSearch12(value){
	    $('#dg12').datagrid('load',{    
	    	q:value  
	    });
	}
	function reload12(value){
		$('#dg12').datagrid({    
	    	url: '<?=base_url($module12 . '/' . $appLink12 . '/read/')?>/'+idParent  
	    });
	}
</script>
 
<div class="tabs-container">                
	<table id="dg12" class="easyui-datagrid" 
	data-options="  
	    url: '<?=base_url($module12 . '/' . $appLink12 . '/read')?>',
		singleSelect:'true', 
	    title:'<?=$app12?>',
	    toolbar:'#toolbar12',
	    iconCls:'icon-cog',
	    rownumbers:'true',  
	    idField:'<?=$idField12?>', 
	    pagination:'true',
	    fitColumns:'true',
	    pageList: [10,20,30]
	    "
	>
	    <thead>
	    <tr>
	    	<th field="id_ppasiva" width="100" sortable="true">ID</th>
	        <th field="jenis_aktiva" width="100" sortable="true">Unsur Biaya	</th>
	        <th field="jumlah" width="100" sortable="true">Jumlah</th>
			<th field="satuan" width="100" sortable="true">Satuan</th>
			<th field="harga_satuan" width="100" sortable="true">Harga Satuan</th>
			<th field="total" width="100" sortable="true">Total</th>



	    </thead>
	</table>
	
	<!-- Model Start -->
	<div id="dlg12" class="easyui-dialog" style="width:500px; height:auto; padding:10px" closed="true" buttons="#t_dlg_dis-buttons12" >
	    <form id="fm12" method="post" enctype="multipart/form-data" action="">
	        <table width="100%" align="center" border="0">
	            <tr>
	               <td>Jenis Pasiva</td>
	               <td>:</td>
	               <td>
	               <input type="text" name="jenis_aktiva" id="jenis_aktiva" required="required" style="width: 150px;">
	               </td>
	            </tr>
	            <tr>
	               <td>Jumlah</td>
	               <td>:</td>
	               <td>
	               <input type="text" name="jumlah" id="jumlah"  style="width: 50px;">
	               </td>
	            </tr>
	           <tr>
	               <td>Satuan</td>
	               <td>:</td>
	               <td>
	               <input type="text" name="satuan" id="satuan"  style="width: 50px;">
	               </td>
	            </tr>
	           <tr>
	               <td>Harga Satuan</td>
	               <td>:</td>
	               <td><input class="easyui-numberbox" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%'" name="harga_satuan" id="harga_satuan"  style="width: 250px;">
	               </td>
	            </tr>
	             
	             <tr>
	               <td>Total</td>
	               <td>:</td>
	               <td><input class="easyui-numberbox" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%'" name="total" id="total" required  style="width: 250px;" >
	               </td>
	               </tr>		            
	        </table>
	    </form>
	   
	   	<!-- Tombol Add, Edit, Hapus Datagrid -->
       	<div id="toolbar12">  
            <table align="center" style="padding: 0px; width: 99%;">
                <tr>
                    <td>
                        <a href="javascript:void(0)" class="btn btn-small btn-success" onclick="javascript:add12()"><i class="icon-plus-sign icon-large"></i>&nbsp;Add</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:edit12()"><i class="icon-edit"></i>&nbsp;Edit</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-danger" onclick="javascript:hapus12()"><i class="icon-trash icon-large"></i>&nbsp;Delete</a>
                    </td>
                    <td>&nbsp;</td>
                    <td align="right">
                    	<input class="easyui-searchbox" prompt="Silahkan Input Field" searcher="doSearch12"  style="width:300px"/>
                    </td>
                </tr>
            </table>  
        </div>  
        <!-- end tombol datagrid -->
        <!-- Tombol simpan & Batal Form -->
        <div id="t_dlg_dis-buttons12">
            <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:save12()"><i class="icon-save icon-large"></i>&nbsp;Simpan</a>
            <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:$('#dlg12').dialog('close')"><i class="icon-remove icon-large"></i>&nbsp;Batal</a>      
        </div>
	</div>
	<!-- Model end -->
</div>