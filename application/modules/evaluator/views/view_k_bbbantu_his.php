<?php
$app5 = 'Biaya Bahan Bantu'; // nama aplikasi
$module5 = 'evaluator';
$appLink5= 'K_bbbantu_his'; // controller
$idField5  = 'id_bbbantu'; //field key table
?>

<script>
	var url5;
	var app5 = "<?=$app5?>";
	var appLink5 = '<?=$appLink5?>';
	var module5 = '<?=$module5?>';
	var idField5 = '<?=$idField5?>';
	
	function hit3(){
		var harga=$("#fm5 #harga_satuan").numberbox('getValue');
		var prod_akt=$("#fm5 #junit_akt").numberbox('getValue');
		var prod_proy=$("#fm5 #junit_proy").numberbox('getValue');

		var sumharga=harga*prod_akt;
		$("#fm5 #bprod_akt").numberbox('setValue', sumharga);

		var sumharga2=harga*prod_proy;
		$("#fm5 #bprod_proy").numberbox('setValue', sumharga2);
	// console.log(harga);
	}	

    function add5(){
        $('#dlg5').dialog('open').dialog('setTitle','Tambah ' + app5);
		$('#fm5').form('clear');
	    $('#tbl5').html('Save5');
		url5 = '<?=base_url($module5 . '/' . $appLink5 . '/create')?>/'+idParent;
    }
    function edit5(){
        var row = $('#dg5').datagrid('getSelected');
		if (row){
			$('#tbl5').html('Simpan5');
			$('#dlg5').dialog('open').dialog('setTitle','Edit '+ app5);
			$('#fm5').form('load',row);
			url5 = '<?=base_url($module5 . '/' . $appLink5 . '/update')?>/'+row[idField5];
	    }
    }
    function hapus5(){
        var row = $('#dg5').datagrid('getSelected');		
	    if (row){
			$.messager.confirm('Confirm','Apakah Anda Yakin Akan Menghapus ini?',function(r){
				if (r){
					$.post('<?=base_url($module5 . '/' . $appLink5 . '/delete')?>/'+row[idField5],function(result){
						if (result.success){
							$('#dg5').datagrid('reload');	// reload the user data
							$('#dg5').datagrid('reload');
						} else {
							$.messager.show({	// show error message
								title: 'Error',
								msg:result.msg
							});
						}
					},'json');
				}
			});
		}
    }
    function save5(){
	   $('#fm5').form('submit',{
	    	url: url5,
	    	onSubmit: function(){
	    		return $(this).form('validate');
	    	},
	    	success: function(result){
	    		var result = eval('('+result+')');
	    		if (result.success){
	    			$('#dlg5').dialog('close');		
	    			$('#dg5').datagrid('reload');
	                
	    		} else {
	             $.messager.alert('Error Message',result.msg,'error');
	    		}
	    	}
	    });
	}
	function doSearch5(value){
	    $('#dg5').datagrid('load',{    
	    	q:value  
	    });
	}
	function reload5(value){
		$('#dg5').datagrid({    
	    	url: '<?=base_url($module5 . '/' . $appLink5 . '/read/')?>/'+idParent  
	    });
	}
</script>
 
<div class="tabs-container">                
	<table id="dg5" class="easyui-datagrid" 
	data-options="  
	    url: '<?=base_url($module5 . '/' . $appLink5 . '/read')?>',
		singleSelect:'true', 
	    title:'<?=$app5?>',
	    toolbar:'#toolbar5',
	    iconCls:'icon-cog',
	    rownumbers:'true',  
	    idField:'<?=$idField5?>', 
	    pagination:'true',
	    fitColumns:'true',
	    pageList: [10,20,30]
	    "
	>
	    <thead>
	    <tr>
	    	<th rowspan="2" field="id_bbbantu" hidden width="100" sortable="true">ID</th>
	        <th rowspan="2" field="unsur_biaya" width="100" sortable="true">Unsur Biaya	</th>
			<th rowspan="2" field="satuan" width="100" sortable="true">Satuan</th>
			<th rowspan="2" field="harga_satuan" width="100" sortable="true">Harga Satuan</th>
			<th colspan="2"  width="100" sortable="true">Jumlah Unit</th>
			<th colspan="2"  width="100" sortable="true">Biaya Produksi</th>
		</tr>
			<th field="junit_akt" width="100" sortable="true">Akt</th>
			<th field="junit_proy" width="100" sortable="true">Proy</th>
			<th field="bprod_akt" width="100" sortable="true">akt</th>
			<th field="bprod_proy" width="100" sortable="true">Proy</th>



	    </thead>
	</table>
	
	<!-- Model Start -->
	<div id="dlg5" class="easyui-dialog" style="width:500px; height:auto; padding:10px" closed="true" buttons="#t_dlg_dis-buttons5" >
	    <form id="fm5" method="post" enctype="multipart/form-data" action="">
	        <table width="100%" align="center" border="0">
	            <tr>
	               <td>Unsur Biaya</td>
	               <td>:</td>
	               <td>
	               <input type="text" name="unsur_biaya" id="unsur_biaya" required="required" style="width: 150px;">
	               </td>
	            </tr>
	           <tr>
	               <td>Satuan</td>
	               <td>:</td>
	               <td>
	               <input type="text" name="satuan" id="satuan" required style="width: 50px;">
	               </td>
	            </tr>
	           <tr>
	               <td>Harga Satuan</td>
	               <td>:</td>
	               <td><input class="easyui-numberbox" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%', onChange: function(a,b){
	              hit3();}" name="harga_satuan" id="harga_satuan" required style="width: 250px;">
	               </td>
	            </tr>
	            <tr>
	               <td>Jumlah Unit</td>
	               <td>:</td>
	               <td>Aktif &nbsp;<input class="easyui-numberbox"  name="junit_akt" id="junit_akt" required style="width: 50px;" data-options="onChange: function(a,b){
	              hit3();}" >
	               </td></tr>
	               <tr><td></td>
	               <td></td>
	               <td>Proy &nbsp;<input class="easyui-numberbox"  name="junit_proy" id="junit_proy" required style="width: 50px;" data-options="onChange: function(a,b){
	              hit3();}" ></td>
	              </tr>
	              <tr>
	               <td>Biaya Produksi</td>
	               <td>:</td>
	               <td>Aktif &nbsp;<input class="easyui-numberbox" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%'" name="bprod_akt" id="bprod_akt" required style="width: 250px;" readonly="true">
	               </td>
	               </tr>
	               <tr>
	               <td></td>
	               <td></td>
	               <td>Proy &nbsp;<input class="easyui-numberbox" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%'" name="bprod_proy" id="bprod_proy" required style="width: 250px;" readonly="true"></td>
	              </tr>

	            
	        </table>
	    </form>
	   
	   	<!-- Tombol Add, Edit, Hapus Datagrid -->
       	<div id="toolbar5">  
            <table align="center" style="padding: 0px; width: 99%;">
                <tr>
                    <td>
                        <a href="javascript:void(0)" class="btn btn-small btn-success" onclick="javascript:add5()"><i class="icon-plus-sign icon-large"></i>&nbsp;Add</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:edit5()"><i class="icon-edit"></i>&nbsp;Edit</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-danger" onclick="javascript:hapus5()"><i class="icon-trash icon-large"></i>&nbsp;Delete</a>
                    </td>
                    <td>&nbsp;</td>
                    <td align="right">
                    	<input class="easyui-searchbox" prompt="Silahkan Input Field" searcher="doSearch5"  style="width:300px"/>
                    </td>
                </tr>
            </table>  
        </div>  
        <!-- end tombol datagrid -->
        <!-- Tombol simpan & Batal Form -->
        <div id="t_dlg_dis-buttons5">
            <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:save5()"><i class="icon-save icon-large"></i>&nbsp;Simpan</a>
            <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:$('#dlg5').dialog('close')"><i class="icon-remove icon-large"></i>&nbsp;Batal</a>      
        </div>
	</div>
	<!-- Model end -->
</div>