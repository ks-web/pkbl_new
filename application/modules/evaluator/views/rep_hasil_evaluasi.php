		<style>
			table {
				border-spacing: 0px;
				border-collapse: separate;
				font-size: 14px;
				page-break-inside: avoid;
			}

			table, th, td {
				border: 0px solid black;
			}
			html {
				margin: 10px 50px
			}
			.pull-right {
				text-align: right;
			}
			.center {
				text-align: center;
			}
		</style>
	
		<table style="width: 100%;">
			<!-- <thead> -->
				<tr>
					<td class="pull-right">No Reg : <?=$pengusaha['id_mitra']?></td>
				</tr>
				<tr>
					<td>
					<center>
						<h3 style="margin-bottom: 0; margin-top: 0;">Usulan Pinjaman Modal Usaha</h3>
						<div>
							<strong>Usaha Kecil</strong> : <?=$pengusaha['nama']?>, <strong>Sektor Usaha</strong> : <?=$pengusaha['nama_sektor']?>
							<br />
							<strong>Alamat</strong> : <?=$pengusaha['alamat']?>, <?=$pengusaha['kelurahan']?>, <?=$pengusaha['kecamatan']?>, <?=$pengusaha['kota']?>, <strong>Telp.</strong> <?=$pengusaha['telepon']?>
						</div>
					</center>
					<hr  style="border: 1px solid black;"/>
					</td>
				</tr>
			<!-- </thead> -->
			<!-- <tbody> -->
				<tr>
					<td>
					<table style="width: 100%;">
						<!-- <tbody> -->
							<tr>
								<td width="15px"><strong>1.</strong></td>
								<td colspan="2"><strong>Data Penjualan Per Periode (<?php echo $pengusaha['jumlah_hari'] ?> Hari)</strong></td>
								
							</tr>
							<tr>
								<td width="15px"></td>
								<td><strong>A. Hasil Penjualan</strong></td>
								<td width="200px" style="text-align: right;" class="center"><b>Aktual Tahun Terakhir [Rp]</td>
								<td width="200px" style="text-align: right;" class="center">Proyeksi Tahun Depan [Rp]</td>
							</tr>
							<tr>
								<td width="15px"></td>
								<td>&nbsp;&nbsp;1. Hasil Penjualan</td>
								<td width="200px" class="pull-right"><?=number_format($pengusaha['penjualan_akt'], 0,",",".")?></td>
								<td width="200px" class="pull-right"><?=number_format($pengusaha['penjualan_proy'], 0,",",".")?></td>
							</tr>
							<tr>
								<td width="15px">&nbsp;</td>
								<td></td>
								<td width="200px"></td>
								<td width="200px"></td>
							</tr>
							
							<tr>
								<td width="15px"><strong></strong></td>
								<td><strong>B. Biaya Variable</strong></td>
								<td width="200px"></td>
								<td width="200px"></td>
							</tr>
							<tr>
								<td width="15px"><strong></strong></td>
								<td>&nbsp;&nbsp;&nbsp;&nbsp;1. Bahan Baku</td>
								<td width="200px" class="pull-right"><?=number_format($pengusaha['3a1_akt'], 0,",",".")?></td>
								<td width="200px" class="pull-right"><?=number_format($pengusaha['3a1_proy'], 0,",",".")?></td>
							</tr>
							<tr>
								<td width="15px"><strong></strong></td>
								<td>&nbsp;&nbsp;&nbsp;&nbsp;2. Bahan Pembantu</td>
								<td width="200px" class="pull-right"><?=number_format($pengusaha['3a2_akt'], 0,",",".")?></td>
								<td width="200px" class="pull-right"><?=number_format($pengusaha['3a2_proy'], 0,",",".")?></td>
							</tr>
							<tr>
								<td width="15px"><strong></strong></td>
								<td>&nbsp;&nbsp;&nbsp;&nbsp;3. Upah Tenaga Kerja</td>
								<td width="200px" class="pull-right"><?=number_format($pengusaha['3a3_akt'], 0,",",".")?></td>
								<td width="200px" class="pull-right"><?=number_format($pengusaha['3a3_proy'], 0,",",".")?></td>
							</tr>
							<tr>
								<td width="15px"><strong></strong></td>
								<td>&nbsp;&nbsp;&nbsp;&nbsp;4. Biaya Transportasi</td>
								<td width="200px" class="pull-right"><?=number_format($pengusaha['3a4_akt'], 0,",",".")?></td>
								<td width="200px" class="pull-right"><?=number_format($pengusaha['3a4_proy'], 0,",",".")?></td>
							</tr>
							<tr>
								<td width="15px"><strong></strong></td>
								<td>&nbsp;&nbsp;&nbsp;&nbsp;5. Biaya Lain-lain</td>
								<td width="200px" class="pull-right"><?=number_format($pengusaha['3a5_akt'], 0,",",".")?></td>
								<td width="200px" class="pull-right"><?=number_format($pengusaha['3a5_proy'], 0,",",".")?></td>
							</tr>
							<?php
								$total_3a_akt = $pengusaha['3a1_akt'] + $pengusaha['3a2_akt'] + $pengusaha['3a3_akt'] + $pengusaha['3a4_akt'] + $pengusaha['3a5_akt'];
								$total_3a_proy = $pengusaha['3a1_proy'] + $pengusaha['3a2_proy'] + $pengusaha['3a3_proy'] + $pengusaha['3a4_proy'] + $pengusaha['3a5_proy'];
							?>
							<tr>
								<td width="15px"><strong></strong></td>
								<td class="pull-right"><strong>Subtotal :</strong></td>
								<td width="200px" class="pull-right"><strong><?=number_format($pengusaha['tot_variabel_akt'], 0,",",".")?></strong></td>
								<td width="200px" class="pull-right"><strong><?=number_format($pengusaha['tot_variabel_proy'], 0,",",".")?></strong></td>
							</tr>
							<tr>
								<td width="15px"><strong></strong></td>
								<td><strong>C. Biaya Tetap</strong></td>
								<td width="200px"></td>
								<td width="200px"></td>
							</tr>
							<?php
								$gaji_tetap_akt=$pengusaha['3b1_akt']*($pengusaha['jumlah_hari']/30);
								$total_3b_akt = $gaji_tetap_akt + $pengusaha['3b2_akt'] + $pengusaha['3b3_akt'] + $pengusaha['3b4_akt'];
								$gaji_tetap_proy=$pengusaha['3b1_proy']*($pengusaha['jumlah_hari']/30);
								$total_3b_proy = $gaji_tetap_proy+ $pengusaha['3b2_proy'] + $pengusaha['3b3_proy'] + $pengusaha['3b4_proy'];
							?>
							<tr>
								<td width="15px"><strong></strong></td>
								<td>&nbsp;&nbsp;&nbsp;&nbsp;1. Gaji Pimpinan dan Pegawai Tetap</td>
								<td width="200px" class="pull-right"><?=number_format($gaji_tetap_akt, 0,",",".")?></td>
								<td width="200px" class="pull-right"><?=number_format($gaji_tetap_proy, 0,",",".")?></td>
							</tr>
							<tr>
								<td width="15px"><strong></strong></td>
								<td>&nbsp;&nbsp;&nbsp;&nbsp;2. Penyusutan (Depresiasi)</td>
								<td width="200px" class="pull-right"><?=number_format($pengusaha['3b2_akt'], 0,",",".")?></td>
								<td width="200px" class="pull-right"><?=number_format($pengusaha['3b2_proy'], 0,",",".")?></td>
							</tr>
							<tr>
								<td width="15px"><strong></strong></td>
								<td>&nbsp;&nbsp;&nbsp;&nbsp;3. Biaya Pemeliharaan</td>
								<td width="200px" class="pull-right"><?=number_format($pengusaha['3b3_akt'], 0,",",".")?></td>
								<td width="200px" class="pull-right"><?=number_format($pengusaha['3b3_proy'], 0,",",".")?></td>
							</tr>
							<tr>
								<td width="15px"><strong></strong></td>
								<td>&nbsp;&nbsp;&nbsp;&nbsp;4. Biaya Administrasi dan Umum</td>
								<td width="200px" class="pull-right"><?=number_format($pengusaha['3b4_akt'], 0,",",".")?></td>
								<td width="200px" class="pull-right"><?=number_format($pengusaha['3b4_proy'], 0,",",".")?></td>
							</tr>
							
							<tr>
								<td width="15px"><strong></strong></td>
								<td class="pull-right"><strong>Subtotal :</strong></td>
								<td width="200px" class="pull-right"><strong><?=number_format($pengusaha['tot_tetap_akt'], 0,",",".")?></strong></td>
								<td width="200px" class="pull-right"><strong><?=number_format($pengusaha['tot_tetap_proy'], 0,",",".")?></strong></td>
							</tr>
							<?php
								$totbiayaprod_act = $total_3a_akt + $total_3b_akt;
								$totbiayaprod_proy = $total_3a_proy + $total_3b_proy;
							?>
							<tr>
								<td width="15px"><strong></strong></td>
								<td>&nbsp;&nbsp;&nbsp;&nbsp;<strong>Total Biaya Produksi</strong></td>
								<td width="200px" class="pull-right"><strong><?=number_format($pengusaha['tot_produksi_akt'], 0,",",".")?></strong></td>
								<td width="200px" class="pull-right"><strong><?=number_format($pengusaha['tot_produksi_proy'], 0,",",".")?></strong></td>
							</tr>

							<?php
								$keuntungan_akt=$pengusaha['1b_akt']-$totbiayaprod_act;
								$keuntungan_proy=$pengusaha['1b_proy']-$totbiayaprod_proy;
							?>
							<tr>
								<td width="15px"><strong></strong></td>
								<td><strong>D. Keuntungan</strong></td>
								<td width="200px" class="pull-right"><strong><?=number_format($pengusaha['keuntungan_akt'], 0,",",".")?></strong></td>
								<td width="200px" class="pull-right"><strong><?=number_format($pengusaha['keuntungan_proy'], 0,",",".")?></strong></td>
							</tr>
							<?php
								$_4a1_proy = $totbiayaprod_proy - $totbiayaprod_act;
								$_4a2_proy = $pengusaha['4a2_proy'];
								$_4a3_proy = $pengusaha['4a3_proy'];
								$_4a4_proy = $pengusaha['4a4_proy'];
							?>
							<tr>
								<td width="15px"><strong>2.</strong></td>
								<td><strong>Proyeksi Kinerja Perusahaan.</strong></td>
								<td width="200px"></td>
								<td width="200px"></td>
							</tr>
							<?php 
								$ppertahun=$pengusaha['1b_proy']*(360/ $pengusaha['jumlah_hari']);
								$bopperthn=$totbiayaprod_proy*(360/ $pengusaha['jumlah_hari']);
								$lababersihperthn=$ppertahun-$bopperthn;
								$lababersihperbln=$lababersihperthn/12;
								$angsurperbln=$lababersihperbln*(30/100);

							?>
							<tr>
								<td width="15px"><strong></strong></td>
								<td>&nbsp;&nbsp;&nbsp;&nbsp;1. Penjualan per Tahun</td>
								<td width="200px" class="pull-right"></td>
								<td width="200px" class="pull-right"><?=number_format($ppertahun, 0,",",".")?></td>
							</tr>
							<tr>
								<td width="15px"><strong></strong></td>
								<td>&nbsp;&nbsp;&nbsp;&nbsp;2. Total Biaya Produksi per Tahun</td>
								<td width="200px" class="pull-right"></td>
								<td width="200px" class="pull-right"><?=number_format($bopperthn, 0,",",".")?></td>
							</tr>
							<tr>
								<td width="15px"><strong></strong></td>
								<td>&nbsp;&nbsp;&nbsp;&nbsp;3. Keuntungan Bersih per Tahun</td>
								<td width="200px" class="pull-right"></td>
								<td width="200px" class="pull-right"><?=number_format($lababersihperthn, 0,",",".")?></td>
							</tr>
							<tr>
								<td width="15px"><strong></strong></td>
								<td>&nbsp;&nbsp;&nbsp;&nbsp;4. Keuntungan Bersih per Bulan</td>
								<td width="200px" class="pull-right"></td>
								<td width="200px" class="pull-right"><?= number_format($lababersihperbln, 0,",",".")?></td>
							</tr>
							<tr>
								<td width="15px"><strong></strong></td>
								<td>&nbsp;&nbsp;&nbsp;&nbsp;5. Kemampuan Mengangsur per Bulan</td>
								<td width="200px" class="pull-right"></td>
								<td width="200px" class="pull-right"><?= number_format($angsurperbln, 0,",",".")?></td>
							</tr>
							<tr>
								<td width="15px"><strong></strong></td>
								<td>&nbsp;&nbsp;&nbsp;&nbsp;6. Total Asset yang dimiliki</td>
								<td width="200px" class="pull-right"></td>
								<td width="200px" class="pull-right"><?= number_format($pengusaha['4d_proy'], 0,",",".")?></td>
							</tr>
							<tr>
								<td width="15px"><strong></strong></td>
								<td><strong>3. Kebutuhan Tambahan Modal Investasi</strong></td>
								<td width="200px"></td>
								<td width="200px"></td>
							</tr>

							<tr>
								<td width="15px"><strong></strong></td>
								<td><strong>4. Usulan Bantuan Modal Kerja</strong></td>
								<td width="200px"></td>
								<td width="200px"></td>
							</tr>
							<tr>
								<td width="15px"><strong></strong></td>
								<td>&nbsp;&nbsp;&nbsp;&nbsp;1. Biaya Produksi (Proyeksi - Aktual)</td>
								<td width="200px" class="pull-right"></td>
								<td width="200px" class="pull-right"><?=number_format($_4a1_proy, 0,",",".")?></td>
							</tr>
							<tr>
								<td width="15px"><strong></strong></td>
								<td>&nbsp;&nbsp;&nbsp;&nbsp;2. Hutang Jangka Pendek</td>
								<td width="200px" class="pull-right"></td>
								<td width="200px" class="pull-right"><?=number_format($_4a2_proy, 0,",",".")?></td>
							</tr>
							<tr>
								<td width="15px"><strong></strong></td>
								<td>&nbsp;&nbsp;&nbsp;&nbsp;3. Piutang</td>
								<td width="200px" class="pull-right"></td>
								<td width="200px" class="pull-right"><?=number_format($_4a3_proy, 0,",",".")?></td>
							</tr>
							<tr>
								<td width="15px"><strong></strong></td>
								<td>&nbsp;&nbsp;&nbsp;&nbsp;4. Cadangan Kas</td>
								<td width="200px" class="pull-right"></td>
								<td width="200px" class="pull-right"><?=number_format($_4a4_proy, 0,",",".")?></td>
							</tr>
							<?php
								$tot_4a = ($_4a1_proy + $_4a2_proy ) - ($_4a3_proy + $_4a4_proy);
 							?>
							<tr>
								<td width="15px"><strong></strong></td>
								<td class="pull-right"><strong>Subtotal :</strong></td>
								<td width="200px" class="pull-right"><strong></strong></td>
								<td width="200px" class="pull-right"><strong><?=number_format($pengusaha['nilai_rekomendasi'], 0,",",".")?></strong></td>
							</tr>
							<?php
								$_4b_proy = 0;
								$_4b_total_act = $tot_4a;
								$pembulantan = 100000;
								
								$_4b_total_proy = $_4b_total_act - ($_4b_total_act%$pembulantan);
								$_4c_proy = $pengusaha['1b_akt'] - $totbiayaprod_act;
								$_4d_proy = $pengusaha['4d_proy'];
								$_4e_proy = ($_4c_proy*12)+$pengusaha['4d_proy'];
							?>
							
							<tr>
								<td width="15px"><strong></strong></td>
								<td><strong>5. Usulan Besanya Pinjaman Modal Usaha :</strong></td>
								<td width="200px" class="pull-right"><?=number_format($pengusaha['nilai_usulan'], 0,",",".")?> , dibulatkan</td>
								<td width="200px" class="pull-right"><?=number_format($pengusaha['angsuran_pokok'], 0,",",".")?></td>
							</tr>
							<?php
							$jasaadmin=($_4b_total_proy*(6/100))*2;
							$totangsurandanadmin=$_4b_total_proy+$jasaadmin;
							$angsuranperbulan=$totangsurandanadmin/24;
							?>
							<tr>
								<td width="15px"><strong></strong></td>
								<td>&nbsp;&nbsp;&nbsp;&nbsp;1. Jasa Admin: <?=number_format($pengusaha['bunga'], 0,",",".")?>,00% x <?=number_format($pengusaha['tahun'], 0,",",".")?> Tahun</td>
								<td width="200px" class="pull-right"></td>
								<td width="200px" class="pull-right"><?=number_format($pengusaha['angsuran_bunga'], 0,",",".")?></td>
							</tr>
							
							<tr>
								<td width="15px"><strong></strong></td>
								<td>&nbsp;&nbsp;&nbsp;&nbsp;2. Jumlah Angsuran dan Jasa Admin</td>
								<td width="200px" class="pull-right"></td>
								<td width="200px" class="pull-right"><?=number_format($pengusaha['total_pinjaman_bunga'], 0,",",".")?></td>
							</tr>
							
							<tr>
								<td width="15px"><strong></strong></td>
								<td>&nbsp;&nbsp;&nbsp;&nbsp;3. Angsuran Perbulan</td>
								<td width="200px" class="pull-right"></td>
								<td width="200px" class="pull-right"><?=number_format($pengusaha['angsuran_bulanan'], 0,",",".")?></td>
							</tr>
							
							
						<!-- </tbody> -->
					</table>
					<hr  style="border: 1px solid black;"/>
					<table style="width: 100%;">
						<!-- <tbody> -->
							
							<tr>
				<td><br><br></td>
			</tr>
			<tr>
				
				<td colspan="6" style="font-family: font-size:14px;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;vertical-align:top;text-align:right;">
					<center>
					<br>Disetujui Oleh<br><?= divisiTitle?><br><br><br><br><font style="text-decoration: underline;"><?=managerName?></font><br><?=managerTitle?>
					</center>
				</td>
				<td style="font-family:font-size:14px;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;vertical-align:top;text-align:right;"></td>
				<td style="font-family:font-size:14px;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;vertical-align:top;text-align:right;"></td>
				<td style="font-family:font-size:14px;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;vertical-align:top;text-align:right;"></td>
				<td colspan="3" style="font-family:font-size:14px;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;vertical-align:top;text-align:right;">
					<center>
					<?=$kota?>, <?=tanggal_display($pengusaha['tanggal_create'])?></strong><br>Disiapkan Oleh<br><br><br><br><br><font style="text-decoration: underline;"><?= sptTitle?></font><br><?= sptJabatan?>
					</center>
				</td>
			</tr>
						<!-- </tbody> -->
					</table></td>
				</tr>
			<!-- </tbody> -->
			<!-- <tfoot> -->
				<tr>
					<td></td>
				</tr>
			<!-- </tfoot> -->
		</table>