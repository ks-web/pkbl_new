<?php
$app7 = 'Biaya Tenaga Kerja Tak Tetap'; // nama aplikasi
$module7 = 'evaluator';
$appLink7 = 'K_kttetap_his'; // controller
$idField7  = 'id_kttetap'; //field key table
?>

<script>
	var url7;
	var app7 = "<?=$app7?>";
	var appLink7 = '<?=$appLink7?>';
	var module7 = '<?=$module7?>';
	var idField7 = '<?=$idField7?>';
	
    function add7(){
        $('#dlg7').dialog('open').dialog('setTitle','Tambah ' + app7);
		$('#fm7').form('clear');
	    $('#tbl7').html('save7');
		url7 = '<?=base_url($module7 . '/' . $appLink7 . '/create')?>/'+idParent;
    }
    function edit7(){
        var row = $('#dg7').datagrid('getSelected');
		if (row){
			$('#tbl7').html('Simpan');
			$('#dlg7').dialog('open').dialog('setTitle','Edit '+ app7);
			$('#fm7').form('load',row);
			url7 = '<?=base_url($module7 . '/' . $appLink7 . '/update')?>/'+row[idField7]+'/'+idParent;
	    }
    }
    function hapus7(){
        var row = $('#dg7').datagrid('getSelected');		
	    if (row){
			$.messager.confirm('Confirm','Apakah Anda Yakin Akan Menghapus ini?',function(r){
				if (r){
					$.post('<?=base_url($module7 . '/' . $appLink7 . '/delete')?>/'+row[idField7],function(result){
						if (result.success){
							$('#dg7').datagrid('reload');	// reload the user data
							$('#dg7').datagrid('reload');
						} else {
							$.messager.show({	// show error message
								title: 'Error',
								msg:result.msg
							});
						}
					},'json');
				}
			});
		}
    }
    function save7(){
	   $('#fm7').form('submit',{
	    	url: url7,
	    	onSubmit: function(){
	    		return $(this).form('validate');
	    	},
	    	success: function(result){
	    		var result = eval('('+result+')');
	    		if (result.success){
	    			$('#dlg7').dialog('close');		
	    			$('#dg7').datagrid('reload');
	                
	    		} else {
	             $.messager.alert('Error Message',result.msg,'error');
	    		}
	    	}
	    });
	}
	function doSearch7(value){
	    $('#dg7').datagrid('load',{    
	    	q:value  
	    });
	}
	function reload7(value){
		$('#dg7').datagrid({    
	    	url: '<?=base_url($module7 . '/' . $appLink7 . '/read/')?>/'+idParent  
	    });
	}

	function calculate7(){
		var total;
		var harga_satuan = parseInt($("#fm7 #harga_satuan").numberbox('getValue'));
		var produksi = harga_satuan * parseInt($("#fm7 #produksi_akt").numberbox('getValue'));
		$('#fm7 #produksi_proy').numberbox('setValue', produksi);
		var penjualan = harga_satuan * parseInt($("#fm7 #penjualan_akt").numberbox('getValue'));
		$('#fm7 #penjualan_proy').numberbox('setValue', penjualan);
	}
</script>
 
<div class="tabs-container">                
	<table id="dg7" class="easyui-datagrid" 
	data-options="  
	    url: '<?=base_url($module7 . '/' . $appLink7 . '/read')?>',
		singleSelect:'true', 
	    title:'<?=$app7?>',
	    toolbar:'#toolbar7',
	    iconCls:'icon-cog',
	    rownumbers:'true',  
	    idField:'<?=$idField7?>', 
	    pagination:'true',
	    fitColumns:'true',
	    pageList: [10,20,30]
	    "
	>
	    <thead>
	    	<th field="id_kttetap" width="100" hidden sortable="true" rowspan="2">Uraian</th>
	    	<th field="uraian" width="100" sortable="true" rowspan="2">Uraian</th>
			<th field="satuan" width="100" sortable="true" rowspan="2">Satuan</th>
			<th field="harga_satuan" width="100" sortable="true" rowspan="2">Harga Satuan</th>
			<th colspan="2">Produksi</th>
			<th colspan="2">Penjualan</th>
	    </thead>
	    <thead>
			<th field="produksi_akt" width=100" sortable="true">Akt</th>
			<th field="produksi_proy" width=100" sortable="true">Proy</th>
			<th field="penjualan_akt" width=100" sortable="true">Akt</th>
			<th field="penjualan_proy" width=100" sortable="true">Proy</th>
	    </thead>
	</table>
	
	<!-- Model Start -->
	<div id="dlg7" class="easyui-dialog" style="width:500px; height:auto; padding:10px" closed="true" buttons="#t_dlg_dis-buttons7" >
	    <form id="fm7" method="post" enctype="multipart/form-data" action="">
	        <table width="100%" align="center" border="0">
	            <tr>
	                <td>Uraian</td>
	                <td>:</td>
	                <td><textarea name="uraian" id="uraian" required></textarea>
	                </td>
	            </tr>
	            <tr>
	                <td>Satuan</td>
	                <td>:</td>
	                <td>
	                	<input type="text" name="satuan" id="satuan" required style="width: 250px;">
	                </td>
	            </tr>
	            <tr>
	                <td>Harga Satuan</td>
	                <td>:</td>
	                <td>
	                    <input class="easyui-numberbox" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%',onChange: function(a, b){ calculate7(); }" name="harga_satuan" id="harga_satuan" required style="width: 250px;">
	                </td>
	            </tr>
	            <tr>
	                <td>Produksi Atk</td>
	                <td>:</td>
	                <td>
	                	<input class="easyui-numberbox" data-options="label:'',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'',width:'100%',onChange: function(a, b){ calculate7(); }" name="produksi_akt" id="produksi_akt" required style="width: 150px;">
	                </td>
	            </tr>
	            <tr>
	                <td>Produksi Proy</td>
	                <td>:</td>
	                <td>
	                    <input class="easyui-numberbox" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%'" name="produksi_proy" id="produksi_proy" required style="width: 250px;" readonly="readonly">
	                </td>
	            </tr>
	            <tr>
	                <td>Penjualan Atk</td>
	                <td>:</td>
	                <td>
	                	<input class="easyui-numberbox" data-options="label:'',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'',width:'100%',onChange: function(a, b){ calculate7(); }" name="penjualan_akt" id="penjualan_akt" required style="width: 150px;">
	                </td>
	            </tr>
	            <tr>
	                <td>Penjualan Proy</td>
	                <td>:</td>
	                <td>
	                    <input class="easyui-numberbox" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%'" name="penjualan_proy" id="penjualan_proy" required style="width: 250px;" readonly="readonly">
	                </td>
	            </tr>		            
	        </table>
	    </form>
	   
	   	<!-- Tombol Add, Edit, Hapus Datagrid -->
       	<div id="toolbar7">  
            <table align="center" style="padding: 0px; width: 99%;">
                <tr>
                    <td>
                        <a href="javascript:void(0)" class="btn btn-small btn-success" onclick="javascript:add7()"><i class="icon-plus-sign icon-large"></i>&nbsp;Add</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:edit7()"><i class="icon-edit"></i>&nbsp;Edit</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-danger" onclick="javascript:hapus7()"><i class="icon-trash icon-large"></i>&nbsp;Delete</a>
                    </td>
                    <td>&nbsp;</td>
                    <td align="right">
                    	<input class="easyui-searchbox" prompt="Silahkan Input Field" searcher="doSearch7"  style="width:300px"/>
                    </td>
                </tr>
            </table>  
        </div>  
        <!-- end tombol datagrid -->
        <!-- Tombol simpan & Batal Form -->
        <div id="t_dlg_dis-buttons7">
            <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:save7()"><i class="icon-save icon-large"></i>&nbsp;Simpan</a>
            <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:$('#dlg7').dialog('close')"><i class="icon-remove icon-large"></i>&nbsp;Batal</a>      
        </div>
	</div>
	<!-- Model end -->
</div>