<?php
$app2 = 'Periode Perputaran Produksi'; // nama aplikasi
$module2 = 'evaluator';
$appLink2 = 'K_perputaran_his'; // controller
$idField2  = 'id_pperputaran'; //field key table
?>

<script>
	var url2;
	var app2 = "<?=$app2?>";
	var appLink2 = '<?=$appLink2?>';
	var module2 = '<?=$module2?>';
	var idField2 = '<?=$idField2?>';
	
    function add2(){
        $('#dlg2').dialog('open').dialog('setTitle','Tambah ' + app2);
		$('#fm2').form('clear');
	    $('#tbl2').html('Save2');
		url2 = '<?=base_url($module2 . '/' . $appLink2 . '/create')?>/'+idParent;
    }
    function edit2(){
        var row = $('#dg2').datagrid('getSelected');
		if (row){
			$('#tbl2').html('Simpan');
			$('#dlg2').dialog('open').dialog('setTitle','Edit '+ app2);
			$('#fm2').form('load',row);
			url2 = '<?=base_url($module2 . '/' . $appLink2 . '/update')?>/'+row[idField2]+'/'+idParent;;
	    }
    }
    function hapus2(){
        var row = $('#dg2').datagrid('getSelected');		
	    if (row){
			$.messager.confirm('Confirm','Apakah Anda Yakin Akan Menghapus ini?',function(r){
				if (r){
					$.post('<?=base_url($module2 . '/' . $appLink2 . '/delete')?>/'+row[idField2],function(result){
						if (result.success){
							$('#dg2').datagrid('reload');	// reload the user data
							$('#dg2').datagrid('reload');
						} else {
							$.messager.show({	// show error message
								title: 'Error',
								msg:result.msg
							});
						}
					},'json');
				}
			});
		}
    }
    function save2(){
	   $('#fm2').form('submit',{
	    	url: url2,
	    	onSubmit: function(){
	    		return $(this).form('validate');
	    	},
	    	success: function(result){
	    		var result = eval('('+result+')');
	    		if (result.success){
	    			$('#dlg2').dialog('close');		
	    			$('#dg2').datagrid('reload');
	                
	    		} else {
	             $.messager.alert('Error Message',result.msg,'error');
	    		}
	    	}
	    });
	}
	function doSearch2(value){
	    $('#dg2').datagrid('load',{    
	    	q:value  
	    });
	}
	function reload2(value){
		$('#dg2').datagrid({    
	    	url: '<?=base_url($module2 . '/' . $appLink2 . '/read/')?>/'+idParent  
	    });
	}
</script>
 
<div class="tabs-container">                
	<table id="dg2" class="easyui-datagrid" 
	data-options="  
	    url: '<?=base_url($module2 . '/' . $appLink2 . '/read')?>',
		singleSelect:'true', 
	    title:'<?=$app2?>',
	    toolbar:'#toolbar2',
	    rownumbers:'true',  
	    idField:'<?=$idField2?>', 
	    pagination:'true',
	    fitColumns:'true',
	    pageList: [10,20,30]
	    "
	>
	    <thead>
	    	<th field="id_pperputaran" width=100" sortable="true">ID pperputaran</th>
	        <th field="uraian" width=100" sortable="true">Uraian</th>
			<th field="jumlah_hari" width=100" sortable="true">Jumlah hari</th>
	    </thead>
	</table>
	
	<!-- Model Start -->
	<div id="dlg2" class="easyui-dialog" style="width:500px; height:auto; padding:10px" closed="true" buttons="#t_dlg_dis-buttons2" >
	    <form id="fm2" method="post" enctype="multipart/form-data" action="">
	        <table width="100%" align="center" border="0">
	            <tr>
	                <td>Uraian</td>
	                <td>:</td>
	                <td><textarea name="uraian" id="uraian" required s></textarea>
	                </td>
	            </tr>
	            <tr>
	               <td>Jumlah Hari</td>
	               <td>:</td>
	               <td>
	               <input type="text" name="jumlah_hari" id="jumlah_hari" required style="width: 50px;">
	               </td>
	            </tr>
	        </table>
	    </form>
	   
	   	<!-- Tombol Add, Edit, Hapus Datagrid -->
       	<div id="toolbar2">  
            <table align="center" style="padding: 0px; width: 99%;">
                <tr>
                    <td>
                        <!-- <a href="javascript:void(0)" class="btn btn-small btn-success" onclick="javascript:add2()"><i class="icon-plus-sign icon-large"></i>&nbsp;Add</a> -->
                        <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:edit2()"><i class="icon-edit"></i>&nbsp;Edit</a>
                        <!-- <a href="javascript:void(0)" class="btn btn-small btn-danger" onclick="javascript:hapus2()"><i class="icon-trash icon-large"></i>&nbsp;Delete</a> -->
                    </td>
                    <td>&nbsp;</td>
                    <td align="right">
                    	<input class="easyui-searchbox" prompt="Silahkan Input Field" searcher="doSearch2"  style="width:300px"/>
                    </td>
                </tr>
            </table>  
        </div>  
        <!-- end tombol datagrid -->
        <!-- Tombol simpan & Batal Form -->
        <div id="t_dlg_dis-buttons2">
            <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:save2()"><i class="icon-save icon-large"></i>&nbsp;Simpan</a>
            <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:$('#dlg2').dialog('close')"><i class="icon-remove icon-large"></i>&nbsp;Batal</a>      
        </div>
	</div>
	<!-- Model end -->
</div>