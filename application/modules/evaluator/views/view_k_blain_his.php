<?php
$app10 = 'Biaya Lain - Lain'; // nama aplikasi
$module10 = 'evaluator';
$appLink10 = 'K_blain_his'; // controller
$idField10  = 'id_blain'; //field key table
?>

<script>
	var url10;
	var app10 = "<?=$app10?>";
	var appLink10 = '<?=$appLink10?>';
	var module10 = '<?=$module10?>';
	var idField10 = '<?=$idField10?>';
	
	function hit10(){
		var harga=$("#fm10 #harga_satuan").numberbox('getValue');
		var prod_akt=$("#fm10 #junit_akt").numberbox('getValue');
		var prod_proy=$("#fm10 #junit_proy").numberbox('getValue');

		var sumharga=harga*prod_akt;
		$("#fm10 #biaya_akt").numberbox('setValue', sumharga);

		var sumharga2=harga*prod_proy;
		$("#fm10 #biaya_proy").numberbox('setValue', sumharga2);
	// console.log(harga);
	}	

    function add10(){
        $('#dlg10').dialog('open').dialog('setTitle','Tambah ' + app10);
		$('#fm10').form('clear');
	    $('#tbl10').html('Save10');
		url10 = '<?=base_url($module10 . '/' . $appLink10 . '/create')?>/'+idParent;
    }
    function edit10(){
        var row = $('#dg10').datagrid('getSelected');
		if (row){
			$('#tbl10').html('Simpan');
			$('#dlg10').dialog('open').dialog('setTitle','Edit '+ app10);
			$('#fm10').form('load',row);
			url10 = '<?=base_url($module10 . '/' . $appLink10 . '/update')?>/'+row[idField10];
	    }
    }
    function hapus10(){
        var row = $('#dg10').datagrid('getSelected');		
	    if (row){
			$.messager.confirm('Confirm','Apakah Anda Yakin Akan Menghapus ini?',function(r){
				if (r){
					$.post('<?=base_url($module10. '/' . $appLink10 . '/delete')?>/'+row[idField10],function(result){
						if (result.success){
							$('#dg10').datagrid('reload');	// reload the user data
							$('#dg10').datagrid('reload');
						} else {
							$.messager.show({	// show error message
								title: 'Error',
								msg:result.msg
							});
						}
					},'json');
				}
			});
		}
    }
    function save10(){
	   $('#fm10').form('submit',{
	    	url: url10,
	    	onSubmit: function(){
	    		return $(this).form('validate');
	    	},
	    	success: function(result){
	    		var result = eval('('+result+')');
	    		if (result.success){
	    			$('#dlg10').dialog('close');		
	    			$('#dg10').datagrid('reload');
	                
	    		} else {
	             $.messager.alert('Error Message',result.msg,'error');
	    		}
	    	}
	    });
	}
	function doSearch10(value){
	    $('#dg10').datagrid('load',{    
	    	q:value  
	    });
	}
	function reload10(value){
		$('#dg10').datagrid({    
	    	url: '<?=base_url($module10 . '/' . $appLink10 . '/read/')?>/'+idParent  
	    });
	}
</script>
 
<div class="tabs-container">                
	<table id="dg10" class="easyui-datagrid" 
	data-options="  
	    url: '<?=base_url($module10 . '/' . $appLink10 . '/read')?>',
		singleSelect:'true', 
	    title:'<?=$app10?>',
	    toolbar:'#toolbar10',
	    iconCls:'icon-cog',
	    rownumbers:'true',  
	    idField:'<?=$idField10?>', 
	    pagination:'true',
	    fitColumns:'true',
	    pageList: [10,20,30]
	    "
	>
	    <thead>
	    <tr>
	    	<th rowspan="2" field="id_blain" width="100" hidden sortable="true">ID</th>
	        <th rowspan="2" field="unsur_biaya" width="100" sortable="true">Unsur Biaya	</th>
			<th rowspan="2" field="satuan" width="100" sortable="true">Satuan</th>
			<th rowspan="2" field="harga_satuan" width="100" sortable="true">Harga Satuan</th>
			<th colspan="2"  width="100" sortable="true">Jumlah Unit</th>
			<th colspan="2"  width="100" sortable="true">Biaya Produksi</th>
		</tr>
			<th field="junit_akt" width="100" sortable="true">Akt</th>
			<th field="junit_proy" width="100" sortable="true">Proy</th>
			<th field="biaya_akt" width="100" sortable="true">akt</th>
			<th field="biaya_proy" width="100" sortable="true">Proy</th>



	    </thead>
	</table>
	
	<!-- Model Start -->
	<div id="dlg10" class="easyui-dialog" style="width:500px; height:auto; padding:10px" closed="true" buttons="#t_dlg_dis-buttons10" >
	    <form id="fm10" method="post" enctype="multipart/form-data" action="">
	    	
	        <table width="100%" align="center" border="0">
	            <tr>
	               <td>Unsur Biaya</td>
	               <td>:</td>
	               <td>
	               <input type="text" name="unsur_biaya" id="unsur_biaya" required="required" style="width: 150px;">
	               </td>
	            </tr>
	           <tr>
	               <td>Satuan</td>
	               <td>:</td>
	               <td>
	               <input type="text" name="satuan" id="satuan" required style="width: 50px;">
	               </td>
	            </tr>
	           <tr>
	               <td>Harga Satuan</td>
	               <td>:</td>
	               <td><input class="easyui-numberbox" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%', onChange: function(a,b){
	              hit10();}" name="harga_satuan" id="harga_satuan" required style="width: 250px;">
	               </td>
	            </tr>
	            <tr>
	               <td>Jumlah Unit</td>
	               <td>:</td>
	               <td>Aktif &nbsp;<input class="easyui-numberbox"  name="junit_akt" id="junit_akt" required style="width: 50px;" data-options="onChange: function(a,b){
	              hit10();}" >
	               </td></tr>
	               <tr><td></td>
	               <td></td>
	               <td>Proy &nbsp;<input class="easyui-numberbox"  name="junit_proy" id="junit_proy" required style="width: 50px;" data-options="onChange: function(a,b){
	              hit10();}" ></td>
	              </tr>
	              <tr>
	               <td>Biaya Produksi</td>
	               <td>:</td>
	               <td>Aktif &nbsp;<input class="easyui-numberbox" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%'" name="biaya_akt" id="biaya_akt" required style="width: 250px;" readonly="true">
	               </td>
	               </tr>
	               <tr>
	               <td></td>
	               <td></td>
	               <td>Proy &nbsp;<input class="easyui-numberbox" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%'" name="biaya_proy" id="biaya_proy" required style="width: 250px;" readonly="true"></td>
	              </tr>

	            
	        </table>
	    </form>
	   
	   	<!-- Tombol Add, Edit, Hapus Datagrid -->
       	<div id="toolbar10">  
            <table align="center" style="padding: 0px; width: 99%;">
                <tr>
                    <td>
                        <a href="javascript:void(0)" class="btn btn-small btn-success" onclick="javascript:add10()"><i class="icon-plus-sign icon-large"></i>&nbsp;Add</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:edit10()"><i class="icon-edit"></i>&nbsp;Edit</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-danger" onclick="javascript:hapus10()"><i class="icon-trash icon-large"></i>&nbsp;Delete</a>
                    </td>
                    <td>&nbsp;</td>
                    <td align="right">
                    	<input class="easyui-searchbox" prompt="Silahkan Input Field" searcher="doSearch10"  style="width:300px"/>
                    </td>
                </tr>
            </table>  
        </div>  
        <!-- end tombol datagrid -->
        <!-- Tombol simpan & Batal Form -->
        <div id="t_dlg_dis-buttons10">
            <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:save10()"><i class="icon-save icon-large"></i>&nbsp;Simpan</a>
            <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:$('#dlg10').dialog('close')"><i class="icon-remove icon-large"></i>&nbsp;Batal</a>      
        </div>
	</div>
	<!-- Model end -->
</div>