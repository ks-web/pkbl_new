<?php
$app121 = 'Jasa Adm'; // nama aplikasi
$module121 = 'evaluator';
$appLink121 = 'Jasa_adm'; // controller
$idField121 = 'id_mitra'; //field key table
?>

<script>
	var url121;
	var app121 = "<?=$app121?>";
	var appLink121 = '<?=$appLink121?>';
	var module121 = '<?=$module121?>';
	var idField121= '<?=$idField121?>';
	
    function add121(){
        $('#dlg121').dialog('open').dialog('setTitle','Tambah ' + app121);
		$('#fm121').form('clear');
	    $('#tbl121').html('Save121');
		url121 = '<?=base_url($module121 . '/' . $appLink121 . '/create')?>/'+idParent;
    }
    function edit121(){
        var row = $('#dg121').datagrid('getSelected');
		if (row){
			$('#tbl121').html('Simpan');
			$('#dlg121').dialog('open').dialog('setTitle','Edit '+ app121);
			$('#fm121').form('load',row);
			url121 = '<?=base_url($module121 . '/' . $appLink121 . '/update')?>/'+row[idField121]+'/'+idParent;
	    }
    }
    function hapus121(){
        var row = $('#dg121').datagrid('getSelected');		
	    if (row){
			$.messager.confirm('Confirm','Apakah Anda Yakin Akan Menghapus ini?',function(r){
				if (r){
					$.post('<?=base_url($module121 . '/' . $appLink121 . '/delete')?>/'+row[idField121],function(result){
						if (result.success){
							$('#dg121').datagrid('reload');	// reload the user data
							$('#dg121').datagrid('reload');
						} else {
							$.messager.show({	// show error message
								title: 'Error',
								msg:result.msg
							});
						}
					},'json');
				}
			});
		}
    }
    function save121(){
	   $('#fm121').form('submit',{
	    	url: url121,
	    	onSubmit: function(){
	    		return $(this).form('validate');
	    	},
	    	success: function(result){
	    		var result = eval('('+result+')');
	    		if (result.success){
	    			$('#dlg121').dialog('close');		
	    			$('#dg121').datagrid('reload');
	                
	    		} else {
	             $.messager.alert('Error Message',result.msg,'error');
	    		}
	    	}
	    });
	}
	function doSearch121(value){
	    $('#dg121').datagrid('load',{    
	    	q:value  
	    });
	}
	function reload121(value){
		$('#dg121').datagrid({    
	    	url: '<?=base_url($module121 . '/' . $appLink121 . '/read/')?>/'+idParent  
	    });
	}
</script>
 
<div class="tabs-container">                
	<table id="dg121" class="easyui-datagrid" 
	data-options="  
	    url: '<?=base_url($module121 . '/' . $appLink121 . '/read/')?>',
		singleSelect:'true', 
	    title:'<?=$app121?>',
	    toolbar:'#toolbar121',
	    iconCls:'icon-cog',
	    rownumbers:'true',  
	    idField:'<?=$idField121?>', 
	    pagination:'true',
	    fitColumns:'true',
	    pageList: [10,20,30]
	    "
	>
	    <thead>
	    	<th field="id_mitra" width="100" hidden sortable="true">ID Asset</th>
	        <th field="bunga" width="100" sortable="true">Bunga ( % )</th>
			<th field="tahun" width="100" sortable="true">Tahun</th>
			
	    </thead>
	</table>
	
	<!-- Model Start -->
	<div id="dlg121" class="easyui-dialog" style="width:500px; height:auto; padding:10px" closed="true" buttons="#t_dlg_dis-buttons121" >
	    <form id="fm121" method="post" enctype="multipart/form-data" action="">
	       <table width="100%" align="center" border="0">
	            <tr>
	               <td>Bunga</td>
	               <td>:</td>
	               <td>
	               <input class="easyui-numberbox" data-options="label:'%',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',suffix:' %',width:'100%'"  name="bunga" id="bunga" required="required" style="width: 100px;">
	               </td>
	            </tr>
	           <tr>
	               <td>Tahun</td>
	               <td>:</td>
	               <td>
	               <input class="easyui-numberbox" data-options="label:'Tahun',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',suffix:' Tahun',width:'100%'" name="tahun" id="tahun" required style="width: 120px;">
	               </td>
	            
	        </table>
	    </form>
	   
	   	<!-- Tombol Add, Edit, Hapus Datagrid -->
       	<div id="toolbar121">  
            <table align="center" style="padding: 0px; width: 99%;">
                <tr>
                    <td>
                        <!-- <a href="javascript:void(0)" class="btn btn-small btn-success" onclick="javascript:add121()"><i class="icon-plus-sign icon-large"></i>&nbsp;Add</a> -->
                        <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:edit121()"><i class="icon-edit"></i>&nbsp;Edit</a>
                        <!-- <a href="javascript:void(0)" class="btn btn-small btn-danger" onclick="javascript:hapus121()"><i class="icon-trash icon-large"></i>&nbsp;Delete</a> -->
                    </td>
                    <td>&nbsp;</td>
                    <td align="right">
                    	<input class="easyui-searchbox" prompt="Silahkan Input Field" searcher="doSearch121"  style="width:300px"/>
                    </td>
                </tr>
            </table>  
        </div>  
        <!-- end tombol datagrid -->
        <!-- Tombol simpan & Batal Form -->
        <div id="t_dlg_dis-buttons121">
            <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:save121()"><i class="icon-save icon-large"></i>&nbsp;Simpan</a>
            <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:$('#dlg121').dialog('close')"><i class="icon-remove icon-large"></i>&nbsp;Batal</a>      
        </div>
	</div>
	<!-- Model end -->
</div>