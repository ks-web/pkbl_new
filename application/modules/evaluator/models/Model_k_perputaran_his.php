<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	Class Model_k_perputaran_his extends MY_Model {
		function __construct() {
			parent::__construct();
			$this->_table = "tk_pperputaran"; // for insert, update, delete
			$this->_view = "tk_pperputaran_vd"; // for call view
			$this->_order = 'asc';
			$this->_sort = 'id_pperputaran';
			$this->_page = 1;
			$this->_rows = 10;

			$this->_create = true;
			$this->_update = true;

			if ($this->uri->segment(4)) {
				$this->_filter = array('id_mitra' => $this->uri->segment(4));
			}

			//parameter from post/get - search function
			if ($this->input->post('q')) {
				$this->_like = array(
					'id_pperputaran' => $this->input->post('q'),
					'id_mitra' => $this->input->post('q'),
					'uraian' => $this->input->post('q')
				);
			}

			//$this->_param = array('id_pperputaran' => $this->uri->segment(4));

			//data array for input to database
			$this->_data = array(
						
				// 'id_pperputaran' => $this->input->post('id_pperputaran'),
				// 'id_mitra' => $this->input->post('id_mitra'),
				'uraian' => $this->input->post('uraian'),
				'jumlah_hari' => $this->input->post('jumlah_hari')
			);
			
		}


	}
