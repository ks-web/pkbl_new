<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	Class Model_jasa_adm extends MY_Model {
		function __construct() {
			parent::__construct();
			$this->_table = "tpenilaian_survey_pengusaha_evaluator"; // for insert, update, delete
			$this->_view = "jasaadm_vd"; // for call view
			$this->_order = 'asc';
			$this->_sort = 'id_mitra';
			$this->_page = 1;
			$this->_rows = 10;

			$this->_create = true;
			$this->_update = true;

			if ($this->uri->segment(4)) {
				$this->_filter = array('id_mitra' => $this->uri->segment(4));
			}

			//parameter from post/get - search function
			if ($this->input->post('q')) {
				$this->_like = array(
					'id_mitra' => $this->input->post('q'),
					'tahun' => $this->input->post('q'),
					'bunga' => $this->input->post('q')
				);
			}

			$this->_param = array('id_asset' => $this->input->post('id_asset'));

			//data array for input to database
			$this->_data = array(
				// 'id_asset' => $this->input->post('id_asset'),
				// 'id_mitra' => $this->input->post('id_mitra'),
				'tahun' => $this->input->post('tahun'),
				'bunga' => $this->input->post('bunga')
			);
			
		}

		
	}
