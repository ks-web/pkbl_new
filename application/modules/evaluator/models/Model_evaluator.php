<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	Class Model_evaluator extends MY_Model {
		function __construct() {
			parent::__construct();
			$this->_table = "tpenilaian_survey_pengusaha_evaluator"; // for insert, update, delete
			$this->_view = "evaluator2_vd"; // for call view
			$this->_order = 'desc';
			$this->_sort = 'id_mitra';
			$this->_page = 1;
			$this->_rows = 10;

			$this->_create = true;
			$this->_update = true;

			if ($this->uri->segment(4)) {
				$this->_filter = array('id_mitra' => $this->uri->segment(4));
			}

			//parameter from post/get - search function
			if ($this->input->post('q')) {
				$this->_like = array(
					'id_mitra' => $this->input->post('q'),
					'nama' => $this->input->post('q'),
					'nama_perusahaan' => $this->input->post('q')
				);
			}

			$this->_param = array('id_mitra' => $this->input->post('id_mitra'));

			//data array for input to database
			$this->_data = array(
				
				'tanggal_create' => $this->input->post('tanggal_create'),
				'creator' => $this->input->post('creator')
				
			);
			
		}

		public function getDataMitra($filter)
		{
			$this->db->select('A.*');
			$this->db->from('tmitra A');
			$this->db->where($filter);

			$query = $this->db->get();

			return $query->result_array();
		}
		public function get_dataevaluator($filter)
		{
			$this->db->select('A.*');
			$this->db->from('laporan_hasil_evaluator_vd A');
			$this->db->where($filter);

			$query = $this->db->get();

			return $query->result_array();
		}

		public function getPropinsi($filter)
		{
			$this->db->select('A.*');
			$this->db->from('provinces A');
			$this->db->where($filter);

			$query = $this->db->get();

			return $query->result_array();
		}

		public function getKota($filter)
		{
			$this->db->select('A.*');
			$this->db->from('regencies A');
			$this->db->where($filter);

			$query = $this->db->get();

			return $query->result_array();
		}

		public function getKecamatan($filter)
		{
			$this->db->select('A.*');
			$this->db->from('districts A');
			$this->db->where($filter);

			$query = $this->db->get();

			return $query->result_array();
		}

		public function getKelurahan($filter)
		{
			$this->db->select('A.*');
			$this->db->from('villages A');
			$this->db->where($filter);

			$query = $this->db->get();

			return $query->result_array();
		}

		public function updateStatusMitra($filter, $data)
		{
			$this->db->where($filter);
			$this->db->update('tmitra', $data);
		}
		public function getjaminan($filter)
		{
			$this->db->select('A.*');
			$this->db->from('mas_jaminan A');
			//$this->db->where($filter);

			$query = $this->db->get();

			return $query->result_array();
		}

	}
