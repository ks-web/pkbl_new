<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	Class Model_k_ppenjualan_his extends MY_Model {
		function __construct() {
			parent::__construct();
			$this->_table = "tk_ppenjualan"; // for insert, update, delete
			$this->_view = "tk_ppenjualan_vd"; // for call view
			$this->_order = 'asc';
			$this->_sort = 'id_ppenjualan';
			$this->_page = 1;
			$this->_rows = 10;

			$this->_create = true;
			$this->_update = true;

			if ($this->uri->segment(4)) {
				$this->_filter = array('id_mitra' => $this->uri->segment(4));
			}

			//parameter from post/get - search function
			if ($this->input->post('q')) {
				$this->_like = array(
					'id_ppenjualan' => $this->input->post('q'),
					'id_mitra' => $this->input->post('q'),
					'uraian' => $this->input->post('q')
				);
			}

			$this->_param = array('id_ppenjualan' => $this->input->post('id_ppenjualan'));

			//data array for input to database
			$this->_data = array(
				// 'id_ppenjualan' => $this->input->post('id_ppenjualan'),
				// 'id_mitra' => $this->input->post('id_mitra'),
				'jenis_produksi' => $this->input->post('jenis_produksi'),
				'satuan' => $this->input->post('satuan'),
				'harga_satuan' => $this->input->post('harga_satuan'),
				'produksi_akt' => $this->input->post('produksi_akt'),
				'produksi_proy' => $this->input->post('produksi_proy'),
				'penjualan_akt' => $this->input->post('penjualan_akt'),
				'penjualan_proy' => $this->input->post('penjualan_proy')
			);
			
		}


	}
