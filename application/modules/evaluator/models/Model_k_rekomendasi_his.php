<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	Class Model_k_rekomendasi_his extends MY_Model {
		function __construct() {
			parent::__construct();
			$this->_table = "tk_rekomendasi"; // for insert, update, delete
			$this->_view = "tk_rekomendasi_vd"; // for call view
			$this->_order = 'asc';
			$this->_sort = 'id_rekomendasi';
			$this->_page = 1;
			$this->_rows = 10;

			$this->_create = true;
			$this->_update = true;

			if ($this->uri->segment(4)) {
				$this->_filter = array('id_mitra' => $this->uri->segment(4));
			}

			//parameter from post/get - search function
			if ($this->input->post('q')) {
				$this->_like = array(
					'id_rekomendasi' => $this->input->post('q'),
					'id_mitra' => $this->input->post('q'),
					'uraian' => $this->input->post('q')
				);
			}

			$this->_param = array('id_rekomendasi' => $this->input->post('id_rekomendasi'));

			//data array for input to database
			$this->_data = array(
				
				// 'id_rekomendasi' => $this->input->post('id_rekomendasi'),
				// 'id_mitra' => $this->input->post('id_mitra'),
				'pengelolaan' => $this->input->post('pengelolaan'),
				'teknis' => $this->input->post('teknis'),
				'pemasaran' => $this->input->post('pemasaran'),
				'keuangan' => $this->input->post('keuangan'),
				'kekuatan_mitra' => $this->input->post('kekuatan_mitra'),
				'kelemahan_mitra' => $this->input->post('kelemahan_mitra'),
				'rekomendasi' => $this->input->post('rekomendasi')
			);
			
		}

		// public function getDataMitra($filter)
		// {
		// 	$this->db->select('A.*');
		// 	$this->db->from('tmitra A');
		// 	$this->db->where($filter);

		// 	$query = $this->db->get();

		// 	return $query->result_array();
		// }

		// public function getPropinsi($filter)
		// {
		// 	$this->db->select('A.*');
		// 	$this->db->from('provinces A');
		// 	$this->db->where($filter);

		// 	$query = $this->db->get();

		// 	return $query->result_array();
		// }

		// public function getKota($filter)
		// {
		// 	$this->db->select('A.*');
		// 	$this->db->from('regencies A');
		// 	$this->db->where($filter);

		// 	$query = $this->db->get();

		// 	return $query->result_array();
		// }

		// public function getKecamatan($filter)
		// {
		// 	$this->db->select('A.*');
		// 	$this->db->from('districts A');
		// 	$this->db->where($filter);

		// 	$query = $this->db->get();

		// 	return $query->result_array();
		// }

		// public function getKelurahan($filter)
		// {
		// 	$this->db->select('A.*');
		// 	$this->db->from('villages A');
		// 	$this->db->where($filter);

		// 	$query = $this->db->get();

		// 	return $query->result_array();
		// }

	}
