<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	Class Model_k_asset_his extends MY_Model {
		function __construct() {
			parent::__construct();
			$this->_table = "tk_asset"; // for insert, update, delete
			$this->_view = "tk_asset_vd"; // for call view
			$this->_order = 'asc';
			$this->_sort = 'id_asset';
			$this->_page = 1;
			$this->_rows = 10;

			$this->_create = true;
			$this->_update = true;

			if ($this->uri->segment(4)) {
				$this->_filter = array('id_mitra' => $this->uri->segment(4));
			}

			//parameter from post/get - search function
			if ($this->input->post('q')) {
				$this->_like = array(
					'id_asset' => $this->input->post('q'),
					'id_mitra' => $this->input->post('q'),
					'uraian' => $this->input->post('q')
				);
			}

			$this->_param = array('id_asset' => $this->input->post('id_asset'));

			//data array for input to database
			$this->_data = array(
				// 'id_asset' => $this->input->post('id_asset'),
				// 'id_mitra' => $this->input->post('id_mitra'),
				'uraian' => $this->input->post('uraian'),
				'unit' => $this->input->post('unit'),
				'kepemilikan' => $this->input->post('kepemilikan'),
				'nilai_asset' => $this->input->post('nilai_asset'),
				'biaya_penyusutan' => $this->input->post('biaya_penyusutan'),
				'biaya_pemeliharaan' => $this->input->post('biaya_pemeliharaan')
			);
			
		}

		
	}
