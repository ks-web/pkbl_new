<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	Class Model_k_blain_his extends MY_Model {
		function __construct() {
			parent::__construct();
			$this->_table = "tk_blain"; // for insert, update, delete
			$this->_view = "tk_blain_vd"; // for call view
			$this->_order = 'asc';
			$this->_sort = 'id_blain';
			$this->_page = 1;
			$this->_rows = 10;

			$this->_create = true;
			$this->_update = true;

			if ($this->uri->segment(4)) {
				$this->_filter = array('id_mitra' => $this->uri->segment(4));
			}

			//parameter from post/get - search function
			if ($this->input->post('q')) {
				$this->_like = array(
					'id_blain' => $this->input->post('q'),
					'id_mitra' => $this->input->post('q'),
					'uraian' => $this->input->post('q')
				);
			}

			$this->_param = array('id_blain' => $this->input->post('id_blain'));

			//data array for input to database
			$this->_data = array(
				// 'id_blain' => $this->input->post('id_blain'),
				// 'id_mitra' => $this->input->post('id_mitra'),
				'unsur_biaya'=> $this->input->post('unsur_biaya'),
				'satuan'=> $this->input->post('satuan'),
				'harga_satuan'=> $this->input->post('harga_satuan'),
				'junit_akt'=> $this->input->post('junit_akt'),
				'junit_proy'=> $this->input->post('junit_proy'),
				'biaya_akt'=> $this->input->post('biaya_akt'),
				'biaya_proy'=> $this->input->post('biaya_proy')
			);
			
		}

		// public function getDataMitra($filter)
		// {
		// 	$this->db->select('A.*');
		// 	$this->db->from('tmitra A');
		// 	$this->db->where($filter);

		// 	$query = $this->db->get();

		// 	return $query->result_array();
		// }

		// public function getPropinsi($filter)
		// {
		// 	$this->db->select('A.*');
		// 	$this->db->from('provinces A');
		// 	$this->db->where($filter);

		// 	$query = $this->db->get();

		// 	return $query->result_array();
		// }

		// public function getKota($filter)
		// {
		// 	$this->db->select('A.*');
		// 	$this->db->from('regencies A');
		// 	$this->db->where($filter);

		// 	$query = $this->db->get();

		// 	return $query->result_array();
		// }

		// public function getKecamatan($filter)
		// {
		// 	$this->db->select('A.*');
		// 	$this->db->from('districts A');
		// 	$this->db->where($filter);

		// 	$query = $this->db->get();

		// 	return $query->result_array();
		// }

		// public function getKelurahan($filter)
		// {
		// 	$this->db->select('A.*');
		// 	$this->db->from('villages A');
		// 	$this->db->where($filter);

		// 	$query = $this->db->get();

		// 	return $query->result_array();
		// }

	}
