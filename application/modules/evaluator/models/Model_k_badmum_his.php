<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	Class Model_k_badmum_his extends MY_Model {
		function __construct() {
			parent::__construct();
			$this->_table = "tk_badmum"; // for insert, update, delete
			$this->_view = "tk_badmum_vd"; // for call view
			$this->_order = 'asc';
			$this->_sort = 'id_admum';
			$this->_page = 1;
			$this->_rows = 10;

			$this->_create = true;
			$this->_update = true;

			if ($this->uri->segment(4)) {
				$this->_filter = array('id_mitra' => $this->uri->segment(4));
			}

			//parameter from post/get - search function
			if ($this->input->post('q')) {
				$this->_like = array(
					'id_admum' => $this->input->post('q'),
					'id_mitra' => $this->input->post('q'),
					'jenis_biaya' => $this->input->post('q')
				);
			}

			$this->_param = array('id_admum' => $this->input->post('id_admum'));

			//data array for input to database
			$this->_data = array(
				// 'id_mitra'=> $this->input->post('id_mitra'),
				'jenis_biaya'=> $this->input->post('jenis_biaya'),
				// 'biaya_perbulan'=> $this->input->post('biaya_perbulan'),
				'biaya_periode'=> $this->input->post('biaya_periode')
			);
			
		}


	}
