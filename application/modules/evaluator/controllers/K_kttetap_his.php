<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');
	class K_kttetap_his extends MY_Controller {
		public $models = array('k_kttetap_his');
		
		public function __construct() {
			parent::__construct();
		}

		public function index() {
			$data = array();
			$data['menu'] = $this->model_menu->getAllMenu();

			$this->template->load('template', 'view_k_kttetap_his', $data);
		}

		public function read() {
			$this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read());
		}

		public function read_all() {
			$this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read_all());
		}

		public function create() {
			// additional block
			$this->data_add['id_mitra']= $this->uri->segment(4);
			// addtional get
			$result = $this->{$this->models[0]}->insert($this->data_add);
			if ($result == 1) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => $this->db->_error_message())));
			}
		}

		public function update() {
			// additional block
			$id_mitra=$this->uri->segment(5);
			$where_item = array('id_mitra' => $id_mitra);
			$res_item = json_decode($this->{$this->models[0]}->getOtherDataFecth('tpenilaian_survey_pengusaha', $where_item), TRUE);

			foreach ($res_item as $items) {
					$data_penilaian[] = array(
						'id_pengusaha'=>$items['id_pengusaha'],
						'id_mitra'=>$id_mitra,
						'pekerjaan'=>$items['pekerjaan'],
						'tanggungan'=>$items['tanggungan'],
						'biaya_hidup'=>$items['biaya_hidup'],
						'pendapatan_luar'=>$items['pendapatan_luar'],
						'jaminan'=>$items['jaminan'],
						'status_jaminan'=>$items['status_jaminan'],
						'detail_usaha'=>$items['detail_usaha'],
						'alamat'=>$items['alamat'],
						'kelurahan'=>$items['kelurahan'],
						'kecamatan'=>$items['kecamatan'],
						'kota'=>$items['kota'],
						'propinsi'=>$items['propinsi'],
						'status_tempat'=>$items['status_tempat'],
						'mulai_usaha'=>$items['mulai_usaha'],
						'mampu_bayar'=>$items['mampu_bayar'],
						'tenagakerja'=>$items['tenagakerja'],
						'tanggal_create'=>$items['tanggal_create'],
						'creator'=>$items['creator'],
						'tanggal_modified'=>$items['tanggal_modified'],
						'modifier'=>$items['modifier']

					);
					
				}
				$where_idmitra = array('id_mitra' => $id_mitra);
					$result_asset= json_decode($this->{$this->models[0]}->getOtherDataFecth('k_kttetap', $where_idmitra), TRUE);
					foreach ($result_asset as $row) {
							$data_quot[] = array(
								'id_kttetap'=>$row['id_kttetap'],
								'id_mitra'=>$id_mitra,
								'uraian'=>$row['uraian'],
								'satuan'=>$row['satuan'],
								'harga_satuan'=>$row['harga_satuan'],
								'produksi_akt'=>$row['produksi_akt'],
								'produksi_proy'=>$row['produksi_proy'],
								'penjualan_akt'=>$row['penjualan_akt'],
								'penjualan_proy'=>$row['penjualan_proy']
								);
								}

			// getting item berdasarkan dpt
			$this->db->select('count(id_mitra) as jumlah_mitra');
			$this->db->where('id_mitra',$id_mitra);
			$get_penilaian=$this->db->get('tpenilaian_survey_pengusaha_evaluator')->result_array();
			$a=$get_penilaian[0]['jumlah_mitra'];
			if(!$a){
				$res_penilaian = $this->db->insert_batch('tpenilaian_survey_pengusaha_evaluator', $data_penilaian);
			}
echo var_dump($res_item);
die();			
			// $id_asset=$this->uri->segment(4);
			$this->db->select('count(id_mitra) as jumlah_mitra');
			$this->db->where('id_mitra',$id_mitra);
			// $this->db->where('id_asset',$id_asset);
			$get_asset=$this->db->get('tk_kttetap')->result_array();
			$b=$get_asset[0]['jumlah_mitra'];
			if(!$b){
				$res_aset = $this->db->insert_batch('tk_kttetap', $data_quot);
			}
			// addtional data

			// additional where
			$this->where_add['id_kttetap'] = $this->uri->segment(4);

			$result = $this->{$this->models[0]}->update($this->where_add, $this->data_add);
			if ($result == 1) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'Data Gagal Di Update !')));
			}
		}

		public function delete() {
			// additional block

			// additional where
			$this->where_add['id_kttetap']= $this->uri->segment(4);

			$result = $this->{$this->models[0]}->delete($this->where_add);
			
			if ($result == 1) {
				
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'Data Gagal Di Hapus !')));
			}
		}

	}
