<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');
	class Evaluator extends MY_Controller {
		public $models = array('evaluator');

		public function __construct() {
			parent::__construct();
		}

		public function index() {
			$data = array();
			$data['menu'] = $this->model_menu->getAllMenu();

			$this->template->load('template', 'view_evaluator', $data);
		}

		public function read() {
			$this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read());
		}

		public function read_all() {
			$this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read_all());
		}

		public function create() {
			//$param = $this->input->post('param'); // parametter key

			// additional block

			// addtional get
			$result = $this->{$this->models[0]}->insert($this->data_add);
			if ($result == 1) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
				//echo $this->db->last_query();
				$filter = array('id_mitra' => $this->input->post('id_mitra'));
				$data = array('status' => '3');
				$this->{$this->models[0]}->updateStatusMitra($filter, $data);
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => $this->db->_error_message())));
			}
		}

		public function update() {
			$param = $this->uri->segment(4);
			// parameter key

			// additional block

			// addtional data

			// additional where
			$this->where_add['id_mitra'] = $param;

			$result = $this->{$this->models[0]}->update($this->where_add, $this->data_add);
			if ($result == 1) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'Data Gagal Di Update !')));
			}
		}

		public function delete() {
			$param = $this->uri->segment(4);
			// parameter key

			// additional block

			// additional where
			$this->where_add['id_mitra'] = $param;

			$result = $this->{$this->models[0]}->delete($this->where_add);
			if ($result == 1) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'Data Gagal Di Hapus !')));
			}
		}

		public function getDataMitra() {
			$filter = array('A.status' => '2');
			$this->output->set_content_type('application/json')->set_output(json_encode($this->{$this->models[0]}->getDataMitra($filter)));
		}
		public function get_dataevaluator($id_mitra) {
			$filter = array('A.id_mitra' => $id_mitra);
			$this->output->set_content_type('application/json')->set_output(json_encode($this->{$this->models[0]}->get_dataevaluator($filter)));
		}
		

		public function getPropinsi() {
			$filter = array('A.id !=' => 0);
			$this->output->set_content_type('application/json')->set_output(json_encode($this->{$this->models[0]}->getPropinsi($filter)));
		}

		public function getKota($id) {
			$filter = array('A.province_id' => $id);
			$this->output->set_content_type('application/json')->set_output(json_encode($this->{$this->models[0]}->getKota($filter)));
		}

		public function getKecamatan($id) {
			$filter = array('A.regency_id' => $id);
			$this->output->set_content_type('application/json')->set_output(json_encode($this->{$this->models[0]}->getKecamatan($filter)));
		}

		public function getKelurahan($id) {
			$filter = array('A.district_id' => $id);
			$this->output->set_content_type('application/json')->set_output(json_encode($this->{$this->models[0]}->getKelurahan($filter)));
		}

		public function upevaluasi($id){
			
			$penjualan_akt=$this->input->post('1b_akt');
			$penjualan_proy=$this->input->post('1b_proy');
			$tot_variabel_akt=$this->input->post('total_3a_akt');
			$tot_variabel_proy=$this->input->post('total_3a_proy');
			$tot_tetap_akt=$this->input->post('total_3b_akt');
			$tot_tetap_proy=$this->input->post('total_3b_proy');
			$tot_produksi_akt=$this->input->post('totbiayaprod_act');
			$tot_produksi_proy=$this->input->post('totbiayaprod_proy');
			$keuntungan_akt=$this->input->post('keuntungan_akt');
			$keuntungan_proy=$this->input->post('keuntungan_proy');
			$nilai_rekomendasi=$this->input->post('tot_4a');
			$nilai_usulan=$this->input->post('_4b_total_act');
			$angsuran_pokok=$this->input->post('_4b_total_proy');
			$angsuran_bunga=$this->input->post('jasaadmin');
			$total_pinjaman_bunga=$this->input->post('totangsurandanadmin');
			$angsuran_bulanan=$this->input->post('angsuranperbulan');
			$angsuranpokok_perbln=$this->input->post('angsuranpokok_perbln');
			$angsuranbunga_perbln=$this->input->post('angsuranbunga_perbln');
		 	 	
			$data=array(
						'penjualan_akt'=>$penjualan_akt,
						'penjualan_proy'=>$penjualan_proy,
						'tot_variabel_akt'=>$tot_variabel_akt,
						'tot_variabel_proy'=>$tot_variabel_proy,
						'tot_tetap_akt'=>$tot_tetap_akt,
						'tot_tetap_proy'=>$tot_tetap_proy,
						'tot_produksi_akt'=>$tot_produksi_akt,
						'tot_produksi_proy'=>$tot_produksi_proy,
						'keuntungan_akt'=>$keuntungan_akt,
						'keuntungan_proy'=>$keuntungan_proy,
						'nilai_usulan'=>$nilai_usulan,
						'nilai_rekomendasi'=>$nilai_rekomendasi,
						'angsuran_pokok'=>$angsuran_pokok,
						'angsuran_bunga'=>$angsuran_bunga,
						'total_pinjaman_bunga'=>$total_pinjaman_bunga,
						'angsuran_bulanan'=>$angsuran_bulanan,
						'angsuranbunga_perbln'=>$angsuranbunga_perbln,
						'angsuranpokok_perbln'=>$angsuranpokok_perbln
						);

			$this->db->where('id_mitra',$id);
			
			$result = $this->db->update('tpenilaian_survey_pengusaha_evaluator',$data);
			if ($result == 1) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'Data Gagal  !')));
			}



		}

		public function laporan($id = null) {
			$this->db->where('id_mitra', $id);
			$check = $this->db->get('laporan_hasil_evaluator_vd');
			if ($check->num_rows() > 0) {

				$data['pengusaha'] = $check->row_array();

				// kota tanggal
				$data['kota'] = 'Cilegon';
				$this->load->helper('tanggal_indo');
				$data['tanggal'] = tanggal_display($data['pengusaha']['tanggal_create']);

				$html = $this->load->view('rep_hasil_evaluasi', $data, true);

				$this->load->library('dompdf_gen');
				// Load library
				$this->dompdf->set_paper('A4', 'potrait');
				// Setting Paper
				// // Convert to PDF
				$this->dompdf->load_html($html);
				$this->dompdf->render();
				$this->dompdf->stream("Laporan Usulan Pinjaman-" . $id . ".pdf");
			} else {
				show_404();
			}
		}

		public function laporan_lampiran($id = null) {
			$this->db->where('id_mitra', $id);
			$check = $this->db->get('laporan_hasil_survey_vd');
			if ($check->num_rows() > 0) {

				$data['pengusaha'] = $check->row_array();
				$id_mitra = $data['pengusaha']['id_mitra'];
				
				// asset usaha
				$this->db->where('id_mitra', $id_mitra);
				$query = $this->db->get('k_asset');
				$data['asset_usaha'] = null;
				if ($query->num_rows() > 0) {
					$data['asset_usaha'] = $query->result_array();
				}

				// periode usaha
				$this->db->where('id_mitra', $id_mitra);
				$query = $this->db->get('k_pperputaran');
				$data['periode_usaha'] = null;
				if ($query->num_rows() > 0) {
					$data['periode_usaha'] = $query->result_array();
				}

				// produksi dan penjualan
				$this->db->where('id_mitra', $id_mitra);
				$query = $this->db->get('k_ppenjualan');
				$data['produksi_penjualan'] = null;
				if ($query->num_rows() > 0) {
					$data['produksi_penjualan'] = $query->result_array();
				}

				// biaya bahan baku
				$this->db->where('id_mitra', $id_mitra);
				$query = $this->db->get('k_bbbaku');
				$data['bahan_baku'] = null;
				if ($query->num_rows() > 0) {
					$data['bahan_baku'] = $query->result_array();
				}

				// biaya bahan bantu
				$this->db->where('id_mitra', $id_mitra);
				$query = $this->db->get('k_bbbantu');
				$data['bahan_bantu'] = null;
				if ($query->num_rows() > 0) {
					$data['bahan_bantu'] = $query->result_array();
				}

				// tenaga kerja tetap
				$this->db->where('id_mitra', $id_mitra);
				$query = $this->db->get('k_ttkerja');
				$data['tenaga_kerja_tetap'] = null;
				if ($query->num_rows() > 0) {
					$data['tenaga_kerja_tetap'] = $query->result_array();
				}

				// tenaga kerja tidak tetap
				$this->db->where('id_mitra', $id_mitra);
				$query = $this->db->get('k_kttetap');
				$data['tenaga_kerja_tidak_tetap'] = null;
				if ($query->num_rows() > 0) {
					$data['tenaga_kerja_tidak_tetap'] = $query->result_array();
				}

				// biaya transport
				$this->db->where('id_mitra', $id_mitra);
				$query = $this->db->get('k_btransport');
				$data['biaya_transport'] = null;
				if ($query->num_rows() > 0) {
					$data['biaya_transport'] = $query->result_array();
				}
				
				// biaya adm dan umum
				$this->db->where('id_mitra', $id_mitra);
				$query = $this->db->get('k_badmum');
				$data['biaya_adm_umum'] = null;
				if ($query->num_rows() > 0) {
					$data['biaya_adm_umum'] = $query->result_array();
				}
				
				// biaya lain lain
				$this->db->where('id_mitra', $id_mitra);
				$query = $this->db->get('k_blain');
				$data['biaya_lain2'] = null;
				if ($query->num_rows() > 0) {
					$data['biaya_lain2'] = $query->result_array();
				}
				
				// pasiva
				$this->db->where('id_mitra', $id_mitra);
				$query = $this->db->get('k_ppasiva');
				$data['pasiva'] = null;
				if ($query->num_rows() > 0) {
					$data['pasiva'] = $query->result_array();
				}

				// kota tanggal
				$data['kota'] = 'Cilegon';
				$this->load->helper('tanggal_indo');
				$data['tanggal'] = tanggal_display();

				$html = $this->load->view('rep_hasil_survey_lampiran', $data, true);

				$this->load->library('dompdf_gen'); // Load library
				$this->dompdf->set_paper('A4', 'potrait'); // Setting Paper
				// // Convert to PDF
				$this->dompdf->load_html($html);
				$this->dompdf->render();
				$this->dompdf->stream("LaporanHasilSurvey-".$id_mitra.".pdf");
			} else {
				show_404();
			}
		}
		public function getjaminan()
		{
			$filter = array();
			$this->output->set_content_type('application/json')->set_output(json_encode($this->{$this->models[0]}->getjaminan($filter)));
		}
		 function validasi(){
		 	$id_mitra=$this->uri->segment(4);
		  	$creator = $this->session->userdata('id');
		  	$aproval='1';
		  	// $angsuran_pokok=$this->input->post('_4b_total_proy');
		 	$tanggal_create=date("Y-m-d H:i:s");
		 	$this->data_add['tanggal_create']=$tanggal_create;
		 	$this->data_add['creator']=$creator;
		 	
		 	$this->data_add['aproval']=$aproval;


		 	$this->where_add['id_mitra'] = $id_mitra;
			$rest = $this->{$this->models[0]}->update($this->where_add, $this->data_add);

			$id_mitra=$this->uri->segment(4);
		 	$where_idmitra = array('id_mitra' => $id_mitra);
			$data_mitra = array('status' => '5');
			$result = $this->db->update('tmitra', $data_mitra,$where_idmitra );
			$this->db->last_query();
			#die();
			if ($result == 1) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => $this->db->_error_message())));
			}
		 }

	}
