<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');
	class K_rekomendasi_his extends MY_Controller {
		public $models = array('k_rekomendasi_his');
		
		public function __construct() {
			parent::__construct();
		}

		public function index() {
			$data = array();
			$data['menu'] = $this->model_menu->getAllMenu();

			$this->template->load('template', 'view_k_rekomendasi_his', $data);
		}

		public function read() {
			$this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read());
		}

		public function read_all() {
			$this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read_all());
		}

		public function create() {
			// additional block
			$this->data_add['id_mitra']= $this->uri->segment(4); 
			// addtional get
			$result = $this->{$this->models[0]}->insert($this->data_add);
			if ($result == 1) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => $this->db->_error_message())));
			}
		}

		public function update() {
			// additional block

			// addtional data

			// additional where
			$this->where_add['id_rekomendasi'] = $this->uri->segment(4);

			$result = $this->{$this->models[0]}->update($this->where_add, $this->data_add);
			if ($result == 1) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'Data Gagal Di Update !')));
			}
		}

		public function delete() {
			// additional block

			// additional where
			$this->where_add['id_rekomendasi'] = $this->uri->segment(4);

			$result = $this->{$this->models[0]}->delete($this->where_add);
			
			if ($result == 1) {
				
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'Data Gagal Di Hapus !')));
			}
		}

	}
