<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	Class Model_bidangbantuanbl extends MY_Model {
		function __construct() {
			parent::__construct();
			$this->_table = "mas_bidangbantuanbl";
			$this->_view = "mas_bidangbantuanbl_vd";
			$this->_order = 'asc';
			$this->_sort = 'Kode_group';
			$this->_page = 1;
			$this->_rows = 10;

			$this->_update = true;
			$this->_create = true;

			if ($this->uri->segment(4)) {
				$this->_filter = array('tahun' => $this->uri->segment(4));
			}
			if ($this->input->post('q')) {
				$this->_like = array(	
					'Kode_group' 	=> $this->input->post('q'),
					'tahun' => $this->input->post('q'),
					'nama_bidangbantuan' => $this->input->post('q')
				);
			}

			$this->_param = array('Kode_group' => $this->input->post('Kode_group'));

			$this->_data = array(
			'Kode_group'=> $this->input->post('Kode_group'),
			'nama_bidangbantuan'=> $this->input->post('nama_bidangbantuan'),
			'tahun'	=> $this->input->post('tahun'),
			'unit' => $this->input->post('unit'),
			'anggaran' => $this->input->post('anggaran')
			);
		}
		public function gettahun($filter = null)
        {
            $this->db->like('CONCAT( (tahun))', $filter );
            $this->db->group_by('tahun');

            $query = $this->db->get('mas_bidangbantuanbl')->result_array();

            return $query;
        }
         function getbidang($id=null)
         {
            $this->db->like('CONCAT( (id_sektor_bl),(nama_bl))', $id );
         	// $this->db->where('tahun', $thn );
            // $this->db->group_by('tahun');

            $query = $this->db->get('sektor_bl')->result_array();

            return $query;
         }
         
	}
