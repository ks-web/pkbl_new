<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	Class Model_unit_kerja extends MY_Model {
		function __construct() {
			parent::__construct();
			$this->_table = "mas_divisi";
			// $this->_view = "mas_material_vd";
			$this->_order = 'asc';
			$this->_sort = 'kode_divisi';
			$this->_page = 1;
			$this->_rows = 10;

			$this->_create = true;
			$this->_update = true;

			if ($this->uri->segment(4)) {
				$this->_filter = array('kode_divisi' => $this->uri->segment(4));
			}
			if ($this->input->post('q')) {
				$this->_like = array(
					'kode_divisi' => $this->input->post('q'),
					'nama_divisi' => $this->input->post('q')
				);
			}

			$this->_param = array('kode_divisi' => $this->input->post('kode_divisi'));

			$this->_data = array(
				
				'kode_divisi' => $this->input->post('kode_divisi'),
				'nama_divisi' => $this->input->post('nama_divisi'),
				'cost_centre' => $this->input->post('cost_centre')
			);
		}
		function getCboParent()
    {
        $q = $this->db->get_where('mas_divisi')->result_array();
        $this->output->set_output(json_encode($q));
    }
	}
