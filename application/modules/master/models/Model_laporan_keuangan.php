<?php
    if (!defined('BASEPATH'))
        exit('No direct script access allowed');

    Class Model_laporan_keuangan extends MY_Model {
        function __construct() {
            parent::__construct();
            $this->_table = "sys_group_link_laporan"; // for insert, update, delete
            $this->_view = "laporan_keuangan_vd"; // for call view laporan_keuangan_vd
            $this->_order = 'ASC';
            $this->_sort = 'kode_account';
            $this->_page = 1;
            $this->_rows = 10;

            $this->_create = true;
            $this->_update = true;

            if ($this->uri->segment(4)) {
                $this->_filter = array('Node_group' => $this->uri->segment(4));
            }

            //parameter from post/get - search function
            if ($this->input->post('q')) {
                $this->_like = array(
                    'Node_group' => $this->input->post('q')
                );
            }

            // $this->_param = array('Node_group' => $this->input->post('Node_group'));

            //data array for input to database
            $qty = $this->getCount() ? $this->getCount() : 0;
            $this->_data = array(
                'id_laporan'            => $this->input->post('laporan'),
                'node_laporan'          => $qty[0]['qty']+1,
                'keterangan'            => $this->input->post('group_keterangan')
            );
            
        }
       function getGroupLaporan($q=''){
            $this->db->like("group_laporan", $q, 'both');
           $data = $this->db->get('laporan_keuangan_group_vd')->result_array();
           return $data;
       }
       function getNode($where,$q){
            $this->db->like("uraian", $q, 'both');
            $this->db->where('id_laporan',$where);
           $data = $this->db->get('laporan_keuangan_getnode_vd')->result_array();
           return $data;
       }
       function getParent($q){
            $this->db->like("CONCAT(kode_account, uraian)", $q, 'both');
            $this->db->where('parent_group <>',1);
            $this->db->where('parent_group <>',0);
            $this->db->order_by('kode_account','ASC');
           $data = $this->db->get('mas_account')->result_array();
           return $data;
       }
       function getCount(){
           $data = $this->db->query('select node_laporan as qty from sys_group_link_laporan ORDER BY node_laporan DESC LIMIT 1')->result_array();
           return $data;
       }

       function getHeader($q,$where){
            $this->db->like("keterangan", $q, 'both');
            $this->db->where('kode_account is null');
            $this->db->where('parent_laporan >', 0);
            $this->db->where('parent_laporan =', $where);
            $this->db->order_by('node_laporan', 'ASC');
            $data = $this->db->get('laporan_keuangan_vd')->result_array();
            return $data;
       }
       
       function getTree($parent_group=0)
        {
            $res = array();
            $where_add['parent_laporan'] = $parent_group;
            $read = $this->read($where_add);
            $tmp = json_decode($read);

            foreach ($tmp->rows as $row) {
                $row->id = $row->node_laporan;
                $row->text = $row->keterangan;

                $children = $this->getTree($row->node_laporan);
                if (sizeof($children) > 0) {
                    $row->children = $children;
                    $row->state = 'closed';
                }
                array_push($res, $row);
            }

            return $res;
        }

        function get_data($q){
            $this->db->like("CONCAT(kode_account, uraian)", $q, 'both');
            $this->db->where('kode_account is not null');
           $data = $this->db->get('mas_account')->result_array();
           return $data;
       }

    }
