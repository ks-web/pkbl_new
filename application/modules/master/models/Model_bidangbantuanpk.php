<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	Class Model_bidangbantuanpk extends MY_Model {
		function __construct() {
			parent::__construct();
			$this->_table = "mas_bidangbantuanpk"; // for insert, update, delete
			$this->_view = ""; // for call view
			$this->_order = 'asc';
			$this->_sort = 'Node_group';
			$this->_page = 1;
			$this->_rows = 10;

			$this->_create = true;
			$this->_update = true;

			if ($this->uri->segment(4)) {
				$this->_filter = array('Node_group' => $this->uri->segment(4));
			}

			//parameter from post/get - search function
			if ($this->input->post('q')) {
				$this->_like = array(
					'Node_group' => $this->input->post('q')
				);
			}

			$this->_param = array('Node_group' => $this->input->post('Node_group'));

			//data array for input to database
			$this->_data = array(
				'Parent_group' => ($this->input->post('newparent'))? 0 : $this->input->post('Parent_group'),
				'Kode_group' => $this->input->post('Kode_group'),
				'nama_sektor' => $this->input->post('nama_sektor'),
				'unit' => $this->input->post('unit'),
				'anggaran' => $this->input->post('anggaran')
			);
			
		}

		function getTree($Parent_group=0)
		{
			$res = array();

			$where_add['Parent_group'] = $Parent_group;
			$read = $this->read($where_add);
			$tmp = json_decode($read);
			foreach ($tmp->rows as $row) {
				$row->id = $row->Node_group;
				$row->text = $row->nama_sektor;
				$children = $this->getTree($row->Node_group);
				if (sizeof($children) > 0) {
					$row->children = $children;
				}
				array_push($res, $row);
			}

			return $res;
		}

	}
