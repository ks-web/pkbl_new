<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	Class Model_mas_account extends MY_Model {
		function __construct() {
			parent::__construct();
			$this->_table = "mas_account"; // for insert, update, delete
			$this->_view = ""; // for call view
			$this->_order = 'asc';
			$this->_sort = 'node_group';
			$this->_page = 1;
			$this->_rows = 10;

			$this->_create = true;
			$this->_update = true;

			if ($this->uri->segment(4)) {
				$this->_filter = array('node_group' => $this->uri->segment(4));
			}

			//parameter from post/get - search function
			if ($this->input->post('q')) {
				$this->_like = array(
					'node_group' => $this->input->post('q')
				);
			}

			$this->_param = array('node_group' => $this->input->post('node_group'));

			//data array for input to database
			$this->_data = array(
				// 'node_group'=> $this->input->post('node_group'),
				'parent_group'=> $this->input->post('parent_group'),
				'kode_account'=> $this->input->post('kode_account'),
				'uraian'=> $this->input->post('uraian'),
				'tipe'=> $this->input->post('tipe')
			);
			
		}

		function getTree($parent_group=0)
		{
			$res = array();

			$where_add['parent_group'] = $parent_group;
			$read = $this->read($where_add);
			$tmp = json_decode($read);
			foreach ($tmp->rows as $row) {
				$row->id = $row->node_group;
				$row->text = $row->uraian;
				$children = $this->getTree($row->node_group);
				if (sizeof($children) > 0) {
					$row->children = $children;
				}
				array_push($res, $row);
			}

			return $res;
		}

	}
