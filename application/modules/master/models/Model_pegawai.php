<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	Class Model_pegawai extends MY_Model {
		function __construct() {
			parent::__construct();
			$this->_table = "mas_pegawai";
			$this->_view = "mas_pegawai_vd";
			$this->_order = 'asc';
			$this->_sort = 'idPegawai';
			$this->_page = 1;
			$this->_rows = 10;

			$this->_create = true;
			$this->_update = true;

			if ($this->uri->segment(4)) {
				$this->_filter = array('idPegawai' => $this->uri->segment(4));
			}
			if ($this->input->post('q')) {
				$this->_like = array(
					'idPegawai' => $this->input->post('q'),
				);
			}

			$this->_param = array('idPegawai' => $this->input->post('idPegawai'));

			$this->_data = array(
				// 'idPegawai' 	=> $this->input->post('idPegawai'),
				'nip' 			=> $this->input->post('nip'),
				'nama_pegawai' 	=> $this->input->post('nama_pegawai'),
				'Divisi' 		=> $this->input->post('Divisi'),
				'email' 		=> $this->input->post('email'),
				'no_hp' 		=> $this->input->post('no_hp'),
				'id_jabatan' 	=> $this->input->post('id_jabatan')
			);
		}

		// additional function
		function divisi($cari) {
			$this->db->select('kode_divisi, nama_divisi, cost_centre');
			$this->db->like('CONCAT(kode_divisi,nama_divisi)', $cari);
			$data = $this->db->get('mas_divisi')->result();

			$this->output->set_output(json_encode($data));
		}
		function jabatan($cari) {
			$this->db->select('id_jabatan, jabatan');
			$this->db->like('CONCAT(id_jabatan,jabatan)', $cari);
			$data = $this->db->get('mas_jabatan')->result();
			$this->output->set_output(json_encode($data));
		}

	}
