<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	Class Model_bidangbantuanmt extends MY_Model {
		function __construct() {
			parent::__construct();
			$this->_table = "mas_bidangbantuanpk";
			$this->_view = "mas_bidangbantuanpk_vd";
			$this->_order = 'asc';
			$this->_sort = 'Kode_group';
			$this->_page = 1;
			$this->_rows = 10;

			$this->_update = true;
			$this->_create = true;

			if ($this->uri->segment(4)) {
				$this->_filter = array('tahun' => $this->uri->segment(4));
			}
			if ($this->input->post('q')) {
				$this->_like = array(	
					'Kode_group' 	=> $this->input->post('q'),
					'tahun' => $this->input->post('q'),
					'nama_sektor' => $this->input->post('q')
				);
			}

			$this->_param = array('Kode_group' => $this->input->post('Kode_group'));

			$this->_data = array(
			'Kode_group'=> $this->input->post('Kode_group'),
			'nama_sektor'=> $this->input->post('nama_sektor'),
			'tahun'	=> $this->input->post('tahun'),
			'unit' => $this->input->post('unit'),
			'anggaran' => $this->input->post('anggaran')
			);
		}
		public function gettahun($filter = null)
        {
            $this->db->like('CONCAT( (tahun))', $filter );
            $this->db->group_by('tahun');

            $query = $this->db->get('mas_bidangbantuanpk')->result_array();

            return $query;
        }
         function getbidang($id=null)
         {
            $this->db->like('CONCAT( (id_sektor),(nama_sektor))', $id );
         	// $this->db->where('tahun', $thn );
            // $this->db->group_by('tahun');

            $query = $this->db->get('sektor_kemitraan')->result_array();

            return $query;
         }
         
	}
