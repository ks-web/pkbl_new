<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	Class Model_sektor_kemitraan extends MY_Model {
		function __construct() {
			parent::__construct();
			$this->_table = "sektor_kemitraan";
			$this->_view = "sektor_kemitraan_vd";
			$this->_order = 'asc';
			$this->_sort = 'id_sektor';
			$this->_page = 1;
			$this->_rows = 10;

			$this->_update = true;
			$this->_create = true;

			if ($this->uri->segment(4)) {
				$this->_filter = array('id_sektor' => $this->uri->segment(4));
			}
			if ($this->input->post('q')) {
				$this->_like = array(	
					'id_sektor' 	=> $this->input->post('q'),
					'nama_sektor' => $this->input->post('q'),
					'keterangan' => $this->input->post('q')
				);
			}

			$this->_param = array('id_sektor' => $this->input->post('id_sektor'));

			$this->_data = array(
				'id_sektor' 	=> $this->input->post('id_sektor'),
				'nama_sektor' 	=> $this->input->post('nama_sektor'),
				'keterangan'	=> $this->input->post('keterangan')
			);
		}


	}
