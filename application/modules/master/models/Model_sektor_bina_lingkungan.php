<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	Class Model_sektor_bina_lingkungan extends MY_Model {
		function __construct() {
			parent::__construct();
			$this->_table = "sektor_bl";
			$this->_view = "sektor_bl_vd";
			$this->_order = 'asc';
			$this->_sort = 'id_sektor_bl';
			$this->_page = 1;
			$this->_rows = 10;

			$this->_update = true;
			$this->_create = true;

			if ($this->uri->segment(4)) {
				$this->_filter = array('id_sektor_bl' => $this->uri->segment(4));
			}
			if ($this->input->post('q')) {
				$this->_like = array(	
					'id_sektor_bl' 	=> $this->input->post('q'),
					'nama_bl' => $this->input->post('q'),
					'keterangan' => $this->input->post('q')
				);
			}

			$this->_param = array('id_sektor_bl' => $this->input->post('id_sektor_bl'));

			$this->_data = array(
				'id_sektor_bl' 	=> $this->input->post('id_sektor_bl'),
				'nama_bl' 	=> $this->input->post('nama_bl'),
				'keterangan'	=> $this->input->post('keterangan')
			);
		}


	}
