<?php
    if (!defined('BASEPATH'))
        exit('No direct script access allowed');
    class Laporan_keuangan extends MY_Controller {
        public $models = array('laporan_keuangan');
        
        public function __construct() {
            parent::__construct();
        }

        public function index() {
            $data = array();
            $data['menu'] = $this->model_menu->getAllMenu();

            $this->template->load('template', 'view_laporan_keuangan', $data);
        }

        public function read() {
            $this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read());
        }

        public function read_all() {
            $this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read_all());
        }
        public function create() {

            // additional block

            // addtional get
            $parent                     = $this->input->post('parent');
            $node                       = $this->input->post('node');
            $parent_laporan             = $this->input->post('parent_laporan');
            $parent_laporan_group       = $this->input->post('parent_laporan_group');
            $header                     = $this->input->post('header');
            $group_keterangan           = $this->input->post('group_keterangan');
            

            if (!$header) {
                $this->data_add['keterangan'] = $group_keterangan[0];
            }

            if($parent[0]){
                $qty = $this->getCount() ? $this->getCount() : 0;

                foreach ($parent as $key => $value) {
                    $qty = $this->getCount() ? $this->getCount() : 0;
                    $this->data_add['node_laporan'] = $qty[0]['qty']+1;
                    $this->data_add['kode_account'] = $value;
                    $this->data_add['parent_laporan'] = $header;
                    $this->data_add['keterangan'] = $group_keterangan[$key];
                    $result = $this->{$this->models[0]}->insert($this->data_add);
                }
                
            }else if($header){
                $this->data_add['kode_account'] = NULL;
                $this->data_add['keterangan'] = $header;
                $this->data_add['parent_laporan'] = $parent_laporan;
                $result = $this->{$this->models[0]}->insert($this->data_add);
            }else if($node){
                if($parent_laporan){
                    $this->data_add['parent_laporan'] = $parent_laporan;
                }else{
                    $this->data_add['parent_laporan'] = $parent_laporan_group;
                }
                
                $this->data_add['kode_account'] =  $node;

                $result = $this->{$this->models[0]}->insert($this->data_add);
            }
            
            // $result = $this->{$this->models[0]}->insert($this->data_add);
            if ($result == 1) {
                $this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
            } else {
                $this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => $this->db->_error_message())));
            }
        }
        public function update(){
            $data = array();

            if($this->input->post('laporan')){
                $data['id_laporan'] = $this->input->post('laporan');
                $data['keterangan'] = $this->input->post('keterangan');
            }else{
                $data['kode_account'] = $this->input->post('account');
                $data['keterangan'] = $this->input->post('keterangan');
            }

            $data['node_laporan'] = $this->input->post('node_laporan');
            $data['parent_laporan'] = $this->input->post('parent_laporan');

            $id_laporan = $this->input->post('id_laporan');
            $node_laporan = $this->input->post('node_laporan_val');
            $parent_laporan = $this->input->post('parent_laporan_val');
            

            $this->db->where('id_laporan',$id_laporan);
            $this->db->where('node_laporan',$node_laporan);
            $this->db->where('parent_laporan',$parent_laporan);
            $res = $this->db->update('sys_group_link_laporan',$data);

            if($res){
                $this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
            }else{
                $this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => $this->db->_error_message())));
            }

        }
        
        public function getGroupLaporan() {
            $q = $this->input->post('q') ? $this->input->post('q') : '';
            $data = $this->{$this->models[0]}->getGroupLaporan($q);
            $this->output->set_output(json_encode($data));    
        }
        public function getNode($where) {
            $q = $this->input->post('q') ? $this->input->post('q') : '';
            $data = $this->{$this->models[0]}->getNode($where,$q);
            $this->output->set_output(json_encode($data));    
        }
        public function getParent() {
            $q = $this->input->post('q') ? $this->input->post('q') : '';
            $data = $this->{$this->models[0]}->getParent($q);
            $this->output->set_output(json_encode($data));    
        }

        public function getHeader($where) {
            $q = $this->input->post('q') ? $this->input->post('q') : '';
            $data = $this->{$this->models[0]}->getHeader($q,$where);
            $this->output->set_output(json_encode($data));    
        }
        function getTree()
        {
            $data = $this->{$this->models[0]}->getTree();
            $this->output->set_output(json_encode($data));  
        }
        function getCount()
        {
            $data = $this->{$this->models[0]}->getCount();
            return $data;
        }
        public function get_data() {
            $q = $this->input->post('q') ? $this->input->post('q') : '';
            $data = $this->{$this->models[0]}->get_data($q);
            $this->output->set_output(json_encode($data));    
        }

    }
