<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

	class Pegawai extends MY_Controller {
		
		public $models = array('pegawai');

		public function __construct() {
			parent::__construct();
		}

		public function index() {
			$data = array();
			$data['menu'] = $this->model_menu->getAllMenu();

			$this->template->load('template', 'view_pegawai', $data);
		}

		public function read() {
			$this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read());
		}

		public function read_all() {
			$this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read_all());
		}

		public function create() {
			$param = $this->input->post('idPegawai');

			$check = json_decode($this->{$this->models[0]}->read_one(array('nip' => $this->input->post('nip'))));
			if (sizeof($check)>0) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'nip sudah dipakai, coba nip lain.')));
				return false;
			}
			// Upload file
			$dir_pegawai = 'assets/upload/pegawai/' .$param ;
			if (!is_dir($dir_pegawai)) {
				mkdir($dir_pegawai, folderUploadPermission, TRUE);
			}
			$dir = 'assets/upload/pegawai/' .$param ;
			if (!is_dir($dir)) {
				mkdir($dir, folderUploadPermission, TRUE);
			}

			$config['upload_path'] = $dir . '/';
			$config['allowed_types'] = implode('|', json_decode(fileUploadTipe2));
			$config['max_size'] = fileUploadTipe2;
			$config['encrypt_name'] = true;
			$this->upload->initialize($config);

			if (!$this->upload->do_upload('ttd_img')) {
				$this->data_add['ttd_img'] = '';
			} else {
				$file = $this->upload->data();
				$this->data_add['ttd_img'] = $config['upload_path'] . $file['file_name'];
			}

			// additional block
			$check = json_decode($this->{$this->models[0]}->getOtherData('mas_pegawai', array('idPegawai' => $param)));
			if (isset($check)) {
				$this->where_add['idPegawai'] = $param;

				$result = $this->{$this->models[0]}->update($this->where_add, $this->data_add);
				if ($result == 1) {
					$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
				} else {
					$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => $this->db->_error_message())));
				}

				return false;
			}

			// addtional get
			$result = $this->{$this->models[0]}->insert($this->data_add);
			if ($result == 1) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => $this->db->_error_message())));
			}
		}

		public function update() {
			$param = $this->uri->segment(4);
			// Upload file
			$dir_pegawai = 'assets/upload/pegawai/' .$param ;
			if (!is_dir($dir_pegawai)) {
				mkdir($dir_pegawai, folderUploadPermission, TRUE);
			}
			$dir = 'assets/upload/pegawai/' .$param;
			if (!is_dir($dir)) {
				mkdir($dir, folderUploadPermission, TRUE);
			}

			$config['upload_path'] = $dir . '/';
			$config['allowed_types'] = implode('|', json_decode(fileUploadTipe1));
			$config['max_size'] = fileUploadSize2;
			$config['encrypt_name'] = true;
			$this->upload->initialize($config);

			if (!$this->upload->do_upload('ttd_img')) {
				NULL;
			} else {
				$file = $this->upload->data();
				if ($current_file != '') {
					$this->load->library('common');
					$this->common->delete_file($current_file);
				}

				$this->data_add['ttd_img'] = $config['upload_path'] . $file['file_name'];
			}

			// additional block
			
			// addtional data

			// additional where
			$this->where_add['idPegawai'] = $param;

			$result = $this->{$this->models[0]}->update($this->where_add, $this->data_add);
			if ($result == 1) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'Data Gagal Di Update !')));
			}
		}

		public function delete() {
			$param = $this->uri->segment(4);

			// additional block

			// additional where
			$this->where_add['idPegawai'] = $param;

			$result = $this->{$this->models[0]}->delete($this->where_add);
			if ($result == 1) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'Data Gagal Di Hapus !')));
			}
		}

		// additional function
		function divisi() {
			header("Content-Type: application/json");
			$cari = $this->input->post('q') ? $this->input->post('q') : '';
			$this->{$this->models[0]}->divisi($cari);
		}
		function jabatan() {
			header("Content-Type: application/json");
			$cari = $this->input->post('q') ? $this->input->post('q') : '';
			$this->{$this->models[0]}->jabatan($cari);
		}

	}
