<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');
	class Bidangbantuanmt extends MY_Controller {
		public $models = array('bidangbantuanmt');
		
		public function __construct() {
			parent::__construct();
		}

		public function index() {
			$data = array();
			$data['menu'] = $this->model_menu->getAllMenu();

			$this->template->load('template', 'view_bidangbantuanmt', $data);
		}

		public function read() {

			// $thn=date("Y");
			// $this->where_add['tahun'] = $filter;
			$this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read());
		}

		public function read_all() {
			$this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read_all());
		}

		public function create() {
			
			//$this->data_add['parentnode'] = $parentnode;
			
			$result = $this->{$this->models[0]}->insert($this->data_add);
			if ($result == 1) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => $this->db->_error_message())));
			}
		}

		public function update() {
			$param = $this->uri->segment(4);

			$this->where_add['Kode_group'] = $param;
			
			$result = $this->{$this->models[0]}->update($this->where_add, $this->data_add);
			
			if ($result == 1) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'Data Gagal Di Update !')));
			}
		}

		public function delete() {
			// additional where
			$this->where_add['Kode_group'] = $this->uri->segment(4);
			
			$result = $this->{$this->models[0]}->delete($this->where_add);
			if ($result == 1) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'Data Gagal Di Hapus !')));
			}
		}
		public function gettahun($filter = null)
        {
            $this->output->set_content_type('application/json')->set_output(json_encode($this->{$this->models[0]}->gettahun($filter)));
        }
        function getbidang($id=null)
        {
        $this->output->set_content_type('application/json')->set_output(json_encode($this->{$this->models[0]}->getbidang($id)));
    		}

	}
