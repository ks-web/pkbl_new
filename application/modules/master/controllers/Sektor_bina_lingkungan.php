<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');
	class Sektor_bina_lingkungan extends MY_Controller {
		public $models = array('sektor_bina_lingkungan');
		
		public function __construct() {
			parent::__construct();
		}

		public function index() {
			$data = array();
			$data['menu'] = $this->model_menu->getAllMenu();

			$this->template->load('template', 'view_sektor_bina_lingkungan', $data);
		}

		public function read() {
			$this->output->set_content_type('application/json')->set_output($this->sektor_bina_lingkungan->read());
		}

		public function read_all() {
			$this->output->set_content_type('application/json')->set_output($this->sektor_bina_lingkungan->read_all());
		}

		public function create() {
			
			//$this->data_add['parentnode'] = $parentnode;
			
			$result = $this->sektor_bina_lingkungan->insert($this->data_add);
			if ($result == 1) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => $this->db->_error_message())));
			}
		}

		public function update() {
			$param = $this->uri->segment(4);

			$this->where_add['id_sektor_bl'] = $param;
			
			$result = $this->{$this->models[0]}->update($this->where_add, $this->data_add);
			
			if ($result == 1) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'Data Gagal Di Update !')));
			}
		}

		public function delete() {
			// additional where
			$this->where_add['id_sektor_bl'] = $this->uri->segment(4);
			
			$result = $this->sektor_bina_lingkungan->delete($this->where_add);
			if ($result == 1) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'Data Gagal Di Hapus !')));
			}
		}


	}
