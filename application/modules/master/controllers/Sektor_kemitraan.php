<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');
	class Sektor_kemitraan extends MY_Controller {
		public $models = array('sektor_kemitraan');
		
		public function __construct() {
			parent::__construct();
		}

		public function index() {
			$data = array();
			$data['menu'] = $this->model_menu->getAllMenu();

			$this->template->load('template', 'view_sektor_kemitraan', $data);
		}

		public function read() {
			$this->output->set_content_type('application/json')->set_output($this->sektor_kemitraan->read());
		}

		public function read_all() {
			$this->output->set_content_type('application/json')->set_output($this->sektor_kemitraan->read_all());
		}

		public function create() {
			
			//$this->data_add['parentnode'] = $parentnode;
			
			$result = $this->sektor_kemitraan->insert($this->data_add);
			if ($result == 1) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => $this->db->_error_message())));
			}
		}

		public function update() {
			$param = $this->uri->segment(4);

			$this->where_add['id_sektor'] = $param;
			
			$result = $this->{$this->models[0]}->update($this->where_add, $this->data_add);
			
			if ($result == 1) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'Data Gagal Di Update !')));
			}
		}

		public function delete() {
			// additional where
			$this->where_add['id_sektor'] = $this->uri->segment(4);
			
			$result = $this->sektor_kemitraan->delete($this->where_add);
			if ($result == 1) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'Data Gagal Di Hapus !')));
			}
		}


	}
