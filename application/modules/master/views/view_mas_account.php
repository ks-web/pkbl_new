<?php
$app = 'Master Account'; // nama aplikasi
$module = 'master';
$appLink = 'Mas_account'; // controller
$idField  = 'node_group'; //field key table
// $moduleref = '';
// $appLinkref = ''; // controller
?>

<script>
	var url;
	var app = "<?=$app?>";
	var appLink = '<?=$appLink?>';
	var module = '<?=$module?>';
	var idField = '<?=$idField?>';
    function add(){
        $('#dlg').dialog('open').dialog('setTitle','Tambah ' + app);
		$('#fm').form('clear');

		$('#parent_group').combotree('reload');
        var row = $('#dg').treegrid('getSelected');
        if(row){
			$('#fm').form('load', {parent_group: row.node_group});
        }

	    $('#tbl').html('Save');
		url = '<?=base_url($module . '/' . $appLink . '/create')?>';
    }
    function edit(){
        var row = $('#dg').treegrid('getSelected');
		if (row){
			$('#tbl').html('Simpan');
			$('#dlg').dialog('open').dialog('setTitle','Edit '+ app);
			$('#parent_group').combotree('reload');
			$('#fm').form('load',row);
			if (row.parent_group=='0') {
				$('#newparent').prop('checked', true);
			}
			url = '<?=base_url($module . '/' . $appLink . '/update')?>/'+row[idField];
	    }
    }
    function hapus(){
        var row = $('#dg').treegrid('getSelected');		
	    if (row){
			$.messager.confirm('Confirm','Apakah Anda Yakin Akan Menghapus ini?',function(r){
				if (r){
					$.post('<?=base_url($module . '/' . $appLink . '/delete')?>/'+row[idField],function(result){
						if (result.success){
							$('#dg').treegrid('reload');	// reload the user data
						} else {
							$.messager.show({	// show error message
								title: 'Error',
								msg:result.msg
							});
						}
					},'json');
				}
			});
		}
    }
    function save(){
	   $('#fm').form('submit',{
	    	url: url,
	    	onSubmit: function(){
	    		return $(this).form('validate');
	    	},
	    	success: function(result){
	    		var result = eval('('+result+')');
	    		if (result.success){
	    			$('#dlg').dialog('close');		
	    			$('#dg').treegrid('reload');
	                
	    		} else {
	             $.messager.alert('Error Message',result.msg,'error');
	    		}
	    	}
	    });
	}
	function doSearch(value){
	    $('#dg').treegrid('load',{    
	    	q:value  
	    });
	}
</script>
 
<div class="tabs-container">                
	<table id="dg" class="easyui-treegrid" 
	data-options="  
	    url: '<?=base_url($module . '/' . $appLink . '/read')?>',
		singleSelect:'true', 
	    title:'<?=$app?>',
	    toolbar:'#toolbar',
	    iconCls:'icon-cog',
	    rownumbers:'true',  
	    idField:'<?=$idField?>', 
	    treeField:'kode_account',
	    pagination:'true',
	    fitColumns:'true',
	    pageList: [10,20,30]
	    "
	>
	    <thead>
	        <th field="kode_account" width="200" sortable="true">Kode Account</th>
	        <th field="uraian" width="200" sortable="true">Nama Account</th>
	        <th field="tipe" width="200" sortable="true">Tipe</th>
	        
	    </thead>
	</table>
	
	<!-- Model Start -->
	<div id="dlg" class="easyui-dialog" style="width:500px; height:500px; padding:10px" closed="true" buttons="#t_dlg_dis-buttons" >
	    <form id="fm" method="post" enctype="multipart/form-data" action="">
        	<table width="100%" align="center" border="0">
	            <tr>
	                <td valign="top">Parent</td>
	                <td valign="top">:</td>
	                <td>
                    	<select name="parent_group" id="parent_group" class="easyui-combotree" style="width:300px;" data-options="
                        panelWidth:300,
                        value:'',
                        idField:'node_group',
                        textField:'uraian',
                        mode:'remote',
                        url:'<?=base_url($module . '/' . $appLink . '/read')?>',
                        columns:[[
                        {field:'node_group',title:'ID',width:100},
                        {field:'kode_account',title:'Kode Bidang',width:100},
                        {field:'uraian',title:'Nama Account',width:195}
                        ]],
                        onSelect: function(rec){
                        	$('#newparent').prop('checked', false);
                        }
                        "></select>
		                <br>
		                <input style="width:auto;" type="checkbox" name="newparent" id="newparent" value="1">New
	                </td>
	            </tr>
                <tr>
	                <td>Kode Account</td>
	                <td>:</td>
	                <td>
	                    <input type="text" name="kode_account" id="kode_account" style="width: 250px;">
	                </td>
	            </tr>
                <tr>
	                <td>Nama Account</td>
	                <td>:</td>
	                <td>
	                    <input type="text" name="uraian" id="uraian" style="width: 200px;">
	                </td>
	            </tr>
              <tr>
	            <td>tipe</td>
	            <td>:</td>
	            <td>
	            <select class="easyui-combobox" name="tipe" id="tipe" style="width:100px;">
                       <option value="PK">PK</option>
                       <option value="BL">BL</option>
                </select>	             
	            </td>
	          </tr>
			</table>
	    </form>
	   
	   	<!-- Tombol Add, Edit, Hapus treegrid -->
       	<div id="toolbar">  
            <table align="center" style="padding: 0px; width: 99%;">
                <tr>
                    <td>
                        <a href="javascript:void(0)" class="btn btn-small btn-success" onclick="javascript:add()"><i class="icon-plus-sign icon-large"></i>&nbsp;Add</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:edit()"><i class="icon-edit"></i>&nbsp;Edit</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-danger" onclick="javascript:hapus()"><i class="icon-trash icon-large"></i>&nbsp;Delete</a>
                    </td>
                    <td>&nbsp;</td>
                    <td align="right">
                    	<input class="easyui-searchbox" prompt="Silahkan Input Field" searcher="doSearch"  style="width:300px"/>
                    </td>
                </tr>
            </table>  
        </div>  
        <!-- end tombol treegrid -->
        <!-- Tombol simpan & Batal Form -->
        <div id="t_dlg_dis-buttons">
            <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:save()"><i class="icon-save icon-large"></i>&nbsp;Simpan</a>
            <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:$('#dlg').dialog('close')"><i class="icon-remove icon-large"></i>&nbsp;Batal</a>      
        </div>
	</div>
	<!-- Model end -->
</div>