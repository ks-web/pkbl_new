<?php
$app = 'Bidang Bantuan BL';
$module = 'master';
$appLink = 'bidangbantuanbl'; // controller
$idField  = 'Kode_group';

?>

<script>
var app = "<?=$app?>";
var appLink = '<?=$appLink?>';
var idField = '<?=$idField?>';

    function add(){
	$('#dlg').dialog('open').dialog('setTitle','Tambah ' + app);
	$('#fm').form('clear');
    $('#tbl').html('Save');
	url = '<?=base_url('master/' . $appLink . '/create')?>';
    $('#email').val('');
}
function edit(){
	var row = $('#dg').datagrid('getSelected');
	if (row){
	   $('#tbl').html('Simpan');
		$('#dlg').dialog('open').dialog('setTitle','Edit '+ app);
		$('#fm').form('load',row);
        url = '<?=base_url('master/' . $appLink . '/update')?>/'+row[idField];
	    }
}
    
function hapus(){
	var row = $('#dg').datagrid('getSelected');		
    if (row){
		$.messager.confirm('Confirm','Apakah Anda Yakin Akan Menghapus ini?',function(r){
			if (r){
				$.post('<?=base_url("master/" . $appLink . "/delete")?>/'+row[idField],function(result){
					if (result.success){
						$('#dg').datagrid('reload');	// reload the user data
					} else {
						$.messager.show({	// show error message
							title: 'Error',
							msg:result.msg
						});
					}
				},'json');
			}
		});
	}
}
    
function doSearch(value){
    $('#dg').datagrid('load',{    
		q:value  
    });   
}
    
</script>
 
<script type="text/javascript">
var url;
function save(){
    $('#fm').form('submit',{
        url: url,
        onSubmit: function(){
            return $(this).form('validate');
        },
        success: function(result){
            var result = eval('('+result+')');
            if (result.success){
                $('#dlg').dialog('close');      
                $('#dg').datagrid('reload');
                
            } else {
             $('#dlg').dialog('close');
                $.messager.alert('Error Message',result.msg,'error');
            }
        }
    });
}
function getthn(val){
	// alert(val);
	// if (val<>""){
		var url1= "<?=base_url("master/$appLink/read/")?>"+'/'+val;
		// alert(url1);
		// $('#dg').datagrid('load',url1);
		$('#dg').datagrid({'url':url1});
		$('#dg').datagrid('reload');


	// }

}

</script>

<div class="container_s">                
<table id="dg" class="easyui-datagrid" 
    url= "<?=base_url("master/$appLink/read")?>"
    singleSelect="true" 
    iconCls="icon-cogs" 
    rownumbers="true"  
    idField="<?=$idField?>",
    pagination="true"
    fitColumns="true"
    pageList= [10,20,30]
    toolbar="#toolbar"
    title="Daftar Sektor Bantuan Bina Lingkungan"
>
        <thead>
			 <th field="Kode_group" width="200" sortable="true">Kode Bidang Bantuan</th>
	        <th field="nama_bidangbantuan" width="200" sortable="true">Nama Bidang Bantuan</th>
	        <th field="unit" width="200" sortable="true">Unit</th>
	        <th field="tahun" width="200" sortable="true">Tahun</th>
	        <th field="anggaran" width="200" sortable="true">Anggaran</th>
        </thead>
 </table>
 
<!-- Model Start -->
<div id="dlg" class="easyui-dialog" style="width:600px; height:300px; padding:10px" closed="true" buttons="#t_dlg_dis-buttons" >
    <fieldset style="padding: 7px;">
    <form id="fm" method="post" enctype="multipart/form-data" action="">
    
        <table width="100%" align="center" border="0">
            <tr>
	            <td><input style="width:auto;" type="checkbox" name="newparent" id="newparent" value="1">New</td>
	            </tr>
	            <tr>
	            	<td>Tahun</td>
	            	<td>:</td>
	            	<td><input class="easyui-textbox" style="width: 100px;" name="tahun" id="tahun"></td>

	            </tr>
                
                <tr>
	                <td>Nama Bidang Bantuan</td>
	                <td>:</td>
	                <td>
	                    <input  name="nama_bidangbantuan" id="nama_bidangbantuan" style="width: 300px;" class="easyui-combobox" data-options="
                                    url:'<?=base_url($module . '/' . $appLink . '/getbidang')?>',
                                    method:'get',
                                    required:'true',
                                    valueField:'nama_bl',
                                    textField:'nama_bl',
                                    panelHeight:'auto',
                                    onSelect:function(param){
                                    $('#Kode_group').val(param.id_sektor_bl);
                                    }
                                ">
	                </td>
	                
	            </tr>

                <tr>
	              <td>Unit</td>
	                <td>:</td>
	                <td>
	                    <input type="text" name="unit" id="unit" style="width: 50px;">
	                    <input type="hidden" name="Kode_group" id="Kode_group" style="width: 250px;">
	                </td>
	            </tr>
                <tr>
	               <!-- <td>Satuan</td>
	                <td>:</td>
	                <td>
	                    <input class="easyui-numberbox" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%'" name="satuan" id="satuan" style="width: 250px;">
	                </td>-->
	            </tr>
                <tr>
	                <td>Anggaran</td>
	                <td>:</td>
	                <td>
	                    <input class="easyui-numberbox" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%'" name="anggaran" id="anggaran" style="width: 250px;">
	                </td>
	            </tr>
           
            
            </table>
            <div id="toolbar">
				<table align="center" style="padding: 0px; width: 99%;">
					<tr>
						<td>
							<a href="javascript:void(0)" class="btn btn-small btn-success" onclick="javascript:add()"><i class="icon-plus-sign icon-large"></i>&nbsp;Add</a>
							<a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:edit()"><i class="icon-edit"></i>&nbsp;Edit</a>
							<a href="javascript:void(0)" class="btn btn-small btn-danger" onclick="javascript:hapus()"><i class="icon-trash icon-large"></i>&nbsp;Delete</a></td>
							<td><input  name="thn" id="thn" class="easyui-combobox" data-options="
                                    url:'<?=base_url($module . '/' . $appLink . '/gettahun')?>',
                                    method:'get',
                                    required:'true',
                                    valueField:'tahun',
                                    textField:'tahun',
                                    panelHeight:'auto',
                                    onSelect:function(param){
                                    getthn(param.tahun);
                                    }
                                "></td>
						<td>&nbsp;</td>
						<td align="right">
						<input class="easyui-searchbox" prompt="Silahkan Input Field" searcher="doSearch"  style="width:300px"/>
						</td>
					</tr>
				</table>
			</div>
			<div id="t_dlg_dis-buttons">
				<a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:save()"><i class="icon-save icon-large"></i>&nbsp;Simpan</a>
				<a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:$('#dlg').dialog('close')"><i class="icon-remove icon-large"></i>&nbsp;Batal</a>
			</div>
            </fieldset>
    </form>
</div>
<!-- Model end -->   



</div>