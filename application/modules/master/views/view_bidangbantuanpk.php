<?php
$app = 'Alokasi Dana Kemitraan'; // nama aplikasi
$module = 'master';
$appLink = 'bidangbantuanpk'; // controller
$idField  = 'Node_group'; //field key table
// $moduleref = '';
// $appLinkref = ''; // controller
?>

<script>
	var url;
	var app = "<?=$app?>";
	var appLink = '<?=$appLink?>';
	var module = '<?=$module?>';
	var idField = '<?=$idField?>';
    function add(){
        $('#dlg').dialog('open').dialog('setTitle','Tambah ' + app);
		$('#fm').form('clear');
    	$('#persen_anggaran').hide();
    	$('#anggaran').show();

		$('#Parent_group').combotree('reload');
		// var row = $('#dg').treegrid('getSelected');
		// if(row){
		// 	$('#fm').form('load', {Parent_group: row.Node_group, anggaran: row.anggaran});
		// 	$('#persen_anggaran').show();
		// 	$('#anggaran').hide();
		// }

	    $('#tbl').html('Save');
		url = '<?=base_url($module . '/' . $appLink . '/create')?>';
    }
    function edit(){
    	$('#persen_anggaran').numberbox('setValue','');
        var row = $('#dg').treegrid('getSelected');
		if (row){
			$('#tbl').html('Simpan');
			$('#dlg').dialog('open').dialog('setTitle','Edit '+ app);
			$('#Parent_group').combotree('reload');
			$('#fm').form('load',row);
			if (row.Parent_group=='0') {
				$('#newparent').prop('checked', true);
			}
        	$('#persen_anggaran').hide();
        	$('#anggaran').show();
			url = '<?=base_url($module . '/' . $appLink . '/update')?>/'+row[idField];
	    }
    }
    function hapus(){
        var row = $('#dg').treegrid('getSelected');		
	    if (row){
			$.messager.confirm('Confirm','Apakah Anda Yakin Akan Menghapus ini?',function(r){
				if (r){
					$.post('<?=base_url($module . '/' . $appLink . '/delete')?>/'+row[idField],function(result){
						if (result.success){
							$('#dg').treegrid('reload');	// reload the user data
						} else {
							$.messager.show({	// show error message
								title: 'Error',
								msg:result.msg
							});
						}
					},'json');
				}
			});
		}
    }
    function save(){
	   $('#fm').form('submit',{
	    	url: url,
	    	onSubmit: function(){
	    		return $(this).form('validate');
	    	},
	    	success: function(result){
	    		var result = eval('('+result+')');
	    		if (result.success){
	    			$('#dlg').dialog('close');		
	    			$('#dg').treegrid('reload');
	                
	    		} else {
	             $.messager.alert('Error Message',result.msg,'error');
	    		}
	    	}
	    });
	}
	function doSearch(value){
	    $('#dg').treegrid('load',{    
	    	q:value  
	    });
	}
	function is_new() {
		if($('#newparent').prop('checked')){
			$('#anggaran').numberbox('setValue','').show();
			$('#persen_anggaran').hide();
		}else{
			$('#anggaran').numberbox('setValue','').hide();
			var datparent = $('#Parent_group').combotree('tree').tree('getSelected');
			if (datparent) {
				$('#anggaran').numberbox('setValue',parseInt(datparent.anggaran));
			}
			$('#persen_anggaran').show();
		}
	}
</script>
 
<div class="tabs-container">                
	<table id="dg" class="easyui-treegrid" 
	data-options="  
	    url: '<?=base_url($module . '/' . $appLink . '/read')?>',
		singleSelect:'true', 
	    title:'<?=$app?>',
	    toolbar:'#toolbar',
	    iconCls:'icon-cog',
	    rownumbers:'true',  
	    idField:'<?=$idField?>', 
	    treeField:'Kode_group',
	    pagination:'true',
	    fitColumns:'true',
	    pageList: [10,20,30]
	    "
	>
	    <thead>
	        <th field="Kode_group" width="200" sortable="true">Kode Sektor Usaha</th>
	        <th field="nama_sektor" width="200" sortable="true">Nama Sektor Usaha</th>
	        <th field="unit" width="200" sortable="true">Unit</th>
	        <th field="anggaran" width="200" sortable="true">Anggaran</th>
	    </thead>
	</table>
	
	<!-- Model Start -->
	<div id="dlg" class="easyui-dialog" style="width:450px; height:auto; padding:10px" closed="true" buttons="#t_dlg_dis-buttons" >
	    <form id="fm" method="post" enctype="multipart/form-data" action="">
        	<table width="100%" align="center" border="0">
	            <tr>
	                <td valign="top">Parent</td>
	                <td valign="top">:</td>
	                <td>
                    	<select name="Parent_group" id="Parent_group" class="easyui-combotree" style="width:200px;" data-options="
                        panelWidth:200,
                        panelHeight:'auto',
                        value:'',
                        idField:'Node_group',
                        textField:'nama_sektor',
                        mode:'remote',
                        url:'<?=base_url($module . '/' . $appLink . '/read')?>',
                        columns:[[
                        {field:'Node_group',title:'ID',width:100},
                        {field:'Kode_group',title:'Kode Sektor',width:100},
                        {field:'nama_sektor',title:'Nama Sektor',width:195}
                        ]],
                        onSelect: function(rec){
                        	$('#newparent').prop('checked', false);
                        	$('#persen_anggaran').show();
                        	$('#anggaran').numberbox('setValue',rec.anggaran).hide();
                        }
                        "></select>
		                <br>
		                <input style="width:auto;" type="checkbox" name="newparent" id="newparent" value="1"
		                onclick="is_new();">New
	                </td>
	            </tr>
                <tr>
	                <td>Kode Sektor Usaha</td>
	                <td>:</td>
	                <td>
	                    <input type="text" name="Kode_group" id="Kode_group" style="width: 100px;">
	                </td>
	            </tr>
                <tr>
	                <td>Nama Sektor Usaha</td>
	                <td>:</td>
	                <td>
	                    <input type="text" name="nama_sektor" id="nama_sektor" style="width: 200px;">
	                </td>
	            </tr>
                <tr>
	              <td>Unit</td>
	                <td>:</td>
	                <td>
	                    <input type="text" name="unit" id="unit" style="width: 100px;">
	                </td>
	            </tr>
                <tr>
	                <td>Anggaran</td>
	                <td>:</td>
	                <td>
	                    <input class="easyui-numberbox" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%'" name="anggaran" id="anggaran" style="width: 200px;">
	                    <input class="easyui-numberbox" style="display: none;" data-options="suffix:' %',width:'100%'" name="persen_anggaran" id="persen_anggaran" style="width: 200px;">
	                </td>
	            </tr>
			</table>
	    </form>
	   
	   	<!-- Tombol Add, Edit, Hapus treegrid -->
       	<div id="toolbar">  
            <table align="center" style="padding: 0px; width: 99%;">
                <tr>
                    <td>
                        <a href="javascript:void(0)" class="btn btn-small btn-success" onclick="javascript:add()"><i class="icon-plus-sign icon-large"></i>&nbsp;Add</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:edit()"><i class="icon-edit"></i>&nbsp;Edit</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-danger" onclick="javascript:hapus()"><i class="icon-trash icon-large"></i>&nbsp;Delete</a>
                    </td>
                    <td>&nbsp;</td>
                    <td align="right">
                    	<input class="easyui-searchbox" prompt="Silahkan Input Field" searcher="doSearch"  style="width:300px"/>
                    </td>
                </tr>
            </table>  
        </div>  
        <!-- end tombol treegrid -->
        <!-- Tombol simpan & Batal Form -->
        <div id="t_dlg_dis-buttons">
            <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:save()"><i class="icon-save icon-large"></i>&nbsp;Simpan</a>
            <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:$('#dlg').dialog('close')"><i class="icon-remove icon-large"></i>&nbsp;Batal</a>      
        </div>
	</div>
	<!-- Model end -->
</div>