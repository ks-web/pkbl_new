<?php
$app = 'Pegawai';
$appLink = 'pegawai';
?>

<script>
var app = "<?=$app?>";
var appLink = '<?=$appLink?>';

var file_type = JSON.parse('<?=fileUploadTipe2?>');
var file_upload = true;
    
function validate_file(obj){
    var file_name = $(obj).val().replace('C:\\fakepath\\', '');
    var file_name_attr = file_name.split('.');
    file_name_attr[2] = obj.files[0].size/1024;

   
    if(file_type.indexOf(file_name_attr[1]) == -1 || (file_name_attr[2] > <?=fileUploadTipe2?>)){
        $.messager.alert('Error Message', 'File upload harus (' + file_type.join('|') + ') dan size dibawah <?=fileUploadTipe2?>KB', 'error');
        $(obj).wrap('<form>').closest('form').get(0).reset();
        $(obj).unwrap();
    }
}

function add(){
    $('#dlg').dialog('open').dialog('setTitle','Tambah ' + app);
    $('#fm').form('clear');
    $('.edit').hide();
    $('#tbl').html('Save');
    file_upload = true;
    url = '<?=base_url('master/' . $appLink . '/create')?>';
    $('#email').val('');
}
function edit(){
	var row = $('#dg').datagrid('getSelected');
	if (row){
	   $('#tbl').html('Simpan');
		$('#dlg').dialog('open').dialog('setTitle','Edit '+ app);
		$('.edit').hide();
            if(row.ttd_img != ''){
                var ttd_img = '<?=base_url('')?>'+row.ttd_img;
                $('#current_file').attr('href', ttd_img);
                $('#current_file_inp').attr('value', row.ttd_img);
                // row.ttd_img = '';
                $('.edit').show();
                file_upload = false;
            }
            $('#fm').form('clear');
            $('#fm').form('load', {
                nip: row.nip,
                nama_pegawai: row.nama_pegawai,
                Divisi: row.Divisi,
                jabatan: row.jabatan,
                email: row.email,
                no_hp: row.no_hp              
            });
            // var str1 = "kl/2016/7/765/4";
            var str =row.idPegawai;
            var res = str.replace(/\//g, "_");
            // console.log(str+'-jadi-'+ res);        
        url = '<?=base_url('master/' . $appLink . '/update')?>/'+res;
	    }else {
            $.messager.alert('Error Message', 'Anda belum memilih data!', 'error');
        }
}
function hapus(){
	var row = $('#dg').datagrid('getSelected');		
    if (row){
		$.messager.confirm('Confirm','Apakah Anda Yakin Akan Menghapus ini?',function(r){
			if (r){
				$.post('<?=base_url("master/" . $appLink . "/delete")?>/'+row.idPegawai,function(result){
					if (result.success){
						$('#dg').datagrid('reload');	// reload the user data
					} else {
						$.messager.show({	// show error message
							title: 'Error',
							msg:result.msg
						});
					}
				},'json');
			}
		});
	}
}

/*function divisi_view (value,row,index){
	var temp = [];
	temp["H"] = "HRD";
	temp["B"] = "Barang";
	
	return temp[value];
}*/

</script>
<div class="container_s">
<table id="dg" class="easyui-datagrid" 
data-options="  
    url: '<?=base_url('master/' . $appLink . '/read')?>',
	singleSelect:'true', 
    title:'Pegawai',
    toolbar:'#toolbar',
    iconCls:'icon-cog',
    rownumbers:'true',  
    idField:'idPegawai', 
    pagination:'true',
    fitColumns:'true',
    pageList: [10,20,30]
    "
>
        <thead>
            <!-- <th field="idPegawai" width="30" sortable="true">ID</th> -->
            <th field="idPegawai" width="30" sortable="true">Nip</th>
            <th field="nama_pegawai" width="60" sortable="true">Nama</th>
            <th field="nama_divisi" width="60" sortable="true" formatter="">Divisi</th>
            <th field="jabatan" width="60" sortable="true" formatter="">Jabatan</th>
            <th field="email" width="60" sortable="true">E-Mail</th>
            <th field="no_hp" width="50" sortable="true">No Hp</th>
            <th field="ttd_img" width="100" formatter="file_download" align="center">File</th>
        </thead>
 </table>
 
<!-- Model Start -->
<div id="dlg" class="easyui-dialog" style="width:400px; height:300px; padding:10px" closed="true" buttons="#t_dlg_dis-buttons" >
    <form id="fm" method="post" enctype="multipart/form-data" action="">
        <table width="100%" align="center" border="0">
          <tr>
                <td>NIP</td>
                <td>:</td>
                <td>
                    <input type="text" name="idPegawai" id="idPegawai" required style="width: 130px;" class="easyui-validatebox" data-options="required:true">
                </td>
            </tr> 
             <!-- <tr>
                <td>Nip</td>
                <td>:</td>
                <td>
                    <input type="text" name="nip" id="nip" value="nip" required style="width: 130px;" class="easyui-validatebox" data-options="required:true">
                </td>
            </tr> -->
            <tr>
                <td>Nama</td>
                <td>:</td>
                <td>
                    <input type="text" name="nama_pegawai" id="nama_pegawai" required style="width: 250px;" class="easyui-validatebox" data-options="required:true">
                </td>
            </tr>
            <tr>
				<td>Divisi</td>
				<td>:</td>
				<td>
					<select name="Divisi" id="Divisi" class="easyui-combogrid" style="width:100px;" data-options="
                        required:true,
                        panelWidth:300,
                        value:'',
                        idField:'kode_divisi',
                        textField:'nama_divisi',
                        mode:'remote',
                        url:'<?=base_url('master/pegawai/divisi')?>',
                        columns:[[
                        {field:'kode_divisi',title:'Kode Divisi',width:100},
                        {field:'nama_divisi',title:'Nama Divisi',width:195}
                        ]]
                        "></select>
				</td>
			</tr>
            <tr>
                <td>Jabatan</td>
                <td>:</td>
                <td><input type="hidden" id="id_jabatan" name="id_jabatan">
                    <select name="jabatan" id="jabatan" class="easyui-combogrid" style="width:150px;" data-options="
                        required:true,
                        panelWidth:300,
                        value:'jabatan',
                        idField:'jabatan',
                        textField:'jabatan',
                        mode:'remote',
                        url:'<?=base_url('master/pegawai/jabatan')?>',
                        columns:[[
                        {field:'id_jabatan',title:'Id Jabatan',width:100},
                        {field:'jabatan',title:'Nama Jabatan',width:195}
                        ]],
                        onSelect :function(indek,row)
                        {
                            $('#id_jabatan').val(row.id_jabatan);
                        }
                        "></select>
                </td>
            </tr>
            <tr>
                <td>E-Mail</td>
                <td>:</td>
                <td>
                    <input type="email" name="email" id="email" required style="width: 250px;">
                </td>
            </tr>
            <tr>
                <td>No Hp</td>
                <td>:</td>
                <td>
                    <input type="text" name="no_hp" id="no_hp" required style="width: 250px;">
                </td>
            </tr>
            <tr>
                <td>File Dokumen</td>
                <td>:</td>
                <td><a href="" id="current_file" target="_blank" class="edit">File</a><input type="hidden" name="current_file" id="current_file_inp" />
                <input type="file" id="ttd_img" name="ttd_img" size="20" onchange="validate_file(this)" />
                </td>
            </tr>
        </table>
        
    </form>
   <script type="text/javascript">
var url;
function save(){
    $('#fm').form('submit',{
    	url: url,
    	onSubmit: function(){
    		var ret = $(this).form('validate');
            // for file upload
                if(file_upload){
                    if($('#ttd_img').get(0).files.length === 0) {
                        ret = false;
                    }
                }
            if(!ret){
                    $.messager.alert('Error Message', 'Mohon lengkapi data', 'error');
                }
                return ret;
    	},
    	success: function(result){
    		var result = eval('('+result+')');
    		if (result.success){
    			$('#dlg').dialog('close');		
    			$('#dg').datagrid('reload');
                //$('#dg').treegrid('reload');
                
    		} else {
    		 //$('#dlg').dialog('close');
             $.messager.alert('Error Message',result.msg,'error');
    		}
    	}
    });
}

function doSearch(value){
    $('#dg').datagrid('load',{    
    q:value  
    });   
}
function file_download(value,row,index){
    var file = ''
    if(value != '') {
        file = '<a target="_blank" href="<?=base_url()?>'+ value +'" class="btn btn-small">Download</a>';
    }
    
    return file;
}
</script>
   <!-- Tombol Add, Edit, Hapus Datagrid -->
        <div id="toolbar">  
            <table align="center" style="padding: 0px; width: 99%;">
                <tr>
                    <td>
                        <a href="javascript:void(0)" class="btn btn-small btn-success" onclick="javascript:add()"><i class="icon-plus-sign icon-large"></i>&nbsp;Add</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:edit()"><i class="icon-edit"></i>&nbsp;Edit</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-danger" onclick="javascript:hapus()"><i class="icon-trash icon-large"></i>&nbsp;Delete</a>
                    </td>
                    <td>&nbsp;</td>
                    <td align="right">
                    	<input class="easyui-searchbox" prompt="Silahkan Input Field" searcher="doSearch"  style="width:300px"/>
                    </td>
                </tr>
            </table>  
        </div>  
        <!-- end tombol datagrid -->
        <!-- Tombol simpan & Batal Form -->
        <div id="t_dlg_dis-buttons">
            <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:save()"><i class="icon-save icon-large"></i>&nbsp;Simpan</a>
            <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:$('#dlg').dialog('close')"><i class="icon-remove icon-large"></i>&nbsp;Batal</a>      
        </div>
</div>
<!-- Model end -->   

</div>
