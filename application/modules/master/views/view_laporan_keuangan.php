<?php
$app = 'Laporan Keuangan'; // nama aplikasi
$module = 'master';
$appLink = 'laporan_keuangan'; // controller
$idField  = 'id_laporan'; //field key table
// $moduleref = '';
// $appLinkref = ''; // controller
?>

<script>
    var url;
    var app = "<?=$app?>";
    var appLink = '<?=$appLink?>';
    var module = '<?=$module?>';
    var idField = '<?=$idField?>';
    
    function add(){
        $('#dlg').dialog('open').dialog('setTitle','Tambah ' + app);
        $('#tbl').html('Save');
        $('#fm').form('clear');
        url = '<?=base_url($module . '/' . $appLink . '/create')?>';
        $('#group_laporan').combogrid({
            url:'<?=base_url('master/laporan_keuangan/getGroupLaporan')?>',
       });
        $('#parent_2').combogrid('disable');
        $('#parent_3').combogrid('disable');
    }

    function save(){
       $('#fm').form('submit',{
            url: url,
            onSubmit: function(){
                return $(this).form('validate');
            },
            success: function(result){
                var result = eval('('+result+')');
                if (result.success){
                    $('#dg').treegrid('reload');
                    $('#dlg').dialog('close');
                } else {
                 $.messager.alert('Error Message',result.msg,'error');
                }
            }
        });
    }
    function save_edit(){
       $('#fm_edit').form('submit',{
            url: url,
            onSubmit: function(){
                return $(this).form('validate');
            },
            success: function(result){
                var result = eval('('+result+')');
                if (result.success){
                    $('#dg').treegrid('reload');
                    $('#dlg_edit').dialog('close');
                } else {
                 $.messager.alert('Error Message',result.msg,'error');
                }
            }
        });
    }

    function doSearch(value){
        $('#dg').treegrid('load',{    
            q:value  
        });
    }
    function tiktok(){
        var disable = $('#parent_2').combogrid('disable');
        var enable = $('#parent_2').combogrid('enable');
        if(disable){
            enable
        }else{
            disable
        }
           
    }
    function hapus(){
        var node = $('#dg').treegrid('getSelected');
        console.log(node);
    }
    function edit(){
        var node = $('#dg').treegrid('getSelected');
        if (node){
            $('#dlg_edit').dialog('open').dialog('setTitle','Edit '+ app);
            $('#fm_edit').form('clear');
            url = '<?=base_url($module . '/' . $appLink . '/update')?>';
        
            $('#edit_keterangan').val(node.keterangan);
            $('#edit_id_laporan').val(node.id_laporan);
            $('#edit_parent_laporan').val(node.parent_laporan);
            $('#edit_node_laporan').val(node.node_laporan);
            $('#edit_parent_laporan_val').val(node.parent_laporan);
            $('#edit_node_laporan_val').val(node.node_laporan);

            if(node.parent_laporan == 0){
                $('#tr_account').hide();
                $('#tr_header').hide();
                $('#tr_laporan').show();
                $('#tr_header').hide();

                $('#edit_group_laporan').combogrid({
                    url:'<?=base_url('master/laporan_keuangan/getGroupLaporan')?>',
                });
                $('#edit_group_laporan').combogrid('setValue',node.id_laporan);
            }else if(node.kode_account){
                $('#tr_laporan').hide();
                $('#tr_header').hide();
                $('#tr_account').show();
                $('#tr_header').hide();

                $('#edit_account').combogrid({
                    url:'<?=base_url('master/laporan_keuangan/get_data')?>',
                });
                $('#edit_account').combogrid('setValue',node.kode_account);
            }else{
                $('#tr_laporan').hide();
                $('#tr_header').hide();
                $('#tr_account').hide();
                $('#tr_header').show();
            }
            
        }

    }
    $(document).ready(function(){
            // $('#dg').treegrid('collapseAll',2);
            // $('#dg').treegrid(':node_laporan',{
            //     id:'1', 
            // });
    });
        
</script>
 
<div class="tabs-container">                
    
    
    <table id="dg" title="Laporan Keuangan" class="easyui-treegrid" style="height: 500px"
            data-options="
                url: '<?=base_url($module . '/' . $appLink . '/getTree')?>',
                title:'<?=$app?>',
                toolbar:'#toolbar',
                lines: true,
                //rownumbers:'true',
                idField:'node_laporan', 
                treeField:'keterangan',
                fitColumns:'true'
          
            ">
        <thead>
            <tr>
                <th field="node_laporan" width="10" sortable="true">Node</th>
                <th field="parent_laporan" width="10" sortable="true">Parent</th>
                <th field="keterangan" width="150" sortable="true">Keterangan</th>
                <th field="kode_account" width="30" sortable="true">Account</th>
            </tr>
        </thead>
    </table>
        
    <!-- Model Start -->
    <div id="dlg" class="easyui-dialog" style="width:500px; height:350px; padding:10px" closed="true" buttons="#t_dlg_dis-buttons" >
        <form id="fm" method="post" enctype="multipart/form-data" action="">
            <table width="100%" align="center" border="0">
                <tr>
                    <td valign="top">Group Laporan</td>
                    <td valign="top">:</td>
                    <td><select id="group_laporan" name="laporan" class="easyui-combogrid" required style="width: 250px"
    					data-options="
    					panelWidth:250,
    					mode:'remote',
    					
    					idField:'id_laporan',
    					textField:'group_laporan',
    					columns:[[
    					{field:'id_laporan',title:'ID',width:50},
    					{field:'group_laporan',title:'Group Laporan',width:200}
    					]],
                        onSelect: function(index, row){
                           console.log(row);
                           $('#nama_group').val(row.group_laporan);
                           $('#parent_laporan_group').val(row.node_laporan);
                           $('#node').combogrid({
                                url:'<?=base_url('master/laporan_keuangan/getNode')?>/'+row.id_laporan,
                           })
                        }
    					"></select>
                        
    				</td>
                </tr>
                <tr>
                    <td valign="top">Node</td>
                    <td valign="top">:</td>
                    <td><select id="node" name="node" class="easyui-combogrid" style="width: 250px"
                        data-options="
                        panelWidth:350,
                        mode:'remote',
                        
                        idField:'kode_account',
                        textField:'uraian',
                        columns:[[
                        {field:'kode_account',title:'Account',width:100},
                        {field:'uraian',title:'Uraian',width:200}
                        ]],
                        onSelect: function(index, row){
                            $('#nama_group').val(row.uraian);
                            $('#parent_laporan').val(row.node_laporan);
                            $('#header').combogrid({
                                url:'<?=base_url('master/laporan_keuangan/getHeader')?>/'+row.node_laporan,
                            })
                            console.log(row);
                        }
                        "></select>
                        <br>
                        <input type="text" name="group_keterangan[]" id="nama_group" hidden>
                        <input type="text" name="group_keterangan[]" id="nama_group_2" hidden>
                        <input type="text" name="group_keterangan[]" id="nama_group_3" hidden>
                        <input type="text" name="parent_laporan_group" id="parent_laporan_group" hidden style="width: 100px" >
                        <input type="text" name="parent_laporan" id="parent_laporan" hidden style="width: 100px" >
                    </td>
                </tr>
                <tr>
                    <td valign="top">Header</td>
                    <td valign="top">:</td>
                    <td><select id="header" name="header" class="easyui-combogrid" style="width: 250px"
                        data-options="
                        panelWidth:350,
                        mode:'remote',
                        
                        idField:'node_laporan',
                        textField:'keterangan',
                        columns:[[
                        {field:'node_laporan',title:'Node',width:100},
                        {field:'keterangan',title:'Keterangan',width:200}
                        ]]
                        "></select><br/><br/>
                    </td>
                </tr>
                <tr>
                    <td valign="top">Parent</td>
                    <td valign="top">:</td>
                    <td>1 - <select id="parent_1" name="parent[]" class="easyui-combogrid" style="width: 250px"
                        data-options="
                        panelWidth:500,
                        mode:'remote',
                        url:'<?=base_url('master/laporan_keuangan/getParent')?>',
                        idField:'kode_account',
                        textField:'uraian',
                        columns:[[
                        {field:'kode_account',title:'Account',width:100},
                        {field:'uraian',title:'Uraian',width:400}
                        ]],
                        onSelect: function(index, row){
                            $('#nama_group').val(row.uraian);
                        }
                        "></select><br/>
                        2 - <select id="parent_2" name="parent[]" class="easyui-combogrid" style="width: 250px"
                        data-options="
                        panelWidth:500,
                        mode:'remote',
                        url:'<?=base_url('master/laporan_keuangan/getParent')?>',
                        idField:'kode_account',
                        textField:'uraian',
                        columns:[[
                        {field:'kode_account',title:'Account',width:100},
                        {field:'uraian',title:'Uraian',width:400}
                        ]],
                        onSelect: function(index, row){
                            $('#nama_group_2').val(row.uraian);
                        }
                        "></select>
                        <a class="btn" onclick="$('#parent_2').combogrid('disable');"><i class="icon-remove icon-small"></i></a> 
                        <a class="btn" onclick="$('#parent_2').combogrid('enable');"><i class="icon-ok icon-small"></i></a>
                        <br/>
                        3 - <select id="parent_3" name="parent[]" class="easyui-combogrid" style="width: 250px"
                        data-options="
                        panelWidth:500,
                        mode:'remote',
                        url:'<?=base_url('master/laporan_keuangan/getParent')?>',
                        idField:'kode_account',
                        textField:'uraian',
                        columns:[[
                        {field:'kode_account',title:'Account',width:100},
                        {field:'uraian',title:'Uraian',width:400}
                        ]],
                        onSelect: function(index, row){
                            $('#nama_group_3').val(row.uraian);
                        }
                        "></select>
                        <a class="btn" onclick="$('#parent_3').combogrid('disable');"><i class="icon-remove icon-small"></i></a> 
                        <a class="btn" onclick="$('#parent_3').combogrid('enable');"><i class="icon-ok icon-small"></i></a>
                    </td>
                </tr>
            </table>
        </form>
       
        <!-- Tombol Add, Edit, Hapus treegrid -->
        <div id="toolbar">  
            <table align="center" style="padding: 0px; width: 99%;">
                <tr>
                    <td>
                        <a href="javascript:void(0)" class="btn btn-small btn-success" onclick="javascript:add()"><i class="icon-plus-sign icon-large"></i>&nbsp;Add</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:edit()"><i class="icon-edit"></i>&nbsp;Edit</a>
                        <!-- <a href="javascript:void(0)" class="btn btn-small btn-danger" onclick="javascript:hapus()"><i class="icon-trash icon-large"></i>&nbsp;Delete</a> -->
                    </td>
                    <td>&nbsp;</td>
                    <td align="right">
                        <input class="easyui-searchbox" prompt="Silahkan Input Field" searcher="doSearch"  style="width:300px"/>
                    </td>
                </tr>
            </table>  
        </div>  
        <!-- end tombol treegrid -->
        <!-- Tombol simpan & Batal Form -->
        <div id="t_dlg_dis-buttons">
            <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:save()"><i class="icon-save icon-large"></i>&nbsp;Simpan</a>
            <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:$('#dlg').dialog('close')"><i class="icon-remove icon-large"></i>&nbsp;Batal</a>      
        </div>
    </div>
    <!-- Model end -->
    <div id="dlg_edit" class="easyui-dialog" style="width:500px; height:200px; padding:10px" closed="true" buttons="#t_button" >
        <form id="fm_edit" method="post" enctype="multipart/form-data">
            <table>
                <tr id="tr_laporan">
                    <td >Group Laporan</td>
                    <td> : </td>
                    <td>
                        <select id="edit_group_laporan" name="laporan" class="easyui-combogrid" style="width: 300px"
                        data-options="
                        panelWidth:250,
                        mode:'remote',
                        
                        idField:'id_laporan',
                        textField:'group_laporan',
                        columns:[[
                        {field:'id_laporan',title:'ID',width:50},
                        {field:'group_laporan',title:'Group Laporan',width:200}
                        ]],
                        onSelect: function(index, row){
                           $('#edit_keterangan').val(row.group_laporan);
                        }
                        "></select>
                    </td>
                </tr>
                <tr id="tr_account">
                    <td> Account </td>
                    <td> : </td>
                    <td>
                        <select id="edit_account" name="account" class="easyui-combogrid" style="width: 300px"
                        data-options="
                        panelWidth:350,
                        mode:'remote',
                        
                        idField:'kode_account',
                        textField:'uraian',
                        columns:[[
                        {field:'kode_account',title:'Account',width:100},
                        {field:'uraian',title:'Uraian',width:200}
                        ]]
                        
                        "></select>
                    </td>
                </tr>
                <tr id="tr_header">
                    <td> Header </td>
                    <td> : </td>
                    <td> <input type="text" name="keterangan" id="edit_keterangan"> </td>
                </tr>
                <tr>
                    <td>Node</td>
                    <td> : </td>
                    <td> <input type="text" name="node_laporan" id="edit_node_laporan" style="width: 50px"> </td>
                </tr>
                <tr>
                    <td>Parent</td>
                    <td> : </td>
                    <td> <input type="text" name="parent_laporan" id="edit_parent_laporan" style="width: 50px"> </td>
                </tr>
            </table>
            <input type="text" name="id_laporan" id="edit_id_laporan" hidden>
            <input type="text" name="node_laporan_val" id="edit_node_laporan_val" style="width: 50px" hidden>
            <input type="text" name="parent_laporan_val" id="edit_parent_laporan_val" style="width: 50px" hidden>
            
            
        </form>

    </div>
    <div id="t_button">
        <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:save_edit()"><i class="icon-save icon-large"></i>&nbsp;Simpan</a>
        <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:$('#dlg_edit').dialog('close')"><i class="icon-remove icon-large"></i>&nbsp;Batal</a>      
    </div>
</div>
