<?php
$app = 'Unit Kerja';
$appLink = 'unit_kerja';
$idField  = 'kode_divisi';

?>

<script>
var app = "<?=$app?>";
var appLink = '<?=$appLink?>';
var idField = '<?=$idField?>';

function add(){
    $('#dlg').dialog('open').dialog('setTitle','Tambah ' + app);
    $('#fm').form('clear');
    $('#tbl').html('Save');
    url = '<?=base_url('master/' . $appLink . '/create')?>';
}
function edit(){
    var row = $('#dg').datagrid('getSelected');
    if (row){
        $('#tbl').html('Simpan');
        $('#dlg').dialog('open').dialog('setTitle','Edit '+ app);
        $('#fm').form('load',row);
        url = '<?=base_url('master/' . $appLink . '/update')?>/'+row[idField];
    }
}
function hapus(){
    var row = $('#dg').datagrid('getSelected');     
    if (row){
        $.messager.confirm('Confirm','Apakah Anda Yakin Akan Menghapus ini?',function(r){
            if (r){
                $.post('<?=base_url("master/" . $appLink . "/delete")?>/'+row[idField],function(result){
                    if (result.success){
                        $('#dg').datagrid('reload');    // reload the user data
                    } else {
                        $.messager.show({   // show error message
                            title: 'Error',
                            msg:result.msg
                        });
                    }
                },'json');
            }
        });
    }
}

$(function(){
    $('input[name="kode_divisi"]:radio').change(
        function(){
            $('#kode_divisi').combobox('reload', '<?=base_url('master/divisi/get_divisi')?>/' + this.value);
        }
    );
});

</script>
<div class="container_s">
<table id="dg" class="easyui-datagrid" 
data-options="  
    url: '<?=base_url('master/' . $appLink . '/read')?>',
    singleSelect:'true', 
    title:'<?=$app?>',
    toolbar:'#toolbar',
    iconCls:'icon-cog',
    rownumbers:'true',  
    idField:'<?=$idField?>', 
    pagination:'true',
    fitColumns:'true',
    pageList: [10,20,30]
    "
>
        <thead>
            <th field="kode_divisi" width="150" sortable="true">Kode Divisi</th>
            <th field="nama_divisi" width="300" sortable="true">Nama Divisi</th>
            <th field="cost_centre" width="150" sortable="true">Cost Centre</th>
        </thead>
 </table>
 
<!-- Model Start -->
<div id="dlg" class="easyui-dialog" style="width:500px; height:350px; padding:10px" closed="true" buttons="#t_dlg_dis-buttons" >
    <form id="fm" method="post" enctype="multipart/form-data" action="">
        <table width="100%" align="center" border="0">
        <!-- <tr>
                <td>Parent</td>
                <td>:</td>
                <td><select id="Parent_group" name="Parent_group" class="easyui-combogrid" style="width:300px;"
                        data-options="
                        panelWidth:350,
                        value:'',
                        url:'<?php echo base_url('master/unit_kerja/getCboParent'); ?>',
                        mode:'remote',
                        idField:'Node_group',
                        textField:'nama_divisi',
                        columns:[[
                        {field:'kode_divisi',title:'Kode Unit Kerja',width:100,sortable:true},
                        {field:'nama_divisi',title:'Unit Kerja',width:250,sortable:true},
                        {field:'Node_group',title:'Node Group',width:100,sortable:true}, 
                        ]]"></select>
                </td>
            </tr> -->
            <tr>
                <td>Kode Divisi</td>
                <td>:</td>
                <td>
                    <input type="text" name="kode_divisi" id="kode_divisi" required style="width: 150px;" class="easyui-validatebox" data-options="required:true">
                </td>
            </tr>
            <tr>
                <td>Nama Divisi</td>
                <td>:</td>
                <td>
                    <input type="text" name="nama_divisi" id="nama_divisi" required style="width: 250px;" class="easyui-textbox" data-options="required:true">
                </td>
            </tr>
            <tr>
                <td>Cost Centre</td>
                <td>:</td>
                <td>
                    <input type="text" name="cost_centre" id="cost_centre" required style="width: 150px;" class="easyui-numberbox" data-options="min:0,precision:2" >
                </td>
            </tr>
            
        </table>
        
    </form>
   <script>
        var url;
        function save(){
            $('#fm').form('submit',{
                url: url,
                onSubmit: function(){
                    return $(this).form('validate');
                },
                success: function(result){
                    var result = eval('('+result+')');
                    if (result.success){
                        $('#dlg').dialog('close');      
                        $('#dg').datagrid('reload');
                        //$('#dg').treegrid('reload');
                        
                    } else {
                     //$('#dlg').dialog('close');
                     $.messager.alert('Error Message',result.msg,'error');
                    }
                }
            });
        }
        
        function doSearch(value){
            $('#dg').datagrid('load',{    
            q:value  
            });   
        }
</script>
   <!-- Tombol Add, Edit, Hapus Datagrid -->
        <div id="toolbar">  
            <table align="center" style="padding: 0px; width: 99%;">
                <tr>
                    <td>
                        <a href="javascript:void(0)" class="btn btn-small btn-success" onclick="javascript:add()"><i class="icon-plus-sign icon-large"></i>&nbsp;Add</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:edit()"><i class="icon-edit"></i>&nbsp;Edit</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-danger" onclick="javascript:hapus()"><i class="icon-trash icon-large"></i>&nbsp;Delete</a>
                    </td>
                    <td>&nbsp;</td>
                    <td align="right">
                        <input class="easyui-searchbox" prompt="Silahkan Input Field" searcher="doSearch"  style="width:300px"/>
                    </td>
                </tr>
            </table>  
        </div>  
        <!-- end tombol datagrid -->
        <!-- Tombol simpan & Batal Form -->
        <div id="t_dlg_dis-buttons">
            <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:save()"><i class="icon-save icon-large"></i>&nbsp;Simpan</a>
            <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:$('#dlg').dialog('close')"><i class="icon-remove icon-large"></i>&nbsp;Batal</a>      
        </div>
</div>
<!-- Model end -->   

</div>
