<?php
$app = 'Sektor Bina Lingkungan';
$appLink = 'sektor_bina_lingkungan';

?>

<script>
var app = "<?=$app?>";
var appLink = '<?=$appLink?>';

    function add(){
	$('#dlg').dialog('open').dialog('setTitle','Tambah ' + app);
	$('#fm').form('clear');
    $('#tbl').html('Save');
	url = '<?=base_url('master/' . $appLink . '/create')?>';
    $('#email').val('');
}
function edit(){
	var row = $('#dg').datagrid('getSelected');
	if (row){
	   $('#tbl').html('Simpan');
		$('#dlg').dialog('open').dialog('setTitle','Edit '+ app);
		$('#fm').form('load',row);
        url = '<?=base_url('master/' . $appLink . '/update')?>/'+row.id_sektor_bl;
	    }
}
    
function hapus(){
	var row = $('#dg').datagrid('getSelected');		
    if (row){
		$.messager.confirm('Confirm','Apakah Anda Yakin Akan Menghapus ini?',function(r){
			if (r){
				$.post('<?=base_url("master/" . $appLink . "/delete")?>/'+row.id_sektor_bl,function(result){
					if (result.success){
						$('#dg').datagrid('reload');	// reload the user data
					} else {
						$.messager.show({	// show error message
							title: 'Error',
							msg:result.msg
						});
					}
				},'json');
			}
		});
	}
}
    
function doSearch(value){
    $('#dg').datagrid('load',{    
		q:value  
    });   
}
    
</script>

<div class="container_s">                
<table id="dg" class="easyui-datagrid" 
    url= "<?=base_url("master/sektor_bina_lingkungan/read")?>"
    singleSelect="true" 
    iconCls="icon-cogs" 
    rownumbers="true"  
    idField="id" 
    pagination="true"
    fitColumns="true"
    pageList= [10,20,30]
    toolbar="#toolbar"
    title="Daftar Sektor Bina Lingkungan"
>
        <thead>
            <th field="nama_bl" width="35" >Nama Sektor</th>
            <th field="keterangan" width="100" >Keterangan</th>
            <!-- <th field="aktif" width="50"  formatter="status" align="center">Status</th> -->
        </thead>
 </table>
 
<!-- Model Start -->
<div id="dlg" class="easyui-dialog" style="width:550px; height:270px; padding:10px" closed="true" buttons="#t_dlg_dis-buttons" >
    <fieldset style="padding: 7px;">
    <form id="fm" method="post" enctype="multipart/form-data" action="">
    
        <table width="100%" align="center" border="0">
<!--         	<tr class="add">
                <td>User Type</td>
                <td>: <input type="radio" id="user_type_peg" name="user_type" value="Pegawai" onchange="usertipe(this)" style="width: 10px;"/> Pegawai
                	<input type="radio" id="user_type_ven" name="user_type"  value="Vendor" onchange="usertipe(this)" style="width: 10px;"/> Vendor
                </td>
            </tr> -->
            <tr class="Sektor Bina Lingkungan add">
            	<tr >
				<input type="hidden" id="id_sektor_bl" name="id_sektor" style="width:70%;"  />
	            <td>Nama Sektor</td>
					<td> : </td>
	                <td><input type="text" id="nama_bl" name="nama_bl" style="width:70%;"  />
	                </td>
	            </tr>
	            <td>Keterangan</td>
					<td> : </td>
	                <td><textarea id="keterangan" name="keterangan" style="border:1;width:100%;height:100px;"></textarea></td>
	            </tr>
            <tr>
           
            
            </table>
            </fieldset>
    </form>
    <?=$this->load->view('toolbar')?>
</div>
<!-- Model end -->   



</div>