<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

Class Model_realisasi_transaksi extends MY_Model {
	function __construct() {
		parent::__construct();
		$this->_table 	= "trealisasi";
		$this->_view 	= "trealisasi_vd";
		$this->_order 	= 'asc';
		$this->_sort 	= 'id_realisasi';
		$this->_page 	= 1;
		$this->_rows 	= 10;

		$this->_update = true;
		$this->_create = true;

		if ($this->uri->segment(4)) {
			$this->_filter = array('id_realisasi' => $this->uri->segment(4));
		}
		if ($this->input->post('q')) {
			$this->_like = array(	
				'id_realisasi' 	=> $this->input->post('q'),
				'nama' 			=> $this->input->post('q'),
				'transaksi'	 	=> $this->input->post('q')
			);
		}

		$this->_data = array(
			'jenis_transaksi' 	=> $this->input->post('jenis_transaksi'),
			'nomor' 			=> $this->input->post('nomor'),
			'date_modifier' 	=> date('Y-m-d'),
			'modifier' 			=> $this->session->userdata('id')
		);
	}
	
	public function get_tcd_all($sektor)
	{
		if($sektor == '1')
			$jenis_trx = 'BL';
		else if($sektor == '2')
			$jenis_trx = 'KM';
		else	
			$jenis_trx = 'PP';
			
		$this->db->select('tcd.*, bl.*');
		$this->db->join('tblingkungan bl' , 'tcd.no = bl.id_bl', 'left');
		$this->db->where('tcd.code',$jenis_trx);
		$q = $this->db->get('tcd_all tcd');
		return json_encode($q->result_array());
	}
	
	public function read_detail($id_realisasi)
	{
		//$this->db->select();
		$this->db->where('id_realisasi',$id_realisasi);
		$q = $this->db->get('trealisasi_detail');
		return json_encode($q->result_array());
	}
	
	public function save_detail($data)
	{
		$config['upload_path'] = 'assets/upload/realisasi/';
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        $this->upload->set_allowed_types('*');
        
        if (!$this->upload->do_upload('file')) {
            $data['file'] = '';
        }else{
            $this->upload->do_upload('file');
            $file = $this->upload->data();
            $data['file'] = $config['upload_path'].$file['file_name'];
        }
		
		$q = $this->db->insert('trealisasi_detail',$data);
        if ($q) 
		{
            $this->output->set_output(json_encode(array('success'=>TRUE)));
        }else
		{
            $this->output->set_output(json_encode(array('msg'=>'Terjadi kesalahan dalam penambahan data')));
		}
	}
	/*
	public function get_id_realisasi()
	{
		$this->db->select_max('id_realisasi');
		$q 		= $this->db->get('trealisasi')->row();			
		$tahun 	= substr($q->id_realisasi,2,4);
		$seq	= substr($q->id_realisasi,8,3);
		if($q->id_realisasi == NULL)
			return 'RL'.date('Ym').'001';
			
		else
		{
			if($tahun < date('Y'))
				return 'RL'.date('Ym').'001';
					
			else
			{
				$hasil = $seq + 1;
				$nomor = sprintf("%03s", $hasil);
				return 'RL'.date('Ym').$nomor;
			}
		}		
	}
	*/
}