<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	Class Model_laporanmt extends MY_Model {
		function __construct() {
			parent::__construct();
            $this->_view = "tcd_mt_vd";
           
		}
        function getData($rows, $offset, $sort, $order,$sektor_usaha,$tglawal,$tglakhir,$keputusan,$kota) {
        
        if($kota!=''){
	       $this->db->where('a.kota',$kota);
	    } 
	    if($keputusan!=''){
	       $this->db->where('a.keputusan',$keputusan);
	    }
        if($sektor_usaha!=''){
	       $this->db->where('a.sektor_usaha',$sektor_usaha);
	    }
        if($tglawal!=0){
           $this->db->where('a.tanggal_input >=',$tglawal);
        }
        if($tglakhir!=0){
           $this->db->where('a.tanggal_input <=',$tglakhir); 
        }
        $this->db->order_by($sort, $order);
        $this->db->limit($rows, $offset);
                    
        $data = array();
        $data['rows']  = $this->db->get('laporan_bl_vd as a')->result();
        if($kota!=''){
	       $this->db->where('a.kota',$kota);
	    } 
        if($keputusan!=''){
	       $this->db->where('a.keputusan',$keputusan);
	    }
        if($sektor_usaha!=''){
	       $this->db->where('a.sektor_usaha',$sektor_usaha);
	    }
        if($tglawal!=0){
           $this->db->where('a.tanggal_input >=',$tglawal);
        }
        if($tglakhir!=0){
           $this->db->where('a.tanggal_input <=',$tglakhir); 
        }
        $data['total']  = $this->db->get('laporan_bl_vd as a')->num_rows();
        $this->output->set_output(json_encode($data));
            //echo $this->db->last_query();
		}
        public function previewlaporanbl($propinsi,$kota,$nama_sektor,$tgl_awal,$tgl_akhir){        
            
            if ( $propinsi !='x' ) {
                $this->db->where('propinsi ', str_replace("%20"," ",$propinsi) );
            }
            if ( $kota !='x' ) {
                $this->db->where('kota ', str_replace("%20"," ",$kota) );
            }
            if ( $nama_sektor !='x' ) {
                $this->db->where('nama_sektor ', str_replace("%20"," ",$nama_sektor) );
            }
            if ( $tgl_awal !='x' ) {
                $this->db->where('tanggal_input>=', date('Y-m-d', strtotime($tgl_awal)) );
            }
            if ( $tgl_akhir !='x' ) {
                $this->db->where('tanggal_input <=', date('Y-m-d', strtotime($tgl_akhir)) );
            }

            $data = array();
            $data['rows']  = $this->db->get('laporan_mt_vd');
            //echo $this->db->last_query();
            return $data['rows'];
		}

        function sektor_mt($cari) {
            $this->db->select('id_sektor, nama_sektor, keterangan');
            $this->db->like('CONCAT(id_sektor,nama_sektor)', $cari);
            $data = $this->db->get('sektor_kemitraan')->result();
            $this->output->set_output(json_encode($data));
        }
        public function getPropinsi($filter)
        {
            $this->db->select('A.*');
            $this->db->from('provinces A');
            $this->db->where('id','36');
            $this->db->or_where('id','32');
            $this->db->where($filter);

            $query = $this->db->get();

            return $query->result_array();
        }
        public function getKota($filter)
        {
            $this->db->select('A.*');
            $this->db->from('regencies A');
            $this->db->where($filter);

            $query = $this->db->get();

            return $query->result_array();
        }
        public function get_data_for_excel($filter1) {
			/*if (sizeof($get) > 0) {
				$this->db->where($get);
			}*/
			if($filter1=='1'){
			     $result = $this->db->get('laporan_mt_vd_by_sektor');
			}
            if($filter1=='2'){
			     $result = $this->db->get('laporan_mt_vd_by_wilayah');
			}
			
			$data = $result->result_array();
			return $data;
		}
        public function get_data_for_excel2() {
			/*if (sizeof($get) > 0) {
				$this->db->where($get);
			}*/
			
			$result = $this->db->get('laporan_mt_vd_by_wilayah');
			$data = $result->result_array();
			return $data;
		}
       
       
       
	}
