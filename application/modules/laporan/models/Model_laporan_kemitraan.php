<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	Class Model_laporan_kemitraan extends MY_Model {
		function __construct() {
			parent::__construct();
            $this->_view = "tcd_bl_vd";
           
		}
        function getData($rows, $offset, $sort, $order,$jenisbl,$tglawal,$tglakhir,$keputusan,$kota) {
        
        if($kota!=''){
	       $this->db->where('a.kota',$kota);
	    } 
	    if($keputusan!=''){
	       $this->db->where('a.keputusan',$keputusan);
	    }
        if($jenisbl!=''){
	       $this->db->where('a.jenis_bl',$jenisbl);
	    }
        if($tglawal!=0){
           $this->db->where('a.tanggal_input >=',$tglawal);
        }
        if($tglakhir!=0){
           $this->db->where('a.tanggal_input <=',$tglakhir); 
        }
        $this->db->order_by($sort, $order);
        $this->db->limit($rows, $offset);
                    
        $data = array();
        $data['rows']  = $this->db->get('laporan_bl_vd as a')->result();
        if($kota!=''){
	       $this->db->where('a.kota',$kota);
	    } 
        if($keputusan!=''){
	       $this->db->where('a.keputusan',$keputusan);
	    }
        if($jenisbl!=''){
	       $this->db->where('a.jenis_bl',$jenisbl);
	    }
        if($tglawal!=0){
           $this->db->where('a.tanggal_input >=',$tglawal);
        }
        if($tglakhir!=0){
           $this->db->where('a.tanggal_input <=',$tglakhir); 
        }
        $data['total']  = $this->db->get('laporan_bl_vd as a')->num_rows();
        $this->output->set_output(json_encode($data));
            //echo $this->db->last_query();
		}
        public function previewlaporanbl($propinsi,$kota,$nama_bl,$tgl_awal,$tgl_akhir){        
            
            if ( $propinsi !='x' ) {
                $this->db->where('propinsi ', str_replace("%20"," ",$propinsi) );
            }
            if ( $kota !='x' ) {
                $this->db->where('kota ', str_replace("%20"," ",$kota) );
            }
            if ( $nama_bl !='x' ) {
                $this->db->where('nama_bl ', str_replace("%20"," ",$nama_bl) );
            }
            if ( $tgl_awal !='x' ) {
                $this->db->where('tanggal_input>=', date('Y-m-d', strtotime($tgl_awal)) );
            }
            if ( $tgl_akhir !='x' ) {
                $this->db->where('tanggal_input <=', date('Y-m-d', strtotime($tgl_akhir)) );
            }

            $data = array();
            $data['rows']  = $this->db->get('laporan_bl_vd');
            //echo $this->db->last_query();
            return $data['rows'];
		}

        function sektor_bl($cari) {
            $this->db->select('id_sektor_bl, nama_bl, keterangan');
            $this->db->like('CONCAT(id_sektor_bl,nama_bl)', $cari);
            $data = $this->db->get('sektor_bl')->result();
            $this->output->set_output(json_encode($data));
        }
        public function getPropinsi($filter)
        {
            $this->db->select('A.*');
            $this->db->from('provinces A');
            $this->db->where('id','36');
            $this->db->or_where('id','32');
            $this->db->where($filter);

            $query = $this->db->get();

            return $query->result_array();
        }
        public function getKota($filter)
        {
            $this->db->select('A.*');
            $this->db->from('regencies A');
            $this->db->where($filter);

            $query = $this->db->get();

            return $query->result_array();
        }
        public function get_data_for_excel($filter1) {
			if($filter1=='1'){
			     $result = $this->db->get('laporan_bl_vd_by_sektor');
			}
            if($filter1=='2'){
			     $result = $this->db->get('laporan_bl_vd_by_wilayah');
			}
			
			$data = $result->result_array();
			return $data;
		}
        public function get_data_for_excel2() {
			$result = $this->db->get('laporan_bl_vd_by_wilayah');
			$data = $result->result_array();
			return $data;
		}



        public function getLaporanKemitraan($filter)
        {
            $data = $this->db->query("CALL laporan_kemitraan_sp(".$filter.")");
            //$result = $data->result();
            
            return $data->result_array();
        }
       
        public function getDataLaporan($tahun = null, $tahun_sebelum = null, $filter1 = null, $filter2 = null)
        {
            $data = false;
            if($tahun && $tahun_sebelum){
                if($filter1 == '1' && $filter2 == '1'){
                    $q = $this->db->query("CALL sp_laporan_kemitraan_penyaluran_sektor(".$tahun_sebelum.", ".$tahun.")");
                    $data = $q->result_array();
                }
                else if($filter1 == '1' && $filter2 == '2'){
                    $q = $this->db->query("CALL sp_laporan_kemitraan_penyaluran_provinsi(".$tahun_sebelum.", ".$tahun.")");
                    $data = $q->result_array();
                }
                else if($filter1 == '2' && $filter2 == '1'){
                    $q = $this->db->query("CALL sp_laporan_kemitraan_piutang_sektor(".$tahun_sebelum.", ".$tahun.")");
                    $data = $q->result_array();
                }
                else if($filter1 == '2' && $filter2 == '2'){
                    $q = $this->db->query("CALL sp_laporan_kemitraan_piutang_provinsi(".$tahun_sebelum.", ".$tahun.")");
                    $data = $q->result_array();
                }
                else if($filter1 == '3' && $filter2 == '1'){
                    $q = $this->db->query("CALL sp_laporan_kemitraan_sector_plan_sektor(".$tahun.")");
                    $data = $q->result_array();
                }
                
                
            }
           
            return $data;
        }
       
	}
