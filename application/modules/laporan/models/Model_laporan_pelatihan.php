<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	Class Model_laporan_pelatihan extends MY_Model {
		function __construct() {
			parent::__construct();
			$this->_table = "tpersetujuan_mitra"; // for insert, update, delete
			$this->_view = "laporan_pelatihan_vd"; // for call view
			$this->_order = 'desc';
			$this->_sort = 'no_kontrak';
			$this->_page = 1;
			$this->_rows = 10;

			$this->_create = true;
			$this->_update = true;

			// if ($this->uri->segment(4)) {
				// $this->_filter = array('ID' => $this->uri->segment(4));
			// }

			//parameter from post/get - search function
			if ($this->input->post('q')) {
				$this->_like = array(
					'id_mitra' => $this->input->post('q'),
					'nama' => $this->input->post('q'),
					'nama_perusahaan' => $this->input->post('q')
				);
			}

			// $this->_param = array('node' => $this->input->post('node'));

			//data array for input to database
			$this->_data = array(
				// 'col1' => $this->input->post('col1'),
				// 'col2' => $this->input->post('col2'),
			);
			
		}
		
		public function get_surveyor(){
			$this->db->from('laporan_pelatihan_surveyor_vd');

			return $this->db->get()->result_array();
		}

	}
