<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	Class Model_laporan_summary_km extends MY_Model {
		function __construct() {
			parent::__construct();
			$this->_table = "tmitra"; // for insert, update, delete
			$this->_view = "laporan_summary_km_vd"; // for call view
			$this->_order = 'asc';
			$this->_sort = 'id_mitra';
			$this->_page = 1;
			if($this->input->post('sub') == '1'){
				$this->_rows = 10000000;
			} else {
				$this->_rows = 10;
			}

			$this->_create = true;
			$this->_update = true;

			if ($this->input->post('districts')) {
				$this->_filter = array('tanggal_cd >=' => $this->input->post('dari'), 'tanggal_cd <=' => $this->input->post('sampai'), 'Node_group' => $this->input->post('jeniskm'), 'propinsi' => $this->input->post('provinces'), 'kota' => $this->input->post('regencies'), 'kecamatan' => $this->input->post('districts'), 'kelurahan' => $this->input->post('villages'));
			} else if ($this->input->post('districts')) {
				$this->_filter = array('tanggal_cd >=' => $this->input->post('dari'), 'tanggal_cd <=' => $this->input->post('sampai'), 'Node_group' => $this->input->post('jeniskm'), 'propinsi' => $this->input->post('provinces'), 'kota' => $this->input->post('regencies'), 'kecamatan' => $this->input->post('districts'));
			} else if ($this->input->post('regencies')) {
				$this->_filter = array('tanggal_cd >=' => $this->input->post('dari'), 'tanggal_cd <=' => $this->input->post('sampai'), 'Node_group' => $this->input->post('jeniskm'), 'propinsi' => $this->input->post('provinces'), 'kota' => $this->input->post('regencies'));
			} else if ($this->input->post('provinces')) {
				$this->_filter = array('tanggal_cd >=' => $this->input->post('dari'), 'tanggal_cd <=' => $this->input->post('sampai'), 'Node_group' => $this->input->post('jeniskm'), 'propinsi' => $this->input->post('provinces'));
			} else if ($this->input->post('jeniskm')) {
				$this->_filter = array('tanggal_cd >=' => $this->input->post('dari'), 'tanggal_cd <=' => $this->input->post('sampai'), 'Node_group' => $this->input->post('jeniskm'));
			} else if ($this->input->post('sampai') && $this->input->post('dari')) {
				$this->_filter = array('tanggal_cd >=' => $this->input->post('dari'), 'tanggal_cd <=' => $this->input->post('sampai'));
			}

			//parameter from post/get - search function
			if ($this->input->post('q')) {
				$this->_like = array(
					'id_bl' => $this->input->post('q')
				);
			}

			$this->_param = array('id_bl' => $this->input->post('id_bl'));

			//data array for input to database
			$this->_data = array(
				
			);
			
		}

		public function getJenisKM($filter) {
			$this->db->select('A.*');
	        $this->db->from('sektor_kemitraan A');
	        $this->db->where($filter);
	        
	        $query = $this->db->get();
	        
	        return $query->result_array();
		}

		public function getProvinces($filter, $like) {
			$this->db->select('A.*');
	        $this->db->from('provinces A');
	        $this->db->or_like($like);
			$this->db->where($filter);
	        
	        $query = $this->db->get();
	        
	        return $query->result_array();
		}

		public function getRegencies($filter, $like) {
			$this->db->select('A.*');
	        $this->db->from('regencies A');
	        $this->db->or_like($like);
			$this->db->where($filter);
	        
	        $query = $this->db->get();
	        
	        return $query->result_array();
		}

		public function getDistricts($filter, $like) {
			$this->db->select('A.*');
	        $this->db->from('districts A');
	        $this->db->or_like($like);
			$this->db->where($filter);
	        
	        $query = $this->db->get();
	        
	        return $query->result_array();
		}

		public function getVillages($filter, $like) {
			$this->db->select('A.*');
	        $this->db->from('villages A');
	        $this->db->or_like($like);
			$this->db->where($filter);
	        
	        $query = $this->db->get();
	        
	        return $query->result_array();
		}

	}
