<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');
	class Laporan_pelatihan extends MY_Controller {
		public $models = array('laporan_pelatihan');
		
		public function __construct() {
			parent::__construct();
		}

		public function index() {
			$data = array();
			$data['menu'] = $this->model_menu->getAllMenu();

			$this->template->load('template', 'view_laporan_pelatihan', $data);
		}

		public function read() {
			$this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read());
		}

		public function read_all() {
			$this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read_all());
		}
		
		public function read_preview($id = null){
			if ($id) {
				// $arr_id = explode('_', $str_id);
				$this->db->where('petugas_survey', $id);

				$check = $this->db->get('laporan_pelatihan_vd');
				if ($check->num_rows() > 0) {
					$this->load->helper('tanggal_indo');

					// $data['no_pkbl'] = '001/USL/PKBL-KS/2017';
					$data['data'] = $check->result_array();

					// kota tanggal
					// $data['kota'] = 'Cilegon';
					// $data['tanggal'] = '25 Oktober 2016';

					$html = $this->load->view('laporan_pelatihan_preview', $data, true);

					$this->load->library('dompdf_gen');
					$this->dompdf->set_paper('legal', 'landscape');

					$this->dompdf->load_html($html);
					$this->dompdf->render();
					$this->dompdf->stream("Laporan Pelatihan.pdf");
				} else {
					show_404();
				}
			} else {
				show_404();
			}
		}
		
		public function get_surveyor() {
			$ret = $this->{$this->models[0]}->get_surveyor();

			echo json_encode($ret);
		}

		public function create() {
			// // additional block
// 
			// // addtional get
			// $result = $this->{$this->models[0]}->insert($this->data_add);
			// if ($result == 1) {
				// $this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			// } else {
				// $this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => $this->db->_error_message())));
			// }
		}

		public function update() {
			// $param = $this->uri->segment(4); // parameter key
// 
			// // additional block
// 
			// // addtional data
// 
			// // additional where
			// $this->where_add['param'] = $param;
// 
			// $result = $this->{$this->models[0]}->update($this->where_add, $this->data_add);
			// if ($result == 1) {
				// $this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			// } else {
				// $this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'Data Gagal Di Update !')));
			// }
		}

		public function delete() {
			// $param = $this->uri->segment(4); // parameter key
// 
			// // additional block
// 
			// // additional where
			// $this->where_add['param'] = $param;
// 
			// $result = $this->{$this->models[0]}->delete($this->where_add);
			// if ($result == 1) {
				// $this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			// } else {
				// $this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'Data Gagal Di Hapus !')));
			// }
		}

	}
