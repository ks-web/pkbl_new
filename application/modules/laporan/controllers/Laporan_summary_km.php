<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');
	class Laporan_summary_km extends MY_Controller {
		public $models = array('laporan_summary_km');
		
		public function __construct() {
			parent::__construct();
		}

		public function index() {
			$data = array();
			$data['menu'] = $this->model_menu->getAllMenu();

			$this->template->load('template', 'view_laporan_summary_km', $data);
		}

		public function read() {
			$this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read());
			//echo $this->db->last_query();
		}

		public function read_all() {
			$this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read_all());
		}

		public function getJenisKM() {
			$param = $this->uri->segment(4); 
			$filter = array();
			$this->output->set_content_type('application/json')->set_output(json_encode($this->{$this->models[0]}->getJenisKM($filter)));
		}

		public function getProvinces() {
			$param = $this->input->post('q');
			$filter = array();
			$like = array('A.name' => $param);
			$this->output->set_content_type('application/json')->set_output(json_encode($this->{$this->models[0]}->getProvinces($filter, $like)));
		}

		public function getRegencies() {
			$id = $this->uri->segment(4); 
			if($id != ''){ 
				$param = $this->input->post('q');
				$filter = array('A.province_id' => $id);
				$like = array('A.name' => $param);
				$this->output->set_content_type('application/json')->set_output(json_encode($this->{$this->models[0]}->getRegencies($filter, $like)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array()));
			}
		}

		public function getDistricts() {
			$id = $this->uri->segment(4); 
			if($id != ''){ 
				$param = $this->input->post('q');
				$filter = array('A.regency_id' => $id);
				$like = array('A.name' => $param);
				$this->output->set_content_type('application/json')->set_output(json_encode($this->{$this->models[0]}->getDistricts($filter, $like)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array()));
			}
		}

		public function getVillages() {
			$id = $this->uri->segment(4); 
			if($id != ''){ 
				$param = $this->input->post('q');
				$filter = array('A.district_id' => $id);
				$like = array('A.name' => $param);
				$this->output->set_content_type('application/json')->set_output(json_encode($this->{$this->models[0]}->getVillages($filter, $like)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array()));
			}
		}

		function exportPdf()
		{   
		    $json = json_decode($this->{$this->models[0]}->read(), true);
			$data['summary'] = $json['rows'];
		    $html = $this->load->view('view_laporan_summary_km_pdf',$data, true);
			
  			$this->load->library('dompdf_gen'); 
			$this->dompdf->set_paper('A4', 'landscape'); 
			$this->dompdf->load_html($html);
			$this->dompdf->render();
			$this->dompdf->stream("Laporan Summary KM.pdf");
			//echo "asdfsd";
			//echo $this->input->post('sampai');
			/*$json = json_decode($this->{$this->models[0]}->read(), true);
			$data['summary'] = $json['rows'];
			$this->load->view('view_laporan_summary_km_pdf',$data);*/
			
    	}

	}
