<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');
		
		
	class Realisasi_transaksi extends MY_Controller {
		public $models = array('realisasi_transaksi');
		
		public function __construct() {
			parent::__construct();
		}

		public function index() {
			$data = array();
			$data['menu'] = $this->model_menu->getAllMenu();

			$this->template->load('template', 'view_realisasi_transaksi', $data);
		}
		
		public function read() {

			$this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read());
		}
		
		public function create() {
			//$this->data_add['id_realisasi'] = $this->get_id_realisasi();
			$result = $this->{$this->models[0]}->insert($this->data_add);
			if ($result == 1) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => $this->db->_error_message())));
			}
		}

		
		public function update() {
			$param = $this->uri->segment(4); 
			$this->where_add['id_realisasi'] = $param;

			$result = $this->{$this->models[0]}->update($this->where_add, $this->data_add);
			if ($result == 1) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'Data Gagal Di Update !')));
			}
		}

		public function delete() {
			$param = $this->uri->segment(4);
			$this->where_add['id_penerimaan_kas'] = $param;

			$result = $this->{$this->models[0]}->delete($this->where_add);
			if ($result == 1) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'Data Gagal Di Hapus !')));
			}
		}
		
		
		public function get_tcd_all($sektor)
		{
			$this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->get_tcd_all($sektor));
		}
		
		public function save_detail()
		{
			$data = array  (
				'id_realisasi' => $this->input->post('id_realisasi'),
				'deskripsi_sebelum' => $this->input->post('deskripsi_sebelum'),
				'deskripsi_sesudah' => $this->input->post('deskripsi_sesudah'),
				'file'	=> $this->input->post('file'),
				'tanggal_create' 	=> date('Y-m-d'),
				'creator' 			=> $this->session->userdata('id')
			);
			
			$this->{$this->models[0]}->save_detail($data);
		}
		
		public function read_detail($id_realisasi)
		{
			$this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read_detail($id_realisasi));
		}
		/*
		public function get_id_realisasi()
		{
			$this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->get_id_realisasi());
		}
		*/
		
		
	}