<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');
	class Laporan_kemitraan extends MY_Controller {
		public $models = array('laporan_kemitraan');
		
		public function __construct() {
			parent::__construct();
			$this->load->helper('text');
		}

		public function index() {
			$data = array();
			$data['menu'] = $this->model_menu->getAllMenu();

			$this->template->load('template', 'view_laporan_kemitraan', $data);
		}

		public function read_preview_excel($filter1 = null, $filter2 = null) {
		  	//load our new PHPExcel library
			$this->load->library('excel');
			//activate worksheet number 1
			$this->excel->setActiveSheetIndex(0);
			//name the worksheet
			$this->excel->getActiveSheet()->setTitle('Laporan Kemitraan'); 
            //style
            $style = array(
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                )
            );
            $vertical_center = array(
	        'alignment' => array(   
	            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
	                ),
	         );
             $vertical_top_right = array(
	        'alignment' => array(   
	            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
	            'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
	                ),
	        );
            //border
            $border_all = array(
	           'borders' => array(
	                 // 'outline' => array(
	                 'allborders' => array(
	                        'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
	                        'color' => array('argb' => '000000'),
	                 ),
	           ),
	        );
            //set dimmension column
            $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
            $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
            $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
            $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
            $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
            $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
            $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
            $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
            $this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(15);
            
            //header
            $b7 = 'Sektor Usaha Mitra Binaan';
            $thnberjalan=date('Y');
            //$thnsebelum=date('Y')-1;
            $header1='PT KRAKATAU STEEL (Persero) Tbk.';
            $header2='Posisi Piutang Program Kemitraan Tanggal 31 Desember Tahun '.$thnberjalan;
            if(($filter1=='1'&&$filter2=='1')||($filter1=='1'&&$filter2=='2')){
                $header3='Berdasarkan Sektor dan Kwalitas Piutang';
            }
            if(($filter1=='2'&&$filter2=='1')||($filter1=='2'&&$filter2=='2')){
                $header3='Berdasarkan Provinsi dan Kwalitas Piutang';
            }
            
			//set cell A2 content with some text
            $this->excel->getActiveSheet()->setCellValue('B2', $header1);
            $this->excel->getActiveSheet()->setCellValue('B3', $header2);
            $this->excel->getActiveSheet()->setCellValue('B4', $header3);
            $this->excel->getActiveSheet()->mergeCells('B2:J2');
            $this->excel->getActiveSheet()->mergeCells('B3:J3');
            $this->excel->getActiveSheet()->mergeCells('B4:J4');

            $this->excel->getActiveSheet()->getStyle('B2:J2')->applyFromArray($style);
            $this->excel->getActiveSheet()->getStyle('B3:J3')->applyFromArray($style);
            $this->excel->getActiveSheet()->getStyle('B4:J4')->applyFromArray($style);

            //header table
            $this->excel->getActiveSheet()->setCellValue('B6', $b7);
            $this->excel->getActiveSheet()->setCellValue('B9', '1');
            $this->excel->getActiveSheet()->setCellValue('C6', 'Lancar');
            $this->excel->getActiveSheet()->setCellValue('C8', 'Unit MB');
            $this->excel->getActiveSheet()->setCellValue('C9', '2');
            $this->excel->getActiveSheet()->setCellValue('D8', 'Rp');
            $this->excel->getActiveSheet()->setCellValue('D9', '3');
            $this->excel->getActiveSheet()->setCellValue('E6', 'Kurang Lancar');
            $this->excel->getActiveSheet()->setCellValue('E8', 'Unit MB');
            $this->excel->getActiveSheet()->setCellValue('E9', '4');
            $this->excel->getActiveSheet()->setCellValue('F8', 'Rp');
            $this->excel->getActiveSheet()->setCellValue('F9', '5');
            $this->excel->getActiveSheet()->setCellValue('G6', 'Diragukan');
            $this->excel->getActiveSheet()->setCellValue('G8', 'Unit MB');
            $this->excel->getActiveSheet()->setCellValue('G9', '6');
            $this->excel->getActiveSheet()->setCellValue('H8', 'Rp');
            $this->excel->getActiveSheet()->setCellValue('H9', '7');
            $this->excel->getActiveSheet()->setCellValue('I6', 'Macet');
            $this->excel->getActiveSheet()->setCellValue('I8', 'Unit MB');
            $this->excel->getActiveSheet()->setCellValue('I9', '8');
            $this->excel->getActiveSheet()->setCellValue('J8', 'Rp');
            $this->excel->getActiveSheet()->setCellValue('J9', '9');

            $this->excel->getActiveSheet()->mergeCells('B6:B8');
            $this->excel->getActiveSheet()->getStyle('B6:B8')->applyFromArray($vertical_center);
            //$this->excel->getActiveSheet()->getStyle('B6:B8')->applyFromArray($border_all);

            $this->excel->getActiveSheet()->mergeCells('C6:D7');
            $this->excel->getActiveSheet()->getStyle('C6:D7')->applyFromArray($vertical_center);
            //$this->excel->getActiveSheet()->getStyle('C6:D7')->applyFromArray($border_all);
            
            $this->excel->getActiveSheet()->mergeCells('E6:F7');
            $this->excel->getActiveSheet()->getStyle('E6:F7')->applyFromArray($vertical_center);
            //$this->excel->getActiveSheet()->getStyle('E6:F7')->applyFromArray($border_all);

            $this->excel->getActiveSheet()->mergeCells('G6:H7');
            $this->excel->getActiveSheet()->getStyle('G6:H7')->applyFromArray($vertical_center);
            //$this->excel->getActiveSheet()->getStyle('G6:H7')->applyFromArray($border_all);

            $this->excel->getActiveSheet()->mergeCells('I6:J7');
            $this->excel->getActiveSheet()->getStyle('I6:J7')->applyFromArray($vertical_center);
            //$this->excel->getActiveSheet()->getStyle('I6:J7')->applyFromArray($border_all);

            $this->excel->getActiveSheet()->getStyle('C8:J8')->applyFromArray($style);
            //$this->excel->getActiveSheet()->getStyle('C8:J8')->applyFromArray($border_all);
            $this->excel->getActiveSheet()->getStyle('B9:J9')->applyFromArray($style);
            //$this->excel->getActiveSheet()->getStyle('B9:J9')->applyFromArray($border_all);

            $data_laporan = $this->{$this->models[0]}->getLaporanKemitraan($filter2);
            //print_r($data_laporan);

            $i = 10; $j = $i;
            foreach($data_laporan as $data){
                $this->excel->getActiveSheet()->setCellValue('B'.$i, $data['nama_sektor']);
                $this->excel->getActiveSheet()->setCellValue('C'.$i, $data['lancar']);
                $this->excel->getActiveSheet()->setCellValue('D'.$i, $data['lancar_bayar']);
                $this->excel->getActiveSheet()->setCellValue('E'.$i, $data['tidak_lancar']);
                $this->excel->getActiveSheet()->setCellValue('F'.$i, $data['tidak_lancar_bayar']);
                $this->excel->getActiveSheet()->setCellValue('G'.$i, $data['diragukan']);
                $this->excel->getActiveSheet()->setCellValue('H'.$i, $data['diragukan_bayar']);
                $this->excel->getActiveSheet()->setCellValue('I'.$i, $data['macet']);
                $this->excel->getActiveSheet()->setCellValue('J'.$i, $data['macet_bayar']);
                $i++;
            }

            $this->excel->getActiveSheet()->setCellValue('B'.$i, 'Jumlah');
            $this->excel->getActiveSheet()->setCellValue('C'.$i, '=SUM(C'.$j.':C'.($i-1).')');
            $this->excel->getActiveSheet()->setCellValue('D'.$i, '=SUM(D'.$j.':D'.($i-1).')');
            $this->excel->getActiveSheet()->setCellValue('E'.$i, '=SUM(E'.$j.':E'.($i-1).')');
            $this->excel->getActiveSheet()->setCellValue('F'.$i, '=SUM(F'.$j.':F'.($i-1).')');
            $this->excel->getActiveSheet()->setCellValue('G'.$i, '=SUM(G'.$j.':G'.($i-1).')');
            $this->excel->getActiveSheet()->setCellValue('H'.$i, '=SUM(H'.$j.':H'.($i-1).')');
            $this->excel->getActiveSheet()->setCellValue('I'.$i, '=SUM(I'.$j.':I'.($i-1).')');
            $this->excel->getActiveSheet()->setCellValue('J'.$i, '=SUM(J'.$j.':J'.($i-1).')');
            $this->excel->getActiveSheet()->getStyle('B6:J'.$i)->applyFromArray($border_all);

            
			$filename = 'Laporan Kemitraan.xls';
			//save our workbook as this file name
			header('Content-Type: application/vnd.ms-excel');
			//mime type
			header('Content-Disposition: attachment;filename="' . $filename . '"');
			//tell browser what's the file name
			header('Cache-Control: max-age=0');
			//no cache

			//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007'
			// (and adjust the filename extension, also the header mime type)
			//if you want to save it as .XLSX Excel 2007 format
			$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
			//force user to download the Excel file without writing it to server's HD
			$objWriter->save('php://output');
          }
          
          function read_laporan($filter1 = null, $filter2 = null, $tahun = null) {
              if($filter1 && $filter2 && $tahun){
                  $filename = null;
                  $tahunint = (int) $tahun;
                  $tahun_sebelum = $tahunint - 1;
                  $tahun_sebelum = (string) $tahun_sebelum;
                  
                  
                  if($filter1 == '1' && $filter2 == '1'){
                      // penyaluran - sektor
                      
                      $this->load->library('excel');
                      $objPHPExcel = PHPExcel_IOFactory::load(APPPATH . 'template_excel/km-penyaluran-sektor.xls');
                      
                      $data =$this->{$this->models[0]}->getDataLaporan($tahun, $tahun_sebelum, $filter1, $filter2);
                      
                      // set header
                      $objPHPExcel->getActiveSheet()->setCellValue('E5', 'Tahun '.$tahun_sebelum.' dan Tahun ' . $tahun);
                      $objPHPExcel->getActiveSheet()->setCellValue('C9', 's.d Tahun '. $tahun_sebelum);
                      $objPHPExcel->getActiveSheet()->setCellValue('E9', 'Tahun '. $tahun);
                      $objPHPExcel->getActiveSheet()->setCellValue('G9', 's.d Tahun '. $tahun);
                      
                      $baseRow = 13;
                      $tot_unit_tahun_sebelum = 0;
                      $tot_rupiah_tahun_sebelum = 0;
                      $tot_unit_tahun_sekarang = 0;
                      $tot_rupiah_tahun_sekarang = 0;
                      $tot_unit = 0;
                      $tot_rupiah = 0;
                      foreach ($data as $key => $value) {
                          $row = $baseRow + $key;
                          
                          $objPHPExcel->getActiveSheet()->insertNewRowBefore($row,1);
                          
                          $objPHPExcel->getActiveSheet()->setCellValue('B'.$row, $value['nama_sektor']);
                          $objPHPExcel->getActiveSheet()->setCellValue('C'.$row, $value['unit_tahun_sebelum']);
                          $tot_unit_tahun_sebelum += $value['unit_tahun_sebelum'];
                          $objPHPExcel->getActiveSheet()->setCellValue('D'.$row, $value['rupiah_tahun_sebelum']);
                          $tot_rupiah_tahun_sebelum += $value['rupiah_tahun_sebelum'];
                          $objPHPExcel->getActiveSheet()->setCellValue('E'.$row, $value['unit_tahun_sekarang']);
                          $tot_unit_tahun_sekarang += $value['unit_tahun_sekarang'];
                          $objPHPExcel->getActiveSheet()->setCellValue('F'.$row, $value['rupiah_tahun_sekarang']);
                          $tot_rupiah_tahun_sekarang += $value['rupiah_tahun_sekarang'];
                          
                          $objPHPExcel->getActiveSheet()->setCellValue('G'.$row, $value['unit_tahun_sebelum']+$value['unit_tahun_sekarang']);
                          $tot_unit += $value['unit_tahun_sebelum']+$value['unit_tahun_sekarang'];
                          $objPHPExcel->getActiveSheet()->setCellValue('H'.$row, $value['rupiah_tahun_sebelum']+$value['rupiah_tahun_sekarang']);
                          $tot_rupiah += $value['rupiah_tahun_sebelum']+$value['rupiah_tahun_sekarang'];
                      }
                      $objPHPExcel->getActiveSheet()->removeRow($baseRow-1,1);
                      $row_total = $row + 1;
                      $objPHPExcel->getActiveSheet()->setCellValue('C'.$row_total, $tot_unit_tahun_sebelum);
                      $objPHPExcel->getActiveSheet()->setCellValue('D'.$row_total, $tot_rupiah_tahun_sebelum);
                      $objPHPExcel->getActiveSheet()->setCellValue('E'.$row_total, $tot_unit_tahun_sekarang);
                      $objPHPExcel->getActiveSheet()->setCellValue('F'.$row_total, $tot_rupiah_tahun_sekarang);
                      $objPHPExcel->getActiveSheet()->setCellValue('G'.$row_total, $tot_unit);
                      $objPHPExcel->getActiveSheet()->setCellValue('H'.$row_total, $tot_rupiah);
                      
                      $filename = 'km-penyaluran-sektor-'.$tahun.'.xls';
                  } else if($filter1 == '1' && $filter2 == '2'){
                      // penyaluran wilayah
                      
                      $this->load->library('excel');
                      $objPHPExcel = PHPExcel_IOFactory::load(APPPATH . 'template_excel/km-penyaluran-wilayah.xls');
                      
                      $data =$this->{$this->models[0]}->getDataLaporan($tahun, $tahun_sebelum, $filter1, $filter2);
                      
                      // set header
                      $objPHPExcel->getActiveSheet()->setCellValue('E5', 'Tahun '.$tahun_sebelum.' dan Tahun ' . $tahun);
                      $objPHPExcel->getActiveSheet()->setCellValue('C9', 's.d Tahun '. $tahun_sebelum);
                      $objPHPExcel->getActiveSheet()->setCellValue('E9', 'Tahun '. $tahun);
                      $objPHPExcel->getActiveSheet()->setCellValue('G9', 's.d Tahun '. $tahun);
                      
                      $baseRow = 13;
                      $tot_unit_tahun_sebelum = 0;
                      $tot_rupiah_tahun_sebelum = 0;
                      $tot_unit_tahun_sekarang = 0;
                      $tot_rupiah_tahun_sekarang = 0;
                      $tot_unit = 0;
                      $tot_rupiah = 0;
                      foreach ($data as $key => $value) {
                          $row = $baseRow + $key;
                          
                          $objPHPExcel->getActiveSheet()->insertNewRowBefore($row,1);
                          
                          $objPHPExcel->getActiveSheet()->setCellValue('B'.$row, $value['name']);
                          $objPHPExcel->getActiveSheet()->setCellValue('C'.$row, $value['unit_tahun_sebelum']);
                          $tot_unit_tahun_sebelum += $value['unit_tahun_sebelum'];
                          $objPHPExcel->getActiveSheet()->setCellValue('D'.$row, $value['rupiah_tahun_sebelum']);
                          $tot_rupiah_tahun_sebelum += $value['rupiah_tahun_sebelum'];
                          $objPHPExcel->getActiveSheet()->setCellValue('E'.$row, $value['unit_tahun_sekarang']);
                          $tot_unit_tahun_sekarang += $value['unit_tahun_sekarang'];
                          $objPHPExcel->getActiveSheet()->setCellValue('F'.$row, $value['rupiah_tahun_sekarang']);
                          $tot_rupiah_tahun_sekarang += $value['rupiah_tahun_sekarang'];
                          
                          $objPHPExcel->getActiveSheet()->setCellValue('G'.$row, $value['unit_tahun_sebelum']+$value['unit_tahun_sekarang']);
                          $tot_unit += $value['unit_tahun_sebelum']+$value['unit_tahun_sekarang'];
                          $objPHPExcel->getActiveSheet()->setCellValue('H'.$row, $value['rupiah_tahun_sebelum']+$value['rupiah_tahun_sekarang']);
                          $tot_rupiah += $value['rupiah_tahun_sebelum']+$value['rupiah_tahun_sekarang'];
                      }
                      $objPHPExcel->getActiveSheet()->removeRow($baseRow-1,1);
                      $row_total = $row + 1;
                      $objPHPExcel->getActiveSheet()->setCellValue('C'.$row_total, $tot_unit_tahun_sebelum);
                      $objPHPExcel->getActiveSheet()->setCellValue('D'.$row_total, $tot_rupiah_tahun_sebelum);
                      $objPHPExcel->getActiveSheet()->setCellValue('E'.$row_total, $tot_unit_tahun_sekarang);
                      $objPHPExcel->getActiveSheet()->setCellValue('F'.$row_total, $tot_rupiah_tahun_sekarang);
                      $objPHPExcel->getActiveSheet()->setCellValue('G'.$row_total, $tot_unit);
                      $objPHPExcel->getActiveSheet()->setCellValue('H'.$row_total, $tot_rupiah);
                      
                      $filename = 'km-penyaluran-wilayah-'.$tahun.'.xls';
                      
                  } else if($filter1 == '2' && $filter2 == '1'){
                      // piutang sektor
                      
                      $this->load->library('excel');
                      $objPHPExcel = PHPExcel_IOFactory::load(APPPATH . 'template_excel/km-piutang-angsuran-sektor.xls');
                      
                      $data =$this->{$this->models[0]}->getDataLaporan($tahun, $tahun_sebelum, $filter1, $filter2);
                      
                      // set header
                      $objPHPExcel->getActiveSheet()->setCellValue('E4', 'Posisi Piutang Program Kemitraan Tanggal 31 Desember  Tahun '.$tahun_sebelum.' dan Tahun '. $tahun);
                      $objPHPExcel->getActiveSheet()->setCellValue('C7', 'Tahun '. $tahun_sebelum);
                      $objPHPExcel->getActiveSheet()->setCellValue('E7', 'Tahun '. $tahun);
                      $objPHPExcel->getActiveSheet()->setCellValue('G7', '% '.$tahun.' / '. $tahun_sebelum);
                      
                      $baseRow = 12;
                      $tot_unit_tahun_sebelum = 0;
                      $tot_rupiah_tahun_sebelum = 0;
                      $tot_unit_tahun_sekarang = 0;
                      $tot_rupiah_tahun_sekarang = 0;
                      $tot_unit = 0;
                      $tot_rupiah = 0;
                      foreach ($data as $key => $value) {
                          $row = $baseRow + $key;
                          
                          $objPHPExcel->getActiveSheet()->insertNewRowBefore($row,1);
                          
                          $objPHPExcel->getActiveSheet()->setCellValue('B'.$row, $value['nama_sektor']);
                          $objPHPExcel->getActiveSheet()->setCellValue('C'.$row, $value['unit_tahun_sebelum']);
                          $tot_unit_tahun_sebelum += $value['unit_tahun_sebelum'];
                          $objPHPExcel->getActiveSheet()->setCellValue('D'.$row, $value['rupiah_tahun_sebelum']);
                          $tot_rupiah_tahun_sebelum += $value['rupiah_tahun_sebelum'];
                          $objPHPExcel->getActiveSheet()->setCellValue('E'.$row, $value['unit_tahun_sekarang']);
                          $tot_unit_tahun_sekarang += $value['unit_tahun_sekarang'];
                          $objPHPExcel->getActiveSheet()->setCellValue('F'.$row, $value['rupiah_tahun_sekarang']);
                          $tot_rupiah_tahun_sekarang += $value['rupiah_tahun_sekarang'];
                          
                          $objPHPExcel->getActiveSheet()->setCellValue('G'.$row, $value['unit_tahun_sebelum']+$value['unit_tahun_sekarang']);
                          $tot_unit += $value['unit_tahun_sebelum']+$value['unit_tahun_sekarang'];
                          $objPHPExcel->getActiveSheet()->setCellValue('H'.$row, $value['rupiah_tahun_sebelum']+$value['rupiah_tahun_sekarang']);
                          $tot_rupiah += $value['rupiah_tahun_sebelum']+$value['rupiah_tahun_sekarang'];
                      }
                      $objPHPExcel->getActiveSheet()->removeRow($baseRow-1,1);
                      $row_total = $row + 1;
                      $objPHPExcel->getActiveSheet()->setCellValue('C'.$row_total, $tot_unit_tahun_sebelum);
                      $objPHPExcel->getActiveSheet()->setCellValue('D'.$row_total, $tot_rupiah_tahun_sebelum);
                      $objPHPExcel->getActiveSheet()->setCellValue('E'.$row_total, $tot_unit_tahun_sekarang);
                      $objPHPExcel->getActiveSheet()->setCellValue('F'.$row_total, $tot_rupiah_tahun_sekarang);
                      $objPHPExcel->getActiveSheet()->setCellValue('G'.$row_total, $tot_unit);
                      $objPHPExcel->getActiveSheet()->setCellValue('H'.$row_total, $tot_rupiah);
                      
                      $filename = 'km-piutang-angsuran-sektor-'.$tahun.'.xls';
                  } else if($filter1 == '2' && $filter2 == '2'){
                      // piutang wilayah
                      
                      $this->load->library('excel');
                      $objPHPExcel = PHPExcel_IOFactory::load(APPPATH . 'template_excel/km-piutang-angsuran-wilayah.xls');
                      
                      $data =$this->{$this->models[0]}->getDataLaporan($tahun, $tahun_sebelum, $filter1, $filter2);
                      
                      // set header
                      $objPHPExcel->getActiveSheet()->setCellValue('E4', 'Posisi Piutang Program Kemitraan Tanggal 31 Desember  Tahun '.$tahun_sebelum.' dan Tahun '. $tahun);
                      $objPHPExcel->getActiveSheet()->setCellValue('C7', 'Tahun '. $tahun_sebelum);
                      $objPHPExcel->getActiveSheet()->setCellValue('E7', 'Tahun '. $tahun);
                      $objPHPExcel->getActiveSheet()->setCellValue('G7', '% '.$tahun.' / '. $tahun_sebelum);
                      
                      $baseRow = 12;
                      $tot_unit_tahun_sebelum = 0;
                      $tot_rupiah_tahun_sebelum = 0;
                      $tot_unit_tahun_sekarang = 0;
                      $tot_rupiah_tahun_sekarang = 0;
                      $tot_unit = 0;
                      $tot_rupiah = 0;
                      foreach ($data as $key => $value) {
                          $row = $baseRow + $key;
                          
                          $objPHPExcel->getActiveSheet()->insertNewRowBefore($row,1);
                          
                          $objPHPExcel->getActiveSheet()->setCellValue('B'.$row, $value['name']);
                          $objPHPExcel->getActiveSheet()->setCellValue('C'.$row, $value['unit_tahun_sebelum']);
                          $tot_unit_tahun_sebelum += $value['unit_tahun_sebelum'];
                          $objPHPExcel->getActiveSheet()->setCellValue('D'.$row, $value['rupiah_tahun_sebelum']);
                          $tot_rupiah_tahun_sebelum += $value['rupiah_tahun_sebelum'];
                          $objPHPExcel->getActiveSheet()->setCellValue('E'.$row, $value['unit_tahun_sekarang']);
                          $tot_unit_tahun_sekarang += $value['unit_tahun_sekarang'];
                          $objPHPExcel->getActiveSheet()->setCellValue('F'.$row, $value['rupiah_tahun_sekarang']);
                          $tot_rupiah_tahun_sekarang += $value['rupiah_tahun_sekarang'];
                          
                          $objPHPExcel->getActiveSheet()->setCellValue('G'.$row, $value['unit_tahun_sebelum']+$value['unit_tahun_sekarang']);
                          $tot_unit += $value['unit_tahun_sebelum']+$value['unit_tahun_sekarang'];
                          $objPHPExcel->getActiveSheet()->setCellValue('H'.$row, $value['rupiah_tahun_sebelum']+$value['rupiah_tahun_sekarang']);
                          $tot_rupiah += $value['rupiah_tahun_sebelum']+$value['rupiah_tahun_sekarang'];
                      }
                      $objPHPExcel->getActiveSheet()->removeRow($baseRow-1,1);
                      $row_total = $row + 1;
                      $objPHPExcel->getActiveSheet()->setCellValue('C'.$row_total, $tot_unit_tahun_sebelum);
                      $objPHPExcel->getActiveSheet()->setCellValue('D'.$row_total, $tot_rupiah_tahun_sebelum);
                      $objPHPExcel->getActiveSheet()->setCellValue('E'.$row_total, $tot_unit_tahun_sekarang);
                      $objPHPExcel->getActiveSheet()->setCellValue('F'.$row_total, $tot_rupiah_tahun_sekarang);
                      $objPHPExcel->getActiveSheet()->setCellValue('G'.$row_total, $tot_unit);
                      $objPHPExcel->getActiveSheet()->setCellValue('H'.$row_total, $tot_rupiah);
                      
                      $filename = 'km-piutang-angsuran-wilayah-'.$tahun.'.xls';
                  } else if($filter1 == '3' && $filter2 == '1'){
                      // rencana realisasi sektor
                      
                      $this->load->library('excel');
                      $objPHPExcel = PHPExcel_IOFactory::load(APPPATH . 'template_excel/km-rencanarealisasi-sektor.xls');
                      
                      $data =$this->{$this->models[0]}->getDataLaporan($tahun, $tahun_sebelum, $filter1, $filter2);
                      
                      // set header
                      $objPHPExcel->getActiveSheet()->setCellValue('E4', 'Posisi Piutang Program Kemitraan Tanggal 31 Desember  Tahun '.$tahun_sebelum.' dan Tahun '. $tahun);
                      $objPHPExcel->getActiveSheet()->setCellValue('C7', 'Tahun '. $tahun_sebelum);
                      $objPHPExcel->getActiveSheet()->setCellValue('E7', 'Tahun '. $tahun);
                      $objPHPExcel->getActiveSheet()->setCellValue('G7', '% '.$tahun.' / '. $tahun_sebelum);
                      
                      $baseRow = 12;
                      $tot_unit_rencana = 0;
                      $tot_rupiah_rencana = 0;
                      $tot_unit_realisasi = 0;
                      $tot_rupiah_realisasi = 0;
                      $tot_unit = 0;
                      $tot_rupiah = 0;
                      foreach ($data as $key => $value) {
                          $row = $baseRow + $key;
                          
                          $objPHPExcel->getActiveSheet()->insertNewRowBefore($row,1);
                          
                          $objPHPExcel->getActiveSheet()->setCellValue('B'.$row, $value['nama_sektor']);
                          $objPHPExcel->getActiveSheet()->setCellValue('C'.$row, $value['unit_rencana']);
                          $tot_unit_rencana += $value['unit_rencana'];
                          $objPHPExcel->getActiveSheet()->setCellValue('D'.$row, $value['rupiah_rencana']);
                          $tot_rupiah_rencana += $value['rupiah_rencana'];
                          $objPHPExcel->getActiveSheet()->setCellValue('E'.$row, $value['unit_realisasi']);
                          $tot_unit_realisasi += $value['unit_realisasi'];
                          $objPHPExcel->getActiveSheet()->setCellValue('F'.$row, $value['rupiah_realisasi']);
                          $tot_rupiah_realisasi += $value['rupiah_realisasi'];
                          
                          $objPHPExcel->getActiveSheet()->setCellValue('G'.$row, $value['unit_rencana']+$value['unit_realisasi']);
                          $tot_unit += $value['unit_rencana']+$value['unit_realisasi'];
                          $objPHPExcel->getActiveSheet()->setCellValue('H'.$row, $value['rupiah_rencana']+$value['rupiah_realisasi']);
                          $tot_rupiah += $value['rupiah_rencana']+$value['rupiah_realisasi'];
                      }
                      $objPHPExcel->getActiveSheet()->removeRow($baseRow-1,1);
                      $row_total = $row + 1;
                      $objPHPExcel->getActiveSheet()->setCellValue('C'.$row_total, $tot_unit_rencana);
                      $objPHPExcel->getActiveSheet()->setCellValue('D'.$row_total, $tot_rupiah_rencana);
                      $objPHPExcel->getActiveSheet()->setCellValue('E'.$row_total, $tot_unit_realisasi);
                      $objPHPExcel->getActiveSheet()->setCellValue('F'.$row_total, $tot_rupiah_realisasi);
                      $objPHPExcel->getActiveSheet()->setCellValue('G'.$row_total, $tot_unit);
                      $objPHPExcel->getActiveSheet()->setCellValue('H'.$row_total, $tot_rupiah);
                      
                      $filename = 'km-rencanarealisasi-sektor-'.$tahun.'.xls';
                  } else if($filter1 == '3' && $filter2 == '2'){
                      // rencana realisasi wilayah
                  } 
                  
                  if($filename){
                      //save our workbook as this file name
                      header('Content-Type: application/vnd.ms-excel');
                      //mime type
                      header('Content-Disposition: attachment;filename="' . $filename . '"');
                      //tell browser what's the file name
                      header('Cache-Control: max-age=0');
                      //no cache
                      
                      $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
                      $objWriter->save('php://output');
                  } else {
                      show_404();
                  }
              } else {
                  show_404();
              }
          }

	}
