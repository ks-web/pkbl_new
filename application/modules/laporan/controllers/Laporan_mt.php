<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');
	class Laporan_mt extends MY_Controller {
		public $models = array('laporanmt');
		
		public function __construct() {
			parent::__construct();
			$this->load->helper('text');
		}

		public function index() {
			$data = array();
			$data['menu'] = $this->model_menu->getAllMenu();

			$this->template->load('template', 'view_laporanmt', $data);
		}

		public function read() {

			$this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read());
		}

		function getData() {
		    $sektor_usaha     = $this->input->post('sektor_usaha') ? $this->input->post('sektor_usaha') : '';
            $kota        = $this->input->post('kota') ? $this->input->post('kota') : '';
            $keputusan   = $this->input->post('keputusan') ? $this->input->post('keputusan') : '';
            $tglawal     = $this->input->post('tglawal') ? $this->input->post('tglawal') : 0;
            $tglakhir    = $this->input->post('tglakhir') ? $this->input->post('tglakhir') : 0;
		    $order       = $this->input->post('order') ? $this->input->post('order') : 'asc';
            $sort        = $this->input->post('sort') ? $this->input->post('sort') : 'a.id_mitra';
            $page        = $this->input->post('page') ? $this->input->post('page') : 1;
            $rows        = $this->input->post('rows') ? $this->input->post('rows') : 10;
            $offset      = ($page - 1) * $rows;
			header("Content-Type: application/json");
			
			$this->{$this->models[0]}->getData($rows, $offset, $sort, $order,$sektor_usaha,$tglawal,$tglakhir,$keputusan,$kota);
		}
		
        function previewlaporanbl($propinsi = 'x',$kota = 'x',$nama_bl = 'x',$tgl_awal = 'x',$tgl_akhir = 'x'){    // load dompdf
        //load content html
		    $data['laporan_bl'] = $this->{$this->models[0]}->previewlaporanbl($propinsi,$kota,$nama_bl,$tgl_awal,$tgl_akhir)->result_array();
		    $data['periode'] = array('tgl_awal' => $tgl_awal, 'tgl_akhir' => $tgl_akhir);
		    $html = $this->load->view('preview_lap_bl.php', $data,true);
		    // echo $html;
		    // die();
		    // create pdf using dompdf
  			$this->load->library('dompdf_gen'); // Load library
			$this->dompdf->set_paper('A4', 'LANDSCAPE'); // Setting Paper
			// Convert to PDF
			$this->dompdf->load_html($html);
			$this->dompdf->render();
			$this->dompdf->stream("Laporan_Kemitraan.pdf");
        
        }
        function sektor_bl() {
			header("Content-Type: application/json");
			$cari = $this->input->post('q') ? $this->input->post('q') : '';
			$this->{$this->models[0]}->sektor_bl($cari);
		}
		public function getPropinsi()
		{
			$filter = array('A.id !=' => 0);
			$this->output->set_content_type('application/json')->set_output(json_encode($this->{$this->models[0]}->getPropinsi($filter)));
		}
		public function getKota($id)
		{
			$filter = array('A.province_id' => $id);
			$this->output->set_content_type('application/json')->set_output(json_encode($this->{$this->models[0]}->getKota($filter)));
		}

		public function getDataFiltter($propinsi = 'x',$kota = 'x',$nama_bl = 'x',$tgl_awal = 'x',$tgl_akhir = 'x'){
			if ( $propinsi !='x' ) {
				$this->db->where('propinsi ', str_replace("%20"," ",$propinsi) );
			}
			if ( $kota !='x' ) {
				$this->db->where('kota ', str_replace("%20"," ",$kota) );
			}
			if ( $nama_bl !='x' ) {
				$this->db->where('nama_bl ', str_replace("%20"," ",$nama_bl) );
			}
			if ( $tgl_awal !='x' ) {
				$this->db->where('tanggal_input >=', date('Y-m-d', strtotime($tgl_awal)) );
			}
			if ( $tgl_akhir !='x' ) {
				$this->db->where('tanggal_input <=', date('Y-m-d', strtotime($tgl_akhir)) );
			}
			// echo $propinsi.' - '.$kota.' - '.$nama_bl.' - '.$tgl_awal.' - '.$tgl_akhir;
			$data = $this->db->get('laporan_bl_vd')->result_array();

			$this->output->set_content_type('application/json')->set_output(json_encode($data));
		}
		public function read_preview_excel($filter1 = null, $filter2 = null) {
		  	//load our new PHPExcel library
			$this->load->library('excel');
			//activate worksheet number 1
			$this->excel->setActiveSheetIndex(0);
			//name the worksheet
			$this->excel->getActiveSheet()->setTitle('Laporan Kemitraan'); 
            //style
            $style = array(
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                )
            );
            $vertical_center = array(
	        'alignment' => array(   
	            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
	                ),
	         );
             $vertical_top_right = array(
	        'alignment' => array(   
	            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
	            'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
	                ),
	        );
            //border
            $border_all = array(
	           'borders' => array(
	                 // 'outline' => array(
	                 'allborders' => array(
	                        'style' => PHPExcel_Style_Border::BORDER_THIN,
	                        'color' => array('argb' => '000000'),
	                 ),
	           ),
	        );
            //set dimmension column
            $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(50);
            $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
            
            //header
            $thnberjalan=date('Y');
            $thnsebelum=date('Y')-1;
            $header1='PT KRAKATAU STEEL (Persero) Tbk.';
            $header2='Realisasi Akumulasi Penyaluran Program Kemitraan Sampai Dengan';
            $header3='Tahun '.$thnsebelum.' dan Tahun '.$thnberjalan;
            if(($filter1=='1'&&$filter2=='1')||($filter1=='1'&&$filter2=='2')){
                $header4='Berdasarkan Sektor Usaha Mitra Binaan';
            }
            if(($filter1=='2'&&$filter2=='1')||($filter1=='2'&&$filter2=='2')){
                $header4='Berdasarkan Provinsi';
            }
            
			//set cell A2 content with some text
            $this->excel->getActiveSheet()->setCellValue('B2', $header1);
            $this->excel->getActiveSheet()->setCellValue('B3', $header2);
            $this->excel->getActiveSheet()->setCellValue('B4', $header3);
            $this->excel->getActiveSheet()->setCellValue('B5', $header4);
            if(($filter1=='1'&&$filter2=='1')||($filter1=='2'&&$filter2=='1')){
                $this->excel->getActiveSheet()->mergeCells('B2:E2');
                $this->excel->getActiveSheet()->mergeCells('B3:E3');
                $this->excel->getActiveSheet()->mergeCells('B4:E4');
                $this->excel->getActiveSheet()->mergeCells('B5:E5');   
            }
            if(($filter1=='1'&&$filter2=='2')||($filter1=='2'&&$filter2=='2')){
                $this->excel->getActiveSheet()->mergeCells('B2:G2');
                $this->excel->getActiveSheet()->mergeCells('B3:G3');
                $this->excel->getActiveSheet()->mergeCells('B4:G4');
                $this->excel->getActiveSheet()->mergeCells('B5:G5');   
            }
            
            $this->excel->getActiveSheet()->getStyle('B2:G2')->applyFromArray($style);
            $this->excel->getActiveSheet()->getStyle('B3:G3')->applyFromArray($style);
            $this->excel->getActiveSheet()->getStyle('B4:G4')->applyFromArray($style);
            $this->excel->getActiveSheet()->getStyle('B5:G5')->applyFromArray($style);
            //bold font
            $this->excel->getActiveSheet()->getStyle('B2:G8')->getFont()->setBold(true);
            //header tabel
            if(($filter1=='1'&&$filter2=='1')||($filter1=='2'&&$filter2=='1')){
                if($filter1=='1'){
                    $b7='Jenis Bantuan';
                }
                if($filter1=='2'){
                    $b7='Propinsi';
                }
                
                $this->excel->getActiveSheet()->setCellValue('B7',$b7);
                $this->excel->getActiveSheet()->mergeCells('B7:B8');
                $this->excel->getActiveSheet()->setCellValue('C7', 'Penyaluran');
                $this->excel->getActiveSheet()->setCellValue('D7', 'Penyaluran');
                $this->excel->getActiveSheet()->setCellValue('E7', 'Penyaluran');
                $this->excel->getActiveSheet()->setCellValue('C8', 's.d Tahun '.$thnsebelum);
                $this->excel->getActiveSheet()->setCellValue('D8', 'Tahun '.$thnberjalan);
                $this->excel->getActiveSheet()->setCellValue('E8', 's.d. Tahun '.$thnberjalan);
                $this->excel->getActiveSheet()->setCellValue('B9', '1');
                $this->excel->getActiveSheet()->setCellValue('C9', '2');
                $this->excel->getActiveSheet()->setCellValue('D9', '3');
                $this->excel->getActiveSheet()->setCellValue('E9', '4=2+3');
                //style column
                $this->excel->getActiveSheet()->getStyle('B7:B8')->applyFromArray($vertical_center);
                $this->excel->getActiveSheet()->getStyle('B9')->applyFromArray($style);
                $this->excel->getActiveSheet()->getStyle('C7:E9')->applyFromArray($style);
               
            }
            if(($filter1=='1'&&$filter2=='2')||($filter1=='2'&&$filter2=='2')){
                if($filter1=='1'){
                    $b7='Jenis Bantuan';
                }
                if($filter1=='2'){
                    $b7='Propinsi';
                }
                $this->excel->getActiveSheet()->setCellValue('B7',$b7);
                $this->excel->getActiveSheet()->mergeCells('B7:B8');
                $this->excel->getActiveSheet()->setCellValue('C7', 'Tahun '.$thnsebelum);
                $this->excel->getActiveSheet()->setCellValue('D7', 'Tahun '.$thnberjalan);
                $this->excel->getActiveSheet()->mergeCells('D7:E7');
                $this->excel->getActiveSheet()->setCellValue('F7', '% Terhadap');
                $this->excel->getActiveSheet()->setCellValue('G7', '% Dari Tahun');
                $this->excel->getActiveSheet()->setCellValue('C8', 'Realisasi');
                $this->excel->getActiveSheet()->setCellValue('D8', 'Rencana');
                $this->excel->getActiveSheet()->setCellValue('E8', 'Realisasi');
                $this->excel->getActiveSheet()->setCellValue('F8', 'Rencana');
                $this->excel->getActiveSheet()->setCellValue('G8', 'Sebelumnya');
                $this->excel->getActiveSheet()->setCellValue('B9', '1');
                $this->excel->getActiveSheet()->setCellValue('C9', '2');
                $this->excel->getActiveSheet()->setCellValue('D9', '3');
                $this->excel->getActiveSheet()->setCellValue('E9', '4');
                $this->excel->getActiveSheet()->setCellValue('F9', '5=4/3');
                $this->excel->getActiveSheet()->setCellValue('G9', '6=4/2');
                //style column
                $this->excel->getActiveSheet()->getStyle('B7:B8')->applyFromArray($vertical_center);
                $this->excel->getActiveSheet()->getStyle('B9')->applyFromArray($style);
                $this->excel->getActiveSheet()->getStyle('C7:G9')->applyFromArray($style);
            }
            if(($filter1=='1'&&$filter2=='1')||($filter1=='2'&&$filter2=='1')){
                //get data
            $data = $this->{$this->models[0]}->get_data_for_excel($filter1);
            //Loop Result
			$totn = count($data);
			$maxrow = $totn + 1;
			$row = 10;
			$no = 1;
			foreach ($data as $n) {
			 	$this->excel->getActiveSheet()->setCellValue('B' . $row, $n['nama']);
                $this->excel->getActiveSheet()->setCellValue('C' . $row, $n['tahunsebelum']);
                $this->excel->getActiveSheet()->setCellValue('D' . $row, $n['tahunberjalan']);
                $this->excel->getActiveSheet()->setCellValue('E' . $row,$n['tahunsebelum']+$n['tahunberjalan']);
               	$this->excel->getActiveSheet()->getStyle('C'.$row)->applyFromArray($vertical_top_right);
                $this->excel->getActiveSheet()->getStyle('D'.$row)->applyFromArray($vertical_top_right);
                $this->excel->getActiveSheet()->getStyle('E'.$row)->applyFromArray($vertical_top_right);
                $this->excel->getActiveSheet()->getStyle('B7:E'.$row)->applyFromArray($border_all);
                $row++;
				$no++;
             }
             $rowx=$row-1;
             $this->excel->getActiveSheet()->setCellValue('B' . $row,'Jumlah');
             $this->excel->getActiveSheet()->setCellValue('C' . $row,'=SUM(C10:C'.$rowx.')');
             $this->excel->getActiveSheet()->setCellValue('D' . $row,'=SUM(D10:D'.$rowx.')');
             $this->excel->getActiveSheet()->setCellValue('E' . $row,'=SUM(E10:E'.$rowx.')');
             $this->excel->getActiveSheet()->getStyle('B' . $row)->applyFromArray($style);
             $this->excel->getActiveSheet()->getStyle('B'.$row.':E'.$row)->applyFromArray($border_all);
             //bold font
             $this->excel->getActiveSheet()->getStyle('B'.$row.':E'.$row)->getFont()->setBold(true);
            }
            if(($filter1=='1'&&$filter2=='2')||($filter1=='2'&&$filter2=='2')){
                //get data
                $data = $this->{$this->models[0]}->get_data_for_excel($filter1);    
                //Loop Result
    			$totn = count($data);
    			$maxrow = $totn + 1;
    			$row = 10;
    			$no = 1;
                foreach ($data as $n) {
                    $this->excel->getActiveSheet()->setCellValue('B' . $row, $n['nama']);  
                    $this->excel->getActiveSheet()->setCellValue('C' . $row, $n['realisasisebelum']);
                    $this->excel->getActiveSheet()->setCellValue('D' . $row, $n['rencanaberjalan']);
                    $this->excel->getActiveSheet()->setCellValue('E' . $row, $n['realisasiberjalan']);
                    $this->excel->getActiveSheet()->setCellValue('F' . $row,$n['realisasiberjalan']/$n['rencanaberjalan']);
                    $this->excel->getActiveSheet()->setCellValue('G' . $row,$this->division($n['realisasiberjalan'],$n['realisasisebelum']));
                    // $a/$b==0?'':$b;
                    //division
                    $this->excel->getActiveSheet()->getStyle('C'.$row)->applyFromArray($vertical_center);
                    $this->excel->getActiveSheet()->getStyle('D'.$row)->applyFromArray($vertical_center);
                    $this->excel->getActiveSheet()->getStyle('E'.$row)->applyFromArray($vertical_center);
                    $this->excel->getActiveSheet()->getStyle('F'.$row)->applyFromArray($vertical_center);
                    $this->excel->getActiveSheet()->getStyle('G'.$row)->applyFromArray($vertical_center);
                    $this->excel->getActiveSheet()->getStyle('B7:G'.$row)->applyFromArray($border_all);
                    $row++;
				    $no++;  
                    }
                    $rowx=$row-1;
             $this->excel->getActiveSheet()->setCellValue('B' . $row,'Jumlah');
             $this->excel->getActiveSheet()->setCellValue('C' . $row,'=SUM(C10:C'.$rowx.')');
             $this->excel->getActiveSheet()->setCellValue('D' . $row,'=SUM(D10:D'.$rowx.')');
             $this->excel->getActiveSheet()->setCellValue('E' . $row,'=SUM(E10:E'.$rowx.')');
             $this->excel->getActiveSheet()->setCellValue('F' . $row,'=SUM(F10:F'.$rowx.')');
             $this->excel->getActiveSheet()->setCellValue('G' . $row,'=SUM(G10:G'.$rowx.')');
             $this->excel->getActiveSheet()->getStyle('B' . $row.':G'.$row)->applyFromArray($style);
             $this->excel->getActiveSheet()->getStyle('B'.$row.':G'.$row)->applyFromArray($border_all);
             //bold font
             $this->excel->getActiveSheet()->getStyle('B'.$row.':G'.$row)->getFont()->setBold(true);
            }
            
			$filename = 'Laporan Mitra.xls';
			//save our workbook as this file name
			header('Content-Type: application/vnd.ms-excel');
			//mime type
			header('Content-Disposition: attachment;filename="' . $filename . '"');
			//tell browser what's the file name
			header('Cache-Control: max-age=0');
			//no cache

			//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007'
			// (and adjust the filename extension, also the header mime type)
			//if you want to save it as .XLSX Excel 2007 format
			$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
			//force user to download the Excel file without writing it to server's HD
			$objWriter->save('php://output');
          }
          function division($a, $b) { 
            if($b == 0){
                return 0; 
            }else{
            return $a/$b; 
        }
        }

}
