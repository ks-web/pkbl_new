<?php
$app 		= 'Realisasi Transaksi';
$appLink 	= 'realisasi_transaksi';

?>

<script>
var app = "<?=$app?>";
var appLink = '<?=$appLink?>';

function add(){
	$('#dlg').dialog('open').dialog('setTitle','Tambah ' + app);
	$('#fm').form('clear');
    $('#tbl').html('Save');
	url = '<?=base_url('laporan/' . $appLink . '/create')?>';
    $('#email').val('');
}
function edit(){
	var row = $('#dg').datagrid('getSelected');
	if (row){
	   $('#tbl').html('Simpan');
		$('#dlg').dialog('open').dialog('setTitle','Edit '+ app);
		$('#fm').form('load',row);
        url = '<?=base_url('laporan/' . $appLink . '/update')?>/'+row.id_realisasi;
	    }
}
    
function hapus(){
	var row = $('#dg').datagrid('getSelected');		
    if (row){
		$.messager.confirm('Confirm','Apakah Anda Yakin Akan Menghapus ini?',function(r){
			if (r){
				$.post('<?=base_url('laporan/' . $appLink . '/delete')?>/'+row.id_tpohon,function(result){
					if (result.success){
						$('#dg').datagrid('reload');	// reload the user data
					} else {
						$.messager.show({	// show error message
							title: 'Error',
							msg:result.msg
						});
					}
				},'json');
			}
		});
	}
}
    
function doSearch(value){
    $('#dg').datagrid('load',{    
		q:value  
    });   
}
   
function detail()
{
	var row = $('#dg').datagrid('getSelected');
	if(row)
	{
		$('#dlg-detail').dialog('open').dialog('setTitle','Edit '+ app);
		$('#fm-detail').form('load',row);
		url = '<?=base_url('laporan/' . $appLink . '/read_detail')?>'+'/'+row.id_realisasi+'/';
		$('#dg-detail').datagrid({url:url});
	}
	
}

function save_detail(){
    $('#fm-detail').form('submit',{
        url: '<?=base_url('laporan/' . $appLink . '/save_detail')?>',
        onSubmit: function(){
            return $(this).form('validate');
        },
        success: function(result){
            var result = eval('('+result+')');
            if (result.success){
                $('#dg-detail').datagrid('reload');
                
            } else {
             $('#dlg').dialog('close');
                $.messager.alert('Error Message',result.msg,'error');
            }
        }
    });
}

function view_file(value,row,index){

	var file = '';
    if(value != '') {
        file = '<center><a onclick="$(\'#frame\').dialog(\'open\');$(\'#frame iframe\').attr(\'src\',\'<?=base_url()?>'+row.file+'\')" class="btn btn-small btn-default"><i class="icon-file"></i></a></center>';
    }
	return file;
}
	
</script>
 
<script type="text/javascript">
var url;
function save(){
    $('#fm').form('submit',{
        url: url,
        onSubmit: function(){
            return $(this).form('validate');
        },
        success: function(result){
            var result = eval('('+result+')');
            if (result.success){
                $('#dlg').dialog('close');      
                $('#dg').datagrid('reload');
                
            } else {
             $('#dlg').dialog('close');
                $.messager.alert('Error Message',result.msg,'error');
            }
        }
    });
}

function action(val,row)
{
	return '<center><a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:detail()">Detail</a></center>';
}

</script>

<div class="container_s">                
<table id="dg" class="easyui-datagrid" 
    url= "<?=base_url('laporan/' . $appLink . '/read')?>"
    singleSelect="true" 
    iconCls="icon-cogs" 
    rownumbers="true"  
    idField="id_realisasi" 
    pagination="true"
    fitColumns="true"
    pageList= [10,20,30]
    toolbar="#toolbar"
    title="Realisasi Transaksi"
>
        <thead>
			<th field="id_realisasi" width="60" >No Realisasi</th>
            <th field="tanggal_create" width="100" >Tanggal</th>
            <th field="jenis_transaksi" width="100" >Transaksi</th>
			<th field="nama" width="100" >Nama</th>
			<th field="kota" width="100" >Kota/Kabupaten</th>
			<th field="kecamatan" width="100" >Kecamatan</th>
			<th field="kelurahan" width="100" >Kelurahan</th>
			<th field="action" width="100" formatter="action">Action</th>
        </thead>
 </table>
 
<!-- Model Start -->
<div id="dlg" class="easyui-dialog" style="width:400px; height:300px; padding:10px" closed="true" buttons="#t_dlg_dis-buttons" >
    <form id="fm" method="post" enctype="multipart/form-data" action="">
		<table width="100%" align="center" border="0">
		    <!--
			<tr >
				<td>No Transaksi</td>
				<td>:</td>
			    <td><input class="easyui-textbox"  name="id_tpohon" id="id_tpohon" style="width: 247px;" readonly="true"></td>
			</tr>
			-->
			<input class="easyui-textbox"  name="id_tpohon" id="id_tpohon" style="width: 247px;" readonly="true" hidden="true">
			<tr>
			    <td>Transaksi</td>
				<td>:</td>
			    <td >
			        <select class="easyui-combobox" name="jenis_transaksi" id="jenis_transaksi" label="sektor" style="width:250px" data-options="
						onChange:function(value){
							$('#nomor').combobox('reload','<?=base_url('laporan/realisasi_transaksi/get_tcd_all')?>/'+value);
							console.log(value);
							$('#nomor').combobox('setValue', '');
							$('#nama').val('');   
							$('#kota').val(''); 
							$('#kecamatan').val(''); 
							$('#kelurahan').val(''); 
                        }
					">
						<option value="1">Bina Lingkungan</option>
						<option value="2">Kemitraan</option>
						<option value="3">Penanaman Pohon</option>
					</select>
			    </td>
			</tr>
			<tr>
				<td>No Transaksi</td>
				<td>:</td>
			    <td>
					<input class="easyui-textbox"  name="id_realisasi" id="id_realisasi" style="width: 246px;" hidden readonly>
					<input class="easyui-combobox" name="nomor" id="nomor" required style="width: 250px;" data-options="
				        url:'<?=base_url('laporan/realisasi_transaksi/get_tcd_all')?>',
				        method:'get',
				        valueField:'no',
				        textField:'no',
				        panelHeight:'auto',
				        labelPosition: 'top',
						onSelect:function(row){
                            $('#nama').val(row.nama);   
							$('#kota').val(row.kota); 
							$('#kecamatan').val(row.kecamatan); 
							$('#kelurahan').val(row.kelurahan); 
                        }
				    ">
				</td>
			</tr>
			<tr>
				<td>Nama</td>
				<td>: </td>
			    <td><input class="easyui-textbox"  name="nama" id="nama" style="width: 246px;" readonly></td>
			</tr>	
			<tr>
			    <td>Kota/Kabupaten</td>
				<td>: </td>
			    <td>
			        <input class="easyui-textbox" name="kota" id="kota" required style="width: 246px;" readonly>
			     </td>
			</tr>
				<td>Kecamatan</td>
				<td>: </td>
			    <td>
			        <input class="easyui-textbox" name="kecamatan" id="kecamatan" required style="width: 246px;" readonly>
			     </td>
			</tr>
			</tr>
				<td>Kelurahan</td>
				<td>: </td>
			    <td>
			        <input class="easyui-textbox" name="kelurahan" id="kelurahan" required style="width: 246px;" readonly>
			     </td>
			</tr>
			
		</table>
    </form>
    <div id="toolbar">
        <table align="center" style="padding: 0px; width: 99%;">
            <tr>
                <td>
                        <a href="javascript:void(0)" onclick="javascript:add()" class="btn btn-small btn-success"><i class="icon-plus-sign icon-large"></i>&nbsp;Add</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:edit()"><i class="icon-edit"></i>&nbsp;Edit</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-danger" onclick="javascript:hapus()"><i class="icon-trash icon-large"></i>&nbsp;Delete</a>
						
				</td>
                <td>&nbsp;</td>
                <td align="right">
                    	<input class="easyui-searchbox" prompt="Silahkan Input Field" searcher="doSearch"  style="width:300px"/>
                </td>
            </tr>
         </table>
    </div>
		
	<div id="t_dlg_dis-buttons">
        <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:save()"><i class="icon-save icon-large"></i>&nbsp;Simpan</a>
        <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:$('#dlg').dialog('close')"><i class="icon-remove icon-large"></i>&nbsp;Batal</a>
    </div>
</div>

<!-- Model Start -->
<div id="dlg-detail" class="easyui-dialog" style="width:470px; height:500px; padding:10px" closed="true" buttons="#t_dlg_dis-buttons" >
    <form id="fm-detail" method="post" enctype="multipart/form-data" action="">
		<table width="100%" align="center" border="0">
		    <tr>
				<td>Nomor Realisasi</td>
				<td>: </td>
			    <td>
			        <input class="easyui-textbox" name="id_realisasi" id="no_realisasi" required style="width: 50px;">
			     </td>
			</tr>
			<tr>
				<td>Deskripsi sebelum menerima bantuan</td>
				<td>: </td>
			    <td>
			        <textarea name="deskripsi_sebelum" style="height:60px;"></textarea>
			    </td>
			</tr>
			<tr>
				<td>Deskripsi setelah menerima bantuan</td>
				<td>: </td>
			    <td>
			        <textarea name="deskripsi_sesudah" style="height:60px;"></textarea>
			    </td>
			</tr>
			<tr>
				<td>File</td>
				<td>: </td>
			    <td>
			        <input type="file" id="file" name="file" size="20"  />
			    </td>
			</tr>
			<tr >
				<td></td>
				<td></td>
			    <td ><a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:save_detail()"><i class="icon-save icon-large"></i>&nbsp;Simpan</a></td>
			</tr>
			
		</table>
		<table id="dg-detail" class="easyui-datagrid" 
			
			singleSelect="true" 
			iconCls="icon-cogs" 
			rownumbers="true"  
			idField="id" 
			pagination="true"
			fitColumns="true"
			pageList= [10,20,30]
			title="Detail Realisasi Transaksi"
			
			data-options="
			onDblClickRow:function(index, row){
				$('#deskripsi_sesudah').val('asdf');   
			}
			"
		>
        <thead>
            <th field="tanggal_create" width="75" >Tanggal</th>
            <th field="deskripsi_sebelum" width="150" >Deskripsi Sebelum</th>
			<th field="deskripsi_sesudah" width="150" >Deskripsi Sesudah</th>
			<th field="file" width="70" formatter="view_file">File</th>
        </thead>
		</table>
    </form>
    
	<div id="t_dlg_dis-buttons">
        <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:$('#dlg-detail').dialog('close')"><i class="icon-remove icon-large"></i>&nbsp;Batal</a>
    </div>
</div> 

<div class="easyui-dialog" id="frame" style="width: 500px; height: 400px;" closed="true">
     <iframe src="" style="width: 100%;height: 100%">
         
     </iframe>
</div>
	
</div>