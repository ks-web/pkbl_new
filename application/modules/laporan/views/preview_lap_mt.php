<!DOCTYPE html>
<html>
<head>
	<title>Kontrak Kerjasama Bina Lingkungan<br>PT Krakatau Steel Tbk</title>
	<script type="text/javascript" src="<?php echo base_url('assets/lib/jquery.min.js');?>"></script>
	<style>
    
	u {    
    border-bottom: 1px dotted #000;
    text-decoration: none;
}
		div, .hr{

		    background-color: white;
		    /*width: 100%;*/
		    border: 0px solid black;
		    padding: 5px;
		    margin: 1px;
		    font-size: 10;
			padding-bottom:2px;
			padding-top: 0px;

		}
		hr{
		    background-color: white;
		    /*width: 100%;*/
		    border: 0px solid black;
		    padding: 0px;
		    margin: 0px;
		}
		h3, .title{
			text-align: center;
		}	
		table{
		    /*width: 100%;*/
		    border: 0px solid black;
		    border-collapse: collapse;
		    padding: 5px;
		    /*padding-bottom: 5px;*/
		}
		/*table,td,tr{
		    border: 0px solid black;
		    border-collapse: collapse;
		    padding: 1px;

		}*/	
					
	</style>
</head>
<body>
<?php
	$date_a = date_create($periode['tgl_awal']);
	$date_b = date_create($periode['tgl_akhir']);
?>
<div style="padding-top: 5px;padding-bottom: 0px;">
	<center>
		<h2>KETERANGAN REALISASI PENGELUARAN PROGRAM BINA LINGKUNGAN<br/>
		PERIODE <?php echo date_format($date_a,"d - M - Y");?> S/D <?php echo date_format($date_b,"d - M - Y");?></h2><br/><br/>
	</center>
	<table style=" padding-top: 0px;" width="100%" height:"40%" border="1">
		<tr>
			<th>No.</th>
			<th>ID BL</th>
			<th>Nama Pemohon</th>
			<th>Bulan</th>
			<th>Alamat</th>
			<th>Bidang Bantuan</th>
			<th>Nilai</th>
			<th>Keterangan</th>
		</tr>
<?php
$no = 1;
$total ='';

foreach($laporan_bl as $rows){
    echo"<tr>";
    echo"	<td align='center'>$no</td>
    		<td>$rows[id_bl]</td>
    		<td>$rows[nama]</td>
    		<td>$rows[tanggal_input]</td>
    		<td>$rows[kota] - $rows[propinsi]</td>
    		<td>$rows[nama_bl]</td>
    		<td align='right'> ".number_format($rows['nilai_disetujui'],2, ",", ".")." </td>
    		<td>$rows[kegiatan]</td>";
    echo"</tr>";
    
    $no++;
    $total +=$rows['nilai_disetujui'];  
}

echo "	<tr>
    		<td colspan='6' align='right'> Total : </td>
    		<td align='right'> ". number_format($total,2, ",", ".")." </td>
    		<td align='right'></td>
    	</tr>";
?>
</table>
</div>
	<script type="text/javascript">
		$(document).ready(function(){
		})
	</script>
</body>
</html>
