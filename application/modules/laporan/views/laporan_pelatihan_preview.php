<!DOCTYPE html>
<html>
<head>
	<title>Report Pengukuran Nilai Jaminan</title>
	<style type="text/css">
		.judul {
			font-weight: bold;
			color: #000000;
			letter-spacing: 0pt;
			word-spacing: 2pt;
			font-size: 18px;
			text-align: center;
			/*font-family: tahome, sans-serif;*/
			line-height: 1;
		}
		.teks_atas {
			font-size: 11px;
		}
		.teks_nomor_kontrak {
			text-align: center;
			font-weight: bold;
			font-size: 15px;
		}

		.tg {
			border-collapse: collapse;
			border-spacing: 0;
		}
		.tg td {
			font-family: Arial, sans-serif;
			font-size: 10px;
			padding: 10px 5px;
			border-style: solid;
			border-width: 1px;
			overflow: hidden;
			word-break: normal;
		}
		.tg th {
			font-family: Arial, sans-serif;
			font-size: 10px;
			font-weight: normal;
			padding: 10px 5px;
			border-style: solid;
			border-width: 1px;
			overflow: hidden;
			word-break: normal;
		}
		.tg .tg-qqdn {
			font-weight: bold;
			font-size: 10px;
			text-align: right;
			vertical-align: top
		}
		.tg .tg-d55q {
			font-weight: bold;
			font-size: 10px;
			vertical-align: middle
		}
		.tg .tg-ecrz {
			font-size: 10px;
			vertical-align: top
		}
		.tg .tg-07dj {
			font-weight: bold;
			font-size: 10px
		}
	</style>
</head>
<body>
	<div>
		<table class="tg" width="100%">
			<tr>
				<th class="tg-07dj" width="10px">No</th>
				<th class="tg-d55q" width="70px">ID Mitra</th>
				<th class="tg-d55q" width="140px">Nama</th>
				<th class="tg-d55q" width="140px">Perusahaan</th>
				<th class="tg-d55q">Alamat</th>
				<th class="tg-d55q" width="70px">Kota/Kab</th>
				<th class="tg-d55q" width="80px">Telpon</th>
				<th class="tg-d55q" width="80px">Telpon Perusahaan</th>
				<th class="tg-d55q" width="85px">Asset</th>
				<th class="tg-d55q" width="85px">Omset</th>
				<th class="tg-d55q" width="85px">Nilai Pengajuan</th>
				<th class="tg-d55q" width="80px">Surveyor</th>
			</tr>
			<?php
				$no = 1;
				// $data = null;
				
				if($data) {
					foreach ($data as $row) {
						?>
			<tr>
				<th class="tg-ecrz" width=""><?=$no?></th>
				<th class="tg-ecrz" width=""><?=$row['id_mitra']?></th>
				<th class="tg-ecrz" width="" align="left"><?=$row['nama']?></th>
				<th class="tg-ecrz" width="" align="left"><?=$row['nama_perusahaan']?></th>
				<th class="tg-ecrz" align="left"><?=$row['alamat']?></th>
				<th class="tg-ecrz" width=""><?=$row['kota']?></th>
				<th class="tg-ecrz" width=""><?=$row['telepon']?></th>
				<th class="tg-ecrz" width=""><?=$row['telepon_perusahaan']?></th>
				<th class="tg-ecrz" width="" align="right"><?=number_format($row['asset'],0,',','.') ?></th>
				<th class="tg-ecrz" width="" align="right"><?=number_format($row['omset'],0,',','.') ?></th>
				<th class="tg-ecrz" width="" align="right"><?=number_format($row['nilai_usulan'],0,',','.') ?></th>
				<th class="tg-ecrz" width=""><?=$row['nama_pegawai_survey']?></th>
			</tr>
						<?php
						$no++;
					}
				} else {
					?>
			<tr>
				<th class="tg-ecrz" width=""><br /></th>
				<th class="tg-ecrz" width=""><br /></th>
				<th class="tg-ecrz" width=""><br /></th>
				<th class="tg-ecrz" width=""><br /></th>
				<th class="tg-ecrz"><br /></th>
				<th class="tg-ecrz" width=""><br /></th>
				<th class="tg-ecrz" width=""><br /></th>
				<th class="tg-ecrz" width=""><br /></th>
				<th class="tg-ecrz" width=""></th>
				<th class="tg-ecrz" width=""></th>
				<th class="tg-ecrz" width=""></th>
				<th class="tg-ecrz" width=""><br /></th>
			</tr>
					<?php
				}
			?>
		</table>
	</div>
</body>
</html>