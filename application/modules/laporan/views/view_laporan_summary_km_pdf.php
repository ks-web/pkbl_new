<table width="100%">
	<tr align="center">
		<td>Laporan Summary Kemitraan</td>
	</tr>
	<tr align="center">
		<td><?php echo indoDate($this->input->post('dari'), 3); ?> - <?php echo indoDate($this->input->post('sampai'), 3); ?></td>
	</tr>
	<tr align="center">
		<td>&nbsp;</td>
	</tr>
</table>

<table align="center" width="90%" border="1">
	<tr>
		<th>No</th>
		<th>ID BL</th>
		<th>Nama</th>
		<th>Instansi</th>
		<th>Provinsi</th>
		<th>Kab/Kota</th>
		<th>Kecamatan</th>
		<th>Kelurahan</th>
		<th>Bidang Bantuan</th>
		<th align="right">Nilai (Rp)</th>
	</tr>
	<?php $i = 1; foreach ($summary as $key) { ?>
	<tr>
		<td><?php echo $i; ?></td>
		<td><?php echo $key['id_mitra']; ?></td>
		<td><?php echo $key['nama']; ?></td>
		<td><?php echo $key['instansi']; ?></td>
		<td><?php echo $key['propinsi']; ?></td>
		<td><?php echo $key['kota']; ?></td>
		<td><?php echo $key['kecamatan']; ?></td>
		<td><?php echo $key['kelurahan']; ?></td>
		<td><?php echo $key['nama_bidangbantuan']; ?></td>
		<td align="right"><?php echo number_format($key['nilai_disetujui']); ?></td>
	</tr>
	<?php $i++; } ?>
</table>

<?php
function indoDate($date, $format)
{
	$month = array(1 => 'Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agu', 'Sep', 'Okt', 'Nov', 'Des');
	$monthFull = array(1 => 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');

	$date = explode('-', $date);

	$result = '';
	if($format == 1){
		//result : 20/12/2017
		$result = $date[2].'/'.(int)$date[1].'/'.$date[0];
	} else if($format == 2){
		//result : 20 Des 2017
		$result = $date[2].' '.$month[(int)$date[1]].' '.$date[0];
	} else if($format == 3){
		//result : 20 Desember 2017
		$result = $date[2].' '.$monthFull[(int)$date[1]].' '.$date[0];
	} else if($format == 4){
		$result = $date[2];
	} else if($format == 5){
		$result = $month[(int)$date[1]];
	} else if($format == 6){
		$result = $date[0];
	}

	return $result;
}
?>