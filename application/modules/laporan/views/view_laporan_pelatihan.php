<?php
$app = 'Laporan Pelatihan'; // nama aplikasi
$module = 'laporan';
$appLink = 'laporan_pelatihan'; // controller
$idField  = 'id_mitra'; //field key table
?>

<script>
	var url;
	var app = "<?=$app?>";
	var appLink = '<?=$appLink?>';
	var module = '<?=$module?>';
	var idField = '<?=$idField?>';
	
    // function add(){
        // $('#dlg').dialog('open').dialog('setTitle','Tambah ' + app);
		// $('#fm').form('clear');
	    // $('#tbl').html('Save');
		// url = '<?=base_url($module . '/' . $appLink . '/create')?>';
    // }
    // function edit(){
        // var row = $('#dg').datagrid('getSelected');
		// if (row){
			// $('#tbl').html('Simpan');
			// $('#dlg').dialog('open').dialog('setTitle','Edit '+ app);
			// $('#fm').form('load',row);
			// url = '<?=base_url($module . '/' . $appLink . '/update')?>/'+row[idField];
	    // }
    // }
    // function hapus(){
        // var row = $('#dg').datagrid('getSelected');		
	    // if (row){
			// $.messager.confirm('Confirm','Apakah Anda Yakin Akan Menghapus ini?',function(r){
				// if (r){
					// $.post('<?=base_url($module . '/' . $appLink . '/delete')?>/'+row[idField],function(result){
						// if (result.success){
							// $('#dg').datagrid('reload');	// reload the user data
						// } else {
							// $.messager.show({	// show error message
								// title: 'Error',
								// msg:result.msg
							// });
						// }
					// },'json');
				// }
			// });
		// }
    // }
    // function save(){
	   // $('#fm').form('submit',{
	    	// url: url,
	    	// onSubmit: function(){
	    		// return $(this).form('validate');
	    	// },
	    	// success: function(result){
	    		// var result = eval('('+result+')');
	    		// if (result.success){
	    			// $('#dlg').dialog('close');		
	    			// $('#dg').datagrid('reload');
// 	                
	    		// } else {
	             // $.messager.alert('Error Message',result.msg,'error');
	    		// }
	    	// }
	    // });
	// }
	
	function cetak(){
		var surveyor = $('#surveyor').combobox('getValue');
		
		if(surveyor){
			var url = '<?=base_url($module . '/' . $appLink . '/read_preview')?>/'+surveyor;
			window.open(url, '_blank');
		} else {
			$.messager.alert('Error Message','Tidak ada surveyor yang dipilih','error');
		}
    }
    
	function doSearch(value){
	    $('#dg').datagrid('load',{    
	    	q:value  
	    });
	}
	
	function formatItem(row){
        var s = '' + row.petugas_survey + ' - ' +
                '' + row.nama_pegawai_survey + '';
        return s;
    }
</script>
 
<div class="tabs-container">                
	<table id="dg" class="easyui-datagrid" 
	data-options="  
	    url: '<?=base_url($module . '/' . $appLink . '/read')?>',
		singleSelect:'true', 
	    title:'<?=$app?>',
	    toolbar:'#toolbar',
	    iconCls:'icon-cog',
	    rownumbers:'true',  
	    idField:'<?=$idField?>', 
	    pagination:'true',
	    fitColumns:'true',
	    pageList: [10,20,30]
	    "
	>
	    <thead>
	        <th field="id_mitra" width="200" sortable="true">ID Mitra</th>
	        <th field="nama" width="200" sortable="true">Nama Mitra</th>
	        <th field="nama_perusahaan" width="200" sortable="true">Nama Pengusaha</th>
            <th field="kota" width="200" sortable="true">Kota</th>
            <th field="telepon" width="200" sortable="true">Telepon</th>
            <th field="telepon_perusahaan" width="200" sortable="true">Telepon Perusahaan</th>
            <th field="nama_pegawai_survey" width="200" sortable="true">Surveyor</th>
	    </thead>
	</table>
	
	<!-- Model Start -->
	<div id="dlg" class="easyui-dialog" style="width:400px; height:200px; padding:10px" closed="true" buttons="#t_dlg_dis-buttons" >
	    <form id="fm" method="post" enctype="multipart/form-data" action="">
	        
	    </form>
	   
	   	<!-- Tombol Add, Edit, Hapus Datagrid -->
       	<div id="toolbar">  
            <table align="center" style="padding: 0px; width: 99%;">
                <tr>
                    <td>
                    	<input width="200px" class="easyui-combobox" name="surveyor" id="surveyor" data-options="
				            url:'<?=base_url($module . '/' . $appLink . '/get_surveyor')?>',
				            method:'get',
				            valueField:'petugas_survey',
				            textField:'nama_pegawai_survey',
                    		formatter: formatItem,
				            panelHeight:'auto'">
                        <!-- <a href="javascript:void(0)" class="btn btn-small btn-success" onclick="javascript:add()"><i class="icon-plus-sign icon-large"></i>&nbsp;Add</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:edit()"><i class="icon-edit"></i>&nbsp;Edit</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-danger" onclick="javascript:hapus()"><i class="icon-trash icon-large"></i>&nbsp;Delete</a> -->
                        <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:cetak()"><i class="icon-trash icon-large"></i>&nbsp;Cetak</a>
                    </td>
                    <td>&nbsp;</td>
                    <td align="right">
                    	<input class="easyui-searchbox" prompt="Silahkan Input Field" searcher="doSearch"  style="width:300px"/>
                    </td>
                </tr>
            </table>  
        </div>  
        <!-- end tombol datagrid -->
        <!-- Tombol simpan & Batal Form -->
        <div id="t_dlg_dis-buttons">
            <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:save()"><i class="icon-save icon-large"></i>&nbsp;Simpan</a>
            <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:$('#dlg').dialog('close')"><i class="icon-remove icon-large"></i>&nbsp;Batal</a>      
        </div>
	</div>
	<!-- Model end -->
</div>