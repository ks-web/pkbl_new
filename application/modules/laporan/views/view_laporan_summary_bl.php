<?php
$app 	= 'Laporan Summary BL'; // nama aplikasi
$module = 'laporan';
$appLink = 'Laporan_summary_bl'; // controller
$idField  = ''; //field key table
?>

<script>
	var url;
	var app = "<?=$app?>";
	var appLink = '<?=$appLink?>';
	var module = '<?=$module?>';
	var idField = '<?=$idField?>';
	

	function doDownloadPdf(){
		var dari = $('#tgl_awal').datebox('getValue');
		var sampai = $('#tgl_akhir').datebox('getValue');
		// var akun = $('#filter-akun').combogrid('getValue');
		
		if(dari && sampai){
			window.open('<?=base_url($module . '/' . $appLink . '/read_preview_excel')?>/' + dari + '/' + sampai, '_blank');
		} else {
			$.messager.alert('Tanggal Belum Dipilih','Tolong pilih tanggal!','error');
		}
	}

	function cari(){
		var dari = $('#tgl_awal').datebox('getValue');
		var sampai = $('#tgl_akhir').datebox('getValue');
		var jenisbl = $('#jenisbl').combobox('getValue');
		var provinces = $('#provinces').combobox('getValue');
		var regencies = $('#regencies').combobox('getValue');
		var districts = $('#districts').combobox('getValue');
		var villages = $('#villages').combobox('getValue');

		var url = '<?=base_url($module . '/' . $appLink . '/read/')?>/';
		var data;

		if(dari && sampai){

		    if(villages != ''){
		    	data = {'dari': dari, 'sampai': sampai, 'jenisbl': jenisbl, 'provinces': provinces, 'regencies': regencies, 'districts': districts, 'villages': villages };
		    	//url = '<?=base_url($module . '/' . $appLink . '/read/')?>/'+dari+'/'+sampai+'/'+jenisbl+'/'+provinces+'/'+regencies+'/'+districts+'/'+villages;
		    } else if(districts != ''){
		    	data = {'dari': dari, 'sampai': sampai, 'jenisbl': jenisbl, 'provinces': provinces, 'regencies': regencies, 'districts': districts };
		    } else if(regencies != ''){
		    	data = {'dari': dari, 'sampai': sampai, 'jenisbl': jenisbl, 'provinces': provinces, 'regencies': regencies };
		    } else if(provinces != ''){
		    	data = {'dari': dari, 'sampai': sampai, 'jenisbl': jenisbl, 'provinces': provinces, };
		    } else if(jenisbl != '') {
		    	data = {'dari': dari, 'sampai': sampai, 'jenisbl': jenisbl };
		    } else {
		    	data = {'dari': dari, 'sampai': sampai };
		    }

		    $('#dg').datagrid({    
		    	url: url,
		    	queryParams : data  
		    });
		} else {
			$.messager.alert('Tanggal Belum Dipilih','Tolong pilih tanggal!','error');
		}
	}
	var newpage;

	function exportPdf(){
		var dari = $('#tgl_awal').datebox('getValue');
		var sampai = $('#tgl_akhir').datebox('getValue');
		var jenisbl = $('#jenisbl').combobox('getValue');
		var provinces = $('#provinces').combobox('getValue');
		var regencies = $('#regencies').combobox('getValue');
		var districts = $('#districts').combobox('getValue');
		var villages = $('#villages').combobox('getValue');

		var url = '<?=base_url($module . '/' . $appLink . '/exportPdf/')?>/';
		var data;

		if(dari && sampai){

		    /*if(villages != ''){
		    	data = {'dari': dari, 'sampai': sampai, 'jenisbl': jenisbl, 'provinces': provinces, 'regencies': regencies, 'districts': districts, 'villages': villages };
		    	//url = '<?=base_url($module . '/' . $appLink . '/read/')?>/'+dari+'/'+sampai+'/'+jenisbl+'/'+provinces+'/'+regencies+'/'+districts+'/'+villages;
		    } else if(districts != ''){
		    	data = {'dari': dari, 'sampai': sampai, 'jenisbl': jenisbl, 'provinces': provinces, 'regencies': regencies, 'districts': districts };
		    } else if(regencies != ''){
		    	data = {'dari': dari, 'sampai': sampai, 'jenisbl': jenisbl, 'provinces': provinces, 'regencies': regencies };
		    } else if(provinces != ''){
		    	data = {'dari': dari, 'sampai': sampai, 'jenisbl': jenisbl, 'provinces': provinces, };
		    } else if(jenisbl != '') {
		    	data = {'dari': dari, 'sampai': sampai, 'jenisbl': jenisbl };
		    } else {
		    	data = {'dari': dari, 'sampai': sampai };
		    }

		    var form = '';
		    $.each( data, function( key, value ) {
            	form += '<input type="hidden" name="'+key+'" value="'+value+'">';
	        });
	        $('#pdf').append(form);
	        $('#form').submit();*/
	        //alert(form);
	        //$('<form action="'+url+'" method="POST">'+form+'</form>').submit();

		    //dataJ = JSON.stringify(data);


		    /*$.post( url, data)
			  	.done(function( data ) {
					//alert( "Data Loaded: " + data );
					newpage = data;
					window.open('<?=base_url($module . '/' . $appLink . '/pdfCreator/')?>/', 'popUpWindow','height=400, width=650, left=300, top=100, resizable=yes, scrollbars=yes, toolbar=yes, menubar=no, location=no, directories=no, status=yes');
			});*/
			$('#fmsearch').submit();
		} else {
			$.messager.alert('Tanggal Belum Dipilih','Tolong pilih tanggal!','error');
		}
	}
	
</script>
 
<div class="tabs-container">                
	<table id="dg" class="easyui-datagrid" 
	data-options="  
	    url: '<?=base_url($module . '/' . $appLink . '/read')?>',
		singleSelect:'true', 
	    title:'<?=$app?>',
	    toolbar:'#toolbar',
	    iconCls:'icon-cog',
	    rownumbers:'true',  
	    idField:'<?=$idField?>', 
	    pagination:'true',
	    fitColumns:'true',
	    pageList: [10,20,30]
	    "
	>
	    <thead>
	        <th field="id_bl" width="100" sortable="true">ID BL</th>
	        <th field="nama" width="200" sortable="true">Nama</th>
            <th field="instansi" width="200" sortable="true">Instansi</th>
            <th field="propinsi" width="200" sortable="true">Provinsi</th>
            <th field="kota" width="200" sortable="true">Kab/Kota</th>
            <th field="kecamatan" width="200" sortable="true">Kecamatan</th>
            <th field="kelurahan" width="200" sortable="true">Kelurahan</th>
            <th field="nama_bidangbantuan" width="200" sortable="true">Bidang Bantuan</th>
            <th field="nilai_disetujui" width="100" sortable="true" formatter="format_numberdisp">Nilai</th>
	    </thead>
	</table>
	
    <!-- Tombol Add, Edit, Hapus Datagrid -->
<div id="toolbar">  
	<form id="fmsearch" method="post" action="<?=base_url($module . '/' . $appLink . '/exportPdf/')?>" target="_blank">
	    <table  align="center" style="padding: 0px; width: 99%;">
			
	        <tr>
				<td>
				<input type="hidden" name="sub" value="1">
				Dari tanggal : 
				<input class="easyui-datebox" name="dari" id="tgl_awal" style="width:150px;"> 
				Sampai tanggal : 
				<input class="easyui-datebox" name="sampai" id="tgl_akhir" style="width:150px;"><br>
				Jenis Bina Lingkungan : 
				<input name="jenisbl" id="jenisbl"  class="easyui-combobox" required style="width: 250px"
					data-options="
					valueField:'Node_group',
					textField:'nama_bidangbantuan',
					url:'<?=base_url($module . '/' . $appLink . '/getJenisBL') ?>',
					onSelect :function(row)
					{
						
					}" ><br>
				Provinsi : 
				<input name="provinces" id="provinces"  class="easyui-combobox" required style="width: 150px"
					data-options="
					valueField:'name',
					textField:'name',
					mode:'remote',
					url:'<?=base_url($module . '/' . $appLink . '/getProvinces/') ?>',
					onSelect :function(row)
					{
						$('#regencies').combobox('clear');
						$('#districts').combobox('clear');
						$('#villages').combobox('clear');
						$('#regencies').combobox('reload','<?=base_url($module . '/' . $appLink . '/getRegencies') ?>/'+row.id);
						$('#districts').combobox('reload','<?=base_url($module . '/' . $appLink . '/getDistricts') ?>/');
						$('#villages').combobox('reload','<?=base_url($module . '/' . $appLink . '/getVillages') ?>/');
					}" >
				Kab/Kota : 
				<input name="regencies" id="regencies"  class="easyui-combobox" required style="width: 150px"
					data-options="
					valueField:'name',
					textField:'name',
					mode:'remote',
					url:'<?=base_url($module . '/' . $appLink . '/getRegencies/') ?>',
					onSelect :function(row)
					{
						$('#districts').combobox('clear');
						$('#villages').combobox('clear');
						$('#districts').combobox('reload','<?=base_url($module . '/' . $appLink . '/getDistricts') ?>/'+row.id);
						$('#villages').combobox('reload','<?=base_url($module . '/' . $appLink . '/getVillages') ?>/');
					}" >

				Kecamatan : 
				<input name="districts" id="districts"  class="easyui-combobox" required style="width: 150px"
					data-options="
					valueField:'name',
					textField:'name',
					mode:'remote',
					url:'<?=base_url($module . '/' . $appLink . '/getRegencies/') ?>',
					onSelect :function(row)
					{
						$('#villages').combobox('clear');
						$('#villages').combobox('reload','<?=base_url($module . '/' . $appLink . '/getVillages') ?>/'+row.id);
					}" >

				Kelurahan : 
				<input name="villages" id="villages"  class="easyui-combobox" required style="width: 150px"
					data-options="
					valueField:'name',
					textField:'name',
					mode:'remote',
					url:'<?=base_url($module . '/' . $appLink . '/getVillages/') ?>',
					onSelect :function(row)
					{
						console.log(row)
					}" >
				<!--<input class="easyui-combobox" name="language" style="width:250px;" data-options="
	                url:'<?=base_url($module . '/' . $appLink . '/getJenisBL') ?>',
	                method:'get',
	                valueField:'id',
	                textField:'text',
	                panelHeight:'auto',
	                label: 'Language:',
	                labelPosition: 'top'
	                ">-->
				<a href="javascript:void(0)" class="btn btn-small" onclick=" javascript:cari()"><i class="icon-search"></i>&nbsp;Cari</a>
				</td>
			</tr>
			<tr>
				<td><!-- <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick=" javascript:prev_sub_ledger()"><i class="icon-print"></i>&nbsp;Export Sub Ledger</a>&nbsp; --><a href="javascript:void(0)" class="btn btn-small btn-primary" iconCls: 'icon-search' onclick="exportPdf()">&nbsp;Export PDF</a></td>
			</tr>
	    </table>
	</form>
</div>
        <!-- end tombol datagrid -->
</div>