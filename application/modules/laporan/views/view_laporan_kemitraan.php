<?php
$app = 'Laporan Kemitraan'; // nama aplikasi
$module = 'laporan';
$appLink = 'laporan_kemitraan'; // controller
$appLink1 = 'laporan_mt';
$idField  = 'id_bl'; //field key table
$moduleref = 'vendorbl';
$appLinkref = 'databl'; // controller
$modulekemitraan = 'kemitraan';
$appLinkemitraan = 'datamitra'; // controller

?>

<script>
	var url;
	var app = "<?=$app?>";
	var appLink = '<?=$appLink?>';
	var module = '<?=$module?>';
	var idField = '<?=$idField?>';
  	var flagLaporan = 0;

    var dataFilter1 = [
        { label: 'Penyaluran', value: '1'},
        { label: 'Piutang', value: '2'},
        { label: 'Rencana/Realisasi', value: '3'}
    ];

    var dataFilter2 = [
        { label: 'Sektor Usaha', value: '1'},
        { label: 'Wilayah', value: '2'}
    ];

    function unduh(){
    	var filter1 = $('#filter1').combobox('getValue');
		var filter2 = $('#filter2').combobox('getValue');
		var tahun = $('#tahun').numberbox('getValue');
		
		if(filter1 && filter2 && tahun){
			window.open('<?=base_url($module . '/' . $appLink . '/read_laporan')?>/' + filter1 + '/' + filter2 + '/' + tahun, '_blank');
		} else {
			$.messager.alert('Filter Belum Dipilih','Tolong pilih filter!','error');
		}
    }

</script>
<div id="dlg" class="easyui-dialog" style="width:300px; height:200px; padding:10px" title="Filter Laporan" closed="false" buttons="#t_dlg_dis-buttons" >
    <table  align="center" style="padding: 0px; width: 99%;" border="0">
        <tr>
        <td>Filter Berdasarkan :</td>
        </tr>
        <tr>
          <td>
          <input class="easyui-combobox" name="filter1" id="filter1" required style="width: 200px;"
            data-options="
              method:'get',
              valueField:'value',
              textField:'label',
              data:dataFilter1,
              panelHeight:'auto',
              label: 'Language:',
              labelPosition: 'top',
              onSelect: function(param){
                   
              }
              "> 
          </td>
        </tr>
        <tr>
          <td><input class="easyui-combobox" name="filter2" id="filter2" required style="width: 200px;"
            data-options="
              method:'get',
              valueField:'value',
              textField:'label',
              panelHeight:'auto',
              label: 'Language:',
              data:dataFilter2,
              labelPosition: 'top',
              onSelect: function(param){
                   
              }
              "> 
          </td>
        </tr>
         <tr>
          <td>Tahun <input class="easyui-numberbox" id="tahun" name="tahun" data-options="min:1992,max:9999" required style="width:100%;">
          </td>
        </tr>
        <tr>
        <td><a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:unduh()"><i class="icon-print icon-large"></i>&nbsp;Unduh</a></td>
        </tr>
    </table>
</div>
