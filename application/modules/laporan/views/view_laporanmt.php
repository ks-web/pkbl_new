<?php
$app = 'Laporan Kemitraan'; // nama aplikasi
$module = 'laporan';
$appLink = 'laporan_mt'; // controller
$idField  = 'id_mitra'; //field key table
$moduleref = 'vendormt';
$appLinkref = 'datamt'; // controller
$modulekemitraan = 'kemitraan';
$appLinkemitraan = 'datamitra'; // controller

?>

<script>
	var url;
	var app = "<?=$app?>";
	var appLink = '<?=$appLink?>';
	var module = '<?=$module?>';
	var idField = '<?=$idField?>';
    function unduh(){
   	    var filter1 = $('#filter1').val();
		var filter2 = $('#filter2').val();
		
		if(filter1 && filter2){
			window.open('<?=base_url($module . '/' . $appLink . '/read_preview_excel')?>/' + filter1 + '/' + filter2, '_blank');
		} else {
			$.messager.alert('Filter Belum Dipilih','Tolong pilih filter!','error');
		}
    }
 

</script>
<div id="dlg" class="easyui-dialog" style="width:300px; height:200px; padding:10px" title="Filter Laporan" closed="false" buttons="#t_dlg_dis-buttons" >
    <table  align="center" style="padding: 0px; width: 99%;" border="0">
        <tr>
        <td>Filter Berdasarkan :</td>
        </tr>
        <tr>
          <td><select id="filter1" name="filter1">
          <option value="">Pilih...</option>
          <option value="1">Jenis Sektor Usaha</option>
          <option value="2">Wilayah</option>
          </select>
          </td>
        </tr>
        <tr>
          <td><select id="filter2" name="filter2">
          <option value="">Pilih...</option>
          <option value="1">Penyaluran</option>
          <option value="2">Rencana / Realisasi</option>
          </select>
          </td>
        </tr>
        <tr>
        <td><a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:unduh()"><i class="icon-print icon-large"></i>&nbsp;Unduh</a></td>
        </tr>
    </table>
</div>
