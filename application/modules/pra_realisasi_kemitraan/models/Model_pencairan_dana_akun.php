<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	Class Model_pencairan_dana_akun extends MY_Model {
		function __construct() {
			parent::__construct();
			//$this->_table = "takun_mitra"; // for insert, update, delete
			//$this->_view = "takun_mitra_vd"; // for call view
			$this->_table = "detail_akun_temp"; // for insert, update, delete
			$this->_view = "detail_akun_temp_mitra_vd"; // for call view
			$this->_order = 'asc';
			$this->_sort = 'id_akun';
			$this->_page = 1;
			$this->_rows = 10;

			$this->_create = true;
			$this->_update = true;

			if ($this->uri->segment(4)) {
				$this->_filter = array('id_cd_um' => $this->uri->segment(4), 'status' => 'KM', 'transaksi' => 'CD');
			}

			//parameter from post/get - search function
			if ($this->input->post('q')) {
				$this->_like = array(
					'id_akun' => $this->input->post('q'),
					'account_no' => $this->input->post('q'),
					'nama_akun' => $this->input->post('q')
				);
			}

			$this->_param = array('id_akun' => $this->input->post('id_akun'));

			//data array for input to database
			$this->_data = array(
				'account_no' => $this->input->post('account_no'),
				'debet' => $this->input->post('debet'),
				'kredit' => $this->input->post('kredit')
			);
			
		}

		public function getStatusBalance($filter)
		{
			//print_r($filter);
			$id_cd = $filter['A.id_cd_um'];
			//echo $id_cd;
			$filter2 = array('A.id_cd' => $id_cd);
			$this->db->select('A.status_progress');
			$this->db->from('tcd_all A');
			$this->db->where($filter2);

			$query = $this->db->get();
			$status_progress = $query->row()->status_progress;

			if($status_progress == 1){
				return array('status' => 1, 'msg' => 'Gagal Posting, Data Pencairan Dana Sudah Dibatalkan');
			} else {
				$this->db->select('SUM(A.debet)-SUM(A.kredit) balance');
				$this->db->from('detail_akun_temp A');
				$this->db->where($filter);

				$query = $this->db->get();
				$balance = $query->row()->balance;

				if($balance != 0){
					return array('status' => 1, 'msg' => 'Gagal Posting, Jumlah Nilai Akun Tidak Balance');
				} else {
					$this->db->select('A.account_no');
					$this->db->from('detail_akun_temp A');
					$this->db->join('mas_account B', 'B.kode_account = A.account_no AND B.no_rek IS NOT NULL');
					$this->db->where($filter);

					$query = $this->db->get();
					$bank = $query->num_rows();

					if($bank != 0){
						return array('status' => 0, 'msg' => 'Posting Berhasil');
					} else {
						return array('status' => 1, 'msg' => 'Gagal Posting, Tidak Ada Akun Bank Yang Terpilih');
					}
				}
				
			}

			//return $bank;

			//return $query->row()->balance;
		}

		public function getStatusPosting($filter)
		{
			$this->db->select('A.*');
			$this->db->from('tcd_all A');
			$this->db->where($filter);

			$query = $this->db->get();

			return $query->result_array();
		}

		public function setStatusPosting($filter, $data)
		{
			$this->db->where($filter);
			$this->db->update('tcd_all A', $data);

			return array('success' => true);
		}

		public function getAccount($filter, $like)
		{
			$this->db->select('A.*');
			$this->db->from('mas_account A');
			$this->db->or_like($like);
			$this->db->where($filter);

			$query = $this->db->get();

			return $query->result_array();
		}

	}
