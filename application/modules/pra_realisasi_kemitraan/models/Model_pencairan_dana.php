<?php
if (! defined('BASEPATH'))
    exit('No direct script access allowed');

class Model_pencairan_dana extends MY_Model
{

    function __construct()
    {
        parent::__construct();
        // $this->_table = "tcd_mitra"; // for insert, update, delete
        // $this->_view = "tcd_mitra_vc"; // for call view
        $this->_table = "tcd_all"; // for insert, update, delete
        $this->_view = "tcd_all_mitra_vd"; // for call view
        $this->_order = 'asc';
        $this->_sort = 'id_cd';
        $this->_page = 1;
        $this->_rows = 10;
        
        $this->_create = true;
        $this->_update = true;
        
        if ($this->uri->segment(4)) {
            $this->_filter = array(
                'id_cd' => $this->uri->segment(4)
            );
        }
        
        // parameter from post/get - search function
        if ($this->input->post('q')) {
            $this->_like = array(
                'id_cd' => $this->input->post('q'),
                'id_mitra' => $this->input->post('q'),
                'nama' => $this->input->post('q'),
                'nama_perusahaan' => $this->input->post('q')
            );
        }
        
        $this->_param = array(
            'id_cd' => $this->input->post('id_cd')
        );
        
        // data array for input to database
        $this->_data = array(
				/*'id_mitra' => $this->input->post('id_mitra'),
				'tanggal_cd' => $this->input->post('tanggal_cd'),
				'bunga' => $this->input->post('bunga'),
				'tahun' => $this->input->post('tahun'),
				'total_bunga' => $this->input->post('total_bunga'),
				'angsuran_pokok' => $this->input->post('angsuran_pokok'),
				'angsuran_bunga' => $this->input->post('angsuran_bunga'),
				'total_pinjaman_bunga' => $this->input->post('total_pinjaman_bunga'),
				'angsuran_bulanan' => $this->input->post('angsuran_bulanan'),
				'kode_bayar' => $this->input->post('kode_bayar')*/
				'no' => $this->input->post('id_mitra'),
            'tanggal_cd' => $this->input->post('tanggal_cd'),
            'modifier' => $this->session->userdata('id'),
            'tanggal_modified' => date('Y-m-d'),
            'kode_bayar' => $this->input->post('kode_bayar'),
            'keterangan' => $this->input->post('keterangan')
        );
    }

    public function getMitra($filter)
    {
        $this->db->select('A.*, D.angsuran_pokok nilai_pengajuan');
        $this->db->from('tmitra A');
        $this->db->join('tpersetujuan_mitra B', 'A.id_mitra = B.id_mitra');
        $this->db->join('tcd_all_mitra_vd C', 'C.no = B.id_mitra', 'LEFT');
        $this->db->join('tpenilaian_survey_pengusaha_evaluator D', 'D.id_mitra = A.id_mitra', 'LEFT');
        $this->db->where($filter);
        
        $query = $this->db->get();
        
        // echo $this->db->last_query();
        
        return $query->result_array();
    }

    public function getLastDataMitra()
    {
        $this->db->select('A.*');
        $this->db->from('tcd_mitra A');
        $this->db->order_by('id_cd', 'DESC');
        $this->db->limit(1);
        
        $query = $this->db->get();
        $result = $query->result_array();
        
        return $result[0]['id_cd'];
    }

    public function getAccess($filter)
    {
        $this->db->select('A.*');
        $this->db->from('approval_cd A');
        $this->db->where($filter);
        
        $query = $this->db->get();
        
        return $query->result_array();
    }

    public function setAccess($filter, $data)
    {
        $this->db->where($filter);
        $this->db->update('tcd_mitra', $data);
    }

    /*
     * function getakun($idbl){
     * $sql="SELECT * FROM tcd_bl WHERE id_bl='$idbl'";
     * $query = $this->db->query($sql);
     * $row = $query->row();
     * $idcd=$row->id_cd;
     * $this->db->select('*,(SELECT SUM(debet) FROM takun_bl WHERE id_cd="'.$idcd.'")totaldebet,(SELECT SUM(kredit) FROM takun_bl WHERE id_cd="'.$idcd.'")totalkredit');
     * $this->db->where("id_cd", $idcd);
     * $dataakun = $this->db->get("takun_bl");
     * return $dataakun;
     * }
     */
    public function getAccountPencairan($filter)
    {
        $this->db->select('A.*');
        $this->db->from('detail_akun_transaksi_all_mitra_vd A');
        $this->db->where($filter);
        
        $query = $this->db->get();
        
        return $query->result_array();
    }

    public function getPencairan($filter)
    {
        $this->db->select('A.*, A.nilai_usulan nilai_disetujui');
        $this->db->from('tcd_all_mitra_vd A');
        $this->db->where($filter);
        
        $query = $this->db->get();
        
        return $query->result_array();
    } 
	
	public function getPrevPencairan($filter2)
    {
        $this->db->select('A.*');
        $this->db->from('detail_akun_temp_mitra_vd A');
        $this->db->where($filter2);
        
        $query = $this->db->get();
        
        return $query->result_array();
    }

    public function getsign()
    {
        $sign = $this->db->get("sign_vd");
        return $sign;
    }

    function getdataPencairan()
    {
        $param = $this->uri->segment(4);
        $this->db->where("id_cd", $param);
        $vendor = $this->db->get("cetakpencairandana_vd");
        return $vendor;
    }

    public function createAccount($id)
    {
        // $this->db->select('sektor_usaha, nilai_disetujui');
        // $this->db->from('tcd_mitra_vc');
        $this->db->select('sektor_usaha, nilai_usulan AS nilai_disetujui');
        $this->db->from('tcd_all_mitra_vd');
        $this->db->where('id_cd', $id);
        $query = $this->db->get();
        
        $result = $query->row();
        
        /*
         * $this->db->select('A.*, B.uraian debet_account, C.uraian kredit_account');
         * $this->db->from('mas_account_sektor A');
         * $this->db->join('mas_account B', 'B.kode_account = A.debet');
         * $this->db->join('mas_account C', 'C.kode_account = A.kredit');
         * $this->db->where(array('menu' => 1, 'id_sektor' => $result->sektor_usaha));
         * $query2 = $this->db->get();
         *
         * $result2 = $query2->row();
         *
         * $data = array('id_cd_um' => $id, 'account_no' => $result2->debet, 'nama_akun' => $result2->debet_account, 'kredit' => 0, 'debet' => $result->nilai_disetujui, 'status' => 'KM', 'transaksi' => 'CD');
         * //$this->db->insert('takun_mitra', $data);
         * $this->db->insert('detail_akun_transaksi_all', $data);
         *
         * $data = array('id_cd_um' => $id, 'account_no' => $result2->kredit, 'nama_akun' => $result2->kredit_account, 'kredit' => $result->nilai_disetujui, 'debet' => 0, 'status' => 'KM', 'transaksi' => 'CD');
         * //$this->db->insert('takun_mitra', $data);
         * $this->db->insert('detail_akun_transaksi_all', $data);
         */
        
        $this->db->select('A.*, B.uraian');
        $this->db->from('mas_account_sektor A');
        $this->db->join('mas_account B', 'B.kode_account = A.account_no');
        $this->db->where(array(
            'status' => 'KM',
            'transaksi' => 'CD',
            'id_sektor' => $result->sektor_usaha
        ));
        $query2 = $this->db->get();
        $result2 = $query2->result_array();
        // print_r($result2);
        foreach ($result2 as $key) {
            if ($key['posisi'] == 'C') {
                $credit = $result->nilai_disetujui;
                $debet = 0;
            } else {
                $debet = $result->nilai_disetujui;
                $credit = 0;
            }
            $data = array(
                'id_cd_um' => $id,
                'account_no' => $key['account_no'],
                'nama_akun' => $key['uraian'],
                'kredit' => $credit,
                'debet' => $debet,
                'status' => 'KM',
                'transaksi' => 'CD'
            );
            // print_r($data);
            // echo "<br>";
            $this->db->insert('detail_akun_temp', $data);
        }
    }

    public function createJatuhTempo($id)
    {
        $this->db->select('A.id_mitra, A.tanggal_cd, B.tahun t_angsuran, YEAR(tanggal_cd) tahun, MONTH(tanggal_cd) bulan, DAY(tanggal_cd) hari, A.status_sektor');
        $this->db->from('tcd_all_mitra_vd A');
        $this->db->join('tpenilaian_survey_pengusaha_evaluator B', 'B.id_mitra = A.no');
        $this->db->where('A.id_cd', $id);
        $query = $this->db->get();
        
        // echo $this->db->last_query();
        
        $result = $query->row();
        
        if ($result->status_sektor == 1) {
            $jumlah_angsuran = $result->t_angsuran * 2;
            $bulan_step = 6;
        } else {
            $jumlah_angsuran = $result->t_angsuran * 12;
            $bulan_step = 1;
        }
        
        $tahun = $result->tahun;
        $bulan = $result->bulan;
        $hari = $result->hari;
        // if($hari == 31){
        // $hari = 1;
        // $bulan = $bulan + 1;
        // } else if($hari == 28 && $bulan == 2) {
        // $hari = 1;
        // $bulan = $bulan + 1;
        // }
        
        if ($hari > 28) {
            $hari = 1;
            $bulan = $bulan + 1;
        }
        
        for ($i = 1; $i <= $jumlah_angsuran; $i ++) {
            // $bulan = $bulan + 1;
            $bulan = $bulan + $bulan_step;
            if ($bulan == 13) {
                $bulan = $bulan - 12;
                $tahun = $tahun + 1;
            }
            
            $data = array(
                'id_mitra' => $result->id_mitra,
                'tgl_jatuh_tempo' => $tahun . '-' . $bulan . '-' . $hari,
                'angsuran_ke' => $i
            );
            $this->db->insert('tbl_jatuhtempo', $data);
        }
    }

    public function statusDelete($id)
    {
        $this->db->select('COUNT(*) total');
        $this->db->from('tcd_all A');
        $this->db->join('tbl_angsuran B', 'B.id_mitra = A.no');
        $this->db->where('A.id_cd', $id);
        $query = $this->db->get();
        
        $result = $query->row();
        
        return $result->total;
    }

    public function deleteJatuhTempo($id)
    {
        $this->db->select('A.no');
        $this->db->from('tcd_all A');
        $this->db->where('A.id_cd', $id);
        $query = $this->db->get();
        
        $result = $query->row();
        
        $this->db->where('id_mitra', $result->no);
        $this->db->delete('tbl_jatuhtempo');
    }

    public function deleteAccount($id)
    {
        $this->db->where('id_cd_um', $id);
        $this->db->where('status', 'KM');
        $this->db->where('transaksi', 'CD');
        $this->db->delete('detail_akun_transaksi_all');
    }

    public function gen()
    {
        $this->db->select('A.*');
        $this->db->from('tbl1 A');
        // $this->db->where($filter);
        //$this->db->limit(1000, 10000);
        // $this->db->limit(1000);
        
        $query = $this->db->get();
        
        return $query->result_array();
    }
}
