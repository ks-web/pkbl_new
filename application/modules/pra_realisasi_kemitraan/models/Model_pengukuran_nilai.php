<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	Class Model_pengukuran_nilai extends MY_Model {
		function __construct() {
			parent::__construct();
			$this->_table 	= "t_ukur_nilai";
			// for insert, update, delete
			$this->_view 	= "t_ukur_nilai_vd";
			// for call view
			$this->_order 	= 'desc';
			$this->_sort 	= 'id_tnilai';
			$this->_page 	= 1;
			$this->_rows 	= 10;

			$this->_create 	= true;
			$this->_update 	= true;

			if ($this->uri->segment(4)) {
				$this->_filter = array('id_tnilai' => $this->uri->segment(4));
			}

			//parameter from post/get - search function
			if ($this->input->post('q')) {
				$this->_like = array(
					'id_mitra' 			=> $this->input->post('q'),
					'nama_pemohon' 		=> $this->input->post('q'),
					'nama_perusahaan' 	=> $this->input->post('q')
				);
			}

			$this->_param = array('id_tnilai' => $this->input->post('id_tnilai'));

			//data array for input to database
			$this->_data = array(
				'id_mitra' 		=> $this->input->post('id_mitra'),
				'jenis_jaminan' 			=> $this->input->post('id_jaminan'),
				'deskripsi' 				=> $this->input->post('deskripsi_bangunan'),
				'keterangan' 				=> $this->input->post('keterangan_bangunan'),
				'luas_tanah' 				=> $this->input->post('luas_tanah'),
				'luas_bangunan' 			=> $this->input->post('luas_bangunan'),
				'btn_harga_tanah' 			=> str_replace(',','',$this->input->post('btn_harga_tanah')),
				'btn_harga_bangunan' 		=> str_replace(',','',$this->input->post('btn_harga_bangunan')),
				'kelurahan_harga_tanah' 	=> str_replace(',','',$this->input->post('kelurahan_harga_tanah')),
				'kelurahan_harga_bangunan' 	=> str_replace(',','',$this->input->post('kelurahan_harga_bangunan')),
				'njop_harga_tanah' 			=> str_replace(',','',$this->input->post('njop_harga_tanah')),
				'njop_harga_bangunan' 		=> str_replace(',','',$this->input->post('njop_harga_bangunan')),
				'btn_nilai' 				=> str_replace(',','',$this->input->post('btn_nilai')),
				'kelurahan_nilai' 			=> str_replace(',','',$this->input->post('kelurahan_nilai')),
				'njop_nilai' 				=> str_replace(',','',$this->input->post('njop_nilai')),
				'btn_max' 					=> str_replace(',','',$this->input->post('btn_max')),
				'kelurahan_max' 			=> str_replace(',','',$this->input->post('kelurahan_max')),
				'njop_max'	 				=> str_replace(',','',$this->input->post('njop_max')),
				'njop_x3_tanah'	 			=> str_replace(',','',$this->input->post('njop_x3_tanah')),
				'njop_x3_bangunan'	 		=> str_replace(',','',$this->input->post('njop_x3_bangunan')),
				'usulan_pinjaman' 			=> str_replace(',','',$this->input->post('has_usulan_pinjaman')),
				'total_jaminan' 			=> str_replace(',','',$this->input->post('total_jaminan')),
				'max_pinjaman' 				=> str_replace(',','',$this->input->post('max_pinjaman')),
				'rekomendasi' 				=> str_replace(',','',$this->input->post('has_rekomendasi')),
				'status' 					=> '6',
				'tanggal_create' 			=> date('Y-m-d h:m:s'),
				'creator' 					=> $this->session->userdata('id')
				//'tanggal_modified' 	=> str_replace(',','',$this->input->post('')),
				//'modifier' 			=> str_replace(',','',$this->input->post(''))
			);

		}

		// additional function
		/*public function get_mitra($where = null) {
			$this->db->select('*');
			$this->db->from('pengukuran_nilai_mitra_vd');

			/*
			$this->db->or_like(array(
				'id_mitra' => $where,
				'nama' => $where
			));
			
			
			return $this->db->get()->result_array();
		
		}*/
		
		
		public function get_sektor_kemitraan($where = null) {
			$this->db->select('*');
			$this->db->from('sektor_kemitraan_vd');

			$this->db->or_like(array(
				'id_sektor' => $where,
				'nama_sektor' => $where
			));
			return $this->db->get()->result_array();
		}

		public function get_detail_jaminan($get = array()) {
			$this->_view = 't_ukur_nilai_vd';
			$order 	= $this->input->post('order') ? $this->input->post('order') : $this->_order;
			$sort 	= $this->input->post('sort') ? $this->input->post('sort') : $this->_sort;
			$page 	= $this->input->post('page') ? intval($this->input->post('page')) : $this->_page;
			$rows 	= $this->input->post('rows') ? intval($this->input->post('rows')) : $this->_rows;
			$offset = (($page - 1) * $rows);
			if (sizeof($get) > 0) {
				$this->db->where($get);
			}
			if (sizeof($this->_filter) > 0) {
				$this->db->where($this->_filter);
			}
			if (sizeof($this->_like) > 0) {
				$this->db->or_like($this->_like);
			}
			$this->db->order_by($sort, $order);
			$this->db->limit($rows, $offset);
			$result = $this->db->get($this->_view);

			$json['total'] = $this->_total($get);
			$json['rows'] = $result->result_array();

			return json_encode($json);
		}
		
		
		public function view_report_pengukuran($id)
		{
			$this->db->select('*');
			$this->db->where('id_tnilai', $id);
			return $this->db->get('t_ukur_nilai_vd')->row_array();
		}
		
		public function view_report_usulan($id)
		{
			$this->db->select('*');
			$this->db->where('id_mitra', $id);
			return $this->db->get('usulan_pinjaman_vd')->row_array();
		}

		public function get_pegawai()
		{
			$this->db->select('*');
			$this->db->like( 'idPegawai',$this->input->post('q'));
			$this->db->or_like( 'nama_pegawai',$this->input->post('q'));
			$query = $this->db->get('mas_approval_vd');
			return $query->result_array();
		}
		
		public function getPesan()
		{
			$this->db->select('*');
			$query = $this->db->get('mas_pesan_disposisi');
			return $query->result_array();
		}
		
		public function add_disposisi($data)
		{
			$q = $this->db->insert('disposisi_transaksi',$data);
			if ($q) 
			{
				$this->output->set_output(json_encode(array('success'=>TRUE)));
			}
			else
			{
				$this->output->set_output(json_encode(array('msg'=>'Gagal dalam penambahan data')));
			}
		}
		
		public function disposisi_selesai($id_doc, $nip_tujuan, $data){
			$ses_id = $this->session->userdata('idPegawai');

			$this->db->select('nip_tujuan');
			$this->db->where('id_doc',$id_doc);
			$this->db->where('nip_tujuan',$nip_tujuan);
			$nip = $this->db->get('disposisi_transaksi')->result_array();
			$nip_tujuan = $nip[0]['nip_tujuan'];
			
			if($nip_tujuan == $ses_id){
					$this->db->where('id_doc',$id_doc);
					$this->db->where('nip_tujuan',$ses_id);
					$q	= $this->db->update('disposisi_transaksi', $data);

					if ($q)
					{
						$this->output->set_output(json_encode(array('success'=>TRUE)));
					}
					else
					{
						$this->output->set_output(json_encode(array('msg'=>'Gagal update disposisi selesai')));
					}
			}else{
				$this->output->set_output(json_encode(array('msg'=>'User yang Aktif tidak Bisa Melakukan Approve')));
			}
		}
		
		public function get_disposisi($id_doc)
		{
			$this->db->where('node_menu',35);
			$this->db->where('id_doc',$id_doc);
			$data = $this->db->get('disposisi_transaksi_vd')->result_array();

			return $data;
		}

		public function getDataMitra($filter, $like)
		{
			$this->db->select('A.*');
			$this->db->from('pengukuran_nilai_mitra_vd A');
			$this->db->or_like($like);
			//$this->db->where($filter);

			$query = $this->db->get();

			return $query->result_array();
		}

		public function updateStatusMitra($filter, $data)
		{
			$this->db->where($filter);
			$this->db->update('tmitra', $data);
		}
		
		public function get_userAllow( $idpegawai = ''){
			$this->db->where('idpegawai',$idpegawai);
			$this->db->where('nodemenu',35);
			$this->db->where('allow',1);
			$data = $this->db->get('t_allowdata')->row_array();
			$allow = $data['idpegawai'];
			return $allow;
		}
		
	}