<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	Class Model_k_bbaku extends MY_Model {
		function __construct() {
			parent::__construct();
			$this->_table = "k_bbbaku"; // for insert, update, delete
			$this->_view = "k_bbaku_vd"; // for call view
			$this->_order = 'asc';
			$this->_sort = 'id_bbbaku';
			$this->_page = 1;
			$this->_rows = 10;

			$this->_create = true;
			$this->_update = true;

			if ($this->uri->segment(4)) {
				$this->_filter = array('id_mitra' => $this->uri->segment(4));
			}

			//parameter from post/get - search function
			if ($this->input->post('q')) {
				$this->_like = array(
					'id_bbbaku' => $this->input->post('q'),
					'id_mitra' => $this->input->post('q'),
					'uraian' => $this->input->post('q')
				);
			}

			$this->_param = array('id_bbbaku' => $this->input->post('id_bbbaku'));

			//data array for input to database
			$this->_data = array(
				// 'id_bbbaku' => $this->input->post('id_bbbaku'),
				// 'id_mitra' => $this->input->post('id_mitra'),
				'unsur_biaya'=> $this->input->post('unsur_biaya'),
				'satuan'=> $this->input->post('satuan'),
				'harga_satuan'=> $this->input->post('harga_satuan'),
				'junit_akt'=> $this->input->post('junit_akt'),
				'junit_proy'=> $this->input->post('junit_proy'),
				'bprod_akt'=> $this->input->post('bprod_akt'),
				'bprod_proy'=> $this->input->post('bprod_proy')
			);
			
		}
			public function getsatuan($filter = null)
        {
            $this->db->like('CONCAT( (id_satuan),(satuan)  )', $filter );
            $query = $this->db->get('mas_satuan')->result_array();

            return $query;
        }


	}
