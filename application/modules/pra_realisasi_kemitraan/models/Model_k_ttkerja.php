<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	Class Model_k_ttkerja extends MY_Model {
		function __construct() {
			parent::__construct();
			$this->_table = "k_ttkerja"; // for insert, update, delete
			$this->_view = "k_ttkerja_vd"; // for call view
			$this->_order = 'asc';
			$this->_sort = 'id_ttkerja';
			$this->_page = 1;
			$this->_rows = 10;

			$this->_create = true;
			$this->_update = true;

			if ($this->uri->segment(4)) {
				$this->_filter = array('id_mitra' => $this->uri->segment(4));
			}

			//parameter from post/get - search function
			if ($this->input->post('q')) {
				$this->_like = array(
					'id_ttkerja' => $this->input->post('q'),
					'id_mitra' => $this->input->post('q'),
					'bagian' => $this->input->post('q'),
					'upah_periode' => $this->input->post('q')
				);
			}

			$this->_param = array('id_ttkerja' => $this->input->post('id_ttkerja'));

			//data array for input to database
			$this->_data = array(
				// 'id_ttkerja' => $this->input->post('id_ttkerja'),
				// 'id_mitra' => $this->input->post('id_mitra'),
				'bagian' => $this->input->post('bagian'),
				'unit' => $this->input->post('unit'),
				'upah_perbulan' => $this->input->post('upah_perbulan'),
				'jumlah_upah' => $this->input->post('jumlah_upah'),
				'upah_periode' => $this->input->post('upah_periode')
			);
			
		}
		function getperiode($filter){
			$this->db->select('A.*');
			$this->db->from('laporan_hasil_survey_dep_k13 A');
			$this->db->where($filter);

			$query = $this->db->get();

			return $query->result_array();
		}

	}
