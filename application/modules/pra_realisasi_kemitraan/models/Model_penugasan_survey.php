<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	Class Model_penugasan_survey extends MY_Model {
		function __construct() {
			parent::__construct();
			$this->_table = "tpenugasan_survey";
			// for insert, update, delete
			$this->_view = "tpenugasan_survey_vd";
			// for call view
			$this->_order = 'DESC';
			$this->_sort = 'id_mitra';
			$this->_page = 1;
			$this->_rows = 10;

			$this->_create = true;
			$this->_update = true;

			if ($this->uri->segment(4)) {
				$this->_filter = array('id_mitra' => $this->uri->segment(4));
			}

			//parameter from post/get - search function
			if ($this->input->post('q')) {
				$this->_like = array(
					'id_mitra' => $this->input->post('q'),
					'nama_pemohon' => $this->input->post('q'),
					'nama_perusahaan' => $this->input->post('q')
				);
			}

			$this->_param = array('id_mitra' => $this->input->post('id_mitra'));

			//data array for input to database
			$this->_data = array(
				'id_mitra' => $this->input->post('id_mitra'),
				'tanggal_survey' => $this->input->post('tanggal_survey'),
				'petugas_survey' => $this->input->post('petugas_survey'),
				'pemberi_tugas'=> $this->session->userdata('idPegawai')
				// 'pemberi_tugas' => $this->session->userdata('id'),
			);

		}

		// additional function
		public function get_mitra($where = null) {
			$this->db->select('id_mitra, nama, nama_perusahaan, no_ktp, nilai_pengajuan, kota');
			$this->db->from('tmitra');

			$this->db->or_like(array(
				'id_mitra' => $where,
				'nama' => $where
			));
			return $this->db->get()->result_array();
		}

		public function get_petugas_survey($where = null) {
			$this->db->select('idPegawai, nama_pegawai');
			$this->db->from('mas_pegawai');
			$this->db->where('id_jabatan', '7');
			
			$this->db->or_like(array(
				'nama_pegawai' => $where
			));
			return $this->db->get()->result_array();
		}

		public function permohonan(){
			$param = $this->uri->segment(4);
			$this->db->where("id_mitra", $param);
			$vendor = $this->db->get("data_pemohon_usahakecil_vd");
			return $vendor;
		}

	}
