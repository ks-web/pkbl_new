<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	Class Model_k_investasi extends MY_Model {
		function __construct() {
			parent::__construct();
			$this->_table = "k_investasi"; // for insert, update, delete
			$this->_view = "k_investasi_vd"; // for call view
			$this->_order = 'asc';
			$this->_sort = 'id_investasi';
			$this->_page = 1;
			$this->_rows = 10;

			$this->_create = true;
			$this->_update = true;

			if ($this->uri->segment(4)) {
				$this->_filter = array('id_mitra' => $this->uri->segment(4));
			}

			//parameter from post/get - search function
			if ($this->input->post('q')) {
				$this->_like = array(
					'id_investasi' => $this->input->post('q'),
					'id_mitra' => $this->input->post('q'),
					'kebutuhan_investasi' => $this->input->post('q')
				);
			}

			$this->_param = array('id_investasi' => $this->input->post('id_investasi'));

			//data array for input to database
			$this->_data = array(
				
				'kebutuhan_investasi' => $this->input->post('kebutuhan_investasi'),
				'jumlah' => $this->input->post('jumlah'),
				'satuan' => $this->input->post('satuan'),
				'harga_satuan' => $this->input->post('harga_satuan'),
				'unit' => $this->input->post('unit')
					
			);
			
		}
		public function getsatuan($filter = null)
        {
            $this->db->like('CONCAT( (id_satuan),(satuan)  )', $filter );
            $query = $this->db->get('mas_satuan')->result_array();

            return $query;
        }

	}
