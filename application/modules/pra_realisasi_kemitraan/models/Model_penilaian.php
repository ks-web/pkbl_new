<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	Class Model_penilaian extends MY_Model {
		function __construct() {
			parent::__construct();
			$this->_table = "tpenilaian_survey_pengusaha"; // for insert, update, delete
			$this->_view = "tpenilaian_survey_kemitraan_vd"; // for call view
			$this->_order = 'desc';
			$this->_sort = 'id_mitra';
			$this->_page = 1;
			$this->_rows = 10;

			$this->_create = true;
			$this->_update = true;

			if ($this->uri->segment(4)) {
				$this->_filter = array('id_mitra' => $this->uri->segment(4));
			}

			//parameter from post/get - search function
			if ($this->input->post('q')) {
				$this->_like = array(
					'id_mitra' => $this->input->post('q'),
					'nama' => $this->input->post('q'),
					'nama_perusahaan' => $this->input->post('q')
				);
			}

			$this->_param = array('id_mitra' => $this->input->post('id_mitra'));

			//data array for input to database
			$this->_data = array(
				'id_mitra' => $this->input->post('id_mitra'),
				'pekerjaan' => $this->input->post('pekerjaan'),
				'tanggungan' => $this->input->post('tanggungan'),
				'biaya_hidup' => $this->input->post('biaya_hidup'),
				'pendapatan_luar' => $this->input->post('pendapatan_luar'),
				'jaminan' => $this->input->post('jaminan'),
				'status_jaminan' => $this->input->post('status_jaminan'),
				'detail_usaha' => $this->input->post('detail_usaha'),
				'alamat' => $this->input->post('alamat'),
				'propinsi' => $this->input->post('propinsi'),
				'kota' => $this->input->post('kota'),
				'kecamatan' => $this->input->post('kecamatan'),
				'kelurahan' => $this->input->post('kelurahan'),
				'status_tempat' => $this->input->post('status_tempat'),
				'mulai_usaha' => $this->input->post('mulai_usaha'),
				'mampu_bayar' => $this->input->post('mampu_bayar'),
				'tenagakerja' => $this->input->post('tenagakerja')
				// ,
				// 'tanggal_create'=> $this->input->post('tanggal_create'),
				// 'creator'=> $this->input->post('creator'),
				// 'tanggal_modified'=> $this->input->post('tanggal_modified'),
				// 'modifier'=> $this->input->post('modifier'),
				// 'file_survey'=> $this->input->post('file_survey')
			);
			
		}

		public function getDataMitra()
		{
			// $this->db->select('A.*,B.petugas_survey,C.nama_jaminan');
			// $this->db->from('tmitra A');
			// $this->db->join('tpenugasan_survey B', 'B.id_mitra = A.id_mitra', 'left');
			// $this->db->join('mas_jaminan C', 'C.id_jaminan = A.jaminan', 'left');
			// $this->db->where($filter);
			$query = $this->db->get('getmitraforsurvey_vd');

			return $query->result_array();
		}

		public function getPropinsi($filter)
		{
			$this->db->select('A.*');
			$this->db->from('provinces A');
			$this->db->where($filter);

			$query = $this->db->get();

			return $query->result_array();
		}

		public function getKota($filter)
		{
			$this->db->select('A.*');
			$this->db->from('regencies A');
			$this->db->where($filter);

			$query = $this->db->get();

			return $query->result_array();
		}

		public function getKecamatan($filter)
		{
			$this->db->select('A.*');
			$this->db->from('districts A');
			$this->db->where($filter);

			$query = $this->db->get();

			return $query->result_array();
		}

		public function getKelurahan($filter)
		{
			$this->db->select('A.*');
			$this->db->from('villages A');
			$this->db->where($filter);

			$query = $this->db->get();

			return $query->result_array();
		}

		public function updateStatusMitra($filter, $data)
		{
			$this->db->where($filter);
			$this->db->update('tmitra', $data);
		}
		public function getjaminan($filter)
		{
			$this->db->select('A.*');
			$this->db->from('mas_jaminan A');
			//$this->db->where($filter);

			$query = $this->db->get();

			return $query->result_array();
		}
        public function filter($id_mitra){
            $data = $this->db
                ->where('id_mitra',$id_mitra)
                ->get('laporan_hasil_survey_vd')->result_array();
            return $data;
        }
        function update_file($id,$data)
    	{
        $this->db->where('id_mitra',$id);
        $update = $this->db->update($this->_table,$data);
        if($update){
            $hasil=1;
            return $hasil;
        }else{
            $hasil=0;
            return $hasil;
        }
	}
}
