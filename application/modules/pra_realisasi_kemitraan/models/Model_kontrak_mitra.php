<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	Class Model_kontrak_mitra extends MY_Model {
		function __construct() {
			parent::__construct();
			$this->_table = "tmitra"; // for insert, update, delete
			$this->_view = "tkontrak_mitra_vd"; // for call view
			$this->_order = 'asc';
			$this->_sort = 'no_kontrak';
			$this->_page = 1;
			$this->_rows = 10;

			$this->_create = true;
			$this->_update = true;

			if ($this->uri->segment(4)) {
				$this->_filter = array('id_kontrak' => $this->uri->segment(4));
			}

			//parameter from post/get - search function
			if ($this->input->post('q')) {
				$this->_like = array(
					'no_kontrak' => $this->input->post('q'),
					'nama_perusahaan' => $this->input->post('q'),
					'nama' => $this->input->post('q')
				);
			}

			$this->_param = array('id_kontrak' => $this->input->post('id_kontrak'));

			//data array for input to database
			$this->_data = array(
				'status' => '5'
			);
			
		}
        function getmitra($cari) {
			$this->db->like('CONCAT(id_mitra,nama,no_ktp)', $cari);
			$data = $this->db->get('tmitra_for_kontrak_vd')->result();
			$this->output->set_output(json_encode($data));
            // echo $this->db->last_query();
		}
		
        function sektor_mitra($cari) {
			$this->db->select('id_sektor, nama_mitra, keterangan');
			$this->db->like('CONCAT(id_sektor,nama_mitra)', $cari);
			$data = $this->db->get('sektor_mitra')->result();

			$this->output->set_output(json_encode($data));
		}
        function editmitra($id,$data)
        {
            $this->db->where('id_mitra',$id);
            //$this->db->where('idPerusahaan',$this->session->userdata('idPerusahaan'));
            $update = $this->db->update('tmitra',$data);
           
            
        }
        public function kontrak_mitra($param){
			//$param = $this->uri->segment(4);
			$this->db->where("no_kontrak", $param);
			$vendor = $this->db->get("kontrak_mintra_pdf_vd");
			return $vendor;
		}

		public function no_kontrak(){
			$vendor= $this->db->query('SELECT
										tpersetujuan_mitra.id_persetujuan,
										tpersetujuan_mitra.id_mitra,
										tpersetujuan_mitra.tgl_kontrak,
										tpersetujuan_mitra.no_kontrak,
										YEAR (now()) AS tgl_sekarng
									FROM
										tpersetujuan_mitra
									WHERE
										tpersetujuan_mitra.no_kontrak IS NOT NULL
									AND YEAR (tpersetujuan_mitra.tgl_kontrak) = YEAR (now())
									ORDER BY
										tpersetujuan_mitra.no_kontrak DESC
									LIMIT 1');
			return $vendor;
		}

	}
