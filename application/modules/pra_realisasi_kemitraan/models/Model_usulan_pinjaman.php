<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	Class Model_usulan_pinjaman extends MY_Model {
		function __construct() {
			parent::__construct();
			$this->_table 	= "t_ukur_nilai";
			// for insert, update, delete
			//$this->_view 	= "usulan_pinjaman_list_vd";
			$this->_view 	= "t_ukur_nilai_vd";
			// for call view
			$this->_order 	= 'desc';
			$this->_sort 	= 'id_tnilai';
			$this->_page 	= 1;
			$this->_rows 	= 10;

			$this->_create 	= true;
			$this->_update 	= true;

			if ($this->uri->segment(4)) {
				$this->_filter = array('id_tnilai' => $this->uri->segment(4));
			}

			//parameter from post/get - search function
			if ($this->input->post('q')) {
				$this->_like = array(
					'id_mitra' 			=> $this->input->post('q')
				);
			}

			$this->_param = array('id_tnilai' => $this->input->post('id_tnilai'));

			//data array for input to database
			$this->_data = array(
				
			);

		}

		// additional function
		
	}