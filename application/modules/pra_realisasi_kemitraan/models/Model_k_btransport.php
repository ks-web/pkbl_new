<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	Class Model_k_btransport extends MY_Model {
		function __construct() {
			parent::__construct();
			$this->_table = "k_btransport"; // for insert, update, delete
			$this->_view = "k_btransport_vd"; // for call view
			$this->_order = 'asc';
			$this->_sort = 'id_btransport';
			$this->_page = 1;
			$this->_rows = 10;

			$this->_create = true;
			$this->_update = true;

			if ($this->uri->segment(4)) {
				$this->_filter = array('id_mitra' => $this->uri->segment(4));
			}

			//parameter from post/get - search function
			if ($this->input->post('q')) {
				$this->_like = array(
					'id_btransport' => $this->input->post('q'),
					'id_mitra' => $this->input->post('q'),
					'uraian' => $this->input->post('q')
				);
			}

			$this->_param = array('id_btransport' => $this->input->post('id_btransport'));

			//data array for input to database
			$this->_data = array(
				// 'id_btransport'=> $this->input->post('id_btransport'),
				// 'id_mitra'=> $this->input->post('id_mitra'),
				'uraian'=> $this->input->post('uraian'),
				'satuan'=> $this->input->post('satuan'),
				'harga_satuan'=> $this->input->post('harga_satuan'),
				'unit_akt'=> $this->input->post('unit_akt'),
				'unit_proy'=> $this->input->post('unit_proy'),
				'produksi_akt'=> $this->input->post('produksi_akt'),
				'produksi_proy'=> $this->input->post('produksi_proy'),
			);
			
		}
		public function getsatuan($filter = null)
        {
            $this->db->like('CONCAT( (id_satuan),(satuan)  )', $filter );
            $query = $this->db->get('mas_satuan')->result_array();

            return $query;
        }

	}
