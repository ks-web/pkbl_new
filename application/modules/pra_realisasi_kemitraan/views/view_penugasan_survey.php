<?php
$app = 'Penugasan Survey'; // nama aplikasi
$module = 'pra_realisasi_kemitraan';
$appLink = 'penugasan_survey'; // controller
$idField  = 'id_mitra'; //field key table
?>

<script>
	var url;
	var app = "<?=$app?>";
	var appLink = '<?=$appLink?>';
	var module = '<?=$module?>';
	var idField = '<?=$idField?>';
	
    function add(){
        $('#dlg').dialog('open').dialog('setTitle','Tambah ' + app);
		$('#fm').form('clear');
	    $('#tbl').html('Save');
		url = '<?=base_url($module . '/' . $appLink . '/create')?>';
    }
    function edit(){
        var row = $('#dg').datagrid('getSelected');
		if (row){
			if(row.ceklis==null){
			$('#tbl').html('Simpan');
			$('#dlg').dialog('open').dialog('setTitle','Edit '+ app);
			$('#fm').form('load',row);
			url = '<?=base_url($module . '/' . $appLink . '/update')?>/'+row[idField];
		}else{
			$.messager.alert('Error Message','Data Sudah valid');
		}
	    }
    }
    function hapus(){
        var row = $('#dg').datagrid('getSelected');		
	    if (row){
			$.messager.confirm('Confirm','Apakah Anda Yakin Akan Menghapus ini?',function(r){
				if (r){
					$.post('<?=base_url($module . '/' . $appLink . '/delete')?>/'+row[idField],function(result){
						if (result.success){
							$('#dg').datagrid('reload');	// reload the user data
						} else {
							$.messager.show({	// show error message
								title: 'Error',
								msg:result.msg
							});
						}
					},'json');
				}
			});
		}
    }
    function save(){
	   $('#fm').form('submit',{
	    	url: url,
	    	onSubmit: function(){
	    		return $(this).form('validate');
	    	},
	    	success: function(result){
	    		var result = eval('('+result+')');
	    		if (result.success){
	    			$('#dlg').dialog('close');		
	    			$('#dg').datagrid('reload');
	                
	    		} else {
	             $.messager.alert('Error Message',result.msg,'error');
	    		}
	    	}
	    });
	}
	function doSearch(value){
	    $('#dg').datagrid('load',{    
	    	q:value  
	    });
	}
	
	// $(function(){
	// 	$('#tanggal_survey').datebox().datebox('calendar').calendar({
	// 		validator: function(date){
	// 			var now = new Date();
	// 			var d1 = new Date(now.getFullYear(), now.getMonth(), now.getDate());
	// 			return d1<=date;
	// 		}
	// 	});
	// });
	function downloadform(){
    	window.open('<?=base_url('assets/upload/formulir survey mitra.pdf')?>');
    }
// sumiyati 24-08-2020
	function preview(){
        var row = $('#dg').datagrid('getSelected');

        if (row)
        {
            url  = '<?=base_url($module . '/' . $appLink . '/permohonan')?>/'+row[idField];
            window.open(url,'_blank');
        }else{
            $.messager.alert('Data Mita','Minimal Pilih Satu Data','error');
        }
    }



	function ceklist(value,index,row){
		var icon = ""
		if (value == 1){
			icon = "icon-ok";
		}
		return "<span style='color: red'><i class="+icon+"></i></span>";
	}
	$(document).ready(function(){
		var x = '<?php echo $this->session->userdata('group_id');?>';
		if(x == 4){
			$('#delete').hide();
			$('#edit').hide();
		}else{
			$('#delete').show();
			$('#edit').show();
		}
	});
</script>
 
<div class="tabs-container">                
	<table id="dg" class="easyui-datagrid" 
	data-options="  
	    url: '<?=base_url($module . '/' . $appLink . '/read')?>',
		singleSelect:'true', 
	    title:'<?=$app?>',
	    toolbar:'#toolbar',
	    iconCls:'icon-cog',
	    rownumbers:'true',  
	    idField:'<?=$idField?>', 
	    pagination:'true',
	    fitColumns:'true',
	    pageList: [10,20,30]
	    "
	>
	    <thead>
	        <th field="id_mitra" width="120" sortable="true">ID Mitra</th>
	        <th field="nama" width="200" sortable="true">Nama Pemohon</th>
            <th field="nama_perusahaan" width="200" sortable="true">Nama Perusahaan</th>
            <th field="nilai_pengajuan" width="180" sortable="true" formatter="format_numberdisp" align="right">Nilai Pengajuan (RP)</th>
            <th field="status" width="200" sortable="true" hidden>Status</th>
            <th field="tanggal_survey" width="200" sortable="true">Tanggal Survey</th>
            <th field="petugas_survey" width="200" sortable="true">Petugas Survey</th>
            <th field="pemberi_tugas" width="200" sortable="true">Pemberi Tugas</th>
            <th field="ceklis" width="40" sortable="true" formatter="ceklist">CK</th>
	    </thead>
	</table>
	
	<!-- Model Start -->
	<div id="dlg" class="easyui-dialog" style="width:400px; height:200px; padding:10px" closed="true" buttons="#t_dlg_dis-buttons" >
	    <form id="fm" method="post" enctype="multipart/form-data" action="">
	        <table width="100%" align="center" border="0">
	            <tr>
	                <td>Mitra</td>
	                <td>:</td>
	                <td>
	                    <input name="id_mitra" id="id_mitra"  class="easyui-combogrid" required style="width: 200px"
						data-options="panelHeight:200, panelWidth: 700,
						valueField:'',
						idField:'id_mitra',
						textField:'nama',
						mode:'remote',
						url:'<?=base_url($module . '/' . $appLink . '/get_mitra') ?>',
						onSelect :function(indek,row)
						{},
						columns:[[
                       		{field:'id_mitra',title:'ID Mitra',width:80},
                        	{field:'nama',title:'Nama Pemohon',width:150},
                        	{field:'nama_perusahaan',title:'Nama Perusahaan',width:150},
                        	{field:'nilai_pengajuan',title:'Nilai',width:100},
                        	{field:'no_ktp',title:'KTP Pemohon',width:150},
                        	{field:'kota',title:'Kota',width:200}
                        ]]

						" >
	                </td>
	            </tr>
	            <tr>
	                <td>Tanggal</td>
	                <td>:</td>
	                <td>
	                    <input id="tanggal_survey" class="easyui-datebox" name="tanggal_survey" required type="text" style="width: 100px;">
	                </td>
	            </tr>
	            <tr>
	                <td>Petugas Survey</td>
	                <td>:</td>
	                <td>
	                    <input name="petugas_survey" id="petugas_survey"  class="easyui-combogrid" required style="width: 200px"
						data-options="panelHeight:200, panelWidth: 300,
						valueField:'',
						idField:'idPegawai',
						textField:'nama_pegawai',
						mode:'remote',
						url:'<?=base_url($module . '/' . $appLink . '/get_petugas_survey') ?>',
						onSelect :function(indek,row)
						{},
						columns:[[
                       		{field:'idPegawai',title:'ID Pegawai',width:80},
                        	{field:'nama_pegawai',title:'Nama Petugas',width:150}
                        ]]

						" >
	                </td>
	            </tr>
	        </table>
	    </form>
	   
	   	<!-- Tombol Add, Edit, Hapus Datagrid -->
       	<div id="toolbar">  
            <table align="center" style="padding: 0px; width: 99%;">
                <tr>
                    <td>
                       <!--  <a href="javascript:void(0)" class="btn btn-small btn-success" onclick="javascript:add()"><i class="icon-plus-sign icon-large"></i>&nbsp;Add</a> -->
                        <a id="edit" href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:edit()"><i class="icon-edit"></i>&nbsp;Edit</a>
                        <a id="delete" href="javascript:void(0)" class="btn btn-small btn-danger" onclick="javascript:hapus()"><i class="icon-trash icon-large"></i>&nbsp;Delete</a>
                      <!-- <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:downloadform()"><i class="icon-download"></i>&nbsp;Form Survey Mitra</a> -->
			<a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:preview()"><i class="icon-download"></i>&nbsp;Formulir Survey Mitra</a>
                    </td>
                    <td>&nbsp;</td>
                    <td align="right">
                    	<input class="easyui-searchbox" prompt="Silahkan Input Field" searcher="doSearch"  style="width:300px"/>
                    </td>
                </tr>
            </table>  
        </div>  
        <!-- end tombol datagrid -->
        <!-- Tombol simpan & Batal Form -->
        <div id="t_dlg_dis-buttons">
            <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:save()"><i class="icon-save icon-large"></i>&nbsp;Simpan</a>
            <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:$('#dlg').dialog('close')"><i class="icon-remove icon-large"></i>&nbsp;Batal</a>      
        </div>
	</div>
	<!-- Model end -->
</div>
