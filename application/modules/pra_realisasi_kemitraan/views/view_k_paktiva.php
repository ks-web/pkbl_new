<?php
$app11 = 'Posisi Aktiva'; // nama aplikasi
$module11 = 'pra_realisasi_kemitraan';
$appLink11 = 'K_paktiva'; // controller
$idField11  = 'id_paktiva'; //field key table
?>

<script>
	var url11;
	var app11 = "<?=$app11?>";
	var appLink11 = '<?=$appLink11?>';
	var module11 = '<?=$module11?>';
	var idField11 = '<?=$idField11?>';
	
	// function hit10(){
	// 	var harga=$("#fm11 #harga_satuan").numberbox('getValue');
	// 	var prod_akt=$("#fm11 #junit_akt").numberbox('getValue');
	// 	var prod_proy=$("#fm11 #junit_proy").numberbox('getValue');

	// 	var sumharga=harga*prod_akt;
	// 	$("#fm11 #biaya_akt").numberbox('setValue', sumharga);

	// 	var sumharga2=harga*prod_proy;
	// 	$("#fm11 #biaya_proy").numberbox('setValue', sumharga2);
	// // console.log(harga);
	// }
	function view_jenis_aktiva(value,row,index){

        var aktiva_data = [];
        aktiva_data["1"] = "Kas / Bank";
        aktiva_data["2"] = "Piutang";
        aktiva_data["3"] = "Persediaan";
        aktiva_data["4"] = "Hutang Jangka Pendek";
                      
        return aktiva_data[value];
    
	}	

    function add11(){
        $('#dlg11').dialog('open').dialog('setTitle','Tambah ' + app11);
		$('#fm11').form('clear');
	    $('#tbl11').html('Save11');
		url11 = '<?=base_url($module11 . '/' . $appLink11 . '/create')?>/'+idParent;
    }
    function edit11(){
        var row = $('#dg11').datagrid('getSelected');
		if (row){
			$('#tbl11').html('Simpan');
			$('#dlg11').dialog('open').dialog('setTitle','Edit '+ app11);
			$('#fm11').form('load',row);
			url11 = '<?=base_url($module11 . '/' . $appLink11 . '/update')?>/'+row[idField11];
	    }
    }
    function hapus11(){
        var row = $('#dg11').datagrid('getSelected');		
	    if (row){
			$.messager.confirm('Confirm','Apakah Anda Yakin Akan Menghapus ini?',function(r){
				if (r){
					$.post('<?=base_url($module11. '/' . $appLink11 . '/delete')?>/'+row[idField11],function(result){
						if (result.success){
							$('#dg11').datagrid('reload');	// reload the user data
							$('#dg11').datagrid('reload');
						} else {
							$.messager.show({	// show error message
								title: 'Error',
								msg:result.msg
							});
						}
					},'json');
				}
			});
		}
    }
    function save11(){
	   $('#fm11').form('submit',{
	    	url: url11,
	    	onSubmit: function(){
	    		return $(this).form('validate');
	    	},
	    	success: function(result){
	    		var result = eval('('+result+')');
	    		if (result.success){
	    			$('#dlg11').dialog('close');		
	    			$('#dg11').datagrid('reload');
	                
	    		} else {
	             $.messager.alert('Error Message',result.msg,'error');
	    		}
	    	}
	    });
	}
	function doSearch11(value){
	    $('#dg11').datagrid('load',{    
	    	q:value  
	    });
	}
	function reload11(value){
		$('#dg11').datagrid({    
	    	url: '<?=base_url($module11 . '/' . $appLink11 . '/read/')?>/'+idParent  
	    });
	}
</script>
 
<div class="tabs-container">                
	<table id="dg11" class="easyui-datagrid" 
	data-options="  
	    url: '<?=base_url($module11 . '/' . $appLink11 . '/read')?>',
		singleSelect:'true', 
	    title:'<?=$app11?>',
	    toolbar:'#toolbar11',
	    iconCls:'icon-cog',
	    rownumbers:'true',  
	    idField:'<?=$idField11?>', 
	    pagination:'true',
	    fitColumns:'true',
	    pageList: [10,20,30]
	    "
	>
	    <thead>
	    <tr>
	    	<th field="id_paktiva" hidden width="100" sortable="true">ID</th>
	        <th field="jenis_aktiva" formatter="view_jenis_aktiva" width="100" sortable="true">Jenis Aktiva	</th>
	        <th field="jumlah" width="100" sortable="true">Jumlah</th>
			<th field="satuan" width="100" sortable="true">Satuan</th>
			<th field="harga_satuan" width="100" sortable="true">Harga Satuan</th>
			<th field="total" width="100" sortable="true" >Total</th>



	    </thead>
	</table>
	
	<!-- Model Start -->
	<div id="dlg11" class="easyui-dialog" style="width:500px; height:auto; padding:10px" closed="true" buttons="#t_dlg_dis-buttons11" >
	    <form id="fm11" method="post" enctype="multipart/form-data" action="">
	    	
	        <table width="100%" align="center" border="0">
	            <tr>
	               <td>Jenis Aktiva</td>
	               <td>:</td>
	               <td>
	               		<select class="easyui-combobox" name="jenis_aktiva" id="jenis_aktiva"  style="width: 150px;">
	               		<option value="1">Kas /Bank</option>
				        <option value="2">Piutang</option>
				        <option value="3">Persediaan</option>
				        <option value="4">Hutang Jangka Pendek</option>
						</select>
					</td>
	            </tr>
	            <tr>
	               <td>Jumlah</td>
	               <td>:</td>
	               <td>
	               <input type="text" name="jumlah" id="jumlah"  style="width: 50px;">
	               </td>
	            </tr>
	           <tr>
	               <td>Satuan</td>
	               <td>:</td>
	               <td>
	               <input type="text" name="satuan" id="satuan"  style="width: 50px;">
	               </td>
	            </tr>
	           <tr>
	               <td>Harga Satuan</td>
	               <td>:</td>
	               <td><input class="easyui-numberbox" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%'" name="harga_satuan" id="harga_satuan"  style="width: 250px;">
	               </td>
	            </tr>
	             
	             <tr>
	               <td>Total</td>
	               <td>:</td>
	               <td><input class="easyui-numberbox" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%'" name="total" id="total" required  style="width: 250px;" >
	               </td>
	               </tr>		            
	        </table>
	    </form>
	   
	   	<!-- Tombol Add, Edit, Hapus Datagrid -->
       	<div id="toolbar11">  
            <table align="center" style="padding: 0px; width: 99%;">
                <tr>
                    <td>
                        <a href="javascript:void(0)" class="btn btn-small btn-success" onclick="javascript:add11()"><i class="icon-plus-sign icon-large"></i>&nbsp;Add</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:edit11()"><i class="icon-edit"></i>&nbsp;Edit</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-danger" onclick="javascript:hapus11()"><i class="icon-trash icon-large"></i>&nbsp;Delete</a>
                    </td>
                    <td>&nbsp;</td>
                    <td align="right">
                    	<input class="easyui-searchbox" prompt="Silahkan Input Field" searcher="doSearch11"  style="width:300px"/>
                    </td>
                </tr>
            </table>  
        </div>  
        <!-- end tombol datagrid -->
        <!-- Tombol simpan & Batal Form -->
        <div id="t_dlg_dis-buttons11">
            <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:save11()"><i class="icon-save icon-large"></i>&nbsp;Simpan</a>
            <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:$('#dlg11').dialog('close')"><i class="icon-remove icon-large"></i>&nbsp;Batal</a>      
        </div>
	</div>
	<!-- Model end -->
</div>