<?php
    date_default_timezone_set("Asia/Jakarta");
    $tanggal = date('d F Y');
    $tgl=$vendor['tanggal_cd'];
    // $kredit=$vendor['kredit'];
    $hari=date('d',strtotime($tgl));
    $bulan=date('m',strtotime($tgl));
    // echo $bulan;
    $year=date('Y',strtotime($tgl));
    $day = date('D', strtotime($tanggal));
    $dayList = array(
        'Sun' => 'Minggu',
        'Mon' => 'Senin',
        'Tue' => 'Selasa',
        'Wed' => 'Rabu',
        'Thu' => 'Kamis',
        'Fri' => 'Jumat',
        'Sat' => 'Sabtu'
    );
    $bulanList = array('01' => 'Januari', '02' => 'Februari', '03' => 'Maret', '04' => 'April', '05' => 'Mei', '06' => 'Juni', '07' => 'Juli', '08' => 'Augustus', '09' => 'September', '10' => 'October.', '11' => 'Nopember', '12' => 'Desember');
   

function kekata($x) {
        $x = abs($x);
        $angka = array("", "satu", "dua", "tiga", "empat", "lima",
        "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
        $temp = "";
        if ($x <12) {
            $temp = " ". $angka[$x];
        } else if ($x <20) {
            $temp = kekata($x - 10). " belas";
        } else if ($x <100) {
            $temp = kekata($x/10)." puluh". kekata($x % 10);
        } else if ($x <200) {
            $temp = " seratus" . kekata($x - 100);
        } else if ($x <1000) {
            $temp = kekata($x/100) . " ratus" . kekata($x % 100);
        } else if ($x <2000) {
            $temp = " seribu" . kekata($x - 1000);
        } else if ($x <1000000) {
            $temp = kekata($x/1000) . " ribu" . kekata($x % 1000);
        } else if ($x <1000000000) {
            $temp = kekata($x/1000000) . " juta" . kekata($x % 1000000);
        } else if ($x <1000000000000) {
            $temp = kekata($x/1000000000) . " milyar" . kekata(fmod($x,1000000000));
        } else if ($x <1000000000000000) {
            $temp = kekata($x/1000000000000) . " trilyun" . kekata(fmod($x,1000000000000));
        }     
            return $temp;
    }
    function terbilang($x, $style=4) {
        if($x<0) {
            $hasil = "minus ". trim(kekata($x));
        } else {
            $hasil = trim(kekata($x));
        }     
        switch ($style) {
            case 1:
                $hasil = strtoupper($hasil);
                break;
            case 2:
                $hasil = strtolower($hasil);
                break;
            case 3:
                $hasil = ucwords($hasil);
                break;
            default:
                $hasil = ucfirst($hasil);
                break;
        }     
        return $hasil;
    }
    $terbilang = number_format($vendor['kredit']);
    // $harga_penawaran_vendor = number_format($vendor['harga_penawaran_vendor']);
?>
<!DOCTYPE html>
<html>
<head>
	<title>Pecairan Dana</title>
	<style>

	
		div, .hr{

		    background-color: white;
		    width: 100%;
		    border: 0px solid black;
		    padding: 25px;
		    margin: 0px;
		    font-size: 10;
			padding-bottom:2px;
			padding-top: 0px;

		}
		hr{
		    background-color: white;
		    width: 100%;
		    border: 0px solid black;
		    padding: 0px;
		    margin: 0px;
		}
		h3, .title{
			text-align: center;
		}	
		table{
		    width: 100%;
		    border: 0px; solid black;
		    border-collapse: collapse;
		    padding: 5px;
		    /*padding-bottom: 5px;*/
		}
		table,td,tr{
		    border: 0px; solid black;
		    border-collapse: collapse;
		    padding: 2px;
			/*padding-bottom:2px;*/

		}	
					
	</style>
</head>
<body>
<div style="padding-top: 5px;padding-bottom: 0px;">
<table  style="padding-top: 0px;" width="100%" height="70%">
<thead>
	<tr>
		<td colspan="7">
			<center><!-- <img  src='<?=base_url('assets/images/headkti.png')?>' style="width:100%; height:100%;"> --></center>
		</td>
	</tr>
	<br><br>
	<tr>
		<td colspan="7" >
			&nbsp;
		</td>
	</tr>
	<tr>
		<td colspan="7" >
			&nbsp;
		</td>
	</tr>
	</thead>
 <tr><br><br><br><br>
		<td></td>
		<br><br><br><br><br><br><td colspan="6" style="text-align: right;">Cilegon, <?php echo $hari;?> &nbsp;<?php echo $bulanList[$bulan];?>&nbsp;<?php echo $year;?></td>
	</tr>
	<tr>
		<td style="text-align: left;">Nomor</td>
		<td colspan="6" style="text-align: left;">: <?php echo $vendor['id_mitra'];?></td>
	</tr>
	<tr>
		<td>Perihal</td>
		<td colspan="6">:<u><b> Transfer Uang Tunai</b></u></td>
	</tr>	
	<br><br><br><br><br><br>
	<tr>
		<td colspan="7" >
			&nbsp;
		</td>
	</tr>
	<tr>
		<td colspan="7" style="border-bottom: 0px;" >Kepada Yth.</td>

	</tr>
	<tr><td colspan="7">Nama bank alamat bank kota bank,<br>
	Cabang Cilegon Anyer<br>di Cilegon
		</td></tr>
	<tr>
		<td colspan="7" >
			&nbsp;
		</td>
	</tr>
	
		<tr>
			<td colspan="7">Dengan Hormat</td>
		</tr>
		<tr>
			<td colspan="7">Dengan ini kami mohon dari   Rekening <?php echo $vendor['uraian'];?> pada <br>Bank Saudara Nomor : no rek pindah bukukan</td>
		</tr>
		<tr style="padding:1px;">
			<td >Sejumlah</td>
			<td colspan="6" >: Rp <?php echo $terbilang;?></td>
		</tr>
		<tr>
			<td >Terbilang</td>
			<td colspan="6">: <u><?php echo terbilang($vendor['kredit'], $style="3"); ?> Rupiah</u></td>
		</tr>
		<tr>
			<td >Kepada</td>
			<td colspan="6">: <u><?php echo $vendor['nama'];?> </u> / <?php echo $vendor['nama_perusahaan'];?></td>
		</tr>
		
		<tr>
			<td>Alamat</td>
			<td colspan="6" >: <?php echo $vendor['alamat']; ?>,Kel.<?php echo $vendor['kelurahan'];?>, Kec.<?php echo $vendor['kecamatan'];?></td>
		</tr>
	
		<tr>
			<td >Bank</td>
			<td colspan="6" >: <?php echo $vendor['bank'];?></td>
		</tr>
		<tr>
			<td >Rekening</td>
			<td colspan="6" >: <?php echo $vendor['no_rek'];?></td>
		</tr>
		<tr>
			<td >Pembayaran</td>
			<td colspan="6" >: <?php echo $vendor['ket_pembayaran'];?></td>
		</tr>
		<tr>
			<td >SPK Nomor</td>
			<td colspan="6" >: <?php echo $vendor['no_kontrak'];?></td>
		</tr>
		<tr>
			<td >Vouver Nomor</td>
			<td colspan="6" >: <?php echo $vendor['id_cd'];?></td>
		</tr>
		<tr>
			<td >Keterangan</td>
			<td colspan="6" >: <?php echo $vendor['keterangan'];?></td>
		</tr>
		<tr>
		<td colspan="7" >
			&nbsp;
		</td>
	</tr>
		<tr>
			<td colspan="7" >Terimakasih Atas Perhatian Saudara.<br><br><br><br></td>
		</tr>
		
		<thead>
		
		<tr>
				
				<td colspan="3" style="font-family: font-size:14px;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;vertical-align:top;text-align:right;">
					<center>
					<br>Mengetahui / Menyetujui<br>Divisi Community Development<br><br><br><br><br></center>
					<center></center>
					<center><font style="text-decoration: underline;">Syarif Rahman</font><br>Manager
					</center>
				</td>
				
				
				<td style="font-family:font-size:14px;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;vertical-align:top;text-align:right;"></td>
				<td colspan="2" style="font-family:font-size:14px;padding:5px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;vertical-align:top;text-align:right;">
					<center>
					</strong>PT. Krakatau Steel (Persero) Tbk.<br>Direktorat SDM dan Pengembangan Usaha<br>Dinas Community Development Finance<br><br><br><br><br><font style="text-decoration: underline;">Muhammad Ajis</font><br>Superintendent		</center>
				</td>
			</tr>
			</thead> 
		
		</table>
	</div>
	</body>
</html>