<?php
$app4 = 'Biaya Bahan Baku'; // nama aplikasi
$module4 = 'pra_realisasi_kemitraan';
$appLink4 = 'K_bbaku'; // controller
$idField4  = 'id_bbbaku'; //field key table
?>

<script>
	var url4;
	var app4 = "<?=$app4?>";
	var appLink4 = '<?=$appLink4?>';
	var module4 = '<?=$module4?>';
	var idField4 = '<?=$idField4?>';
	
	function hit2(){
		var harga=$("#fm4 #harga_satuan").numberbox('getValue');
		var prod_akt=$("#fm4 #junit_akt").numberbox('getValue');
		var prod_proy=$("#fm4 #junit_proy").numberbox('getValue');

		var sumharga=harga*prod_akt;
		$("#fm4 #bprod_akt").numberbox('setValue', sumharga);

		var sumharga2=harga*prod_proy;
		$("#fm4 #bprod_proy").numberbox('setValue', sumharga2);
	// console.log(harga);
	}	

    function add4(){
        $('#dlg4').dialog('open').dialog('setTitle','Tambah ' + app4);
		$('#fm4').form('clear');
	    $('#tbl4').html('Save4');
		url4 = '<?=base_url($module4 . '/' . $appLink4 . '/create')?>/'+idParent;
    }
    function edit4(){
        var row = $('#dg4').datagrid('getSelected');
		if (row){
			$('#tbl4').html('Simpan');
			$('#dlg4').dialog('open').dialog('setTitle','Edit '+ app4);
			$('#fm4').form('load',row);
			url4 = '<?=base_url($module4 . '/' . $appLink4 . '/update')?>/'+row[idField4];
	    }
    }
    function hapus4(){
        var row = $('#dg4').datagrid('getSelected');		
	    if (row){
			$.messager.confirm('Confirm','Apakah Anda Yakin Akan Menghapus ini?',function(r){
				if (r){
					$.post('<?=base_url($module4. '/' . $appLink4 . '/delete')?>/'+row[idField4],function(result){
						if (result.success){
							$('#dg4').datagrid('reload');	// reload the user data
							$('#dg4').datagrid('reload');
						} else {
							$.messager.show({	// show error message
								title: 'Error',
								msg:result.msg
							});
						}
					},'json');
				}
			});
		}
    }
    function save4(){
	   $('#fm4').form('submit',{
	    	url: url4,
	    	onSubmit: function(){
	    		return $(this).form('validate');
	    	},
	    	success: function(result){
	    		var result = eval('('+result+')');
	    		if (result.success){
	    			$('#dlg4').dialog('close');		
	    			$('#dg4').datagrid('reload');
	                
	    		} else {
	             $.messager.alert('Error Message',result.msg,'error');
	    		}
	    	}
	    });
	}
	function doSearch4(value){
	    $('#dg4').datagrid('load',{    
	    	q:value  
	    });
	}
	function reload4(value){
		$('#dg4').datagrid({    
	    	url: '<?=base_url($module4 . '/' . $appLink4 . '/read/')?>/'+idParent  
	    });
	}
</script>
 
<div class="tabs-container">                
	<table id="dg4" class="easyui-datagrid" 
	data-options="  
	    url: '<?=base_url($module4 . '/' . $appLink4 . '/read')?>',
		singleSelect:'true', 
	    title:'<?=$app4?>',
	    toolbar:'#toolbar4',
	    iconCls:'icon-cog',
	    rownumbers:'true',  
	    idField:'<?=$idField4?>', 
	    pagination:'true',
	    fitColumns:'true',
	    pageList: [10,20,30]
	    "
	>
	    <thead>
	    <tr>
	    	<th rowspan="2" field="id_bbbaku" hidden width="100" sortable="true">ID</th>
	        <th rowspan="2" field="unsur_biaya" width="100" sortable="true">Unsur Biaya	</th>
			<th rowspan="2" field="satuan" width="100" sortable="true">Satuan</th>
			<th rowspan="2" field="harga_satuan" width="100" sortable="true">Harga Satuan</th>
			<th colspan="2"  width="100" sortable="true">Jumlah Unit</th>
			<th colspan="2"  width="100" sortable="true">Biaya Produksi</th>
		</tr>
			<th field="junit_akt" width="100" sortable="true">Akt</th>
			<th field="junit_proy" width="100" sortable="true">Proy</th>
			<th field="bprod_akt" width="100" sortable="true">Akt</th>
			<th field="bprod_proy" width="100" sortable="true">Proy</th>



	    </thead>
	</table>
	
	<!-- Model Start -->
	<div id="dlg4" class="easyui-dialog" style="width:500px; height:auto; padding:10px" closed="true" buttons="#t_dlg_dis-buttons4" >
	    <form id="fm4" method="post" enctype="multipart/form-data" action="">
	    	
	        <table width="100%" align="center" border="0">
	            <tr>
	               <td>Unsur Biaya</td>
	               <td>:</td>
	               <td>
	               <textarea name="unsur_biaya" id="unsur_biaya" required="required" style="width: 150px;"></textarea>
	               </td>
	            </tr>
	           <tr>
	               <td>Satuan</td>
	               <td>:</td>
	               <td>
	               <input style="width: 70px;" class="easyui-combobox" name="satuan" id="satuan" required data-options="
                                    url:'<?=base_url($module4 . '/' . $appLink4 . '/getsatuan')?>',
                                    method:'get',
                                    required:'true',
                                    valueField:'satuan',
                                    textField:'satuan',
                                    panelHeight:'auto'
                                ">
	               </td>
	            </tr>
	           <tr>
	               <td>Harga Satuan</td>
	               <td>:</td>
	               <td><input class="easyui-numberbox" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%', onChange: function(a,b){
	              hit2();}" name="harga_satuan" id="harga_satuan" required style="width: 250px;">
	               </td>
	            </tr>
	            <tr>
	               <td><b>Jumlah Unit</b></td>
	               <td>:</td>
	            <tr>
	            <tr>
	               <td style="text-align: right;">Akt</td>
	               <td></td>
	               <td>  &nbsp;<input class="easyui-numberbox"  name="junit_akt" id="junit_akt" required style="width: 50px;" data-options="onChange: function(a,b){
	              hit2();}" ></td>
	            </tr>
	            <tr>
	            	<td style="text-align: right;">Proy </td>
	            	<td></td>
	            	<td>&nbsp;<input class="easyui-numberbox"  name="junit_proy" id="junit_proy" required style="width: 50px;" data-options="onChange: function(a,b){
	              hit2();}" ></td>
	            </tr>
	            
	            <tr>
	               <td><b>Biaya Produksi</b></td>
	               <td>:</td>
	            </tr>
	            <tr>
	               <td style="text-align: right;">Akt</td>
	               <td></td>
	               <td>&nbsp;<input class="easyui-numberbox" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%'" name="bprod_akt" id="bprod_akt" required style="width: 250px;" readonly="true">
	               </td>
	            </tr>
	            <tr>
	               <td style="text-align: right;">Proy</td>
	               <td></td>
	               <td>&nbsp;<input class="easyui-numberbox" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%'" name="bprod_proy" id="bprod_proy" required style="width: 250px;" readonly="true"></td>
	            </tr>

	            
	        </table>
	    </form>
	   
	   	<!-- Tombol Add, Edit, Hapus Datagrid -->
       	<div id="toolbar4">  
            <table align="center" style="padding: 0px; width: 99%;">
                <tr>
                    <td>
                        <a href="javascript:void(0)" class="btn btn-small btn-success" onclick="javascript:add4()"><i class="icon-plus-sign icon-large"></i>&nbsp;Add</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:edit4()"><i class="icon-edit"></i>&nbsp;Edit</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-danger" onclick="javascript:hapus4()"><i class="icon-trash icon-large"></i>&nbsp;Delete</a>
                    </td>
                    <td>&nbsp;</td>
                    <td align="right">
                    	<input class="easyui-searchbox" prompt="Silahkan Input Field" searcher="doSearch4"  style="width:300px"/>
                    </td>
                </tr>
            </table>  
        </div>  
        <!-- end tombol datagrid -->
        <!-- Tombol simpan & Batal Form -->
        <div id="t_dlg_dis-buttons4">
            <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:save4()"><i class="icon-save icon-large"></i>&nbsp;Simpan</a>
            <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:$('#dlg4').dialog('close')"><i class="icon-remove icon-large"></i>&nbsp;Batal</a>      
        </div>
	</div>
	<!-- Model end -->
</div>