		<?php
		$url_img = url_img;
		$divisiTitle=divisiTitle;
		$sptTitle=sptTitle;
		?>
		<style>
			table {
				border-spacing: 0px;
				border-collapse: separate;
				font-size: 14px;
				page-break-inside: avoid;
			}

			table, th, td {
				border: 0px solid black;
			}
			html {
				margin: 50px 80px
			}
			.bordered, .bordered td {
				border: 1px solid black;
				font-size: 14px;
			}
			
			.bordered-catat{
				border: 1px solid black;
				font-size: 14px;
			}
			td.no_border_bot {
				border-top: 0px solid black;
				border-bottom: 0px solid black;
			}

			.pull-right {
				text-align: right;
			}
			.center {
				text-align: center;
			}

			.bold {
				font-weight: bold;
			}
			.corporate {
				color: red;
				font-size: 24px;
			}
			.corporate_sub {
				font-size: 12px;
			}
			.red {
				color: red;
			}
		</style>

		<table style="width: 100%;">
			<!-- <tbody> -->
				<tr>
					<td>
					<table style="width: 100%;">
						<tr>
							<td width="70px"><img  src="<?= $url_img.'assets/images/logo_KS.png'?>" style="width: 50%;"></td>
							<td><span class="corporate bold">PT. KRAKATAU STEEL</span>
							<br />
							<span class="corporate_sub bold"><?= strtoupper($divisiTitle);?></span></td>
							<td width="200px"></span>
							<br />
							</td>
						</tr>
					</table>
					<br />
					<br />
					<?php $asd = 10000;number_format($asd, 0,",",".");?>
					<table style="width: 100%;">
						<tr align="right">
							<td><?=$kota?>, <?=tanggal_display()?></td>
						</tr>
						<tr>
							<td>Nomor   : <?=$data['id_cd']?></td>
						</tr>
						<tr>
							<td>Perihal : <strong><u>Transfer Uang Tunai</u></strong></td>
						</tr>
					</table>
					<br />
					<br />
					<table style="width: 100%;">
						<tr>
							<td>
								Kepada Yth. :<br/>
								PT. Bank Mandiri (Persero) Tbk.<br/>
								Cabang Cilegon Anyer<br/>
								di Cilegon
							</td>
						</tr>
						<tr>
							<td>
								<br><br>Dengan Hormat,<br>
								<br>
								Dengan ini kami mohon agar dari rekening Pegelkop PT Krakatau Steel (Persero) Tbk. pada bank Saudara Nomor : 116-0091023789 dipindahbukukan :
							</td>
						</tr>
					</table>
					<br />
					<br />
					<table style="width: 100%;">
						<tr>
							<td valign="top">Sejumlah</td>
							<td valign="top">:</td>
							<td>Rp. <?=number_format($data['angsuran_pokok'], 0,",",".");?></td>
						</tr>
						<tr>
							<td valign="top">Terbilang</td>
							<td valign="top">:</td>
							<td><?=terbilang_display($data['angsuran_pokok'],3)?> Rupiah</td>
						</tr>
						<tr>
							<td valign="top">Kepada</td>
							<td valign="top">:</td>
							<td><?=$data['nama']?>/<?=$data['nama_perusahaan']?></td>
						</tr>
						<tr>
							<td valign="top">Alamat</td>
							<td valign="top">:</td>
							<td><?=$data['alamat']?>, Kel. <?=$data['kelurahan']?>, Kec. <?=$data['kecamatan']?>, <?=$data['kota']?></td>
						</tr>
						<tr>
							<td valign="top">Bank</td>
							<td valign="top">:</td>
							<td><?=$data['bank']?></td>
						</tr>
						<tr>
							<td valign="top">Rekening</td>
							<td valign="top">:</td>
							<td><?=$data['no_rek']?></td>
						</tr>
						<tr>
							<td valign="top">Pembayaran</td>
							<td valign="top">:</td>
							<td><?=$data['keterangan']?></td>
						</tr>
						<tr>
							<td valign="top">SPK Nomor</td>
							<td valign="top">:</td>
							<td><?=$data['id_mitra']?></td>
						</tr>
						<tr>
							<td valign="top">Voucher Nomor</td>
							<td valign="top">:</td>
							<td><?=$data['id_cd']?></td>
						</tr>
						<tr>
							<td valign="top">Keterangan</td>
							<td valign="top">:</td>
							<td>Biaya ditanggung Pegelkop PT Krakatau Steel</td>
						</tr>
					</table>
					<br />
					<br />
					<table style="width: 100%;">
						<tr>
							<td>
								Terima kasih atas perhatian Saudara.
							</td>
						</tr>
					</table>
					<br />
					<br />
					<br />
					<table style="width: 100%;">
						<tr>
							<td align="center" valign="bottom" style="font-size: 13px;">
								Mengetahui/Menyetujui,<br>
								<?=divisiTitle?>
							</td>
							<td align="center" valign="bottom"  style="font-size: 13px;">
								PT. Krakatau Steel (Perseor) Tbk.
								<br>Direktorat SDM
								<br>Dinas Community Development
							</td>
						</tr>
						<tr>
							<td><br><br><br><br><br></td>
							<td><br><br><br><br><br></td>
						</tr>
						<tr>
							<td align="center" ><strong><u><?=managerName?></u></strong></td>
							<td align="center" ><strong><u><?=sptTitle?></u></strong></td>
						</tr>
						<tr>
							<td align="center" >Manager</td>
							<td align="center" >Superintendent</td>
						</tr>
					</table>
					</td>
				</tr>
			<!-- </tbody> -->
		</table>

