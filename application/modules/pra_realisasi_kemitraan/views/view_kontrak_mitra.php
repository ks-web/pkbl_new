<?php
$app = 'Data Kontrak'; // nama aplikasi
$module = 'pra_realisasi_kemitraan';
$appLink = 'Kontrak_mitra'; // controller
$idField  = 'no_kontrak'; //field key table
$moduleref = 'vendormitra';
$appLinkref = 'datamitra'; // controller
?>

<script>
    $("#nama_perusahaan").hide();
	var url;
	var app = "<?=$app?>";
	var appLink = '<?=$appLink?>';
	var module = '<?=$module?>';
	var idField = '<?=$idField?>';
	function aksi(){
	   return a='Layak';
	}
    function add(){
        $("#nama_perusahaan").hide();
        $("#nama").show();
        $('#dlg').dialog('open').dialog('setTitle','Tambah ' + app);
		$('#fm').form('clear');
	    $('#tbl').html('Save');
		url = '<?=base_url($module . '/' . $appLink . '/create')?>';

        var url_get_mitra = '<?=base_url($module . '/' . $appLink . '/getmitra')?>';
        $('#id_mitra').combogrid({
        	mode:'remote',
        	url:url_get_mitra
        })
    }
    function edit(){
        
        var row = $('#dg').datagrid('getSelected');
		if (row){
		    $("#nama").hide();
            $("#nama_perusahaan").show();
			$('#tbl').html('Simpan');
			$('#dlg').dialog('open').dialog('setTitle','Edit '+ app);
			$('#fm').form('load',row);
			url = '<?=base_url($module . '/' . $appLink . '/update')?>/'+row[idField];
            
	    }
    }
    function hapus(){
        var row = $('#dg').datagrid('getSelected');		
	    if (row){
			$.messager.confirm('Confirm','Apakah Anda Yakin Akan Menghapus ini?',function(r){
				if (r){
					$.post('<?=base_url($module . '/' . $appLink . '/delete')?>/'+row.id_mitra,function(result){
						if (result.success){
							$('#dg').datagrid('reload');	// reload the user data
						} else {
							$.messager.show({	// show error message
								title: 'Error',
								msg:result.msg
							});
						}
					},'json');
				}
			});
		}
    }

    function preview(){
	    var row = $('#dg').datagrid('getSelected');
	    url  = '<?=base_url($module . '/' . $appLink . '/previewkontrak_mitra')?>/'+row[idField];
	    if (row)
	    {
	       //alert(row[idField]);
	        window.open(url,'_blank');
	    }
	}
	
	function preview_bast(){
	    var row = $('#dg').datagrid('getSelected');
	    url  = '<?=base_url($module . '/' . $appLink . '/preview_bast_jaminan')?>/'+row[idField];
	    if (row)
	    {
	        window.open(url,'_blank');
	    }
	}
	function cetak_jaminan(){
	    var row = $('#dg').datagrid('getSelected');
	    url  = '<?=base_url($module . '/' . $appLink . '/surat_jaminan')?>/'+row[idField];
	    if (row)
	    {
	        window.open(url,'_blank');
	    }
	}

    function save(){
	   $('#fm').form('submit',{
	    	url: url,
	    	onSubmit: function(){
	    		return $(this).form('validate');
	    	},
	    	success: function(result){
	    		var result = eval('('+result+')');
	    		if (result.success){
	    			$('#dlg').dialog('close');		
	    			$('#dg').datagrid('reload');
	                
	    		} else {
	             $.messager.alert('Error Message',result.msg,'error');
	    		}
	    	}
	    });
	}
	function doSearch(value){
	    $('#dg').datagrid('load',{    
	    	q:value  
	    });
	}
</script>
 
<div class="tabs-container">                
	<table id="dg" class="easyui-datagrid" 
	data-options="  
	    url: '<?=base_url($module . '/' . $appLink . '/read')?>',
		singleSelect:'true', 
	    title:'<?=$app?>',
	    toolbar:'#toolbar',
	    iconCls:'icon-cog',
	    rownumbers:'true',  
	    idField:'<?=$idField?>', 
	    pagination:'true',
	    fitColumns:'true',
	    pageList: [10,20,30],
	    nowrap:false
	    "
	>
	    <thead>
	        <th field="no_kontrak" width="70" sortable="true">No Kontrak</th>
	        <!--<th field="tanggal_create" width="100" sortable="true">Tanggal Daftar</th>-->
            <th field="tgl_kontrak" width="90" sortable="true">Tanggal Kontrak</th>
            <th field="nama" width="100" sortable="true">Nama Pemohon</th>
            <th field="nama_perusahaan" width="130" sortable="true">Nama Perusahaan</th>
            <th field="kota" width="80" sortable="true">Kota</th>
            <th field="keterangan" width="100" sortable="true">Kegiatan</th>
	    </thead>
	</table>
	
	<!-- Model Start -->
	<div id="dlg" class="easyui-dialog" style="width:600px; height:600px; padding:10px" closed="true" buttons="#t_dlg_dis-buttons" >
	    <form id="fm" method="post" enctype="multipart/form-data" action="">
	        <table width="100%" align="center" border="0">
                <tr style="height: 30px">
	                <td>Tanggal Kontrak</td>
	                <td>:</td>
	                <td>
	                    <input id="tgl_input" name="tgl_input"  required class="easyui-datebox" style="width:100px;" /></td>
	                </td>
	            </tr>
	            <tr style="height: 30px">
	                <td>Nama Mitra</td>
	                <td>:</td>
	                <td>
                    <select name="id_mitra" id="id_mitra" class="easyui-combogrid" style="width:300px;" data-options="
                        required:true,
                        panelWidth:500,
                        idField:'id_mitra',
                        textField:'nama_perusahaan',
                        columns:[[
                        {field:'id_mitra',title:'ID',width:80},
                        {field:'tanggal_input',title:'Tanggal Daftar',width:100},
                        {field:'nama',title:'Nama Pemohon',width:100},
                        {field:'nama_perusahaan',title:'Nama Prusahaan',width:130},
                        {field:'nilai_pengajuan',title:'Nilai Pengajuan',width:100},
                        {field:'kota',title:'Kota',width:100},
                        {field:'tanggal_survey',title:'Tanggal Survey',width:100}
                        ]],
                        onSelect: function(no,row){
                           $('#nama').val(row.nama);
                           $('#no_ktp').val(row.no_ktp);
                           $('#alamat').val('Kelurahan : '+row.kelurahan+' - Kec. '+row.kecamatan+' - Kab/kota. '+row.kota+' - Prov. '+row.propinsi+' - Alamat : '+row.alamat);

                           //$('#nilai_pengajuan').val('Rp. '+format_numberdisp(row.nilai_pengajuan));
                           $('#nilai_pengajuan').numberbox('setValue', row.nilai_pengajuan);
                           $('#nilai_rekomendasi_survey').numberbox('setValue', row.angsuran_pokok);
                           $('#nilai_jasa').numberbox('setValue', row.angsuran_bunga);
                           $('#nilai_total_pinjaman_jasa').numberbox('setValue', row.total_pinjaman_bunga);
                           // $('#nilai_rekomendasi_survey').val('Rp. '+format_numberdisp(row.nilai_rekomendasi_pengajuan));
                           // $('#nilai_disetujui').val('Rp. '+format_numberdisp(row.nilai_disetujui));
                           $('#nama_sdana').val(row.nama_sdana);
                           $('#sektor_usaha').val(row.sektor_usaha);
                           $('#rekomendasi_penilaian').val(row.rekomendasi_penilaian);
                           
                           $('#nama').val(row.nama);
                           $('#tanggal_survey').val(row.tanggal_survey);
                           
                        }
                        "></select>
	            </tr>
                <tr style="height: 30px">
	                <td>Nama Pemohon</td>
	                <td>:</td>
	                <td>
	                    <input type="text" name="nama" id="nama" required style="width: 250px;" readonly="true">
	                </td>
	            </tr>
                <tr style="height: 30px">
	                <td>Nomor KTP</td>
	                <td>:</td>
	                <td>
	                    <input type="text" name="no_ktp" id="no_ktp" required style="width: 250px;" readonly="true">
	                </td>
	            </tr>
                <tr style="height: 30px">
	                <td>Alamat</td>
	                <td>:</td>
	                <td>
	                    <textarea id="alamat" name="alamat" style="width: 300px; height:100px;" required readonly="true"></textarea>
	                </td>
	            </tr>
                <tr style="height: 30px">
	                <td>Nilai Pengajuan</td>
	                <td>:</td>
	                <td>
                        <input class="easyui-numberbox" name="nilai_pengajuan" id="nilai_pengajuan" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%',disabled:'true'" style="width: 255px;text-align: right">
	                </td>
	            </tr>

                <tr style="height: 30px">
	                <td>Nilai Rekomendasi</td>	
	                <td>:</td>
	                <td>
                        <input class="easyui-numberbox" name="nilai_rekomendasi_survey" id="nilai_rekomendasi_survey" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%',disabled:'true'" style="width: 255px;text-align: right">
	                </td>
	            </tr>
	            <tr style="height: 30px">
	                <td>Nilai Jasa</td>
	                <td>:</td>
	                <td>
                        <input class="easyui-numberbox" name="nilai_jasa" id="nilai_jasa" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%',disabled:'true'" style="width: 255px;text-align: right">
	                </td>
	            </tr>

                <tr style="height: 30px">
	                <td>Nilai Total Pinjaman dan Jasa</td>	
	                <td>:</td>
	                <td>
                        <input class="easyui-numberbox" name="nilai_total_pinjaman_jasa" id="nilai_total_pinjaman_jasa" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%',disabled:'true'" style="width: 255px;text-align: right">
	                </td>
	            </tr>
               <!--  <tr style="height: 30px">
	                <td>Nilai Yang Disetujui</td>
	                <td>:</td>
	                <td>
                        <input class="easyui-numberbox" name="nilai_disetujui" id="nilai_disetujui" style="width: 255px;text-align: 
                        right" disabled>
	                </td>
	            </tr>
                <tr style="height: 30px">
	                <td>Jenis Mitra</td>
	                <td>:</td>
	                <td>
	                    <input type="text" name="sektor_usaha" id="sektor_usaha" required style="width: 250px;" disabled>
	                </td>
	            </tr>
                <tr style="height: 30px">
	                <td>Sumber Dana</td>
	                <td>:</td>
	                <td>
	                    <input type="text" name="nama_sdana" id="nama_sdana" required style="width: 250px;" disabled>
	                </td>
	            </tr> -->
                <tr style="height: 30px">
	                <td>Rekomendasi Penilaian</td>
	                <td>:</td>
	                <td>
	                    <input type="text" name="rekomendasi_penilaian" id="rekomendasi_penilaian" required style="width: 250px;" disabled>
	                </td>
	            </tr>
	        </table>
	    </form>
	   
	   	<!-- Tombol Add, Edit, Hapus Datagrid -->
       	<div id="toolbar">  
            <table align="center" style="padding: 0px; width: 99%;">
                <tr>
                    <td>
                        <a href="javascript:void(0)" class="btn btn-small btn-success" onclick="javascript:add()"><i class="icon-plus-sign icon-large"></i>&nbsp;Add</a>
                        <!-- <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:edit()"><i class="icon-edit"></i>&nbsp;Edit</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-danger" onclick="javascript:hapus()"><i class="icon-trash icon-large"></i>&nbsp;Delete</a> -->
                        <a href="javascript:void(0)" class="btn btn-small btn-warning" iconCls: 'icon-search' onclick="javascript:preview()"><i class="icon-file icon-large"></i>&nbsp;Create Kontrak</a>
						<a href="javascript:void(0)" class="btn btn-small btn-warning" iconCls: 'icon-search' onclick="javascript:preview_bast()"><i class="icon-file icon-large"></i>&nbsp;BAST Jaminan</a>
						<a href="javascript:void(0)" class="btn btn-small btn-warning" iconCls: 'icon-print' onclick="javascript:cetak_jaminan()"><i class="icon-print icon-large"></i>&nbsp;Perjanjian Jaminan</a>
                    </td>
                    <td>&nbsp;</td>
                    <td align="right">
                    	<input class="easyui-searchbox" prompt="Silahkan Input Field" searcher="doSearch"  style="width:300px"/>
                    </td>
                </tr>
            </table>  
        </div>  
        <!-- end tombol datagrid -->
        <!-- Tombol simpan & Batal Form -->
        <div id="t_dlg_dis-buttons">
            <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:save()"><i class="icon-save icon-large"></i>&nbsp;Simpan</a>
            <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:$('#dlg').dialog('close')"><i class="icon-remove icon-large"></i>&nbsp;Batal</a>      
        </div>
	</div>
	<!-- Model end -->
</div>