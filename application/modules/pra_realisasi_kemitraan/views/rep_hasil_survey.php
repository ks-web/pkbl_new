<style>
	table {
		border-spacing: 0px;
		border-collapse: separate;
		font-size: 14px;
		page-break-inside: avoid;
	}

	table,
	th,
	td {
		border: 0px solid black;
	}

	html {
		margin: 10px 50px
	}

	.pull-right {
		text-align: right;
	}

	.center {
		text-align: center;
	}
</style>
<table style="width: 100%;">
	<tr>
		<td class="pull-right">No Reg : <?= $pengusaha['id_mitra'] ?></td>
	</tr>
	<tr>
		<td>
			<center>
				<h3 style="margin-bottom: 0; margin-top: 0;">Laporan Hasil Survey</h3>
				<div>
					<strong>Usaha Kecil</strong> : <?= $pengusaha['nama'] ?>, <strong>Sektor Usaha</strong> : <?= $pengusaha['nama_sektor'] ?>
					<br />
					<strong>Alamat</strong> : <?= $pengusaha['alamat'] ?>, <?= $pengusaha['kelurahan'] ?>, <?= $pengusaha['kecamatan'] ?>, <?= $pengusaha['kota'] ?>, <strong>Telp.</strong> <?= $pengusaha['telepon'] ?>
				</div>
			</center>
			<hr style="border: 1px solid black;" />
		</td>
	</tr>
</table>
<table style="width: 100%;">

	<br>
	<table>
		<tr>
			<td>
				<table style="width: 100%;">
					<table>
						<tr>
							<td width="15px"><strong>1.</strong></td>
							<td><strong>Data Penjualan Per Periode (<?= $pengusaha['periode'] ?> Hari)</strong></td>
							<td width="170px" class="center">Aktual Tahun Terakhir [Rp]</td>
							<td width="170px" class="center">Proyeksi Tahun Depan [Rp]</td>
						</tr>
						<tr>
							<td width="15px"></td>
							<td>A. Kapasitas Produksi [Unit]</td>
							<td width="170px"></td>
							<td width="170px"></td>
						</tr>
						<tr>
							<td width="15px"></td>
							<td>B. Hasil Penjualan</td>
							<td width="170px" class="pull-right"><?= number_format($pengusaha['1b_akt'], 0, ",", ".") ?></td>
							<td width="170px" class="pull-right"><?= number_format($pengusaha['1b_proy'], 0, ",", ".") ?></td>
						</tr>
						<tr>
							<td width="15px">&nbsp;</td>
							<td></td>
							<td width="170px"></td>
							<td width="170px"></td>
						</tr>
						<tr>
							<td width="15px"><strong>2.</strong></td>
							<td><strong>Kebutuhan Tambahan Investasi</strong></td>
							<td width="170px"></td>
							<td width="170px"></td>
						</tr>
						<tr>
							<td width="15px"><strong>3.</strong></td>
							<td><strong>Biaya Produksi per Periode (<?= $pengusaha['periode'] ?> Hari)</strong></td>
							<td width="170px"></td>
							<td width="170px"></td>
						</tr>
						<tr>
							<td width="15px"><strong></strong></td>
							<td><strong>A. Biaya Variable</strong></td>
							<td width="170px"></td>
							<td width="170px"></td>
						</tr>
						<tr>
							<td width="15px"><strong></strong></td>
							<td>&nbsp;&nbsp;&nbsp;&nbsp;1. Bahan Baku</td>
							<td width="170px" class="pull-right"><?= number_format($pengusaha['3a1_akt'], 0, ",", ".") ?></td>
							<td width="170px" class="pull-right"><?= number_format($pengusaha['3a1_proy'], 0, ",", ".") ?></td>
						</tr>
						<tr>
							<td width="15px"><strong></strong></td>
							<td>&nbsp;&nbsp;&nbsp;&nbsp;2. Bahan Pembantu</td>
							<td width="170px" class="pull-right"><?= number_format($pengusaha['3a2_akt'], 0, ",", ".") ?></td>
							<td width="170px" class="pull-right"><?= number_format($pengusaha['3a2_proy'], 0, ",", ".") ?></td>
						</tr>
						<tr>
							<td width="15px"><strong></strong></td>
							<td>&nbsp;&nbsp;&nbsp;&nbsp;3. Upah Tenaga Kerja</td>
							<td width="170px" class="pull-right"><?= number_format($pengusaha['3a3_akt'], 0, ",", ".") ?></td>
							<td width="170px" class="pull-right"><?= number_format($pengusaha['3a3_proy'], 0, ",", ".") ?></td>
						</tr>
						<tr>
							<td width="15px"><strong></strong></td>
							<td>&nbsp;&nbsp;&nbsp;&nbsp;4. Biaya Transportasi</td>
							<td width="170px" class="pull-right"><?= number_format($pengusaha['3a4_akt'], 0, ",", ".") ?></td>
							<td width="170px" class="pull-right"><?= number_format($pengusaha['3a4_proy'], 0, ",", ".") ?></td>
						</tr>
						<tr>
							<td width="15px"><strong></strong></td>
							<td>&nbsp;&nbsp;&nbsp;&nbsp;5. Biaya Lain-lain</td>
							<td width="170px" class="pull-right"><?= number_format($pengusaha['3a5_akt'], 0, ",", ".") ?></td>
							<td width="170px" class="pull-right"><?= number_format($pengusaha['3a5_proy'], 0, ",", ".") ?></td>
						</tr>
						<?php
						$total_3a_akt = $pengusaha['3a1_akt'] + $pengusaha['3a2_akt'] + $pengusaha['3a3_akt'] + $pengusaha['3a4_akt'] + $pengusaha['3a5_akt'];
						$total_3a_proy = $pengusaha['3a1_proy'] + $pengusaha['3a2_proy'] + $pengusaha['3a3_proy'] + $pengusaha['3a4_proy'] + $pengusaha['3a5_proy'];
						?>
						<tr>
							<td width="15px"><strong></strong></td>
							<td class="pull-right"><strong>Subtotal :</strong></td>
							<td width="170px" class="pull-right"><strong><?= number_format($total_3a_akt, 0, ",", ".") ?></strong></td>
							<td width="170px" class="pull-right"><strong><?= number_format($total_3a_proy, 0, ",", ".") ?></strong></td>
						</tr>
						<tr>
							<td width="15px"><strong></strong></td>
							<td><strong>B. Biaya Tetap</strong></td>
							<td width="170px"></td>
							<td width="170px"></td>
						</tr>
						<tr>
							<td width="15px"><strong></strong></td>
							<td>&nbsp;&nbsp;&nbsp;&nbsp;1. Gaji Pimpinan dan Pegawai Tetap</td>
							<td width="170px" class="pull-right"><?= number_format($pengusaha['3b1_akt'], 0, ",", ".") ?></td>
							<td width="170px" class="pull-right"><?= number_format($pengusaha['3b1_proy'], 0, ",", ".") ?></td>
						</tr>
						<tr>
							<td width="15px"><strong></strong></td>
							<td>&nbsp;&nbsp;&nbsp;&nbsp;2. Penyusutan (Depresiasi)</td>
							<td width="170px" class="pull-right"><?= number_format($pengusaha['3b2_akt'], 0, ",", ".") ?></td>
							<td width="170px" class="pull-right"><?= number_format($pengusaha['3b2_proy'], 0, ",", ".") ?></td>
						</tr>
						<tr>
							<td width="15px"><strong></strong></td>
							<td>&nbsp;&nbsp;&nbsp;&nbsp;3. Biaya Pemeliharaan</td>
							<td width="170px" class="pull-right"><?= number_format($pengusaha['3b3_akt'], 0, ",", ".") ?></td>
							<td width="170px" class="pull-right"><?= number_format($pengusaha['3b3_proy'], 0, ",", ".") ?></td>
						</tr>
						<tr>
							<td width="15px"><strong></strong></td>
							<td>&nbsp;&nbsp;&nbsp;&nbsp;4. Biaya Administrasi dan Umum</td>
							<td width="170px" class="pull-right"><?= number_format($pengusaha['3b4_akt'], 0, ",", ".") ?></td>
							<td width="170px" class="pull-right"><?= number_format($pengusaha['3b4_proy'], 0, ",", ".") ?></td>
						</tr>
						<?php
						$total_3b_akt = $pengusaha['3b1_akt'] + $pengusaha['3b2_akt'] + $pengusaha['3b3_akt'] + $pengusaha['3b4_akt'];
						$total_3b_proy = $pengusaha['3b1_proy'] + $pengusaha['3b2_proy'] + $pengusaha['3b3_proy'] + $pengusaha['3b4_proy'];
						?>
						<tr>
							<td width="15px"><strong></strong></td>
							<td class="pull-right"><strong>Subtotal :</strong></td>
							<td width="170px" class="pull-right"><strong><?= number_format($total_3b_akt, 0, ",", ".") ?></strong></td>
							<td width="170px" class="pull-right"><strong><?= number_format($total_3b_proy, 0, ",", ".") ?></strong></td>
						</tr>
						<?php
						$totbiayaprod_act = $total_3a_akt + $total_3b_akt;
						$totbiayaprod_proy = $total_3a_proy + $total_3b_proy;
						?>
						<tr>
							<td width="15px"><strong></strong></td>
							<td>&nbsp;&nbsp;&nbsp;&nbsp;<strong>Total Biaya Produksi</strong></td>
							<td width="170px" class="pull-right"><strong><?= number_format($totbiayaprod_act, 0, ",", ".") ?></strong></td>
							<td width="170px" class="pull-right"><strong><?= number_format($totbiayaprod_proy, 0, ",", ".") ?></strong></td>
						</tr>
						<?php
						$_4a1_proy = $totbiayaprod_proy - $totbiayaprod_act;
						$_4a2_proy = $pengusaha['4a2_proy'];
						$_4a3_proy = $pengusaha['4a3_proy'];
						$_4a4_proy = $pengusaha['4a4_proy'];
						?>
						<tr>
							<td width="15px"><strong>4.</strong></td>
							<td><strong>Kebutuhan Tambahan Modal Usaha</strong></td>
							<td width="170px"></td>
							<td width="170px"></td>
						</tr>
						<tr>
							<td width="15px"><strong></strong></td>
							<td><strong>A. Kebutuhan Tambahan Modal Kerja per Periode</strong></td>
							<td width="170px"></td>
							<td width="170px"></td>
						</tr>
						<tr>
							<td width="15px"><strong></strong></td>
							<td>&nbsp;&nbsp;&nbsp;&nbsp;1. Biaya Produksi (Proyeksi - Aktual)</td>
							<td width="170px" class="pull-right"></td>
							<td width="170px" class="pull-right"><?= number_format($_4a1_proy, 0, ",", ".") ?></td>
						</tr>
						<tr>
							<td width="15px"><strong></strong></td>
							<td>&nbsp;&nbsp;&nbsp;&nbsp;2. Hutang Jangka Pendek</td>
							<td width="170px" class="pull-right"></td>
							<td width="170px" class="pull-right"><?= number_format($_4a2_proy, 0, ",", ".") ?></td>
						</tr>
						<tr>
							<td width="15px"><strong></strong></td>
							<td>&nbsp;&nbsp;&nbsp;&nbsp;3. Piutang</td>
							<td width="170px" class="pull-right"></td>
							<td width="170px" class="pull-right"><?= number_format($_4a3_proy, 0, ",", ".") ?></td>
						</tr>
						<tr>
							<td width="15px"><strong></strong></td>
							<td>&nbsp;&nbsp;&nbsp;&nbsp;4. Cadangan Kas</td>
							<td width="170px" class="pull-right"></td>
							<td width="170px" class="pull-right"><?= number_format($_4a4_proy, 0, ",", ".") ?></td>
						</tr>
						<?php
						$tot_4a = ($_4a1_proy + $_4a2_proy) - ($_4a3_proy + $_4a4_proy);
						?>
						<tr>
							<td width="15px"><strong></strong></td>
							<td class="pull-right"><strong>Subtotal :</strong></td>
							<td width="170px" class="pull-right"><strong></strong></td>
							<td width="170px" class="pull-right"><strong><?= number_format($tot_4a, 0, ",", ".") ?></strong></td>
						</tr>
						<?php
						$_4b_proy = 0;
						$_4b_total_act = $tot_4a;
						$pembulantan = 100000;

						$_4b_total_proy = $_4b_total_act - ($_4b_total_act % $pembulantan);
						$_4c_proy = $pengusaha['1b_akt'] - $totbiayaprod_act;
						$_4d_proy = $pengusaha['4d_proy'];
						// $_4e_proy = ($_4c_proy*12)+$pengusaha['4d_proy'];
						// $_4e_proy = $pengusaha['1b_proy'] * 33 / 360
						?>
						<tr>
							<td width="15px"><strong></strong></td>
							<td><strong>B. Kebutuhan Tambahan Investasi</strong></td>
							<td width="170px"></td>
							<td width="170px" class="pull-right"></td>
						</tr>
						<tr>
							<td width="15px"><strong></strong></td>
							<td><strong>Total Kebutuhan Tambahan Modal Usaha :</strong></td>
							<td width="170px" class="pull-right"><?= number_format($_4b_total_act, 0, ",", ".") ?> , dibulatkan</td>
							<td width="170px" class="pull-right"><?= number_format($_4b_total_proy, 0, ",", ".") ?></td>
						</tr>
						<tr>
							<td width="15px"><strong></strong></td>
							<td><strong>C. Keuntungan Aktual per Bulan</strong></td>
							<td width="170px"></td>
							<td width="170px" class="pull-right"><?= number_format($_4c_proy, 0, ",", ".") ?></td>
						</tr>
						<tr>
							<td width="15px"><strong></strong></td>
							<td><strong>D. Total Asset yang dimiliki</strong></td>
							<td width="170px"></td>
							<td width="170px" class="pull-right"><?= number_format($_4d_proy, 0, ",", ".") ?></td>
						</tr>
						<tr>
							<td width="15px"><strong></strong></td>
							<td><strong>E. Omset per Tahun</strong></td>
							<td width="170px"></td>
							<td width="170px" class="pull-right"><?= number_format($pengusaha['4e_proy'], 0, ",", ".") ?></td>
						</tr>
					</table>
					<br><br>
				</table>
				<br><br>
				<hr style="border: 1px solid black;" />
				<br>
				<br>
				<table style="width: 100%;">
					<table style="width: 100%;">
						<tr>
							<td width="15px"><strong></strong></td>
							<td><strong></strong></td>
							<td width="170px" class="center"></td>
							<td width="170px" class="center"><strong><?= $kota ?>, <?= tanggal_display($pengusaha['tanggal_create']) ?></strong></td>
						</tr>
						<tr>
							<td width="15px"><strong></strong></td>
							<td><strong></strong></td>
							<td width="170px" class="center"></td>
							<td width="170px" class="center">Disiapkan Oleh:</td>
						</tr>
						<?php
						if ($_4d_proy >= 500000000 || $pengusaha['4e_proy'] >= 2500000000) { ?>
							<tr>
								<td colspan="2">
									<strong>Keterangan : DITOLAK</strong>
								</td>

								<td width="170px" class="center">
								</td>
								<td width="170px" class="center"><br /><br /></td>
							</tr>
						<?php }
						?>
						<tr>
							<td colspan="2">
								<strong></strong>
							</td>

							<td width="170px" class="center">
							</td>
							<td width="170px" class="center"><br /><br /></td>

						</tr>
						<tr>
							<td><strong></strong></td>
							<td><strong></strong></td>
							<td width="170px" class="center"></td>
							<td width="170px" class="center"><strong><u><?= $pengusaha['nama_pegawai'] ?></u></strong></td>
						</tr>
						<tr>
							<td><strong></strong></td>
							<td><strong></strong></td>
							<td width="170px" class="center"></td>
							<td width="170px" class="center">Surveyor</td>
						</tr>
					</table>
				</table>
			</td>
		</tr>
	</table>
</table>