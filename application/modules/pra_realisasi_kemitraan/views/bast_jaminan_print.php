<?php
  date_default_timezone_set("Asia/Jakarta");
    $tanggal = date('d F Y');
    //$tgl=$vendor['tgl_berdiri'];
    $tgl    = $kontrak_mitra['tanggal_input'];
    $hari   = date('d',strtotime($tgl));
    $bulan  = date('m');
    // echo $bulan;
    $year   = date('Y',strtotime($tgl));
    $day    = date('D', strtotime($tanggal));
    $dayList = array(
        'Sun' => 'Minggu',
        'Mon' => 'Senin',
        'Tue' => 'Selasa',
        'Wed' => 'Rabu',
        'Thu' => 'Kamis',
        'Fri' => 'Jumat',
        'Sat' => 'Sabtu'
    );
    $bulanList = array('01' => 'Januari', '02' => 'Februari', '03' => 'Maret', '04' => 'April', '05' => 'Mei', '06' => 'Juni', '07' => 'Juli', '08' => 'Augustus', '09' => 'September', '10' => 'October.', '11' => 'Nopember', '12' => 'Desember');
    $datapemohon=$kontrak_mitra['alamat']." Dalam Hal Ini Diwakili Oleh <strong>".$kontrak_mitra['nama']."</strong> Nomor KTP ".$kontrak_mitra['no_ktp']."
     Lahir Di Lebak Tanggal 6 Pebruari 1980, alamat rumah di ".$kontrak_mitra['alamat']." dalam hal ini bertindak atas nama pimpinan <strong>".$kontrak_mitra['nama']."</strong>
     yang bergerak pada sektor ".$kontrak_mitra['nama_perusahaan'].", dari dan oleh sebab itu bertindak untuk dan atas nama usaha kecil ".$kontrak_mitra['nama_perusahaan']." selanjutnya disebut <strong>PIHAK KEDUA</strong>";

    if($kontrak_mitra['status_jaminan']=='1'){
		$atasnama=$kontrak_mitra['pemilik_jaminan'];
	}else{
		$atasnama=$kontrak_mitra['nama'];
		}
    
?>

	<title>BAST Jaminan Mitra<br>PT Krakatau Steel Tbk</title>
	<style>

	.judul 
	{
		font-family: Times;
		font-size: 28px;
		font-style: normal;
		font-weight: bold;
		text-decoration: underline;
		text-transform: none;
		color: #000000;
		background-color: #ffffff;
	}
		
	.dibawah_judul
	{
		font-family: Arial;
		font-size: 16px;
		font-style: normal;
		font-weight: normal;
		text-decoration: none;
		text-transform: none;
		color: #000000;
		background-color: #ffffff;
	}
	
	.teks
	{
		font-family: Arial;
		font-size: 14px;
		font-style: normal;
		font-weight: normal;
		text-decoration: none;
		text-transform: none;
		color: #000000;
		background-color: #ffffff;
	}
	
	u {    
    border-bottom: 1px dotted #000;
    text-decoration: none;
}
		div, .hr{

		    background-color: white;
		    /*width: 100%;*/
		    border: 0px solid black;
		    padding: 25px;
		    margin: 1px;
		    font-size: 10;
			padding-bottom:2px;
			padding-top: 0px;

		}
		hr{
		    background-color: white;
		    /*width: 100%;*/
		    border: 0px solid black;
		    padding: 0px;
		    margin: 0px;
		}
		h3, .title{
			text-align: center;
		}	
		table{
		    /*width: 100%;*/
		    border: 0px solid black;
		    border-collapse: collapse;
		    padding: 5px;
		    /*padding-bottom: 5px;*/
		}
		table,td,tr{
		    border: 0px solid black;
		    border-collapse: collapse;
		    padding: 1px;
			font-family: 'Times New Roman', Times, serif;
			/*padding-bottom:2px;*/

		}	
					
	</style>

<center><img  src="<?= url_img.'assets/images/headkti.png'?>" style="width:20%; height:20%;"><center><br><br>
<div style="padding-top: 5px;padding-bottom: 0px;">
    <table style=" padding-top: 0px;" width="100%" height:40%>
        <tr>
            <td align="center" colspan="2" style="border-top:1px;border-bottom: 1px; padding-left: 5px;vertical-align: justify;padding-top: 5px;"><div class="judul">BERITA ACARA SERAH TERIMA JAMINAN</div></td>
        </tr>
        <tr>
            <td align="center" colspan="2" style="border-top:1px;border-bottom: 1px; padding-left: 5px;vertical-align: justify;padding-top: 5px;"><div class="dibawah_judul">SESUAI DENGAN SURAT PERJANJIAN KERJA SAMA ANTARA</div></td>
        </tr>
        <tr>
            <td align="center" colspan="2" style="border-top:1px;border-bottom: 1px; padding-left: 5px;vertical-align: justify;padding-top: 5px;"><strong>USAHA KECIL <?php echo $kontrak_mitra['nama_perusahaan'];?></strong></td>
        </tr>
        <tr>
            <td align="center" colspan="2" style="border-top:1px;border-bottom: 1px; padding-left: 5px;vertical-align: justify;padding-top: 5px;"><strong>DENGAN PT KRAKATAU STEEL (PERSERO) Tbk. TENTANG PINJAMAN MODAL USAHA</strong></td>
        </tr>
        <tr>
            <td align="center" colspan="2" style="border-top:1px;border-bottom: 1px; padding-left: 5px;vertical-align: justify;padding-top: 5px;"><br><strong>NOMOR KONTRAK : <?php echo $kontrak_mitra['no_kontrak'];?><!--/MB/--><?php //echo date("Y"); ?></strong><br><br><br></td>
        </tr>
        <tr>
            <td colspan="2" style="border-top:1px;border-bottom: 1px; padding-left: 5px;vertical-align: justify;padding-top: 5px;">Pada hari ini tanggal <?php echo date('d')?>, bulan <?php echo $bulanList[$bulan];?>, tahun <?php echo $year; ?> yang bertanda tangan dibawah ini</td>
        </tr>
        
        <tr>
			<td align="center" colspan="2" style="border-top:1px;border-bottom: 1px; padding-left: 5px;vertical-align: justify;padding-top: 5px;">
				<br>
				<table class="tg" style="undefined;table-layout: fixed; width: 322px">
					<colgroup>
						<col style="width: 23.2px">
						<col style="width: 153.2px">
						<col style="width: 21.2px">
						<col style="width: 124.2px">
					</colgroup>
					<tr>
						<td class="tg-031e">1.</th>
						<td class="tg-yw4l">Nama</th>
						<td class="tg-lqy6">:</th>
						<td class="tg-yw4l"><strong><?php echo $kontrak_mitra['nama'];?></strong></th>
					</tr>
					<tr>
						<td class="tg-yw4l"></td>
						<td class="tg-yw4l">Alamat</td>
						<td class="tg-lqy6">:</td>
						<td class="tg-yw4l"><?php echo $kontrak_mitra['alamat'];?></td>
					</tr>
					<tr>
						<td class="tg-yw4l"></td>
						<td class="tg-yw4l">Jabatan</td>
						<td class="tg-lqy6">:</td>
						<td class="tg-yw4l">Pimpinan Usaha Kecil <?php echo $kontrak_mitra['nama_perusahaan'] ?><br>Selanjutnya disebut PIHAK KEDUA</td>
					</tr>
					<tr>
						<td class="tg-yw4l"><br></td>
						<td class="tg-yw4l"></td>
						<td class="tg-yw4l"></td>
						<td class="tg-yw4l"></td>
					</tr>
					<tr>
						<td class="tg-yw4l">2.</td>
						<td class="tg-yw4l">Nama</td>
						<td class="tg-lqy6">:</td>
						<td class="tg-yw4l"><strong><?=managerName?></strong></td>
					</tr>
					<tr>
						<td class="tg-yw4l"></td>
						<td class="tg-yw4l">Alamat</td>
						<td class="tg-lqy6">:</td>
						<td class="tg-yw4l">PT Krakatau Steel (Persero) - Banten</td>
					</tr>
					<tr>
						<td class="tg-yw4l"></td>
						<td class="tg-yw4l">Jabatan</td>
						<td class="tg-lqy6">:</td>
						<td class="tg-yw4l"><?=managerTitle?><br>Selanjutnya disebut PIHAK KEDUA</td>
					</tr>
				</table>
				<br><br>
			</td>
		</tr>
		
		 <tr>
            <td colspan="2" style="border-top:1px;border-bottom: 1px; padding-left: 5px;vertical-align: justify;padding-top: 5px;">
				Sesuai dengan Surat Perjanjian Kerja sama yang ditandatangani oleh kedua belah pihak dan surat pernyataan jaminan PIHAK PERTAMA, maka dengan ini KEDUA BELAH PIHAK sepakat untuk melaksanakan serah terima jaminan sebagai berikut :
			</td>
        </tr>
		
		<tr>
            <td colspan="2" style="border-top:1px;border-bottom: 1px; padding-left: 5px;vertical-align: justify;padding-top: 5px;">
				PIHAK PERTAMA menyerahkan jaminan berupa <?php echo $kontrak_mitra['nama_jaminan'] ?> atas nama <?php echo $atasnama; ?>
			</td>
        </tr>
		
		<tr>
            <td colspan="2" style="border-top:1px;border-bottom: 1px; padding-left: 5px;vertical-align: justify;padding-top: 5px;">
				PIHAK KEDUA menyatakan menerima jaminan dengan baik dari PIHAK PERTAMA dan apabila PIHAK PERTAMA tidak menepati janjinya sesuai dengan Surat Perjanjian Kerja sama yang telah dibuat (tidak dapat melunasi pinjamannya), maka PIHAK KEDUA berhak untuk menjual asset / barang yang dijaminkan.
			</td>
        </tr>
		
		<tr>
            <td colspan="2" style="border-top:1px;border-bottom: 1px; padding-left: 5px;vertical-align: justify;padding-top: 5px;">
				Demikian Berita Acara ini dibuat rangkap 3 (tiga), satu bermaterai cukup untuk dapat dipergunakan sebagaimana mestinya.
			</td>
        </tr>
		
		<tr>
				<td  style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;vertical-align:top;text-align:right;">
					<center>
					<br>PIHAK KEDUA<br>Yang Menerima<br><?=divisiTitle?><br><br><br><br><br><font style="text-decoration: underline;"><?=managerName?></font><br>Manager
					</center>
				</td>
				<td  style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;vertical-align:top;text-align:right;">
					<center>
					<br>PIHAK PERTAMA<br>Yang Menyerahkan<br><br><br><br><br><br><font style="text-decoration: underline;"><?php echo $kontrak_mitra['nama'] ?></font><br>Pimpinan Usaha Kecil
					</center>
				</td>
			</tr>
		<tr>
            <td colspan="2" style="border-top:1px;border-bottom: 1px; padding-left: 5px;vertical-align: justify;padding-top: 5px;">
				<br><br>Catatan :<br>Apabila pinjaman sudah lunas, Dokumen Jaminan<br>agar diambil dengan membawa Berita Acara ini.
			</td>
        </tr>
		
    </table>
	<br>
</div>
