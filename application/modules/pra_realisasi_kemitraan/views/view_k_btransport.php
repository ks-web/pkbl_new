<?php
$app8 = 'Biaya Transport'; // nama aplikasi
$module8 = 'pra_realisasi_kemitraan';
$appLink8= 'K_btransport'; // controller
$idField8  = 'id_btransport'; //field key table
?>

<script>
	var url8;
	var app8 = "<?=$app8?>";
	var appLink8 = '<?=$appLink8?>';
	var module8 = '<?=$module8?>';
	var idField8 = '<?=$idField8?>';
	
	function hit4(){
		var harga=$("#fm8 #harga_satuan").numberbox('getValue');
		var prod_akt=$("#fm8 #unit_akt").numberbox('getValue');
		var prod_proy=$("#fm8 #unit_proy").numberbox('getValue');

		var sumharga=harga*prod_akt;
		$("#fm8 #produksi_akt").numberbox('setValue', sumharga);

		var sumharga2=harga*prod_proy;
		$("#fm8 #produksi_proy").numberbox('setValue', sumharga2);
	// console.log(harga);
	}	

    function add8(){
        $('#dlg8').dialog('open').dialog('setTitle','Tambah ' + app8);
		$('#fm8').form('clear');
	    $('#tbl8').html('Save8');
		url8 = '<?=base_url($module8 . '/' . $appLink8 . '/create')?>/'+idParent;
    }
    function edit8(){
        var row = $('#dg8').datagrid('getSelected');
		if (row){
			$('#tbl8').html('Simpan8');
			$('#dlg8').dialog('open').dialog('setTitle','Edit '+ app8);
			$('#fm8').form('load',row);
			url8 = '<?=base_url($module8 . '/' . $appLink8 . '/update')?>/'+row[idField8];
	    }
    }
    function hapus8(){
        var row = $('#dg8').datagrid('getSelected');		
	    if (row){
			$.messager.confirm('Confirm','Apakah Anda Yakin Akan Menghapus ini?',function(r){
				if (r){
					$.post('<?=base_url($module8 . '/' . $appLink8 . '/delete')?>/'+row[idField8],function(result){
						if (result.success){
							$('#dg8').datagrid('reload');	// reload the user data
							$('#dg8').datagrid('reload');
						} else {
							$.messager.show({	// show error message
								title: 'Error',
								msg:result.msg
							});
						}
					},'json');
				}
			});
		}
    }
    function save8(){
	   $('#fm8').form('submit',{
	    	url: url8,
	    	onSubmit: function(){
	    		return $(this).form('validate');
	    	},
	    	success: function(result){
	    		var result = eval('('+result+')');
	    		if (result.success){
	    			$('#dlg8').dialog('close');		
	    			$('#dg8').datagrid('reload');
	                
	    		} else {
	             $.messager.alert('Error Message',result.msg,'error');
	    		}
	    	}
	    });
	}
	function doSearch8(value){
	    $('#dg8').datagrid('load',{    
	    	q:value  
	    });
	}
	function reload8(value){
		$('#dg8').datagrid({    
	    	url: '<?=base_url($module8 . '/' . $appLink8 . '/read/')?>/'+idParent  
	    });
	}
</script>
 
<div class="tabs-container">                
	<table id="dg8" class="easyui-datagrid" 
	data-options="  
	    url: '<?=base_url($module8 . '/' . $appLink8 . '/read')?>',
		singleSelect:'true', 
	    title:'<?=$app8?>',
	    toolbar:'#toolbar8',
	    iconCls:'icon-cog',
	    rownumbers:'true',  
	    idField:'<?=$idField8?>', 
	    pagination:'true',
	    fitColumns:'true',
	    pageList: [10,20,30]
	    "
	>
	    <thead>
	    <tr>
	    	<th rowspan="2" field="id_btransport" hidden width="100" sortable="true">ID</th>
	        <th rowspan="2" field="uraian" width="200" sortable="true">Unsur Biaya	</th>
			<th rowspan="2" field="satuan" width="100" sortable="true">Satuan</th>
			<th rowspan="2" field="harga_satuan" width="100" sortable="true">Harga Satuan</th>
			<th colspan="2"  width="100" sortable="true">Jumlah Unit</th>
			<th colspan="2"  width="100" sortable="true">Biaya Produksi</th>
		</tr>
			<th field="unit_akt" width="100" sortable="true">Akt</th>
			<th field="unit_proy" width="100" sortable="true">Proy</th>
			<th field="produksi_akt" width="100" sortable="true">akt</th>
			<th field="produksi_proy" width="100" sortable="true">Proy</th>



	    </thead>
	</table>
	
	<!-- Model Start -->
	<div id="dlg8" class="easyui-dialog" style="width:500px; height:auto; padding:10px" closed="true" buttons="#t_dlg_dis-buttons8" >
	    <form id="fm8" method="post" enctype="multipart/form-data" action="">
	        <table width="100%" align="center" border="0">
	            <tr>
	               <td>Unsur Biaya</td>
	               <td>:</td>
	               <td>
	               <textarea type="text" name="uraian" id="uraian" required="required" style="width: 150px;"></textarea>
	               </td>
	            </tr>
	           <tr>
	               <td>Satuan</td>
	               <td>:</td>
	               <td>
	               <input style="width: 70px;" class="easyui-combobox" name="satuan" id="satuan" required data-options="
                                    url:'<?=base_url($module8 . '/' . $appLink8 . '/getsatuan')?>',
                                    method:'get',
                                    required:'true',
                                    valueField:'satuan',
                                    textField:'satuan',
                                    panelHeight:'auto'
                                ">
	               </td>
	            </tr>
	           <tr>
	               <td>Harga Satuan</td>
	               <td>:</td>
	               <td><input class="easyui-numberbox" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%', onChange: function(a,b){
	              hit4();}" name="harga_satuan" id="harga_satuan" required style="width: 250px;">
	               </td>
	            </tr>
	            <tr>
	               <td><b>Jumlah Unit</b></td>
	               <td></td>
	               <td></td>
	            </tr>
	            <tr>
	               <td style="text-align: right;">Aktif </td>
	               <td></td>
	               <td>&nbsp;<input class="easyui-numberbox"  name="unit_akt" id="unit_akt" required style="width: 50px;" data-options="onChange: function(a,b){
	              hit4();}" >
	               </td>
	            </tr>
	            <tr>
	                <td style="text-align: right;">Proy</td>
	                <td></td>
	                <td> &nbsp;<input class="easyui-numberbox"  name="unit_proy" id="unit_proy" required style="width: 50px;" data-options="onChange: function(a,b){
	              hit4();}" ></td>
	            </tr>
	            <tr>
	               <td><b>Biaya Produksi</b></td>
	               <td></td>
	               <td></td>
	            <tr>
	               <td style="text-align: right;">Aktif</td>
	               <td></td>
	               <td>&nbsp;<input class="easyui-numberbox" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%'" name="produksi_akt" id="produksi_akt" required style="width: 250px;" readonly="true">
	               </td>
	            </tr>
	            <tr>
	                <td style="text-align: right;">Proy</td>
	                <td></td>
	                <td>&nbsp;<input class="easyui-numberbox" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%'" name="produksi_proy" id="produksi_proy" required style="width: 250px;" readonly="true"></td>
	            </tr>

	            
	        </table>
	    </form>
	   
	   	<!-- Tombol Add, Edit, Hapus Datagrid -->
       	<div id="toolbar8">  
            <table align="center" style="padding: 0px; width: 99%;">
                <tr>
                    <td>
                        <a href="javascript:void(0)" class="btn btn-small btn-success" onclick="javascript:add8()"><i class="icon-plus-sign icon-large"></i>&nbsp;Add</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:edit8()"><i class="icon-edit"></i>&nbsp;Edit</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-danger" onclick="javascript:hapus8()"><i class="icon-trash icon-large"></i>&nbsp;Delete</a>
                    </td>
                    <td>&nbsp;</td>
                    <td align="right">
                    	<input class="easyui-searchbox" prompt="Silahkan Input Field" searcher="doSearch8"  style="width:300px"/>
                    </td>
                </tr>
            </table>  
        </div>  
        <!-- end tombol datagrid -->
        <!-- Tombol simpan & Batal Form -->
        <div id="t_dlg_dis-buttons8">
            <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:save8()"><i class="icon-save icon-large"></i>&nbsp;Simpan</a>
            <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:$('#dlg8').dialog('close')"><i class="icon-remove icon-large"></i>&nbsp;Batal</a>      
        </div>
	</div>
	<!-- Model end -->
</div>