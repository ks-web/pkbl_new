<?php
	$no			= 1;
	$bulan  	= date('m');
	$bulanList 	= array('01' => 'Januari', '02' => 'Februari', '03' => 'Maret', '04' => 'April', '05' => 'Mei', '06' => 'Juni', '07' => 'Juli', '08' => 'Augustus', '09' => 'September', '10' => 'October.', '11' => 'Nopember', '12' => 'Desember');

$tittleManager = managerTitle;
$namemanager = managerName;
$url_img = url_img;
$divisi = divisiTitle;
$sptTitle= sptTitle;
?>

	<title>Report Pengukuran Nilai Jaminan</title>
	<style type="text/css">
		.judul {font-weight:bold;color:#000000;letter-spacing:0pt;word-spacing:2pt;font-size:26px;text-align:left;font-family:palatino linotype, palatino, serif;line-height:1;}
		.teks_atas
		{
			font-size:10px;
		}
		.tg  
		{
			border-collapse:collapse;
		}
		.tg td
		{
			font-family:Arial, sans-serif;
			font-size:14px;
			padding:10px 5px;
			border-style:solid;
			border-width:1px;
			overflow:hidden;
			word-break:normal;
		}
		.tg th
		{
			font-family:Arial, sans-serif;
			font-size:14px;
			font-weight:normal;
			padding:10px 5px;
			border-style:solid;
			border-width:1px;
			overflow:hidden;
			word-break:normal;
		}
		.tg .tg-3j8g
		{
			font-weight:bold;
			font-size:10px;
			text-align:center;
			vertical-align:middle;
		}
		
	</style>

	<font class="teks_atas">
		PT. Krakatau Steel (Persero) Tbk.<br>
		<?=$divisi;?>.<br>
	</font>
	<font class="judul">
		Pengukuran Nilai Jaminan Modal Usaha Kecil
	</font>
	<br><br><br>
	
	<div>
		<table>
			<tr>
				<td>
					Usaha Kecil 
				</td>
				<td>
					: <?php echo $data['nama'] ?>
				</td>
				<td>
					
				</td>
			</tr>
			<tr>
				<td>
					Sektor Usaha
				</td>
				<td>
					: Sektor usaha <?php echo $data['nama_sektor'] ?>
				</td>
			</tr>
			<tr>
				<td>
					Alamat
				</td>
				<td>
					: <?php echo $data['alamat'] ?>
				</td>
			</tr>
			<tr>
				<td>
					Bentuk Jaminan
				</td>
				<td>
					: <?php echo $data['nama_jaminan'] ?>
				</td>
			</tr>
		</table>
		
		<table class="tg">
			<tr>
				<th class="tg-3j8g" rowspan="3">No</th>
				<th class="tg-3j8g" rowspan="3">No. Reg</th>
				<th class="tg-3j8g" rowspan="3">Keterangan</th>
				<th class="tg-3j8g" rowspan="2">Luas Tanah <br>Bangunan M2</th>
				<th class="tg-3j8g" colspan="3">Harga Pasar BTN</th>
				<th class="tg-3j8g" colspan="3">Harga Pasar Kelurahan</th>
				<th class="tg-3j8g" colspan="4">Harga NJOP</th>
			</tr>
			<tr>
				<td class="tg-3j8g">Harga M2</td>
				<td class="tg-3j8g">Nilai</td>
				<td class="tg-3j8g">Pinjaman Maksimum</td>
				<td class="tg-3j8g">Harga M2</td>
				<td class="tg-3j8g">Nilai</td>
				<td class="tg-3j8g">Pinjaman Maksimum</td>
				<td class="tg-3j8g">NJOP</td>
				<td class="tg-3j8g">Harga M2</td>
				<td class="tg-3j8g">Nilai</td>
				<td class="tg-3j8g">Pinjaman Maksimum</td>
			</tr>
			<tr>
				<td class="tg-3j8g">A</td>
				<td class="tg-3j8g">B</td>
				<td class="tg-3j8g">C = A X B</td>
				<td class="tg-3j8g">D = C * 100 / 130%</td>
				<td class="tg-3j8g">E</td>
				<td class="tg-3j8g">F = A X E</td>
				<td class="tg-3j8g">G = F / 100 X 80%</td>
				<td class="tg-3j8g">H</td>
				<td class="tg-3j8g">I1 = (A X H1) X 3 I2 = (A X H2) X 50%</td>
				<td class="tg-3j8g">J = I1 + I2</td>
				<td class="tg-3j8g">K = I X 100 / 130</td>
			</tr>
			<tr>
				<td class="tg-3j8g"><?php echo $no++ ?></td>
				<td class="tg-3j8g"><?php echo $data['id_mitra'] ?> </td>
				<td class="tg-3j8g"><?php echo $data['keterangan'] ?></td>
				<td class="tg-3j8g"><?php echo $data['luas_tanah'] ?></td>
				<td class="tg-3j8g">Rp. <?php echo number_format($data['btn_harga_tanah'],0,',','.') ?></td>
				<td class="tg-3j8g">Rp. <?php echo number_format($data['btn_nilai'],0,',','.') ?></td>
				<td class="tg-3j8g">Rp. <?php echo number_format($data['btn_max'],0,',','.') ?></td>
				<td class="tg-3j8g">Rp. <?php echo number_format($data['kelurahan_harga_bangunan'],0,',','.') ?></td>
				<td class="tg-3j8g">Rp. <?php echo number_format($data['kelurahan_nilai'],0,',','.') ?></td>
				<td class="tg-3j8g">Rp. <?php echo number_format($data['kelurahan_max'],0,',','.') ?></td>
				<td class="tg-3j8g">Rp. <?php echo number_format($data['njop_harga_tanah'],0,',','.') ?></td>
				<td class="tg-3j8g">Rp. <?php echo number_format($data['njop_x3_tanah'],0,',','.') ?></td>
				<td class="tg-3j8g">Rp. <?php echo number_format($data['njop_nilai'],0,',','.') ?></td>
				<td class="tg-3j8g">Rp. <?php echo number_format($data['njop_max'],0,',','.') ?></td>
			</tr>
		</table>
		<br><br><br>
		
		<table border="0" width="40%">
			<tr>
				<td >Besaran Usulan Pinjaman</td>
				<td style="text-align:right"> : </td>
				<td style="text-align:right">Rp. <?php echo number_format($data['usulan_pinjaman'],0,',','.') ?></td>
			</tr>
			<tr>
				<td >Pengukuran Nilai Total Jaminan</td>
				<td style="text-align:right"> : </td>
				<td style="text-align:right">Rp. <?php echo number_format($data['total_jaminan'],0,',','.') ?></td>
			</tr >
			<tr>
				<td >Maksimal Pinjaman</td>
				<td style="text-align:right"> : </td>
				<td style="text-align:right">Rp. <?php echo number_format($data['max_pinjaman'],0,',','.') ?></td>
			</tr>
			<tr>
				<td >Rekomendasi</td>
				<td style="text-align:right"> : </td>
				<td style="text-align:right; font-weight:bold"><?php echo $data['rekomendasi'] ?></td>
			</tr>
		</table>
		
		<br><br><br><br>
		
		<table border="0" width="100%">
			<tr>
				<td width="35%" style="text-align:center">
					Menyetujui,<br>
					Dinas Community Development
					<br><br><br><br>
					<font style="text-decoration: underline;"><?=$sptTitle?></font><br>
					<?=sptJabatan?>
					
					
				</td>
				<td width="30%"></td>
				<td width="35%" style="text-align:center">
					Cilegon, <?php date("d") .' '.$bulanList[$bulan] .' '. date("Y") ?><br>
					Disiapkan Oleh
					<br><br><br><br>
					<font style="text-decoration: underline;"><?php echo $data['petugas_survey'];?></font><br>
					
				</td>
			</tr>
			
		</table>
	</div>
