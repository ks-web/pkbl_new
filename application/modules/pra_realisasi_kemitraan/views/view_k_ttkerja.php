<?php
$app6 = 'Biaya Tenaga Kerja Tetap'; // nama aplikasi
$module6 = 'pra_realisasi_kemitraan';
$appLink6 = 'K_ttkerja'; // controller
$idField6  = 'id_ttkerja'; //field key table
?>

<script>
	var url6;
	var app6 = "<?=$app6?>";
	var appLink6 = '<?=$appLink6?>';
	var module6 = '<?=$module6?>';
	var idField6 = '<?=$idField6?>';

	function getperiode(){
		$.get('<?=base_url($module6 . '/' . $appLink6 . '/getperiode')?>/'+idParent,function(result){
			console.log(result[0].jumlah_hari);
			$('#periode').numberbox('setValue',result[0].jumlah_hari);

						if (result.success){
							// $('#dg6').datagrid('reload');	// reload the user data
							// $('#dg6').datagrid('reload');
						} else {
							// $.messager.show({	// show error message
							// 	title: 'Error',
							// 	msg:result.msg
							// });
						}
					});
	}
	
    function add6(){
        $('#dlg6').dialog('open').dialog('setTitle','Tambah ' + app6);
		$('#fm6').form('clear');
	    $('#tbl6').html('save6');
	    getperiode();

		url6 = '<?=base_url($module6 . '/' . $appLink6 . '/create')?>/'+idParent;
    }
    function edit6(){
        var row = $('#dg6').datagrid('getSelected');
		if (row){
			$('#tbl6').html('Simpan');
			$('#dlg6').dialog('open').dialog('setTitle','Edit '+ app6);
			$('#fm6').form('load',row);
			getperiode();
			url6 = '<?=base_url($module6 . '/' . $appLink6 . '/update')?>/'+row[idField6];
	    }
    }
    function hapus6(){
        var row = $('#dg6').datagrid('getSelected');		
	    if (row){
			$.messager.confirm('Confirm','Apakah Anda Yakin Akan Menghapus ini?',function(r){
				if (r){
					$.post('<?=base_url($module6 . '/' . $appLink6 . '/delete')?>/'+row[idField6],function(result){
						if (result.success){
							$('#dg6').datagrid('reload');	// reload the user data
							$('#dg6').datagrid('reload');
						} else {
							$.messager.show({	// show error message
								title: 'Error',
								msg:result.msg
							});
						}
					},'json');
				}
			});
		}
    }
    function save6(){
	   $('#fm6').form('submit',{
	    	url: url6,
	    	onSubmit: function(){
	    		return $(this).form('validate');
	    	},
	    	success: function(result){
	    		var result = eval('('+result+')');
	    		if (result.success){
	    			$('#dlg6').dialog('close');		
	    			$('#dg6').datagrid('reload');
	                
	    		} else {
	             $.messager.alert('Error Message',result.msg,'error');
	    		}
	    	}
	    });
	}
	function doSearch6(value){
	    $('#dg6').datagrid('load',{    
	    	q:value  
	    });
	}
	function reload6(value){
		$('#dg6').datagrid({    
	    	url: '<?=base_url($module6 . '/' . $appLink6 . '/read/')?>/'+idParent  
	    });
	}

	function calculate6(){
		var total;
		// url: '<?=base_url($module6 . '/' . $appLink6 . '/read/')?>/'+idParent  
		total = parseInt($("#fm6 #unit").numberbox('getValue')) * parseInt($("#fm6 #upah_perbulan").numberbox('getValue'));
		$('#fm6 #jumlah_upah').numberbox('setValue', total);
		upah_periode=total*(parseInt($("#fm6 #periode").numberbox('getValue'))/30);
		$('#fm6 #upah_periode').numberbox('setValue',upah_periode);

	}
</script>
 
<div class="tabs-container">                
	<table id="dg6" class="easyui-datagrid" 
	data-options="  
	    url: '<?=base_url($module6 . '/' . $appLink6 . '/read')?>',
		singleSelect:'true', 
	    title:'<?=$app6?>',
	    toolbar:'#toolbar6',
	    iconCls:'icon-cog',
	    rownumbers:'true',  
	    idField:'<?=$idField6?>', 
	    pagination:'true',
	    fitColumns:'true',
	    pageList: [10,20,30]
	    "
	>
	    <thead>
	    	<th field="id_ttkerja" width="100" hidden sortable="true">ID</th>
	        <th field="bagian" width="100" sortable="true">Bagian</th>
			<th field="unit" width="100" sortable="true">Unit</th>
			<th field="upah_perbulan" width="100" sortable="true">Upah Perbulan</th>
			<th field="jumlah_upah" width="100" sortable="true">Jumlah Upah</th>
			<th field="upah_periode" width="100" sortable="true">Jumlah Upah Periode</th>											

	    </thead>
	</table>
	
	<!-- Model Start -->
	<div id="dlg6" class="easyui-dialog" style="width:500px; height:auto; padding:10px" closed="true" buttons="#t_dlg_dis-buttons6" >
	    <form id="fm6" method="post" enctype="multipart/form-data" action="">
	        <table width="100%" align="center" border="0">
	            <tr>
	                <td>Bagian</td>
	                <td>:</td>
	                <td><input type="text" name="bagian" id="bagian" required style="width: 100px;">
	                </td>
	            </tr>
	            <tr>
	                <td>Unit</td>
	                <td>:</td>
	                <td>
	                    <input class="easyui-numberbox" data-options="label:'',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'',width:'100%',onChange: function(a, b){ calculate6(); }" name="unit" id="unit" required style="width: 50px;">
	                </td>
	            </tr>
	            <tr>
	                <td>Upah Perbulan</td>
	                <td>:</td>
	                <td>
	                    <input class="easyui-numberbox" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%',onChange: function(a, b){ calculate6(); }" name="upah_perbulan" id="upah_perbulan" required style="width: 250px;">
	                </td>
	            </tr>
	             <tr>
	                <td>Jumlah Upah</td>
	                <td>:</td>
	                <td>
	                    <input class="easyui-numberbox" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%'" name="jumlah_upah" id="jumlah_upah" required style="width: 250px;" readonly="readonly">
	                </td>
	            </tr>	
	            <tr>
	            	<td>Periode</td>
	            	 <td>:</td>
	                <td>
	                    <input class="easyui-numberbox" name="periode" id="periode" required style="width: 50px;" readonly="readonly">
	                </td>
	            </tr>
	            <tr>
	            	<td>Jumlah Upah Periode</td>
	            	 <td>:</td>
	                <td>
	                    <input  name="upah_periode" id="upah_periode" class="easyui-numberbox" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%'" style="width: 250px;" readonly="readonly">
	                </td>
	            </tr>	            
	        </table>
	    </form>
	   
	   	<!-- Tombol Add, Edit, Hapus Datagrid -->
       	<div id="toolbar6">  
            <table align="center" style="padding: 0px; width: 99%;">
                <tr>
                    <td>
                        <a href="javascript:void(0)" class="btn btn-small btn-success" onclick="javascript:add6()"><i class="icon-plus-sign icon-large"></i>&nbsp;Add</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:edit6()"><i class="icon-edit"></i>&nbsp;Edit</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-danger" onclick="javascript:hapus6()"><i class="icon-trash icon-large"></i>&nbsp;Delete</a>
                    </td>
                    <td>&nbsp;</td>
                    <td align="right">
                    	<input class="easyui-searchbox" prompt="Silahkan Input Field" searcher="doSearch6"  style="width:300px"/>
                    </td>
                </tr>
            </table>  
        </div>  
        <!-- end tombol datagrid -->
        <!-- Tombol simpan & Batal Form -->
        <div id="t_dlg_dis-buttons6">
            <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:save6()"><i class="icon-save icon-large"></i>&nbsp;Simpan</a>
            <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:$('#dlg6').dialog('close')"><i class="icon-remove icon-large"></i>&nbsp;Batal</a>      
        </div>
	</div>
	<!-- Model end -->
</div>