<?php
  date_default_timezone_set("Asia/Jakarta");
    
?>

    <title>Kontrak Kerjasama Bina Lingkungan<br>PT Krakatau Steel Tbk</title>
    <style>

    u {    
    border-bottom: 1px dotted #000;
    text-decoration: none;
}
        div, .hr{

            background-color: white;
            /*width: 100%;*/
            border: 0px solid black;
            padding: 25px;
            margin: 1px;
            font-size: 10;
            padding-bottom:2px;
            padding-top: 0px;

        }
        hr{
            background-color: white;
            /*width: 100%;*/
            border: 0px solid black;
            padding: 0px;
            margin: 0px;
        }
        h3, .title{
            text-align: center;
        }   
        .border {
            /*width: 100%;*/
            border: 1px solid black;
            border-collapse: collapse;
            padding: 5px;
            /*padding-bottom: 5px;*/
        }   
    </style>

<?php //print_r($account); ?>
<div style="padding-top: 5px;padding-bottom: 0px;" align="right">
    <table style=" padding-top: 0px;" border="0"  width="100%">
        <tr >
            <td align="center" colspan="4" style="padding: 10px;"><b>CASH DISHBURSEMENT</b></td>
        </tr>
        <tr >
            <td align="center" colspan="4" style="padding: 10px;"><b>No. <?php echo $pencairan[0]['id_cd'];?></b></td>
        </tr>
        <tr>
            <td style="padding: 5px;">Tanggal Posting</td><td>: <?php echo $pencairan[0]['tgl_posting'];?></td>
        </tr>
        <tr>
            <td style="padding: 5px;">Tanggal CD</td><td>: <?php echo $pencairan[0]['tanggal_cd'];?></td>
        </tr>
        <tr>
            <td style="padding: 5px;">Nomor Register</td><td >: <?php echo $pencairan[0]['id_mitra'];?></td>
        </tr>
        <tr>
            <td style="padding: 5px;">Nomor Kontrak</td><td >: <?php echo $pencairan[0]['no_kontrak'];?></td>
        </tr>
        <tr>
            <td style="padding: 5px;">Kepada</td><td >: <b><?php echo $pencairan[0]['nama_perusahaan'];?></b></td>
        </tr>
        <tr>
            <td></td><td  >: <?php echo $pencairan[0]['alamat'];?></td>
        </tr>
        <tr>
            <td style="padding: 5px;">Jumlah</td><td  >: <b>Rp <?php echo number_format($pencairan[0]['nilai_disetujui']);?></b></td>
        </tr>
        <tr>
            <td style="padding: 5px;">Terbilang</td><td  >: <?php echo ucwords(Terbilang($pencairan[0]['nilai_disetujui']));?> Rupiah</td>
        </tr>
        <tr>
            <td style="padding: 5px;">Untuk Pembayaran</td><td  >: <?php echo $pencairan[0]['keterangan'];?></td>
        </tr>
    </table><br /><br />
    <table class="border" border="1" width="100%">
        <tr align="center">
            <td rowspan="2">Keterangan Jurnal</td>
            <td rowspan="2">Account</td>
            <td colspan="2">Jumlah</td>
        </tr>
        <tr align="center">
            <td>Debet</td><td>Credit </td>
        </tr>
		
		<?php
		if (count($account==0)){$b=$preview;}else{$b=$account;}
		?>
        <?php $debet = 0; $credit = 0; foreach($b as $rows){ ?>
            <tr>
                <td style='padding: 5px;'><?php echo $rows['nama_akun']; ?></td>
                <td><?php echo $rows['account_no']; ?></td>
                <td align="right"><?php echo number_format($rows['debet']); ?></td>
                <td align="right"><?php echo number_format($rows['kredit']); ?></td>
            </tr>
        <?php $debet = $debet + $rows['debet']; $credit = $credit + $rows['kredit']; } ?>
        <tr>
            <td style="padding: 5px;"></td><td style="padding: 5px;"><b>TOTAL</b></td><td align="right"><?php echo number_format($debet); ?></td><td align="right"><?php echo number_format($credit); ?></td>
        </tr>
		
		
       
    </table>
    <table class="border" style=" padding-top: 0px;" border="1"  width="100%">
     <tr align="center">
        <td style="padding: 5px;">Disiapkan</td><td>Diperiksa</td><td>Disetujui</td><td>Dibayar</td><td>Penerima</td>
    </tr>
    <br>
    <tr align="center">
        <!--<td><br /><br /><br />()</td><td><br /><br /><br />(<?php echo $sign['Diperiksa'];?>)</td><td><br /><br /><br />(<?php echo $sign['Disetujui'];?>)</td><td><br /><br /><br />(<?php echo $sign['Dibayarkan'];?>)</td><td><br /><br /><br />(<?php echo $pencairan[0]['nama'];?>)</td>-->
        <td><br /><br /><br /></td><td><br /><br /><br /></td><td><br /><br /><br /></td><td><br /><br /><br /></td><td><br /><br /><br /></td>
     </tr>
    </table>
    
</div>

<?php
function Terbilang($x)
{
  $abil = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
  if ($x < 12)
    return " " . $abil[$x];
  elseif ($x < 20)
    return Terbilang($x - 10) . "belas";
  elseif ($x < 100)
    return Terbilang($x / 10) . " puluh" . Terbilang($x % 10);
  elseif ($x < 200)
    return " seratus" . Terbilang($x - 100);
  elseif ($x < 1000)
    return Terbilang($x / 100) . " ratus" . Terbilang($x % 100);
  elseif ($x < 2000)
    return " seribu" . Terbilang($x - 1000);
  elseif ($x < 1000000)
    return Terbilang($x / 1000) . " ribu" . Terbilang($x % 1000);
  elseif ($x < 1000000000)
    return Terbilang($x / 1000000) . " juta" . Terbilang($x % 1000000);
}

function tanggal_indo($tanggal)
{
    $bulan = array (1 =>   'Januari',
                'Februari',
                'Maret',
                'April',
                'Mei',
                'Juni',
                'Juli',
                'Agustus',
                'September',
                'Oktober',
                'November',
                'Desember'
            );
    $split = explode('-', $tanggal);
    return $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];
}
?>