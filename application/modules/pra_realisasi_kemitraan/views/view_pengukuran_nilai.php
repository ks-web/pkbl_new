<?php
$app 		= 'Pengukuran Nilai'; // nama aplikasi
$module 	= 'pra_realisasi_kemitraan';
$appLink 	= 'pengukuran_nilai'; // controller
$idField  	= 'id_p_survey'; //field key table
$nodeMenu
?>

<script>
	var url;
	var app = "<?= $app ?>";
	var appLink = '<?= $appLink ?>';
	var module = '<?= $module ?>';
	var idField = '<?= $idField ?>';

	function add() {
		$('#dlg').dialog('open').dialog('setTitle', 'Tambah ' + app);
		$('#input_jaminan').hide();
		//$('#dlg').dialog({height: 440,width: 600});
		$('#jamin').hide();
		$('#fm').form('clear');
		$('#tbl').html('Save');
		url = '<?= base_url($module . '/' . $appLink . '/create') ?>';
	}



	function edit() {
		var row = $('#dg-pengukuran_nilai').datagrid('getSelected');
		// $('#input_jaminan').hide();
		//$('#dlg').dialog({height: 440,width: 600});
		// $('#jamin').hide();
		if (row) {
			$('#tbl').html('Simpan');
			$('#dlg').dialog('open').dialog('setTitle', 'Edit ' + app);
			$('#fm').form('load', row);
			url = '<?= base_url($module . '/' . $appLink . '/update') ?>/' + row[idField];
		}
	}

	function hapus() {
		var row = $('#dg-pengukuran_nilai').datagrid('getSelected');
		if (row) {
			$.messager.confirm('Confirm', 'Apakah Anda Yakin Akan Menghapus ini?', function(r) {
				if (r) {
					$.post('<?= base_url($module . '/' . $appLink . '/delete') ?>/' + row.id_tnilai, function(result) {
						if (result.success) {
							$('#dg').datagrid('reload'); // reload the user data
						} else {
							$.messager.show({ // show error message
								title: 'Error',
								msg: result.msg
							});
						}
					}, 'json');
				}
			});
		}
	}

	function save() {
		$('#fm').form('submit', {
			url: url,
			onSubmit: function() {
				return $(this).form('validate');
			},
			success: function(result) {
				var result = eval('(' + result + ')');
				if (result.success) {
					$('#dlg').dialog('close');
					var g = $('#id_mitra').combogrid('grid'); // get datagrid object
					var r = g.datagrid('reload'); // get the selected row	
					$('#dg').datagrid('reload');

				} else {
					$.messager.alert('Error Message', result.msg, 'error');
				}
			}
		});
	}

	function doSearch(value) {
		$('#dg').datagrid('load', {
			q: value
		});
	}

	/*
	$(function(){
		$('#tanggal_survey').datebox().datebox('calendar').calendar({
			validator: function(date){
				var now = new Date();
				var d1 = new Date(now.getFullYear(), now.getMonth(), now.getDate());
				return d1<=date;
			}
		});
	});
	*/


	function hitung_pengukuran() {
		if ($('#luas_tanah').val() == '') {
			$.messager.alert('Error Message', 'Luas tanah belum diisi', 'error');
		} else {
			/*
				A2 = luas_bangunan  (jika jaminan = sertifikat tanah dan bangunan
				A = luas_tanah, B = btn_harga, C = btn_nilai, D = btn_max
								E = kelurahan_harga, F = kelurahan_nilai, G = kelurahan_max
				H = NJOP		I = NJOP * 3, J = H, K = I * 100/300

			*/

			var usulan = $('#nilai_usulan_eval').val();
			var A1 = $('#luas_tanah').val();
			var A2 = $('#luas_bangunan').val();

			//---HARGA PASAR BTN
			var B1 = $('#btn_harga_tanah').val();
			var B2 = $('#btn_harga_pasar').val();
			var C = A1 * B1;
			var D = (C * 100) / (130 / 100);
			$('#btn_nilai').val(format_numberdisp(C));
			$('#btn_max').val(format_numberdisp(D));

			//---HARGA PASAR KELURAHAN
			var E1 = $('#kelurahan_harga_tanah').val();
			var E2 = $('#kelurahan_harga_tanah').val();
			var F = A1 * E2;
			var G = (F * 80) / 100;
			$('#kelurahan_nilai').val(format_numberdisp(F));
			$('#kelurahan_max').val(format_numberdisp(G));

			//---HARGA NJOP
			var H1 = $('#njop_harga_tanah').val();
			var H2 = $('#njop_harga_bangunan').val();
			var I1 = (A1 * H1) * 3;
			var I2 = (A2 * H2) * (50 / 100);
			var J = I1 + I2;
			var K = J * 100 / 130
			$('#njop_nilai').val(format_numberdisp(J));
			$('#njop_max').val(format_numberdisp(K));
			$('#njop_x3_tanah').val(format_numberdisp(I1));
			$('#njop_x3_bangunan').val(format_numberdisp(I2));

			var total_jaminan_max = Math.max(C, F, J);
			var max_pinjaman = Math.max(D, G, K)

			//---PEMBULATAN total_jaminan
			var total_jaminan_to_string = total_jaminan_max.toString();
			total_jaminan_length = total_jaminan_to_string.length;
			total_jaminan_length2 = total_jaminan_length - 5;
			var total_jaminan_pengurang = total_jaminan_to_string.substring(total_jaminan_length, total_jaminan_length2);
			//console.log("F = "+F_to_string+" - length = "+F_length+" - length2 = "+F_length2+" - pengurang = "+F_pengurang);
			total_jaminan = total_jaminan_max - total_jaminan_pengurang;

			if (usulan < max_pinjaman)
				$('#has_rekomendasi').val('LAYAK');
			else if (usulan <= 3000000)
				$('#has_rekomendasi').val('LAYAK');
			else
				$('#has_rekomendasi').val('TIDAK LAYAK');

			$('#has_usulan_pinjaman').val(format_numberdisp(usulan));
			$('#total_jaminan').val(format_numberdisp(total_jaminan));
			$('#max_pinjaman').val(format_numberdisp(max_pinjaman));

		}
	}

	/*
	function rekom_ulang()
	{
		var usulan = $('#has_usulan_pinjaman').val();
		var pinjaman_max = $('#has_maks_pinjaman').val();
		var total_jaminan = $('#has_total_jaminan').val();

		if(usulan < pinjaman_max || usulan < total_jaminan)
		{
			$('#has_rekomendasi').val('LAYAK');
		}
		else
		{
			$('#has_rekomendasi').val('TIDAK LAYAK');
		}
	}
	*/

	function jaminan_bpkb(usulan, batas_nilai, tahun_valid, batas_tahun_jaminan) {
		//var usulan = parseInt($('#nilai_pengajuan').val());
		console.log(usulan, batas_nilai, tahun_valid, batas_tahun_jaminan);
		//var total_jaminan =
		$('#has_usulan_pinjaman').val(format_numberdisp(usulan));
		$('#total_jaminan').val(format_numberdisp(usulan));
		$('#max_pinjaman').val(format_numberdisp(usulan));
		//var tahun_valid = $('#valid_bpkb').val();

		if (usulan <= batas_nilai && tahun_valid <= batas_tahun_jaminan) {
			$('#has_rekomendasi').val('LAYAK');
		} else {
			$('#has_rekomendasi').val('TIDAK LAYAK');
		}
	}

	function non_jaminan(usulan, batas_nilai) {
		//var usulan = parseInt($('#nilai_pengajuan').val());
		console.log(usulan, batas_nilai);
		$('#has_usulan_pinjaman').val(format_numberdisp(usulan));
		$('#total_jaminan').val(format_numberdisp(usulan));
		$('#max_pinjaman').val(format_numberdisp(usulan));

		if (usulan <= batas_nilai) {
			$('#has_rekomendasi').val('LAYAK');
		} else {
			$('#has_rekomendasi').val('TIDAK LAYAK');
		}
	}

	function view_report_pengukuran() {
		var row = $('#dg-pengukuran_nilai').datagrid('getSelected');
		var id_tnilai = row.id_tnilai;
		data = "<?= base_url('pra_realisasi_kemitraan/pengukuran_nilai/view_report_pengukuran') ?>/" + id_tnilai;
		window.open(data);
	}


	function view_report_usulan() {
		var row = $('#dg-pengukuran_nilai').datagrid('getSelected');
		var id_mitra = row.id_mitra;
		data = "<?= base_url('pra_realisasi_kemitraan/pengukuran_nilai/view_report_usulan') ?>/" + id_mitra;
		window.open(data);
	}


	function add_pengukuran_nilai() {
		//$('#dlg_pengukuran_nilai').dialog('open').dialog('setTitle','Pengukuran Nilai Jaminan Modal');

		$('#dlg').dialog({
			height: 680,
			width: 700
		});
		$('#dlg').dialog('open').dialog('setTitle', 'Pengukuran Nilai Jaminan Modal');
		$('#jamin').show()
	}

	function detail_jaminan() {
		var row = $('#dg').datagrid('getSelected');
		/*
		if (row){
			alert('Item ID:'+row.id_tnilai);
		}
		*/
		if (row) {
			$('#dlg-detail-jaminan').dialog('open').dialog('setTitle', 'Detail Pengukuran Nilai (' + row.id_mitra + ')');
			url_detail_jaminan_grid = '<?= base_url($module . '/' . $appLink . '/get_detail_jaminan') ?>/' + row.id_tnilai;

			$('#dg-detail-jaminan').datagrid({
				url: url_detail_jaminan_grid
			});
		} else {
			$.messager.alert('Error Message', 'Anda belum memilih data!', 'error');
		}

	}

	function action(val, row, index) {
		disposisi = '<a href="javascript:disposisi_view(\'' + row.id_mitra + '\')" title="Disposisi" class="btn btn-small btn-default easyui-tooltip" ><i class="icon-tasks icon-large"></i></a>';
		return disposisi;
	}

	function disposisi_view(id_mitra) {
		$('#dlg_disposisi').dialog('open').dialog('setTitle', 'List Disposisi');
		$('#id_doc').val(id_mitra);
		url_list = "<?= base_url('pra_realisasi_kemitraan/pengukuran_nilai/get_disposisi') ?>/" + id_mitra;
		$('#dg-disposisi').datagrid({
			url: url_list
		});
	}

	function action_disposisi(val, row, index) {
		//var idUser = "<?= $this->session->userdata('idUser') ?>";
		baris = $('#dg-disposisi').datagrid('getRows')[index];
		console.log(baris.approve);
		if (baris.approve == '1') {
			disposisi = '<center><span style="color:green;"><i class="icon-check icon-large"></i><span><center>';
		} else {
			disposisi = '<a href="javascript:disposisi_selesai(' + index + ')" title="Disposisi" class="btn btn-small btn-default easyui-tooltip" ><i class="icon-ok icon-large"></i></a>';
		}
		return disposisi;
	}

	function disposisi_selesai(index) {
		row = $('#dg-disposisi').datagrid('getRows')[index];
		console.log(row);
		$.post('<?= base_url('pra_realisasi_kemitraan/pengukuran_nilai/disposisi_selesai') ?>/' + row.id_doc + '/' + row.nip_tujuan, function(result) {
			if (result.success) {
				$.messager.show({
					title: 'Success',
					msg: 'Disposisi sudah selesai'
				});
				$('#dg-disposisi').datagrid('reload');
			} else {
				$.messager.show({
					title: 'Error',
					msg: result.msg
				});
				$('#dg-disposisi').datagrid('reload');
			}
		}, 'json');
		$('#dg-disposisi').datagrid('reload');
	}

	function add_disposisi() {
		$('#fm_disposisi').form('submit', {
			url: "<?= base_url('pra_realisasi_kemitraan/pengukuran_nilai/add_disposisi') ?>",
			onSubmit: function() {
				return $(this).form('validate');
			},
			success: function(result) {
				var result = eval('(' + result + ')');
				if (result.success) {
					$('#dg-disposisi').datagrid('reload');
					$('#fm_disposisi').form('clear');
				} else {
					$.messager.alert('Error Message', result.msg, 'error');
				}
			}
		});
	}

	function hapus_disposisi() {
		var data = $('#dg-disposisi').datagrid('getSelected');
		// console.log(data);
		$.messager.confirm('Confirm', 'Apakah Anda Yakin Akan Menghapus ini?', function(r) {
			if (r) {
				$.post('<?= base_url('pra_realisasi_kemitraan/pengukuran_nilai/hapus_disposisi') ?>/' + data.id_doc + '/' + data.nip_tujuan + '/' + data.creator, function(result) {
					if (result.success) {
						$.messager.show({
							title: 'Success',
							msg: 'Hapus Disposisi Berhasil'
						});
						$('#dg-disposisi').datagrid('reload');
					} else {
						$.messager.show({
							title: 'Error',
							msg: result.msg
						});
						$('#dg-disposisi').datagrid('reload');
					}
				}, 'json');
				$('#dg-disposisi').datagrid('reload');
			}
		});
	}
</script>

<div class="tabs-container">
	<table id="dg-pengukuran_nilai" class="easyui-datagrid" data-options="
	    url: '<?= base_url($module . '/' . $appLink . '/read') ?>',
		singleSelect:'true',
	    title:'<?= $app ?>',
	    toolbar:'#toolbar',
	    iconCls:'icon-cog',
	    rownumbers:'true',
	    idField:'<?= $idField ?>',
	    pagination:'true',
	    fitColumns:'true',
	    pageList: [10,20,30]
	    ">
		<thead>
			<th field="id_tnilai" width="100" sortable="true" hidden="true"></th>
			<th field="id_mitra" width="150" sortable="true">ID Mitra</th>
			<th field="nama_jaminan" width="200" sortable="true">Jaminan</th>
			<th align="right" field="usulan_pinjaman" width="200" sortable="true" formatter="format_numberdisp">Usulan Pinjaman</th>
			<th align="right" field="total_jaminan" width="200" sortable="true" formatter="format_numberdisp">Pengukuran Nilai Jaminan</th>
			<th align="right" field="max_pinjaman" width="200" sortable="true" formatter="format_numberdisp">Maksimal Pinjaman</th>
			<th field="rekomendasi" width="200" sortable="true">Rekomendasi</th>
			<!-- <th field="asd" formatter="action">Action</th> -->
		</thead>
	</table>

	<!-- Model Start -->
	<div id="dlg" class="easyui-dialog" style="width:700px; height:440px; padding:10px" closed="true" buttons="#t_dlg_dis-buttons">
		<form id="fm" method="post" enctype="multipart/form-data" action="">
			<table width="100%" align="center" border="0">
				<tr>
					<input type="hidden" id="id_tnilai" name="id_tnilai" style="width:70%;" />
					<td>Mitra</td>
					<td>:</td>
					<td>
						<input name="id_mitra" id="id_mitra" class="easyui-combogrid" required style="width: 203px" data-options="panelHeight:200, panelWidth: 700,
						valueField:'',
						idField:'id_mitra',
						textField:'nama',
						mode:'remote',
						url:'<?= base_url($module . '/' . $appLink . '/get_mitra') ?>',
						onSelect :function(indek,row)
						{
							console.log(row),
							$('#nama').val(row.nama),
							$('#sektor_kemitraan').val(row.nama_sektor),
							$('#no_ktp').val(row.no_ktp),
							$('#alamat').val(row.alamat),
							$('#kota').val(row.kota),
							$('#nilai_pengajuan').val(row.nilai_pengajuan),
							$('#nilai_usulan_eval').val(row.nilai_usulan),
							$('#jenis_jaminan_nama').val(row.nama_jaminan),
							$('#id_jaminan').val(row.id_jaminan),
							$('#sektor_kemitraan').val(row.nama_sektor)

							if(row.id_jaminan == '3' || row.id_jaminan == '7') //3 = Sertifikat Tanah dari mas_jaminan
							{
								$('#input_jaminan').show();
								$('#bangunan').hide();
								$('#harga_bangunan').hide();
							}

							else if(row.id_jaminan == '5' || row.id_jaminan == '4') //5 = sertifikat Tanah plus Bangunan yang berdiri diatasnya 5 = SKMT & APTH
							{
								$('#input_jaminan').show();
								$('#bangunan').show();
							}

							else if(row.id_jaminan == 1 || row.id_jaminan == 2) //1 = BPKB Motor 2 = BPKB Mobil
							{
								jaminan_bpkb(row.nilai_pengajuan, row.batas_nilai, row.valid_bpkb, row.batas_tahun_jaminan);
								$('#input_jaminan').hide();
								$('#bangunan').hide();
								$('#harga_bangunan').hide();
								$('#tahun_bpkb_row').show();
								$('#valid_bpkb').val(row.valid_bpkb);
								$('#tahun_bpkb').val(row.tahun_bpkb);
							}

							else
							{
								non_jaminan(row.nilai_pengajuan, row.batas_nilai);
							}
							
						},
						columns:[[
                       		{field:'id_mitra',title:'ID Mitra',width:80},
                        	{field:'nama',title:'Nama Pemohon',width:150},
                        	{field:'nama_perusahaan',title:'Nama Perusahaan',width:150},
                        	{field:'nilai_pengajuan',title:'Nilai',width:100},
							{field:'nama_jaminan',title:'Jaminan',width:150}
                        ]]

						">
					</td>

				</tr>
				<tr>
					<td>Nama Pemohon</td>
					<td>:</td>
					<td>
						<!--<input id="nama" name="nama" required type="text" style="width: 200px;">-->
						<input id="nama" name="nama" style="width: 200px;">
					</td>

				</tr>
				<tr>
					<td>No KTP</td>
					<td>:</td>
					<td>
						<input id="no_ktp" name="no_ktp" required type="text" style="width: 200px;">
					</td>

				</tr>
				<tr>
					<td>Alamat</td>
					<td>:</td>
					<td>
						<input id="alamat" name="alamat" required type="text" style="width: 200px;">
					</td>
					<td>Usulan Pinjaman</td>
					<td>:</td>
					<td>
						<input id="has_usulan_pinjaman" name="has_usulan_pinjaman" required type="text" style="width: 130px; text-align:right;" class="easyui-textbox" readonly>
					</td>
				</tr>
				<tr>
					<td>Kota</td>
					<td>:</td>
					<td>
						<input id="kota" name="kota" class="easyui-textbox" style="width:200px" required>
					</td>
					<td>Nilai Total Jaminan</td>
					<td>:</td>
					<td>
						<input id="total_jaminan" name="total_jaminan" required type="text" style="width: 130px; text-align:right;" class="easyui-textbox" readonly>
					</td>
				</tr>
				<tr>
					<td>Nilai Pengajuan</td>
					<td>:</td>
					<td>
						<input id="nilai_pengajuan" name="nilai_pengajuan" required type="text" style="width: 200px; " class="easyui-textbox" data-options="precision:2,precision:0,groupSeparator:'.',decimalSeparator:','" readonly>
					</td>
					<td>Maksimal Pinjaman</td>
					<td>:</td>
					<td>
						<input id="max_pinjaman" name="max_pinjaman" required type="text" style="width: 130px; text-align:right;" class="easyui-textbox" readonly>
					</td>
				</tr>

				<tr>
					<td>Nilai Usulan Evaluator</td>
					<td>:</td>
					<td>
						<input id="nilai_usulan_eval" name="nilai_usulan_eval" required type="text" style="width: 200px; " class="easyui-textbox" data-options="precision:2,precision:0,groupSeparator:'.',decimalSeparator:','" readonly>
					</td>
					<td>Rekomendasi</td>
					<td>:</td>
					<td>
						<input id="has_rekomendasi" name="has_rekomendasi" required type="text" style="width: 130px;" readonly>
					</td>
				</tr>

				<tr>
					<td>Jenis Jaminan</td>
					<td>:</td>
					<td>
						<input id="id_jaminan" name="id_jaminan" required type="hidden" style="width: 200px;" readonly>
						<input id="jenis_jaminan_nama" name="jenis_jaminan_nama" required type="text" style="width: 200px;" readonly>
					</td>
					<td></td>
					<td></td>
					<td stlye="align:right;">
						<!--<a href="javascript:void(0)" onclick="javascript:rekom_ulang()"><i class="icon-refresh"></i>&nbsp;Refresh</a>-->
					</td>
				</tr>

				<tr id="tahun_bpkb_row" style="display:none;">
					<td>Tahun BPKB</td>
					<td>:</td>
					<td>
						<input name="tahun_bpkb" id="tahun_bpkb" required type="text" style="width: 200px;" readonly>
						<input name="valid_bpkb" id="valid_bpkb" required type="text" style="width: 200px;" readonly hidden>
					</td>
					<td></td>
					<td></td>
					<td stlye="align:right;">
						<!--<a href="javascript:void(0)" onclick="javascript:rekom_ulang()"><i class="icon-refresh"></i>&nbsp;Refresh</a>-->
					</td>
				</tr>

				<tr>
					<td>Sektor kemitraan</td>
					<td>:</td>
					<td>
						<input name="sektor_kemitraan" id="sektor_kemitraan" required type="text" style="width: 200px;" readonly>
					</td>
					<td></td>
					<td></td>
					<td stlye="align:right;">
						<!--<a href="javascript:void(0)" onclick="javascript:rekom_ulang()"><i class="icon-refresh"></i>&nbsp;Refresh</a>-->
					</td>
				</tr>

				<tr>
					<td></td>
					<td></td>
					<td>
						<a href="javascript:void(0)" onclick="javascript:add_pengukuran_nilai()" id="input_jaminan" name="input_jaminan" class="easyui-linkbutton" style="display:none;">Input Data Jaminan</a>
					</td>
				</tr>
			</table>

			<table id="jamin" width="100%" align="center" border="0" style="display:none;">

				<tr>
					<td colspan="4">
						<hr>
					</td>
				</tr>
				<tr>
					<td colspan="4">
						<center><strong>PENGUKURAN NILAI JAMINAN MODAL</strong></center>
					</td>
				</tr>
				<tr>
					<td style="text-align:right">Deskripsi Bangunan</td>
					<td>:</td>
					<td>
						<input id="deskripsi_bangunan" name="deskripsi_bangunan" style="width: 200px;">
					</td>
					<td></td>
				</tr>

				<tr>
					<td style="text-align:right">Keterangan Bangunan</td>
					<td>:</td>
					<td>
						<input id="keterangan_bangunan" name="keterangan_bangunan" style="width: 200px; ">
					</td>
					<td></td>
				</tr>

				<tr>
					<td style="text-align:right">Luas Tanah</td>
					<td>:</td>
					<td>
						<input type="text" id="luas_tanah" name="luas_tanah" style="width: 100px; text-align:right;"> M2
					</td>
				</tr>
				<tr id="bangunan">
					<td style="text-align:right">Luas Bangunan</td>
					<td>:</td>
					<td>
						<input type="text" id="luas_bangunan" name="luas_bangunan" style="width: 100px; text-align:right;"> M2
					</td>
				</tr>

				<tr>
					<td style="text-align:right" valign="bottom">Harga Tanah</td>
					<td valign="bottom">:</td>
					<td>
						<table>
							<tr style="text-align:center">
								<td>Pasar BPN</td>
								<td>Pasar Kelurahan</td>
								<td>NJOP</td>
							</tr>
							<tr>
								<td><input name="btn_harga_tanah" id="btn_harga_tanah" style="width:130px; text-align:right;" class="easyui-textbox"></td>
								<td><input name="kelurahan_harga_tanah" id="kelurahan_harga_tanah" style="width:130px; text-align:right;" class="easyui-textbox"></td>
								<td><input name="njop_harga_tanah" id="njop_harga_tanah" style="width:130px; text-align:right;" class="easyui-textbox"></td>
								<td></td>
							</tr>

						</table>
					</td>
				</tr>
				<tr id="harga_bangunan">
					<td style="text-align:right">Harga Bangunan</td>
					<td>:</td>
					<td>
						<table>

							<tr>
								<td><input name="btn_harga_bangunan" id="btn_harga_bangunan" style="width:130px; text-align:right;" class="easyui-textbox"></td>
								<td><input name="kelurahan_harga_bangunan" id="kelurahan_harga_bangunan" style="width:130px; text-align:right;" class="easyui-textbox"></td>
								<td><input name="njop_harga_bangunan" id="njop_harga_bangunan" style="width:130px; text-align:right;" class="easyui-textbox"></td>
								<td></td>
							</tr>

						</table>
					</td>
				</tr>

				<tr>
					<td></td>
					<td></td>
					<td>
						<a href="javascript:void(0)" onclick="javascript:hitung_pengukuran()" id="hitung" name="hitung" class="easyui-linkbutton">Hitung</a>
					</td>
					<td></td>
				</tr>

				<tr>
					<td></td>
					<td></td>
					<td>
						<br>
					</td>
					<td></td>
				</tr>

				<tr>
					<td style="text-align:right" valign="bottom">NJOP X 3</td>
					<td valign="bottom">:</td>
					<td>
						<table>
							<tr style="text-align:center">
								<td>Tanah</td>
								<td>Bangunan</td>
							</tr>
							<tr>
								<td><input name="njop_x3_tanah" id="njop_x3_tanah" style="width:130px; text-align:right;" class="easyui-textbox" readonly></td>
								<td><input name="njop_x3_bangunan" id="njop_x3_bangunan" style="width:130px; text-align:right;" class="easyui-textbox" readonly></td>

							</tr>

						</table>
					</td>
					<td></td>
				</tr>

				<tr>
					<td valign="bottom" style="text-align:right; ">Nilai</td>
					<td valign="bottom">:</td>
					<td>
						<table>
							<tr style="text-align:center">
								<td>Pasar BPN</td>
								<td>Pasar Kelurahan</td>

								<td>NJOP</td>
							</tr>

							<tr>
								<td><input name="btn_nilai" id="btn_nilai" style="width:130px; text-align:right;" class="easyui-textbox" readonly></td>
								<td><input name="kelurahan_nilai" id="kelurahan_nilai" style="width:130px; text-align:right;" class="easyui-textbox" readonly></td>
								<td><input name="njop_nilai" id="njop_nilai" style="width:130px; text-align:right;" class="easyui-textbox" readonly></td>
								<td></td>
							</tr>

						</table>
					</td>
				</tr>
				<tr>
					<td style="text-align:right">Pinjaman Maximum</td>
					<td>:</td>
					<td>
						<table>

							<tr>
								<td><input name="btn_max" id="btn_max" style="width:130px; text-align:right;" class="easyui-textbox" readonly></td>
								<td><input name="kelurahan_max" id="kelurahan_max" style="width:130px; text-align:right;" class="easyui-textbox" readonly></td>
								<td><input name="njop_max" id="njop_max" style="width:130px; text-align:right;" class="easyui-textbox" readonly></td>
								<td></td>
							</tr>

						</table>
					</td>
				</tr>



			</table>
		</form>


		<!-- Tombol Add, Edit, Hapus Datagrid -->
		<div id="toolbar">
			<table align="center" style="padding: 0px; width: 99%;">
				<tr>
					<td>
						<a href="javascript:void(0)" onclick="javascript:add()" class="btn btn-small btn-success"><i class="icon-plus-sign icon-large"></i>&nbsp;Add</a>
						<!-- <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:edit()"><i class="icon-edit"></i>&nbsp;Edit</a> -->
						<!-- <a href="javascript:void(0)" class="btn btn-small btn-danger" onclick="javascript:hapus()"><i class="icon-trash icon-large"></i>&nbsp;Delete</a> -->
						<!--<a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:detail_jaminan()"><i class="icon-file"></i>&nbsp;Detail</a>-->
						<a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:view_report_pengukuran()"><i class="icon-print"></i>&nbsp;Report Pengukuran Sertifikat</a>
						<!-- <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:view_report_usulan()"><i class="icon-print"></i>&nbsp;Report Usulan</a> -->
					</td>
					<td>&nbsp;</td>
					<td align="right">
						<input class="easyui-searchbox" prompt="Silahkan Input Field" searcher="doSearch" style="width:300px" />
					</td>
				</tr>
			</table>
		</div>
		<!-- end tombol datagrid -->
		<!-- Tombol simpan & Batal Form -->
		<div id="t_dlg_dis-buttons">
			<a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:save()"><i class="icon-save icon-large"></i>&nbsp;Simpan</a>
			<a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:$('#dlg').dialog('close')"><i class="icon-remove icon-large"></i>&nbsp;Batal</a>
		</div>
	</div>

	<div id="dlg-detail-jaminan" class="easyui-dialog" style="width:1100px; height:200px; padding:10px" closed="true">
		<table id="dg-detail-jaminan" class="easyui-datagrid" data-options="
		singleSelect:'true',
	    title:'Detail',
	    idField:'id_tnilai',
	    pagination:'true',
	    fitColumns:'true',
	    pageList: [10,20,30]
	    ">
			<thead>
				<th field="id_tnilai" sortable="true" hidden="true">ID</th>
				<th field="id_mitra" sortable="true">ID Pengusaha</th>
				<th field="jenis_jaminan" sortable="true">Jenis Jaminan</th>
				<<<<<<< .mine ||||||| .r252=======>>>>>>> .r255
					<th field="rekomendasi" sortable="true">Rekomendasi</th>
			</thead>

		</table>
	</div>
</div>

<div id="dlg_disposisi" class="easyui-dialog" style="width:1050px; height:400px; padding:10px" closed="true" buttons="#t_dlg_dis_view-buttons">
	<form id="fm_disposisi" method="post" enctype="multipart/form-data" action="">
		<table align="left">
			<tr>
				<td>
					Tujuan
				</td>
				<td> : </td>
				<td>
					<input type="hidden" id="id_doc" name="id_doc" style="width: 750px;">
					<select id="tujuan_disposisi" name="tujuan_disposisi" class="easyui-combogrid" style="width:215px;height:25px;" data-options="
						panelWidth:350,
						mode:'remote',
						url:'<?= base_url('pra_realisasi_kemitraan/pengukuran_nilai/get_pegawai') ?>',
						idField:'idPegawai',
						textField:'nama_pegawai',
						columns:[[
						{field:'idPegawai',title:'NIP',width:120},
						{field:'nama_pegawai',title:'Nama Pegawai',width:230},
						]]
                        ">
					</select>
				</td>
			</tr>

			<tr>
				<td>
					Pesan
				</td>
				<td> : </td>
				<td>
					<input class="easyui-combobox" id="pesan_disposisi" name="pesan_disposisi" style="width:215px;height:25px;" data-options="valueField:'id',textField:'pesan_disposisi',url:'<?= base_url('pra_realisasi_kemitraan/pengukuran_nilai/get_pesan') ?>'">
				</td>
			</tr>

			<tr>
				<td>
					Catatan
				</td>
				<td> : </td>
				<td>
					<input type="text" id="catatan_disposisi" name="catatan_disposisi" style="width: 950px;">
				</td>

			</tr>

			<tr>
				<td></td>
				<td></td>
				<td align="left">
					<!-- <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:add_disposisi()"><i class="icon-plus icon-large"></i>&nbsp;Add</a>
					<a href="javascript:void(0)" class="btn btn-small btn-danger" onclick="javascript:hapus_disposisi()"><i class="icon-remove icon-large"></i>&nbsp;Delete</a> -->
				</td>
			</tr>
			<!--tr>
                <td>
                    <textarea placeholder="Catatan" name="catatan" id="catatan"></textarea>
                </td>
            </tr-->
		</table>
	</form>
	<br>
	<table id="dg-disposisi" class="easyui-datagrid" style="height:200px;width:1000px;" data-options="
        singleSelect:'true',
        title:'List',
        iconCls:'icon-tasks',
        rownumbers:'true',
        fitColumns:'true'
        ">
		<thead>
			<!--th field="username" width="100">Username</th-->
			<th field="id_doc" sortable="true">Id</th>
			<th field="approve" sortable="true" hidden="true"></th>
			<th field="id_penerima" sortable="true" hidden="true"></th>
			<th field="pemberi" width="80">Pemberi Disposisi</th>
			<th field="penerima" width="80">Ditujukan Untuk</th>
			<th field="pesan_disposisi" width="100">Pesan</th>
			<th field="catatan_disposisi" width="200">Catatan</th>
			<th field="approve_date_time" width="70">Tanggal Approve</th>
			<!-- <th field="aasdf" formatter="action_disposisi">Action</th> -->
		</thead>
	</table>

</div>