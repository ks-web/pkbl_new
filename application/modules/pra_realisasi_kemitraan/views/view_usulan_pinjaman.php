<?php
$app 		= 'Usulan Pinjaman'; // nama aplikasi
$module 	= 'pra_realisasi_kemitraan';
$appLink 	= 'usulan_pinjaman'; // controller
$idField  	= ''; //field key table
$nodeMenu
?>

<script>
	var url;
	var app = "<?=$app?>";
	var appLink = '<?=$appLink?>';
	var module = '<?=$module?>';
	var idField = '<?=$idField?>';

	function doSearch(value){
	    $('#dg').datagrid('load',{
	    	q:value
	    });
	}
	
	function view_report_usulan(){
		var rows = $('#dg').datagrid('getSelections');
		
		if(rows.length > 0){
    		var ids_selected= [];
	    	rows.forEach(function(entry){
	    		ids_selected.push (entry.id_mitra);
	    	});
	    	
	    	var ids_selected_str = ids_selected.join('_');
	    	
	    	window.open('<?=base_url($module . '/' . $appLink . '/view_report_usulan')?>/'+ids_selected_str, '_blank');
    	} else {
    		$.messager.alert('Error Message','Tidak ada data yang dipilih','error');
    	}
		
		// var row = $('#dg').datagrid('getSelected');
		// var id_mitra = row.id_mitra;
		// url = '<?=base_url($module . '/' . $appLink . '/view_report_usulan')?>/'+id_mitra;
        // window.open(url, '_blank');
	}

</script>

<div class="tabs-container">
	<table id="dg" class="easyui-datagrid" data-options="
	    url: '<?=base_url($module . '/' . $appLink . '/read')?>',
	    title:'<?=$app?>',
	    toolbar:'#toolbar',
	    iconCls:'icon-cog',
	    rownumbers:'true',
	    idField:'<?=$idField?>',
	    pagination:'true',
	    fitColumns:'true',
	    pageList: [10,20,30]
	    ">
	    <thead>
			<th data-options="checkbox:true"></th>
	        <th field="id_mitra" width="150" sortable="true">ID Mitra</th>
	        <th field="nama_jaminan" width="200" sortable="true">Jaminan</th>
            <th align="right" field="usulan_pinjaman" width="200" sortable="true" formatter="format_numberdisp">Usulan Pinjaman</th>
            <th align="right" field="total_jaminan" width="200" sortable="true" formatter="format_numberdisp">Pengukuran Nilai Jaminan</th>
            <th align="right" field="max_pinjaman" width="200" sortable="true" formatter="format_numberdisp">Maksimal Pinjaman</th>
            <th field="rekomendasi" width="200" sortable="true">Rekomendasi</th>
	    </thead>
	</table>

	<!-- Model Start -->
	<div id="dlg" class="easyui-dialog" style="width:700px; height:440px; padding:10px" closed="true" buttons="#t_dlg_dis-buttons" >
	    <form id="fm" method="post" enctype="multipart/form-data" action="">
	        
	    </form>
	   	<!-- Tombol Add, Edit, Hapus Datagrid -->
       	<div id="toolbar">
            <table align="center" style="padding: 0px; width: 99%;">
                <tr>
                    <td>
						<a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:view_report_usulan()"><i class="icon-print"></i>&nbsp;Report Usulan</a>
						<a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:view_report_usulan2()"><i class="icon-print"></i>&nbsp;Report Usulan 2</a>
					</td>
                    <td>&nbsp;</td>
                    <td align="right">
                    	<input class="easyui-searchbox" prompt="Silahkan Input Field" searcher="doSearch"  style="width:300px"/>
                    </td>
                </tr>
            </table>
        </div>
        <!-- end tombol datagrid -->
        <!-- Tombol simpan & Batal Form -->
        <div id="t_dlg_dis-buttons">
            <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:save()"><i class="icon-save icon-large"></i>&nbsp;Simpan</a>
            <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:$('#dlg').dialog('close')"><i class="icon-remove icon-large"></i>&nbsp;Batal</a>
        </div>
	</div>
</div>