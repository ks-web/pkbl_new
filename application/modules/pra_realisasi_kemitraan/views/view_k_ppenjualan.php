<?php
$app3 = 'Produksi dan Penjualan Perperiode'; // nama aplikasi
$module3 = 'pra_realisasi_kemitraan';
$appLink3 = 'K_ppenjualan'; // controller
$idField3  = 'id_ppenjualan'; //field key table
?>

<script>
	var url3;
	var app3 = "<?=$app3?>";
	var appLink3 = '<?=$appLink3?>';
	var module3 = '<?=$module3?>';
	var idField3 = '<?=$idField3?>';

	function hit1(){
		var harga=$("#fm3 #harga_satuan").numberbox('getValue');
		var prod_akt=$("#fm3 #produksi_akt").numberbox('getValue');
		var prod_proy=$("#fm3 #produksi_proy").numberbox('getValue');

		var sumharga=harga*prod_akt;
		$("#fm3 #penjualan_akt").numberbox('setValue', sumharga);

		var sumharga2=harga*prod_proy;
		$("#fm3 #penjualan_proy").numberbox('setValue', sumharga2);
	// console.log(harga);
	}	
	
    function add3(){
        $('#dlg3').dialog('open').dialog('setTitle','Tambah ' + app3);
		$('#fm3').form('clear');
	    $('#tbl3').html('Save3');
		url3 = '<?=base_url($module3 . '/' . $appLink3 . '/create')?>/'+idParent;
    }
    function edit3(){
        var row = $('#dg3').datagrid('getSelected');
		if (row){
			$('#tbl3').html('Simpan');
			$('#dlg3').dialog('open').dialog('setTitle','Edit '+ app3);
			$('#fm3').form('load',row);
			url3 = '<?=base_url($module3 . '/' . $appLink3 . '/update')?>/'+row[idField3];
	    }
    }
    function hapus3(){
        var row = $('#dg3').datagrid('getSelected');		
	    if (row){
			$.messager.confirm('Confirm','Apakah Anda Yakin Akan Menghapus ini?',function(r){
				if (r){
					$.post('<?=base_url($module3 . '/' . $appLink3 . '/delete')?>/'+row[idField3],function(result){
						if (result.success){
							$('#dg3').datagrid('reload');	// reload the user data
							$('#dg3').datagrid('reload');
						} else {
							$.messager.show({	// show error message
								title: 'Error',
								msg:result.msg
							});
						}
					},'json');
				}
			});
		}
    }
    function save3(){
	   $('#fm3').form('submit',{
	    	url: url3,
	    	onSubmit: function(){
	    		return $(this).form('validate');
	    	},
	    	success: function(result){
	    		var result = eval('('+result+')');
	    		if (result.success){
	    			$('#dlg3').dialog('close');		
	    			$('#dg3').datagrid('reload');
	                
	    		} else {
	             $.messager.alert('Error Message',result.msg,'error');
	    		}
	    	}
	    });
	}
	function doSearch3(value){
	    $('#dg3').datagrid('load',{    
	    	q:value  
	    });
	}
	function reload3(value){
		$('#dg3').datagrid({    
	    	url: '<?=base_url($module3 . '/' . $appLink3 . '/read/')?>/'+idParent  
	    });
	}
</script>
 
<div class="tabs-container">                
	<table id="dg3" class="easyui-datagrid" 
	data-options="  
	    url: '<?=base_url($module3 . '/' . $appLink3 . '/read/')?>',
		singleSelect:'true', 
	    title:'<?=$app3?>',
	    toolbar:'#toolbar3',
	    iconCls:'icon-cog',
	    rownumbers:'true',  
	    idField:'<?=$idField3?>', 
	    pagination:'true',
	    fitColumns:'true',
	    pageList: [10,20,30]
	    "
	>
	   <thead>
	    <tr>
	    	<th rowspan="2" field="id_ppenjualan" hidden width="100" sortable="true">ID</th>
	    	<th rowspan="2" field="jenis_produksi" width="100" sortable="true">Jenis Produksi</th>
			<th rowspan="2" field="satuan" width="100" sortable="true">Satuan</th>
			<th rowspan="2" field="harga_satuan" width="100" sortable="true">Harga Satuan</th>
			<th colspan="2"  width="100" sortable="true">Produksi</th>
			<th colspan="2"  width="100" sortable="true">Penjualan </th>
		</tr>
			<th field="produksi_akt" width="100" sortable="true">Akt</th>
			<th field="produksi_proy" width="100" sortable="true">Proy</th>
			<th field="penjualan_akt" width="100" sortable="true">Akt</th>
			<th field="penjualan_proy" width="100" sortable="true">Proy</th>

	    </thead>
	</table>
	
	<!-- Model Start -->
	<div id="dlg3" class="easyui-dialog" style="width:500px; height:auto; padding:10px" closed="true" buttons="#t_dlg_dis-buttons3" >
	    <form id="fm3" method="post" enctype="multipart/form-data" action="">
	       <table width="100%" align="center" border="0">
	           <tr>
	               <td>Jenis produksi</td>
	               <td>:</td>
	               <td>
	               <textarea  name="jenis_produksi" id="jenis_produksi" required style="width: 250px;"></textarea>
	               </td>
	            </tr>
	           <tr>
	               <td>Satuan</td>
	               <td>:</td>
	               <td><input style="width: 70px;" class="easyui-combobox" name="satuan" id="satuan" required data-options="
                                    url:'<?=base_url($module3 . '/' . $appLink3 . '/getsatuan')?>',
                                    method:'get',
                                    required:'true',
                                    valueField:'satuan',
                                    textField:'satuan',
                                    panelHeight:'auto'
                                ">
	               </td>
	            </tr>
	           <tr>
	               <td>Harga Satuan</td>
	               <td>:</td>
	               <td><input class="easyui-numberbox" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%', onChange: function(a,b){
	              hit1();}" name="harga_satuan" id="harga_satuan" required style="width: 250px;">
	               </td>
	            </tr>
	            <tr>
	               <td><b>Produksi</b></td>
	               <td></td>
	            </tr>
	            <tr>	            	               
	                <td style="text-align: right;">Akt</td>
	                <td></td>
	                <td>&nbsp;<input class="easyui-numberbox"  name="produksi_akt" id="produksi_akt" required style="width: 50px;" data-options="onChange: function(a,b){
	              hit1();}" >
	               </td>
	            </tr>
	            <tr>             
	               <td style="text-align: right;">Proy </td>
	               <td></td>
	               <td>&nbsp;<input class="easyui-numberbox"  name="produksi_proy" id="produksi_proy" required style="width: 50px;" data-options="onChange: function(a,b){
	              hit1();}" ></td>
	            </tr>
	            
	            <tr>
	               <td><b>Penjualan</b></td>
	               <td></td>
	            </tr>
	            <tr>
	               <td style="text-align: right;">Akt </td>
	               <td></td>
	               <td>&nbsp;<input class="easyui-numberbox" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%'" name="penjualan_akt" id="penjualan_akt" required style="width: 250px;" readonly="true">
	               </td>
	            </tr>
	            <tr>
	               	               
	               <td style="text-align: right;">Proy </td>
	               <td></td>
	               <td>&nbsp;<input class="easyui-numberbox" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%'" name="penjualan_proy" id="penjualan_proy" required style="width: 250px;" readonly="true"></td>
	            </tr>
	        </table>
	    </form>
	   
	   	<!-- Tombol Add, Edit, Hapus Datagrid -->
       	<div id="toolbar3">  
            <table align="center" style="padding: 0px; width: 99%;">
                <tr>
                    <td>
                        <a href="javascript:void(0)" class="btn btn-small btn-success" onclick="javascript:add3()"><i class="icon-plus-sign icon-large"></i>&nbsp;Add</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:edit3()"><i class="icon-edit"></i>&nbsp;Edit</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-danger" onclick="javascript:hapus3()"><i class="icon-trash icon-large"></i>&nbsp;Delete</a>
                    </td>
                    <td>&nbsp;</td>
                    <td align="right">
                    	<input class="easyui-searchbox" prompt="Silahkan Input Field" searcher="doSearch3"  style="width:300px"/>
                    </td>
                </tr>
            </table>  
        </div>  
        <!-- end tombol datagrid -->
        <!-- Tombol simpan & Batal Form -->
        <div id="t_dlg_dis-buttons3">
            <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:save3()"><i class="icon-save icon-large"></i>&nbsp;Simpan</a>
            <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:$('#dlg3').dialog('close')"><i class="icon-remove icon-large"></i>&nbsp;Batal</a>      
        </div>
	</div>
	<!-- Model end -->
</div>