<?php
date_default_timezone_set("Asia/Jakarta");

$tittleManager = managerTitle;
$namemanager = managerName;
$url_img = url_img;

$tanggal = date('d F Y');

function tanggal_indo($tanggal)
{
	$bulan_sumi = array (1 =>   'Januari',
				'Februari',
				'Maret',
				'April',
				'Mei',
				'Juni',
				'Juli',
				'Agustus',
				'September',
				'Oktober',
				'November',
				'Desember'
			);
	$split = explode('-', $tanggal);
	return $split[2] . ' ' . $bulan_sumi[ (int)$split[1] ] . ' ' . $split[0];
}

function kekata($x)
{
	$x = abs($x);
	$angka = array(
		"", "satu", "dua", "tiga", "empat", "lima",
		"enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas"
	);
	$temp = "";
	if ($x < 12) {
		$temp = " " . $angka[$x];
	} else if ($x < 20) {
		$temp = kekata($x - 10) . " belas";
	} else if ($x < 100) {
		$temp = kekata($x / 10) . " puluh" . kekata($x % 10);
	} else if ($x < 200) {
		$temp = " seratus" . kekata($x - 100);
	} else if ($x < 1000) {
		$temp = kekata($x / 100) . " ratus" . kekata($x % 100);
	} else if ($x < 2000) {
		$temp = " seribu" . kekata($x - 1000);
	} else if ($x < 1000000) {
		$temp = kekata($x / 1000) . " ribu" . kekata($x % 1000);
	} else if ($x < 1000000000) {
		$temp = kekata($x / 1000000) . " juta" . kekata($x % 1000000);
	} else if ($x < 1000000000000) {
		$temp = kekata($x / 1000000000) . " milyar" . kekata(fmod($x, 1000000000));
	} else if ($x < 1000000000000000) {
		$temp = kekata($x / 1000000000000) . " trilyun" . kekata(fmod($x, 1000000000000));
	}
	return $temp;
}
function terbilang($x, $style = 4)
{
	if ($x < 0) {
		$hasil = "minus " . trim(kekata($x));
	} else {
		$hasil = trim(kekata($x));
	}
	switch ($style) {
		case 1:
			$hasil = strtoupper($hasil);
			break;
		case 2:
			$hasil = strtolower($hasil);
			break;
		case 3:
			$hasil = ucwords($hasil);
			break;
		default:
			$hasil = ucfirst($hasil);
			break;
	}
	return $hasil;
}
$nilai_pengajuan = number_format($vendor['nilai_pengajuan']);
// $harga_penawaran_vendor = number_format($vendor['harga_penawaran_vendor']);
?>

<title>Formulir Survey Mitra</title>
<style>
	u {
		border-bottom: 2px dotted #000;
		text-decoration: none;
	}

	div,
	.hr {

		background-color: white;
		/*width: 100%;*/
		border: 0px solid black;
		padding: 25px;
		margin: 0px;
		font-size: 10;
		padding-bottom: 2px;
		padding-top: 0px;

	}

	hr {
		background-color: white;
		/*width: 100%;*/
		border: 0px solid black;
		padding: 0px;
		margin: 0px;
	}

	h2,
    .title {
		text-align: center;
        font-weight: bold;
	}

    h3
	.title {
		text-align: left;
        font-weight: normal;
	}
    h4
	.title {
		text-align: left;
        font-weight: normal;
	}
    h6 {
        border-top: 1px dashed black;
    }


	table {
		/*width: 100%;*/
		border: 1px solid black;
		border-collapse: collapse;
		padding: 5px;
		/*padding-bottom: 5px;*/
	}

	table,
	td,
	tr {
		border: 0px solid black;
		border-collapse: collapse;
		padding: 2px;
        font-size: 12;
		/*padding-bottom:2px;*/
	}
    br {
        content: "";
        margin: 0.5em;
        display: block;
        }
@media print {
  .new-page {
    page-break-before: always;
  }
}

</style>

<div style="padding-top: 5px;padding-bottom: 0px;">
	<table style="padding-top: 0px;" width="100%" height="40%">
        <tr>
            <td>&nbsp;</td>
        </tr>
        
        <tr>
            <td>&nbsp;</td>
        </tr>
		<tr>
			<td>
                <h2>DATA PEMOHON USAHA KECIL PERORANGAN</h2><br>
                <p style="font-weight: normal;text-align: center; font-size: 9;">UNTUK SEKTOR INDUSTRI, PERDAGANGAN, JASA, PETERNAKAN, PERIKANAN, PERTANIAN, PERKEBUNAN</p><br>
                <h6>&nbsp;</h6>
            </td>
		</tr>
	</table>
    <table>
        <tr>
            <td colspan="5"><h3>A. DATA PENGUSAHA</h3> </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;&nbsp;1.</td>
            <td> Nama Pemohon</td>
            <td>&nbsp;:&nbsp;</td>
            <td><?php echo $vendor['nama']; ?></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;&nbsp;2.</td>
            <td> Alamat Pemohon</td>
            <td>&nbsp;:&nbsp;</td>
            <td><?php echo $vendor['alamat']; ?></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;&nbsp;3.</td>
            <td> Tempat Tanggal Lahir</td>
            <td>&nbsp;:&nbsp;</td>
            <td><?php echo $vendor['tempat_lahir']; ?>, &nbsp;<?php echo tanggal_indo($vendor['tgl_lahir']);?></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;&nbsp;4.</td>
            <td> Pendidikan</td>
            <td>&nbsp;:&nbsp;</td>
            <td><?php echo $vendor['status_pendidikan']; ?></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;&nbsp;5.</td>
            <td> No. KTP</td>
            <td>&nbsp;:&nbsp;</td>
            <td><?php echo $vendor['no_ktp']; ?></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;&nbsp;6.</td>
            <td> Pekerjaan Lain Disamping Usaha Kecil</td>
            <td>&nbsp;:&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;&nbsp;7.</td>
            <td> Jumlah Tanggungan Keluarga</td>
            <td>&nbsp;:&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;&nbsp;8.</td>
            <td> No. Telpon/HP</td>
            <td>&nbsp;:&nbsp;</td>
            <td><?php echo $vendor['handphone']; ?></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;&nbsp;9.</td>
            <td> Jumlah Biaya Hidup Per Bulan</td>
            <td>&nbsp;:&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;&nbsp;10.</td>
            <td>Pendapatan di luar Usaha</td>
            <td>&nbsp;:&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;&nbsp;11.</td>
            <td>Jaminan Berupa</td>
            <td>&nbsp;:&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>
    <table>
        <tr>
            <td colspan="5"><h3>B. DATA PERUSAHAAN</h3> </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;&nbsp;1.</td>
            <td> a. Bidang Usaha</td>
            <td>&nbsp;:&nbsp;</td>
            <td><?php echo $vendor['nama_sektor']; ?></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;&nbsp;</td>
            <td> b. Komoditi</td>
            <td>&nbsp;:&nbsp;</td>
            <td><?php echo $vendor['komoditi']; ?></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;&nbsp;2.</td>
            <td> Alamat Usaha</td>
            <td>&nbsp;:&nbsp;</td>
            <td><?php echo $vendor['alamat_perusahaan']; ?></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;&nbsp;</td>
            <td> a. Desa/Kelurahan</td>
            <td>&nbsp;:&nbsp;</td>
            <td><?php echo $vendor['kelurahan_perusahaan']; ?></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;&nbsp;</td>
            <td> b. Kecamatan</td>
            <td>&nbsp;:&nbsp;</td>
            <td><?php echo $vendor['kecamatan_perusahaan']; ?></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;&nbsp;</td>
            <td> c. Kabupaten</td>
            <td>&nbsp;:&nbsp;</td>
            <td><?php echo $vendor['kota_perusahaan']; ?></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;&nbsp;</td>
            <td> d. Provinsi</td>
            <td>&nbsp;:&nbsp;</td>
            <td><?php echo $vendor['propinsi_perusahaan']; ?></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;&nbsp;3.</td>
            <td> Tempat Usaha</td>
            <td>&nbsp;:&nbsp;</td>
            <td>Milik &nbsp;<?php echo $vendor['status_tempat']; ?></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;&nbsp;4.</td>
            <td> Waktu Mulai Usaha</td>
            <td>&nbsp;:&nbsp;</td>
            <td><?php echo tanggal_indo($vendor['tgl_berdiri']);?></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;&nbsp;5.</td>
            <td> Kemampuan Membayar</td>
            <td>&nbsp;:&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;&nbsp;6.</td>
            <td> Jumlah Tenaga Kerja</td>
            <td>&nbsp;:&nbsp;</td>
            <td>&nbsp;</td>
        </tr>        
    </table>

    <table style="padding-top: 0px;" width="100%" height="40%">
        <tr  width="100%" align="right">
            <td>
            <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u>
            </td>
        </tr>
    </table>
    <br><br><br><br><br><br><br><br>
    <table border="1" width="100%" height="40%" >
        
        <tr  align="center">
            <td width="50%" style="padding-top:50px"><u><?php echo $vendor['petugas_survey'];?></u><br>Surveyor</td>
            <td width="50%" style="padding-top:50px"><u><?php echo $vendor['nama']; ?></u><br>Pemilik Usaha Kecil</td>
            <td width="50%" style="padding-top:50px"><u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u><br>Ahli Waris Pemilik UK</td>
        </tr>
        
    </table>
</div>
<div style = "page-break-before:always;padding-left:0px;">
   <img src="<?= $url_img.'assets/images/page_2.jpg'?>" style="width:750px; height:900px;" align="left">
</div>
<div style = "page-break-before:always;padding-left:0px;">
   <img src="<?= $url_img.'assets/images/page_3.jpg'?>" style="width:750px; height:900px;" align="left">
</div>
<div style = "page-break-before:always;padding-left:0px;">
   <img src="<?= $url_img.'assets/images/page_4.jpg'?>" style="width:750px; height:900px;" align="left">
</div>
<div style = "page-break-before:always;padding-left:0px;">
   <img src="<?= $url_img.'assets/images/page_5.jpg'?>" style="width:750px; height:900px;" align="left">
</div>
<div style = "page-break-before:always;padding-left:0px;">
   <img src="<?= $url_img.'assets/images/page_6.jpg'?>" style="width:750px; height:900px;" align="left">
</div>
<div style = "page-break-before:always;padding-left:0px;">
   <img src="<?= $url_img.'assets/images/page_7.jpg'?>" style="width:750px; height:900px;" align="left">
</div>
