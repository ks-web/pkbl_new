		<style>
			table {
				border-spacing: 0px;
				border-collapse: separate;
				font-size: 12px;
				page-break-inside: avoid;
			}
			
			.page-break {
				page-break-after: always;
			}

			table, th, td {
				border: 0px solid black;
			}

			.bordered, .bordered td {
				border: 1px solid black;
			}
			html {
				margin: 50px 50px 75px;
			}
			.pull-right {
				text-align: right;
			}
			.center {
				text-align: center;
			}
		</style>
		
		<table style="width: 100%;">
			<!-- <thead> -->
				<tr>
					<td class="pull-right">No Reg : <?=$pengusaha['id_mitra']?></td>
				</tr>
				<tr>
					<td>
					<center>
						<h3 style="margin-bottom: 0; margin-top: 0;">Lampiran Hasil Survey (Arsip)</h3>
						<div>
							<strong>Usaha Kecil</strong> : <?=$pengusaha['nama']?>, <strong>Sektor Usaha</strong> : <?=$pengusaha['nama_sektor']?>
							<br />
							<strong>Alamat</strong> : <?=$pengusaha['alamat']?>, <?=$pengusaha['kelurahan']?>, <?=$pengusaha['kecamatan']?>, <?=$pengusaha['kota']?>, <strong>Telp.</strong> <?=$pengusaha['telepon']?>
						</div>
					</center>
					<hr  style="border: 1px solid black;"/>
					</td>
				</tr>
			<!-- </thead> -->
			<!-- <tbody> -->
				<tr>
					<td>
						<strong>1. Asset Usaha :</strong>
					<br />
					<table width="100%" class="bordered">
						<!-- <thead> -->
							<tr>
								<td width="25px" class="center"><strong>No.</strong></td>
								<td class="center"><strong>Uraian</strong></td>
								<td width="40px" class="center"><strong>Unit</strong></td>
								<td width="70px" class="center"><strong>Dimili Sejak</strong></td>
								<td width="90px" class="center"><strong>Nilai Asset</strong></td>
								<td width="80px" class="center"><strong>Biaya Penyusutan</strong></td>
								<td width="80px" class="center"><strong>Biaya Pemeliharaan</strong></td>
							</tr>
						<!-- </thead> -->
						<!-- <tbody> -->
							<?php
							$no = 1;
							$tot_nilai_asset = 0;
							$tot_biaya_penyu = 0;
							$tot_biaya_pemel = 0;
							if($asset_usaha) {
								foreach ($asset_usaha as $row) {
									$tot_nilai_asset += $row['nilai_asset'];
									$tot_biaya_penyu += $row['biaya_penyusutan'];
									$tot_biaya_pemel += $row['biaya_pemeliharaan'];
								?>
							<tr>
								<td width="25px" class="center"><?=$no?></td>
								<td><?=$row['uraian']?></td>
								<td width="40px" class="center"><?=$row['unit']?></td>
								<td width="70px" class="center"><?=$row['kepemilikan']?></td>
								<td width="90px" class="pull-right"><?=number_format($row['nilai_asset'],0,",", ".")?></td>
								<td width="80px" class="pull-right"><?=number_format($row['biaya_penyusutan'],0,",", ".")?></td>
								<td width="80px" class="pull-right"><?=number_format($row['biaya_pemeliharaan'],0,",", ".")?></td>
							</tr>
								<?php
									$no++;
								}
							} else {
								?>
							<tr>
								<td width="25px" class="center"><br/></td>
								<td></td>
								<td width="40px" class="center"></td>
								<td width="70px" class="center"></td>
								<td width="90px" class="pull-right"></td>
								<td width="80px" class="pull-right"></td>
								<td width="80px" class="pull-right"></td>
							</tr>
								<?php
							}
							?>
						<!-- </tbody> -->
						<!-- <tfoot> -->
							<tr>
								<td colspan="4" class="pull-right"><strong>Subtotal :</strong></td>
								<td width="90px" class="pull-right"><strong><?=number_format($tot_nilai_asset,0,",", ".")?></strong></td>
								<td width="80px" class="pull-right"><strong><?=number_format($tot_biaya_penyu,0,",", ".")?></strong></td>
								<td width="80px" class="pull-right"><strong><?=number_format($tot_biaya_pemel,0,",", ".")?></strong></td>
							</tr>
						<!-- </tfoot> -->
					</table>
					<br /><br />
					<!-- End Asset Usaha -->
					<strong>2. Periode Usaha :</strong>
					<br />
					<table width="100%" class="bordered">
						<!-- <thead> -->
							<tr>
								<td width="25px" class="center"><strong>No.</strong></td>
								<td class="center"><strong>Uraian</strong></td>
								<td width="80px" class="center"><strong>Jumlah Hari</strong></td>
							</tr>
						<!-- </thead> -->
						<!-- <tbody> -->
							<?php
							$no = 1;
							$tot_hari = 0;
							if($periode_usaha) {
								foreach ($periode_usaha as $row) {
									$tot_hari += $row['jumlah_hari'];
								?>
							<tr>
								<td width="25px" class="center"><?=$no?></td>
								<td><?=$row['uraian']?></td>
								<td width="80px" class="pull-right"><?=$row['jumlah_hari']?></td>
							</tr>
								<?php
									$no++;
								}
							} else {
								?>
							<tr>
								<td width="25px" class="center"><br /></td>
								<td></td>
								<td width="80px" class="pull-right"></td>
							</tr>
								<?php
							}
							?>
						<!-- </tbody> -->
						<!-- <tfoot> -->
							<tr>
								<td colspan="2" class="pull-right"><strong>Periode Perputaran Produksi [Hari] :</strong></td>
								<td width="80px" class="pull-right"><strong><?=$tot_hari?></strong></td>
							</tr>
						<!-- </tfoot> -->
					</table><br /><br />
					<!-- End Periode Usaha -->
					<strong>3. Produksi dan Penjualan :</strong>
					<br />
					<table width="100%" class="bordered">
						<!-- <thead> -->
							<tr>
								<td rowspan="2" width="25px" class="center"><strong>No.</strong></td>
								<td rowspan="2" class="center"><strong>Jenis Produksi</strong></td>
								<td rowspan="2" width="40px" class="center"><strong>Satuan</strong></td>
								<td rowspan="2" width="70px" class="center"><strong>Harga Satuan</strong></td>
								<td colspan="2" class="center"><strong>Produksi</strong></td>
								<td colspan="2" class="center"><strong>Penjualan</strong></td>
							</tr>
							<tr>
								<td width="45px" class="center"><strong>Aktual</strong></td>
								<td width="45px" class="center"><strong>Proyeksi</strong></td>
								<td width="80px" class="center"><strong>Aktual</strong></td>
								<td width="80px" class="center"><strong>Proyeksi</strong></td>
							</tr>
						<!-- </thead> -->
						<!-- <tbody> -->
							<?php
							$no = 1;
							$tot_prod_akt = 0;
							$tot_prod_pro = 0;
							$tot_penj_akt = 0;
							$tot_penj_pro = 0;
							if($produksi_penjualan) {
								foreach ($produksi_penjualan as $row) {
									$tot_prod_akt += $row['produksi_akt'];
									$tot_prod_pro += $row['produksi_proy'];
									$tot_penj_akt += $row['penjualan_akt'];
									$tot_penj_pro += $row['penjualan_proy'];
								?>
							<tr>
								<td width="25px" class="center"><?=$no?></td>
								<td><?=$row['jenis_produksi']?></td>
								<td width="40px" class="center"><?=$row['satuan']?></td>
								<td width="70px" class="pull-right"><?=number_format($row['harga_satuan'],0,",", ".")?></td>
								<td width="45px" class="pull-right"><?=$row['produksi_akt']?></td>
								<td width="45px" class="pull-right"><?=$row['produksi_proy']?></td>
								<td width="80px" class="pull-right"><?=number_format($row['penjualan_akt'],0,",", ".")?></td>
								<td width="80px" class="pull-right"><?=number_format($row['penjualan_proy'],0,",", ".")?></td>
							</tr>
								<?php
									$no++;
								}
							} else {
								?>
							<tr>
								<td width="25px" class="center"><br /></td>
								<td></td>
								<td width="40px" class="center"></td>
								<td width="70px" class="pull-right"></td>
								<td width="45px" class="pull-right"></td>
								<td width="45px" class="pull-right"></td>
								<td width="80px" class="pull-right"></td>
								<td width="80px" class="pull-right"></td>
							</tr>
								<?php
							}
							?>
						<!-- </tbody> -->
						<!-- <tfoot> -->
							<tr>
								<td colspan="4" class="pull-right"><strong>Subtotal :</strong></td>
								<td width="45px" class="pull-right"><strong><?=$tot_prod_akt?></strong></td>
								<td width="45px" class="pull-right"><strong><?=$tot_prod_pro?></strong></td>
								<td width="80px" class="pull-right"><strong><?=number_format($tot_penj_akt,0,",", ".")?></strong></td>
								<td width="80px" class="pull-right"><strong><?=number_format($tot_penj_pro,0,",", ".")?></strong></td>
							</tr>
						<!-- </tfoot> -->
					</table>
					<br /><br />
					<!-- End Produksi dan Penjualan -->
					<strong>4. Biaya Bahan Baku :</strong>
					<br />
					<table width="100%" class="bordered">
						<!-- <thead> -->
							<tr>
								<td rowspan="2" width="25px" class="center"><strong>No.</strong></td>
								<td rowspan="2" class="center"><strong>Unsur Biaya</strong></td>
								<td rowspan="2" width="40px" class="center"><strong>Satuan</strong></td>
								<td rowspan="2" width="70px" class="center"><strong>Harga Satuan</strong></td>
								<td colspan="2" class="center"><strong>Jumlah Unit</strong></td>
								<td colspan="2" class="center"><strong>Biaya Produksi</strong></td>
							</tr>
							<tr>
								<td width="45px" class="center"><strong>Aktual</strong></td>
								<td width="45px" class="center"><strong>Proyeksi</strong></td>
								<td width="80px" class="center"><strong>Aktual</strong></td>
								<td width="80px" class="center"><strong>Proyeksi</strong></td>
							</tr>
						<!-- </thead> -->
						<!-- <tbody> -->
							<?php
							$no = 1;
							$tot_prod_akt = 0;
							$tot_prod_pro = 0;
							$tot_penj_akt = 0;
							$tot_penj_pro = 0;
							if($bahan_baku) {
								foreach ($bahan_baku as $row) {
									$tot_prod_akt += $row['junit_akt'];
									$tot_prod_pro += $row['junit_proy'];
									$tot_penj_akt += $row['bprod_akt'];
									$tot_penj_pro += $row['bprod_proy'];
								?>
							<tr>
								<td width="25px" class="center"><?=$no?></td>
								<td><?=$row['unsur_biaya']?></td>
								<td width="40px" class="center"><?=$row['satuan']?></td>
								<td width="70px" class="pull-right"><?=number_format($row['harga_satuan'],0,",", ".")?></td>
								<td width="45px" class="pull-right"><?=$row['junit_akt']?></td>
								<td width="45px" class="pull-right"><?=$row['junit_proy']?></td>
								<td width="80px" class="pull-right"><?=number_format($row['bprod_akt'],0,",", ".")?></td>
								<td width="80px" class="pull-right"><?=number_format($row['bprod_proy'],0,",", ".")?></td>
							</tr>
								<?php
									$no++;
								}
							} else {
								?>
							<tr>
								<td width="25px" class="center"><br /></td>
								<td></td>
								<td width="40px" class="center"></td>
								<td width="70px" class="pull-right"></td>
								<td width="45px" class="pull-right"></td>
								<td width="45px" class="pull-right"></td>
								<td width="80px" class="pull-right"></td>
								<td width="80px" class="pull-right"></td>
							</tr>
								<?php
							}
							?>
						<!-- </tbody> -->
						<!-- <tfoot> -->
							<tr>
								<td colspan="4" class="pull-right"><strong>Subtotal :</strong></td>
								<td width="45px" class="pull-right"><strong><?=$tot_prod_akt?></strong></td>
								<td width="45px" class="pull-right"><strong><?=$tot_prod_pro?></strong></td>
								<td width="80px" class="pull-right"><strong><?=number_format($tot_penj_akt,0,",", ".")?></strong></td>
								<td width="80px" class="pull-right"><strong><?=number_format($tot_penj_pro,0,",", ".")?></strong></td>
							</tr>
						<!-- </tfoot> -->
					</table>
					<br /><br />
					<!-- End Biaya Bahan Baku -->
					<strong>5. Biaya Bahan Bantu :</strong>
					<br />
					<table width="100%" class="bordered">
						<!-- <thead> -->
							<tr>
								<td rowspan="2" width="25px" class="center"><strong>No.</strong></td>
								<td rowspan="2" class="center"><strong>Unsur Biaya</strong></td>
								<td rowspan="2" width="40px" class="center"><strong>Satuan</strong></td>
								<td rowspan="2" width="70px" class="center"><strong>Harga Satuan</strong></td>
								<td colspan="2" class="center"><strong>Jumlah Unit</strong></td>
								<td colspan="2" class="center"><strong>Biaya Produksi</strong></td>
							</tr>
							<tr>
								<td width="45px" class="center"><strong>Aktual</strong></td>
								<td width="45px" class="center"><strong>Proyeksi</strong></td>
								<td width="80px" class="center"><strong>Aktual</strong></td>
								<td width="80px" class="center"><strong>Proyeksi</strong></td>
							</tr>
						<!-- </thead> -->
						<!-- <tbody> -->
							<?php
							$no = 1;
							$tot_prod_akt = 0;
							$tot_prod_pro = 0;
							$tot_penj_akt = 0;
							$tot_penj_pro = 0;
							if($bahan_bantu) {
								foreach ($bahan_bantu as $row) {
									$tot_prod_akt += $row['junit_akt'];
									$tot_prod_pro += $row['junit_proy'];
									$tot_penj_akt += $row['bprod_akt'];
									$tot_penj_pro += $row['bprod_proy'];
								?>
							<tr>
								<td width="25px" class="center"><?=$no?></td>
								<td><?=$row['unsur_biaya']?></td>
								<td width="40px" class="center"><?=$row['satuan']?></td>
								<td width="70px" class="pull-right"><?=number_format($row['harga_satuan'],0,",", ".")?></td>
								<td width="45px" class="pull-right"><?=$row['junit_akt']?></td>
								<td width="45px" class="pull-right"><?=$row['junit_proy']?></td>
								<td width="80px" class="pull-right"><?=number_format($row['bprod_akt'],0,",", ".")?></td>
								<td width="80px" class="pull-right"><?=number_format($row['bprod_proy'],0,",", ".")?></td>
							</tr>
								<?php
									$no++;
								}
							} else {
								?>
							<tr>
								<td width="25px" class="center"><br /></td>
								<td></td>
								<td width="40px" class="center"></td>
								<td width="70px" class="pull-right"></td>
								<td width="45px" class="pull-right"></td>
								<td width="45px" class="pull-right"></td>
								<td width="80px" class="pull-right"></td>
								<td width="80px" class="pull-right"></td>
							</tr>
								<?php
							}
							?>
						<!-- </tbody> -->
						<!-- <tfoot> -->
							<tr>
								<td colspan="4" class="pull-right"><strong>Subtotal :</strong></td>
								<td width="45px" class="pull-right"><strong><?=$tot_prod_akt?></strong></td>
								<td width="45px" class="pull-right"><strong><?=$tot_prod_pro?></strong></td>
								<td width="80px" class="pull-right"><strong><?=number_format($tot_penj_akt,0,",", ".")?></strong></td>
								<td width="80px" class="pull-right"><strong><?=number_format($tot_penj_pro,0,",", ".")?></strong></td>
							</tr>
						<!-- </tfoot> -->
					</table>
					<br /><br />
					<!-- End Biaya Bahan Bantu -->
					<!-- <div class="page-break"></div> -->
					<strong>6. Tenaga Kerja Tetap :</strong>
					<br />
					<table width="100%" class="bordered">
						<!-- <thead> -->
							<tr>
								<td width="25px" class="center"><strong>No.</strong></td>
								<td class="center"><strong>Bagian</strong></td>
								<td width="45px" class="center"><strong>Unit</strong></td>
								<td width="80px" class="center"><strong>Upah per Bulan</strong></td>
								<td width="80px" class="center"><strong>Jumlah Upah</strong></td>
							</tr>
						<!-- </thead> -->
						<!-- <tbody> -->
							<?php
							$no = 1;
							$tot_upah = 0;
							if($tenaga_kerja_tetap) {
								foreach ($tenaga_kerja_tetap as $row) {
									$tot_upah += $row['jumlah_upah'];
								?>
							<tr>
								<td width="25px" class="center"><?=$no?></td>
								<td><?=$row['bagian']?></td>
								<td width="45px" class="pull-right"><?=$row['unit']?></td>
								<td width="80px" class="pull-right"><?=number_format($row['upah_perbulan'],0,",", ".")?></td>
								<td width="80px" class="pull-right"><?=number_format($row['jumlah_upah'],0,",", ".")?></td>
							</tr>
								<?php
									$no++;
								}
							} else {
								?>
							<tr>
								<td width="25px" class="center"><br /></td>
								<td></td>
								<td width="45px" class="pull-right"></td>
								<td width="80px" class="pull-right"></td>
								<td width="80px" class="pull-right"></td>
							</tr>
								<?php
							}
							?>
						<!-- </tbody> -->
						<!-- <tfoot> -->
							<tr>
								<td colspan="4" class="pull-right"><strong>Subtotal :</strong></td>
								<td width="80px" class="pull-right"><strong><?=number_format($tot_upah,0,",", ".")?></strong></td>
							</tr>
						<!-- </tfoot> -->
					</table>
					<br /><br />
					<!-- End Tenaga Kerja Tetap -->
					<div class="page-break"></div>
					<strong>7. Tenaga Kerja Tidak Tetap :</strong>
					<br />
					<table width="100%" class="bordered">
						<!-- <thead> -->
							<tr>
								<td rowspan="2" width="25px" class="center"><strong>No.</strong></td>
								<td rowspan="2" class="center"><strong>Uraian</strong></td>
								<td rowspan="2" width="40px" class="center"><strong>Satuan</strong></td>
								<td rowspan="2" width="70px" class="center"><strong>Harga Satuan</strong></td>
								<td colspan="2" class="center"><strong>Produksi</strong></td>
								<td colspan="2" class="center"><strong>Penjualan</strong></td>
							</tr>
							<tr>
								<td width="45px" class="center"><strong>Aktual</strong></td>
								<td width="45px" class="center"><strong>Proyeksi</strong></td>
								<td width="80px" class="center"><strong>Aktual</strong></td>
								<td width="80px" class="center"><strong>Proyeksi</strong></td>
							</tr>
						<!-- </thead> -->
						<!-- <tbody> -->
							<?php
							$no = 1;
							$tot_prod_akt = 0;
							$tot_prod_pro = 0;
							$tot_penj_akt = 0;
							$tot_penj_pro = 0;
							if($tenaga_kerja_tidak_tetap) {
								foreach ($tenaga_kerja_tidak_tetap as $row) {
									$tot_prod_akt += $row['produksi_akt'];
									$tot_prod_pro += $row['produksi_proy'];
									$tot_penj_akt += $row['penjualan_akt'];
									$tot_penj_pro += $row['penjualan_proy'];
								?>
							<tr>
								<td width="25px" class="center"><?=$no?></td>
								<td><?=$row['uraian']?></td>
								<td width="40px" class="center"><?=$row['satuan']?></td>
								<td width="70px" class="pull-right"><?=number_format($row['harga_satuan'],0,",", ".")?></td>
								<td width="45px" class="pull-right"><?=$row['produksi_akt']?></td>
								<td width="45px" class="pull-right"><?=$row['produksi_proy']?></td>
								<td width="80px" class="pull-right"><?=number_format($row['penjualan_akt'],0,",", ".")?></td>
								<td width="80px" class="pull-right"><?=number_format($row['penjualan_proy'],0,",", ".")?></td>
							</tr>
								<?php
									$no++;
								}
							} else {
								?>
							<tr>
								<td width="25px" class="center"><br /></td>
								<td></td>
								<td width="40px" class="center"></td>
								<td width="70px" class="pull-right"></td>
								<td width="45px" class="pull-right"></td>
								<td width="45px" class="pull-right"></td>
								<td width="80px" class="pull-right"></td>
								<td width="80px" class="pull-right"></td>
							</tr>
								<?php
							}
							?>
						<!-- </tbody> -->
						<!-- <tfoot> -->
							<tr>
								<td colspan="4" class="pull-right"><strong>Subtotal :</strong></td>
								<td width="45px" class="pull-right"><strong><?=$tot_prod_akt?></strong></td>
								<td width="45px" class="pull-right"><strong><?=$tot_prod_pro?></strong></td>
								<td width="80px" class="pull-right"><strong><?=number_format($tot_penj_akt,0,",", ".")?></strong></td>
								<td width="80px" class="pull-right"><strong><?=number_format($tot_penj_pro,0,",", ".")?></strong></td>
							</tr>
						<!-- </tfoot> -->
					</table>
					<br /><br />
					<!-- End Biaya Bahan Bantu -->
					<strong>8. Biaya transportasi :</strong>
					<br />
					<table width="100%" class="bordered">
						<!-- <thead> -->
							<tr>
								<td rowspan="2" width="25px" class="center"><strong>No.</strong></td>
								<td rowspan="2" class="center"><strong>Unsur Biaya</strong></td>
								<td rowspan="2" width="40px" class="center"><strong>Satuan</strong></td>
								<td rowspan="2" width="70px" class="center"><strong>Harga Satuan</strong></td>
								<td colspan="2" class="center"><strong>Jumlah Unit</strong></td>
								<td colspan="2" class="center"><strong>Biaya Produksi</strong></td>
							</tr>
							<tr>
								<td width="45px" class="center"><strong>Aktual</strong></td>
								<td width="45px" class="center"><strong>Proyeksi</strong></td>
								<td width="80px" class="center"><strong>Aktual</strong></td>
								<td width="80px" class="center"><strong>Proyeksi</strong></td>
							</tr>
						<!-- </thead> -->
						<!-- <tbody> -->
							<?php
							$no = 1;
							$tot_prod_akt = 0;
							$tot_prod_pro = 0;
							$tot_penj_akt = 0;
							$tot_penj_pro = 0;
							if($biaya_transport) {
								foreach ($biaya_transport as $row) {
									$tot_prod_akt += $row['unit_akt'];
									$tot_prod_pro += $row['unit_proy'];
									$tot_penj_akt += $row['produksi_akt'];
									$tot_penj_pro += $row['produksi_proy'];
								?>
							<tr>
								<td width="25px" class="center"><?=$no?></td>
								<td><?=$row['uraian']?></td>
								<td width="40px" class="center"><?=$row['satuan']?></td>
								<td width="70px" class="pull-right"><?=number_format($row['harga_satuan'],0,",", ".")?></td>
								<td width="45px" class="pull-right"><?=$row['unit_akt']?></td>
								<td width="45px" class="pull-right"><?=$row['unit_proy']?></td>
								<td width="80px" class="pull-right"><?=number_format($row['produksi_akt'],0,",", ".")?></td>
								<td width="80px" class="pull-right"><?=number_format($row['produksi_proy'],0,",", ".")?></td>
							</tr>
								<?php
									$no++;
								}
							} else {
								?>
							<tr>
								<td width="25px" class="center"><br /></td>
								<td></td>
								<td width="40px" class="center"></td>
								<td width="70px" class="pull-right"></td>
								<td width="45px" class="pull-right"></td>
								<td width="45px" class="pull-right"></td>
								<td width="80px" class="pull-right"></td>
								<td width="80px" class="pull-right"></td>
							</tr>
								<?php
							}
							?>
						<!-- </tbody> -->
						<!-- <tfoot> -->
							<tr>
								<td colspan="4" class="pull-right"><strong>Subtotal :</strong></td>
								<td width="45px" class="pull-right"><strong><?=$tot_prod_akt?></strong></td>
								<td width="45px" class="pull-right"><strong><?=$tot_prod_pro?></strong></td>
								<td width="80px" class="pull-right"><strong><?=number_format($tot_penj_akt,0,",", ".")?></strong></td>
								<td width="80px" class="pull-right"><strong><?=number_format($tot_penj_pro,0,",", ".")?></strong></td>
							</tr>
						<!-- </tfoot> -->
					</table>
					<br /><br />
					<!-- End Biaya transportasi -->
					<strong>9. Biaya Administrasi dan Umum :</strong>
					<br />
					<table width="100%" class="bordered">
						<!-- <thead> -->
							<tr>
								<td width="25px" class="center"><strong>No.</strong></td>
								<td class="center"><strong>Uraian</strong></td>
								<td width="80px" class="center"><strong>Biaya</strong></td>
							</tr>
						<!-- </thead> -->
						<!-- <tbody> -->
							<?php
							$no = 1;
							$tot = 0;
							if($biaya_adm_umum) {
								foreach ($biaya_adm_umum as $row) {
									$tot += $row['biaya_periode'];
								?>
							<tr>
								<td width="25px" class="center"><?=$no?></td>
								<td><?=$row['jenis_biaya']?></td>
								<td width="80px" class="pull-right"><?=$row['biaya_periode']?></td>
							</tr>
								<?php
									$no++;
								}
							} else {
								?>
							<tr>
								<td width="25px" class="center"><br /></td>
								<td></td>
								<td width="80px" class="pull-right"></td>
							</tr>
								<?php
							}
							?>
						<!-- </tbody> -->
						<!-- <tfoot> -->
							<tr>
								<td colspan="2" class="pull-right"><strong>Subtotal :</strong></td>
								<td width="80px" class="pull-right"><strong><?=$tot?></strong></td>
							</tr>
						<!-- </tfoot> -->
					</table><br /><br />
					<!-- End Periode Usaha -->
					<strong>10. Biaya Lain-lain :</strong>
					<br />
					<table width="100%" class="bordered">
						<!-- <thead> -->
							<tr>
								<td rowspan="2" width="25px" class="center"><strong>No.</strong></td>
								<td rowspan="2" class="center"><strong>Unsur Biaya</strong></td>
								<td rowspan="2" width="40px" class="center"><strong>Satuan</strong></td>
								<td rowspan="2" width="70px" class="center"><strong>Harga Satuan</strong></td>
								<td colspan="2" class="center"><strong>Unit</strong></td>
								<td colspan="2" class="center"><strong>Biaya</strong></td>
							</tr>
							<tr>
								<td width="45px" class="center"><strong>Aktual</strong></td>
								<td width="45px" class="center"><strong>Proyeksi</strong></td>
								<td width="80px" class="center"><strong>Aktual</strong></td>
								<td width="80px" class="center"><strong>Proyeksi</strong></td>
							</tr>
						<!-- </thead> -->
						<!-- <tbody> -->
							<?php
							$no = 1;
							$tot_prod_akt = 0;
							$tot_prod_pro = 0;
							$tot_penj_akt = 0;
							$tot_penj_pro = 0;
							if($biaya_lain2) {
								foreach ($biaya_lain2 as $row) {
									$tot_prod_akt += $row['junit_akt'];
									$tot_prod_pro += $row['junit_proy'];
									$tot_penj_akt += $row['biaya_akt'];
									$tot_penj_pro += $row['biaya_proy'];
								?>
							<tr>
								<td width="25px" class="center"><?=$no?></td>
								<td><?=$row['unsur_biaya']?></td>
								<td width="40px" class="center"><?=$row['satuan']?></td>
								<td width="70px" class="pull-right"><?=number_format($row['harga_satuan'],0,",", ".")?></td>
								<td width="45px" class="pull-right"><?=$row['junit_akt']?></td>
								<td width="45px" class="pull-right"><?=$row['junit_proy']?></td>
								<td width="80px" class="pull-right"><?=number_format($row['biaya_akt'],0,",", ".")?></td>
								<td width="80px" class="pull-right"><?=number_format($row['biaya_proy'],0,",", ".")?></td>
							</tr>
								<?php
									$no++;
								}
							} else {
								?>
							<tr>
								<td width="25px" class="center"><br /></td>
								<td></td>
								<td width="40px" class="center"></td>
								<td width="70px" class="pull-right"></td>
								<td width="45px" class="pull-right"></td>
								<td width="45px" class="pull-right"></td>
								<td width="80px" class="pull-right"></td>
								<td width="80px" class="pull-right"></td>
							</tr>
								<?php
							}
							?>
						<!-- </tbody> -->
						<!-- <tfoot> -->
							<tr>
								<td colspan="4" class="pull-right"><strong>Subtotal :</strong></td>
								<td width="45px" class="pull-right"><strong><?=$tot_prod_akt?></strong></td>
								<td width="45px" class="pull-right"><strong><?=$tot_prod_pro?></strong></td>
								<td width="80px" class="pull-right"><strong><?=number_format($tot_penj_akt,0,",", ".")?></strong></td>
								<td width="80px" class="pull-right"><strong><?=number_format($tot_penj_pro,0,",", ".")?></strong></td>
							</tr>
						<!-- </tfoot> -->
					</table>
					<br /><br />
					<!-- End Biaya Lain-lain -->
					<strong>11. Activa :</strong>
					<br />
					<table width="100%" class="bordered">
						<!-- <thead> -->
							<tr>
								<td width="25px" class="center"><strong>No.</strong></td>
								<td class="center"><strong>Jenis Activa</strong></td>
								<td width="80px" class="center"><strong>Nilai</strong></td>
							</tr>
						<!-- </thead> -->
						<!-- <tbody> -->
							<?php
							$no = 1;
							$tot = 0;
							if($aktiva) {
								foreach ($aktiva as $row) {
									$tot += $row['total'];
								?>
							<tr>
								<td width="25px" class="center"><?=$no?></td>
								<td><?=$row['jenis_aktiva']?></td>
								<td width="80px" class="pull-right"><?=number_format($row['total'],0,",", ".")?></td>
							</tr>
								<?php
									$no++;
								}
							} else {
								?>
							<tr>
								<td width="25px" class="center"><br /></td>
								<td></td>
								<td width="80px" class="pull-right"></td>
							</tr>
								<?php
							}
							?>
						<!-- </tbody> -->
						<!-- <tfoot> -->
							<tr>
								<td colspan="2" class="pull-right"><strong>Subtotal :</strong></td>
								<td width="80px" class="pull-right"><strong><?=number_format($tot,0,",", ".")?></strong></td>
							</tr>
						<!-- </tfoot> -->
					</table><br /><br />
					<!-- End Activa -->
					<strong>12. Pasiva :</strong>
					<br />
					<table width="100%" class="bordered">
						<!-- <thead> -->
							<tr>
								<td width="25px" class="center"><strong>No.</strong></td>
								<td class="center"><strong>Jenis Pasiva</strong></td>
								<td width="80px" class="center"><strong>Nilai</strong></td>
							</tr>
						<!-- </thead> -->
						<!-- <tbody> -->
							<?php
							$no = 1;
							$tot = 0;
							if($pasiva) {
								foreach ($pasiva as $row) {
									$tot += $row['total'];
								?>
							<tr>
								<td width="25px" class="center"><?=$no?></td>
								<td><?=$row['jenis_pasiva']?></td>
								<td width="80px" class="pull-right"><?=number_format($row['total'],0,",", ".")?></td>
							</tr>
								<?php
									$no++;
								}
							} else {
								?>
							<tr>
								<td width="25px" class="center"><br /></td>
								<td></td>
								<td width="80px" class="pull-right"></td>
							</tr>
								<?php
							}
							?>
						<!-- </tbody> -->
						<!-- <tfoot> -->
							<tr>
								<td colspan="2" class="pull-right"><strong>Subtotal :</strong></td>
								<td width="80px" class="pull-right"><strong><?=number_format($tot,0,",", ".")?></strong></td>
							</tr>
						<!-- </tfoot> -->
					</table><br /><br />
					<!-- End Pasiva -->

					<strong>13. Kebutuhan Tambahan Investasi :</strong>
					<br />
					<table width="100%" class="bordered">
						<!-- <thead> -->
							<tr>
								<td width="25px" class="center"><strong>No.</strong></td>
								<td class="center"><strong>Kebutuhan Investasi</strong></td>
								<td width="80px" class="center"><strong>Unit</strong></td>
								<td width="80px" class="center"><strong>Satuan</strong></td>
								<td width="80px" class="center"><strong>Harga Satuan</strong></td>
								<td width="80px" class="center"><strong>Jumlah</strong></td>
							</tr>
						<!-- </thead> -->
						<!-- <tbody> -->
							<?php
							$no = 1;
							$tot = 0;
							if($data_invest) {
								foreach ($data_invest as $row) {
									$tot += $row['jumlah'];
								?>
							<tr>
								<td width="25px" class="center"><?=$no?></td>
								<td><?=$row['kebutuhan_investasi']?></td>
								<td><?=$row['unit']?></td>
								<td><?=$row['satuan']?></td>
								<td><?=$row['harga_satuan']?></td>
								<td width="80px" class="pull-right"><?=number_format($row['jumlah'],0,",", ".")?></td>
							</tr>
								<?php
									$no++;
								}
							} else {
								?>
							<tr>
								<td width="25px" class="center"><br /></td>
								<td></td>
								<td width="80px" class="pull-right"></td>
							</tr>
								<?php
							}
							?>
						<!-- </tbody> -->
						<!-- <tfoot> -->
							<tr>
								<td colspan="4" class="pull-right"><strong>Subtotal :</strong></td>
								<td width="80px" class="pull-right"><strong><?=number_format($tot,0,",", ".")?></strong></td>
							</tr>
						<!-- </tfoot> -->
					</table><br /><br />
					<!-- End invest -->



					<br /><br /><br />
					<hr  style="border: 1px solid black;"/>
					<table style="width: 100%;">
						<!-- <tbody> -->
							<tr>
								<td width="15px"><strong></strong></td>
								<td><strong></strong></td>
								<td width="200px" class="center"></td>
								<td width="200px" class="center"><strong><?=$kota?>, <?=tanggal_display($pengusaha['tanggal_create'])?></strong></td>
							</tr>
							<tr>
								<td width="15px"><strong></strong></td>
								<td><strong></strong></td>
								<td width="200px" class="center"></td>
								<td width="200px" class="center">Disiapkan Oleh:</td>
							</tr>
							<tr>
								<td ><strong></strong></td>
								<td><strong></strong></td>
								<td width="200px" class="center"></td>
								<td width="200px" class="center"><br /><br /></td>
							</tr>
							<tr>
								<td ><strong></strong></td>
								<td><strong></strong></td>
								<td width="200px" class="center"></td>
								<td width="200px" class="center"><strong><u><?=$pengusaha['nama_pegawai']?></u></strong></td>
							</tr>
							<tr>
								<td><strong></strong></td>
								<td><strong></strong></td>
								<td width="200px" class="center"></td>
								<td width="200px" class="center">Surveyor</td>
							</tr>
						<!-- </tbody> -->
					</table>
					</td>
				</tr>
			<!-- </tbody> -->
			<!-- <tfoot> -->
				<tr>
					<td></td>
				</tr>
			<!-- </tfoot> -->
		</table>

