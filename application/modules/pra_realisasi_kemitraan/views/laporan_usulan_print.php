<?php
	$no			= 1;
	$bulan  	= date('m');
	$bulanList 	= array('01' => 'Januari', '02' => 'Februari', '03' => 'Maret', '04' => 'April', '05' => 'Mei', '06' => 'Juni', '07' => 'Juli', '08' => 'Augustus', '09' => 'September', '10' => 'October.', '11' => 'Nopember', '12' => 'Desember');
?>
<!DOCTYPE html>
<html>
<head>
	<title>Report Pengukuran Nilai Jaminan</title>
	<style type="text/css">
		.judul {font-weight:bold;color:#000000;letter-spacing:0pt;word-spacing:2pt;font-size:26px;text-align:center;font-family:Arial, sans-serif; line-height:1;}
		.teks_atas
		{
			font-size:10px;
		}
		.teks_nomor_kontrak
		{
			text-align:center;
			font-weight:bold;
			font-size:14px;
		}
		
		.tg  {border-collapse:collapse;border-spacing:0;}
		.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
		.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
		.tg .tg-qqdn{font-weight:bold;font-size:14px;text-align:right;vertical-align:top}
		.tg .tg-d55q{font-weight:bold;font-size:14px;vertical-align:middle}
		.tg .tg-ecrz{font-size:14px;vertical-align:top}
		.tg .tg-07dj{font-weight:bold;font-size:14px}
		
	</style>
</head>
<body>
	<p class="teks_atas">
		PT. Krakatau Steel (Persero) Tbk.<br>
		Divisi Community Development.<br>
	</p>
	<p>
		<font class="judul">
			Usulan Pinjaman Modal Kepada Usaha Kecil
		</font>
		<br>
		<font class="teks_nomor_kontrak">
			No.<?php echo $data['id_mitra'] ?>/USL/PKBL-KS/2016
		</font>
	</p>
	
	<br><br><br>
	
	<div>
		<table class="tg">
			<tr>
				<th class="tg-07dj">No</th>
				<th class="tg-d55q">No. Reg</th>
				<th class="tg-d55q">Nama Perusahaan</th>
				<th class="tg-d55q">Sektor Usaha</th>
				<th class="tg-d55q">Alamat</th>
				<th class="tg-d55q">Kota / Kab</th>
				<th class="tg-d55q">Asset</th>
				<th class="tg-d55q">Omset per Bulan</th>
				<th class="tg-d55q">TKJ</th>
				<th class="tg-d55q">Pinjaman</th>
				<th class="tg-d55q">Tahun</th>
				<th class="tg-d55q" colspan="2">Jasa Admin</th>
				<th class="tg-d55q">Angsuran</th>
			</tr>
			<tr>
				<td class="tg-ecrz"><?php echo $no++ ?></td>
				<td class="tg-ecrz"><?php echo $data['id_mitra'] ?></td>
				<td class="tg-ecrz"><?php echo $data['nama'] ?></td>
				<td class="tg-ecrz"><?php echo $data['sektor_usaha']?></td>
				<td class="tg-ecrz"><?php echo $data['alamat'] ?></td>
				<td class="tg-ecrz"><?php echo $data['kota'] ?></td>
				<td class="tg-ecrz"><?php  ?></td>
				<td class="tg-ecrz"><?php  ?></td>
				<td class="tg-ecrz"><?php echo $data['tenagakerja'] ?></td>
				<td class="tg-ecrz"><?php echo number_format($data['angsuran_pokok'],0,',','.') ?></td>
				<td class="tg-ecrz"><?php echo $data['tahun'] ?></td>
				<td class="tg-ecrz"><?php echo number_format($data['bunga'],0,',','.') ?></td>
				<td class="tg-ecrz"><?php echo number_format($data['total_bunga'],0,',','.') ?></td>
				<td class="tg-ecrz"><?php echo number_format($data['angsuran_bulanan'],0,',','.') ?></td>
			</tr>
			<tr>
				<td class="tg-qqdn" colspan="8">Jumlah</td>
				<td class="tg-ecrz" colspan="2"></td>
				<td class="tg-ecrz" colspan="3"></td>
				<td class="tg-ecrz"></td>
			</tr>
		</table>
		
		<br><br><br><br>
		
		<table border="0" width="100%">
			<tr>
				<td width="35%" style="text-align:center">
					Menyetujui,<br>
					Dinas Community Development
					<br><br><br><br><br>
					<font style="text-decoration: underline;">Syarif Rahman</font><br>
					Manager
				</td>
				<td width="30%"></td>
				<td width="35%" style="text-align:center">
					Cilegon, <?php echo date("d") .' '.$bulanList[$bulan] .' '. date("Y") ?><br>
					Disiapkan Oleh<br>
					Dinas Community Development
					<br><br><br><br>
					<font style="text-decoration: underline;">Jamal Abdul Nasir</font><br>
					Superintendent
				</td>
			</tr>
			
		</table>
	</div>
	
		
</body>
</html>