<?php
  date_default_timezone_set("Asia/Jakarta");
    $tanggal = date('d F Y');
    //$tgl=$vendor['tgl_berdiri'];
    $tgl    = $kontrak_mitra['tanggal_input'];
    $hari   = date('d',strtotime($tgl));
    $bulan  = date('m',strtotime($tgl));
    // echo $bulan;
    $year   = date('Y',strtotime($tgl));
    $day    = date('D', strtotime($tanggal));
    $dayList = array(
        'Sun' => 'Minggu',
        'Mon' => 'Senin',
        'Tue' => 'Selasa',
        'Wed' => 'Rabu',
        'Thu' => 'Kamis',
        'Fri' => 'Jumat',
        'Sat' => 'Sabtu'
    );
    $bulanList = array('01' => 'Januari', '02' => 'Februari', '03' => 'Maret', '04' => 'April', '05' => 'Mei', '06' => 'Juni', '07' => 'Juli', '08' => 'Augustus', '09' => 'September', '10' => 'October.', '11' => 'Nopember', '12' => 'Desember');
    $datapemohon=$kontrak_mitra['alamat']." Dalam Hal Ini Diwakili Oleh <strong>".$kontrak_mitra['nama']."</strong> Nomor KTP ".$kontrak_mitra['no_ktp']."
     Lahir Di Lebak Tanggal 6 Pebruari 1980, alamat rumah di ".$kontrak_mitra['alamat']." dalam hal ini bertindak atas nama pimpinan <strong>".$kontrak_mitra['nama']."</strong>
     yang bergerak pada sektor ".$kontrak_mitra['nama_perusahaan'].", dari dan oleh sebab itu bertindak untuk dan atas nama usaha kecil ".$kontrak_mitra['nama_perusahaan']." selanjutnya disebut <strong>PIHAK KEDUA</strong>";
/* 
function kekata($x) {
        $x = abs($x);
        $angka = array("", "satu", "dua", "tiga", "empat", "lima",
        "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
        $temp = "";
        if ($x <12) {
            $temp = " ". $angka[$x];
        } else if ($x <20) {
            $temp = kekata($x - 10). " belas";
        } else if ($x <100) {
            $temp = kekata($x/10)." puluh". kekata($x % 10);
        } else if ($x <200) {
            $temp = " seratus" . kekata($x - 100);
        } else if ($x <1000) {
            $temp = kekata($x/100) . " ratus" . kekata($x % 100);
        } else if ($x <2000) {
            $temp = " seribu" . kekata($x - 1000);
        } else if ($x <1000000) {
            $temp = kekata($x/1000) . " ribu" . kekata($x % 1000);
        } else if ($x <1000000000) {
            $temp = kekata($x/1000000) . " juta" . kekata($x % 1000000);
        } else if ($x <1000000000000) {
            $temp = kekata($x/1000000000) . " milyar" . kekata(fmod($x,1000000000));
        } else if ($x <1000000000000000) {
            $temp = kekata($x/1000000000000) . " trilyun" . kekata(fmod($x,1000000000000));
        }     
            return $temp;
    }
    function terbilang($x, $style=4) {
        if($x<0) {
            $hasil = "minus ". trim(kekata($x));
        } else {
            $hasil = trim(kekata($x));
        }     
        switch ($style) {
            case 1:
                $hasil = strtoupper($hasil);
                break;
            case 2:
                $hasil = strtolower($hasil);
                break;
            case 3:
                $hasil = ucwords($hasil);
                break;
            default:
                $hasil = ucfirst($hasil);
                break;
        }     
        return $hasil;
    }
    $nilai_pengajuan = number_format($vendor['nilai_pengajuan']);*/
    // $harga_penawaran_vendor = number_format($vendor['harga_penawaran_vendor']);
    
    
?>
<!DOCTYPE html>
<html>
<head>
	<title>Kontrak Kerjasama Bina Lingkungan<br>PT Krakatau Steel Tbk</title>
	<style>

	u {    
    border-bottom: 1px dotted #000;
    text-decoration: none;
}
		div, .hr{

		    background-color: white;
		    /*width: 100%;*/
		    border: 0px solid black;
		    padding: 25px;
		    margin: 1px;
		    font-size: 10;
			padding-bottom:2px;
			padding-top: 0px;

		}
		hr{
		    background-color: white;
		    /*width: 100%;*/
		    border: 0px solid black;
		    padding: 0px;
		    margin: 0px;
		}
		h3, .title{
			text-align: center;
		}	
		table{
		    /*width: 100%;*/
		    border: 0px solid black;
		    border-collapse: collapse;
		    padding: 5px;
		    /*padding-bottom: 5px;*/
		}
		table,td,tr{
		    border: 0px solid black;
		    border-collapse: collapse;
		    padding: 1px;
			/*padding-bottom:2px;*/

		}	
					
	</style>
</head>
<body>
<div style="padding-top: 5px;padding-bottom: 0px;">
    <table style=" padding-top: 0px;" width="100%" height:40%>
        <tr>
            <td align="center" colspan="2" style="border-top:1px;border-bottom: 1px; padding-left: 5px;vertical-align: justify;padding-top: 5px;"><strong>Perjanjian Antara</strong></td>
        </tr>
        <tr>
            <td align="center" colspan="2" style="border-top:1px;border-bottom: 1px; padding-left: 5px;vertical-align: justify;padding-top: 5px;"><strong>PT. Krakatau Steel Tbk.</strong></td>
        </tr>
        <tr>
            <td align="center" colspan="2" style="border-top:1px;border-bottom: 1px; padding-left: 5px;vertical-align: justify;padding-top: 5px;"><strong>Dengan</strong></td>
        </tr>
        <tr>
            <td align="center" colspan="2" style="border-top:1px;border-bottom: 1px; padding-left: 5px;vertical-align: justify;padding-top: 5px;"><strong>Usaha Kecil <?php echo $kontrak_mitra['nama_perusahaan'];?></strong></td>
        </tr>
        <tr>
            <td align="center" colspan="2" style="border-top:1px;border-bottom: 1px; padding-left: 5px;vertical-align: justify;padding-top: 5px;"><strong>Tentang</strong></td>
        </tr>
        <tr>
            <td align="center" colspan="2" style="border-top:1px;border-bottom: 1px; padding-left: 5px;vertical-align: justify;padding-top: 5px;"><strong>Pinjaman Modal Usaha</strong></td>
        </tr>
        <tr>
            <td align="center" colspan="2" style="border-top:1px;border-bottom: 1px; padding-left: 5px;vertical-align: justify;padding-top: 5px;"><strong>Nomor Kontrak : <?php echo $kontrak_mitra['no_kontrak'];?>/MB/<?php echo date("Y"); ?></strong></td>
        </tr>
        <tr>
            <td colspan="2" style="border-top:1px;border-bottom: 1px; padding-left: 5px;vertical-align: justify;padding-top: 5px;">Perjanjian Program Bina Lingkungan ini (selanjutnya disebut Perjanjian) ditandatangani pada hari ini
            tanggal <?php echo $hari;?>, bulan <?php echo $bulanList[$bulan];?>, tahun <?php echo $year; ?> di Cilegon, oleh dan
            antara :</td>
        </tr>
        <tr>
            <td style="width: 150px"><strong>1. PT Krakatau Steel (Persero) Tbk.</strong></td>
            <td align="justify" style="border-top:1px;border-bottom: 1px; padding-left: 5px;vertical-align: justify;padding-top: 5px;">Berkedudukan di Cilegon, di Jalan Industri Nomor 5, Provinsi Banten, sesuai anggaran dasar yang tercantum dalam akta nomor 51 Tanggal 27 April 2015yang dibuat oleh Jose Dima Satria, S.H., M.Kn.. Notaris di Jakarta, dalam hal ini diwakili oleh <strong>Syarif Rahman</strong>, Manager Community Development berdasarkan Surat Keputusan Direksi PT Krakatau Steel (Persero) Tbk. Nomor 59/C/DU-KS/Kpts/2015 tanggal 29 Mei 2015, dan Pengaturan Kewenangan di Divisi Program Kemitraan &amp; Bina Lingkungan No. 49 A/C/DU- KS/Kpts/2006 tanggal 22 Agustus 2006, yang bertindak mewakili Direksi, dari dan oleh sebab itu bertindak untuk dan atas nama Krakatau Steel (Persero) Tbk. selanjutnya disebut <strong>PIHAK PERTAMA</strong>.</td>
        </tr>
        <tr>
            <td><strong>2. <?php echo $kontrak_mitra['nama_perusahaan'];?></strong></td>
            <td align="justify" style="border-top:1px;border-bottom: 1px; padding-left: 5px;vertical-align: justify;padding-top: 5px;">Berkedudukan Di&nbsp;<?php echo $datapemohon;?></td>
        </tr>
        <tr>
            <td align="justify" colspan="2" style="border-top:1px;border-bottom: 1px; padding-left: 5px;vertical-align: justify;padding-top: 5px;"><strong>PIHAK PERTAMA</strong> dan <strong>PIHAK KEDUA</strong> selanjutnya disebut kedua pihak. <strong>Kedua pihak</strong> terlebih dahulu menerangkan :</td>
        </tr>
    </table>
</div>
</body>
</html>