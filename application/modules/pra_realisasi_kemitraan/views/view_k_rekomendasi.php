<?php
$app13 = 'Rekomendasi Penilaian Kelayakan'; // nama aplikasi
$module13 = 'pra_realisasi_kemitraan';
$appLink13 = 'K_rekomendasi'; // controller
$idField13  = 'id_rekomendasi'; //field key table
?>

<script>
	var url13;
	var app13 = "<?=$app13?>";
	var appLink13 = '<?=$appLink13?>';
	var module13 = '<?=$module13?>';
	var idField13 = '<?=$idField13?>';
	
    function add13(){
        $('#dlg13').dialog('open').dialog('setTitle','Tambah ' + app13);
		$('#fm13').form('clear');
	    $('#tbl13').html('Save13');
		url13 = '<?=base_url($module13 . '/' . $appLink13 . '/create')?>/'+idParent;
    }
    function edit13(){
        var row = $('#dg13').datagrid('getSelected');
		if (row){
			$('#tbl13').html('Simpan');
			$('#dlg13').dialog('open').dialog('setTitle','Edit '+ app13);
			$('#fm13').form('load',row);
			url13 = '<?=base_url($module13 . '/' . $appLink13 . '/update')?>/'+row[idField13];
	    }
    }
    function hapus13(){
        var row = $('#dg13').datagrid('getSelected');		
	    if (row){
			$.messager.confirm('Confirm','Apakah Anda Yakin Akan Menghapus ini?',function(r){
				if (r){
					$.post('<?=base_url($module13 . '/' . $appLink13 . '/delete')?>/'+row[idField13],function(result){
						if (result.success){
							$('#dg13').datagrid('reload');	// reload the user data
							$('#dg13').datagrid('reload');
						} else {
							$.messager.show({	// show error message
								title: 'Error',
								msg:result.msg
							});
						}
					},'json');
				}
			});
		}
    }
    function save13(){
	   $('#fm13').form('submit',{
	    	url: url13,
	    	onSubmit: function(){
	    		return $(this).form('validate');
	    	},
	    	success: function(result){
	    		var result = eval('('+result+')');
	    		if (result.success){
	    			$('#dlg13').dialog('close');		
	    			$('#dg13').datagrid('reload');
	                
	    		} else {
	             $.messager.alert('Error Message',result.msg,'error');
	    		}
	    	}
	    });
	}
	function doSearch13(value){
	    $('#dg13').datagrid('load',{    
	    	q:value  
	    });
	}
	function reload13(value){
		$('#dg13').datagrid({    
	    	url: '<?=base_url($module13 . '/' . $appLink13 . '/read/')?>/'+idParent  
	    });
	}
	function rekomen_view(value,row,index){
        var rekomen_data = [];
        rekomen_data["01"] = "Layak untuk menjadi Mitra Binaan";
        rekomen_data["02"] = "Dipertimbangkan untuk menjadi Mitra Binaan";
        rekomen_data["03"] = "Tidak Layak untuk menjadi Mitra Binaan";
                      
        return rekomen_data[value];
    }
</script>
 
<div class="tabs-container">                
	<table id="dg13" class="easyui-datagrid" 
	data-options="  
	    url: '<?=base_url($module13 . '/' . $appLink13 . '/read')?>',
		singleSelect:'true', 
	    title:'<?=$app13?>',
	    toolbar:'#toolbar13',
	    iconCls:'icon-cog',
	    rownumbers:'true',  
	    idField:'<?=$idField13?>', 
	    pagination:'true',
	    fitColumns:'false',
	    pageList: [10,20,30],
	    nowrap:false
	    

	    "
	>
	    <thead >
	    <tr >
	    	<th rowspan="2" field="id_rekomendasi" hidden width="100" sortable="true" halign="center" ><strong>ID</strong></th>
	    	<th colspan="4" halign="center" ><strong>Aspek</strong></th>
	    	<th colspan="3" width="400" sortable="true" halign="center" ><strong>Catatan Khusus  Surveyor</strong></th>	
		</tr>
			<th field="pengelolaan" width="200" halign="center"  sortable="true">Pengolahan<br>& Administrasi</th>
			<th field="teknis" width="200" sortable="true" halign="center" >Teknis / Produksi</th>
			<th field="pemasaran" width="200" sortable="true" halign="center" >Pemasaran</th>
			<th field="keuangan" width="200" sortable="true" halign="center" >Keuangan</th>
			<th field="kekuatan_mitra" width="200" sortable="true" halign="center" >Kekuatan</th>
			<th field="kelemahan_mitra" width="200" sortable="true" halign="center" >Kelemahan</th>
			<th field="rekomendasi" formatter="rekomen_view" width="200" sortable="true" halign="center" >Rekomendasi</th>
	    
	    </thead>
	</table>
	
	<!-- Model Start -->
	<div id="dlg13" class="easyui-dialog" style="width:900px; height:600; padding:10px" closed="true" buttons="#t_dlg_dis-buttons13" >
	    <form id="fm13" method="post" enctype="multipart/form-data" action="">
<table width="100%" align="center" border="0">
			            <tr><td><strong>1. Aspek Pengelolaan & Administrasi</strong></td>
			            	<td><strong>2. Aspek Teknis / Produksi</strong></td>
			            </tr>
			            <tr>
			              <td><textarea name="pengelolaan" id="pengelolaan" required style="width:400px;height: 70px; "></textarea>
			                </td>
			                <td>
			              <textarea name="teknis" id="teknis" required style="width:400px;height: 70px;">
		                  </textarea>
			               </td>             
			            </tr>
			           <tr><td><strong>3. Aspek Pemasaran</strong></td>
			           		<td><strong>4. Aspek Keuangan</strong></td>
			           </tr>
			           <tr><td>
			           		<textarea name="pemasaran" id="pemasaran" required style="width:400px;height: 70px; "></textarea>
			                </td>
			                <td>
			             	<textarea name="keuangan" id="keuangan" required style="width:400px;height: 70px; "></textarea>
			                </td>
			            </tr>
			            <tr><td><strong>5. Catatan Khusus Surveyor</strong></td></tr>
			             <tr><td>&nbsp;&nbsp;&nbsp;<u>Kekuatan</u></td>
			             <tr><td><textarea name="kekuatan_mitra" id="kekuatan_mitra" required style="width:400px;height: 70px; "></textarea>
			                </td>
			            </tr>
			            <tr><td colspan="2">&nbsp;&nbsp;&nbsp;<u>Kelemahan</u></td></tr>
			            <tr><td><textarea name="kelemahan_mitra" id="kelemahan_mitra" required style="width:400px;height: 70px; "></textarea>
			                </td>
			            </tr>
			           <tr>
			            <td>&nbsp;&nbsp;&nbsp;Rekomendasi</td></tr>
			            <tr>			            
			            <td><input type="radio" id="re" name="rekomendasi" value="01" style="width: 13px;" <?=set_radio('rekomendasi', '01', TRUE) ?>/> Layak untuk menjadi Mitra Binaan <br>
							<input type="radio" id="rekomendasi" name="rekomendasi" value="02" style="width: 13px;" <?=set_radio('rekomendasi', '02') ?> /> Dipertimbangkan untuk menjadi Mitra Binaan <br>
							<input type="radio" id="rekomendasi" name="rekomendasi" value="03" style="width: 13px;" <?=set_radio('rekomendasi', '03') ?> /> Tidak Layak untuk Menjadi Mitra Binaan </td> 
			            <!-- -->
			        </table>
	    </form>
	   
	   	<!-- Tombol Add, Edit, Hapus Datagrid -->
       	<div id="toolbar13">  
            <table align="center" style="padding: 0px; width: 99%;">
                <tr>
                    <td>
                        <a href="javascript:void(0)" class="btn btn-small btn-success" onclick="javascript:add13()"><i class="icon-plus-sign icon-large"></i>&nbsp;Add</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:edit13()"><i class="icon-edit"></i>&nbsp;Edit</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-danger" onclick="javascript:hapus13()"><i class="icon-trash icon-large"></i>&nbsp;Delete</a>
                    </td>
                    <td>&nbsp;</td>
                    <td align="right">
                    	<input class="easyui-searchbox" prompt="Silahkan Input Field" searcher="doSearch13"  style="width:300px"/>
                    </td>
                </tr>
            </table>  
        </div>  
        <!-- end tombol datagrid -->
        <!-- Tombol simpan & Batal Form -->
        <div id="t_dlg_dis-buttons13">
            <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:save13()"><i class="icon-save icon-large"></i>&nbsp;Simpan</a>
            <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:$('#dlg13').dialog('close')"><i class="icon-remove icon-large"></i>&nbsp;Batal</a>      
        </div>
	</div>
	<!-- Model end -->
</div>