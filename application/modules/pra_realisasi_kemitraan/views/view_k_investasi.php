<?php
$app14 = 'Modal Investasi'; // nama aplikasi
$module14 = 'pra_realisasi_kemitraan';
$appLink14 = 'K_investasi'; // controller
$idField14  = 'id_investasi'; //field key table
?>

<script>
	var url14;
	var app14 = "<?=$app14?>";
	var appLink14 = '<?=$appLink14?>';
	var module14 = '<?=$module14?>';
	var idField14 = '<?=$idField14?>';
	

	//
	function hit14()
		{
			var harga=$("#fm14 #harga_satuan").numberbox('getValue');
			var unit=$("#fm14 #unit").numberbox('getValue');
			
			var jumlah=harga*unit;
			$("#fm14 #jumlah").numberbox('setValue', jumlah);
		}	
	//
    function add14(){
        $('#dlg14').dialog('open').dialog('setTitle','Tambah ' + app14);
		$('#fm14').form('clear');
	    $('#tbl14').html('Save14');
		url14 = '<?=base_url($module14 . '/' . $appLink14 . '/create')?>/'+idParent;
		 var today = "<?php echo date('Y-m-d')?>";
		 var nilai=0;
        $('#kepemilikan').datebox('setValue', today);
        $('#nilai_asset').numberbox('setValue',nilai);
        $('#biaya_penyusutan').numberbox('setValue',nilai);
        $('#biaya_pemeliharaan').numberbox('setValue',nilai);
    }
    function edit14(){
        var row = $('#dg14').datagrid('getSelected');
		if (row){
			$('#tbl14').html('Simpan');
			$('#dlg14').dialog('open').dialog('setTitle','Edit '+ app14);
			$('#fm14').form('load',row);
			url14 = '<?=base_url($module14 . '/' . $appLink14 . '/update')?>/'+row[idField14];
	    }
    }
    function hapus14(){
        var row = $('#dg14').datagrid('getSelected');		
	    if (row){
			$.messager.confirm('Confirm','Apakah Anda Yakin Akan Menghapus ini?',function(r){
				if (r){
					$.post('<?=base_url($module14 . '/' . $appLink14 . '/delete')?>/'+row[idField14],function(result){
						if (result.success){
							$('#dg14').datagrid('reload');	// reload the user data
							$('#dg14').datagrid('reload');
						} else {
							$.messager.show({	// show error message
								title: 'Error',
								msg:result.msg
							});
						}
					},'json');
				}
			});
		}
    }
    function save14(){
	   $('#fm14').form('submit',{
	    	url: url14,
	    	onSubmit: function(){
	    		return $(this).form('validate');
	    	},
	    	success: function(result){
	    		var result = eval('('+result+')');
	    		if (result.success){
	    			$('#dlg14').dialog('close');		
	    			$('#dg14').datagrid('reload');
	                
	    		} else {
	             $.messager.alert('Error Message',result.msg,'error');
	    		}
	    	}
	    });
	}
	function doSearch14(value){
	    $('#dg14').datagrid('load',{    
	    	q:value  
	    });
	}
	function reload14(value){
		$('#dg14').datagrid({    
	    	url: '<?=base_url($module14 . '/' . $appLink14 . '/read/')?>/'+idParent  
	    });
	}
</script>
 
<div class="tabs-container">                
	<table id="dg14" class="easyui-datagrid" 
	data-options="  
	    url: '<?=base_url($module14 . '/' . $appLink14 . '/read/')?>',
		singleSelect:'true', 
	    title:'<?=$app14?>',
	    toolbar:'#toolbar14',
	    iconCls:'icon-cog',
	    rownumbers:'true',  
	    idField:'<?=$idField14?>', 
	    pagination:'true',
	    fitColumns:'true',
	    pageList: [10,20,30]
	    "
	>
	    <thead>
	    	<th field="id_investasi" hidden width="100" sortable="true">ID Invenst</th>
	        <th field="kebutuhan_investasi" width="100" sortable="true">Kebutuhan Investasi</th>
			<th field="unit" width="100" sortable="true">Unit</th>
			<th field="satuan" width=100" sortable="true">Satuan</th>
			<th field="harga_satuan" width=100" sortable="true">Harga Satuan</th>
			<th field="biaya_penyusutan" width="100" sortable="true">Biaya Penyusutan</th>
			<th field="jumlah" width=100" sortable="true">Jumlah</th>
	    </thead>
	</table>
	
	<!-- Model Start -->
	<div id="dlg14" class="easyui-dialog" style="width:500px; height:auto; padding:10px" closed="true" buttons="#t_dlg_dis-buttons14" >
	    <form id="fm14" method="post" enctype="multipart/form-data" action="">
	       <table width="100%" align="center" border="0">
	            <tr>
	                <td>Kebutuhan Investasi</td>
	                <td>:</td>
	                <td><input type="text" style="width: 170px;" name="kebutuhan_investasi" id="kebutuhan_investasi">
	                </td>
	            </tr>
	            <tr>
	                <td>Unit</td>
	                <td>:</td>
	                <td>
	                    <input type="text" name="unit" id="unit" required class="easyui-numberbox"  style="width: 50px;" data-options="onChange: function(a){
	              hit14();}">
	                </td>
	            </tr>
	            <tr>
	               <td>Satuan</td>
	               <td>:</td>
	               <td>
	               <input style="width: 70px;" class="easyui-combobox" name="satuan" id="satuan" required data-options="
                                    url:'<?=base_url($module14 . '/' . $appLink14 . '/getsatuan')?>',
                                    method:'get',
                                    required:'true',
                                    valueField:'satuan',
                                    textField:'satuan',
                                    panelHeight:'auto'
                                ">
	               </td>
	            </tr>
	           <tr>
	               <td>Harga Satuan</td>
	               <td>:</td>
	               <td><input class="easyui-numberbox" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%',required:'true', onChange: function(a){
	              hit14();}" name="harga_satuan" id="harga_satuan"  style="width: 250px;">
	               </td>
	            </tr>
	             
	             <tr>
	               <td>Jumlah</td>
	               <td>:</td>
	               <td><input class="easyui-numberbox" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%'" name="jumlah" id="jumlah" required  style="width: 250px;" >
	               </td>
	               </tr>
	            
	        </table>
	    </form>
	   
	   	<!-- Tombol Add, Edit, Hapus Datagrid -->
       	<div id="toolbar14">  
            <table align="center" style="padding: 0px; width: 99%;">
                <tr>
                    <td>
                        <a href="javascript:void(0)" class="btn btn-small btn-success" onclick="javascript:add14()"><i class="icon-plus-sign icon-large"></i>&nbsp;Add</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:edit14()"><i class="icon-edit"></i>&nbsp;Edit</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-danger" onclick="javascript:hapus14()"><i class="icon-trash icon-large"></i>&nbsp;Delete</a>
                    </td>
                    <td>&nbsp;</td>
                    <td align="right">
                    	<input class="easyui-searchbox" prompt="Silahkan Input Field" searcher="doSearch14"  style="width:300px"/>
                    </td>
                </tr>
            </table>  
        </div>  
        <!-- end tombol datagrid -->
        <!-- Tombol simpan & Batal Form -->
        <div id="t_dlg_dis-buttons14">
            <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:save14()"><i class="icon-save icon-large"></i>&nbsp;Simpan</a>
            <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:$('#dlg14').dialog('close')"><i class="icon-remove icon-large"></i>&nbsp;Batal</a>      
        </div>
	</div>
	<!-- Model end -->
</div>