<?php
$app = 'Pencairan Dana'; // nama aplikasi
$module = 'pra_realisasi_kemitraan';
$appLink = 'pencairan_dana'; // controller
$idField  = 'id_cd'; //field key table
?>

<script>
	var url;
	var app = "<?=$app?>";
	var appLink = '<?=$appLink?>';
	var module = '<?=$module?>';
	var idField = '<?=$idField?>';
	var idParent = '';
	var gAccess = '<?=$access[0]['ID']?>';
	
    function add(){
        $('#dlg').dialog('open').dialog('setTitle','Tambah ' + app);
		$('#fm').form('clear');
	    $('#tbl').html('Save');
		url = '<?=base_url($module . '/' . $appLink . '/create')?>';
		$('#angsuran').hide();
		$("#akun").hide();
    }
    function edit(){
        var row = $('#dg').datagrid('getSelected');
		if (row){
			$('#tbl').html('Simpan');
			$('#dlg').dialog('open').dialog('setTitle','Edit '+ app);
			$('#fm').form('load',row);
			url = '<?=base_url($module . '/' . $appLink . '/update')?>/'+row[idField];
			idParent = row.id_cd;
			$("#akun").show();
			reload1();
	    }
    }
    function hapus(){
        var row = $('#dg').datagrid('getSelected');		
	    if (row){
			$.messager.confirm('Confirm','Apakah Anda Yakin Akan Menghapus ini?',function(r){
				if (r){
					$.post('<?=base_url($module . '/' . $appLink . '/delete')?>/'+row[idField],function(result){
						if (result.success){
							$('#dg').datagrid('reload');	// reload the user data
						} else {
							$.messager.show({	// show error message
								title: 'Error',
								msg:result.msg
							});
						}
					},'json');
				}
			});
		}
    }
    function preview(){
	    var row = $('#dg').datagrid('getSelected');
	    url  = '<?=base_url($module . '/' . $appLink . '/previewPencairanDana')?>/'+row[idField];
	    if (row)
	    {
	        window.open(url,'_blank');
	    }
	}
    function save(){
	   $('#fm').form('submit',{
	    	url: url,
	    	onSubmit: function(){
	    		return $(this).form('validate');
	    	},
	    	success: function(result){
	    		var result = eval('('+result+')');
	    		if (result.success){
	    			$('#dlg').dialog('close');		
	    			$('#dg').datagrid('reload');
	    			/*console.log(result);
	                idParent = result.id;
					$("#akun").show();
					reload1();*/
	    		} else {
	             $.messager.alert('Error Message',result.msg,'error');
	    		}
	    	}
	    });
	}
	function doSearch(value){
	    $('#dg').datagrid('load',{    
	    	q:value  
	    });
	}
	function editAccount(id){
		//var row = $('#dg').datagrid('getSelected');
		//console.log(row);
		//if (row){
			//$('#tbl').html('Simpan');
			$('#dlgx').dialog('open').dialog('setTitle','Edit '+ app);
			//idParent = row.id_cd;
			idParent = id;
			reload1();
	    //}
	}
	function hitungAngsuran(){
		var bunga = $('#bunga').numberbox('getValue');
		var tahun = $('#tahun').numberbox('getValue');
		var nilai_pengajuan = $('#nilai_pengajuan').numberbox('getValue');

		if(bunga != '' && tahun != '' && nilai_pengajuan != ''){
			tahun = tahun*12;

			var total_bunga = nilai_pengajuan * (bunga/100);
			var angsuran_bunga = total_bunga/tahun;
			var angsuran_pokok = nilai_pengajuan/tahun;

			$('#total_bunga').numberbox('setValue', total_bunga);
			$('#angsuran_bunga').numberbox('setValue', angsuran_bunga);
			$('#angsuran_pokok').numberbox('setValue', angsuran_pokok);
			$('#total_pinjaman_bunga').numberbox('setValue', (parseInt(nilai_pengajuan)+parseInt(total_bunga)));
			$('#angsuran_bulanan').numberbox('setValue', (angsuran_pokok+angsuran_bunga));
		}
	}
	function approvelist(val, row){
		var result = '<a href="javascript:void(0)" title="Daftar Akun" class="btn btn-small btn-default" onclick="javascript:editAccount('+row.id_cd+')"><i class="icon-tasks icon-large"></i></a> ';

		//console.log(gAccess);

		if(gAccess == '1'){
			if(val == 0){
				result += '<a href="javascript:appoveSubmit('+val+', '+row.id_cd+')" title="Belum Di Approve" class="btn btn-small btn-default easyui-tooltip" ><i class="icon-remove icon-large" style="color:red"></i></a>';
			} else {
				result += '<a href="#" title="Sudah Di Approve" class="btn btn-small btn-default easyui-tooltip" ><i class="icon-ok icon-large" style="color:green"></i></a>';
			}
		} else if(gAccess == '2') {
			if(val == 1){
				result += '<a href="javascript:appoveSubmit('+val+', '+row.id_cd+')" title="Belum Di Approve" class="btn btn-small btn-default easyui-tooltip" ><i class="icon-remove icon-large" style="color:red"></i></a>';
			} else if(val == 0) {
				result += '<a href="#" title="Belum Di Approve" class="btn btn-small btn-default easyui-tooltip" ><i class="icon-minus icon-large" style="color:red"></i></a>';
			} else {
				result += '<a href="#" title="Sudah Di Approve" class="btn btn-small btn-default easyui-tooltip" ><i class="icon-ok icon-large" style="color:green"></i></a>';
			}
		} else if(gAccess == '3') {
			if(val == 2){
				result += '<a href="javascript:appoveSubmit('+val+', '+row.id_cd+')" title="Belum Di Approve" class="btn btn-small btn-default easyui-tooltip" ><i class="icon-remove icon-large" style="color:red"></i></a>';
			} else if(val == 0 || val == 1) {
				result += '<a href="#" title="Belum Di Approve" class="btn btn-small btn-default easyui-tooltip" ><i class="icon-minus icon-large" style="color:red"></i></a>';
			} else {
				result += '<a href="#" title="Sudah Di Approve" class="btn btn-small btn-default easyui-tooltip" ><i class="icon-ok icon-large" style="color:green"></i></a>';
			}
		}

		/*if(val == 0){
			result = '<a href="javascript:disposisi_selesai('+val+')" title="Belum Di Approve" class="btn btn-small btn-default easyui-tooltip" ><i class="icon-remove icon-large" style="color:red"></i></a>';
		} else {
			result = '<a href="javascript:disposisi_selesai('+val+')" title="Sudah Di Approve" class="btn btn-small btn-default easyui-tooltip" ><i class="icon-ok icon-large" style="color:green"></i></a>';
		}*/

		return result;
	}
	function appoveSubmit(val, id){
		var url = '<?=base_url($module . '/' . $appLink . '/setAccess')?>/';
		$.post( url, { access: val, id: id })
	  	.done(function( data ) {
	    	//alert( "Data Loaded: " + data );
	    	//console.log(data);
	    	$('#dg').datagrid('reload');
	  	});
	}
</script>
 
<div class="tabs-container">                
	<table id="dg" class="easyui-datagrid" 
	data-options="  
	    url: '<?=base_url($module . '/' . $appLink . '/read')?>',
		singleSelect:'true', 
	    title:'<?=$app?>',
	    toolbar:'#toolbar',
	    iconCls:'icon-cog',
	    rownumbers:'true',  
	    idField:'<?=$idField?>', 
	    pagination:'true',
	    fitColumns:'true',
	    pageList: [10,20,30]
	    "
	>
	    <thead>
	        <th field="id_mitra" width="100" sortable="true">ID Mitra</th>
	        <th field="id_cd" width="200" sortable="true">No CD</th>
            <th field="tanggal_cd" width="200" sortable="true">Tgl CD</th>
            <th field="nama" width="200" sortable="true">Nama Pemohon</th>
            <th field="nama_perusahaan" width="200" sortable="true">Nama Instansi</th>
            <!--<th field="alamat" width="200" sortable="true">Alamat</th>-->
            <th field="kota" width="200" sortable="true">Kota</th>
            <th field="sektor_usaha" width="200" sortable="true">Sektor Usaha</th>
            <?php if(count($access) > 0){ ?>
            <th field="status" width="200" sortable="true" formatter="approvelist">Approve</th>
            <?php } ?>
	    </thead>
	</table>
	
	<div id="dlg" class="easyui-dialog" style="width:800px; height:440px; padding:10px" closed="true" buttons="#t_dlg_dis-buttons" >
		<table width="100%" align="center" border="0">
			<tr>
				<td width="100%">
					<form id="fm" method="post" enctype="multipart/form-data" action="">
				        <table width="100%" align="center" border="0">
				            <tr>
								<input type="hidden" id="id_tnilai" name="id_tnilai" style="width:70%;"  />
				                <td>Mitra</td>
				                <td>:</td>
				                <td>
				                    <input name="id_mitra" id="id_mitra"  class="easyui-combogrid" required style="width: 203px"
									data-options="panelHeight:200, panelWidth: 700,
									valueField:'',
									idField:'id_mitra',
									textField:'nama',
									mode:'remote',
									url:'<?=base_url($module . '/' . $appLink . '/getmitra') ?>',
									onSelect :function(indek,row)
									{
										console.log(row),
										$('#nama').val(row.nama),
										$('#no_ktp').val(row.no_ktp),
										$('#alamat').val(row.alamat),
										$('#kota').val(row.kota),
										$('#nilai_pengajuan').numberbox('setValue', row.nilai_pengajuan),
										$('#angsuran').show()
									},
									columns:[[
			                       		{field:'id_mitra',title:'ID Mitra',width:80},
			                        	{field:'nama',title:'Nama Pemohon',width:150},
			                        	{field:'nilai_pengajuan',title:'Nilai',width:100},
										{field:'jaminan',title:'jaminan',width:150},
			                        	{field:'no_ktp',title:'KTP Pemohon',width:150},
										{field:'alamat',title:'Alamat',width:150},
			                        	{field:'kota',title:'Kota',width:200}
			                        ]]

									" >
				                </td>
							
				            </tr>
				            <tr>
				                <td >Nama Pemohon</td>
				                <td>:</td>
				                <td>
				                    <!--<input id="nama" name="nama" required type="text" style="width: 200px;">-->
									<input id="nama" name="nama" style="width: 200px;" readonly="readonly" >
				                </td>
								
				            </tr>
				            <tr>
				                <td>No KTP</td>
				                <td>:</td>
				                <td>
				                    <input id="no_ktp" name="no_ktp" required type="text" style="width: 200px;" readonly="readonly">
				                </td>
								
				            </tr>
							<tr>
				                <td>Alamat</td>
				                <td>:</td>
				                <td>
				                    <input id="alamat" name="alamat" required type="text" style="width: 200px;" readonly="readonly">
				                </td>
				            </tr>
							<tr>
				                <td>Kota</td>
				                <td>:</td>
				                <td>
									<input id="kota" name="kota" class="easyui-textbox"  style="width:200px" required readonly="readonly">
				                </td>
				            </tr>
							<tr>
				                <td>Nilai Pengajuan</td>
				                <td>:</td>
				                <td>
				                	<input class="easyui-numberbox" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%'" name="nilai_pengajuan" id="nilai_pengajuan" required style="width: 200px;" readonly>
				                </td>
				            </tr>
							
				        </table>
						
						<table id="angsuran" width="100%" align="center" border="0" style="display: none">
				          	<tr>
				                <td colspan="3"><h4>Data Angsuran</h4></td>
				            </tr>
				            <tr>
				                <td >Tanggal CD</td>
				                <td>:</td>
				                <td>
									<input class="easyui-datebox" label="Start Date:" labelPosition="top" name="tanggal_cd" id="tanggal_cd" required style="width: 120px;">
				                </td>
				            </tr>
				            
							<tr>
				                <td >Jasa Administrasi</td>
				                <td>:</td>
				                <td>
									<input class="easyui-numberbox" data-options="label:'%',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',suffix:' %',width:'100%',onChange: function(){ hitungAngsuran(); }" name="bunga" id="bunga" required style="width: 200px;">
				                </td>
								
				            </tr>
				            <tr>
				                <td>Tahun</td>
				                <td>:</td>
				                <td>
				                    <input class="easyui-numberbox" data-options="label:'Tahun',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',suffix:' Tahun',width:'100%',onChange: function(){ hitungAngsuran(); }" name="tahun" id="tahun" required style="width: 200px;">
				                </td>
								
				            </tr>
							<tr>
				                <td>Total Jasa Adm</td>
				                <td>:</td>
				                <td>
				                    <input class="easyui-numberbox" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%'" name="total_bunga" id="total_bunga" required style="width: 200px;">
				                </td>
				            </tr>
							<tr>
				                <td>Angsuran Pokok</td>
				                <td>:</td>
				                <td>
									<input class="easyui-numberbox" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%'" name="angsuran_pokok" id="angsuran_pokok" required style="width: 200px;">
				                </td>
				            </tr>
							<tr>
				                <td>Angsuran Jasa Adm</td>
				                <td>:</td>
				                <td>
				                    <input class="easyui-numberbox" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%'" name="angsuran_bunga" id="angsuran_bunga" required style="width: 200px;">
				                </td>
				            </tr>
				            <tr>
				                <td>Total Angsuran + Jasa Adm</td>
				                <td>:</td>
				                <td>
				                    <input class="easyui-numberbox" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%'" name="total_pinjaman_bunga" id="total_pinjaman_bunga" required style="width: 200px;">
				                </td>
				            </tr>
				            <tr>
				                <td>Angsuran Bulanan</td>
				                <td>:</td>
				                <td>
				                    <input class="easyui-numberbox" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%'" name="angsuran_bulanan" id="angsuran_bulanan" required style="width: 200px;">
				                </td>
				            </tr>
				           	<tr>
				                <td>Kode Bayar</td>
				                <td>:</td>
				                <td>
				                    <select  class="easyui-combobox" type="text" name="kode_bayar" id="kode_bayar" required style="width: 200px;">
				                    	<option value="Transfer">Transfer</option>
				                    	<!--<option value="Cheque">Cheque</option>-->
				                    </select>
				                </td>
				            </tr>
				        </table>
				    </form>
				</td>
			</tr>
		</table>
	  
	   	<!-- Tombol Add, Edit, Hapus Datagrid -->
       	<div id="toolbar">  
            <table align="center" style="padding: 0px; width: 99%;">
                <tr>
                    <td>
                        <a href="javascript:void(0)" onclick="javascript:add()" class="btn btn-small btn-success"><i class="icon-plus-sign icon-large"></i>&nbsp;Add</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:edit()"><i class="icon-edit"></i>&nbsp;Edit</a>
                        <!--<a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:editAccount()"><i class="icon-edit"></i>&nbsp;Edit Akun</a>-->
                        <a href="javascript:void(0)" class="btn btn-small btn-danger" onclick="javascript:hapus()"><i class="icon-trash icon-large"></i>&nbsp;Delete</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-warning" iconCls: 'icon-search' onclick="javascript:preview()"><i class="icon-search icon-large"></i>&nbsp;Preview</a>
						<!--<a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:detail_jaminan()"><i class="icon-file"></i>&nbsp;Detail</a>
						<a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:view_report()"><i class="icon-print"></i>&nbsp;Report</a>-->
                    </td>
                    <td>&nbsp;</td>
                    <td align="right">
                    	<input class="easyui-searchbox" prompt="Silahkan Input Field" searcher="doSearch"  style="width:300px"/>
                    </td>
                </tr>
            </table>  
        </div>  
        <!-- end tombol datagrid -->
        <!-- Tombol simpan & Batal Form -->
        <div id="t_dlg_dis-buttons">
            <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:save()"><i class="icon-save icon-large"></i>&nbsp;Simpan</a>
            <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:$('#dlg').dialog('close')"><i class="icon-remove icon-large"></i>&nbsp;Batal</a>      
        </div>
	</div>
	<!-- Model end -->

	<div id="dlgx" class="easyui-dialog" style="width:800px; height:auto; padding:10px" closed="true" buttons="#t_dlg_dis-buttonsx" >
		<?php $this->load->view('view_pencairan_dana_akun'); ?>
		<!-- Tombol simpan & Batal Form -->
        <div id="t_dlg_dis-buttonsx">
            <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:$('#dlgx').dialog('close')"><i class="icon-remove icon-large"></i>&nbsp;Batal</a>      
        </div>
	</div>
	<!-- Model end -->

</div>