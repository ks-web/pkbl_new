<!DOCTYPE html>
<html>
<head>
	<title>Report Pengukuran Nilai Jaminan</title>
	<style type="text/css">
		.judul {
			font-weight: bold;
			color: #000000;
			letter-spacing: 0pt;
			word-spacing: 2pt;
			font-size: 18px;
			text-align: center;
			/*font-family: tahome, sans-serif;*/
			line-height: 1;
		}
		.teks_atas {
			font-size: 11px;
		}
		.teks_nomor_kontrak {
			text-align: center;
			font-weight: bold;
			font-size: 15px;
		}

		.tg {
			border-collapse: collapse;
			border-spacing: 0;
		}
		.tg td {
			font-family: Arial, sans-serif;
			font-size: 10px;
			padding: 10px 5px;
			border-style: solid;
			border-width: 1px;
			overflow: hidden;
			word-break: normal;
		}
		.tg th {
			font-family: Arial, sans-serif;
			font-size: 10px;
			font-weight: normal;
			padding: 10px 5px;
			border-style: solid;
			border-width: 1px;
			overflow: hidden;
			word-break: normal;
		}
		.tg .tg-qqdn {
			font-weight: bold;
			font-size: 10px;
			text-align: right;
			vertical-align: top
		}
		.tg .tg-d55q {
			font-weight: bold;
			font-size: 10px;
			vertical-align: middle
		}
		.tg .tg-ecrz {
			font-size: 10px;
			vertical-align: top
		}
		.tg .tg-07dj {
			font-weight: bold;
			font-size: 10px
		}
	</style>
</head>
<body>
	<table width="100%">
		<tr>
			<td class="teks_atas" width="150px" align="center">PT. Krakatau Steel (Persero) Tbk.<br />Divisi Community Development.</td>
			<td align="center">
				<font class="judul">
					Usulan Pinjaman Modal Kepada Usaha Kecil
				</font>
				<br />
				<font class="teks_nomor_kontrak">
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/USL/PKBL-KS/<?=date("Y")?>
				</font>
			</td>
		</tr>
	</table>
	
	<br /><br />
	<div>
		<table class="tg" width="100%">
			<tr>
				<th class="tg-07dj" width="10px">No</th>
				<th class="tg-d55q" width="70px">No. Reg</th>
				<th class="tg-d55q" width="140px">Nama Perusahaan</th>
				<th class="tg-d55q" width="80px">Sektor Usaha</th>
				<th class="tg-d55q">Alamat</th>
				<th class="tg-d55q" width="70px">Kota/Kab</th>
				<th class="tg-d55q" width="100px">Asset</th>
				<th class="tg-d55q" width="100px">Omset/Thn</th>
				<th class="tg-d55q" width="10px">TKJ</th>
				<th class="tg-d55q" width="80px">Pinjaman</th>
				<th class="tg-d55q" width="10px">Th</th>
				<th class="tg-d55q" colspan="2" width="100px">Jasa Admin</th>
				<th class="tg-d55q" width="70px">Angsuran</th>
			</tr>
			<?php
				$no = 1;
				
				if($data) {
					$tot_pinjaman = $tot_jasa_admin = $tot_angsurn = 0;
					foreach ($data as $row) {
						$tot_pinjaman =+ $row['angsuran_pokok'];
						$tot_jasa_admin =+ $row['angsuran_bunga'];
						$tot_angsurn =+ $row['angsuran_bulanan'];
						?>
			<tr>
				<td class="tg-ecrz"><?=$no?></td>
				<td class="tg-ecrz"><?=$row['id_mitra']?></td>
				<td class="tg-ecrz"><?=$row['nama']?></td>
				<td class="tg-ecrz"><?=$row['nama_sektor']?></td>
				<td class="tg-ecrz"><?=$row['alamat'] ?></td>
				<td class="tg-ecrz"><?=$row['kota'] ?></td>
				<td class="tg-ecrz" align="right"><?=number_format($row['asset'],0,',','.') ?></td>
				<td class="tg-ecrz" align="right"><?=number_format($row['omset'],0,',','.') ?></td>
				<td class="tg-ecrz"><?=$row['tenagakerja'] ?></td>
				<td class="tg-ecrz" align="right"><?=number_format($row['angsuran_pokok'],0,',','.') ?></td>
				<td class="tg-ecrz"><?=$row['tahun'] ?></td>
				<td class="tg-ecrz" align="right"><?=number_format($row['bunga'],0,',','.') ?></td>
				<td class="tg-ecrz" align="right"><?=number_format($row['angsuran_bunga'],0,',','.') ?></td>
				<td class="tg-ecrz" align="right"><?=number_format($row['angsuran_bulanan'],0,',','.') ?></td>
			</tr>
						<?php
						$no++;
					}
				} else {
					?>
			<tr>
				<td class="tg-ecrz"><br /></td>
				<td class="tg-ecrz"><br /></td>
				<td class="tg-ecrz"><br /></td>
				<td class="tg-ecrz"><br /></td>
				<td class="tg-ecrz"><br /></td>
				<td class="tg-ecrz"><br /></td>
				<td class="tg-ecrz"><br /></td>
				<td class="tg-ecrz"><br /></td>
				<td class="tg-ecrz"><br /></td>
				<td class="tg-ecrz" align="right"><br /></td>
				<td class="tg-ecrz"><br /></td>
				<td class="tg-ecrz" align="right"><br /></td>
				<td class="tg-ecrz" align="right"><br /></td>
				<td class="tg-ecrz" align="right"><br /></td>
			</tr>
					<?php
				}
			?>
			<tr>
				<td class="tg-qqdn" colspan="8">Jumlah</td>
				<td class="tg-ecrz" colspan="2" align="right"><?=number_format($tot_pinjaman,0,',','.') ?></td>
				<td class="tg-ecrz" colspan="3" align="right"><?=number_format($tot_jasa_admin,0,',','.') ?></td>
				<td class="tg-ecrz" align="right"><?=number_format($tot_angsurn,0,',','.') ?></td>
			</tr>
		</table>
		
		<br /><br />
		
		<?php
		$ttd_kiri = '';
		$title_kiri = '';
		$ttd_kanan = '';
		$title_kanan = '';
		
		if($tot_pinjaman <= 100000000){
			$ttd_kiri = managerName;
			$title_kiri = managerTitle;
			$ttd_kanan = kadisPKName;
			$title_kanan = kadisPKTitle;
		} else if($tot_pinjaman > 100000000 && $tot_pinjaman <= 1000000000000) {
			$ttd_kiri = GmanagerName;
			$title_kiri = GmanagerTitle;
			$ttd_kanan = managerName;
			$title_kanan = managerTitle;
		} else if($tot_pinjaman > 1000000000000) {
			$ttd_kiri = DirSDMPUName;
			$title_kiri = DirSDMPUTitle;
			$ttd_kanan = GmanagerName;
			$title_kanan = GmanagerTitle;
		}
		?>
		
		<table border="0" width="100%">
			<tr>
				<td width="35%" style="text-align:center">
					Menyetujui,<br>
					Dinas Community Development
					<br><br><br><br><br>
					<font style="text-decoration: underline;"><?=$ttd_kiri?></font><br>
					<?=$title_kiri?>
				</td>
				<td width="30%"></td>
				<td width="35%" style="text-align:center">
					Cilegon, <?=tanggal_display()?><br>
					Disiapkan Oleh<br>
					Dinas Community Development
					<br><br><br><br>
					<font style="text-decoration: underline;"><?=$ttd_kanan?></font><br>
					<?=$title_kanan?>
				</td>
			</tr>
			
		</table>
	</div>
	
		
</body>
</html>