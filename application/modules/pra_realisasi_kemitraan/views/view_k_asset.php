<?php
$app1 = 'Aset Perusahaan'; // nama aplikasi
$module1 = 'pra_realisasi_kemitraan';
$appLink1 = 'K_asset'; // controller
$idField1  = 'id_asset'; //field key table
?>

<script>
	var url1;
	var app1 = "<?=$app1?>";
	var appLink1 = '<?=$appLink1?>';
	var module1 = '<?=$module1?>';
	var idField1 = '<?=$idField1?>';
	
    function add1(){
        $('#dlg1').dialog('open').dialog('setTitle','Tambah ' + app1);
		$('#fm1').form('clear');
	    $('#tbl1').html('Save1');
		url1 = '<?=base_url($module1 . '/' . $appLink1 . '/create')?>/'+idParent;
		 var today = "<?php echo date('Y-m-d')?>";
		 var nilai=0;
        $('#kepemilikan').datebox('setValue', today);
        $('#nilai_asset').numberbox('setValue',nilai);
        $('#biaya_penyusutan').numberbox('setValue',nilai);
        $('#biaya_pemeliharaan').numberbox('setValue',nilai);
    }
    function edit1(){
        var row = $('#dg1').datagrid('getSelected');
		if (row){
			$('#tbl1').html('Simpan');
			$('#dlg1').dialog('open').dialog('setTitle','Edit '+ app1);
			$('#fm1').form('load',row);
			url1 = '<?=base_url($module1 . '/' . $appLink1 . '/update')?>/'+row[idField1];
	    }
    }
    function hapus1(){
        var row = $('#dg1').datagrid('getSelected');		
	    if (row){
			$.messager.confirm('Confirm','Apakah Anda Yakin Akan Menghapus ini?',function(r){
				if (r){
					$.post('<?=base_url($module1 . '/' . $appLink1 . '/delete')?>/'+row[idField1],function(result){
						if (result.success){
							$('#dg1').datagrid('reload');	// reload the user data
							$('#dg1').datagrid('reload');
						} else {
							$.messager.show({	// show error message
								title: 'Error',
								msg:result.msg
							});
						}
					},'json');
				}
			});
		}
    }
    function save1(){
	   $('#fm1').form('submit',{
	    	url: url1,
	    	onSubmit: function(){
	    		return $(this).form('validate');
	    	},
	    	success: function(result){
	    		var result = eval('('+result+')');
	    		if (result.success){
	    			$('#dlg1').dialog('close');		
	    			$('#dg1').datagrid('reload');
	                
	    		} else {
	             $.messager.alert('Error Message',result.msg,'error');
	    		}
	    	}
	    });
	}
	function doSearch1(value){
	    $('#dg1').datagrid('load',{    
	    	q:value  
	    });
	}
	function reload1(value){
		$('#dg1').datagrid({    
	    	url: '<?=base_url($module1 . '/' . $appLink1 . '/read/')?>/'+idParent  
	    });
	}
</script>
 
<div class="tabs-container">                
	<table id="dg1" class="easyui-datagrid" 
	data-options="  
	    url: '<?=base_url($module1 . '/' . $appLink1 . '/read/')?>',
		singleSelect:'true', 
	    title:'<?=$app1?>',
	    toolbar:'#toolbar1',
	    iconCls:'icon-cog',
	    rownumbers:'true',  
	    idField:'<?=$idField1?>', 
	    pagination:'true',
	    fitColumns:'true',
	    pageList: [10,20,30]
	    "
	>
	    <thead>
	    	<th field="id_asset" hidden width="100" sortable="true">ID Asset</th>
	        <th field="uraian" width="100" sortable="true">Uraian</th>
			<th field="unit" width="100" sortable="true">Unit</th>
			<th field="kepemilikan" width=100" sortable="true">Dimiliki Sejak</th>
			<th field="nilai_asset" width=100" sortable="true">Nilai</th>
			<th field="biaya_penyusutan" width="100" sortable="true">Biaya Penyusutan</th>
			<th field="biaya_pemeliharaan" width=100" sortable="true">Biaya Pemeliharaan</th>
	    </thead>
	</table>
	
	<!-- Model Start -->
	<div id="dlg1" class="easyui-dialog" style="width:500px; height:auto; padding:10px" closed="true" buttons="#t_dlg_dis-buttons1" >
	    <form id="fm1" method="post" enctype="multipart/form-data" action="">
	       <table width="100%" align="center" border="0">
	            <tr>
	                <td>Uraian</td>
	                <td>:</td>
	                <td><input style="width: 150px;" class="easyui-combobox" name="uraian" id="uraian" required data-options="
                                    url:'<?=base_url($module1 . '/' . $appLink1 . '/geturaian')?>',
                                    method:'get',
                                    required:'true',
                                    valueField:'uraian',
                                    textField:'uraian',
                                    panelHeight:'auto'
                                ">
	                </td>
	            </tr>
	            <tr>
	                <td>Unit</td>
	                <td>:</td>
	                <td>
	                    <input type="text" name="unit" id="unit" required style="width: 50px;">
	                </td>
	            </tr>
	            <tr>
	                <td>Dimiliki Sejak</td>
	                <td>:</td>
	                <td>
	                    <input type="text" name="kepemilikan" id="kepemilikan" class="easyui-datebox" style="width: 100px;">
	                </td>
	            </tr>
	            <tr>
	                <td>Nilai</td>
	                <td>:</td>
	                <td>
	                    <input class="easyui-numberbox" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%'" name="nilai_asset" id="nilai_asset" required style="width: 250px;">
	                </td>
	            </tr>
	             <tr>
	                <td>Biaya Penyusutan</td>
	                <td>:</td>
	                <td>
	                    <input class="easyui-numberbox" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%'" name="biaya_penyusutan" id="biaya_penyusutan" required style="width: 250px;">
	                </td>
	            </tr>
	             <tr>
	                <td>Biaya Pemeliharaan</td>
	                <td>:</td>
	                <td>
	                    <input class="easyui-numberbox" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%'" name="biaya_pemeliharaan" id="biaya_pemeliharaan" required style="width: 250px;">
	                </td>
	            </tr>
	            
	        </table>
	    </form>
	   
	   	<!-- Tombol Add, Edit, Hapus Datagrid -->
       	<div id="toolbar1">  
            <table align="center" style="padding: 0px; width: 99%;">
                <tr>
                    <td>
                        <a href="javascript:void(0)" class="btn btn-small btn-success" onclick="javascript:add1()"><i class="icon-plus-sign icon-large"></i>&nbsp;Add</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:edit1()"><i class="icon-edit"></i>&nbsp;Edit</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-danger" onclick="javascript:hapus1()"><i class="icon-trash icon-large"></i>&nbsp;Delete</a>
                    </td>
                    <td>&nbsp;</td>
                    <td align="right">
                    	<input class="easyui-searchbox" prompt="Silahkan Input Field" searcher="doSearch1"  style="width:300px"/>
                    </td>
                </tr>
            </table>  
        </div>  
        <!-- end tombol datagrid -->
        <!-- Tombol simpan & Batal Form -->
        <div id="t_dlg_dis-buttons1">
            <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:save1()"><i class="icon-save icon-large"></i>&nbsp;Simpan</a>
            <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:$('#dlg1').dialog('close')"><i class="icon-remove icon-large"></i>&nbsp;Batal</a>      
        </div>
	</div>
	<!-- Model end -->
</div>