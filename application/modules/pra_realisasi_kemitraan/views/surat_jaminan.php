<?php
  date_default_timezone_set("Asia/Jakarta");
    $tanggal = date('d F Y');
    //$tgl=$vendor['tgl_berdiri'];

    //tgl lahir pemilik usaha
    $tgl_lhr=$kontrak_mitra['tgl_lahir'];
    $kaping=date('d',strtotime($tgl_lhr));
    $bulan=date('m',strtotime($tgl_lhr));
    $tahun=date('Y', strtotime($tgl_lhr));
    //tgl lahir pemilik usaha

    $tgl    = $kontrak_mitra['tgl_kontrak'];
    $hari   = date('d',strtotime($tgl));
    $moon  = date('m',strtotime($tgl));
    // echo $moon;
    $year   = date('Y',strtotime($tgl));
    $day    = date('D', strtotime($tgl));
    $dayList = array(
        'Sun' => 'Minggu',
        'Mon' => 'Senin',
        'Tue' => 'Selasa',
        'Wed' => 'Rabu',
        'Thu' => 'Kamis',
        'Fri' => 'Jumat',
        'Sat' => 'Sabtu'
    );
    $moonList = array('01' => 'Januari', '02' => 'Februari', '03' => 'Maret', '04' => 'April', '05' => 'Mei', '06' => 'Juni', '07' => 'Juli', '08' => 'Augustus', '09' => 'September', '10' => 'Oktober', '11' => 'Nopember', '12' => 'Desember');
?>

		<style>
			table {
				border-spacing: 0px;
				border-collapse: separate;
				font-size: 14px;
				page-break-inside: avoid;
				padding: 5px;
			}

			table, th, td {
				border: 0px solid black;
			}
			html {
				margin: 10px 50px
			}
			.pull-right {
				text-align: right;
			}
			.center {
				text-align: center;
			}
		</style>
	
		<div style="padding-top: 5px;padding-bottom: 0px;padding-left: 25px;padding-right: 25px;">
		<table style="width: 100%;">			
			<tr>
				<td colspan="3">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="3">&nbsp;</td>
			</tr>

				<tr>
					<td colspan="3" style="padding-bottom: 10px;"><center><strong>PERJANJIAN KUASA JUAL <br> JAMINAN  / TANGGUNGAN</strong></center>
					</td>
				</tr>
			<tr>
				<td colspan="3" style="border-top:1.2px;border-bottom: 0px; padding-left: 5px;vertical-align: justify;padding-top: 5px;border-style: dashed;padding-bottom: 0px;"></td>
			</tr>
				<tr>
					<td  colspan="3" style="border-top:1.2px;border-bottom: 0px; padding-left: 5px;vertical-align: justify;padding-top: 0.1px;border-style: dashed;padding-bottom: 5px;"></td>

				</tr>
				<tr><td colspan="3">&nbsp;</td></tr>
				<tr ><td colspan="3" style="padding-top: 10px;">Perjanjian ini ditandatangani pada hari ini &nbsp;<b><i><?php echo $dayList[$day];?></i></b>&nbsp;, Tanggal &nbsp; <b><i><?php echo $hari;?></i></b>&nbsp;  Bulan &nbsp; <b><i><?php echo $moonList[$moon];?></i></b> &nbsp; Tahun &nbsp; <?php echo $year; ?>&nbsp;di Cilegon Oleh dan Antara :</td>
				</tr>
				<tr>
				<td align="center" colspan="3" style="vertical-align: justify;">
				<table class="tg" style="table-layout: fixed; width: 100%;">
					<colgroup>
						<col style="width: 25%">
						<col style="width: 25%">
						<col style="width: 25%">
						<col style="width: 25%">
					</colgroup>
				<tr>
					<td class="tg-yw4l"></td>
					<td class="tg-yw4l" style="vertical-align: top;padding-left: 50px;padding-top: 5px;padding-bottom: 5px;">Nama</td>
					<td class="tg-lqy6" style="vertical-align: top;padding-top: 5px;padding-bottom: 5px;">:</td>
					<td class="tg-yw4l">
						<b><?php  
							if ($kontrak_mitra['status_jaminan']==0 || $kontrak_mitra['status_jaminan']=='') {
								echo $kontrak_mitra['nama'];
							}else{
									echo $kontrak_mitra['pemilik_jaminan'];
								}							
							?> 
						</b>
					</td>
					
				</tr>
				<tr>
					<td class="tg-yw4l"></td>
					<td class="tg-yw4l" style="vertical-align: top;padding-left: 50px;padding-top: 5px;padding-bottom: 5px;">Umur</td>
					<td class="tg-lqy6" style="vertical-align: top;padding-top: 5px;padding-bottom: 5px;">:</td>
					<td class="tg-yw4l">			
							<?php 
													
							$biday = new DateTime($kontrak_mitra['tgl_lahir']);
							$today = new DateTime();
							$diff = $today->diff($biday);
							if ($kontrak_mitra['status_jaminan']==0 || $kontrak_mitra['status_jaminan']=='') {
								echo $diff->y;
							 	# code...
							 } else {
							 	echo $kontrak_mitra['umur_pemilik'];
							 }

							 ?> &nbsp; Tahun
				</td>
					
				</tr>
				<tr>
					<td class="tg-yw4l"></td>
					<td class="tg-yw4l" style="vertical-align: top;padding-left: 50px;padding-top: 5px;padding-bottom: 5px;">Alamat</td>
					<td class="tg-lqy6" style="vertical-align: top;padding-top: 5px;padding-bottom: 5px;">:</td>
					<td class="tg-yw4l">
							<?php
								
								if($kontrak_mitra['status_jaminan']==0 || $kontrak_mitra['status_jaminan']==''){
									echo $kontrak_mitra['alamat'];
								}else{
									echo $kontrak_mitra['alamat_pemilik'];
								}

							 ?>
					</td>
					
				</tr>
				<tr>
					<td class="tg-yw4l"></td>
					<td class="tg-yw4l" style="vertical-align: top;padding-left: 50px;padding-top: 5px;padding-bottom: 5px;">Jabatan</td>
					<td class="tg-lqy6" style="vertical-align: top;padding-top: 5px;padding-bottom: 5px;">:</td>
					<td class="tg-yw4l">Bertindak Selaku Pemilik Jaminan Berupa : <?php echo $kontrak_mitra['nama_jaminan'];?> &nbsp;,&nbsp;<?php echo $kontrak_mitra['detail_jaminan'];?></td>
					
				</tr>
				<tr>
					<td class="tg-yw4l"></td>
					<td class="tg-yw4l" style="vertical-align: top;padding-left: 50px;padding-top: 5px;padding-bottom: 5px;">dan</td>
					<td></td>			
					<td></td>
				</tr>
				<tr>
					<td class="tg-yw4l"></td>
					<td class="tg-yw4l" style="vertical-align: top;padding-left: 50px;padding-top: 5px;padding-bottom: 5px;">Nama</td>
					<td class="tg-lqy6" style="vertical-align: top;padding-top: 5px;padding-bottom: 5px;">:</td>
					<td class="tg-yw4l"><b><?php echo $kontrak_mitra['nama'];?></b> &nbsp;  No.KTP. :&nbsp;<?php echo $kontrak_mitra['no_ktp'];?> </td>
					
				</tr>
				<tr>
					<td class="tg-yw4l"></td>
					<td class="tg-yw4l" style="vertical-align: top;padding-left: 50px;padding-top: 5px;padding-bottom: 5px;">Tempat / Tgl.Lahir</td>
					<td class="tg-lqy6" style="vertical-align: top;padding-top: 5px;padding-bottom: 5px;">:</td>
					<td class="tg-yw4l"> <?php echo $kontrak_mitra['tempat_lahir'];?> ,&nbsp;<?php echo $kaping;?>&nbsp;<?php echo $moonList[$bulan];?>&nbsp;<?php echo $tahun;?></td>
					
				</tr>
				<tr>
					<td class="tg-yw4l"></td>
					<td class="tg-yw4l" style="vertical-align: top;padding-left: 50px;padding-top: 5px;padding-bottom: 5px;">Alamat</td>
					<td class="tg-lqy6" style="vertical-align: top;padding-top: 5px;padding-bottom: 5px;">:</td>
					<td class="tg-yw4l"><?php echo $kontrak_mitra['alamat'];?>&nbsp;, Desa <?php echo $kontrak_mitra['kelurahan'];?> &nbsp;, Kecamatan &nbsp;<?php echo $kontrak_mitra['kecamatan'];?> &nbsp;, <?php echo $kontrak_mitra['kota'];?></td>
					
				</tr>
				<tr>
					<td class="tg-yw4l"></td>
					<td class="tg-yw4l" style="vertical-align: top;padding-left: 50px;padding-top: 5px;padding-bottom: 5px;">Jabatan</td>
					<td class="tg-lqy6" style="vertical-align: top;padding-top: 5px;padding-bottom: 5px;">:</td>
					<td class="tg-yw4l">Bertindak Selaku Pemilik Usaha Kecil <?php echo $kontrak_mitra['nama_perusahaan'];?><br> yang meminjam modal usaha dari PT Krakatau Steel (Persero) Tbk.</td>
					
				</tr>
				<tr><td colspan="4">&nbsp;</td></tr>
				<tr>
					<td class="tg-yw4l"></td>
					<td class="tg-yw4l" colspan="3" style="padding-left: 50px;">Selanjutnya disebut <b>PIHAK PERTAMA.</b></td>
				</tr>
				<tr><td colspan="4">&nbsp;</td></tr>
				<tr>
					<td class="tg-031e">&nbsp;</td>
					<td class="tg-yw4l" style="vertical-align: top;padding-left: 50px;padding-top: 5px;padding-bottom: 5px;">Nama</td>
					<td class="tg-lqy6" style="vertical-align: top;padding-top: 5px;padding-bottom: 5px;">:</td>
					<td class="tg-yw4l"><b><?= managerName?></b></td>			
				</tr>
				<tr>
					<td class="tg-yw4l"></td>
					<td class="tg-yw4l" style="vertical-align: top;padding-left: 50px;padding-top: 5px;padding-bottom: 5px;">Alamat</td>
					<td class="tg-lqy6" style="vertical-align: top;padding-top: 5px;padding-bottom: 5px;">:</td>
					<td class="tg-yw4l">PT Krakatau Steel (Persero) Cilegon - Banten</td>			
				</tr>
				<tr>
					<td class="tg-yw4l"></td>
					<td class="tg-yw4l" style="vertical-align: top;padding-left: 50px;padding-top: 5px;padding-bottom: 5px;">Jabatan</td>
					<td class="tg-lqy6" style="vertical-align: top;padding-top: 5px;padding-bottom: 5px;">:</td>
					<td class="tg-yw4l"><?=managerTitle?> <br> Bertindak Atas nama PT Krakatau Steel (Persero) Tbk. </td>			
				</tr>
				<tr><td colspan="4">&nbsp;</td></tr>
				<tr>
					<td class="tg-yw4l"></td>
					<td class="tg-yw4l" colspan="3" style="padding-bottom: 5px;padding-left: 50px;">Selanjutnya disebut <b>PIHAK KEDUA.</b></td>
				</tr>
				</table>
				</td>
				</tr>
				<tr>
					<td colspan="3" style="padding-top: 5px;padding-bottom: 5px;vertical-align: justify"><b>PIHAK PERTAMA</b> dan <b>PIHAK KEDUA</b> selanjutnya disebut <b>PARA PIHAK.</b><br>
					<b>PARA PIHAK menerangkan sebagai berikut:<br></b>
					<b>PIHAK PERTAMA</b> memberikan kuasa jual secara penuh kepada <b>PIHAK KEDUA</b> untuk melakukan penjualan Barang atau harta kekayaan berupa : <?php echo $kontrak_mitra['nama_jaminan'];?>&nbsp;, &nbsp;<?php echo $kontrak_mitra['detail_jaminan'];?>&nbsp;, &nbsp;Apa bila <b>PIHAK PERTAMA</b> tidak melakukan kewajiban sesuai yang tercantum dalam perjanjian No. Kontrak : &nbsp;<u><?php echo $kontrak_mitra['no_kontrak'];?></u>&nbsp; Tertanggal sesuai dalam Pembayaran angsuran.</td>

				</tr>
				<tr>
					<td colspan="3"><p>Demikian Perjanjian ini dibuat secara sadar tanpa paksaan dan tekanan pihak manapun untuk dipergunakan sebagaimana mestinya.</p></td>
				</tr>
				<tr>
					<td><center><b>PIHAK KEDUA</b></center></td>
					<td> </td>
					<td><center><b>PIHAK PERTAMA</b></center></td>
				</tr>
				<tr>
					<td><center><?=divisiTitle?></center></td>
					<td></td>
					<td>
							<center><img src="<?= url_img.'assets/images/mtr.png'?>" style="width: 40%;height: 40%; vertical-align: center;"></center>
							</td>
				</tr>
				
				<tr>
					<td> &nbsp;</td>
					<td> &nbsp;</td>
					<td> &nbsp;</td>
				</tr>
				<tr>
					<td> &nbsp;</td>
					<td> &nbsp;</td>
					<td> &nbsp;</td>
				</tr>
				<tr>
					<td> &nbsp;</td>
					<td> &nbsp;</td>
					<td> &nbsp;</td>
				</tr>
				
				<tr>
					<td><center><b><u><?=managerName?></u></b><br>Manager</center></td>
					<td></td>
					<td><center><b>(<?php  
							if ($kontrak_mitra['status_jaminan']==0 || $kontrak_mitra['status_jaminan']=='') {
								echo $kontrak_mitra['nama'];
							}else{
									echo $kontrak_mitra['pemilik_jaminan'];
								}							
							?>)<br>Pemilik Jaminan</center></b></td>
				</tr>
				
				<tr>
					<td> &nbsp;</td>
					<td> &nbsp;</td>
					<td> &nbsp;</td>
				</tr>
				<tr>
					<td ><center>MENYETUJUI</center></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td> &nbsp;</td>
					<td> &nbsp;</td>
					<td> &nbsp;</td>
				</tr>
				<tr>
					<td> &nbsp;</td>
					<td> &nbsp;</td>
					<td> &nbsp;</td>
				</tr>
				<tr>
					<td><center><b>(&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)</b></center><br><center>Ahli Waris Pemilik Jaminan</center></td>
					<td></td>
					<td><center><b>(<?php echo $kontrak_mitra['nama'];?>)</b><br>Pemilik Usaha Kecil</center></td>
				</tr>

			
			<!-- <tfoot> -->
				<tr>
					<td></td>
				</tr>
			<!-- </tfoot> -->
		</table>
		</div>

