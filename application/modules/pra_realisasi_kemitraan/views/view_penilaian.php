<?php
$app = 'Penilaian'; // nama aplikasi
$module = 'pra_realisasi_kemitraan';
$appLink = 'penilaian'; // controller
// $idField  = 'id_mitra'; //field key table
$idField='id_mitra';
?>

<script>
	var url;
	var app = "<?=$app?>";
	var appLink = '<?=$appLink?>';
	var module = '<?=$module?>';
	var idField = '<?=$idField?>';
	var idParent;
	    //file
	var file_type = JSON.parse('<?=fileUploadTipe5?>');
	var file_upload = true;

	function validate_file(obj){
        var file_name = $(obj).val().replace('C:\\fakepath\\', '');
        var file_name_attr = file_name.split('.');
        file_name_attr[2] = obj.files[0].size/1024;


        if(file_type.indexOf(file_name_attr[1]) == -1 || (file_name_attr[2] > <?=fileUploadSize5?>)){
        	$.messager.alert('Error Message', 'File upload harus (' + file_type.join('|') + ') dan size dibawah <?=fileUploadSize5?>KB', 'error');
        	$(obj).wrap('<form>').closest('form').get(0).reset();
        	$(obj).unwrap();
        }
    }
	
    function add(){
        $('#dlg').dialog('open').dialog('setTitle','Tambah ' + app);
		$('#fm').form('clear');
	    $('#tbl').html('Save');
		url = '<?=base_url($module . '/' . $appLink . '/create')?>';
    }
    function edit(){
        var row = $('#dg').datagrid('getSelected');
		if (row){
			$('#tbl').html('Simpan');
			$('#dlg').dialog('open').dialog('setTitle','Edit '+ app);
			$('#fm').form('load',row);
			url = '<?=base_url($module . '/' . $appLink . '/update')?>/'+row[idField];
	    }
    }
    function hapus(){
        var row = $('#dg').datagrid('getSelected');		
	    if (row){
			$.messager.confirm('Confirm','Apakah Anda Yakin Akan Menghapus ini?',function(r){
				if (r){
					$.post('<?=base_url($module . '/' . $appLink . '/delete')?>/'+row[idField],function(result){
						if (result.success){
							$('#dg').datagrid('reload');	// reload the user data
						} else {
							$.messager.show({	// show error message
								title: 'Error',
								msg:result.msg
							});
						}
					},'json');
				}
			});
		}
    }
    function formatValidasi(value,row,index){
    	
    	// console.log(value);
    	// rowSelected = value;
    if(row.status== 3){
        disposisi = '<center> <a onclick="javascript:validasi('+index+')" title="Validasi" class="btn btn-small btn-default easyui-tooltip" style="color:red"><i class="icon-check icon-large"></i></a> </center>';
    	}else{
    		disposisi = '<center> <a  title="Sudah tervalidasi" class="btn btn-small btn-default easyui-tooltip" style="color:green"><i class="icon-check icon-large"></i></a> </center>';
    		
    	}
        return disposisi;

    }
    
    function validasi(idx){
    	var row = $('#dg').datagrid('getRows')[idx];
    	// var row = $('#dg').datagrid('getSelected');		
	    if (row){
			$.messager.confirm('Confirm','Apakah Anda Yakin Akan Memvalidasi Data ini?',function(r){
				if (r){
					$.post('<?=base_url($module . '/' . $appLink . '/validasi')?>/'+row[idField],function(result){
						if (result.success){
							$('#dg').datagrid('reload');	// reload the user data
						} else {
							$.messager.show({	// show error message
								title: 'Error',
								msg:result.msg
							});
						}
					},'json');
				}
			});
		}
    }
    function save(){
	   $('#fm').form('submit',{
	    	url: url,
	    	onSubmit: function(){
	    		return $(this).form('validate');
	    	},
	    	success: function(result){
	    		var result = eval('('+result+')');
	    		if (result.success){
	    			$('#dlg').dialog('close');
	    			var g = $('#id_mitra').combogrid('grid');	// get datagrid object
					var r = g.datagrid('reload');	// get the selected row	
	    			$('#dg').datagrid('reload');
	                
	    		} else {
	             $.messager.alert('Error Message',result.msg,'error');
	    		}
	    	}
	    });
	}
	function doSearch(value){
	    $('#dg').datagrid('load',{    
	    	q:value  
	    });
	}

	//addtional
	function editPenilaian(){
		var row = $('#dg').datagrid('getSelected');
		if (row){
			if(row.status==3){
			//$('#tbl').html('Simpan');
			$('#dlgx').dialog('open').dialog('setTitle','Edit '+ app);
			idParent = row.id_mitra;
			reload1(); reload2(); reload3(); reload4(); reload5(); reload6(); reload7(); reload8(); reload9();
			reload10(); reload11(); reload12(); reload13();reload14();
			//$('#fm').form('load',row);
			//url = '<?=base_url($module . '/' . $appLink . '/update')?>/'+row[idField];
	    	}else{
			alert('Data Sudah di Validasi')
				}
    	}
	}
    
    function downloadLaporan(){
		var row = $('#dg').datagrid('getSelected');
		if (row){
			window.open('<?=base_url('pra_realisasi_kemitraan/penilaian/laporan')?>/'+row.id_mitra, '_blank');
	    }
    }
    
    function downloadLampiran(){
		var row = $('#dg').datagrid('getSelected');
		if (row){
			window.open('<?=base_url('pra_realisasi_kemitraan/penilaian/laporan_lampiran')?>/'+row.id_mitra, '_blank');
	    }
    }
    function upload_lampiran() {
        $('#fm_upload_file').form('submit',{
            url: '<?=base_url($module . '/' . $appLink . '/upload_lampiran')?>',
            success: function(result){
                var result = eval('('+result+')');
                if (result.success){
                    $('#dlg_upload').dialog('close');
                    $('#dg').datagrid('reload');
                } else {
                    $.messager.alert('Error Message',result.msg,'error');
                }
            }
        });
    }
    function upload_file_all(){
        var row = $('#dg').datagrid('getSelected');
		if (row){
		  $('#dlg_upload_all').dialog('open').dialog('setTitle','Upload File');
          $('.edit').hide();
          file_upload=false;
          
            //lampiran file
            if(row.file_survey != ''){
            	var file_survey = '<?=base_url()?>'+row.file_survey;
                $('#current_file').attr('href', file_survey).show();
                $('#current_file_inp').attr('value', row.file_survey);
            } 
            
            url = '<?=base_url($module . '/' . $appLink . '/upload_lampiran_all')?>/'+row[idField];
          }
    }
    function upload_lampiran_all(){
        var row = $('#dg').datagrid('getSelected');
        if(row){
            
            $('#fm_upload_file_all').form('submit',{
                url: url,
                success: function(result){
                    var result = eval('('+result+')');
                    if (result.success){
                        $('#dlg_upload_all').dialog('close');
                        $('#dg').datagrid('reload');
                        
                    } else {
                        $.messager.alert('Error Message',result.msg,'error');
                    }
                }
            });
        }
         
    }

    function upload_data_lampiran(id_mitra,folder,form,btn1,btn2){
        $('#'+form).form('submit',{
            url: "<?=base_url($module . '/' . $appLink . '/upload')?>/"+id_mitra+"/"+folder,
            onSubmit: function() {
                return $(this).form('validate');
            },
            success: function(result){
                var url_file = 'assets-upload-Survey-'+id_mitra+'-'+folder;
                $.get('<?php echo base_url();?>'+module+'/'+appLink+'/get_file/'+url_file,function(data){
                    if(data[0]){
                        $('#'+btn1).hide();
                        $('#'+btn2).show();
                    }
                });
                $('#'+form).form('reset');
            }
        });
    }
    function lam_file(value,row,index){


        var ktp = ''
        if(value != '') {
            ktp = '<a onclick="$(\'#frem\').dialog(\'open\');$(\'#frem iframe\').attr(\'src\',\'<?=base_url()?>'+row.file_survey+'\')" class="btn btn-small btn-default"><i class="icon-search"></i></a>';
        }

        return ktp;
    }

</script>
 
<div class="tabs-container">                
	<table id="dg" class="easyui-datagrid" 
	data-options="  
	    url: '<?=base_url($module . '/' . $appLink . '/read')?>',
		singleSelect:'true', 
	    title:'<?=$app?>',
	    toolbar:'#toolbar',
	    iconCls:'icon-cog',
	    rownumbers:'true',  
	    idField:'<?=$idField?>', 
	    pagination:'true',
	    fitColumns:'true',
	    pageList: [10,20,30]
	    "
	>
	    <thead>
	        <th field="id_mitra" width="130" sortable="true">ID</th>
	        <th field="nama" width="200" sortable="true">Nama Pemohon</th>
            <th field="nama_perusahaan" width="200" sortable="true">Nama Perusahaan</th>
            <th field="nama_jaminan" width="150" sortable="true">Jaminan</th>
            <th align="right" field="biaya_hidup" width="150" sortable="true" formatter="format_numberdisp" >Biaya Hidup/Bulan</th>
            <th field="detail_usaha" width="200" sortable="true">Detail Usaha</th>
            <th field="action" width="100" formatter="formatValidasi" sortable="true">Validasi</th>
            <th data-options="field:'file_survey',width:60" formatter="lam_file">Berkas<br />Survey</th>
            <!-- <th data-options="field:'file_survey',width:60" formatter="file_survey">Berkas<br />Survey</th> -->
	    </thead>
	</table>
	<!-- Model Start -->
	<div id="dlg" class="easyui-dialog" style="width:500px; height:450px; padding:10px" closed="true" buttons="#t_dlg_dis-buttons" >
	    <form id="fm" method="post" enctype="multipart/form-data" action="">
	    	<div id="tt" class="easyui-tabs" style="width:440px;height:auto;">
		        <div title="Data Pengusaha" style="padding:20px;">
			        <table width="100%" align="center" border="0">
			            <tr>
			                <td>Mitra</td>
			                <td>:</td>
			                <td>
			                	<select class="easyui-combogrid" name="id_mitra" id="id_mitra" required style="width:250px" data-options="
				                    panelWidth: 500,
				                    idField: 'id_mitra',
				                    textField: 'id_mitra',
				                    url:'<?=base_url($module . '/' . $appLink . '/getDataMitra')?>',
				                   
				                    columns: [[
				                        {field:'id_mitra',title:'ID',width:100},
				                        {field:'nama',title:'Nama Pemohon',width:120},
				                        {field:'nama_perusahaan',title:'Nama Perusahaan',width:120}
				                    ]],
				                    onSelect: function(index,row){
				                    $('#jaminan').val(row.jaminan);
				                    $('#nama_jaminan').val(row.nama_jaminan);
				                    $('#status_jaminan').val(row.status_rumah);
				                    $('#alamat').val(row.alamat);
				                    $('#propinsi').val(row.propinsi);
				                    $('#kota').val(row.kota);
				                    $('#kecamatan').val(row.kecamatan);
				                    $('#kelurahan').val(row.kelurahan);
				                    $('#mulai_usaha').val(row.tgl_berdiri);
				                    $('#detail_usaha').val(row.detail_usaha);
				                    $('#status_tempat').val(row.status_tempat);
				                    
				                    }
				                ">
			                	</select>
			                </td>
			            </tr>
			            <tr>
			                <td>Pekerjaan Lain</td>
			                <td>:</td>
			                <td>
			                    <input type="text" name="pekerjaan" id="pekerjaan" required style="width: 250px;">
			                </td>
			            </tr>
			            <tr>
			                <td>Tanggungan Keluarga</td>
			                <td>:</td>
			                <td>
			                    <input class="easyui-numberbox" data-options="label:'Currency:Orang ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',suffix:' Orang',width:'100%'" name="tanggungan" id="tanggungan" required style="width: 250px;">
			                </td>
			            </tr>
			            <tr>
			                <td>Biaya Hidup/Bulan</td>
			                <td>:</td>
			                <td>
			                    <input class="easyui-numberbox" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%'" name="biaya_hidup" id="biaya_hidup" required style="width: 250px;">
			                </td>
			            </tr>
			            <tr>
			                <td>Pendapatan Di Luar Usaha</td>
			                <td>:</td>
			                <td>
			                	<input class="easyui-numberbox" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%'" name="pendapatan_luar" id="pendapatan_luar" required style="width: 250px;">
			                </td>
			            </tr>
			            <tr>
			                <td>Jaminan Berupa</td>
			                <td>:</td>
			                <td>
			                    <input readonly  name="nama_jaminan" id="nama_jaminan" style="width: 250px;">
			                    <input type="hidden" id="jaminan" name="jaminan">
			                </td>
			            </tr>
			            <tr>
			                <td>Status Jaminan</td>
			                <td>:</td>
			                <td>
			                    <input type="text" name="status_jaminan" id="status_jaminan" readonly style="width: 250px;">
			                    	
			                </td>
			            </tr>
			        </table>
			    </div>
			    <div title="Data Perusahaan" style="padding:20px;">
			    	<table width="100%" align="center" border="0">
			            <tr>
			                <td>Detail Usaha</td>
			                <td>:</td>
			                <td>
			                    <input type="text" name="detail_usaha" id="detail_usaha" readonly required style="width: 250px;">
			                </td>
			            </tr>
			            <tr>
			                <td>Alamat Usaha</td>
			                <td>:</td>
			                <td>
			                    <input type="text" name="alamat" id="alamat" required style="width: 250px;" readonly>
			                </td>
			            </tr>
			            <tr>
			                <td>Propinsi</td>
			                <td>:</td>
			                <td>
			                    <input  name="propinsi" id="propinsi" required style="width: 250px;" readonly>
			                </td>
			            </tr>
			            <tr>
			                <td>Kota/Kabupaten</td>
			                <td>:</td>
			                <td>
			                    <input  name="kota" id="kota" readonly style="width: 250px;">
			                </td>
			            </tr>
			            <tr>
			                <td>Kecamatan</td>
			                <td>:</td>
			                <td>
			                    <input readonly name="kecamatan" id="kecamatan"  style="width: 250px;" >
			                </td>
			            </tr>
			            <tr>
			                <td>Kelurahan</td>
			                <td>:</td>
			                <td>
			                    <input readonly name="kelurahan" id="kelurahan" >
			                </td>
			            </tr>
			            <tr>
			                <td>Status Tempat Usaha</td>
			                <td>:</td>
			                <td>
			                    <input class="easyui-textbox" type="text" name="status_tempat" id="status_tempat" readonly style="width: 250px;" />
			                </td>
			            </tr>
			            <tr>
			                <td>Waktu Mulai Usaha</td>
			                <td>:</td>
			                <td>
			                    <input  name="mulai_usaha" id="mulai_usaha" readonly style="width: 120px;">
			                </td>
			            </tr>
			            <tr>
			                <td>Kemampuan Membayar</td>
			                <td>:</td>
			                <td>
			                    <input class="easyui-numberbox" data-options="precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%'" name="mampu_bayar" id="mampu_bayar" required  style="width: 250px;">
			                </td>
			            </tr>
			            <tr>
			                <td>Jumlah Tenaga Kerja</td>
			                <td>:</td>
			                <td>
			                    <input class="easyui-numberbox" data-options="precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'',width:'100%'" name="tenagakerja" id="tenagakerja" required  style="width: 250px;">
			                </td>
			            </tr>
			        </table>
			    </div>
	    </form>
	   
	   	<!-- Tombol Add, Edit, Hapus Datagrid -->
       	<div id="toolbar">  
            <table align="center" style="padding: 0px; width: 99%;">
                <tr>
                    <td>
                       <a href="javascript:void(0)" class="btn btn-small btn-success" onclick="javascript:add()"><i class="icon-plus-sign icon-large"></i>&nbsp;Add</a>
                         <!-- <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:edit()"><i class="icon-edit"></i>&nbsp;Edit</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-danger" onclick="javascript:hapus()"><i class="icon-trash icon-large"></i>&nbsp;Delete</a> -->
                        <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:editPenilaian()"><i class="icon-edit"></i>&nbsp;Penilaian&nbsp;</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:downloadLaporan()"><i class="icon-download"></i>&nbsp;LHS&nbsp;</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:downloadLampiran()"><i class="icon-download"></i>&nbsp;Lampiran&nbsp;</a>
                        <a id="upload_file" href="javascript:void(0)" class="btn btn-small btn-success" iconCls: 'icon-search' onclick="javascript:upload_file_all()"><i class="icon-arrow-up icon-large"></i>&nbsp;Uplod File</a>
                        
                        <!-- <a href="javascript:void(0)" class="btn btn-small btn-danger" onclick="javascript:validasi()"><i class="icon-check icon-large"></i>&nbsp;Validasi</a> -->
                    </td>
                    <td>&nbsp;</td>
                    <td align="right">
                    	<input class="easyui-searchbox" prompt="Silahkan Input Field" searcher="doSearch"  style="width:300px"/>
                    </td>
                </tr>
            </table>  
        </div>  
        <!-- end tombol datagrid -->
        <!-- <div id="dlg_upload" class="easyui-dialog" title="Upload File" data-options="iconCls:'icon-file'" closed="true" style="width:400px;height:110px;padding:10px">
        <form id="fm_upload_file" method="POST" enctype="multipart/form-data">
            <input type="hidden" name="id_mitra" id="id_mitra" />
            <input type="hidden" name="jenis" id="jenis" /> -->
            <!-- <input type="file" id="file" name="file" >
            <a href="javascript:void(0)" onclick="upload_lampiran()" class="btn btn-small btn-default">Upload</a>
        </form> -->
    <!-- </div> -->
        <!-- Tombol simpan & Batal Form -->
        <div id="t_dlg_dis-buttons">
            <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:save()"><i class="icon-save icon-large"></i>&nbsp;Simpan</a>
            <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:$('#dlg').dialog('close')"><i class="icon-remove icon-large"></i>&nbsp;Batal</a>      
        </div>

	</div>
	<div class="easyui-dialog" id="frem" style="width: 500px; height: 400px;" closed="true">
     <iframe src="" style="width: 100%;height: 100%">
         
     </iframe>
    </div>
	
	<!-- Model end -->
	<div id="dlg_upload" class="easyui-dialog" title="Upload File" data-options="iconCls:'icon-file'" closed="true" style="width:400px;height:110px;padding:10px">
        <form id="fm_upload_file" method="POST" enctype="multipart/form-data">
            <input type="hidden" name="id_mitra" id="id_mitra" />
            <input type="hidden" name="jenis" id="jenis" />
            <input type="file" id="file" name="file" >
            <a href="javascript:void(0)" onclick="upload_lampiran()" class="btn btn-small btn-default">Upload</a>
        </form>
    </div>
	<!-- Model Start -->
	<div id="dlgx" class="easyui-dialog" style="width:800px; height:auto; padding:10px" closed="true" buttons="#t_dlg_dis-buttonsx" >
		<div class="easyui-tabs" style="width:770px;height:auto">
			<div title="Asset Perusahaan" style="padding:10px">
				<?php $this->load->view('view_k_asset'); ?>
			</div>
			<div title="Periode Perputaran Produksi" style="padding:10px">
				<?php $this->load->view('view_k_perputaran'); ?>
			</div>
			<div title="Produksi dan Penjualan Per Produksi" style="padding:10px">
				<?php $this->load->view('view_k_ppenjualan'); ?>
			</div>
			<div title="Biaya Bahan Baku" style="padding:10px">
				<?php $this->load->view('view_k_bbaku'); ?>
			</div>
			<div title="Biaya Bahan Bantu" style="padding:10px">
				<?php $this->load->view('view_k_bbbantu'); ?>
			</div>
			<div title="Biaya Tenaga Kerja Tetap" style="padding:10px">
				<?php $this->load->view('view_k_ttkerja'); ?>
			</div>
			<div title="Biaya Tenaga Kerja Tak Tetap" style="padding:10px">
				<?php $this->load->view('view_k_kttetap'); ?>
			</div>
			<div title="Biaya Transportasi" style="padding:10px">
				<?php $this->load->view('view_k_btransport'); ?>
			</div>
			<div title="Biaya Administrasi dan Umum" style="padding:10px">
				<?php $this->load->view('view_k_badmum'); ?>
			</div>
			<div title="Biaya Lain-lain" style="padding:10px">
				<?php $this->load->view('view_k_blain'); ?>
			</div>
			<div title="Posisi Aktiva" style="padding:10px">
				<?php $this->load->view('view_k_paktiva'); ?>
			</div>
			<div title="Posisi Pasiva" style="padding:10px">
				<?php $this->load->view('view_k_ppasiva'); ?>
			</div>
			<div title="Modal Investasi" style="padding:10px">
				<?php $this->load->view('view_k_investasi'); ?>
			</div>
			<div title="Rekomendasi" style="padding:10px">
				<?php $this->load->view('view_k_rekomendasi'); ?>
			</div>
		</div>
		<!-- Tombol simpan & Batal Form -->
        <div id="t_dlg_dis-buttonsx">
            <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:$('#dlgx').dialog('close')"><i class="icon-remove icon-large"></i>&nbsp;Tutup</a>      
        </div>
	</div>
	<!--Dialog upload file-->
<div id="dlg_upload_all" class="easyui-dialog" style="width:500px; height:200px; padding:10px" closed="true" buttons="#t_dlg_upload_view-buttons" >
    <form id="fm_upload_file_all" method="POST" enctype="multipart/form-data">
        <table>
            
            <tr>
				<td>File Survey</td>
				<td>:</td>
				<td><a href="" id="current_file" class="edit">File</a><input type="hidden" name="current_file" id="current_file_inp" />
				<input type="file" id="file_survey" name="file_survey" size="20" onchange="validate_file(this)" />
				</td>
		   </tr>
        </table>
    </form>
    <!--Tombol upload file all-->
    <div id="t_dlg_upload_view">
        <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:upload_lampiran_all()"><i class="icon-arrow-up icon-large"></i>&nbsp;Upload</a>
        <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:$('#dlg_upload_all').dialog('close')"><i class="icon-remove icon-large"></i>&nbsp;Batal</a>      
    </div>
    <!--End Tombol upload file all-->
</div>
<!--End Dialog upload file-->
