<?php
    if (!defined('BASEPATH'))
        exit('No direct script access allowed');
        /* @property phpword_model $phpword_model */
        include_once(APPPATH."third_party/PhpWord/Autoloader.php");
        //include_once(APPPATH."core/Front_end.php");
        
        use PhpOffice\PhpWord\Autoloader;
        use PhpOffice\PhpWord\Settings;
        Autoloader::register();
        Settings::loadConfig();
    class kontrak_mitra extends MY_Controller {
        public $models = array('kontrak_mitra');
        
        public function __construct() {
            parent::__construct();
        }

        public function index() {
            $data = array();
            $data['menu'] = $this->model_menu->getAllMenu();

            $this->template->load('template', 'view_kontrak_mitra', $data);
        }

        public function read() {
            $this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read());
        }

        public function read_all() {
            $this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read_all());
        }

        public function create() {
            $id_mitra=$this->input->post('id_mitra');

            $data_id = array(
                   'status' => '8'
            );
            $this->db->where('id_mitra', $id_mitra);
            $this->db->update('tmitra', $data_id);

            $data_id = array(
                   'tgl_kontrak'    => $this->input->post('tgl_input'),
                   'no_kontrak'     => $this->no_kontrak()
            );
            $this->db->where('id_mitra', $id_mitra);
            $result = $this->db->update('tpersetujuan_mitra', $data_id);

            // additional block
            
            if ($result == 1) {
                $this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
            } else {
                $this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => $this->db->_error_message())));
            }
        }

        public function update() {
            $param = $this->uri->segment(4); // parameter key

            // additional block

            // addtional data

            // additional where
            $this->where_add['id_kontrak'] = $param;
            

            $result = $this->{$this->models[0]}->update($this->where_add, $this->data_add);
            if ($result == 1) {
                $this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
                
            } else {
                $this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'Data Gagal Di Update !')));
            }
        }

        public function delete() {
            $param = $this->uri->segment(4); // parameter key

            // additional block

            // additional where
            $this->where_add['id_mitra'] = $param;

            $result = $this->{$this->models[0]}->update($this->where_add, $this->data_add);
            if ($result == 1) {
                $this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
            } else {
                $this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'Data Gagal Di Hapus !')));
            }
        }
        function sektormitra() {
            header("Content-Type: application/json");
            $cari = $this->input->post('q') ? $this->input->post('q') : '';
            $this->{$this->models[0]}->sektormitra($cari);
        }
        function getmitra() {
            header("Content-Type: application/json");
            $cari = $this->input->post('q') ? $this->input->post('q') : '';
            $this->{$this->models[0]}->getmitra($cari);
        }

        function kontrak(){
            $phpWord = new \PhpOffice\PhpWord\PhpWord();
            $phpWord->getCompatibility()->setOoxmlVersion(14);
            $phpWord->getCompatibility()->setOoxmlVersion(15);
            $url_img = url_img;
            $pathImg = $url_img.'/assets/images/';
            $filename = 'KontrakMitra.docx';

            $section = $phpWord->addSection(array('marginTop'=>1));
            $section->addText('I am placed on a default section.');
            //$header = $section->createHeader();
            //$header->firstPage();
            //$header->addImage($pathImg.'krakatausteelpartnership.png', array('width'=>100, 'height'=>100));

            $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
            $objWriter->save($path.$filename);
            // send results to browser to download
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename='.$filename);
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            header('Content-Length: ' . filesize($path.$filename));
            flush();
            readfile($path.$filename);
            unlink($path.$filename); // deletes the temporary file
            exit;
        }

        function previewkontrak_mitra($id){    // load dompdf
    
            //load content html
          $kontrak_mitra= $this->{$this->models[0]}->kontrak_mitra($id)->row_array();
          $jumlahbulan=$kontrak_mitra['tahun']*12;
            // $html = $this->load->view('sph3.php', $data, true);
          /*  $html = $this->load->view('kontrak_mitra_print',$data, true);

            // echo var_dump($data);
            // echo $html;
            // die();
            // create pdf using dompdf
            $this->load->library('dompdf_gen'); // Load library
            $this->dompdf->set_paper('A4', 'portrait'); // Setting Paper
            // Convert to PDF
            $this->dompdf->load_html($html);
            $this->dompdf->render();
            $this->dompdf->stream("Kontrak Kerjasama Kemitraan.pdf");*/
            $this->load->helper('tanggal_indo');
            $this->load->helper('terbilang_helper');
            $url_img = url_img;
            $managerName= managerName;
            $managerTitle= managerTitle;
            $divisi= divisiTitle;
            $phpWord = new \PhpOffice\PhpWord\PhpWord();
            $phpWord->getCompatibility()->setOoxmlVersion(14);
            $phpWord->getCompatibility()->setOoxmlVersion(15);
            $targetFile = $url_img."/global/uploads/headkti.png";
            
            $path = $url_img.'/assets/captcha/';
            $pathImg = $url_img.'/assets/images/';
            $filename = 'KontrakMitra'.$id.'.docx';
            $section = $phpWord->addSection(array('marginTop'=>400));
            //$section->addImage('krakatausteelpartnership.png')
            //header
            //$header = $section->addHeader();
            //$header->firstPage();
            // $section->addImage($pathImg.'krakatausteelpartnership.png', array('width'=>150, 'height'=>72, 'align' => 'center'));
            $section->addImage($pathImg. 'headkti.png', array('width'=>150, 'height'=>72, 'align' => 'center'));
            $section->addTextBreak(1);
            //content
            $section->addText("PERJANJIAN", array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center', 'spaceAfter' => 10));
            $section->addText("ANTARA", array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center', 'spaceAfter' => 10));
            $section->addText("PT.KRAKATAU STEEL(PERSERO)Tbk", array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center', 'spaceAfter' => 10));
            $section->addText("DENGAN", array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center', 'spaceAfter' => 10));
            $section->addText(strtoupper($kontrak_mitra['nama_perusahaan']), array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center', 'spaceAfter' => 10));
            $section->addText("TENTANG", array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center', 'spaceAfter' => 10));
            $section->addText(strtoupper("PINJAMAN MODAL USAHA"), array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center', 'spaceAfter' => 40));
            //$section->addText(strtoupper("Nomor Kontrak : ".$kontrak_mitra['no_kontrak']."/MB/".date("Y")), array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center', 'spaceAfter' => 40));
            $section->addText(strtoupper("Nomor Kontrak : ".$kontrak_mitra['no_kontrak']), array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center', 'spaceAfter' => 40));
            //$section->addTextBreak(5);
            //$section->addText(date("Y"), array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center', 'spaceAfter' => 10));
            $section->addTextBreak(1);
            $this->load->helper('tanggal_indo');
            $bulan=date('m',strtotime(date("Y-m-d")));
            $isi="Perjanjian Modal Usaha ini (selanjutnya disebut Perjanjian) ditandatangani pada hari ini "."tanggal      , bulan                 , tahun ".date("Y")." (".terbilang_display(date("Y"),3).") di Cilegon, oleh dan antara :";
            $section->addText($isi, array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify', 'spaceAfter' => 10));
            $section->addTextBreak(1);
            //buat tabel pertama
            //pihak pertama
            $table = $section->addTable();
            $table->addRow();
            // Add cells
            $table->addCell()->addText("1. ", array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center'));
            $table->addCell(4000)->addText("PT.KRAKATAU STEEL (PERSERO)Tbk", array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table->addCell()->addText(":", array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center'));
            $isipihak1="Berkedudukan di Cilegon, di Jalan Industri Nomor 5, Provinsi Banten, sesuai anggaran dasar yang tercantum dalam akta nomor 51 Tanggal 27 April 2015 yang dibuat oleh Jose Dima Satria, S.H., M.Kn.. Notaris di Jakarta, dalam hal ini diwakili oleh ".$managerName.", ".$managerTitle." berdasarkan Surat Keputusan Direksi PT Krakatau Steel (Persero) Tbk. Nomor  59/C/DU-KS/Kpts/2015 tanggal 29  Mei 2015, dan Pengaturan Kewenangan di Divisi Program Kemitraan Dan Bina Lingkungan No. 49 A/C/DU- KS/Kpts/2006 tanggal 22 Agustus 2006, yang bertindak mewakili Direksi, dari dan oleh sebab itu bertindak untuk dan atas nama Krakatau Steel (Persero) Tbk. selanjutnya disebut PIHAK PERTAMA.";
            //$html="";
            //$html.=$isipihak1;
            //$xhtml= new \PhpOffice\PhpWord\Shared\Html();
            //$xhtml->addHtml($section, $xhtml);
            
            $table->addCell(5000)->addText($isipihak1, array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify'));
            //pihak kedua
            $table->addRow();
            // Add cells
            $table->addCell()->addText("2. ", array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center'));
            $table->addCell(4000)->addText(strtoupper($kontrak_mitra['nama_perusahaan']), array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table->addCell()->addText(":", array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center'));
            $isipihak2="Berkedudukan di ".$kontrak_mitra['alamat'].", dalam hal ini diwakili oleh ".$kontrak_mitra['nama'].", Nomor KTP ".$kontrak_mitra['no_ktp'].", lahir di ".$kontrak_mitra['tempat_lahir'].", tanggal ".tanggal_display($kontrak_mitra['tgl_lahir']).", alamat rumah di ".$kontrak_mitra['alamat']." dalam hal ini bertindak atas nama Pimpinan ".strtoupper($kontrak_mitra['nama_perusahaan']).", yang bergerak pada ".$kontrak_mitra['nama_sektor'].", ".$kontrak_mitra['detail_usaha'].", dari dan oleh sebab itu bertindak dan untuk atas nama ".strtoupper($kontrak_mitra['nama_perusahaan']).", selanjutnya disebut PIHAK KEDUA.";
            
            $table->addCell(5000)->addText($isipihak2, array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify'));
            $section->addTextBreak(1);
            $isi2="PIHAK PERTAMA dan PIHAK KEDUA selanjutnya disebut PARA PIHAK";
            $section->addText($isi2, array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify', 'spaceAfter' => 10));
            $section->addText("Kedua Pihak terlebih dahulu menerangkan : ", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify', 'spaceAfter' => 10));
            //$section->addTextBreak(1);
            $section = $phpWord->addSection();
            $pertama= "1. Bahwa PIHAK PERTAMA adalah salah satu Badan Usaha Milik Negara yang bergerak di bidang industri baja dan sebagaimana amanat Peraturan Menteri Negara Badan Usaha Milik Negara Republik Indonesia Nomor PER-02/MBU/7/2017 tanggal 20  Juli 2017 tentang Program Kemitraan Badan Usaha Milik Negara  dengan Usaha Kecil dan Program Bina Lingkungan, PIHAK PERTAMA menjalankan kegiatan kemitraan dengan usaha kecil, dalam hal ini memberikan pinjaman modal usaha bagi usaha kecil mitra binaan.";
            $section->addText($pertama, array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify', 'spaceAfter' => 10));
            $section->addTextBreak(1);
            $kedua="2. Bahwa PIHAK KEDUA merupakan usaha kecil mitra binaan PIHAK PERTAMA yang bergerak di bidang usaha Genteng bermaksud mengajukan permohonan Pinjaman Modal Usaha kepada PIHAK PERTAMA.";
            $section->addText($kedua, array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify', 'spaceAfter' => 10));
            $section->addTextBreak(1);
            $ketiga="3. Berdasarkan permohonan PIHAK KEDUA dan survey serta evaluasi kelayakan usaha  PIHAK    KEDUA,  maka PIHAK PERTAMA menyetujui memberikan pinjaman dana pengembangan usaha    untuk Pembinaan Usaha Kecil kepada PIHAK KEDUA.";
            $section->addText($ketiga, array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify', 'spaceAfter' => 10));
            $section->addTextBreak(1);
            $keempat="4. PIHAK PERTAMA menyetujui memberikan Pinjaman Modal Usaha kepada PIHAK KEDUA dan PIHAK KEDUA menerima pinjaman dimaksud.";
            $section->addText($keempat, array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify', 'spaceAfter' => 10));
            $section->addTextBreak(1);
            $berdasarkan="Berdasarkan hal-hal tersebut di atas Kedua Pihak sepakat mengatur hal dimaksud berdasarkan suatu Perjanjian dengan syarat-syarat dan ketentuan-ketentuan sebagai berikut :";
            $section->addText($berdasarkan, array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify', 'spaceAfter' => 10));
            $section = $phpWord->addSection();
            $section->addText("PASAL 1", array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center', 'spaceAfter' => 10));
            $section->addText("MAKSUD DAN TUJUAN", array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center', 'spaceAfter' => 10));
            $section->addTextBreak(1);
            $pasal1="Pemberian pinjaman oleh PIHAK PERTAMA kepada PIHAK KEDUA dimaksudkan untuk digunakan bagi kepentingan pembinaan dan pengembangan usaha kecil dan dalam rangka meningkatkan taraf hidup pengusaha kecil khususnya dan masyarakat sekitar pada umumnya.";
            $section->addText($pasal1, array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify', 'spaceAfter' => 10));
            $section->addTextBreak(1);
            $section->addText("PASAL 2", array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center', 'spaceAfter' => 10));
            $section->addText("PINJAMAN DAN PEMBAYARAN", array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center', 'spaceAfter' => 10));
            $section->addTextBreak(1);
            //buat tabel kedua
            $table2 = $section->addTable();
            $table2->addRow();
            $table2->addCell(500)->addText("(1) ", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table2->addCell(10000)->addText("PIHAK KEDUA memperoleh pinjaman dari PIHAK PERTAMA untuk pengembangan usaha Genteng berupa Modal Usaha dengan jangka waktu, jumlah, jasa, dan angsuran sebagai berikut :", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify'));
            //buat tabel ketiga
            $table3 = $section->addTable();
            $table3->addRow();
            $table3->addCell(500)->addText("", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table3->addCell(3000)->addText("Jangka  Waktu Pinjaman", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify'));
            $table3->addCell(500)->addText(" : ", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table3->addCell(6500)->addText($kontrak_mitra['tahun']." Tahun  atau (".ucwords(terbilang_display($kontrak_mitra['tahun'],3))." Tahun) ".$jumlahbulan."  Bulan  atau (".ucwords(terbilang_display($jumlahbulan,3))." Bulan) ", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify'));
            $table3->addRow();
            $table3->addCell(500)->addText("", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table3->addCell(3000)->addText("Jumlah Pinjaman", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify'));
            $table3->addCell(500)->addText(" : ", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table3->addCell(6500)->addText("Rp ".number_format($kontrak_mitra['angsuran_pokok'])." (".ucwords(terbilang_display($kontrak_mitra['angsuran_pokok'],3))." rupiah).", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify'));
            $table3->addRow();
            $table3->addCell(500)->addText("", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table3->addCell(3000)->addText("Jasa Pinjaman  3,00% Pertahun", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify'));
            $table3->addCell(500)->addText(" : ", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table3->addCell(6500)->addText("Rp ".number_format($kontrak_mitra['angsuran_bunga'])." (".ucwords(terbilang_display($kontrak_mitra['angsuran_bunga'],3))." rupiah).", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify'));
            $table3->addRow();
            $table3->addCell(500)->addText("", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table3->addCell(3000)->addText("Total Pinjaman", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify'));
            $table3->addCell(500)->addText(" : ", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table3->addCell(6500)->addText("Rp ".number_format($kontrak_mitra['total_pinjaman_bunga'])."(".ucwords(terbilang_display($kontrak_mitra['total_pinjaman_bunga'],3))." rupiah).", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify'));
            $table3->addRow();
            $table3->addCell(500)->addText("", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table3->addCell(3000)->addText("Angsuran per bulan", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify'));
            $table3->addCell(500)->addText(" : ", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table3->addCell(6500)->addText("Rp ".number_format($kontrak_mitra['angsuran_bulanan'])."(".ucwords(terbilang_display($kontrak_mitra['angsuran_bulanan'],3))." rupiah).", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify'));
            //tabel kedua
            $table2 = $section->addTable();
            $table2->addRow();
            $table2->addCell(500)->addText("(2) ", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table2->addCell(10000)->addText("Pinjaman dimaksud akan diberikan oleh PIHAK PERTAMA kepada PIHAK KEDUA dengan cara ditransfer melalui Bank yang ditunjuk oleh PIHAK KEDUA.", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify'));
            $table2->addRow();
            $table2->addCell(500)->addText("(3) ", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table2->addCell(10000)->addText("Pengembalian angsuran akan dilakukan oleh PIHAK KEDUA kepada PIHAK PERTAMA dimulai 1(satu) bulan setelah proses keuangan ke bank yang ditunjuk PIHAK PERTAMA yakni :", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify'));
            $table2->addRow();
            $table2->addCell(500)->addText("", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table2->addCell(10000)->addText("Bank Mandiri Cabang Cilegon Anyer", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify'));
            $table3 = $section->addTable();
            $table3->addRow();
            $table3->addCell(500)->addText("", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table3->addCell(3000)->addText("Nama Rekening", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify'));
            $table3->addCell(500)->addText(" : ", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table3->addCell(6500)->addText("Pegelkop PT. Krakatau Steel ", array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify'));
            $table3->addRow();
            $table3->addCell(500)->addText("", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table3->addCell(3000)->addText("Nomor Rekening", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify'));
            $table3->addCell(500)->addText(" : ", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table3->addCell(6500)->addText("116-0091023789", array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify'));
            $table2 = $section->addTable();
            $table2->addRow();
            $table2->addCell(500)->addText("(4) ", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table2->addCell(10000)->addText("Biaya-biaya yang timbul sehubungan dengan ketentuan ayat (3) pasal ini dan/atau sehubungan dengan pelaksanaan Perjanjian ini sepenuhnya menjadi beban dan tanggung jawab PIHAK KEDUA.", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify'));
            $section->addTextBreak(1);
            $section->addText("PASAL 3", array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center', 'spaceAfter' => 10));
            $section->addText("HAK PIHAK PERTAMA", array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center', 'spaceAfter' => 10));
            $section->addTextBreak(1);
            $section->addText("PIHAK PERTAMA berhak untuk  : ", array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify', 'spaceAfter' => 10));
            $section->addTextBreak(1);
            $table2 = $section->addTable();
            $table2->addRow();
            $table2->addCell(500)->addText("(1) ", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table2->addCell(10000)->addText("Melakukan segala tindakan yang dianggap perlu dan tidak terbatas mengawasi, memonitor, meminta keterangan dan/atau melakukan investigasi baik langsung maupun melalui pihak ketiga perihal pemanfaatan atau penggunaan pinjaman oleh PIHAK KEDUA. ", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify'));
            $table2->addRow();
            $table2->addCell(500)->addText("(2) ", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table2->addCell(10000)->addText("Membatalkan sebagian atau seluruh pinjaman yang diberikan kepada PIHAK KEDUA apabila PIHAK KEDUA tidak mentaati kewajiban-kewajiban sebagaimana disyaratkan dalam Perjanjian ini dan/atau berdasarkan hasil evaluasi PIHAK PERTAMA atau pihak ketiga yang ditunjuk hal ini layak untuk dilakukan.", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify'));
            $table2->addRow();
            $table2->addCell(500)->addText("(3) ", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table2->addCell(10000)->addText("Memberikan informasi kepada instansi terkait maupun BUMN Pembina lainnya mengenai performansi PIHAK KEDUA, apabila PIHAK KEDUA tidak memenuhi kewajiban-kewajiban sebagaimana diatur dalam Perjanjian ini.", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify'));
            $section->addTextBreak(1);
            $section->addText("PASAL 4", array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center', 'spaceAfter' => 10));
            $section->addText("PERNYATAAN DAN JAMINAN", array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center', 'spaceAfter' => 10));
            $section->addTextBreak(1);
            $section->addText("PIHAK KEDUA bersama ini menyatakan dan menjamin bahwa : ", array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify', 'spaceAfter' => 10));
            $section->addTextBreak(1);
            $table2 = $section->addTable();
            $table2->addRow();
            $table2->addCell(500)->addText("(1) ", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table2->addCell(10000)->addText("PIHAK KEDUA mempunyai kewenangan dan kekuasaan serta berhak untuk membuat, menandatangani, dan melaksanakan segala ketentuan dalam Perjanjian ini berdasarkan anggaran dasar dan/atau ketentuan hukum yang berlaku untuk sahnya suatu perjanjian.", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify'));
            $table2->addRow();
            $table2->addCell(500)->addText("(2) ", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table2->addCell(10000)->addText("PIHAK KEDUA akan menggunakan Pinjaman modal usaha ini sesuai dengan rencana pengembangan usaha. Apabila dalam pelaksanaannya tidak sesuai dengan rencana, Pihak Kedua bersedia mendapat sangsi sesuai dengan aturan yang berlaku.", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify'));
            $table2->addRow();
            $table2->addCell(500)->addText("(3) ", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table2->addCell(10000)->addText("Perjanjian ini dan segala dokumen yang timbul sehubungan dengan atau sebagai akibat dari Perjanjian ini merupakan satu kesatuan yang sah dan mengikat PIHAK KEDUA, yang mana tidak melanggar ketentuan hukum yang berlaku dan/atau tidak melanggar setiap Perjanjian atau dokumen yang berlaku yang telah ada yang mengikat PIHAK KEDUA sebelum ditandatangani Perjanjian ini, kecuali terhadap hal-hal yang telah diberitahukan terlebih dahulu secara tertulis oleh PIHAK KEDUA kepada PIHAK PERTAMA.", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify'));
            $table2->addRow();
            $table2->addCell(500)->addText("(4) ", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table2->addCell(10000)->addText("Tidak ada sengketa maupun tuntutan terhadap PIHAK KEDUA mau pun terhadap harta kekayaan yang dijadikan jaminan, baik di dalam maupun diluar pengadilan yang membawa dampak resiko terhadap kelangsungan usaha PIHAK KEDUA pada umumnya dan keadaan keuangan PIHAK KEDUA khususnya, sehingga dapat  membahayakan PIHAK PERTAMA atas pemberian pinjaman sebagaimana dimaksud Perjanjian ini.", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify'));
            $table2->addRow();
            $table2->addCell(500)->addText("(5) ", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table2->addCell(10000)->addText("PIHAK KEDUA tidak akan menuntut secara hukum berupa apapun terhadap PIHAK PERTAMA, namun tidak terbatas pada tuntutan ganti rugi atas kerugian-kerugian yang mungkin timbul kemudian dan merugikan PIHAK KEDUA sebagai akibat dari Perjanjian ini.", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify'));
            $table2->addRow();
            $table2->addCell(500)->addText("(6) ", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table2->addCell(10000)->addText("Tidak dalam keadaan lalai, wanprestasi dan/atau melakukan pelanggaran berdasarkan Perjanjian kredit dengan pihak lain terutama Bank.", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify'));
            $table2->addRow();
            $table2->addCell(500)->addText("(7) ", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table2->addCell(10000)->addText("Tidak dalam keadaan dan/atau sedang mengajukan permohonan penundaaan pembayaran terhadap fasilitas kredit yang diberikan berdasarkan Perjanjian ini dan/atau tidak dinyatakan pailit dan/atau tidak kehilangan haknya untuk mengurus dan menguasai harta kekayaannya.", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify'));
            $section->addTextBreak(1);
            $section->addText("PASAL 5", array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center', 'spaceAfter' => 10));
            $section->addText("JAMINAN PINJAMAN", array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center', 'spaceAfter' => 10));
            $section->addTextBreak(1);
            $table2 = $section->addTable();
            $table2->addRow();
            $table2->addCell(500)->addText("(1) ", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table2->addCell(10000)->addText("Untuk menjamin pembayaran lunas pinjaman dan seluruh pelaksanaan sebagaimana dimaksud oleh Perjanjian ini, maka PIHAK KEDUA wajib menyerahkan kepada PIHAK PERTAMA jaminan-jaminan sebagaimana akan tertuang dalam model penyerahan jaminan yang sudah diatur tersendiri, serta menyerahkan dokumen asli jaminan tersebut kepada PIHAK PERTAMA paling lambat pada saat ditandatanganinya Perjanjian ini.", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify'));
            $table2->addRow();
            $table2->addCell(500)->addText("(2) ", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table2->addCell(10000)->addText("Sehubungan ayat (1) Pasal ini, PIHAK KEDUA wajib membuat dan menyerahkan kepada PIHAK PERTAMA tentang Surat Kuasa Menjual yang diketahui oleh para Ahli Waris PIHAK KEDUA atau atas persetujuan ahli waris pemilik jaminan yang menyatakan bahwa, ''Apabila PIHAK KEDUA tidak dapat melunasi pinjaman dan/atau tidak melaksanakan atau melanggar berdasarkan ketentuan-ketentuan dimaksud dalam Perjanjian ini, maka PIHAK PERTAMA diberi hak secara serta merta tanpa menunggu putusan pengadilan yang berkekuatan hukum tetap namun tidak terbatas untuk menjual  agunan dan/atau harta kekayaan yang baik yang dijaminkan  atau tidak oleh PIHAK KEDUA sampai dengan pinjaman PIHAK KEDUA kepada PIHAK PERTAMA lunas seluruhnya'' paling lambat pada saat Perjanjian ini ditandatangani. (Surat Kuasa Menjual terlampir).", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify'));
            $section->addTextBreak(1);
            $section->addText("PASAL 6", array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center', 'spaceAfter' => 10));
            $section->addText("SANKSI", array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center', 'spaceAfter' => 10));
            $section->addTextBreak(1);
            $table2 = $section->addTable();
            $table2->addRow();
            $table2->addCell(500)->addText("(1) ", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table2->addCell(10000)->addText("Apabila di kemudian hari PIHAK PERTAMA mengetahui bahwa PIHAK KEDUA telah mendapat bantuan dan atau binaan dari BUMN Pembina lainnya, maka PIHAK KEDUA harus memilih salah satu pembinanya. Apabila PIHAK KEDUA memilih BUMN lain selaku pembinanya, maka PIHAK KEDUA wajib mengembalikan secara tunai dan sekaligus uang pinjaman yang telah diterima oleh PIHAK KEDUA dan termasuk jasa pinjaman kepada PIHAK PERTAMA.", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify'));
            $table2->addRow();
            $table2->addCell(500)->addText("(2) ", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table2->addCell(10000)->addText("Apabila dalam waktu 30 (tiga puluh ) hari setelah batas waktu yang telah ditetapkan dalam Perjanjian ini dan/atau berdasarkan batas waktu yang ditentukan PIHAK PERTAMA kemudian, PIHAK KEDUA tidak dapat mengembalikan pinjaman, maka PIHAK PERTAMA secara serta merta dapat menjual agunan dan/atau harta kekayaan namun tidak terbatas pada apa yang dijaminkan PIHAK KEDUA sampai dengan pinjaman PIHAK KEDUA lunas.", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify'));
            $section->addTextBreak(1);
            $section->addText("PASAL 7", array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center', 'spaceAfter' => 10));
            $section->addText("HAL-HAL YANG DIWAJIBKAN", array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center', 'spaceAfter' => 10));
            $section->addTextBreak(1);
            $section->addText("PIHAK KEDUA berkewajiban untuk menjalankan/melaksanakan hal-hal sebagai berikut : ", array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify', 'spaceAfter' => 10));
            $section->addTextBreak(1);
            $table2 = $section->addTable();
            $table2->addRow();
            $table2->addCell(500)->addText("(1) ", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table2->addCell(12000)->addText("Menjalankan usahanya sesuai dengan perijinan yang disyaratkan dan berdasarkan peraturan perundang-undangan yang berlaku serta layak dan efisien.", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify'));
            $table2->addRow();
            $table2->addCell(500)->addText("(2) ", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table2->addCell(11000)->addText("Menunjukkan bukti setor / nota transfer pembayaran cicilan pinjaman setiap bulannya atau paling lama 3 (tiga) bulan sekali atau setiap ada pembinaan di lapangan.", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify'));
            $table2->addRow();
            $table2->addCell(500)->addText("(3) ", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table2->addCell(11000)->addText("Mengikuti Training Kewirausahaan yang diselenggarakan oleh PIHAK PERTAMA.", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify'));
            $table2->addRow();
            $table2->addCell(500)->addText("(4) ", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table2->addCell(11000)->addText("Mengelola, memelihara, dan menjaga keutuhan atas Pinjaman tersebut dengan memperhatikan Maksud dan Tujuan penyampaian Pinjaman.", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify'));
            $table2->addRow();
            $table2->addCell(500)->addText("(5) ", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table2->addCell(11000)->addText("Senantiasa memberikan izin kepada PIHAK PERTAMA atau petugas-petugas yang diberi kuasa oleh PIHAK PERTAMA untuk :", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify'));
            $table4 = $section->addTable();
            $table4->addRow();
            $table4->addCell(500)->addText("", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table4->addCell(500)->addText("a. ", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table4->addCell(11000)->addText("Melakukan pemeriksaan atau audit tidak terbatas terhadap buku-buku, catatan-catatan, dan administrasi PIHAK KEDUA yang terkait dengan Perjanjian ini. ", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify'));
            $table4->addRow();
            $table4->addCell(500)->addText("", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table4->addCell(500)->addText("b. ", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table4->addCell(10000)->addText("Melakukan peninjauan ke dalam proyek, bangunan-bangunan lain, dan kantor-kantor yang digunakan PIHAK KEDUA.", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify'));
            $table2 = $section->addTable();
            $table2->addRow();
            $table2->addCell(500)->addText("(6) ", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table2->addCell(12000)->addText("Melakukan pembukuan mengenai keuangan perusahaan dan membuat catatan-catatan yang mencerminkan keadaan keuangan perusahaan yang sesungguhnya serta hasil pengoperasian perusahaan yang sesuai dengan prinsip-prinsip pembukuan yang diterima secara umum atau sesuai dengan prinsip-prinsip Akuntansi Indonesia yang mencerminkan kewajaran dan dilaksanakan secara konsisten dan/atau sistem laporan keuangan/akuntansi sebagaimana disyaratkan oleh PIHAK PERTAMA.", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify'));
            $table2->addRow();
            $table2->addCell(500)->addText("(7) ", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table2->addCell(10000)->addText("Memberikan segala informasi/keterangan/data-data namun tidak terbatas  pada laporan keuangan PIHAK KEDUA, yaitu :", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify'));
            $table4 = $section->addTable();
            $table4->addRow();
            $table4->addCell(500)->addText("", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table4->addCell(500)->addText("a. ", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table4->addCell(11000)->addText("Sehubungan  dengan keuangan  dan  usaha PIHAK KEDUA terkait dengan Perjanjian ini.", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify'));
            $table4->addRow();
            $table4->addCell(500)->addText("", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table4->addCell(500)->addText("b. ", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table4->addCell(11000)->addText("Bilamana terjadi perubahan dalam sifat atau ruang lingkup usaha PIHAK KEDUA bilamana terjadi suatu peristiwa atau keadaan yang dapat mempengaruhi keadaan usaha atau keuangan, setiap waktu,  baik diminta maupun tidak diminta oleh PIHAK PERTAMA.", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify'));
            $table2 = $section->addTable();
            $table2->addRow();
            $table2->addCell(500)->addText("(8) ", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table2->addCell(12000)->addText("Memperoleh, mempertahankan, memperpanjang atau memperbaharui apabila sudah habis jangka waktunya semua izin usaha dan izin-izin lainnya termasuk namun tidak terbatas pada izin mengenai Analisis Mengenai Dampak Lingkungan (AMDAL) yang harus dimiliki  dalam rangka menjalankan usahanya dan menyerahkan fotocopy dari izin-izin tersebut kepada PIHAK PERTAMA dan apabila ternyata di kemudian hari diperlukan surat-surat izin dan persetujuan-persetujuan yang baru, PIHAK KEDUA wajib segera mengurus dan memperolehnya.", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify'));
            $table2->addRow();
            $table2->addCell(500)->addText("(9) ", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table2->addCell(12000)->addText("Membayar pajak-pajak yang ditetapkan peraturan perundang-undangan yang berlaku. ", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify'));
            $table2->addRow();
            $table2->addCell(500)->addText("(10) ", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table2->addCell(12000)->addText("Mematuhi seluruh hukum dan peraturan perundang-undangan yang berlaku terhadap PIHAK KEDUA, kegiatan usahanya dan harta kekayaannya.", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify'));
            $table2->addRow();
            $table2->addCell(500)->addText("(11) ", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table2->addCell(12000)->addText("Membuat dan menyerahkan kepada PIHAK PERTAMA :", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify'));
            $table2->addRow();
            $table2->addCell(500)->addText("", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table2->addCell(12000)->addText("Laporan Keuangan Triwulanan, selambat-lambatnya 90 (sembilan puluh) hari sejak tanggal laporan, yang ditandatangani oleh PIHAK KEDUA.", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify'));
            $section->addTextBreak(1);
            $section->addText("PASAL 8", array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center', 'spaceAfter' => 10));
            $section->addText("HAL-HAL YANG DILARANG", array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center', 'spaceAfter' => 10));
            $section->addTextBreak(1);
            $section->addText("PIHAK KEDUA dilarang untuk menjalankan/melaksanakan hal-hal sebagai berikut :", array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify', 'spaceAfter' => 10));
            $section->addTextBreak(1);
            $table2 = $section->addTable();
            $table2->addRow();
            $table2->addCell(500)->addText("(1) ", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table2->addCell(12000)->addText("Tidak dibenarkan melakukan pemindah tanganan atas Pinjaman yang diperoleh PIHAK KEDUA dari PIHAK PERTAMA berdasarkan Perjanjian ini.", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify'));
            $table2->addRow();
            $table2->addCell(500)->addText("(2) ", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table2->addCell(12000)->addText("Selama Perjanjian ini berlangsung, peralatan / investasi yang dibeli PIHAK KEDUA dari dana pinjaman ini tidak dapat dijual, dialihkan atau dipindah tangankan baik, sebagian atau seluruhnya kepada pihak lain tanpa izin tertulis terlebih dahulu  PIHAK PERTAMA.", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify'));
            $table2->addRow();
            $table2->addCell(500)->addText("(3) ", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table2->addCell(12000)->addText("Menjual atau menjaminkan seluruh atau sebagian harta kekayaan dan/atau jaminan yang telah diagunkan kepada PIHAK PERTAMA tanpa sepengetahuan dan persetujuan secara tertulis terlebih dahulu dari PIHAK PERTAMA.", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify'));
            $table2->addRow();
            $table2->addCell(500)->addText("(4) ", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table2->addCell(12000)->addText("Membuat keputusan perjanjian yang dapat menimbulkan resiko terhadap pelaksanaan kewajiban PIHAK KEDUA kepada PIHAK PERTAMA sebagaimana diatur dalam Perjanjian ini.", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify'));
            $table2->addRow();
            $table2->addCell(500)->addText("(5) ", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table2->addCell(12000)->addText("Menjamin langsung maupun tidak langsung pihak ketiga lainnya, kecuali melakukan endorsemen atau pengalihan utang atas surat-surat yang dapat diperdagangkan untuk keperluan pembayaran atau penagihan transaksi-transaksi lain yang lazim dilakukan dalam menjalankan usaha.", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify'));
            $table2->addRow();
            $table2->addCell(500)->addText("(6) ", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table2->addCell(12000)->addText("Memberikan dan/atau menerima pinjaman dari pihak ketiga.", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify'));
            $table2->addRow();
            $table2->addCell(500)->addText("(7) ", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table2->addCell(12000)->addText("Mengadakan perubahan sifat dan kegiatan usaha PIHAK KEDUA sebagaimana pada awal dijalankan tanpa pemberitahuan dan persetujuan terlebih dahulu secara tertulis dari PIHAK PERTAMA.", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify'));
            $table2->addRow();
            $table2->addCell(500)->addText("(8) ", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table2->addCell(12000)->addText("Mengubah susunan kepengurusan tanpa pemberitahuan dan persetujuan terlebih dahulu secara tertulis dari PIHAK PERTAMA.", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify'));
            $table2->addRow();
            $table2->addCell(500)->addText("(9) ", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table2->addCell(12000)->addText("Melakukan penggabungan dan/atau pembubaran perusahaan tanpa pemberitahuan dan persetujuan terlebih dahulu secara tertulis dari PIHAK PERTAMA.", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify'));
            $section->addTextBreak(1);
            $section->addText("PASAL 9", array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center', 'spaceAfter' => 10));
            $section->addText("BEBAN BIAYA PIHAK KEDUA", array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center', 'spaceAfter' => 10));
            $section->addTextBreak(1);
            $table2 = $section->addTable();
            $table2->addRow();
            $table2->addCell(500)->addText("(1) ", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table2->addCell(12000)->addText("Segala biaya yang timbul sehubungan dengan pelaksanaan Perjanjian ini namun tidak terbatas pada biaya bea meterai menjadi tanggung jawab PIHAK KEDUA.", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify'));
            $table2->addRow();
            $table2->addCell(500)->addText("(2) ", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table2->addCell(12000)->addText("Apabila terjadi perubahan pada peraturan perundang-undangan yang berlaku dan/atau hal-hal lain namun tidak terbatas pada Perjanjian ini yang mengakibatkan kenaikan biaya, seperti namun tidak terbatas pada bea meterai pada PIHAK PERTAMA, maka  biaya tersebut akan ditanggung oleh PIHAK KEDUA.", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify'));
            $section->addTextBreak(1);
            $section->addText("PASAL 10", array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center', 'spaceAfter' => 10));
            $section->addText("FORCE MAJEURE", array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center', 'spaceAfter' => 10));
            $section->addTextBreak(1);
            $table2 = $section->addTable();
            $table2->addRow();
            $table2->addCell(500)->addText("(1) ", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table2->addCell(12000)->addText("Kedua Pihak tidak bertanggung jawab pada Pihak lainnya bagi kegagalan tersebut dalam Perjanjian ini apabila kegagalan tersebut diakibatkan oleh Force Majeure (keadaan kahar) atau darurat. Force Majeure adalah  kejadian atau keadaan-keadaan di luar kemampuan pengawasan suatu pihak, yang termasuk tapi tidak terbatas pada kecelakaan, kebakaran, banjir, kerusuhan, perang, badai, sabotase, ledakan, pemogokan, gangguan perburuhan, undang-undang pemerintah, ketentuan-ketentuan, kaidah, peraturan keputusan baik sah secara hukum maupun tidak, kerusakan luar biasa pada peralatan atau apparatus, ketidakmampuan memperoleh energy secara keseluruhan (secara terpisah maupun kolektif disebut ''Force Majeure''). ", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify'));
            $table2->addRow();
            $table2->addCell(500)->addText("(2) ", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table2->addCell(12000)->addText("Segera setelah tanggal terjadinya keadaan Force Majeure atau maksimal 3 x 24 jam setelah terjadinya Force Majeure, Pihak yang bermaksud mengajukan kejadian Force Majeure tersebut sebagai sebab yang dapat diterima bagi penundaan pelaksanaan setiap kewajiban dalam Perjanjian ini agar wajib  menyampaikan secara tertulis kepada Pihak lainnya yang diketahui oleh pejabat setempat mengenai saat, sifat dan informasi pada Pihak lainnya mengenai akibat-akibat dari kejadian tersebut terhadap pelaksanaan kewajiban-kewajibannya berdasarkan Perjanjian ini dan jangka waktu yang diperkirakan dari kejadian Force Majeure tersebut.", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify'));
            $table2->addRow();
            $table2->addCell(500)->addText("(3) ", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table2->addCell(12000)->addText("Atas pemberitahuan pihak yang bersangkutan dimaksud, pihak lainnya akan menerima atau menolak secara tertulis  keadaan Force Majeure tersebut paling lambat  dalam waktu 7 x 24 jam.", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify'));
            $table2->addRow();
            $table2->addCell(500)->addText("(4) ", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table2->addCell(12000)->addText("Dalam jangka waktu yang layak setelah saat berakhirnya kejadian Force Majeure, Pihak yang mengajukan keadaan Force Majeure selanjutnya berkonsultasi tentang akibat-akibat dari penundaan tersebut agar melakukan semua upaya yang wajar untuk mencegah, mengurangi dan menekan akibat-akibat dari keadaan Force Majeure seminimal mungkin.", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify'));
            $table2->addRow();
            $table2->addCell(500)->addText("(5) ", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table2->addCell(12000)->addText("Selama keadaan Force Majeure berlangsung, maka Kedua Pihak tidak berkewajiban untuk melaksanakan ketentuan-ketentuan sebagaimana tertuang dalam Perjanjian ini. Pelaksanaan pekerjaan sebagaimana dimaksud perjanjian ini selama keadaan Force Majeure dilakukan berdasarkan hal-hal yang disepakati.", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify'));
            $section->addTextBreak(1);
            $section->addText("PASAL 11", array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center', 'spaceAfter' => 10));
            $section->addText("PENGAKHIRAN PERJANJIAN", array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center', 'spaceAfter' => 10));
            $section->addTextBreak(1);
            $table2 = $section->addTable();
            $table2->addRow();
            $table2->addCell(500)->addText("(1) ", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table2->addCell(12000)->addText("Perjanjian ini berakhir apabila :", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify'));
            $table4 = $section->addTable();
            $table4->addRow();
            $table4->addCell(500)->addText("", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table4->addCell(500)->addText("a. ", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table4->addCell(11000)->addText("PIHAK KEDUA dinyatakan pailit atau likuidasi oleh putusan pengadilan yang berkekuatan hukum tetap dan/atau PIHAK KEDUA oleh suatu sebab dinyatakan tidak berhak mengurus dan menguasai seluruh atau sebagian besar kekayaannya.", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify'));
            $table4->addRow();
            $table4->addCell(500)->addText("", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table4->addCell(500)->addText("b. ", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table4->addCell(11000)->addText("Seluruh atau sebagian besar harta kekayaan PIHAK KEDUA dikenakan sitaan eksekusi oleh Pengadilan sebagai akibat dari putusan pengadilan yang berkekuatan hukum tetap.", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify'));
            $table4->addRow();
            $table4->addCell(500)->addText("", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table4->addCell(500)->addText("c. ", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table4->addCell(11000)->addText("Data-data, namun tidak terbatas pada dokumen-dokumen yang diberikan PIHAK KEDUA kepada PIHAK PERTAMA ternyata di kemudian hari terbukti tidak benar atau tidak sesuai dengan kenyataannya.", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify'));
            $table4->addRow();
            $table4->addCell(500)->addText("", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table4->addCell(500)->addText("d. ", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table4->addCell(11000)->addText("PIHAK KEDUA tercantum dalam daftar hitam lokal yang menyatakan memiliki kredit bermasalah.", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify'));
            $table4->addRow();
            $table4->addCell(500)->addText("", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table4->addCell(500)->addText("e. ", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table4->addCell(11000)->addText("PIHAK KEDUA gagal atau lalai melaksanakan suatu kewajiban atau melakukan pelanggaran berdasarkan Perjanjian ini. ", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify'));
            $table2 = $section->addTable();
            $table2->addRow();
            $table2->addCell(500)->addText("(2) ", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table2->addCell(12000)->addText("Apabila salah satu pihak hendak mengakhiri Perjanjian ini, maka pihak yang bersangkutan wajib memberitahukan secara tertulis kepada pihak lainnya  paling lambat 1 (satu) bulan sebelum tanggal pengakhiran Perjanjian yang diinginkan oleh pihak yang bersangkutan.", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify'));
            $table2->addRow();
            $table2->addCell(500)->addText("(3) ", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table2->addCell(12000)->addText("Apabila Perjanjian ini diakhiri berdasarkan permintaan PIHAK KEDUA, maka PIHAK KEDUA berkewajiban untuk membayar lunas secara sekaligus seluruh pinjaman namun tidak terbatas pada angsuran, paling lambat 14 (empat belas) hari sejak pemberitahuan pengakhiran Perjanjian diberitahukan diterima PIHAK PERTAMA", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify'));
            $table2->addRow();
            $table2->addCell(500)->addText("(4) ", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table2->addCell(12000)->addText("Sehubungan dengan Perjanjian ini Kedua Pihak sepakat untuk tidak memberlakukan Pasal 1266 KUH Perdata (izin pengadilan).", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify'));
            $section->addTextBreak(1);
            $section->addText("PASAL 12", array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center', 'spaceAfter' => 10));
            $section->addText("PENYELESAIAN PERSELISIHAN", array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center', 'spaceAfter' => 10));
            $table2 = $section->addTable();
            $table2->addRow();
            $table2->addCell(500)->addText("(1) ", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table2->addCell(12000)->addText("Apabila terjadi perselisihan sehubungan dengan Perjanjian ini, maka akan diselesaikan dengan cara musyawarah.", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify'));
            $table2->addRow();
            $table2->addCell(500)->addText("(2) ", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table2->addCell(12000)->addText("Apabila dengan jalan musyawarah perselisihan dimaksud tidak dapat diselesaikan, maka Kedua Pihak sepakat menyerahkan penyelesaiannya kepada Pengadilan Negeri Serang Provinsi Banten.", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify'));
            $section->addTextBreak(1);
            $section->addText("PASAL 13", array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center', 'spaceAfter' => 10));
            $section->addText("ADDENDUM", array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center', 'spaceAfter' => 10));
            $table2 = $section->addTable();
            $table2->addRow();
            $table2->addCell(500)->addText("(1) ", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table2->addCell(12000)->addText("Hal-hal yang belum diatur didalam Perjanjian dapat dirundingkan secara musyawarah oleh Kedua Pihak.", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify'));
            $table2->addRow();
            $table2->addCell(500)->addText("(2) ", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table2->addCell(12000)->addText("Hasil Musyawarah yang disetujui oleh Kedua Pihak secara tertulis merupakan ketentuan-ketentuan tambahan dan/atau perubahan yang akan dituangkan di dalam perjanjian tersendiri, dan akan dinamakan ''ADDENDUM'' yang merupakan bagian yang mengikat dan tidak terpisahkan dari Perjanjian.", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify'));
            $section->addTextBreak(1);
            $section->addText("PASAL 14", array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center', 'spaceAfter' => 10));
            $section->addText("PENUTUP", array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center', 'spaceAfter' => 10));
            $table2 = $section->addTable();
            $table2->addRow();
            $table2->addCell(500)->addText("(1) ", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table2->addCell(12000)->addText("Perjanjian ini tidak akan berakhir dengan meninggalnya PIHAK KEDUA, akan tetapi turun-temurun dan harus dipenuhi oleh (para) ahli waris dari PIHAK KEDUA.", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify'));
            $table2->addRow();
            $table2->addCell(500)->addText("(2) ", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table2->addCell(12000)->addText("Perjanjian dibuat rangkap 2 (dua) diberi meterai secukupnya dan mempunyai kekuatan hukum yang sama untuk Kedua Pihak.", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify'));
            $table5 = $section->addTable();
            $table5->addRow();
            // Add cells
            $table5->addCell(6000)->addText("PIHAK KEDUA", array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center'));
            $table5->addCell(6000)->addText("PIHAK PERTAMA", array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center'));
            $table5->addRow();
            // Add cells
            $table5->addCell(6000)->addText(strtoupper($kontrak_mitra['nama_perusahaan']), array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center'));
            $table5->addCell(6000)->addText($divisi, array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center'));
            $table5->addRow(1500);
            // Add cells
            $table5->addCell(6000)->addText("", array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center'));
            $table5->addCell(6000)->addText("PT Krakatau Steel (Persero) Tbk.", array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center'));
            $table5->addRow();
            // Add cells
            $table5->addCell()->addText($kontrak_mitra['nama'], array('underline' => 'single','bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center'));
            $table5->addCell()->addText($managerName , array('underline' => 'single','bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center'));
            $table5->addRow(1000);
            // Add cells
            $table5->addCell()->addText("Pimpinan", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center'));
            $table5->addCell()->addText("Manager", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center'));
            $table5->addRow();
            // Add cells
            $table5->addCell()->addText("Mengetahui", array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center'));
            $table5->addCell()->addText("", array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center'));
            $table5->addRow(1500);
            // Add cells
            $table5->addCell()->addText("AHLI WARIS PIHAK KEDUA", array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center'));
            $table5->addCell()->addText("", array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center'));
            $table5->addRow();
            // Add cells
            $table5->addCell()->addText($kontrak_mitra['nama_ibu'], array('underline' => 'single','bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center'));
            $table5->addCell()->addText("", array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center'));
            
            $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
            $objWriter->save($path.$filename);
            // send results to browser to download
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename='.$filename);
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            header('Content-Length: ' . filesize($path.$filename));
            flush();
            readfile($path.$filename);
            unlink($path.$filename); // deletes the temporary file
            exit;
    
        }
        
        function preview_bast_jaminan(){    // load dompdf
    
            //load content html
            $param = $this->uri->segment(4);
            $data['kontrak_mitra'] = $this->{$this->models[0]}->kontrak_mitra($param)->row_array();
            $html = $this->load->view('bast_jaminan_print',$data, true);

            $this->load->library('dompdf_gen'); 
            $this->dompdf->set_paper('A4', 'portrait'); 
            
            $this->dompdf->load_html($html);
            $this->dompdf->render();
            $this->dompdf->stream("BAST Jaminan.pdf");
        }
        
        Private function no_kontrak(){

            $data = $this->{$this->models[0]}->no_kontrak()->row_array();
            $no_kontrak = $data['no_kontrak'];
            $temp = '0000';
   
                $thn = date('Y');
                $sec = substr($no_kontrak, 4,8)+1;
                // echo $sec;
                $long_temp = strlen($temp);
                $long_sec  = strlen($sec);

                return $thn.substr($temp, 0,$long_temp-$long_sec).$sec;         
            
        }
        // sumiyati 09 sep 17
        function surat_jaminan(){    // load dompdf
    
            //load content html
            $param = $this->uri->segment(4);
            $data['kontrak_mitra'] = $this->{$this->models[0]}->kontrak_mitra($param)->row_array();
            $html = $this->load->view('surat_jaminan',$data, true);

            $this->load->library('dompdf_gen'); 
            $this->dompdf->set_paper('A4', 'portrait'); 
            
            $this->dompdf->load_html($html);
            $this->dompdf->render();
            $this->dompdf->stream("Surat Perjanjian Kuasa Jual.pdf");
        }
        

    }
