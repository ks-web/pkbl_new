<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');
	class Pencairan_dana_akun extends MY_Controller {
		public $models = array('pencairan_dana_akun');
		
		public function __construct() {
			parent::__construct();
		}

		public function index() {
			$data = array();
			$data['menu'] = $this->model_menu->getAllMenu();

			$this->template->load('template', 'view_pencairan_dana_akun', $data);
		}

		public function read() {
			$this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read());
		}

		public function read_all() {
			$this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read_all());
		}

		public function create() {
			// additional block
			$this->data_add['id_cd_um']=$this->uri->segment(4);
			$this->data_add['status']='KM';
			$this->data_add['transaksi']='CD';
			// addtional get
			$result = $this->{$this->models[0]}->insert($this->data_add);
			if ($result == 1) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => $this->db->_error_message())));
			}
		}

		public function update() {
			// additional block

			// addtional data

			// additional where
			$this->where_add['id_akun'] = $this->uri->segment(4);

			$result = $this->{$this->models[0]}->update($this->where_add, $this->data_add);
			if ($result == 1) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'Data Gagal Di Update !')));
			}
		}

		public function delete() {
			$param = $this->uri->segment(4); 
			// additional block

			// additional where
			$this->where_add['id_akun'] = $param;

			$result = $this->{$this->models[0]}->delete($this->where_add);
			
			if ($result == 1) {
				
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'Data Gagal Di Hapus !')));
			}
		}

		public function posting()
		{
			$param = $this->uri->segment(4); 
			$filter = array('A.id_cd_um' => $param, 'A.status' => 'KM', 'A.transaksi' => 'CD');
			$balance = $this->{$this->models[0]}->getStatusBalance($filter);
			//print_r($balance);
			//echo $balance['status'];
			if($balance['status'] != 0){
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => $balance['msg'])));
			} else {
				$filter = array('A.id_cd' => $param);
				$data = array('A.status_posted' => 1, 'A.tgl_posting' => date('Y-m-d'));
				$this->output->set_content_type('application/json')->set_output(json_encode($this->{$this->models[0]}->setStatusPosting($filter, $data)));
				//continue to trigger update on table tcd_all
			}
		}

		public function getStatusPosting()
		{
			$param = $this->uri->segment(4); 
			$filter = array('A.id_cd' => $param);
			$this->output->set_content_type('application/json')->set_output(json_encode($this->{$this->models[0]}->getStatusPosting($filter)));
		}

		public function getAccount()
		{
			$param = $this->input->post('q');
			$filter = array('A.tipe' => 'PK');
			$like = array('A.kode_account' => $param, 'A.uraian' => $param);
			$this->output->set_content_type('application/json')->set_output(json_encode($this->{$this->models[0]}->getAccount($filter, $like)));
		}
	}
