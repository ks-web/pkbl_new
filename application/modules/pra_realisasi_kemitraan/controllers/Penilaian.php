<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');
	class Penilaian extends MY_Controller {
		public $models = array('penilaian');

		public function __construct() {
			parent::__construct();
		}

		public function index() {
			$data = array();
			$data['menu'] = $this->model_menu->getAllMenu();

			$this->template->load('template', 'view_penilaian', $data);
		}

		public function read() {
			$this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read());
		}

		public function read_all() {
			$this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read_all());
		}

		public function create() {
			$param = $this->session->userdata('id');
			#parametter key

			#additional block
			$this->data_add['tanggal_create'] = date('Y-m-d H:i:s');
			$this->data_add['creator'] = $param;

			#addtional get
			$result = $this->{$this->models[0]}->insert($this->data_add);
			if ($result == 1) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
				//echo $this->db->last_query();
				$filter = array('id_mitra' => $this->input->post('id_mitra'));
				$data = array('status' => '3');
				$this->{$this->models[0]}->updateStatusMitra($filter, $data);
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => $this->db->_error_message())));
			}
		}

		public function update() {
			$param = $this->uri->segment(4);
			#parameter key

			#additional block

			#addtional data

			#additional where
			$this->where_add['id_mitra'] = $param;

			$result = $this->{$this->models[0]}->update($this->where_add, $this->data_add);
			if ($result == 1) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'Data Gagal Di Update !')));
			}
		}

		public function delete() {
			$param = $this->uri->segment(4);
			#parameter key

			#additional block

			#additional where
			$this->where_add['id_mitra'] = $param;

			$result = $this->{$this->models[0]}->delete($this->where_add);
			if ($result == 1) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'Data Gagal Di Hapus !')));
			}
		}

		public function getDataMitra() {
			// $id_petugas_survey = $this->session->userdata('idPegawai');
			// $filter = array(
				// 'A.status' => '3',
				// 'B.petugas_survey' => $id_petugas_survey
			// );
			$this->output->set_content_type('application/json')->set_output(json_encode($this->{$this->models[0]}->getDataMitra()));
		}

		public function getPropinsi() {
			$filter = array('A.id !=' => 0);
			$this->output->set_content_type('application/json')->set_output(json_encode($this->{$this->models[0]}->getPropinsi($filter)));
		}

		public function getKota($id) {
			$filter = array('A.province_id' => $id);
			$this->output->set_content_type('application/json')->set_output(json_encode($this->{$this->models[0]}->getKota($filter)));
		}

		public function getKecamatan($id) {
			$filter = array('A.regency_id' => $id);
			$this->output->set_content_type('application/json')->set_output(json_encode($this->{$this->models[0]}->getKecamatan($filter)));
		}

		public function getKelurahan($id) {
			$filter = array('A.district_id' => $id);
			$this->output->set_content_type('application/json')->set_output(json_encode($this->{$this->models[0]}->getKelurahan($filter)));
		}

		public function laporan($id = null) {
			$this->db->where('id_mitra', $id);
			$check = $this->db->get('laporan_hasil_survey_vd');
			if ($check->num_rows() > 0) {

				$data['pengusaha'] = $check->row_array();

				#kota tanggal
				$data['kota'] = 'Cilegon';
				$this->load->helper('tanggal_indo');
				$data['tanggal'] = tanggal_display($data['pengusaha']['tanggal_create']);

				$html = $this->load->view('rep_hasil_survey', $data, true);

				$this->load->library('dompdf_gen');
				#Load library
				$this->dompdf->set_paper('A4', 'potrait');
				#Setting Paper
				##Convert to PDF
				$this->dompdf->load_html($html);
				$this->dompdf->render();
				$this->dompdf->stream("LaporanHasilSurvey-" . $id . ".pdf");
			} else {
				show_404();
			}
		}

		public function laporan_lampiran($id = null) {
			$this->db->where('id_mitra', $id);
			$check = $this->db->get('laporan_hasil_survey_vd');
			if ($check->num_rows() > 0) {

				$data['pengusaha'] = $check->row_array();
				$id_mitra = $data['pengusaha']['id_mitra'];

				#asset usaha
				$this->db->where('id_mitra', $id_mitra);
				$query = $this->db->get('k_asset');
				$data['asset_usaha'] = null;
				if ($query->num_rows() > 0) {
					$data['asset_usaha'] = $query->result_array();
				}

				#investasi
				$this->db->where('id_mitra', $id_mitra);
				$query = $this->db->get('k_investasi');
				$data['data_invest'] = null;
				if ($query->num_rows() > 0) {
					$data['data_invest'] = $query->result_array();
				}

				#periode usaha
				$this->db->where('id_mitra', $id_mitra);
				$query = $this->db->get('k_pperputaran');
				$data['periode_usaha'] = null;
				if ($query->num_rows() > 0) {
					$data['periode_usaha'] = $query->result_array();
				}

				#produksi dan penjualan
				$this->db->where('id_mitra', $id_mitra);
				$query = $this->db->get('k_ppenjualan');
				$data['produksi_penjualan'] = null;
				if ($query->num_rows() > 0) {
					$data['produksi_penjualan'] = $query->result_array();
				}

				#biaya bahan baku
				$this->db->where('id_mitra', $id_mitra);
				$query = $this->db->get('k_bbbaku');
				$data['bahan_baku'] = null;
				if ($query->num_rows() > 0) {
					$data['bahan_baku'] = $query->result_array();
				}

				#biaya bahan bantu
				$this->db->where('id_mitra', $id_mitra);
				$query = $this->db->get('k_bbbantu');
				$data['bahan_bantu'] = null;
				if ($query->num_rows() > 0) {
					$data['bahan_bantu'] = $query->result_array();
				}

				#tenaga kerja tetap
				$this->db->where('id_mitra', $id_mitra);
				$query = $this->db->get('k_ttkerja');
				$data['tenaga_kerja_tetap'] = null;
				if ($query->num_rows() > 0) {
					$data['tenaga_kerja_tetap'] = $query->result_array();
				}

				#tenaga kerja tidak tetap
				$this->db->where('id_mitra', $id_mitra);
				$query = $this->db->get('k_kttetap');
				$data['tenaga_kerja_tidak_tetap'] = null;
				if ($query->num_rows() > 0) {
					$data['tenaga_kerja_tidak_tetap'] = $query->result_array();
				}

				#biaya transport
				$this->db->where('id_mitra', $id_mitra);
				$query = $this->db->get('k_btransport');
				$data['biaya_transport'] = null;
				if ($query->num_rows() > 0) {
					$data['biaya_transport'] = $query->result_array();
				}

				#biaya adm dan umum
				$this->db->where('id_mitra', $id_mitra);
				$query = $this->db->get('k_badmum');
				$data['biaya_adm_umum'] = null;
				if ($query->num_rows() > 0) {
					$data['biaya_adm_umum'] = $query->result_array();
				}

				#biaya lain lain
				$this->db->where('id_mitra', $id_mitra);
				$query = $this->db->get('k_blain');
				$data['biaya_lain2'] = null;
				if ($query->num_rows() > 0) {
					$data['biaya_lain2'] = $query->result_array();
				}

				#activa
				$this->db->select('k_paktiva.id_paktiva, k_paktiva.id_mitra, k_paktiva.total, mas_aktiva.id_aktiva, mas_aktiva.jenis_aktiva');
				$this->db->where('id_mitra', $id_mitra);
				$this->db->from('k_paktiva');
				$this->db->join('mas_aktiva', 'mas_aktiva.id_aktiva = k_paktiva.jenis_aktiva');
				$query = $this->db->get();
				$data['aktiva'] = null;
				if ($query->num_rows() > 0) {
					$data['aktiva'] = $query->result_array();
				}

				#pasiva
				$this->db->select('k_ppasiva.id_ppasiva, k_ppasiva.id_mitra, k_ppasiva.total, mas_pasiva.id_pasiva, mas_pasiva.jenis_pasiva');
				$this->db->where('id_mitra', $id_mitra);
				$this->db->from('k_ppasiva');
				$this->db->join('mas_pasiva', 'mas_pasiva.id_pasiva = k_ppasiva.jenis_aktiva');
				$query = $this->db->get();
				$data['pasiva'] = null;
				if ($query->num_rows() > 0) {
					$data['pasiva'] = $query->result_array();
				}

				#kota tanggal
				$data['kota'] = 'Cilegon';
				$this->load->helper('tanggal_indo');
				$data['tanggal'] = tanggal_display();

				$html = $this->load->view('rep_hasil_survey_lampiran', $data, true);

				$this->load->library('dompdf_gen');
				#Load library
				$this->dompdf->set_paper('A4', 'potrait');
				#Setting Paper
				// // Convert to PDF
				$this->dompdf->load_html($html);
				$this->dompdf->render();
				$this->dompdf->stream("LaporanHasilSurvey-" . $id_mitra . ".pdf");
			} else {
				show_404();
			}
		}

		public function validasi() {
            $id_mitra = $this->uri->segment(4);
            $data = $this->{$this->models[0]}->filter($id_mitra);
            $asset = $data[0]['filter_asset'];
            $omset = $data[0]['filter_omset'];
            
            if($asset == '0' and $omset == '0')
            {
                $where_item = array('id_mitra' => $id_mitra);
                $res_item = json_decode($this->{$this->models[0]}->getOtherDataFecth('tpenilaian_survey_pengusaha', $where_item), TRUE);

                // collecting data
                foreach ($res_item as $items) {
                    $data_quot[] = array(
                        'id_pengusaha' => $items['id_pengusaha'],
                        'id_mitra' => $items['id_mitra'],
                        'pekerjaan' => $items['pekerjaan'],
                        'tanggungan' => $items['tanggungan'],
                        'biaya_hidup' => $items['biaya_hidup'],
                        'pendapatan_luar' => $items['pendapatan_luar'],
                        'jaminan' => $items['jaminan'],
                        'status_jaminan' => $items['status_jaminan'],
                        'detail_usaha' => $items['detail_usaha'],
                        'alamat' => $items['alamat'],
                        'kelurahan' => $items['kelurahan'],
                        'kecamatan' => $items['kecamatan'],
                        'kota' => $items['kota'],
                        'propinsi' => $items['propinsi'],
                        'status_tempat' => $items['status_tempat'],
                        'mulai_usaha' => $items['mulai_usaha'],
                        'mampu_bayar' => $items['mampu_bayar'],
                        'tenagakerja' => $items['tenagakerja'],
                        'tanggal_create' => $items['tanggal_create'],
                        'creator' => $items['creator'],
                        'tanggal_modified' => $items['tanggal_modified'],
                        'modifier' => $items['modifier'],

                    );
                    # code...
                }
                // investasi
                $where_investasi = array('id_mitra' => $id_mitra);
                $rest_inves = json_decode($this->{$this->models[0]}->getOtherDataFecth('k_investasi', $where_investasi), TRUE);
                //collect data invest
                foreach ($rest_inves as $inves) {
                    $data_inves[] = array(
                        'id_investasi'=> $inves['id_investasi'],
                        'id_mitra'=> $id_mitra,
                        'kebutuhan_investasi'=> $inves['kebutuhan_investasi'],
                        'jumlah'=> $inves['jumlah'],
                        'satuan'=> $inves['satuan'],
                        'harga_satuan'=> $inves['harga_satuan'],
                        'unit'=> $inves['unit']
                    );
                    # code...
                }
                //end investasi

                // asset
                $where_asset = array('id_mitra' => $id_mitra);
                $res_asset = json_decode($this->{$this->models[0]}->getOtherDataFecth('k_asset', $where_asset), TRUE);
                
                //collect data assset
                
                    foreach ($res_asset as $key) {
                        $data_asset[] = array(
                            'id_asset' => $key['id_asset'],
                            'id_mitra' => $id_mitra,
                            'uraian' => $key['uraian'],
                            'unit' => $key['unit'],
                            'kepemilikan' => $key['kepemilikan'],
                            'nilai_asset' => $key['nilai_asset'],
                            'biaya_penyusutan' => $key['biaya_penyusutan'],
                            'biaya_pemeliharaan' => $key['biaya_pemeliharaan']
                        );
                    }

                $where_k_badmum = array('id_mitra' => $id_mitra);
                $res_k_badmum = json_decode($this->{$this->models[0]}->getOtherDataFecth('k_badmum', $where_k_badmum), TRUE);
                // echo var_dump($res_k_badmum);
                //collect data badmum
                foreach ($res_k_badmum as $keys) {
                    $data_admum[] = array(
                        'id_admum' => $keys['id_admum'],
                        'id_mitra' => $keys['id_mitra'],
                        'jenis_biaya' => $keys['jenis_biaya'],
                        'biaya_perbulan' => $keys['biaya_perbulan'],
                        'biaya_periode' => $keys['biaya_periode']
                    );
                    // echo var_dump($data_admum);
                    # code...
                }
                //collect data k_bbbaku
                $where_k_bbbaku = array('id_mitra' => $id_mitra);
                $res_k_bbbaku = json_decode($this->{$this->models[0]}->getOtherDataFecth('k_bbbaku', $where_k_bbbaku), TRUE);

                foreach ($res_k_bbbaku as $row) {
                    $data_bbbaku[] = array(
                        'id_bbbaku' => $row['id_bbbaku'],
                        'id_mitra' => $row['id_mitra'],
                        'unsur_biaya' => $row['unsur_biaya'],
                        'satuan' => $row['satuan'],
                        'harga_satuan' => $row['harga_satuan'],
                        'junit_akt' => $row['junit_akt'],
                        'junit_proy' => $row['junit_proy'],
                        'bprod_akt' => $row['bprod_akt'],
                        'bprod_proy' => $row['bprod_proy']
                    );
                    # code...
                }

                $where_k_bbbantu = array('id_mitra' => $id_mitra);
                $res_k_bbbantu = json_decode($this->{$this->models[0]}->getOtherDataFecth('k_bbbantu', $where_k_bbbantu), TRUE);
                foreach ($res_k_bbbantu as $bantu) {
                    $data_bbbantu[] = array(
                        'id_bbbantu' => $bantu['id_bbbantu'],
                        'id_mitra' => $bantu['id_mitra'],
                        'unsur_biaya' => $bantu['unsur_biaya'],
                        'satuan' => $bantu['satuan'],
                        'harga_satuan' => $bantu['harga_satuan'],
                        'junit_akt' => $bantu['junit_akt'],
                        'junit_proy' => $bantu['junit_proy'],
                        'bprod_akt' => $bantu['bprod_akt'],
                        'bprod_proy' => $bantu['bprod_proy']
                    );
                    # code...
                }
                // collect data blain
                $where_k_blain = array('id_mitra' => $id_mitra);
                $res_k_blain = json_decode($this->{$this->models[0]}->getOtherDataFecth('k_blain', $where_k_blain), TRUE);
                foreach ($res_k_blain as $blain) {
                    $data_k_blain[] = array(
                        'id_blain' => $blain['id_blain'],
                        'id_mitra' => $blain['id_mitra'],
                        'unsur_biaya' => $blain['unsur_biaya'],
                        'satuan' => $blain['satuan'],
                        'harga_satuan' => $blain['harga_satuan'],
                        'junit_akt' => $blain['junit_akt'],
                        'junit_proy' => $blain['junit_proy'],
                        'biaya_akt' => $blain['biaya_akt'],
                        'biaya_proy' => $blain['biaya_proy']
                    );
                    # code...
                }

                // collect data kblain
                $where_k_btransport = array('id_mitra' => $id_mitra);
                $res_k_btransport = json_decode($this->{$this->models[0]}->getOtherDataFecth('k_btransport', $where_k_btransport), TRUE);
                foreach ($res_k_btransport as $trans) {
                    $data_k_btransport[] = array(
                        'id_btransport' => $trans['id_btransport'],
                        'id_mitra' => $trans['id_mitra'],
                        'uraian' => $trans['uraian'],
                        'satuan' => $trans['satuan'],
                        'harga_satuan' => $trans['harga_satuan'],
                        'unit_akt' => $trans['unit_akt'],
                        'unit_proy' => $trans['unit_proy'],
                        'produksi_akt' => $trans['produksi_akt'],
                        'produksi_proy' => $trans['produksi_proy']
                    );
                    # code...
                }


                // colect data k_paktiva
                $where_k_paktiva = array('id_mitra' => $id_mitra);
                $res_k_paktiva = json_decode($this->{$this->models[0]}->getOtherDataFecth('k_paktiva', $where_k_paktiva), TRUE);
                foreach ($res_k_paktiva as $aktiva) {
                    $data_k_paktiva[] = array(
                        'id_paktiva' => $aktiva['id_paktiva'],
                        'id_mitra' => $aktiva['id_mitra'],
                        'jenis_aktiva' => $aktiva['jenis_aktiva'],
                        'jumlah' => $aktiva['jumlah'],
                        'satuan' => $aktiva['satuan'],
                        'harga_satuan' => $aktiva['harga_satuan'],
                        'total' => $aktiva['total']
                    );
                    # code...
                }
                // collect data kttetap
                $where_k_kttetap = array('id_mitra' => $id_mitra);
                $res_k_kttetap = json_decode($this->{$this->models[0]}->getOtherDataFecth('k_kttetap', $where_k_kttetap), TRUE);
                foreach ($res_k_kttetap as $tetap) {
                    $data_k_kttetap[] = array(
                        'id_kttetap' => $tetap['id_kttetap'],
                        'id_mitra' => $tetap['id_mitra'],
                        'uraian' => $tetap['uraian'],
                        'satuan' => $tetap['satuan'],
                        'harga_satuan' => $tetap['harga_satuan'],
                        'produksi_akt' => $tetap['produksi_akt'],
                        'produksi_proy' => $tetap['produksi_proy'],
                        'penjualan_akt' => $tetap['penjualan_akt'],
                        'penjualan_proy' => $tetap['penjualan_proy']
                    );
                    # code...
                }
                //collect data k_pasiva
                $where_k_ppasiva = array('id_mitra' => $id_mitra);
                $res_k_ppasiva = json_decode($this->{$this->models[0]}->getOtherDataFecth('k_ppasiva', $where_k_ppasiva), TRUE);
                // if (sizeof($res_k_ppasiva)>0) {
                # code...

                foreach ($res_k_ppasiva as $pasiva) {
                    $data_k_ppasiva[] = array(
                        'id_ppasiva' => $pasiva['id_ppasiva'],
                        'id_mitra' => $pasiva['id_mitra'],
                        'jenis_aktiva' => $pasiva['jenis_aktiva'],
                        'jumlah' => $pasiva['jumlah'],
                        'satuan' => $pasiva['satuan'],
                        'harga_satuan' => $pasiva['harga_satuan'],
                        'total' => $pasiva['total']
                    );
                    # code...
                }
                // }
                // collect data kpenjualan
                $where_k_ppenjualan = array('id_mitra' => $id_mitra);
                $res_k_ppenjualan = json_decode($this->{$this->models[0]}->getOtherDataFecth('k_ppenjualan', $where_k_ppenjualan), TRUE);
                foreach ($res_k_ppenjualan as $penjualan) {
                    $data_k_ppenjualan[] = array(
                        'id_ppenjualan' => $penjualan['id_ppenjualan'],
                        'id_mitra' => $penjualan['id_mitra'],
                        'jenis_produksi' => $penjualan['jenis_produksi'],
                        'satuan' => $penjualan['satuan'],
                        'harga_satuan' => $penjualan['harga_satuan'],
                        'produksi_akt' => $penjualan['produksi_akt'],
                        'produksi_proy' => $penjualan['produksi_proy'],
                        'penjualan_akt' => $penjualan['penjualan_akt'],
                        'penjualan_proy' => $penjualan['penjualan_proy']
                    );
                    # code...
                }
                // collect data k_pperputaran
                $where_k_pperputaran = array('id_mitra' => $id_mitra);
                $res_k_pperputaran = json_decode($this->{$this->models[0]}->getOtherDataFecth('k_pperputaran', $where_k_pperputaran), TRUE);
                foreach ($res_k_pperputaran as $perputaran) {
                    $data_k_pperputaran[] = array(
                        'id_pperputaran' => $perputaran['id_pperputaran'],
                        'id_mitra' => $perputaran['id_mitra'],
                        'uraian' => $perputaran['uraian'],
                        'jumlah_hari' => $perputaran['jumlah_hari']
                    );
                    # code...
                }
                # collect data k_rekomendasi

                #collect data k_ttkerja
                $where_k_ttkerja = array('id_mitra' => $id_mitra);
                $res_k_ttkerja = json_decode($this->{$this->models[0]}->getOtherDataFecth('k_ttkerja', $where_k_ttkerja), TRUE);
                foreach ($res_k_ttkerja as $tkerja) {
                    $data_k_ttkerja[] = array(
                        'id_ttkerja' => $tkerja['id_ttkerja'],
                        'id_mitra' => $tkerja['id_mitra'],
                        'bagian' => $tkerja['bagian'],
                        'unit' => $tkerja['unit'],
                        'upah_perbulan' => $tkerja['upah_perbulan'],
                        'jumlah_upah' => $tkerja['jumlah_upah'],
                        'upah_periode'=>$tkerja['upah_periode']
                    );
                    # code...
                }
                //tk_rekomendasi
                $where_k_rekomendasi = array('id_mitra' => $id_mitra);
                $res_k_rekomendasi = json_decode($this->{$this->models[0]}->getOtherDataFecth('k_rekomendasi', $where_k_rekomendasi), TRUE);
                foreach ($res_k_rekomendasi as $rek) {
                    $data_k_rekomendasi[] = array(
                        'id_rekomendasi' => $rek['id_rekomendasi'],
                        'id_mitra' => $rek['id_mitra'],
                        'pengelolaan' => $rek['pengelolaan'],
                        'teknis' => $rek['teknis'],
                        'pemasaran' => $rek['pemasaran'],
                        'keuangan' => $rek['keuangan'],
                        'kekuatan_mitra' => $rek['kekuatan_mitra'],
                        'kelemahan_mitra' => $rek['kelemahan_mitra'],
                        'rekomendasi' => $rek['rekomendasi']
                    );
                    # code...
                }

                $this->db->select('count(id_mitra) as jml');
                $this->db->where('id_mitra', $id_mitra);
                $a = $this->db->get('tpenilaian_survey_pengusaha_evaluator')->result_array();
                if ($a[0]['jml'] == 0) {
                    $res_evaluator = $this->db->insert_batch('tpenilaian_survey_pengusaha_evaluator', $data_quot);
                }

                $this->db->select('count(id_mitra) as jumlah_mitra');
                $this->db->where('id_mitra', $id_mitra);
                $aset = $this->db->get('tk_asset')->result_array();
				if($aset[0]['jumlah_mitra']  and $res_asset)
                {
                	
                }
                else if ($aset[0]['jumlah_mitra'] == 0 and $res_asset) {
                    $insert_aset = $this->db->insert_batch('tk_asset', $data_asset);
                }else{
                    // notif error jika data kosong
                        exit(json_encode(array('msg' => 'Data Asset Kosong')));
                    }
                
                
                //insert admum
                $this->db->select('count(id_mitra) as jumlah_mitra');
                $this->db->where('id_mitra', $id_mitra);
                $badmum = $this->db->get('tk_badmum')->result_array();
                if ($badmum[0]['jumlah_mitra'] and $res_k_badmum) {
                	# code...
                }
                else if ($badmum[0]['jumlah_mitra'] == 0 and $res_k_badmum) {
                    $insert_badmum = $this->db->insert_batch('tk_badmum', $data_admum);

                }
                else{
                    // notif error jika data kosong
                     // $this->output->set_content_type('application/json')
                     //    ->set_output();
                        exit(json_encode(array('msg' => 'Data admum Kosong')));
                }
                
                //isert data baku
                $this->db->select('count(id_mitra) as jumlah_mitra');
                $this->db->where('id_mitra', $id_mitra);
                $tk_bbbaku = $this->db->get('tk_bbbaku')->result_array();
                if ($tk_bbbaku[0]['jumlah_mitra']  and $res_k_bbbaku) {
                	# code...
                }
                else if ($tk_bbbaku[0]['jumlah_mitra'] == 0 and $res_k_bbbaku) {
                    $insert_tk_bbbaku = $this->db->insert_batch('tk_bbbaku', $data_bbbaku);
                }
                else{
                    // notif error jika data kosong
                     // $this->output->set_content_type('application/json')
                        // ->set_output();
                        exit(json_encode(array('msg' => 'Data baku Kosong')));
                }
                //insert bbantu
                $this->db->select('count(id_mitra) as jumlah_mitra');
                $this->db->where('id_mitra', $id_mitra);
                $tk_bbbantu = $this->db->get('tk_bbbantu')->result_array();
                if ($tk_bbbantu[0]['jumlah_mitra']  and $res_k_bbbantu) 
                		{}
                else if($tk_bbbantu[0]['jumlah_mitra'] == 0 and $res_k_bbbantu)
                		{
                			$insert_tk_bbbantu = $this->db->insert_batch('tk_bbbantu', $data_bbbantu);
                		 }
                	else{
                    // notif error jika data kosong
                     // $this->output->set_content_type('application/json')
                     //    ->set_output();
                        exit(json_encode(array('msg' => 'Data bahan bantu Kosong')));
                }
                
                //insert data tk_blain
                $this->db->select('count(id_mitra) as jml');
                $this->db->where('id_mitra', $id_mitra);
                $tk_blain = $this->db->get('tk_blain')->result_array();
                if ($tk_blain[0]['jml']  and $res_k_blain) {
                	# code...
                }
                elseif ($tk_blain[0]['jml'] == 0 and $res_k_blain) {
                    $insert_tk_balin = $this->db->insert_batch('tk_blain', $data_k_blain);
                }
                else{
                    // notif error jika data kosong
                     // $this->output->set_content_type('application/json')
                     //    ->set_output();
                        exit(json_encode(array('msg' => 'Data Biaya lain Kosong')));
                }
                //insert data tk_btransport
                $this->db->select('count(id_mitra) as jml');
                $this->db->where('id_mitra', $id_mitra);
                $tk_btransport = $this->db->get('tk_btransport')->result_array();
                if ($tk_btransport[0]['jml'] and $res_k_btransport) {
                	# code...
                }
                else if ($tk_btransport[0]['jml'] == 0 and $res_k_btransport) {
                    $insert_tk_btransport = $this->db->insert_batch('tk_btransport', $data_k_btransport);
                }
                else{
                    // notif error jika data kosong
                     // $this->output->set_content_type('application/json')
                     //    ->set_output();
                        exit(json_encode(array('msg' => 'Data transport Kosong')));
                }
                //insert data tk_kttetap
                $this->db->select('count(id_mitra) as jml');
                $this->db->where('id_mitra', $id_mitra);
                $tk_kttetap = $this->db->get('tk_kttetap')->result_array();
                if ($tk_kttetap[0]['jml'] and $res_k_kttetap) {
                	# code...
                }
                else if ($tk_kttetap[0]['jml'] == 0 and $res_k_kttetap) {
                    $insert_tk_kttetap = $this->db->insert_batch('tk_kttetap', $data_k_kttetap);
                }
                else{
                    // notif error jika data kosong
                     // $this->output->set_content_type('application/json')
                     //    ->set_output();
                        exit(json_encode(array('msg' => 'Data Tenaga Kerja Tak Tetap Kosong')));
                }
                
                //insert data k_paktiva
                $this->db->select('count(id_mitra) as jml');
                $this->db->where('id_mitra', $id_mitra);
                $tk_paktiva = $this->db->get('tk_paktiva')->result_array();
                if ($tk_paktiva[0]['jml']  and $res_k_paktiva) {
                	# code...
                }
                else if ($tk_paktiva[0]['jml'] == 0 and $res_k_paktiva) {
                    $insert_tk_paktiva = $this->db->insert_batch('tk_paktiva', $data_k_paktiva);
                }
                else{
                    // notif error jika data kosong
                     // $this->output->set_content_type('application/json')
                     //    ->set_output();
                		exit(json_encode(array('msg' => 'Data Aktiva Kosong')));
                }
                //insert data k_ppasiva
                $this->db->select('count(id_mitra) as jml');
                $this->db->where('id_mitra', $id_mitra);
                $tk_ppasiva = $this->db->get('tk_ppasiva')->result_array();
                if ($tk_ppasiva[0]['jml']  and $res_k_ppasiva) {
                	# code...
                }
                else if ($tk_ppasiva[0]['jml'] == 0 and $res_k_ppasiva) {
                    $insert_tk_ppasiva = $this->db->insert_batch('tk_ppasiva', $data_k_ppasiva);
                }
                else{
                    // notif error jika data kosong
                     // $this->output->set_content_type('application/json')
                     //    ->set_output();
                		exit(json_encode(array('msg' => 'Data pasiva Kosong')));
                }
                //insert data k_ppenjualan
                $this->db->select('count(id_mitra) as jml');
                $this->db->where('id_mitra', $id_mitra);
                $tk_ppenjualan = $this->db->get('tk_ppenjualan')->result_array();
                if ($tk_ppenjualan[0]['jml']  and $res_k_ppenjualan) {
                	# code...
                }
                else if ($tk_ppenjualan[0]['jml'] == 0 and $res_k_ppenjualan) 
                {
                    $insert_tk_ppenjualan = $this->db->insert_batch('tk_ppenjualan', $data_k_ppenjualan);
                }
                else{
                    // notif error jika data kosong
                     // $this->output->set_content_type('application/json')
                     //    ->set_output();
                        	exit(json_encode(array('msg' => 'Data Penjualan Kosong')));
                }
                //insert data k_pperputaran
                $this->db->select('count(id_mitra) as jml');
                $this->db->where('id_mitra', $id_mitra);
                $tk_pperputaran = $this->db->get('tk_pperputaran')->result_array();
                if ($tk_pperputaran[0]['jml'] and $res_k_pperputaran) {
                	# code...
                }
                else if ($tk_pperputaran[0]['jml'] == 0 and $res_k_pperputaran) {
                    $insert_tk_pperputaran = $this->db->insert_batch('tk_pperputaran', $data_k_pperputaran);
                }
                else{
                    // notif error jika data kosong
                     // $this->output->set_content_type('application/json')
                     //    ->set_output();
                        exit(json_encode(array('msg' => 'Data Perputaran Kosong')));
                }
                //insert data k_rekomendasi
                $this->db->select('count(id_mitra) as jml');
                $this->db->where('id_mitra', $id_mitra);
                $tk_rekomendasi = $this->db->get('tk_rekomendasi')->result_array();
                if ($tk_rekomendasi[0]['jml'] and $res_k_rekomendasi) {
                	# code...
                }
                else if ($tk_rekomendasi[0]['jml'] == 0 and $res_k_rekomendasi) {
                    $insert_tk_rekomendasi = $this->db->insert_batch('tk_rekomendasi', $data_k_rekomendasi);
                }
                else{
                    // notif error jika data kosong
                     // $this->output->set_content_type('application/json')
                     //    ->set_output();
                        exit(json_encode(array('msg' => 'Data rekomendasi Kosong')));
                }
                //insert data k_ttkerja
                $this->db->select('count(id_mitra) as jml');
                $this->db->where('id_mitra', $id_mitra);
                $tk_ttkerja = $this->db->get('tk_ttkerja')->result_array();
                if ($tk_ttkerja[0]['jml'] and $res_k_ttkerja) {
                	# code...
                }
                else if ($tk_ttkerja[0]['jml'] == 0 and $res_k_ttkerja) {
                    $insert_tk_ttkerja = $this->db->insert_batch('tk_ttkerja', $data_k_ttkerja);
                }
                else{
                    // notif error jika data kosong
                     // $this->output->set_content_type('application/json')
                     //    ->set_output();
                       exit(json_encode(array('msg' => 'Data Tenaga Kerja Kosong')));
                }

                //insert data tk_investasi
                $this->db->select('count(id_mitra) as jml');
                $this->db->where('id_mitra', $id_mitra);
                $tk_investasi = $this->db->get('tk_investasi')->result_array();
                if ($tk_investasi[0]['jml'] and $rest_inves) {
                	# code...
                }
                else if ($tk_investasi[0]['jml'] == 0 and $rest_inves) {
                    $insert_inves = $this->db->insert_batch('tk_investasi', $data_inves);
                }

                //end insert investasi

                #$this->db->last_query();
                #die();
                $where_mitra = array('id_mitra' => $id_mitra);
                $creator = $this->session->userdata('id');
                $tanggal_create = date("Y-m-d H:i:s");
                $mitra = array(
                    'creator' => $creator,
                    'tanggal_create' => $tanggal_create
                );
                $rest = $this->db->update('tpenilaian_survey_pengusaha', $mitra, $where_mitra);

                $whereidmitra = array('id_mitra' => $id_mitra);
                $aproval = '1';
                $tahun = '1';
                $bunga='3.00';
                $mitra2 = array(
                    'aproval' => $aproval,
                    'tahun' => $tahun,
                    'bunga'=>$bunga
                );
                $rest2 = $this->db->update('tpenilaian_survey_pengusaha_evaluator', $mitra2, $whereidmitra);

                $where_idmitra = array('id_mitra' => $id_mitra);
                $data_mitra = array('status' => '4');
                $result = $this->db->update('tmitra', $data_mitra, $where_idmitra);
                // $this->db->last_query();
                #die();
                if ($rest == 1) {
                    $this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
                } else {
                    $this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => $this->db->_error_message())));
                }
   		   }else{
   		   	$this->output->set_content_type('application/json')
                    ->set_output(json_encode(array('msg' => 'Omset atau Asset Lebih dari Ketentuan')));
   		   }
        }
		public function getjaminan(){
			$filter = array();
			$this->output->set_content_type('application/json')->set_output(json_encode($this->{$this->models[0]}->getjaminan($filter)));
		}
		function upload_lampiran() {
 
            $this->data_add['id_mitra'] =$this->input->post('id_mitra');
            $param=$this->input->post('id_mitra');

			$vendor_dir = 'assets/upload/Survey/' . $param;
			if (!is_dir($vendor_dir)) {
				mkdir($vendor_dir, folderUploadPermission, TRUE);
			}
			$dir = 'assets/upload/Survey/'. $param . '/file_survey';
			if (!is_dir($dir)) {
				mkdir($dir, folderUploadPermission, TRUE);
			}
			$config['upload_path'] = $dir . '/';
			$config['allowed_types'] = implode('|', json_decode(fileUploadTipe5));
			$config['max_size'] = fileUploadSize5;
			$config['encrypt_name'] = true;
			$this->upload->initialize($config);
			if (!$this->upload->do_upload('file_survey')) {
				NULL;
			} else {
				$file = $this->upload->data();
				if ($current_file != '') {
					$this->load->library('common');
					$this->common->delete_file($current_file);
				}

				$this->data_add['lampiran_pernyataan'] = $config['upload_path'] . $file['file_name'];
			}

			$this->where_add['id_mitra'] = $param;
            $result = $this->{$this->models[0]}->update($this->where_add, $this->data_add);
                  
                  if ($result) {
                        //$this->_log_vendor('create', 'Ijin Usaha');
                        $this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
                  } else {
                        $this->output->set_content_type('application/json')->set_output(json_encode(array('msg' =>'Data Gagal Diupdate !')));
                  }
              }
              function get_file($url){
			$this->load->helper('directory');
			$url_ok = str_replace("-","/",$url);
			$map = directory_map($url_ok, FALSE, TRUE);
			// echo var_dump($map);
			
			$this->output->set_content_type('application/json')->set_output(json_encode($map));
			
			
		}
		function upload_lampiran_all($idbl){
            $param=$idbl;
            //$this->where_add['id_bl'] = $param;
            // Upload ktp
			$bl_dir = 'assets/upload/Survey/' . $param.'/file';
			if (!is_dir($bl_dir)) {
				mkdir($bl_dir, folderUploadPermission, TRUE);
			}
			$dir = 'assets/upload/Survey/' . $param . '/file';
			if (!is_dir($dir)) {
				mkdir($dir, folderUploadPermission, TRUE);
			}

			$config['upload_path'] = $dir . '/';
			$config['allowed_types'] = implode('|', json_decode(fileUploadTipe5));
			$config['max_size'] = fileUploadTipe5;
			$config['encrypt_name'] = true;
			$this->upload->initialize($config);

			if (!$this->upload->do_upload('file_survey')) {
				//$this->data_add['lampiran_ktp'] = '';
			} else {
				$file = $this->upload->data();
				//$this->data_add['lampiran_ktp'] = $config['upload_path'] . $file['file_name'];
                $data=array('file_survey'=>$config['upload_path'] . $file['file_name']);
			}
            
            $result = $this->{$this->models[0]}->update_file($param, $data);
			if ($result == 1) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => $result)));
			}
        }

	}

		

