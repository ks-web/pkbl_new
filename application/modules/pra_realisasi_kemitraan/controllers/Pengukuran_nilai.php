<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');
	class Pengukuran_nilai extends MY_Controller {
		public $models = array('pengukuran_nilai');

		public function __construct() {
			parent::__construct();
			$this->load->library('disposisi');
		}

		public function index() 
		{
			$data = array();
			$data['menu'] = $this->model_menu->getAllMenu();

			$this->template->load('template', 'view_' . $this->models[0], $data);
		}

		public function read() 
		{
			$id_peg = $this->session->userdata('idPegawai');
			$allowed = $this->{$this->models[0]}->get_userAllow($id_peg);
			if($id_peg == $allowed){
				$this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read());
			}else{
				$data_where = array(
						'nip_tujuan' => $id_peg
					);
				$this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read($data_where));
			}
			
		}

		public function read_all() 
		{
			$this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read_all());
		}

		public function create() 
		{
			// parametter key

			// additional block
			//$this->data_add['status'] = '1';
			// addtional get
			$result = $this->{$this->models[0]}->insert($this->data_add);
			if ($result == 1) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
				$filter = array('id_mitra' => $this->input->post('id_mitra'));
				$data = array('status' => '6');
				$this->{$this->models[0]}->updateStatusMitra($filter, $data);
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => $this->db->_error_message())));
			}
		}

		public function update() 
		{
			$param = $this->uri->segment(4);
		
			// additional where
			$this->where_add['id_p_survey'] = $param;

			$result = $this->{$this->models[0]}->update($this->where_add, $this->data_add);
			if ($result == 1) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'Data Gagal Di Update !')));
			}
		}

		public function delete() 
		{
			$param = $this->uri->segment(4);
		
			// additional where
			$this->where_add['id_tnilai'] = $param;

			$result = $this->{$this->models[0]}->delete($this->where_add);
			if ($result == 1) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'Data Gagal Di Hapus !')));
			}
		}

		public function get_mitra() 
		{
			// 
			// $q = $this->input->post('q');

			// $ret = $this->{$this->models[0]}->get_mitra($q);

			// if (sizeof($ret) > 0) {
			// $ret[0]['selected'] = true;
			// }

			// echo json_encode($ret);
			// modifikasi sumi 26/03/17
			// untuk menampilkan nama_jaminan
			//
			
			$param = $this->input->post('q');

			$filter 	= array('A.status' => '5');
			$like 		= array('A.id_mitra' => $param, 'A.nama' => $param, 'A.nama_perusahaan' => $param, 'A.nama_jaminan' => $param);
			$this->output->set_content_type('application/json')->set_output(json_encode($this->{$this->models[0]}->getDataMitra($filter, $like)));
		}
		
		
		public function get_sektor_kemitraan() {
			$q = $this->input->post('q');
			$ret = $this->{$this->models[0]}->get_sektor_kemitraan($q);

			echo json_encode($ret);
		}

		public function get_detail_jaminan($id_tnilai = null) {
			if ($id_tnilai) {
				$this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->get_detail_jaminan(array('id_tnilai' => $id_tnilai)));
			} else {
				show_404();
			}
		}
		
		public function get_pegawai()
		{
			$this->output->set_content_type('application/json')->set_output(json_encode($this->{$this->models[0]}->get_pegawai()));		
		}
		
		public function get_pesan()
		{
			$this->output->set_content_type('application/json')->set_output(json_encode($this->disposisi->get_pesan()));		
		}
			
		public function add_disposisi()
		{
			$data = array(
					'id_doc' 			=> $this->input->post('id_doc'),
					'node_menu'			=> '35', //35 adalah node menu pengukuran nilai
					'nip_tujuan'		=> $this->input->post('tujuan_disposisi'),
					'pesan'				=> $this->input->post('pesan_disposisi'),
					'catatan_disposisi'	=> $this->input->post('catatan_disposisi'),
					'creator'			=> $this->session->userdata('idPegawai'),
					'tanggal_disposisi' => date('Y-m-d h:m:s')
					);
					
			$this->{$this->models[0]}->add_disposisi($data);	
		}
		
		public function get_disposisi($id_doc)
		{
			// echo $id_doc;
			$this->output->set_content_type('application/json')
						 ->set_output(json_encode($this->{$this->models[0]}->get_disposisi($id_doc)));
		}
		
		public function disposisi_selesai($id_doc, $nip_tujuan)
		{
			header("Content-Type: application/json");
			date_default_timezone_set('Asia/Jakarta');
			
			$data = array
					(		
						'approve' => '1',
						'approve_date_time' => date("Y-m-d H:i:s")
					);
					
			$this->{$this->models[0]}->disposisi_selesai($id_doc, $nip_tujuan, $data);
		}
		
		public function hapus_disposisi(){
			$id_doc  = $this->uri->segment(4);
			$tujuan  = $this->uri->segment(5);
			$creator  = $this->uri->segment(6);
			$creator_aktiv = $this->session->userdata('idPegawai');

			if($creator_aktiv != $creator){
				$this->output->set_output(json_encode(array('msg'=>'User yang Aktif tidak Bisa Menghapus')));
			}else{

				$where = array(
					'id_doc' 	 => $id_doc,
					'nip_tujuan' => $tujuan,
					'creator'	 => $creator
				);
				$this->db->where($where);
				$data = $this->db->delete('disposisi_transaksi');

				if ($data) {
					$this->output->set_output(json_encode(array('success'=>TRUE)));
				}
				else{
					$this->output->set_output(json_encode(array('msg'=>'Hapus Gagal')));
				}

			}
		}
		
		
		public function view_report_pengukuran($id_tnilai)
		{
		    $data['data'] = $this->{$this->models[0]}->view_report_pengukuran($id_tnilai);
		    $html = $this->load->view('pengukuran_nilai_print',$data, true);

  			$this->load->library('dompdf_gen'); 
			$this->dompdf->set_paper('A4', 'landscape'); 
			
			$this->dompdf->load_html($html);
			$this->dompdf->render();
			$this->dompdf->stream("Laporan Pengukuran Nilai Kemitraan.pdf");
			
		}
		
		public function view_report_usulan($id_mitra)
		{
			$data['data'] = $this->{$this->models[0]}->view_report_usulan($id_mitra);
		    $html = $this->load->view('laporan_usulan_print',$data, true);

  			$this->load->library('dompdf_gen'); 
			$this->dompdf->set_paper('A4', 'landscape'); 
			
			$this->dompdf->load_html($html);
			$this->dompdf->render();
			$this->dompdf->stream("Laporan Usulan Kemitraan.pdf");
			/*
			$data 	= $this->{$this->models[0]}->view_report_usulan($id_mitra);
			echo '
			PT. Krakatau Steel (Persero) Tbk.<br>
			Divisi ComDev.
			<center><h2><strong> Usulan Pinjaman Modal Kepada Usaha Kecil </strong></h2></center>
			<center><h3><strong> No.'.$data->id_mitra.'/USL/PKBL-KS/2016 </strong></h3></center>
			<BR><BR>
			<center>
			<table style="border-collapse:collapse;border-spacing:0">
			<tr>
				<th style="font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;vertical-align:top" ><center><strong>No</th>
				<th style="font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;vertical-align:top" ><center><strong>No. Reg</th>
				<th style="font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;vertical-align:top" ><center><strong>Nama Perusahaan</th>
				<th style="font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;vertical-align:top" ><center><strong>Sektor Usaha</th>
				<th style="font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;vertical-align:top" ><center><strong>Alamat</th>
				<th style="font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;vertical-align:top" ><center><strong>Kota/Kab</th>
				<th style="font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;vertical-align:top" ><center><strong>Asset</th>
				<th style="font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;vertical-align:top" ><center><strong>Omset/Bulan</th>
				<th style="font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;vertical-align:top" ><center><strong>TKJ</th>
				<th style="font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;vertical-align:top" ><center><strong>Pinjaman</th>
				<th style="font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;vertical-align:top" ><center><strong>Tahun</th>
				<th style="font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;vertical-align:top" colspan="2" ><center><strong>Jasa Admin</th>
				<th style="font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;vertical-align:top" ><center><strong>Angsuran</th>				
			</tr>
			
			<tr>
				<th style="font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;vertical-align:top" >1</th>
				<th style="font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;vertical-align:top" >'.$data->id_mitra.'</th>
				<th style="font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;vertical-align:top" >'.$data->nama.'</th>
				<th style="font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;vertical-align:top" >'.$data->sektor_usaha.'</th>
				<th style="font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;vertical-align:top" >'.$data->alamat.'</th>
				<th style="font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;vertical-align:top" >'.$data->kota.'</th>
				<th style="font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;vertical-align:top;text-align:right;" ></th>
				<th style="font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;vertical-align:top;text-align:right;" ></th>
				<th style="font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;vertical-align:top;text-align:right;" >'.$data->tenagakerja.'</th>
				<th style="font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;vertical-align:top;text-align:right;" >'.number_format($data->angsuran_pokok,0,'.',',').'</th>
				<th style="font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;vertical-align:top;text-align:right;" >'.number_format($data->tahun,0,'.',',').'</th>
				<th style="font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;vertical-align:top" >'.number_format($data->bunga,0,'.',',').'</th>
				<th style="font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;vertical-align:top;text-align:right;" >'.number_format($data->total_bunga,0,'.',',').'</th>
				<th style="font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;vertical-align:top;text-align:right;" >'.number_format(($data->angsuran_bulanan) / 24,0,'.',',' ).'</th>
			</tr>
			<tr>
				<th style="font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;vertical-align:top" colspan="8"><strong>Jumlah</th>
				<th style="font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;vertical-align:top; text-align:right" colspan="2"><strong>'.number_format($data->angsuran_pokok,0,'.',',').'</th>
				<th style="font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;vertical-align:top; text-align:right" colspan="3"><strong></th>
				<th style="font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;vertical-align:top; text-align:right" ><strong></th>
			</tr>
			<tr>
				<td><br><br><br></td>
			</tr>
			<tr>
				
				<td colspan="6" style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;vertical-align:top;text-align:right;">
					<center>
					<br>Disetujui Oleh<br>Divisi Community Development<br><br><br><br><font style="text-decoration: underline;">Syarif Rahman</font><br>Manager
					</center>
				</td>
				<td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;vertical-align:top;text-align:right;"></td>
				<td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;vertical-align:top;text-align:right;"></td>
				<td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;vertical-align:top;text-align:right;"></td>
				<td colspan="3" style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;vertical-align:top;text-align:right;">
					<center>
					Cilegon, '. date("d-m-Y")  .'<br>Disiapkan Oleh<br>Divisi Community Development<br><br><br><br><font style="text-decoration: underline;">Jamal Abdul Nasir</font><br>Superintendent
					</center>
				</td>
			</tr>
			
			</table>
			';
			
			*/
		}
		
		
	}
