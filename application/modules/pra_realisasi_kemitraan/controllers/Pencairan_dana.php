<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');
	class Pencairan_dana extends MY_Controller {
		public $models = array('pencairan_dana');

		public function __construct() {
			parent::__construct();
		}

		public function index() {
			$data = array();
			$data['menu'] = $this->model_menu->getAllMenu();

			//print_r($this->session->userdata());

			$filter = array('A.idUser' => $this->session->userdata('id'));
			//$filter = array('A.idUser' => 1); 
			//print_r($filter);
			$data['access'] = $this->{$this->models[0]}->getAccess($filter);
			if(count($data['access']) == 0){
				$data['access'][0]['ID'] = 1;
			}
			//print_r($data['access']);

			$this->template->load('template', 'view_pencairan_dana', $data);
		}

		public function read() {
			$this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read());
		}

		public function read_all() {
			$this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read_all());
		}

		public function create() {
			//$param = $this->input->post('param'); // parametter key

			// additional block
			$tgl_cd = explode('-', $this->input->post('tanggal_cd'));
			$thn_bln = $tgl_cd[0] . '-' . $tgl_cd[1];
			
			$id = $this->get_no($thn_bln);
			$this->data_add['id_cd'] = $id;
			$this->data_add['code'] = 'KM';
			$this->data_add['creator'] = $this->session->userdata('id');
			$this->data_add['tanggal_create'] = date('Y-m-d H:i:s');

			// addtional get
			$result = $this->{$this->models[0]}->insert($this->data_add);
			if ($result == 1) {
				//$id = $this->{$this->models[0]}->getLastDataMitra();
				//$id = $this->db->insert_id();
				$this->{$this->models[0]}->createJatuhTempo($id);
				$this->{$this->models[0]}->createAccount($id);
				//print_r($id);
				$this->output->set_content_type('application/json')->set_output(json_encode(array(
					'success' => true,
					'id' => $id
				)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => $this->db->_error_message())));
			}
		}

		public function update() {
			$param = $this->uri->segment(4);
			// parameter key

			// additional block

			// addtional data

			// additional where
			$this->where_add['id_cd'] = $param;

			$result = $this->{$this->models[0]}->update($this->where_add, $this->data_add);
			if ($result == 1) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'Data Gagal Di Update !')));
			}
		}

		public function delete() {
			$param = $this->uri->segment(4);
			// parameter key

			// additional block

			// additional where
			$this->where_add['id_cd'] = $param;
			$this->where_add['code'] = 'KM';

			$angsuran = $this->{$this->models[0]}->statusDelete($param);
			//echo $angsuran;

			if($angsuran == 0){
				$this->{$this->models[0]}->deleteJatuhTempo($param);
				$this->{$this->models[0]}->deleteAccount($param);
				$result = $this->{$this->models[0]}->delete($this->where_add);
				if ($result == 1) {
					$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
				} else {
					$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'Data Gagal Di Hapus !')));
				}
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'Data Gagal Di Hapus ! Mitra Sudah Melakukan Pembayaran Angsuran')));
			}

			
		}

		public function getMitra() {
			$filter = array('B.keputusan' => 'Disetujui', 'C.id_cd' => NULL);
			$this->output->set_content_type('application/json')->set_output(json_encode($this->{$this->models[0]}->getMitra($filter)));
		}

		private function get_no($thn_bln) {
			$thn_bln_arr = explode('-', $thn_bln);

			$no = '000';

			$sql = "
				SELECT
					RIGHT(max(a.id_cd),4)*1 AS last_seq
				FROM
					tcd_all a
				WHERE
					DATE_FORMAT(a.tanggal_cd, '%Y-%m') = ? AND code = 'KM'
			";
			$query = $this->db->query($sql, array($thn_bln));
			$row = $query->row_array();

			$last_seq = (int)$row['last_seq'];
			$next_seq = $last_seq + 1;

			$leng_temp_no = strlen($next_seq);
			$no = 'CD'.implode('', $thn_bln_arr) . substr($no, $leng_temp_no) . $next_seq;

			return $no;

		}

		public function setAccess()
		{
			$access = $this->input->post('access');
			$id = $this->input->post('id');
			$filter = array('id_cd' => $id);
			$data = array('status' => ($access+1));
			
			$this->output->set_content_type('application/json')->set_output(json_encode($this->{$this->models[0]}->setAccess($filter, $data)));
		}

		function previewPencairanDana($idbl){    // load dompdf
    		$param = $this->uri->segment(4); // parameter key
			$filter = array('A.id_cd' => $param);
			$filter2 = array('A.id_cd_um' => $param);

		    //load content html
		    //$data['mitra'] = $this->{$this->models[0]}->kartuPiutangMitra($filter);
		    $data['pencairan'] = $this->{$this->models[0]}->getPencairan($filter);
		    $data['preview'] = $this->{$this->models[0]}->getPrevPencairan($filter2);
			
            $data['account'] = $this->{$this->models[0]}->getAccountPencairan($filter2);
            $data['sign']=$this->{$this->models[0]}->getsign()->row_array();
		    // $html = $this->load->view('sph3.php', $data, true);
		    //$html = $this->load->view('view_pencairan_dana_cd', $data,true);
		    $html = $this->load->view('view_pencairan_dana_cd', $data, true);
		    // create pdf using dompdf
  			$this->load->library('dompdf_gen'); // Load library
			$this->dompdf->set_paper('A4', 'portrait'); // Setting Paper
			// Convert to PDF
			$this->dompdf->load_html($html);
			$this->dompdf->render();
			$this->dompdf->stream("Pencairan_Dana_KM_".$idbl.".pdf");
        }

        public function test()
        {
        	//$id = $this->db->insert_id();
        	$id = '2017040001';
				$this->{$this->models[0]}->createAccount($id);
        }



        function cetakpencairan(){    // load dompdf

		    //load content html
		    $data['vendor'] = $this->{$this->models[0]}->getdataPencairan()->row_array();
		    // $html = $this->load->view('sph3.php', $data, true);
		    $html = $this->load->view('cetak_pencairandana.php', $data,true);
		    // echo $html;
		    // die();
		    // create pdf using dompdf
  			$this->load->library('dompdf_gen'); // Load library
			$this->dompdf->set_paper('A4', 'portrait'); // Setting Paper
			// Convert to PDF
			$this->dompdf->load_html($html);
			$this->dompdf->render();
			$this->dompdf->stream("Permohonan Mitra Binaan.pdf");
		}

		public function gen()
		{
			$this->load->helper('file');

			$file = '';
			
			$gen = $this->{$this->models[0]}->gen();

			foreach ($gen as $data) {
				# code...
				for($i = 1; $i <= $data['bulan']; $i++){
					$tanggal = strtotime($data['tgl_cd']);
					//$tgl = date("Y-m-d", strtotime("+1 month", $tanggal));
					$tgl = date("Y-m-d", strtotime("+".$i." month", $tanggal));
					//$file = $file."INSERT INTO tbl_jatuhtempo VALUES (NULL, '".$data['id_mitra']."', 0, '".$tgl."', 1, ".$i.");<br>";
					echo "INSERT INTO tbl_jatuhtempo VALUES (NULL, '".$data['id_mitra']."', 0, '".$tgl."', 1, ".$i.");<br>";
				}
			}

			/*if ( ! write_file(base_url().'assets/data.txt', $file))
			    {
			            echo 'Unable to write the file';
			    }
			    else
			    {
			            echo 'File written!';
			    }*/
		}
		
		public function preview_bukti_transfer($id = null) {
		    $this->db->where('id_mitra', $id);
		    $check = $this->db->get('bukti_transfer_cd');
		    if ($check->num_rows() > 0) {
		        
		        $data['data'] = $check->row_array();
		        
		        #kota tanggal
		        $data['kota'] = 'Cilegon';
		        $this->load->helper('tanggal_indo');
		        $this->load->helper('terbilang');
		        $data['tanggal'] = tanggal_display();
		        
		        $html = $this->load->view('preview_bukti_transfer', $data, true);
		        
		        $this->load->library('dompdf_gen');
		        #Load library
		        $this->dompdf->set_paper('A4', 'potrait');
		        #Setting Paper
		        ##Convert to PDF
		        $this->dompdf->load_html($html);
		        $this->dompdf->render();
		        $this->dompdf->stream("Bukti-transfer-" . $id . ".pdf");
		    } else {
		        show_404();
		    }
		}
		
		public function batal_cd() {
		    // parameter key
		    $id_cd = $this->input->post('id_cd');
		    $code = $this->input->post('code');
		    
		    // additional block
		    
		    // addtional data
		    $this->data_add['status_progress'] = '1';
		    
		    // additional where
		    $this->where_add['id_cd'] = $id_cd;
		    $this->where_add['code'] = $code;
		    
		    $result = $this->{$this->models[0]}->updateOtherData('tcd_all', $this->where_add, $this->data_add);
		    if ($result == 1) {
		        $this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
		    } else {
		        $this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'Data Gagal Di Update !')));
		    }
		}

	}
