<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');
	class Usulan_pinjaman extends MY_Controller {
		public $models = array('usulan_pinjaman');

		public function __construct() {
			parent::__construct();
			$this->load->library('disposisi');
		}

		public function index() {
			$data = array();
			$data['menu'] = $this->model_menu->getAllMenu();

			$this->template->load('template', 'view_' . $this->models[0], $data);
		}

		public function read() {
			$this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read());
		}

		public function read_all() {
			$this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read_all());
		}

		public function create() {

		}

		public function update() {

		}

		public function delete() {

		}

		public function view_report_usulan($str_id = null) {
			if ($str_id) {
				$arr_id = explode('_', $str_id);
				$this->db->where_in('id_mitra', $arr_id);

				$check = $this->db->get('usulan_pinjaman_preview_vd');
				if ($check->num_rows() > 0) {
					$this->load->helper('tanggal_indo');

					// $data['no_pkbl'] = '001/USL/PKBL-KS/2017';
					$data['data'] = $check->result_array();

					// kota tanggal
					// $data['kota'] = 'Cilegon';
					// $data['tanggal'] = '25 Oktober 2016';

					$html = $this->load->view('usulan_pinjaman_preview', $data, true);

					$this->load->library('dompdf_gen');
					$this->dompdf->set_paper('legal', 'landscape');

					$this->dompdf->load_html($html);
					$this->dompdf->render();
					$this->dompdf->stream("Laporan Usulan Kemitraan.pdf");
				} else {
					show_404();
				}
			} else {
				show_404();
			}
		}

	}
