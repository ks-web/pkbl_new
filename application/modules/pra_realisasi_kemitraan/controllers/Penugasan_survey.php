<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');
	class Penugasan_survey extends MY_Controller {
		public $models = array('penugasan_survey');

		public function __construct() {
			parent::__construct();
		}

		public function index() {
			$data = array();
			$data['menu'] = $this->model_menu->getAllMenu();

			$this->template->load('template', 'view_' . $this->models[0], $data);
		}

		public function read() {
			$group_id = $this->session->userdata('group_id');
			$id_pegawai = $this->session->userdata('idPegawai');
			$where = array('id_petugas_survey' => $id_pegawai );
			$data = $this->{$this->models[0]}->read($where);
			$json = json_decode($data);

			if ($group_id == "4"){
				$this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read($where));
			}else{
				$this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read());
			}
		}

		public function read_all() {
			$this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read_all());
		}

		public function create() {
			// parametter key

			// additional block
			$this->data_add['status'] = '1';
			// addtional get
			$result = $this->{$this->models[0]}->insert($this->data_add);
			if ($result == 1) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => $this->db->_error_message())));
			}
		}

		public function update() {
			$param = $this->uri->segment(4);
			// parameter key

			// additional block

			// addtional data

			// additional where
			$this->where_add['id_mitra'] = $param;

			$result = $this->{$this->models[0]}->update($this->where_add, $this->data_add);
			if ($result == 1) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'Data Gagal Di Update !')));
			}
		}

		public function delete() {
			$param = $this->uri->segment(4);
			// parameter key

			// additional block

			// additional where
			$this->where_add['id_mitra'] = $param;

			$result = $this->{$this->models[0]}->delete($this->where_add);
			if ($result == 1) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'Data Gagal Di Hapus !')));
			}
		}

		public function get_mitra() {
			$q = $this->input->post('q');

			$ret = $this->{$this->models[0]}->get_mitra($q);

			// if (sizeof($ret) > 0) {
			// $ret[0]['selected'] = true;
			// }

			echo json_encode($ret);
		}

		public function get_petugas_survey() {
			$q = $this->input->post('q');

			$ret = $this->{$this->models[0]}->get_petugas_survey($q);

			// if (sizeof($ret) > 0) {
			// $ret[0]['selected'] = true;
			// }

			echo json_encode($ret);
		}

		function permohonan() {// load dompdf

			//load content html
			$data['vendor'] = $this->{$this->models[0]}->permohonan()->row_array();
			// $html = $this->load->view('sph3.php', $data, true);
			$html = $this->load->view('Datasurveymitra.php', $data, true);
			// echo $html;
			// die();
			// create pdf using dompdf
			$this->load->library('dompdf_gen');
			// Load library
			$this->dompdf->set_paper('A4', 'portrait');
			// Setting Paper
			// Convert to PDF
			$this->dompdf->load_html($html);
			$this->dompdf->render();
			$this->dompdf->stream("Formulir Survey Mitra - ".$data['vendor']['id_mitra'].".pdf");
		}

	}
