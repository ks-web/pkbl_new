<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');
	class K_investasi extends MY_Controller {
		public $models = array('k_investasi');
		
		public function __construct() {
			parent::__construct();
		}

		public function index() {
			$data = array();
			$data['menu'] = $this->model_menu->getAllMenu();

			$this->template->load('template', 'view_k_investasi', $data);
		}

		public function read() {
			$this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read());
			$this->db->last_query();
		}

		public function read_all() {
			$this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read_all());
		}

		public function create() {
			// additional block
			$this->data_add['id_mitra']=$this->uri->segment(4);
			// addtional get
			$result = $this->{$this->models[0]}->insert($this->data_add);
			if ($result == 1) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => $this->db->_error_message())));
			}
		}

		public function update() {
			// additional block

			// addtional data

			// additional where
			$this->where_add['id_asset'] = $this->uri->segment(4);

			$result = $this->{$this->models[0]}->update($this->where_add, $this->data_add);
			if ($result == 1) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'Data Gagal Di Update !')));
			}
		}

		public function delete() {
			$param = $this->uri->segment(4); 
			// additional block

			// additional where
			$this->where_add['id_asset'] = $param;

			$result = $this->{$this->models[0]}->delete($this->where_add);
			
			if ($result == 1) {
				
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'Data Gagal Di Hapus !')));
			}
		}

		private function get_id_asset($id_mitra = NULL) {
			$last_id = $id_mitra;
			if ($id_mitra) {
				$this->db->SELECT_max('id_asset');
				$this->db->where(array('id_mitra' => $id_mitra));
				$oni = $this->db->get('k_investasi')->row_array();
				$last_id = $oni['id_asset'];
				if ($last_id == NULL)
					$last_id = 0;

				$last_id = (int)$last_id;
				$last_id += 1;
			}
			return $last_id;

		}
		public function geturaian($filter = null)
        {
            $this->output->set_content_type('application/json')->set_output(json_encode($this->{$this->models[0]}->geturaian($filter)));
        }  
        public function getsatuan($filter = null)
        {
            $this->output->set_content_type('application/json')->set_output(json_encode($this->{$this->models[0]}->getsatuan($filter)));
        }
	}
