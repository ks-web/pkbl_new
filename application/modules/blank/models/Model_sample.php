<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	Class Model_sample extends MY_Model {
		function __construct() {
			parent::__construct();
			$this->_table = "dummy"; // for insert, update, delete
			$this->_view = "view_dummy"; // for call view
			$this->_order = 'asc';
			$this->_sort = 'ID';
			$this->_page = 1;
			$this->_rows = 10;

			$this->_create = true;
			$this->_update = true;

			if ($this->uri->segment(4)) {
				$this->_filter = array('ID' => $this->uri->segment(4));
			}

			//parameter from post/get - search function
			if ($this->input->post('q')) {
				$this->_like = array(
					'col1' => $this->input->post('q'),
					'col2' => $this->input->post('q'),
					'col3' => $this->input->post('q')
				);
			}

			$this->_param = array('node' => $this->input->post('node'));

			//data array for input to database
			$this->_data = array(
				'col1' => $this->input->post('col1'),
				'col2' => $this->input->post('col2'),
			);
			
		}

	}
