<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');
	class Datamitra extends MY_Controller {
		public $models = array('datamitra');

		public function __construct() {
			parent::__construct();
		}

		public function index() {
			$data = array();
			$data['menu'] = $this->model_menu->getAllMenu();

			$this->template->load('template', 'view_datamitra', $data);
		}

		public function read() {
			$this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read());
		}

		public function read_all() {
			$this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read_all());
		}

		public function create() {
			//$param = $this->input->post('param'); // parametter key

			// additional block
			$this->data_add['tanggal_input'] = date('Y-m-d');
			$this->data_add['tanggal_create'] = date('Y-m-d');
			$this->data_add['creator'] = $this->session->userdata('idPegawai');
			$this->data_add['id_mitra'] = $this->get_idmitra();
			$this->data_add['status'] = 2;

			// addtional get
			$result = $this->{$this->models[0]}->insert($this->data_add);
			if ($result == 1) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => $this->db->_error_message())));
			}
		}

		public function update() {

			$param = $this->uri->segment(4);
			// parameter key

			// additional block

			// addtional data

			// additional where
			$this->where_add['id_mitra'] = $param;
			$this->data_add['tanggal_modified'] = date('Y-m-d');
			$this->data_add['modifier'] = $this->session->userdata('idPegawai');

			$result = $this->{$this->models[0]}->update($this->where_add, $this->data_add);
			if ($result == 1) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'Data Gagal Di Update !')));
			}
		}

		public function delete() {
			$param = $this->uri->segment(4);
			// parameter key

			// additional block

			// additional where
			$this->where_add['id_mitra'] = $param;

			$result = $this->{$this->models[0]}->delete($this->where_add);
			if ($result == 1) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'Data Gagal Di Hapus !')));
			}
		}

		public function getsektormitra() {
			$filter = array();
			$this->output->set_content_type('application/json')->set_output(json_encode($this->{$this->models[0]}->getsektormitra($filter)));
		}

		public function getpendidikan() {
			$filter = array();
			$this->output->set_content_type('application/json')->set_output(json_encode($this->{$this->models[0]}->getpendidikan($filter)));
		}

		public function getjaminan() {
			$filter = array();
			$this->output->set_content_type('application/json')->set_output(json_encode($this->{$this->models[0]}->getjaminan($filter)));
		}

		public function getbank($filter = null) {
			$this->output->set_content_type('application/json')->set_output(json_encode($this->{$this->models[0]}->getbank($filter)));
		}

		public function getPropinsi() {
			$filter = array('A.id !=' => 0);
			$this->output->set_content_type('application/json')->set_output(json_encode($this->{$this->models[0]}->getPropinsi($filter)));
		}

		public function getKota($id) {
			$filter = array('A.province_id' => $id);
			$this->output->set_content_type('application/json')->set_output(json_encode($this->{$this->models[0]}->getKota($filter)));
		}

		public function getKecamatan($id) {
			$filter = array('A.regency_id' => $id);
			$this->output->set_content_type('application/json')->set_output(json_encode($this->{$this->models[0]}->getKecamatan($filter)));
		}

		public function getKelurahan($id) {
			$filter = array('A.district_id' => $id);
			$this->output->set_content_type('application/json')->set_output(json_encode($this->{$this->models[0]}->getKelurahan($filter)));
		}

		public function getLokasi() {
			$filter = array();
			$this->output->set_content_type('application/json')->set_output(json_encode($this->{$this->models[0]}->getLokasi($filter)));
		}

		function permohonan() {// load dompdf

			//load content html
			$data['vendor'] = $this->{$this->models[0]}->permohonan()->row_array();
			// $html = $this->load->view('sph3.php', $data, true);
			$html = $this->load->view('Permohonanmitra.php', $data, true);
			// echo $html;
			// die();
			// create pdf using dompdf
			$this->load->library('dompdf_gen');
			// Load library
			$this->dompdf->set_paper('A4', 'portrait');
			// Setting Paper
			// Convert to PDF
			$this->dompdf->load_html($html);
			$this->dompdf->render();
			$this->dompdf->stream("Permohonan Mitra Binaan - ".$data['vendor']['id_mitra'].".pdf");
		}

		function get_idmitra() {
			$maxkm = $this->{$this->models[0]}->get_idmitra();
			if ($maxkm->num_rows() > 0) {
				$q = $maxkm->result_array();
				$a = $q[0]['id_mitra'];
			} else {
				$a = "0001";
			}
			$hasil = "KM" . date('Ym') . $a;
			return $hasil;
		}

		// DISPOSISI START
		// -------------------------------------------------------------------------
		public function getPegawai() {
			//$filter = array('A.id !=' => 0);
			$this->output->set_content_type('application/json')->set_output(json_encode($this->{$this->models[0]}->getPegawai()));
		}

		public function getPesan() {
			$this->output->set_content_type('application/json')->set_output(json_encode($this->{$this->models[0]}->getPesan()));
		}

		public function add_disposisi() {
			date_default_timezone_set('Asia/Jakarta');

			$id_doc = $this->input->post('id_doc');
			$tujuan_disposisi = $this->input->post('tujuan_disposisi');
			$pesan_disposisi = $this->input->post('pesan_disposisi');
			$catatan_disposisi = $this->input->post('catatan_disposisi');

			$data = array(
				'id_doc' => $id_doc,
				'node_menu' => 22,
				'nip_tujuan' => $tujuan_disposisi,
				'pesan' => $pesan_disposisi,
				'catatan_disposisi' => $catatan_disposisi,
				'creator' => $this->session->userdata('idPegawai'),
				'tanggal_disposisi' => date("Y-m-d H:i:s")
			);

			$res = $this->db->insert('disposisi_transaksi', $data);

			if ($res) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'Gagal Kirim Disposisi!')));
			}
		}

		public function disposisi_affterProses() {
			$this->output->set_content_type('application/json')->set_output(json_encode($this->{$this->models[0]}->disposisi_affterProses()));
		}

		public function disposisi_selesai() {
			$tujuan = $this->uri->segment(4);
			$id_doc = $this->uri->segment(5);
			header("Content-Type: application/json");
			date_default_timezone_set('Asia/Jakarta');

			$data = array(
				'approve' => '1',
				'approve_date_time' => date("Y-m-d H:i:s")
			);

			$this->{$this->models[0]}->disposisi_selesai($tujuan, $id_doc, $data);

		}

		public function get_approval() {
			$ses_id = $this->session->userdata('idPegawai');

			$this->db->where('idPegawai', $ses_id);
			$data = $this->db->get('mas_approval_vd')->row_array();

			// // echo var_dump($data);
			$this->output->set_content_type('application/json')->set_output(json_encode($data));

		}

		public function get_mitra() {
			$this->output->set_content_type('application/json')->set_output(json_encode($this->{$this->models[0]}->get_mitra()));
		}

		public function hapus_disposisi() {
			$id_doc = $this->uri->segment(4);
			$tujuan = $this->uri->segment(5);
			$creator = $this->uri->segment(6);
			$creator_aktiv = $this->session->userdata('idPegawai');

			if ($creator_aktiv != $creator) {
				$this->output->set_output(json_encode(array('msg' => 'User yang Aktif tidak Bisa Menghapus')));
			} else {

				$where = array(
					'id_doc' => $id_doc,
					'nip_tujuan' => $tujuan,
					'creator' => $creator
				);
				$this->db->where($where);
				$data = $this->db->delete('disposisi_transaksi');

				if ($data) {
					$this->output->set_output(json_encode(array('success' => TRUE)));
				} else {
					$this->output->set_output(json_encode(array('msg' => 'Hapus Gagal')));
				}

			}
		}

		function cetak_disposisi($id = null) {// load dompdf
			$this->db->where('id_mitra', $id);
			$check = $this->db->get('tmitra_cetak_disp_vd');
			if ($check->num_rows() > 0) {
				$data['data_head'] = $check->row_array();
				$data['data_detail'] = null;

				$this->db->where('id_doc', $id);
				// $q = $this->db->get('tmitra_cetak_disp_detail_vd');
				$q = $this->db->get('disposisi_pesan_detail_vd');
				if ($q->num_rows() > 0) {
					$data['data_detail'] = $q->result_array();
				}

				// kota tanggal
				$data['kota'] = 'Cilegon';
				$this->load->helper('tanggal_indo');
				$this->load->helper('terbilang');
				$data['tanggal'] = tanggal_display();

				$html = $this->load->view('cetak_disposisi_datamitra', $data, true);

				$this->load->library('dompdf_gen');
				// Load library
				$this->dompdf->set_paper('A4', 'potrait');
				// Setting Paper
				// // Convert to PDF
				$this->dompdf->load_html($html);
				$this->dompdf->render();
				$this->dompdf->stream("Lembar_Disposisi-" . $id . ".pdf");
			} else {
				show_404();
			}

		}

		// DISPOSISI END
		// -------------------------------------------------------------------------
		function kirim_survey() {
			$id_mitra = $this->input->post('id_mitra');
			$tgl_survey = $this->input->post('tgl_survey');
			$petugas = $this->input->post('petugas');
			$ket = $this->input->post('ket');

			if ($id_mitra) {
				foreach ($id_mitra as $key => $value) {
					$data = array(
						'id_mitra' => $id_mitra[$key],
						'tanggal_survey' => $tgl_survey,
						'petugas_survey' => $petugas,
						'keterangan' => $ket,
						'tanggal_create'=>$tgl_survey,
						'pemberi_tugas' => $this->session->userdata('idPegawai')
					);

					$res = $this->db->insert('tpenugasan_survey', $data);
				}
				if ($res) {

					$this->output->set_output(json_encode(array('success' => TRUE)));

					foreach ($id_mitra as $key => $value) {
						$data = array('status' => 3);

						$where = array('id_mitra' => $id_mitra[$key]);

						$res = $this->db->update('tmitra', $data, $where);
					}

				} else {
					$this->output->set_output(json_encode(array('msg' => 'Gagal')));
				}
			} else {
				$this->output->set_output(json_encode(array('msg' => 'Pilih Minimal Satu Mitra')));
			}

		}

		function test() {
			echo "<pre>";
			print_r($this->session->all_userdata());
			echo "</pre>";
		}

		function upload_lampiran() {

			$this->data_add['id_mitra'] = $this->input->post('id_mitra');
			$param = $this->input->post('id_mitra');

			$vendor_dir = 'assets/upload/MT/' . $param;
			if (!is_dir($vendor_dir)) {
				mkdir($vendor_dir, folderUploadPermission, TRUE);
			}
			$dir = 'assets/upload/MT/' . $param . '/ktp';
			if (!is_dir($dir)) {
				mkdir($dir, folderUploadPermission, TRUE);
			}

			$config['upload_path'] = $dir . '/';
			$config['allowed_types'] = implode('|', json_decode(fileUploadTipe5));
			$config['max_size'] = fileUploadSize5;
			$config['encrypt_name'] = true;
			$this->upload->initialize($config);
			//ktp_no
			if (!$this->upload->do_upload('lampiran_ktp')) {
				NULL;
			} else {
				$file = $this->upload->data();
				if ($current_ktp != '') {
					$this->load->library('common');
					$this->common->delete_file($current_ktp);
				}

				$this->data_add['lampiran_ktp'] = $config['upload_path'] . $file['file_name'];
			}
			//foto
			$vendor_dir = 'assets/upload/MT/' . $param;
			if (!is_dir($vendor_dir)) {
				mkdir($vendor_dir, folderUploadPermission, TRUE);
			}
			$dir = 'assets/upload/MT/' . $param . '/Pas_Photo';
			if (!is_dir($dir)) {
				mkdir($dir, folderUploadPermission, TRUE);
			}
			$config['upload_path'] = $dir . '/';
			$config['allowed_types'] = implode('|', json_decode(fileUploadTipe5));
			$config['max_size'] = fileUploadSize5;
			$config['encrypt_name'] = true;
			$this->upload->initialize($config);
			if (!$this->upload->do_upload('lampiran_paspoto')) {
				NULL;
			} else {
				$file = $this->upload->data();
				if ($current_foto != '') {
					$this->load->library('common');
					$this->common->delete_file($current_foto);
				}

				$this->data_add['lampiran_paspoto'] = $config['upload_path'] . $file['file_name'];
			}

			// foto_ahli_waris
			$vendor_dir = 'assets/upload/MT/' . $param;
			if (!is_dir($vendor_dir)) {
				mkdir($vendor_dir, folderUploadPermission, TRUE);
			}
			$dir = 'assets/upload/MT/' . $param . '/Pas_foto_ahli_waris';
			if (!is_dir($dir)) {
				mkdir($dir, folderUploadPermission, TRUE);
			}
			$config['upload_path'] = $dir . '/';
			$config['allowed_types'] = implode('|', json_decode(fileUploadTipe5));
			$config['max_size'] = fileUploadSize5;
			$config['encrypt_name'] = true;
			$this->upload->initialize($config);
			if (!$this->upload->do_upload('lampiran_paspoto_ahli_waris')) {
				NULL;
			} else {
				$file = $this->upload->data();
				if ($current_foto != '') {
					$this->load->library('common');
					$this->common->delete_file($current_foto);
				}
				$this->data_add['lampiran_paspoto_ahli_waris'] = $config['upload_path'] . $file['file_name'];
			}

			//kk
			$vendor_dir = 'assets/upload/MT/' . $param;
			if (!is_dir($vendor_dir)) {
				mkdir($vendor_dir, folderUploadPermission, TRUE);
			}
			$dir = 'assets/upload/MT/' . $param . '/KK';
			if (!is_dir($dir)) {
				mkdir($dir, folderUploadPermission, TRUE);
			}
			$config['upload_path'] = $dir . '/';
			$config['allowed_types'] = implode('|', json_decode(fileUploadTipe5));
			$config['max_size'] = fileUploadSize5;
			$config['encrypt_name'] = true;
			$this->upload->initialize($config);
			if (!$this->upload->do_upload('lampiran_kk')) {
				NULL;
			} else {
				$file = $this->upload->data();
				if ($current_kk != '') {
					$this->load->library('common');
					$this->common->delete_file($current_kk);
				}

				$this->data_add['lampiran_kk'] = $config['upload_path'] . $file['file_name'];
			}
			//lampiran_suratusaha
			$vendor_dir = 'assets/upload/MT/' . $param;
			if (!is_dir($vendor_dir)) {
				mkdir($vendor_dir, folderUploadPermission, TRUE);
			}
			$dir = 'assets/upload/MT/' . $param . '/Surat Usaha';
			if (!is_dir($dir)) {
				mkdir($dir, folderUploadPermission, TRUE);
			}
			$config['upload_path'] = $dir . '/';
			$config['allowed_types'] = implode('|', json_decode(fileUploadTipe5));
			$config['max_size'] = fileUploadSize5;
			$config['encrypt_name'] = true;
			$this->upload->initialize($config);
			if (!$this->upload->do_upload('lampiran_suratusaha')) {
				NULL;
			} else {
				$file = $this->upload->data();
				if ($current_surtusaha != '') {
					$this->load->library('common');
					$this->common->delete_file($current_surtusaha);
				}

				$this->data_add['lampiran_suratusaha'] = $config['upload_path'] . $file['file_name'];
			}
			//lampiran_tempatusaha
			$vendor_dir = 'assets/upload/MT/' . $param;
			if (!is_dir($vendor_dir)) {
				mkdir($vendor_dir, folderUploadPermission, TRUE);
			}
			$dir = 'assets/upload/MT/' . $param . '/Tempat Usaha';
			if (!is_dir($dir)) {
				mkdir($dir, folderUploadPermission, TRUE);
			}
			$config['upload_path'] = $dir . '/';
			$config['allowed_types'] = implode('|', json_decode(fileUploadTipe5));
			$config['max_size'] = fileUploadSize5;
			$config['encrypt_name'] = true;
			$this->upload->initialize($config);
			if (!$this->upload->do_upload('lampiran_tempatusaha')) {
				NULL;
			} else {
				$file = $this->upload->data();
				if ($current_temptusaha != '') {
					$this->load->library('common');
					$this->common->delete_file($current_temptusaha);
				}

				$this->data_add['lampiran_tempatusaha'] = $config['upload_path'] . $file['file_name'];
			}
			//lampiran_produk
			$vendor_dir = 'assets/upload/MT/' . $param;
			if (!is_dir($vendor_dir)) {
				mkdir($vendor_dir, folderUploadPermission, TRUE);
			}
			$dir = 'assets/upload/MT/' . $param . '/Produk';
			if (!is_dir($dir)) {
				mkdir($dir, folderUploadPermission, TRUE);
			}
			$config['upload_path'] = $dir . '/';
			$config['allowed_types'] = implode('|', json_decode(fileUploadTipe5));
			$config['max_size'] = fileUploadSize5;
			$config['encrypt_name'] = true;
			$this->upload->initialize($config);
			if (!$this->upload->do_upload('lampiran_produk')) {
				NULL;
			} else {
				$file = $this->upload->data();
				if ($current_produk != '') {
					$this->load->library('common');
					$this->common->delete_file($current_produk);
				}

				$this->data_add['lampiran_produk'] = $config['upload_path'] . $file['file_name'];
			}
			//lampiran_kegiatanusaha
			$vendor_dir = 'assets/upload/MT/' . $param;
			if (!is_dir($vendor_dir)) {
				mkdir($vendor_dir, folderUploadPermission, TRUE);
			}
			$dir = 'assets/upload/MT/' . $param . '/kegiatan_usaha';
			if (!is_dir($dir)) {
				mkdir($dir, folderUploadPermission, TRUE);
			}
			$config['upload_path'] = $dir . '/';
			$config['allowed_types'] = implode('|', json_decode(fileUploadTipe5));
			$config['max_size'] = fileUploadSize5;
			$config['encrypt_name'] = true;
			$this->upload->initialize($config);
			if (!$this->upload->do_upload('lampiran_kegiatanusaha')) {
				NULL;
			} else {
				$file = $this->upload->data();
				if ($current_kegiatanprod != '') {
					$this->load->library('common');
					$this->common->delete_file($current_kegiatanprod);
				}

				$this->data_add['lampiran_kegiatanusaha'] = $config['upload_path'] . $file['file_name'];
			}
			//lampiran_keterangantinggal
			$vendor_dir = 'assets/upload/MT/' . $param;
			if (!is_dir($vendor_dir)) {
				mkdir($vendor_dir, folderUploadPermission, TRUE);
			}
			$dir = 'assets/upload/MT/' . $param . '/Ket Tinggal';
			if (!is_dir($dir)) {
				mkdir($dir, folderUploadPermission, TRUE);
			}
			$config['upload_path'] = $dir . '/';
			$config['allowed_types'] = implode('|', json_decode(fileUploadTipe5));
			$config['max_size'] = fileUploadSize5;
			$config['encrypt_name'] = true;
			$this->upload->initialize($config);
			if (!$this->upload->do_upload('lampiran_keterangantinggal')) {
				NULL;
			} else {
				$file = $this->upload->data();
				if ($current_kettinggal != '') {
					$this->load->library('common');
					$this->common->delete_file($current_kettinggal);
				}

				$this->data_add['lampiran_keterangantinggal'] = $config['upload_path'] . $file['file_name'];
			}
			//lampiran_bukutabungan
			$vendor_dir = 'assets/upload/MT/' . $param;
			if (!is_dir($vendor_dir)) {
				mkdir($vendor_dir, folderUploadPermission, TRUE);
			}
			$dir = 'assets/upload/MT/' . $param . '/buku_tabungan';
			if (!is_dir($dir)) {
				mkdir($dir, folderUploadPermission, TRUE);
			}
			$config['upload_path'] = $dir . '/';
			$config['allowed_types'] = implode('|', json_decode(fileUploadTipe5));
			$config['max_size'] = fileUploadSize5;
			$config['encrypt_name'] = true;
			$this->upload->initialize($config);
			if (!$this->upload->do_upload('lampiran_bukutabungan')) {
				NULL;
			} else {
				$file = $this->upload->data();
				if ($current_btabungan != '') {
					$this->load->library('common');
					$this->common->delete_file($current_btabungan);
				}

				$this->data_add['lampiran_bukutabungan'] = $config['upload_path'] . $file['file_name'];
			}
			//lampiran_pernyataan
			$vendor_dir = 'assets/upload/MT/' . $param;
			if (!is_dir($vendor_dir)) {
				mkdir($vendor_dir, folderUploadPermission, TRUE);
			}
			$dir = 'assets/upload/MT/' . $param . '/Pernyataan';
			if (!is_dir($dir)) {
				mkdir($dir, folderUploadPermission, TRUE);
			}
			$config['upload_path'] = $dir . '/';
			$config['allowed_types'] = implode('|', json_decode(fileUploadTipe5));
			$config['max_size'] = fileUploadSize5;
			$config['encrypt_name'] = true;
			$this->upload->initialize($config);
			if (!$this->upload->do_upload('lampiran_pernyataan')) {
				NULL;
			} else {
				$file = $this->upload->data();
				if ($current_pernyataan != '') {
					$this->load->library('common');
					$this->common->delete_file($current_pernyataan);
				}

				$this->data_add['lampiran_pernyataan'] = $config['upload_path'] . $file['file_name'];
			}

			$this->where_add['id_mitra'] = $param;
			$result = $this->{$this->models[0]}->update($this->where_add, $this->data_add);

			if ($result) {
				//$this->_log_vendor('create', 'Ijin Usaha');
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'Data Gagal Diupdate !')));
			}
		}

		function upload_lampiran_all($id_mitra) {

			$param = $id_mitra;
			$vendor_dir = 'assets/upload/MT/' . $param;
			if (!is_dir($vendor_dir)) {
				mkdir($vendor_dir, folderUploadPermission, TRUE);
			}
			$dir = 'assets/upload/MT/' . $param . '/ktp';
			if (!is_dir($dir)) {
				mkdir($dir, folderUploadPermission, TRUE);
			}

			$config['upload_path'] = $dir . '/';
			$config['allowed_types'] = implode('|', json_decode(fileUploadTipe5));
			$config['max_size'] = fileUploadSize5;
			$config['encrypt_name'] = true;
			$this->upload->initialize($config);
			//ktp_no
			if (!$this->upload->do_upload('lampiran_ktp')) {

			} else {
				$file = $this->upload->data();
				$data = array('lampiran_ktp' => $config['upload_path'] . $file['file_name']);
			}
			//foto
			$vendor_dir = 'assets/upload/MT/' . $param;
			if (!is_dir($vendor_dir)) {
				mkdir($vendor_dir, folderUploadPermission, TRUE);
			}
			$dir = 'assets/upload/MT/' . $param . '/Pas_Photo';
			if (!is_dir($dir)) {
				mkdir($dir, folderUploadPermission, TRUE);
			}
			$config['upload_path'] = $dir . '/';
			$config['allowed_types'] = implode('|', json_decode(fileUploadTipe5));
			$config['max_size'] = fileUploadSize5;
			$config['encrypt_name'] = true;
			$this->upload->initialize($config);
			if (!$this->upload->do_upload('lampiran_paspoto')) {

			} else {
				$file = $this->upload->data();
				$data = array('lampiran_paspoto' => $config['upload_path'] . $file['file_name']);
			}

			// foto_ahli_waris
			$vendor_dir = 'assets/upload/MT/' . $param;
			if (!is_dir($vendor_dir)) {
				mkdir($vendor_dir, folderUploadPermission, TRUE);
			}
			$dir = 'assets/upload/MT/' . $param . '/Pas_foto_ahli_waris';
			if (!is_dir($dir)) {
				mkdir($dir, folderUploadPermission, TRUE);
			}
			$config['upload_path'] = $dir . '/';
			$config['allowed_types'] = implode('|', json_decode(fileUploadTipe5));
			$config['max_size'] = fileUploadSize5;
			$config['encrypt_name'] = true;
			$this->upload->initialize($config);
			if (!$this->upload->do_upload('lampiran_paspoto_ahli_waris')) {

			} else {
				$file = $this->upload->data();
				$data = array('lampiran_paspoto_ahli_waris' => $config['upload_path'] . $file['file_name']);
			}

			//kk
			$vendor_dir = 'assets/upload/MT/' . $param;
			if (!is_dir($vendor_dir)) {
				mkdir($vendor_dir, folderUploadPermission, TRUE);
			}
			$dir = 'assets/upload/MT/' . $param . '/KK';
			if (!is_dir($dir)) {
				mkdir($dir, folderUploadPermission, TRUE);
			}
			$config['upload_path'] = $dir . '/';
			$config['allowed_types'] = implode('|', json_decode(fileUploadTipe5));
			$config['max_size'] = fileUploadSize5;
			$config['encrypt_name'] = true;
			$this->upload->initialize($config);
			if (!$this->upload->do_upload('lampiran_kk')) {

			} else {
				$file = $this->upload->data();
				$data = array('lampiran_kk' => $config['upload_path'] . $file['file_name']);
			}
			//lampiran_suratusaha
			$vendor_dir = 'assets/upload/MT/' . $param;
			if (!is_dir($vendor_dir)) {
				mkdir($vendor_dir, folderUploadPermission, TRUE);
			}
			$dir = 'assets/upload/MT/' . $param . '/Surat Usaha';
			if (!is_dir($dir)) {
				mkdir($dir, folderUploadPermission, TRUE);
			}
			$config['upload_path'] = $dir . '/';
			$config['allowed_types'] = implode('|', json_decode(fileUploadTipe5));
			$config['max_size'] = fileUploadSize5;
			$config['encrypt_name'] = true;
			$this->upload->initialize($config);
			if (!$this->upload->do_upload('lampiran_suratusaha')) {

			} else {
				$file = $this->upload->data();
				$data = array('lampiran_suratusaha' => $config['upload_path'] . $file['file_name']);
			}
			//lampiran_tempatusaha
			$vendor_dir = 'assets/upload/MT/' . $param;
			if (!is_dir($vendor_dir)) {
				mkdir($vendor_dir, folderUploadPermission, TRUE);
			}
			$dir = 'assets/upload/MT/' . $param . '/Tempat Usaha';
			if (!is_dir($dir)) {
				mkdir($dir, folderUploadPermission, TRUE);
			}
			$config['upload_path'] = $dir . '/';
			$config['allowed_types'] = implode('|', json_decode(fileUploadTipe5));
			$config['max_size'] = fileUploadSize5;
			$config['encrypt_name'] = true;
			$this->upload->initialize($config);
			if (!$this->upload->do_upload('lampiran_tempatusaha')) {

			} else {
				$file = $this->upload->data();
				$data = array('lampiran_tempatusaha' => $config['upload_path'] . $file['file_name']);
			}
			//lampiran_produk
			$vendor_dir = 'assets/upload/MT/' . $param;
			if (!is_dir($vendor_dir)) {
				mkdir($vendor_dir, folderUploadPermission, TRUE);
			}
			$dir = 'assets/upload/MT/' . $param . '/Produk';
			if (!is_dir($dir)) {
				mkdir($dir, folderUploadPermission, TRUE);
			}
			$config['upload_path'] = $dir . '/';
			$config['allowed_types'] = implode('|', json_decode(fileUploadTipe5));
			$config['max_size'] = fileUploadSize5;
			$config['encrypt_name'] = true;
			$this->upload->initialize($config);
			if (!$this->upload->do_upload('lampiran_produk')) {

			} else {
				$file = $this->upload->data();
				$data = array('lampiran_produk' => $config['upload_path'] . $file['file_name']);
			}
			//lampiran_kegiatanusaha
			$vendor_dir = 'assets/upload/MT/' . $param;
			if (!is_dir($vendor_dir)) {
				mkdir($vendor_dir, folderUploadPermission, TRUE);
			}
			$dir = 'assets/upload/MT/' . $param . '/kegiatan_usaha';
			if (!is_dir($dir)) {
				mkdir($dir, folderUploadPermission, TRUE);
			}
			$config['upload_path'] = $dir . '/';
			$config['allowed_types'] = implode('|', json_decode(fileUploadTipe5));
			$config['max_size'] = fileUploadSize5;
			$config['encrypt_name'] = true;
			$this->upload->initialize($config);
			if (!$this->upload->do_upload('lampiran_kegiatanusaha')) {

			} else {
				$file = $this->upload->data();
				$data = array('lampiran_kegiatanusaha' => $config['upload_path'] . $file['file_name']);
			}
			//lampiran_keterangantinggal
			$vendor_dir = 'assets/upload/MT/' . $param;
			if (!is_dir($vendor_dir)) {
				mkdir($vendor_dir, folderUploadPermission, TRUE);
			}
			$dir = 'assets/upload/MT/' . $param . '/Ket Tinggal';
			if (!is_dir($dir)) {
				mkdir($dir, folderUploadPermission, TRUE);
			}
			$config['upload_path'] = $dir . '/';
			$config['allowed_types'] = implode('|', json_decode(fileUploadTipe5));
			$config['max_size'] = fileUploadSize5;
			$config['encrypt_name'] = true;
			$this->upload->initialize($config);
			if (!$this->upload->do_upload('lampiran_keterangantinggal')) {

			} else {
				$file = $this->upload->data();
				$data = array('lampiran_keterangantinggal' => $config['upload_path'] . $file['file_name']);
			}
			//lampiran_bukutabungan
			$vendor_dir = 'assets/upload/MT/' . $param;
			if (!is_dir($vendor_dir)) {
				mkdir($vendor_dir, folderUploadPermission, TRUE);
			}
			$dir = 'assets/upload/MT/' . $param . '/buku_tabungan';
			if (!is_dir($dir)) {
				mkdir($dir, folderUploadPermission, TRUE);
			}
			$config['upload_path'] = $dir . '/';
			$config['allowed_types'] = implode('|', json_decode(fileUploadTipe5));
			$config['max_size'] = fileUploadSize5;
			$config['encrypt_name'] = true;
			$this->upload->initialize($config);
			if (!$this->upload->do_upload('lampiran_bukutabungan')) {

			} else {
				$file = $this->upload->data();
				$data = array('lampiran_bukutabungan' => $config['upload_path'] . $file['file_name']);
			}
			//lampiran_pernyataan
			$vendor_dir = 'assets/upload/MT/' . $param;
			if (!is_dir($vendor_dir)) {
				mkdir($vendor_dir, folderUploadPermission, TRUE);
			}
			$dir = 'assets/upload/MT/' . $param . '/Pernyataan';
			if (!is_dir($dir)) {
				mkdir($dir, folderUploadPermission, TRUE);
			}
			$config['upload_path'] = $dir . '/';
			$config['allowed_types'] = implode('|', json_decode(fileUploadTipe5));
			$config['max_size'] = fileUploadSize5;
			$config['encrypt_name'] = true;
			$this->upload->initialize($config);
			if (!$this->upload->do_upload('lampiran_pernyataan')) {

			} else {
				$file = $this->upload->data();
				$data = array('lampiran_pernyataan' => $config['upload_path'] . $file['file_name']);
			}
			$result = $this->{$this->models[0]}->update_file($param, $data);
			if ($result == 1) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => $result)));
			}
		}

		function get_file($url) {
			$this->load->helper('directory');
			$url_ok = str_replace("-", "/", $url);
			$map = directory_map($url_ok, FALSE, TRUE);
			// echo var_dump($map);

			$this->output->set_content_type('application/json')->set_output(json_encode($map));

		}

		function upload($id_mitra, $folder) {
			$param = $id_mitra;
			$vendor_dir = 'assets/upload/MT/' . $param;
			if (!is_dir($vendor_dir)) {
				mkdir($vendor_dir, folderUploadPermission, TRUE);
			}
			$dir = 'assets/upload/MT/' . $param . '/' . $folder;
			if (!is_dir($dir)) {
				mkdir($dir, folderUploadPermission, TRUE);
			}

			$config['upload_path'] = $dir . '/';
			$config['allowed_types'] = implode('|', json_decode(fileUploadTipe5));
			$config['max_size'] = fileUploadSize5;
			$config['encrypt_name'] = true;
			$this->upload->initialize($config);
			//ktp_no
			if (!$this->upload->do_upload($folder)) {

			} else {
				$file = $this->upload->data();
				$data = array($folder => $config['upload_path'] . $file['file_name']);
				$where = array('id_mitra' => $param);
				$this->db->update('tmitra', $data, $where);
			}
		}

	}
