<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');
	class Persetujuan_pinjaman extends MY_Controller {
		public $models = array('persetujuan_pinjaman');
		
		public function __construct() {
			parent::__construct();
		}

		public function index() {
			$data = array();
			$data['menu'] = $this->model_menu->getAllMenu();

			$this->template->load('template', 'view_persetujuan_pinjaman', $data);
		}

		public function read() {
			$this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read());
		}

		public function read_all() {
			$this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read_all());
		}

		public function create() {
			//$param = $this->input->post('param'); // parametter key
			$this->data_add['id_mitra'] = $this->input->post('id_mitra');
			// $id_mitra=
			// additional block
			// $this->data_add['tanggal_input'] = date('Y-m-d');

			// addtional get
			$check = json_decode($this->{$this->models[0]}->getOtherData('tmitra', $this->data_add), true);
			if ($check['status'] == '6') {
				$result = $this->{$this->models[0]}->updateOtherData('tmitra', $this->data_add, array('status' => '7'));
			if($this->input->post('keputusan') == 'Disetujui'){
				$msg = 'Data Berhasil Disimpan';
				$result = $this->{$this->models[0]}->insert($this->data_add);	
			} else {
				$filter = array('id_mitra' => $this->input->post('id_mitra'));
				$data = array('status' => 4, 'tgl_ditolak' => $this->input->post('tanggal_disetujui'), 'ket_ditolak' => $this->input->post('ket_ditolak'));
				$this->{$this->models[0]}->updateStatusMitra($filter, $data);
				$result = 1;
				$this->{$this->models[0]}->deleteUkurNilai($filter);
				$msg = 'Status Pengajuan Ditolak, Data Akan kembali Ke Proses Evaluasi Penilaian';
			}
			
			// $where = array('id_mitra' => $id_mitra);

			// $data = array('status' => 'persetujuan');

			// $result = $this->{$this->models[0]}->update('tmitra', $where, $data);
			if ($result == 1) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true, 'msg' => $msg)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => $this->db->_error_message())));
			}
		}
}
		public function update() {
			$param = $this->uri->segment(4); // parameter key

			// additional block

			// addtional data

			// additional where
			$this->where_add['id_persetujuan'] = $param;

			$result = $this->{$this->models[0]}->update($this->where_add, $this->data_add);
			if ($result == 1) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'Data Gagal Di Update !')));
			}
		}

		public function delete() {
			$param = $this->uri->segment(4); // parameter key

			// additional block

			// additional where
			$this->where_add['id_persetujuan'] = $param;

			$result = $this->{$this->models[0]}->delete($this->where_add);
			if ($result == 1) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'Data Gagal Di Hapus !')));
			}
		}

		public function instansi(){
			header("Content-Type: application/json");
			$cari = $this->input->post('q') ? $this->input->post('q') : '';
			$this->{$this->models[0]}->instansi();
		}
		public function dana(){
			header("Content-Type: application/json");
			$cari = $this->input->post('q') ? $this->input->post('q') : '';
			$this->{$this->models[0]}->dana();
		}

		public function getDataMitra() {
			$filter = array('A.status' => '6');
			$this->output->set_content_type('application/json')->set_output(json_encode($this->{$this->models[0]}->getDataMitra($filter)));
		}

		public function session(){
			$data = $this->session->userdata();
			$this->output->set_content_type('application/json')->set_output(json_encode($data));
		}

		public function diketahui($id_mitra){

			$session = $this->session->userdata('username');

			$data = array('diketahui' => $session);
			
			$res = $this->db->update('tpersetujuan_mitra', $data, array('id_mitra' => $id_mitra));
			$this->output->set_content_type('application/json')->set_output(json_encode($res));

		}

}

