<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	Class Model_persetujuan_pinjaman extends MY_Model {
		function __construct() {
			parent::__construct();
			$this->_table = "tpersetujuan_mitra"; // for insert, update, delete
			$this->_view = "tpersetujuan_mitra_vd"; // for call view
			$this->_order = 'asc';
			$this->_sort = 'id_mitra';
			$this->_page = 1;
			$this->_rows = 10;

			$this->_create = true;
			$this->_update = true;

			if ($this->uri->segment(4)) {
				$this->_filter = array('id_persetujuan' => $this->uri->segment(4));
			}

			//parameter from post/get - search function
			if ($this->input->post('q')) {
				$this->_like = array(
					'col1' => $this->input->post('q'),
					'col2' => $this->input->post('q'),
					'col3' => $this->input->post('q')
				);
			}

			$this->_param = array('id_mitra' => $this->input->post('id_mitra'));

			//data array for input to database
			$this->_data = array(
				// 'id_persetujuan'=> $this->input->post('id_persetujuan'),
				'id_mitra'=> $this->input->post('id_mitra'),
				'tanggal_disetujui'=> $this->input->post('tanggal_disetujui'),
				'nilai_pengajuan'=> $this->input->post('nilai_pengajuan'),
				'nilai_rekomendasi_survey'=> $this->input->post('nilai_rekomendasi_survey'),
				'nilai_rekomendasi_pengajuan'=> $this->input->post('nilai_rekomendasi_pengajuan'),
				//'nilai_disetujui'=> $this->input->post('nilai_disetujui'),
				'keputusan'=> $this->input->post('keputusan'),
				'id_sdana'=> $this->input->post('id_sdana'),
				'keteranganpemberian'=> $this->input->post('keteranganpemberian')
			);
			
		}

		public function instansi(){
			$this->db->select('*');
			// $this->db->like('CONCAT(id_mitra,nama_perusahaan)', $cari);
			$this->db->where('status','4');
			$data = $this->db->get('tmitra')->result();
			$this->output->set_output(json_encode($data));
			// echo $this->db->last_query();
		}

		public function dana(){
			$this->db->select('*');
			$data = $this->db->get('mas_sdana')->result();
			$this->output->set_output(json_encode($data));
			// echo $this->db->last_query();
		}

		public function getDataMitra($filter)
		{
			$this->db->select('A.*');
			$this->db->from('datamitra_vd A');
			$this->db->where($filter);

			$query = $this->db->get();

			return $query->result_array();
		}

		public function updateStatusMitra($filter, $data)
		{
			$this->db->where($filter);
			$this->db->update('tmitra', $data);
		}

		public function deleteUkurNilai($filter)
		{
			$this->db->where($filter);
			$this->db->delete('t_ukur_nilai');
		}

		// 

	}