<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	Class Model_datamitra extends MY_Model {
		function __construct() {
			parent::__construct();
			$this->_table = "tmitra"; // for insert, update, delete
			$this->_view = "datamitra_vd"; // for call view
			$this->_order = 'desc';
			$this->_sort = 'id_mitra';
			$this->_page = 1;
			$this->_rows = 10;

			$this->_create = true;
			$this->_update = true;

			if ($this->uri->segment(4)) {
				$this->_filter = array('nama' => $this->uri->segment(4));
			}

			//parameter from post/get - search function
			if ($this->input->post('q')) {
				$this->_like = array(
					'id_mitra' => $this->input->post('q'),
					'nama' => $this->input->post('q'),
					'no_ktp' => $this->input->post('q')
				);
			}

			$this->_param = array('id_mitra' => $this->input->post('id_mitra'));

			//data array for input to database
			$nama=strtoupper($this->input->post('nama'));
			$nama_perusahaan=strtoupper($this->input->post('nama_perusahaan'));
			$nama_ibu=strtoupper($this->input->post('nama_ibu'));
			$email=strtolower($this->input->post('email'));
			$this->_data = array(
				'nama' => $nama,
				'nama_perusahaan' =>$nama_perusahaan,
				'no_ktp' => $this->input->post('no_ktp'),
				'tempat_lahir' => $this->input->post('tempat_lahir'),
				'tgl_lahir' => $this->input->post('tgl_lahir'),
				'nama_ibu' => $nama_ibu,
				'nilai_pengajuan' => $this->input->post('nilai_pengajuan'),
				'jaminan' => $this->input->post('jaminan'),
				'jangka_waktu' => $this->input->post('jangka_waktu'),
				'sektor_usaha' => $this->input->post('sektor_usaha'),
				'alamat' => $this->input->post('alamat'),
				'propinsi' => $this->input->post('propinsi'),
				'kota' => $this->input->post('kota'),
				'kecamatan' => $this->input->post('kecamatan'),
				'kelurahan' => $this->input->post('kelurahan'),
				'kode_pos' => $this->input->post('kode_pos'),
				'status_rumah' => $this->input->post('status_rumah'),
				'telepon' => $this->input->post('telepon'),
				'handphone' => $this->input->post('handphone'),
				'email' => $email,
				'status_nikah' => $this->input->post('status_nikah'),
				'status_pendidikan' => $this->input->post('status_pendidikan'),
				'tgl_berdiri'=>$this->input->post('tgl_berdiri'),
				'keterangan'=>$this->input->post('keterangan'),
				'detail_usaha'=>$this->input->post('detail_usaha'),
				'alamat_perusahaan'=>$this->input->post('alamat_perusahaan'),
				'propinsi_perusahaan'=>$this->input->post('propinsi_perusahaan'),
				'kota_perusahaan'=>$this->input->post('kota_perusahaan'),
				'kecamatan_perusahaan'=>$this->input->post('kecamatan_perusahaan'),
				'kelurahan_perusahaan'=>$this->input->post('kelurahan_perusahaan'),
				'telepon_perusahaan'=>$this->input->post('telepon_perusahaan'),
                'status_tempat'=>$this->input->post('status_tempat_usaha'),
                'mulai_usaha'=>$this->input->post('tgl_berdiri'),
                'mampu_bayar'=>$this->input->post('kemampuan_membayar'),
                'tenagakerja'=>$this->input->post('jumlah_tenaga_kerja'),
                'bank' =>$this->input->post('bank'),
                'no_rek' =>$this->input->post('no_rek'),
                'tahun_bpkb' =>$this->input->post('thn_bpkb'),
                 // sumiyati 09 Sep 17
                 'pemilik_jaminan'=>$this->input->post('pemilik_jaminan'),
                'umur_pemilik'=>$this->input->post('umur_pemilik'),
                'alamat_pemilik'=>$this->input->post('alamat_pemilik'),
                'detail_jaminan'=>$this->input->post('detail_jaminan'),
                'status_jaminan'=>$this->input->post('status_jaminan'),
                'ahliwaris_jaminan'=>$this->input->post('ahliwaris_jaminan'),
                'komoditi'=>$this->input->post('komoditi')
			);

		}

		function get_idmitra(){
        $q = $this->db->query("SELECT LPAD(RIGHT(id_mitra,4)+1,4,0) as id_mitra FROM tmitra WHERE id_mitra LIKE DATE_FORMAT(NOW(),'KM%Y%') ORDER BY id_mitra DESC limit 1");
        return $q;
        }

		public function getsektormitra($filter)
		{
			$this->db->select('A.*');
			$this->db->from('sektor_kemitraan A');
			//$this->db->where($filter);

			$query = $this->db->get();

			return $query->result_array();
		}
		public function getpendidikan($filter)
		{
			$this->db->select('A.*');
			$this->db->from('mas_pendidikan A');
			//$this->db->where($filter);

			$query = $this->db->get();

			return $query->result_array();
		}

		public function getjaminan($filter)
		{
			$this->db->select('A.*');
			$this->db->from('mas_jaminan A');
			//$this->db->where($filter);

			$query = $this->db->get();

			return $query->result_array();
		}
        public function getbank($filter = null)
        {
            $this->db->like('CONCAT( (name),(code)  )', $filter );
            $query = $this->db->get('mas_bank')->result_array();

            return $query;
        }

		public function getLokasi()
		{
			$this->db->select('A.id, A.name namaKecamatan, B.name namaKota, C.name namaPropinsi');
			$this->db->from('districts A');
			$this->db->join('regencies B', 'B.id = A.regency_id');
			$this->db->join('provinces C', 'C.id = B.province_id');
			//$this->db->where($filter);

			$query = $this->db->get();

			return $query->result_array();
		}

		public function getPropinsi($filter)
		{
			$this->db->select('A.*');
			$this->db->from('provinces A');
			// $this->db->where('id','36');
			// $this->db->or_where('id','32');
			$this->db->where($filter);

			$query = $this->db->get();

			return $query->result_array();
		}

		public function getKota($filter)
		{
			$this->db->select('A.*');
			$this->db->from('regencies A');
			$this->db->where($filter);

			$query = $this->db->get();

			return $query->result_array();
		}

		public function getKecamatan($filter)
		{
			$this->db->select('A.*');
			$this->db->from('districts A');
			$this->db->where($filter);

			$query = $this->db->get();

			return $query->result_array();
		}

		public function getKelurahan($filter)
		{
			$this->db->select('A.*');
			$this->db->from('villages A');
			$this->db->where($filter);

			$query = $this->db->get();

			return $query->result_array();
		}
		public function permohonan(){
			$param = $this->uri->segment(4);
			$this->db->where("id_mitra", $param);
			$vendor = $this->db->get("permohonan_mitra_vd");
			return $vendor;
		}
// DISPOSISI START -------------------------------------------------------------------------
		public function getPegawai()
		{
			$this->db->select('*');
			$this->db->like( 'idPegawai',$this->input->post('q'));
			$this->db->or_like( 'nama_pegawai',$this->input->post('q'));
			$query = $this->db->get('mas_approval_vd');
			return $query->result_array();
		}
		public function getPesan()
		{
			$this->db->select('*');
			$query = $this->db->get('mas_pesan_disposisi');
			return $query->result_array();
		}
		public function disposisi_affterProses(){
			$id_doc = $this->uri->segment(4);
			// $ses_id = $this->session->userdata('idPegawai');

			// $this->db->where('node_menu',22);
			$this->db->where('id_doc',$id_doc);
			//$data = $this->db->get('disposisi_transaksi_vd')->result_array();
			 $data = $this->db->get('disposisi_pesan_vd')->result_array();

			return $data;

		}
		public function disposisi_selesai($tujuan,$id_doc, $data){
			$ses_id = $this->session->userdata('idPegawai');

			$this->db->select('nip_tujuan');
			$this->db->where('id_doc',$id_doc);
			$this->db->where('nip_tujuan',$tujuan);
			$nip = $this->db->get('disposisi_transaksi')->result_array();
			$nip_tujuan = $nip[0]['nip_tujuan'];

			if($nip_tujuan == $ses_id){
					$this->db->where('id_doc',$id_doc);
					$this->db->where('nip_tujuan',$ses_id);
					$q	= $this->db->update('disposisi_transaksi', $data);

					if ($q)
					{
						$this->output->set_output(json_encode(array('success'=>TRUE)));
					}
					else
					{
						$this->output->set_output(json_encode(array('msg'=>'Gagal update disposisi selesai')));
					}
			}else{
				$this->output->set_output(json_encode(array('msg'=>'User yang Aktif tidak Bisa Melakukan Approve')));
			}

		}
		public function get_mitra(){

			$order = $this->input->post('order') ? $this->input->post('order') : 'desc';
			$sort = $this->input->post('sort') ? $this->input->post('sort') : 'id_mitra';
			$page = $this->input->post('page') ? intval($this->input->post('page')) : 1;
			$rows = $this->input->post('rows') ? intval($this->input->post('rows')) : 10;
			$like = $this->input->post('q') ? $this->input->post('q') : '';
			$offset = (($page - 1) * $rows);

			$id_peg = $this->session->userdata('idPegawai');
			$allow_data = $this->get_userAllow($id_peg);
			// echo var_dump($allow_data);
			if($allow_data){
				// $this->db->group_by("id_mitra");
				$this->db->or_like('CONCAT(id_mitra,nama,no_ktp)', $like);
				$json['total'] = $this->db->get('datamitra_vd')->num_rows();

				$this->db->or_like('CONCAT(id_mitra,nama,no_ktp)', $like);
				$this->db->order_by($sort, $order);
				$this->db->limit($rows, $offset);

				// $this->db->group_by("id_mitra");
				$json['rows'] = $this->db->get("datamitra_vd")->result_array();
				return $json;
			}else{

				$this->db->where("creator_dis", $id_peg);
				$this->db->or_where("nip_tujuan", $id_peg);
				$this->db->or_like('CONCAT(id_mitra,nama,no_ktp)', $like);
				// $this->db->group_by("id_mitra");
				$json['total'] = $this->db->get('datamitra_vd')->num_rows();


				$this->db->order_by($sort, $order);
				$this->db->limit($rows, $offset);

				$this->db->where("creator_dis", $id_peg);
				$this->db->or_where("nip_tujuan", $id_peg);
				$this->db->or_like('CONCAT(id_mitra,nama,no_ktp)', $like);
				// $this->db->group_by("id_mitra");
				$json['rows'] = $this->db->get("datamitra_vd")->result_array();
				return $json;
			}
		}

		 private function get_userAllow( $idpegawai = ''){
			$this->db->where('idpegawai',$idpegawai);
			$this->db->where('nodemenu',22);
			$this->db->where('allow',1);
			$data = $this->db->get('t_allowdata')->row_array();
			$allow = $data['idpegawai'];
			return $allow;
		}
// DISPOSISI END -------------------------------------------------------------------------
		function update_file($id,$data)
    		{
		        $this->db->where('id_mitra',$id);
		        $update = $this->db->update($this->_table,$data);
		        if($update){
		            $hasil=1;
		            return $hasil;
		        }else{
		            $hasil=0;
		            return $hasil;
		        }

    		}
		public function lampiran()
		{
			$this->db->select('lampiran_ktp,
lampiran_kk,
lampiran_paspoto,
lampiran_suratusaha,
lampiran_keterangantinggal,
lampiran_tempatusaha,
lampiran_produk,
lampiran_kegiatanusaha,
lampiran_bukutabungan,
lampiran_akte,
lampiran_npwp
');
			$this->db->from('tmitra A');
			$this->db->where('id_mitra','KM2017040001');

			$query = $this->db->get();

			return $query->result_array();
		}
	}
