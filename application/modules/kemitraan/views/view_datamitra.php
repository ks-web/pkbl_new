<?php
$app = 'Data Mitra'; // nama aplikasi
$module = 'kemitraan';
$appLink = 'datamitra'; // controller
$idField  = 'id_mitra'; //field key table
?>

<script>
	var url;
	var app = "<?=$app?>";
	var appLink = '<?=$appLink?>';
	var module = '<?=$module?>';
	var idField = '<?=$idField?>';

    //file
	var file_type = JSON.parse('<?=fileUploadTipe5?>');
	var file_upload = true;

	function validate_file(obj){
        var file_name = $(obj).val().replace('C:\\fakepath\\', '');
        var file_name_attr = file_name.split('.');
        file_name_attr[2] = obj.files[0].size/1024;


        if(file_type.indexOf(file_name_attr[1]) == -1 || (file_name_attr[2] > <?=fileUploadSize5?>)){
        	$.messager.alert('Error Message', 'File upload harus (' + file_type.join('|') + ') dan size dibawah <?=fileUploadSize5?>KB', 'error');
        	$(obj).wrap('<form>').closest('form').get(0).reset();
        	$(obj).unwrap();
        }
    }
    //


    function add(){
        $('#dlg').dialog('open').dialog('setTitle','Tambah ' + app);
		$('#fm').form('clear');
		$('.edit').hide();
	    $('#tbl').html('Save');
	    // file_upload=true;
		url = '<?=base_url($module . '/' . $appLink . '/create')?>';
        $('#tanya').show();
        var today = "<?php echo date('Y-m-d')?>";
        $('#tgl_berdiri').datebox('setValue', today);
        $('#tgl_lahir').datebox('setValue', today);
        //$('#waktu_mualai_usaha').datebox('setValue', today);
        $('#thn_bpkb').datebox('setValue', today);
        $('#no_ktp').numberbox('setValue',0);
        $('#nilai_pengajuan').numberbox('setValue',0);
        $('#jangka_waktu').numberbox('setValue',0);
        $('#kemampuan_membayar').numberbox('setValue',0);
        $('#jumlah_tenaga_kerja').numberbox('setValue',0);
        $('#nama').focus();
        $('#tahun_bpkb').hide();
    }
    function edit(){
        var row = $('#dg').datagrid('getSelected');
		if (row){
            $('#tahun_bpkb').show();
			$('#tbl').html('Edit');
            $('#dlg').dialog('open').dialog('setTitle', 'Edit '+ app);
            $('.edit').hide();
			$('#fm').form('clear');
			$('#fm').form('load', {

				id_mitra:row.id_mitra,
				nama:row.nama,
				nama_perusahaan:row.nama_perusahaan,
				no_ktp:row.no_ktp,
				tempat_lahir:row.tempat_lahir,
				tgl_lahir:row.tgl_lahir,
                tgl_berdiri:row.tgl_berdiri,
				nama_ibu:row.nama_ibu,
				nilai_pengajuan:row.nilai_pengajuan,
				jaminan:row.jaminan,
				jangka_waktu:row.jangka_waktu,
				sektor_usaha:row.sektor_usaha,
				komoditi:row.komoditi,
				alamat:row.alamat,
				propinsi:row.propinsi,
				kota:row.kota,
                keterangan:row.keterangan,
				kecamatan:row.kecamatan,
				kelurahan:row.kelurahan,
				kode_pos:row.kode_pos,
				status_rumah:row.status_rumah,
				telepon:row.telepon,
				handphone:row.handphone,
				email:row.email,
				status_nikah:row.status_nikah,
				status_pendidikan:row.status_pendidikan,

				detail_usaha:row.detail_usaha,
                alamat_perusahaan:row.alamat_perusahaan,
                propinsi_perusahaan:row.propinsi_perusahaan,
                kota_perusahaan:row.kota_perusahaan,
                kecamatan_perusahaan:row.kecamatan_perusahaan,
                kelurahan_perusahaan:row.kelurahan_perusahaan,
                telepon_perusahaan:row.telepon_perusahaan,

                status_tempat_usaha:row.status_tempat,
                waktu_mualai_usaha:row.mulai_usaha,
                kemampuan_membayar:row.mampu_bayar,
                jumlah_tenaga_kerja:row.tenagakerja,
                bank:row.bank,
                no_rek:row.no_rek,
                thn_bpkb:row.tahun_bpkb,

                // sumiyati 9 Sep 17
                pemilik_jaminan:row.pemilik_jaminan,
                umur_pemilik:row.umur_pemilik,
                alamat_pemilik:row.alamat_pemilik,
                detail_jaminan:row.detail_jaminan,
                status_jaminan:row.status_jaminan,
                ahliwaris_jaminan:row.ahliwaris_jaminan
			});
            if(row.status_jaminan==1){
                $('#pem_jaminan').show();
                $('#almt_pemilik').show();                
                $('#umur').show();
                $('#ahliwaris').show();
                }
                else{
                $('#pem_jaminan').hide();
                $('#almt_pemilik').hide();
                $('#umur').hide();
                $('#ahliwaris').hide();
            }
            var jaminan = $('#jaminan').datebox('getValue');
            if(jaminan == '1' || jaminan == '2'){
                $('#tahun_bpkb').show();
            }else{
                $('#tahun_bpkb').hide();
            }
			url = '<?=base_url($module . '/' . $appLink . '/update')?>/'+row[idField];
	    }else{
	        $.messager.alert('Data Mita','Minimal Pilih Satu Data','error');
	    }
        $('#tanya').hide();
    }
    function hapus(){
        var row = $('#dg').datagrid('getSelected');
	    if (row){
			$.messager.confirm('Confirm','Apakah Anda Yakin Akan Menghapus ini?',function(r){
				if (r){
					$.post('<?=base_url($module . '/' . $appLink . '/delete')?>/'+row[idField],function(result){
						if (result.success){
							$('#dg').datagrid('reload');	// reload the user data
						} else {
							$.messager.show({	// show error message
								title: 'Error',
								msg:result.msg
							});
						}
					},'json');
				}
			});
		}else{
		    $.messager.alert('Data Mita','Minimal Pilih Satu Data','error');
		}
    }

    function preview(){
        var row = $('#dg').datagrid('getSelected');

        if (row)
        {
            url  = '<?=base_url($module . '/' . $appLink . '/permohonan')?>/'+row[idField];
            window.open(url,'_blank');
        }else{
            $.messager.alert('Data Mita','Minimal Pilih Satu Data','error');
        }
    }

    function save(){
        var date =  new Date( $('#thn_bpkb').datebox('getValue') );
        var today = new Date("<?php echo date('Y-m-d')?>");
        var t = (today-date)/1000;
        var lama = Math.floor(t/(86400*30*12) );
        if (lama <= 5 || $('#thn_bpkb').hide() ){
                $('#fm').form('submit',{
                url: url,
                // onSubmit: function(){
                //  return $(this).form('validate');
                // },
                onSubmit: function() {
                    // return $(this).form('validate');
                    var ret = $(this).form('validate');

                    if(!ret){
                        $.messager.alert('Error Message', 'Mohon lengkapi data', 'error');
                    }
                    return ret;

                },
                success: function(result){
                    var result = eval('('+result+')');
                    if (result.success){
                        $('#dlg').dialog('close');
                        $('#dg').datagrid('reload');

                    } else {
                     $.messager.alert('Error Message',result.msg,'error');
                    }
                }
            });
        }else{
            $.messager.alert('Error Message','BPKB Lebih Dari 5 Tahun','error');
        }


	}


    // view file

    function lampiran_ktp(value,row,index){


        var ktp = ''
        if(value != '') {
            ktp = '<a onclick="$(\'#frem\').dialog(\'open\');$(\'#frem iframe\').attr(\'src\',\'<?=base_url()?>'+row.lampiran_ktp+'\')" class="btn btn-small btn-default"><i class="icon-search"></i></a>';
        }

        return ktp;
    }
    function lampiran_paspoto(value,row,index){


        var photo = ''
        if(value != '') {
            photo = '<a onclick="$(\'#frem\').dialog(\'open\');$(\'#frem iframe\').attr(\'src\',\'<?=base_url()?>'+row.lampiran_paspoto+'\')" class="btn btn-small btn-default"><i class="icon-search"></i></a>';
        }

        return photo;
    }
    function lampiran_paspoto_ahli_waris(value,row,index){


        var photo = ''
        if(value != '') {
            photo = '<a onclick="$(\'#frem\').dialog(\'open\');$(\'#frem iframe\').attr(\'src\',\'<?=base_url()?>'+row.lampiran_paspoto_ahli_waris+'\')" class="btn btn-small btn-default"><i class="icon-search"></i></a>';
        }

        return photo;
    }
    function lampiran_suratusaha(value,row,index){


        var surat_usaha = ''
        if(value != '') {
            surat_usaha = '<a onclick="$(\'#frem\').dialog(\'open\');$(\'#frem iframe\').attr(\'src\',\'<?=base_url()?>'+row.lampiran_suratusaha+'\')" class="btn btn-small btn-default"><i class="icon-search"></i></a>';
        }

        return surat_usaha;
    }
    function lampiran_produk(value,row,index){


        var produk = ''
        if(value != '') {
            produk = '<a onclick="$(\'#frem\').dialog(\'open\');$(\'#frem iframe\').attr(\'src\',\'<?=base_url()?>'+row.lampiran_produk+'\')" class="btn btn-small btn-default"><i class="icon-search"></i></a>';
        }

        return produk;
    }

    //end view file
	function doSearch(value){
	    $('#dg').datagrid('load',{
	    	q:value
	    });
	}

    function upload_lampiran() {
        $('#fm_upload_file').form('submit',{
            url: '<?=base_url($module . '/' . $appLink . '/upload_lampiran')?>',
            success: function(result){
                var result = eval('('+result+')');
                if (result.success){
                    $('#dlg_upload').dialog('close');
                    $('#dg').datagrid('reload');
                } else {
                    $.messager.alert('Error Message',result.msg,'error');
                }
            }
        });
    }

    function upload_file_all(){
        var row = $('#dg').datagrid('getSelected');
        if (row){
          $('#dlg_upload_all').dialog('open').dialog('setTitle','Upload File');
          $('#fm_upload_file_all').form('clear');
          $('.edit').hide();
          file_upload=false;
          //ktp
            if(row.lampiran_ktp != ''){
                var lampiran_ktp = '<?=base_url()?>'+row.lampiran_ktp;
                $('#current_ktp').attr('href', lampiran_ktp).show();
                $('#current_ktp_inp').attr('value', row.lampiran_ktp);
            }

            //kk
           if(row.lampiran_kk != ''){
                var lampiran_kk = '<?=base_url()?>'+row.lampiran_kk;
                $('#current_kk').attr('href', lampiran_kk).show();
                $('#current_kk_inp').attr('value', row.lampiran_kk);
            }

              //paspoto
           if(row.lampiran_paspoto != ''){
                var lampiran_paspoto = '<?=base_url()?>'+row.lampiran_paspoto;
                $('#current_foto').attr('href', lampiran_paspoto).show();
                $('#current_foto_inp').attr('value', row.lampiran_paspoto);
            }

            //paspoto ahli waris
           if(row.lampiran_paspoto_ahli_waris != ''){
                var lampiran_paspoto_ahli_waris = '<?=base_url()?>'+row.lampiran_paspoto_ahli_waris;
                $('#current_ahli_waris').attr('href', lampiran_paspoto_ahli_waris).show();
                $('#current_ahli_waris_inp').attr('value', row.lampiran_paspoto_ahli_waris);
            }
               //surat usaha
           if(row.lampiran_suratusaha != ''){
                var lampiran_suratusaha = '<?=base_url()?>'+row.lampiran_suratusaha;
                $('#current_surtusaha').attr('href', lampiran_suratusaha).show();
                $('#current_surtusaha_inp').attr('value', row.lampiran_suratusaha);
            }
                //tempat usaha
           if(row.lampiran_tempatusaha != ''){
                var lampiran_tempatusaha = '<?=base_url()?>'+row.lampiran_tempatusaha;
                $('#current_temptusaha').attr('href', lampiran_tempatusaha).show();
                $('#current_temptusaha_inp').attr('value', row.lampiran_tempatusaha);
            }
            //keterangan tinggal
            if(row.lampiran_keterangantinggal != ''){
                var lampiran_keterangantinggal = '<?=base_url()?>'+row.lampiran_keterangantinggal;
                $('#current_kettinggal').attr('href', lampiran_keterangantinggal).show();
                $('#current_kettinggal_inp').attr('value', row.lampiran_keterangantinggal);
            }
            //lampiran_bukutabungan
            if(row.lampiran_bukutabungan != ''){
                var lampiran_bukutabungan = '<?=base_url()?>'+row.lampiran_bukutabungan;
                $('#current_btabungan').attr('href', lampiran_bukutabungan).show();
                $('#current_btabungan_inp').attr('value', row.lampiran_bukutabungan);
            }
            //lampiran_kegiatanusaha
            if(row.lampiran_kegiatanusaha != ''){
                var lampiran_kegiatanusaha = '<?=base_url()?>'+row.lampiran_kegiatanusaha;
                $('#current_kegiatanprod').attr('href', lampiran_kegiatanusaha).show();
                $('#current_kegiatanprod_inp').attr('value', row.lampiran_kegiatanusaha);
            }
            //lampiran_produk
            if(row.lampiran_produk != ''){
                var lampiran_produk = '<?=base_url()?>'+row.lampiran_produk;
                $('#current_produk').attr('href', lampiran_produk).show();
                $('#current_produk_inp').attr('value', row.lampiran_produk);
            }
            //lampiran_pernyataan
             if(row.lampiran_pernyataan != ''){
                var lampiran_pernyataan = '<?=base_url()?>'+row.lampiran_pernyataan;
                $('#current_pernyataan').attr('href', lampiran_pernyataan).show();
                $('#current_pernyataan_inp').attr('value', row.lampiran_pernyataan);
            }

            url = '<?=base_url($module . '/' . $appLink . '/upload_lampiran_all')?>/'+row[idField];

            // up lapmiran KTP
            var url_up = 'assets-upload-MT-'+row.id_mitra+'-lampiran_ktp';
            $.get('<?php echo base_url();?>'+module+'/'+appLink+'/get_file/'+url_up,function(data){
                if(data[0]){
                    $('#upktp').hide();
                    $('#ktp').show();
                }else{
                    $('#upktp').show();
                    $('#ktp').hide();
                    $('#upktp').attr("onclick"," upload_data_lampiran( \""+row.id_mitra+"\" ,\"lampiran_ktp\" ,\"fm_ktp\" ,\"upktp\" ,\"ktp\" ) ");
                    //parameter yd dibutuhkan = id mitra,name input, id form, id btn upload, id ceklis
                }
            });

            // up lapmiran KTP
            var url_up = 'assets-upload-MT-'+row.id_mitra+'-lampiran_kk';
            $.get('<?php echo base_url();?>'+module+'/'+appLink+'/get_file/'+url_up,function(data){
                if(data[0]){
                    $('#upkk').hide();
                    $('#kk').show();
                }else{
                    $('#upkk').show();
                    $('#kk').hide();
                    $('#upkk').attr("onclick"," upload_data_lampiran( \""+row.id_mitra+"\" ,\"lampiran_kk\" ,\"fm_kk\" ,\"upkk\" ,\"kk\") ");
                    //parameter yd dibutuhkan = id mitra,name input, id form, id btn upload, id ceklis
                }
            });

            // up pasfoto
            var url_up = 'assets-upload-MT-'+row.id_mitra+'-lampiran_paspoto';
            $.get('<?php echo base_url();?>'+module+'/'+appLink+'/get_file/'+url_up,function(data){
                if(data[0]){
                    $('#upfoto').hide();
                    $('#foto').show();
                }else{
                    $('#upfoto').show();
                    $('#foto').hide();
                    $('#upfoto').attr("onclick"," upload_data_lampiran( \""+row.id_mitra+"\" ,\"lampiran_paspoto\" ,\"fm_foto\" ,\"upfoto\" ,\"foto\") ");
                    //parameter yd dibutuhkan = id mitra,name input, id form, id btn upload, id ceklis
                }
            });

            // up pasfoto ahli waris
            var url_up = 'assets-upload-MT-'+row.id_mitra+'-lampiran_paspoto_ahli_waris';
            $.get('<?php echo base_url();?>'+module+'/'+appLink+'/get_file/'+url_up,function(data){
                if(data[0]){
                    $('#upwaris').hide();
                    $('#waris').show();
                }else{
                    $('#upwaris').show();
                    $('#waris').hide();
                    $('#upwaris').attr("onclick"," upload_data_lampiran( \""+row.id_mitra+"\" ,\"lampiran_paspoto_ahli_waris\" ,\"fm_waris\",\"upwaris\",\"waris\" ) ");
                    //parameter yd dibutuhkan = id mitra,name input, id form, id btn upload, id ceklis
                }
            });

            // up pasfoto lampiran_suratusaha
            var url_up = 'assets-upload-MT-'+row.id_mitra+'-lampiran_suratusaha';
            $.get('<?php echo base_url();?>'+module+'/'+appLink+'/get_file/'+url_up,function(data){
                if(data[0]){
                    $('#upusaha').hide();
                    $('#usaha').show();
                }else{
                    $('#upusaha').show();
                    $('#usaha').hide();
                    $('#upusaha').attr("onclick"," upload_data_lampiran( \""+row.id_mitra+"\" ,\"lampiran_suratusaha\" ,\"fm_usaha\",\"upusaha\",\"usaha\" ) ");
                    //parameter yd dibutuhkan = id mitra,name input, id form, id btn upload, id ceklis
                }
            });

            // up pasfoto lampiran_tempatusaha
            var url_up = 'assets-upload-MT-'+row.id_mitra+'-lampiran_tempatusaha';
            $.get('<?php echo base_url();?>'+module+'/'+appLink+'/get_file/'+url_up,function(data){
                if(data[0]){
                    $('#upTusaha').hide();
                    $('#Tusaha').show();
                }else{
                    $('#upTusaha').show();
                    $('#Tusaha').hide();
                    $('#upTusaha').attr("onclick"," upload_data_lampiran( \""+row.id_mitra+"\" ,\"lampiran_tempatusaha\" ,\"fm_Tusaha\",\"upTusaha\",\"Tusaha\" ) ");
                    //parameter yd dibutuhkan = id mitra,name input, id form, id btn upload, id ceklis
                }
            });

            // up pasfoto lampiran_keterangantinggal
            var url_up = 'assets-upload-MT-'+row.id_mitra+'-lampiran_keterangantinggal';
            $.get('<?php echo base_url();?>'+module+'/'+appLink+'/get_file/'+url_up,function(data){
                if(data[0]){
                    $('#uptinggal').hide();
                    $('#tinggal').show();
                }else{
                    $('#uptinggal').show();
                    $('#tinggal').hide();
                    $('#uptinggal').attr("onclick"," upload_data_lampiran( \""+row.id_mitra+"\" ,\"lampiran_keterangantinggal\" ,\"fm_tinggal\",\"uptinggal\",\"tinggal\" ) ");
                    //parameter yd dibutuhkan = id mitra,name input, id form, id btn upload, id ceklis
                }
            });

            // up pasfoto lampiran_keterangantinggal
            var url_up = 'assets-upload-MT-'+row.id_mitra+'-lampiran_keterangantinggal';
            $.get('<?php echo base_url();?>'+module+'/'+appLink+'/get_file/'+url_up,function(data){
                if(data[0]){
                    $('#uptinggal').hide();
                    $('#tinggal').show();
                }else{
                    $('#uptinggal').show();
                    $('#tinggal').hide();
                    $('#uptinggal').attr("onclick"," upload_data_lampiran( \""+row.id_mitra+"\" ,\"lampiran_keterangantinggal\" ,\"fm_tinggal\",\"uptinggal\",\"tinggal\" ) ");
                    //parameter yd dibutuhkan = id mitra,name input, id form, id btn upload, id ceklis
                }
            });

            // up pasfoto lampiran_produk
            var url_up = 'assets-upload-MT-'+row.id_mitra+'-lampiran_produk';
            $.get('<?php echo base_url();?>'+module+'/'+appLink+'/get_file/'+url_up,function(data){
                if(data[0]){
                    $('#upproduk').hide();
                    $('#produk').show();
                }else{
                    $('#upproduk').show();
                    $('#produk').hide();
                    $('#upproduk').attr("onclick"," upload_data_lampiran( \""+row.id_mitra+"\" ,\"lampiran_produk\" ,\"fm_produk\",\"upproduk\",\"produk\" ) ");
                    //parameter yd dibutuhkan = id mitra,name input, id form, id btn upload, id ceklis
                }
            });

            // up pasfoto lampiran_kegiatanusaha
            var url_up = 'assets-upload-MT-'+row.id_mitra+'-lampiran_kegiatanusaha';
            $.get('<?php echo base_url();?>'+module+'/'+appLink+'/get_file/'+url_up,function(data){
                if(data[0]){
                    $('#upKusaha').hide();
                    $('#Kusaha').show();
                }else{
                    $('#upKusaha').show();
                    $('#Kusaha').hide();
                    $('#upKusaha').attr("onclick"," upload_data_lampiran( \""+row.id_mitra+"\" ,\"lampiran_kegiatanusaha\" ,\"fm_Kusaha\",\"upKusaha\",\"Kusaha\" ) ");
                    //parameter yd dibutuhkan = id mitra,name input, id form, id btn upload, id ceklis
                }
            });

            // up pasfoto lampiran_bukutabungan
            var url_up = 'assets-upload-MT-'+row.id_mitra+'-lampiran_bukutabungan';
            $.get('<?php echo base_url();?>'+module+'/'+appLink+'/get_file/'+url_up,function(data){
                if(data[0]){
                    $('#upBtabungan').hide();
                    $('#Btabungan').show();
                }else{
                    $('#upBtabungan').show();
                    $('#Btabungan').hide();
                    $('#upBtabungan').attr("onclick"," upload_data_lampiran( \""+row.id_mitra+"\" ,\"lampiran_bukutabungan\" ,\"fm_Btabungan\",\"upBtabungan\",\"Btabungan\" ) ");
                    //parameter yd dibutuhkan = id mitra,name input, id form, id btn upload, id ceklis
                }
            });

            // up pasfoto lampiran_pernyataan
            var url_up = 'assets-upload-MT-'+row.id_mitra+'-lampiran_pernyataan';
            $.get('<?php echo base_url();?>'+module+'/'+appLink+'/get_file/'+url_up,function(data){
                if(data[0]){
                    $('#uppertanyaan').hide();
                    $('#pertanyaan').show();
                }else{
                    $('#uppertanyaan').show();
                    $('#pertanyaan').hide();
                    $('#uppertanyaan').attr("onclick"," upload_data_lampiran( \""+row.id_mitra+"\" ,\"lampiran_pernyataan\" ,\"fm_pertanyaan\",\"uppertanyaan\",\"pertanyaan\" ) ");
                    //parameter yd dibutuhkan = id mitra,name input, id form, id btn upload, id ceklis
                }
            });

        }
    }

	function action(index,value) {
		// console.log(value);
        var idbl = value.id_mitra;
        var tooltip = 'Belum ada disposisi yang di-approve';
        var style = ' style="color:red" ';
        if(value.approve_disposisi > 0){
            //if(value.total_approve == value.approve_disposisi){
                tooltip = 'Sudah ada disposisi yang di-approve';
                style = ' style="color:green" ';
            //}
        }
        disposisi = '<center> <a onclick="javascript:disposisi_view(\''+idbl+'\')" title="Disposisi" class="btn btn-small btn-default easyui-tooltip"'+style+' '+tooltip+'><i class="icon-tasks icon-large"></i></a> </center>';
        return disposisi;
    }

	function disposisi_view(idbl) {
        $('#dlg_disposisi').dialog('open').dialog('setTitle','List Disposisi');
        // console.log(idbl);
        $('#id_doc').val(idbl);

        $('#dg-disposisi').datagrid({
            url:'<?=base_url($module . '/' . $appLink . '/disposisi_affterProses')?>/'+idbl,
        });
    }
	function action_disposisi(val, row, index)
    {
        //var idUser = "<?=$this->session->userdata('idUser')?>";
        baris = $('#dg-disposisi').datagrid('getRows')[index];
        // console.log(baris.approve);
        if(baris.approve == '1')
        {
            disposisi = '<center><span style="color:green;"><i class="icon-check icon-large"></i><span><center>';
        }
        else
        {
            disposisi = '<a href="javascript:disposisi_selesai('+index+')" title="Disposisi" class="btn btn-small btn-default easyui-tooltip" ><i class="icon-ok icon-large"></i></a>';
        }
        return disposisi;
    }
    function add_disposisi(){
        $('#fm_disposisi').form('submit',{
            url: '<?=base_url($module . '/' . $appLink . '/add_disposisi')?>',
            success: function(result){
                var result = eval('('+result+')');
                if (result.success){
                    $('#fm_disposisi').form('reset');
                    $('#dg-disposisi').datagrid('reload');
                } else {
                    $.messager.alert('Error Message',result.msg,'error');
                }
            }
        });
    }
    function disposisi_selesai(index)
    {
        row = $('#dg-disposisi').datagrid('getRows')[index];
        // console.log(row.id_doc);

        $.post('<?=base_url($module . '/' . $appLink . '/disposisi_selesai')?>/'+row.nip_tujuan+'/'+row.id_doc,function(result)
        {
            if (result.success)
            {
                $.messager.show({
                    title: 'Success',
                    msg:'Disposisi sudah selesai'
                });
                $('#dg-disposisi').datagrid('reload');
            }
            else
            {
                $.messager.show({
                    title: 'Error',
                    msg:result.msg
                });
                $('#dg-disposisi').datagrid('reload');
            }
        },'json');
        $('#dg-disposisi').datagrid('reload');
    }

    function hapus_disposisi(){
        var data = $('#dg-disposisi').datagrid('getSelected');
        // console.log(data);
        $.messager.confirm('Confirm','Apakah Anda Yakin Akan Menghapus ini?',function(r){
            if (r){
                $.post('<?=base_url('kemitraan/datamitra/hapus_disposisi')?>/'+data.id_doc+'/'+data.nip_tujuan+'/'+data.creator,function(result){
                    if (result.success){
                        $.messager.show({
                            title: 'Success',
                            msg:'Hapus Disposisi Berhasil'
                        });
                        $('#dg-disposisi').datagrid('reload');
                    }else{
                        $.messager.show({
                            title: 'Error',
                            msg:result.msg
                        });
                        $('#dg-disposisi').datagrid('reload');
                    }
                },'json');
                $('#dg-disposisi').datagrid('reload');
            }
        });
    }
    
    function cetak_disposisi(){
		var id_doc = $('#id_doc').val();
		if (id_doc){
			window.open('<?=base_url($module . '/' . $appLink . '/cetak_disposisi')?>/'+id_doc, '_blank');
	    }
    }

    function survey(){
         $('#dlg_survey').dialog('open');
         $('#p').show();
         var tgl = '<?php echo date('Y-m-d'); ?>';

         $('#tgl').datebox('setValue', tgl);
    }
    function pilih_mitra(value,row,index){
        // console.log(value);
        var cek = ""
        if(value){
            cek = "checked disabled"
        }

        return "<input style='width: 20px' "+cek+" type='checkbox' id='id_mitra' name='id_mitra[]' value='"+row.id_mitra+"'/>" ;
    }
    function kirim_survey(){
       $('#fm_survey').form('submit',{
            url: "<?=base_url($module . '/' . $appLink . '/kirim_survey')?>",
            onSubmit: function() {
                return $(this).form('validate');
            },
            success: function(result){
                var result = eval('('+result+')');
                if (result.success){
                    $('#dlg').dialog('close');
                    $('#dg').datagrid('reload');
                    $('#fm_survey').form('reset');
                    $('#p').hide();

                } else {
                 $.messager.alert('Error Message',result.msg,'error');
                }
            }
        });
       // $('#p').hide();
    }
    function kirim_batal(){
          $('#fm_survey').form('reset');
         $('#p').hide();
    }

    $(document).ready(function(){
        $.post('<?=base_url('kemitraan/datamitra/get_approval')?>',function(result)
        {
            // var res = result[0].id_jabatan;
            if(result == null){
                $('#dg').datagrid('hideColumn', 'asd');
            }else{
                $('#dg').datagrid('showColumn', 'asd');
                if(result.disposisi != 1 ){
                    $('#fm_disposisi').hide();
                }else{
                    $('#fm_disposisi').show();
                }

                if (result.kirim_survey == null ) {
                    $('#dg').datagrid('hideColumn', 'pilih');
                    $('#survey').hide();
                }else{
                    $('#dg').datagrid('showColumn', 'pilih');
                    $('#survey').show();
                }

            }

        },'json');
        $('#p').hide();

        $('#ya').click(function(){
            $('#alamat_perusahaan').val($('#alamat').val());
            $('#propinsi_perusahaan').combobox('setValue',$('#propinsi').combobox('getValue'));

            $('#kota_perusahaan').combobox('setValue',$('#kota').combobox('getValue'));
            $('#kecamatan_perusahaan').combobox('setValue',$('#kecamatan').combobox('getValue'));
            $('#kelurahan_perusahaan').combobox('setValue',$('#kelurahan').combobox('getValue'));
            $('#telepon_perusahaan').val( $('#telepon').val() );
			
			$('#waktu_mualai_usaha').datebox('setValue',$('#tgl_berdiri').combobox('getValue'));
        });
        $('#tidak').click(function(){
            $('#alamat_perusahaan').val('');
            $('#propinsi_perusahaan').combobox('setValue','');

            $('#kota_perusahaan').combobox('setValue','');
            $('#kecamatan_perusahaan').combobox('setValue','');
            $('#kelurahan_perusahaan').combobox('setValue','');
            $('#telepon_perusahaan').val('');
        });
    })

    function upload_data_lampiran(id_mitra,folder,form,btn1,btn2){
        $('#'+form).form('submit',{
            url: "<?=base_url($module . '/' . $appLink . '/upload')?>/"+id_mitra+"/"+folder,
            onSubmit: function() {
                return $(this).form('validate');
            },
            success: function(result){
                var url_file = 'assets-upload-MT-'+id_mitra+'-'+folder;
                $.get('<?php echo base_url();?>'+module+'/'+appLink+'/get_file/'+url_file,function(data){
                    if(data[0]){
                        $('#'+btn1).hide();
                        $('#'+btn2).show();
                    }
                });
                $('#'+form).form('reset');
            }
        });
    }

</script>
<div class="tabs-container">
<form id="fm_survey" method="post" enctype="multipart/form-data" action=""> <!-- form Buka -->
	<table id="dg" class="easyui-datagrid"
	data-options="
	    url: '<?=base_url($module . '/' . $appLink . '/get_mitra')?>',
		singleSelect:'true',
	    title:'<?=$app?>',
	    toolbar:'#toolbar',
	    iconCls:'icon-cog',
	    rownumbers:'true',
        nowrap:'false',
	    idField:'<?=$idField?>',
	    pagination:'true',
	    pageList: [10,20,30]
	    "
	>
        <thead  data-options="frozen:true">
            <tr>
                <th field="pilih" width="50" sortable="true" formatter="pilih_mitra">Pilih</th>
                <th field="id_mitra" width="100" sortable="true">No Mitra</th>

            </tr>
        </thead>
	    <thead>
        <tr>
            <th field="nama" width="150" sortable="true">Nama Lengkap</th>
            <th field="nama_perusahaan" width="200" sortable="true">Nama Perusahaan</th>
            <th field="nama_sektor" width="150" sortable="true">Sektor Usaha</th>
            <th field="kota" width="150" sortable="true">Kota</th>
			<th field="asd" formatter="action">Disposisi</th>
            <th data-options="field:'lampiran_ktp',width:60" formatter="lampiran_ktp">Lampiran<br />KTP</th>
            <th data-options="field:'lampiran_paspoto',width:60" formatter="lampiran_paspoto">Lampiran<br />Photo</th>
            <th data-options="field:'lampiran_paspoto_ahli_waris',width:60" formatter="lampiran_paspoto_ahli_waris">Lampiran<br />Photo Ahli Waris</th>
	        <th data-options="field:'lampiran_suratusaha',width:60" formatter="lampiran_suratusaha">Lampiran<br />Surat Usaha</th>
            <th data-options="field:'lampiran_produk',width:60" formatter="lampiran_produk">Lampiran<br />Produk</th>
         </tr>
        </thead>
	</table>

    <!-- Tombol Add, Edit, Hapus Datagrid -->
    <div id="toolbar">
        <table align="center" style="padding: 0px; width: 99%;">
            <tr>
                <td>
                    <a href="javascript:void(0)" class="btn btn-small btn-success" onclick="javascript:add()"><i class="icon-plus-sign icon-large"></i>&nbsp;Tambah</a>
                    <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:edit()"><i class="icon-edit"></i>&nbsp;Ubah</a>
                    <a href="javascript:void(0)" class="btn btn-small btn-danger" onclick="javascript:hapus()"><i class="icon-trash icon-large"></i>&nbsp;Hapus</a>
                    <a href="javascript:void(0)" class="btn btn-small btn-warning" iconCls: 'icon-search' onclick="javascript:preview()"><i class="icon-print icon-large"></i>&nbsp;Cetak Seri C</a>
                    <a id="upload_file" href="javascript:void(0)" class="btn btn-small btn-success" iconCls: 'icon-search' onclick="javascript:upload_file_all()"><i class="icon-arrow-up icon-large"></i>&nbsp;Uplod File</a>

                    <a id="survey" href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:survey()"><i class="icon-bullhorn icon-large"></i>&nbsp;Survey</a>
                </td>
                <td>&nbsp;</td>
                <td align="right">
                    <input class="easyui-searchbox" prompt="Silahkan Input Field" searcher="doSearch"  style="width:300px"/>
                </td>
            </tr>
        </table>

        <div id="p" class="easyui-panel" title=" " style="width: 1000%;height:180px;">

                <table style="margin: 10px;" border="0">
                    <tr>
                        <td>Tanggal Disposisi</td>
                        <td style="width: 30px;height: 35px "></td>
                        <td> : <input type="date" id="tgl" name="tgl_survey" class="easyui-datebox" required></td>
                    </tr>

                    <tr>
                        <td>Petugas Survey</td>
                        <td style="width: 30px;height: 35px "></td>
                        <td> : <select id="tujuan_disposisi" name="petugas" class="easyui-combogrid" required style="width: 150px"
                                data-options="
                                    panelWidth:450,
                                    mode:'remote',
                                    url:'<?=base_url('kemitraan/Datamitra/getPegawai')?>',
                                    idField:'idPegawai',
                                    textField:'nama_pegawai',
                                    columns:[[
                                    {field:'idPegawai',title:'ID Pegawai',width:50},
                                    {field:'nama_pegawai',title:'Nama Pegawai',width:200},
                                    {field:'jabatan',title:'jabatan',width:180},
                                    ]]
                                    ">
                                </select>
                        </td>
                    </tr>

                    <tr>
                        <td>Keterangan</td>
                        <td style="width: 30px;height: 35px "></td>
                        <td> : <input type="text" name="ket" class="easyui-textbox"></td>
                    </tr>
                    <tr>
                        <td colspan="3">
                           <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:kirim_survey()"><i class="icon-save icon-large"></i>&nbsp;Simpan</a>
                           <a href="javascript:void(0)" class="btn btn-small btn-danger" onclick="javascript:kirim_batal()"><i class="icon-remove"></i>&nbsp;Batal</a>
                        </td>
                    </tr>
                </table>

            </form> <!-- form tutup -->
            <br/>
        </div>
    </div>
    <!-- end tombol datagrid -->


	<div class="easyui-dialog" id="frem" style="width: 500px; height: 580px;" closed="true">
     <iframe src="" style="width: 100%;height: 100%">

     </iframe>
    </div>
	<!-- Model Start -->
	<div id="dlg" class="easyui-dialog" style="width:500px; height:580px; padding:10px" closed="true" buttons="#t_dlg_dis-buttons" >
	    <form id="fm" method="post" enctype="multipart/form-data" action="">
	    	<div id="tt" class="easyui-tabs" style="width:440px;height:auto;">
		        <div title="Data Mitra" style="padding:20px;">
		            <table width="100%" align="center" border="0">
			            <tr>
			                <td>Nama Lengkap</td>
			                <td>:</td>
			                <td>
			                    <input type="text" name="nama" id="nama" required style="width: 250px;">
			                </td>
			            </tr>
			            <tr>
			                <td>Nama Perusahaan</td>
			                <td>:</td>
			                <td>
			                    <input type="text" name="nama_perusahaan" id="nama_perusahaan" required style="width: 250px;">
			                </td>
			            </tr>
			            <tr>
			                <td>No KTP</td>
			                <td>:</td>
			                <td>
			                    <input class="easyui-numberbox" type="text" name="no_ktp" id="no_ktp" required style="width: 250px;" maxlength="16">
			                </td>
			            </tr>
			            <tr>
			                <td>Tempat Lahir</td>
			                <td>:</td>
			                <td>
			                    <input type="text" name="tempat_lahir" id="tempat_lahir" required style="width: 250px;">
			                </td>
			            </tr>
			            <tr>
			                <td>Tanggal Lahir</td>
			                <td>:</td>
			                <td>
			                    <input class="easyui-datebox" label="Start Date:" labelPosition="top" name="tgl_lahir" id="tgl_lahir" required style="width: 120px;">
			                </td>
			            </tr>
			            <tr>
			                <td>Nama Ibu</td>
			                <td>:</td>
			                <td>
			                    <input type="text" name="nama_ibu" id="nama_ibu" required style="width: 250px;">
			                </td>
			            </tr>
			            <tr>
			                <td>Nilai Pengajuan</td>
			                <td>:</td>
			                <td>
			                    <input class="easyui-numberbox" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%'" name="nilai_pengajuan" id="nilai_pengajuan" required style="width: 250px;">
			                </td>
			            </tr>
			            <tr>
			                <td>Jaminan</td>
			                <td>:</td>
			                <td>
			                    <input class="easyui-combobox" name="jaminan" id="jaminan" style="width: 250px;" data-options="
				                    url:'<?= base_url($module . '/' . $appLink . '/getjaminan')?>',
				                    method:'get',
				                    required:'true',
				                    valueField:'id_jaminan',
				                    textField:'nama_jaminan',
				                    panelHeight:'auto',
				                    label: 'Language:',
				                    onSelect: function(rec){
                                        if(rec.id_jaminan == 1 || rec.id_jaminan == 2){
                                            $('#tahun_bpkb').show();
                                        }else{

                                           $('#tahun_bpkb').hide();
                                           $('#detail_jaminan').hide();
                                           $('#thn_bpkb').datebox('setValue','');

                                        }

                                    }" />
			                </td>
			            </tr>
			            <tr id="tahun_bpkb">
			                <td>Tahun BPKB</td>
			                <td>:</td>
			                <td>
			                    <input class="easyui-datebox" style="width: 100px" id="thn_bpkb" name="thn_bpkb"/><span id="msg_thn"></span>
			                </td>
			            </tr>
                        <!-- sumiyati 09 Sep 17 -->
                        <tr  id="status_jaminan">
                            <td>Status Jaminan</td>
                            <td></td>
                            <td>
                                <input class="easyui-combobox" name="status_jaminan" id="status_jaminan" data-options="
                                required:'true',
                                valueField: 'value',
                                textField: 'label',
                                data: [{
                                    label: 'Milik Sendiri',
                                    value: '0'
                                },{
                                    label: 'Orang Lain',
                                    value: '1'
                                },
                                {
                                    label: 'Tanpa Jaminan',
                                    value: '2'
                                }],
                                onSelect:function(param){
                                if(param.value == 1)
                                        {
                                           $('#pem_jaminan').show();
                                            $('#almt_pemilik').show();
                                            $('#ahliwaris').show();
                                            $('#umur').show();
												  $('#detail_jaminan').show();
                                        }else{
                                           $('#pem_jaminan').hide();
                                            $('#almt_pemilik').hide();
                                            $('#ahliwaris').hide();
                                            $('#umur').hide();
												  $('#detail_jaminan').hide();
                                            }
                                   }" />
                            </td>
                        </tr>
                        <tr id="pem_jaminan" style="display: none;">
                            <td>Pemilik Jaminan</td>
                            <td>:</td>
                            <td><input type="text" name="pemilik_jaminan" id="pemilik_jaminan"></td>
                        </tr>
                        <tr id="almt_pemilik" style="display: none;">
                            <td>Alamat </td>
                            <td>:</td>
                            <td><input type="text" name="alamat_pemilik" id="alamat_pemilik"></td>
                            
                        </tr>
                        <tr id="umur" style="display: none;">
                            <td>Umur</td>
                            <td>:</td>
                            <td><input type="text" name="umur_pemilik" id="umur_pemilik"></td>
                        </tr>
                        <tr id="detail_jaminan">
                            <td>Detail Jaminan</td>
                            <td>:</td>
                            <td><textarea name="detail_jaminan" id="detail_jaminan"></textarea></td>
                        </tr>
                        <tr id="ahliwaris" style="display: none;">
                            <td>Ahli Waris</td>
                            <td>:</td>
                            <td><input type="text" name="ahliwaris_jaminan" id="ahliwaris_jaminan"></td>
                        </tr>
                        <!-- sumiyati -->
			            <tr>
			                <td>Jangka Waktu</td>
			                <td>:</td>
			                <td>
			                    <input class="easyui-numberbox" data-options="label:'Bulan',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',suffix:' Bulan',width:'100%'" name="jangka_waktu" id="jangka_waktu" required style="width: 250px;">
			                </td>
			            </tr>

			            <tr>
			                <td>Sektor Usaha</td>
			                <td>:</td>
			                <td>
			                    <input class="easyui-combobox" name="sektor_usaha" id="sektor_usaha" style="width: 250px;" data-options="
				                    url:'<?=base_url($module . '/' . $appLink . '/getsektormitra')?>',
				                    method:'get',
				                    required:'true',
				                    valueField:'id_sektor',
				                    textField:'nama_sektor',
				                    panelHeight:'auto',
				                    label: 'Language:'">
			                </td>
			            </tr>
						<tr>
							<td>Komoditi</td>
							<td>:</td>
							<td><input type="text" name="komoditi" id="komoditi" ></td>
							
						</tr>
                        <tr>
                            <td>Mulai Kegiatan Usaha Sejak</td>
                            <td>:</td>
                            <td><input class="easyui-datebox" name="tgl_berdiri" id="tgl_berdiri"></td>
                        </tr>
                        <tr>
                            <td>Keterangan</td>
                            <td>:</td>
                            <td><input type="text" name="keterangan" id="keterangan" ><font color="red" size="1">*digunakan untuk</font></td>
                        </tr>
			        </table>
		        </div>
		        <div title="Data Pribadi" style="overflow:auto;padding:20px;">
		            <table width="100%" align="center" border="0">
		            	<tr>
			                <td>Alamat Rumah</td>
			                <td>:</td>
			                <td>
			                    <input type="text" name="alamat" id="alamat" required style="width: 250px;">
			                </td>
			            </tr>
			            <tr>
			                <td>Propinsi</td>
			                <td>:</td>
			                <td>
			                    <input class="easyui-combobox" name="propinsi" id="propinsi" required style="width: 250px;" data-options="
				                    url:'<?=base_url($module . '/' . $appLink . '/getPropinsi')?>',
				                    method:'get',
				                    valueField:'name',
				                    textField:'name',
				                    panelHeight:'auto',
				                    label: 'Language:',
				                    labelPosition: 'top',
				                    onSelect: function(param){
				                    	$('#kota').combobox('clear');
				                    	$('#kota').combobox('reload','<?=base_url($module . '/' . $appLink . '/getKota/')?>/'+param.id);
				                    }
				                ">
			                </td>
			            </tr>
			            <tr>
			                <td>Kota/Kabupaten</td>
			                <td>:</td>
			                <td>
			                    <input class="easyui-combobox" name="kota" id="kota" required style="width: 250px;" data-options="
				                    url:'<?=base_url($module . '/' . $appLink . '/getKota/')?>',
				                    method:'get',
				                    valueField:'name',
				                    textField:'name',
				                    panelHeight:'auto',
				                    label: 'Language:',
				                    labelPosition: 'top',
				                    onSelect: function(param){
				                    	$('#kecamatan').combobox('clear');
				                    	$('#kecamatan').combobox('reload','<?=base_url($module . '/' . $appLink . '/getKecamatan/')?>/'+param.id);
				                    }
				                ">
			                </td>
			            </tr>
			            <tr>
			                <td>Kecamatan</td>
			                <td>:</td>
			                <td>
			                    <input class="easyui-combobox" name="kecamatan" id="kecamatan" required style="width: 250px;" data-options="
				                    url:'<?=base_url($module . '/' . $appLink . '/getKecamatan')?>',
				                    method:'get',
				                    valueField:'name',
				                    textField:'name',
				                    panelHeight:'auto',
				                    label: 'Language:',
				                    labelPosition: 'top',
				                    onSelect: function(param){
				                    	$('#kelurahan').combobox('clear');
				                    	$('#kelurahan').combobox('reload','<?=base_url($module . '/' . $appLink . '/getKelurahan/')?>/'+param.id);
				                    }
				                ">
			                </td>
			            </tr>
			            <tr>
			                <td>Kelurahan</td>
			                <td>:</td>
			                <td>
			                    <input class="easyui-combobox" name="kelurahan" id="kelurahan" required style="width: 250px;" data-options="
				                    url:'<?=base_url($module . '/' . $appLink . '/getKelurahan')?>',
				                    method:'get',
				                    valueField:'name',
				                    textField:'name',
				                    panelHeight:'auto',
				                    label: 'Language:',
				                    labelPosition: 'top'
				                ">
			                </td>
			            </tr>
			            <tr>
			                <td>Kode POS</td>
			                <td>:</td>
			                <td>
			                    <input type="text" name="kode_pos" id="kode_pos" required style="width: 250px;" maxlength="5">
			                </td>
			            </tr>
			            <tr>
			                <td>Kepemilikan Rumah</td>
			                <td>:</td>
			                <td>
			                    <select  class="easyui-combobox" type="text" name="status_rumah" id="status_rumah" required style="width: 250px;">
			                    	<option value="Sendiri">Sendiri</option>
			                    	<option value="Orang Tua">Orang Tua</option>
			                    	<option value="Kontrak">Kontrak</option>
			                    </select>
			                </td>
			            </tr>
			            <tr>
			                <td>Telepon</td>
			                <td>:</td>
			                <td>
			                    <input type="text" name="telepon" id="telepon" required style="width: 250px;">
			                </td>
			            </tr>
			            <tr>
			                <td>Handphone</td>
			                <td>:</td>
			                <td>
			                    <input type="text" name="handphone" id="handphone" required style="width: 250px;">
			                </td>
			            </tr>
			            <tr>
			                <td>E-mail</td>
			                <td>:</td>
			                <td>
			                    <input type="text" name="email" id="email" required style="width: 250px;">
			                </td>
			            </tr>
			            <tr>
			                <td>Status Pernikahan</td>
			                <td>:</td>
			                <td>
			                    <select  class="easyui-combobox" type="text" name="status_nikah" id="status_nikah" required style="width: 250px;">
			                    	<option value="Belum">Belum Menikah</option>
			                    	<option value="Sudah">Sudah Menikah</option>
			                    </select>
			                </td>
			            </tr>
			            <tr>
			                <td>Pendidikan Terakhir</td>
			                <td>:</td>
			                <td>
			                    <input  class="easyui-combobox" name="status_pendidikan" id="status_pendidikan" required style="width: 250px;" data-options="
                                    url:'<?=base_url($module . '/' . $appLink . '/getpendidikan')?>',
                                    method:'get',
                                    required:'true',
                                    valueField:'pendidikan',
                                    textField:'pendidikan',
                                    panelHeight:'auto',
                                    label: 'Language:'">
			                </td>
			            </tr>

                        <tr>
                            <td>BANK</td>
                            <td>:</td>
                            <td>
                                <input class="easyui-combobox" name="bank" id="bank" style="width: 250px;" data-options="
                                    url:'<?=base_url($module . '/' . $appLink . '/getbank')?>',
                                    method:'get',
                                    required:'true',
                                    valueField:'name',
                                    textField:'name',
                                    panelHeight:'auto'
                                ">
                            </td>
                        </tr>
                        <tr>
                            <td>Nomor Rekening</td>
                            <td>:</td>
                            <td>
                                <input type="text" style="width: 150px" class="easyui-numberbox" name="no_rek" id="no_rek"/>
                            </td>
                        </tr>
		            </table>
		        </div>

                <div title="Data Perusahaan" style="overflow:auto;padding:20px;">
                    <span id="tanya">Alamat Perusahaan sama dengan alamat rumah ? <a href="#" id="ya">Ya</a> | <a href="#" id="tidak">Tidak <br/><br/></a></span>
                    <table id="data_perusahaan" width="100%" align="center" border="0">
                        <tr>
                            <td>Detail Usaha</td>
                            <td>:</td>
                            <td>
                                <input type="text" name="detail_usaha" id="detail_usaha" required style="width: 250px;">
                            </td>
                        </tr>
                        <tr>
                            <td>Alamat Perusahaan</td>
                            <td>:</td>
                            <td>
                                <input type="text" name="alamat_perusahaan" id="alamat_perusahaan" required style="width: 250px;">
                            </td>
                        </tr>
                        <tr>
                            <td>Propinsi</td>
                            <td>:</td>
                            <td>
                                <input class="easyui-combobox" name="propinsi_perusahaan" id="propinsi_perusahaan" required style="width: 250px;" data-options="
                                    url:'<?=base_url($module . '/' . $appLink . '/getPropinsi')?>',
                                    method:'get',
                                    valueField:'name',
                                    textField:'name',
                                    panelHeight:'auto',
                                    label: 'Language:',
                                    labelPosition: 'top',
                                    onSelect: function(param){
                                        $('#kota_perusahaan').combobox('clear');
                                        $('#kota_perusahaan').combobox('reload','<?=base_url($module . '/' . $appLink . '/getKota/')?>/'+param.id);
                                    }
                                ">
                            </td>
                        </tr>
                        <tr>
                            <td>Kota/Kabupaten</td>
                            <td>:</td>
                            <td>
                                <input class="easyui-combobox" name="kota_perusahaan" id="kota_perusahaan" required style="width: 250px;" data-options="
                                    url:'<?=base_url($module . '/' . $appLink . '/getKota/')?>',
                                    method:'get',
                                    valueField:'name',
                                    textField:'name',
                                    panelHeight:'auto',
                                    label: 'Language:',
                                    labelPosition: 'top',
                                    onSelect: function(param){
                                        $('#kecamatan_perusahaan').combobox('clear');
                                        $('#kecamatan_perusahaan').combobox('reload','<?=base_url($module . '/' . $appLink . '/getKecamatan/')?>/'+param.id);
                                    }
                                ">
                            </td>
                        </tr>
                        <tr>
                            <td>Kecamatan</td>
                            <td>:</td>
                            <td>
                                <input class="easyui-combobox" name="kecamatan_perusahaan" id="kecamatan_perusahaan" required style="width: 250px;" data-options="
                                    url:'<?=base_url($module . '/' . $appLink . '/getKecamatan')?>',
                                    method:'get',
                                    valueField:'name',
                                    textField:'name',
                                    panelHeight:'auto',
                                    label: 'Language:',
                                    labelPosition: 'top',
                                    onSelect: function(param){
                                        $('#kelurahan_perusahaan').combobox('clear');
                                        $('#kelurahan_perusahaan').combobox('reload','<?=base_url($module . '/' . $appLink . '/getKelurahan/')?>/'+param.id);
                                    }
                                ">
                            </td>
                        </tr>
                        <tr>
                            <td>Kelurahan</td>
                            <td>:</td>
                            <td>
                                <input class="easyui-combobox" name="kelurahan_perusahaan" id="kelurahan_perusahaan" required style="width: 250px;" data-options="
                                    url:'<?=base_url($module . '/' . $appLink . '/getKelurahan')?>',
                                    method:'get',
                                    valueField:'name',
                                    textField:'name',
                                    panelHeight:'auto',
                                    label: 'Language:',
                                    labelPosition: 'top'
                                ">
                            </td>
                        </tr>

                        <tr>
                            <td>Telepon/Hp</td>
                            <td>:</td>
                            <td>
                                <input type="text" name="telepon_perusahaan" id="telepon_perusahaan" required maxlength="15" required style="width: 250px;">
                            </td>
                        </tr>
                        <tr>
                            <td>Status Tempat Usaha</td>
                            <td>:</td>
                            <td>

                            <select class="easyui-combobox" name="status_tempat_usaha" required id="status_tempat_usaha" style="width: 150px">
    							<option value="Sendiri">Sendiri</option>
    							<option value="Kontrak">Kontrak</option>
    						</select>

                            </td>
                        </tr>
                        <tr>
                            <td>Waktu Mulai Usaha</td>
                            <td>:</td>
                            <td>

                            <input type="text" style="width: 150px" class="easyui-datebox" required name="waktu_mualai_usaha" id="waktu_mualai_usaha" readonly="true"/>

                            </td>
                        </tr>
                        <!-- <tr>
                            <td>Kemampuan Membayar</td>
                            <td>:</td>
                            <td>
                            <input type="text" style="width: 250px; text-align: right" class="easyui-numberbox" required name="kemampuan_membayar" id="kemampuan_membayar" data-options="min:0,precision:2,groupSeparator:',',prefix:'Rp. '"/>
                            </td>
                        </tr>
                        <tr>
                            <td>Jumlah Tenaga Kerja</td>
                            <td>:</td>
                            <td>

                            <input type="text" style="width: 150px;" class="easyui-numberbox" required name="jumlah_tenaga_kerja" id="jumlah_tenaga_kerja"/>

                            </td>
                        </tr> -->

                    </table>
                </div>
		    </div>
	    </form>

        <!-- Tombol simpan & Batal Form -->
        <div id="t_dlg_dis-buttons">
            <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:save()"><i class="icon-save icon-large"></i>&nbsp;Simpan</a>
            <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:$('#dlg').dialog('close')"><i class="icon-remove icon-large"></i>&nbsp;Batal</a>
        </div>
	</div>
	<!-- Model end -->
    <div id="dlg_upload" class="easyui-dialog" title="Upload File" data-options="iconCls:'icon-file'" closed="true" style="width:400px;height:110px;padding:10px">
        <form id="fm_upload_file" method="POST" enctype="multipart/form-data">
            <input type="hidden" name="id_mitra" id="id_mitra" />
            <!-- <input type="hidden" name="jenis" id="jenis" /> -->
            <input type="file" id="file" name="file" >
            <a href="javascript:void(0)" onclick="upload_lampiran()" class="btn btn-small btn-default">Upload</a>
        </form>
    </div>

</div>

<div id="dlg_disposisi" class="easyui-dialog" style="width:1000px; height:400px; padding:10px" closed="true" buttons="#t_dlg_dis_view-buttons" >
    <form id="fm_disposisi" method="post" enctype="multipart/form-data" action="">
        <table align="left">
			<!-- <tr>
				<td>
					Tujuan
				</td>
				<td> : </td>
                <td>
					<input type="hidden" id="id_doc" class="easyui-textbox" name="id_doc" style="width: 750px;" >
                    <select id="tujuan_disposisi" name="tujuan_disposisi" class="easyui-combogrid" style="width:215px;height:25px;"
                    data-options="
                        panelWidth:400,
                        mode:'remote',
                        url:'<?=base_url('kemitraan/Datamitra/getPegawai')?>',
                        idField:'idPegawai',
                        textField:'nama_pegawai',
                        columns:[[
                        {field:'idPegawai',title:'NIP',width:50},
                        {field:'nama_pegawai',title:'Nama Pegawai',width:150},
                        {field:'jabatan',title:'jabatan',width:180},
                        ]]
                        ">
                    </select>
                </td>
            </tr> -->

			<!-- <tr>
				<td>
					Pesan
				</td>
				<td> : </td>
				<td>
					 <input class="easyui-combobox" id="pesan_disposisi" name="pesan_disposisi" style="width:215px;height:25px;" data-options="valueField:'id',textField:'pesan_disposisi',url:'<?=base_url('kemitraan/Datamitra/getPesan')?>'">
				</td>
			</tr> -->

			<!-- <tr>
				<td>
					Catatan
				</td>
				<td> : </td>
                <td>
					<input type="text" id="catatan_disposisi" name="catatan_disposisi" style="width: 950px;">
                </td>

            </tr> -->

			<tr >
				<td></td>
				<td></td>
				<td align="left">
                    <!-- <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:add_disposisi()"><i class="icon-plus icon-large"></i>&nbsp;Tambah</a> -->
                    <!-- <a href="javascript:void(0)" class="btn btn-small btn-danger" onclick="javascript:hapus_disposisi()"><i class="icon-remove icon-large"></i>&nbsp;Hapus</a> -->
                    <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:cetak_disposisi()"><i class="icon-large"></i>&nbsp;Cetak</a>
                </td>
			</tr>
            <!--tr>
                <td>
                    <textarea placeholder="Catatan" name="catatan" id="catatan"></textarea>
                </td>
            </tr-->
        </table>
    </form>
	<br>
	<table id="dg-disposisi" class="easyui-datagrid" style="height:200px;width:1000px;"
        data-options="
        singleSelect:'true',
        title:'List',
        iconCls:'icon-tasks',
        rownumbers:'true',
        fitColumns:'true'
        "
    >
        <thead>
            <th field="id_doc" sortable="true" >ID DOC</th>
            <th field="pemberi" width="80">Pemberi Disposisi</th>
            <th field="penerima" width="80">Ditujukan Untuk</th>
            <th field="pesan_disposisi" width="100">Pesan</th>
            <th field="catatan_disposisi" width="200">Catatan</th>
            <th field="approve_date_time" width="80">Tanggal Approve</th>
            <th field="asd" width="40" formatter="action_disposisi">Action</th>
        </thead>
    </table>
</div>
<div  id="dlg_upload_all" class="easyui-dialog" style="width:600px; height:450px; padding:10px" closed="true" buttons="#t_dlg_upload_view">
    <!-- <form id='fm_upload_file_all' method="POST" enctype="multipart/form-data"> -->
        <table width="100%" align="center" border="0">

            <tr>
                <form id="fm_ktp" method="POST" enctype="multipart/form-data">
                    <td width="150px">File KTP</td>
                    <td>:</td>
                    <td>
                        <a href="" id="current_ktp" class="edit">File</a><input type="hidden" name="current_ktp" id="current_ktp_inp" />
                        <input type="file" id="lampiran_ktp" name="lampiran_ktp" size="20" onchange="validate_file(this)" /> <!-- name harus sama dengan field di database -->
                        </td>
                </form>
                    <td width="170px">
                        <span id="ktp"><i style='color:blue' class='icon-ok icon-sm'></i></span>
                        <a href="#" class="btn btn-primary" id="upktp">Up</a>
                    </td>
            </tr>

            <tr>
                <form id="fm_kk" method="POST" enctype="multipart/form-data">
                    <td width="150px">File Kartu Keluarga</td>
                    <td>:</td>
                    <td>
                        <a href="" id="current_kk" class="edit">File</a><input type="hidden" name="current_kk" id="current_kk_inp" />
                        <input type="file" id="lampiran_kk" name="lampiran_kk" size="20" onchange="validate_file(this)" />
                    </td><!-- name harus sama dengan field di database -->
                </form>
                    <td width="170px">
                        <span id="kk"><i style='color:blue' class='icon-ok icon-sm'></i></span>
                        <a href="#" class="btn btn-primary" id="upkk">Up</a>
                    </td>
            </tr>

            <tr>
                <form id="fm_foto" method="POST" enctype="multipart/form-data">
                    <td width="150px">File Pas Foto</td>
                    <td>:</td>
                    <td>
                        <a href="" id="current_foto" class="edit">File</a><input type="hidden" name="current_foto" id="current_foto_inp" />
                        <input type="file" id="lampiran_paspoto" name="lampiran_paspoto" size="20" onchange="validate_file(this)" /><!-- name harus sama dengan field di database -->
                    </td>
                </form>
                    <td width="170px">
                        <span id="foto"><i style='color:blue' class='icon-ok icon-sm'></i></span>
                        <a href="#" class="btn btn-primary" id="upfoto">Up</a>
                    </td>
            </tr>

            <tr>
                <form id="fm_waris" method="POST" enctype="multipart/form-data">
                    <td width="150px">File Pas Foto Ahli Waris</td>
                    <td>:</td>
                    <td>
                        <a href="" id="current_ahli_waris" class="edit">File</a><input type="hidden" name="current_ahli_waris" id="current_ahli_waris_inp" value/>
                        <input type="file" id="lampiran_paspoto_ahli_waris" name="lampiran_paspoto_ahli_waris" size="20" onchange="validate_file(this)" /><!-- name harus sama dengan field di database -->
                    </td>
                </form>
                    <td width="170px">
                        <span id="waris"><i style='color:blue' class='icon-ok icon-sm'></i></span>
                        <a href="#" class="btn btn-primary" id="upwaris">Up</a>
                    </td>
            </tr>

            <tr>
                <form id="fm_usaha" method="POST" enctype="multipart/form-data">
                    <td width="150px">File Surat Usaha</td>
                    <td>:</td>
                    <td>
                        <a href="" id="current_surtusaha" class="edit">File</a><input type="hidden" name="current_surtusaha" id="current_surtusaha_inp" />
                        <input type="file" id="lampiran_suratusaha" name="lampiran_suratusaha" size="20" onchange="validate_file(this)" /><!-- name harus sama dengan field di database -->
                    </td>
                </form>
                    <td width="170px">
                        <span id="usaha"><i style='color:blue' class='icon-ok icon-sm'></i></span>
                        <a href="#" class="btn btn-primary" id="upusaha">Up</a>
                    </td>
            </tr>

            <tr>
                <form id="fm_Tusaha" method="POST" enctype="multipart/form-data">
                    <td width="150px">File Tempat Usaha</td>
                    <td>:</td>
                    <td>
                        <a href="" id="current_temptusaha" class="edit">File</a><input type="hidden" name="current_temptusaha" id="current_temptusaha_inp" />
                        <input type="file" id="lampiran_tempatusaha" name="lampiran_tempatusaha" size="20" onchange="validate_file(this)" /><!-- name harus sama dengan field di database -->
                    </td>
                </form>
                    <td width="170px">
                        <span id="Tusaha"><i style='color:blue' class='icon-ok icon-sm'></i></span>
                        <a href="#" class="btn btn-primary" id="upTusaha">Up</a>
                    </td>
            </tr>

            <tr>
                <form id="fm_tinggal" method="POST" enctype="multipart/form-data">
                    <td width="150px">File Keterangan Tinggal</td>
                    <td>:</td>
                    <td>
                        <a href="" id="current_kettinggal" class="edit">File</a><input type="hidden" name="current_kettinggal" id="current_kettinggal_inp" />
                       <input type="file" id="lampiran_keterangantinggal" name="lampiran_keterangantinggal" size="20" onchange="validate_file(this)" /><!-- name harus sama dengan field di database -->
                    </td>
                </form>
                    <td width="170px">
                        <span id="tinggal"><i style='color:blue' class='icon-ok icon-sm'></i></span>
                        <a href="#" class="btn btn-primary" id="uptinggal">Up</a>
                    </td>
            </tr>

            <tr>
                <form id="fm_produk" method="POST" enctype="multipart/form-data">
                    <td width="150px">File Produk</td>
                    <td>:</td>
                    <td>
                        <a href="" id="current_produk" class="edit">File</a><input type="hidden" name="current_produk" id="current_produk_inp" />
                        <input type="file" id="lampiran_produk" name="lampiran_produk" size="20" onchange="validate_file(this)" /><!-- name harus sama dengan field di database -->
                    </td>
                </form>
                    <td width="170px">
                        <span id="produk"><i style='color:blue' class='icon-ok icon-sm'></i></span>
                        <a href="#" class="btn btn-primary" id="upproduk">Up</a>
                    </td>
            </tr>

            <tr>
                <form id="fm_Kusaha" method="POST" enctype="multipart/form-data">
                    <td width="150px">File Kegiatan Usaha</td>
                    <td>:</td>
                    <td>
                        <a href="" id="current_kegiatanprod" class="edit">File</a><input type="hidden" name="current_kegiatanprod" id="current_kegiatanprod_inp" />
                        <input type="file" id="lampiran_kegiatanusaha" name="lampiran_kegiatanusaha" size="20" onchange="validate_file(this)" /><!-- name harus sama dengan field di database -->
                    </td>
                </form>
                    <td width="170px">
                        <span id="Kusaha"><i style='color:blue' class='icon-ok icon-sm'></i></span>
                        <a href="#" class="btn btn-primary" id="upKusaha">Up</a>
                    </td>
            </tr>

            <tr>
                <form id="fm_Btabungan" method="POST" enctype="multipart/form-data">
                    <td width="150px">File Buku Tabungan</td>
                    <td>:</td>
                    <td>
                        <a href="" id="current_btabungan" class="edit">File</a><input type="hidden" name="current_btabungan" id="current_btabungan_inp" />
                        <input type="file" id="lampiran_bukutabungan" name="lampiran_bukutabungan" size="20" onchange="validate_file(this)" /><!-- name harus sama dengan field di database -->
                    </td>
                </form>
                    <td width="170px">
                        <span id="Btabungan"><i style='color:blue' class='icon-ok icon-sm'></i></span>
                        <a href="#" class="btn btn-primary" id="upBtabungan">Up</a>
                    </td>
            </tr>
            <tr>
                <form id="fm_pertanyaan" method="POST" enctype="multipart/form-data">
                    <td width="150px">File Pernyataan</td>
                    <td>:</td>
                    <td>
                        <a href="" id="current_pernyataan" class="edit">File</a><input type="hidden" name="current_pernyataan" id="current_pernyataan_inp" />
                        <input type="file" id="lampiran_pernyataan" name="lampiran_pernyataan" size="20" onchange="validate_file(this)" /><!-- name harus sama dengan field di database -->
                    </td>
                </form>
                    <td width="170px">
                        <span id="pertanyaan"><i style='color:blue' class='icon-ok icon-sm'></i></span>
                        <a href="#" class="btn btn-primary" id="uppertanyaan">Up</a>
                    </td>
            </tr>
            </table>
            <!-- </form> -->
            <!--Tombol upload file all-->
    <div id="t_dlg_upload_view">
        <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:$('#dlg_upload_all').dialog('close')"><i class="icon-remove icon-large"></i>&nbsp;Batal</a>
    </div>
    <!--End Tombol upload file all-->
</div>
