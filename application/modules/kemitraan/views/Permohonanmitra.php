<?php
date_default_timezone_set("Asia/Jakarta");

$tittleManager = managerTitle;
$namemanager = managerName;
$url_img = url_img;

$tanggal = date('d F Y');
$tgl = $vendor['tgl_berdiri'];
$hari = date('d', strtotime($tgl));
$bulan = date('m', strtotime($tgl));
// echo $bulan;
$year = date('Y', strtotime($tgl));
$day = date('D', strtotime($tanggal));
$dayList = array(
	'Sun' => 'Minggu',
	'Mon' => 'Senin',
	'Tue' => 'Selasa',
	'Wed' => 'Rabu',
	'Thu' => 'Kamis',
	'Fri' => 'Jumat',
	'Sat' => 'Sabtu'
);
$bulanList = array('01' => 'Januari', '02' => 'Februari', '03' => 'Maret', '04' => 'April', '05' => 'Mei', '06' => 'Juni', '07' => 'Juli', '08' => 'Augustus', '09' => 'September', '10' => 'October.', '11' => 'Nopember', '12' => 'Desember');


function kekata($x)
{
	$x = abs($x);
	$angka = array(
		"", "satu", "dua", "tiga", "empat", "lima",
		"enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas"
	);
	$temp = "";
	if ($x < 12) {
		$temp = " " . $angka[$x];
	} else if ($x < 20) {
		$temp = kekata($x - 10) . " belas";
	} else if ($x < 100) {
		$temp = kekata($x / 10) . " puluh" . kekata($x % 10);
	} else if ($x < 200) {
		$temp = " seratus" . kekata($x - 100);
	} else if ($x < 1000) {
		$temp = kekata($x / 100) . " ratus" . kekata($x % 100);
	} else if ($x < 2000) {
		$temp = " seribu" . kekata($x - 1000);
	} else if ($x < 1000000) {
		$temp = kekata($x / 1000) . " ribu" . kekata($x % 1000);
	} else if ($x < 1000000000) {
		$temp = kekata($x / 1000000) . " juta" . kekata($x % 1000000);
	} else if ($x < 1000000000000) {
		$temp = kekata($x / 1000000000) . " milyar" . kekata(fmod($x, 1000000000));
	} else if ($x < 1000000000000000) {
		$temp = kekata($x / 1000000000000) . " trilyun" . kekata(fmod($x, 1000000000000));
	}
	return $temp;
}
function terbilang($x, $style = 4)
{
	if ($x < 0) {
		$hasil = "minus " . trim(kekata($x));
	} else {
		$hasil = trim(kekata($x));
	}
	switch ($style) {
		case 1:
			$hasil = strtoupper($hasil);
			break;
		case 2:
			$hasil = strtolower($hasil);
			break;
		case 3:
			$hasil = ucwords($hasil);
			break;
		default:
			$hasil = ucfirst($hasil);
			break;
	}
	return $hasil;
}
$nilai_pengajuan = number_format($vendor['nilai_pengajuan']);
// $harga_penawaran_vendor = number_format($vendor['harga_penawaran_vendor']);
?>

<title>Permohonan menjadi Mitra Binaan<br>PT Krakatau Steel Tbk</title>
<style>
	u {
		border-bottom: 1px dotted #000;
		text-decoration: none;
	}

	div,
	.hr {

		background-color: white;
		/*width: 100%;*/
		border: 0px solid black;
		padding: 25px;
		margin: 0px;
		font-size: 10;
		padding-bottom: 2px;
		padding-top: 0px;

	}

	hr {
		background-color: white;
		/*width: 100%;*/
		border: 0px solid black;
		padding: 0px;
		margin: 0px;
	}

	h3,
	.title {
		text-align: center;
	}

	table {
		/*width: 100%;*/
		border: 1px solid black;
		border-collapse: collapse;
		padding: 5px;
		/*padding-bottom: 5px;*/
	}

	table,
	td,
	tr {
		border: 1px solid black;
		border-collapse: collapse;
		padding: 2px;
		/*padding-bottom:2px;*/

	}
</style>

<div style="padding-top: 5px;padding-bottom: 0px;">
	<table style="padding-top: 0px;" width="100%" height="40%">
		<tr>
			<td style="padding-bottom: 0px;padding-top: 0px;padding-left: 0px;padding-right: 0px;">
				<center><img src="<?= $url_img.'assets/images/headkti.png'?>" style="width:90%; height:90%;"></center>
			</td>
			<td align="center" colspan="4" style="font-size: 14px;">
				PERMOHONAN<br> MENJADI MITRA BINAAN<br>PT KRAKATAU STEEL (PERSERO)Tbk
			</td>
			<td colspan="2">
				<center>
					<font style=" font-size: 15px;">Seri</font><strong style=" font-size: 30px;">C</strong>
				</center><br>
				<center>
					<font style="font-size: 20px;"> No. <?php echo $vendor['id_mitra']; ?>
			</td>
			</center>
		</tr>

		<tr>
			<td colspan="7" style="border-bottom: 1px;padding-left: 20px;font-size: 11px;">Kepada Yth.<br><?= $tittleManager ?><br>PT. Krakatau Steel (Persero) Tbk<br>Di Cilegon,</td>
		</tr>
		<tr>
			<td colspan="7" style="border-bottom: 0px;border-top: 0px;font-size: 11px;">
				&nbsp;
			</td>
		</tr>

		<tr>
			<td colspan="7" style="border-top:1px;border-bottom: 1px;padding-left: 20px;font-size: 11px;">Dengan Hormat</td>
		</tr>
		<tr>
			<td colspan="7" style="border-top:1px;border-bottom: 1px;padding-left: 20px;font-size: 11px;">Saya yang bertanda tangan dibawah ini</td>
		</tr>
		<tr style="padding:1px;">
			<td colspan="2" style="border-top:1px;border-bottom: 1px;border-right: 1px;padding-left: 20px;font-size: 11px;">1. Nama Pemilik Usaha</td>
			<td colspan="5" style="border-top:1px;border-bottom: 1px;border-left: 1px;padding-left: 20px;font-size: 11px;">: <u><?php echo $vendor['nama']; ?></u></td>
		</tr>
		<tr>
			<td colspan="2" style="border-top:1px;border-bottom: 1px;border-right: 1px;padding-left: 20px;font-size: 11px;">2. Alamat Rumah</td>
			<td colspan="5" style="border-top:1px;border-bottom: 1px;border-left: 1px;padding-left: 20px;font-size: 11px;">: <u><?php echo $vendor['alamat']; ?></u></td>
		</tr>
		<tr>
			<td colspan="2" style="border-top:1px;border-bottom: 1px;border-right: 1px;padding-left: 20px;font-size: 11px;"></td>
			<td colspan="5" style="border-top:1px;border-bottom: 1px;border-left: 1px;padding-left: 20px;font-size: 11px;"> <u></u></td>
		</tr>
		<tr>
			<td colspan="2" style="border-top:1px;border-bottom: 1px;border-right: 1px;padding-left: 20px;font-size: 11px;">&nbsp;&nbsp;&nbsp; - Desa / Kelurahan</td>
			<td colspan="5" style="border-top:1px;border-bottom: 1px;border-left: 1px;padding-left: 20px;font-size: 11px;">: <u><?php echo $vendor['kelurahan']; ?></u></td>
		</tr>
		<tr>
			<td colspan="2" style="border-top:1px;border-bottom: 1px;border-right: 1px;padding-left: 20px;font-size: 11px;">&nbsp;&nbsp;&nbsp; - Kecamatan</td>
			<td colspan="5" style="border-top:1px;border-bottom: 1px;border-left: 1px;padding-left: 20px;font-size: 11px;">: <u><?php echo $vendor['kecamatan']; ?></u></td>
		</tr>
		<tr>
			<td colspan="2" style="border-top:1px;border-bottom: 1px;border-right: 1px;padding-left: 20px;font-size: 11px;">&nbsp;&nbsp;&nbsp; - Kabupaten/Kota</td>
			<td colspan="5" style="border-top:1px;border-bottom: 1px;border-left: 1px;padding-left: 20px;font-size: 11px;">: <u><?php echo $vendor['kota']; ?></u></td>
		</tr>
		<tr>
			<td colspan="2" style="border-top:1px;border-bottom: 1px;border-right: 1px;padding-left: 20px;font-size: 11px;">&nbsp;&nbsp;&nbsp; - Provinsi</td>
			<td colspan="5" style="border-top:1px;border-bottom: 1px;border-left: 1px;padding-left: 20px;font-size: 11px;">: <u><?php echo $vendor['propinsi']; ?></u></td>
		</tr>
		<tr>
			<td colspan="2" style="border-top:1px;border-bottom: 1px;border-right: 1px;padding-left: 20px;font-size: 11px;">&nbsp;&nbsp;&nbsp; - Telpon / Hp</td>
			<td colspan="5" style="border-top:1px;border-bottom: 1px;border-left: 1px;padding-left: 20px;font-size: 11px;">: <u><?php echo $vendor['telepon']; ?> / <?php echo $vendor['handphone']; ?> </u></td>
		</tr>
		<tr>
			<td colspan="2" style="border-top:1px;border-bottom: 1px;border-right: 1px;padding-left: 20px;font-size: 11px;">3. Bidang Usaha</td>
			<td colspan="5" style="border-top:1px;border-bottom: 1px;border-left: 1px;padding-left: 20px;font-size: 11px;">: <u><?php echo $vendor['nama_sektor']; ?></u></td>
		</tr>
		<tr>
			<td colspan="2" style="border-top:1px;border-bottom: 1px;border-right: 1px;padding-left: 20px;font-size: 11px;">4. Komoditi</td>
			<td colspan="5" style="border-top:1px;border-bottom: 1px;border-left: 1px;padding-left: 20px;font-size: 11px;">: <u><?php echo $vendor['komoditi']; ?></u></td>
		</tr>
		<tr>
			<td colspan="2" style="border-top:1px;border-bottom: 1px;border-right: 1px;padding-left: 20px;font-size: 11px;">5. Nama Perusahaan</td>
			<td colspan="5" style="border-top:1px;border-bottom: 1px;border-left: 1px;padding-left: 20px;font-size: 11px;">: <u><?php echo $vendor['nama_perusahaan']; ?></u></td>
		</tr>
		<tr>
			<td colspan="2" style="border-top:1px;border-bottom: 1px;border-right: 1px;padding-left: 20px;border-spacing: 1px;font-size: 11px;">6. Alamat Perusahaan</td>
			<td colspan="5" style="border-top:1px;border-bottom: 1px;border-left: 1px;padding-left: 20px;font-size: 11px;">: <u><?php echo $vendor['alamat_perusahaan']; ?></u></td>
		</tr>
		<!-- <tr>
			<td colspan="2" style="border-top:1px;border-bottom: 1px;border-right: 1px;padding-left: 20px;"></td>
			<td colspan="5" style="border-top:1px;border-bottom: 1px;border-left: 1px;padding-left: 20px;"> <u style="width: 100%"></u></td>
		</tr> -->
		<tr>
			<td colspan="2" style="border-top:1px;border-bottom: 1px;border-right: 1px; font-size: 11px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- Desa / Kelurahan</td>
			<td colspan="5" style="border-top:1px;border-bottom: 1px;border-left: 1px;padding-left: 20px;font-size: 11px;">: <u><?php echo $vendor['kelurahan_perusahaan']; ?></u></td>
		</tr>
		<tr>
			<td colspan="2" style="border-top:1px;border-bottom: 1px;border-right: 1px;padding-left: 20px;font-size: 11px;">&nbsp;&nbsp;&nbsp; - Kecamatan</td>
			<td colspan="5" style="border-top:1px;border-bottom: 1px;border-left: 1px;padding-left: 20px;font-size: 11px;">: <u><?php echo $vendor['kecamatan_perusahaan']; ?> </u></td>
		</tr>
		<tr>
			<td colspan="2" style="border-top:1px;border-bottom: 1px;border-right: 1px;padding-left: 20px;font-size: 11px;">&nbsp;&nbsp;&nbsp; - Kabupaten / Kota</td>
			<td colspan="5" style="border-top:1px;border-bottom: 1px;border-left: 1px;padding-left: 20px;">: <u><?php echo $vendor['kota_perusahaan']; ?></u></td>
		</tr>
		<tr>
			<td colspan="2" style="border-top:1px;border-bottom: 1px;border-right: 1px;padding-left: 20px;font-size: 11px;">&nbsp;&nbsp;&nbsp; - Provinsi</td>
			<td colspan="5" style="border-top:1px;border-bottom: 1px;border-left: 1px;padding-left: 20px;font-size: 11px;">: <u><?php echo $vendor['propinsi_perusahaan']; ?> </u></td>
		</tr>
		<tr>
			<td colspan="2" style="border-top:1px;border-bottom: 1px;border-right: 1px;padding-left: 20px;font-size: 11px;">&nbsp;&nbsp;&nbsp; - Telpon / Hp</td>
			<td colspan="5" style="border-top:1px;border-bottom: 1px;border-left: 1px;padding-left: 20px;font-size: 11px;">: <u><?php echo $vendor['telepon_perusahaan']; ?> </u></td>
		</tr>
		<tr>
			<td colspan="2" style="border-top:1px;border-bottom: 1px;border-right: 1px;padding-left: 20px;font-size: 11px;">7. Mulai Kegiatan Usaha Sejak</td>
			<td colspan="5" style="border-top:1px;border-bottom: 1px;border-left: 1px;padding-left: 20px;font-size: 11px;">: <u> <?php echo $hari; ?>&nbsp;  <?php echo $bulanList[$bulan]; ?></u> <u><?php echo $year; ?></u><br><br></td>
		</tr>
		<tr>
			<td colspan="7" style="text-align: justify;border-top:1px;border-bottom: 1px; padding-left: 20px;padding-right: 20px;vertical-align: justify;padding-top: 0px;font-size: 11px;">Dengan ini kami mengajukan permohonan menjadi Mitra Binaan PT Krakatau Steel (Persero) Tbk, dengan Pengajuan Pinjaman Modal Usaha sebesar Rp.&nbsp;<strong><?php echo $nilai_pengajuan; ?> </strong>(<u><i><?php echo terbilang($vendor['nilai_pengajuan'], $style = 3); ?> &nbsp;Rupiah</i></u>) untuk digunakan <u><b><i><?php echo $vendor['keterangan']; ?></i></b></u> dan bersedia mentaati semua peraturan sesuai Perjanjian Kerjasama yang disepakati bersama.
			</td>
		</tr>

		<tr>
			<td colspan="7" style="border-top:1px;border-bottom: 1px;padding-left: 20px;padding-right: 20px;padding-top: 3px;font-size: 11px;">Sehubungan dengan hal tersebut diatas maka dalam kesepakatan ini saya menyatakan bahwa :
				<ol style="text-align: justify;">
					<li>
						Belum menjadi anak Angkat/Mitra Binaan BUMN lain.
					</li>
					<li>
						Berjanji untuk melaksanakan kewajiban sebagai mitra Binaan termasuk mengembalikan/ mengangsur pinjaman Modal Usaha tepat waktu sesuai aturan yang ditentukan oleh PT Krakatau Steel (Persero) Tbk / Pembinaan dan tidak mengalihkan Pinjaman tersebut kepada pihak lain.
					</li>
					<li>
						Bersedia menyerahkan jaminan berupa <strong><?php echo $vendor['nama_jaminan']; ?></strong>.
					</li>
					<li>
						Apabila hutang saya tidak terbayar / tidak tepat waktu, saya menguasakan jaminan pada point 3 kepada PT Krakatau Steel (Persero) Tbk untuk dijual.
					</li>
				</ol>
				<br>
		</tr>
		<tr>
			<td colspan="7" style="border-top:1px;border-bottom: 1px;padding-left: 20px;font-size: 11px;">Demikian permohonan ini saya buat dengan sesungguhnya dan dapat dipertanggung jawabkan sesuai dengan ketentuan yang berlaku.<br /><br /></td>
		</tr>
		<tr>
			<td colspan="7" style="border-top:1px;border-bottom: 1px;padding-left: 20px;padding-right:  20px;font-size: 11px;" align="right">.....................................................<br></td>
		</tr>
		<tr style="padding-top: 10px;">
			<td colspan="3" style="border-top: 0px; border-bottom: 0px;border-right: 0px;font-size: 11px;">
				<table border="0" style="margin-left: 10px">
					<tr>
						<td>
							<img src='<?php
										if ($vendor['lampiran_paspoto'] != NULL) {
											echo $url_img. $vendor['lampiran_paspoto'];
										} else {
											echo $url_img.'assets/images/3x4.png';
										} ?>' style="width: 113px;height: 151px;">
						</td>
						<td><img src='<?php
										if ($vendor['lampiran_paspoto_ahli_waris'] != NULL) {
											echo $url_img. $vendor['lampiran_paspoto_ahli_waris'];
										} else {
											echo  $url_img.'assets/images/3x4.png';
										} ?>' style="width: 113px;height: 151px;">
						</td>
					</tr>
					<tr>
						<td align="center">Calon Mitra Binaan</td>
						<td align="center">Ahli Waris</td>
					</tr>
				</table>

			</td>
			<td colspan="4" style="border-top:  0px;border-left: 0px;border-bottom: 0px;font-size: 11px;">
				<center>
					<img src='<?= $url_img.'assets/images/mtr.png' ?>' style="width: 40%;height: 40%; vertical-align: center;"><br><br><br><br><br>(<u><?php echo $vendor['nama']; ?></u>)<br>Calon Mitra Binaan<br>
				</center>
			</td>
		</tr>
		<tr>
			<td colspan="3" style="border-top: 0px; border-bottom: 0px;border-right: 0px;font-size: 11px;"><br><br><br>
				<font style="font-size: 10px;">*)Coret yang tidak perlu</font>
			</td>
			<td colspan="4" style="border-top: 0px; border-bottom: 0px;border-left: : 0px;font-size: 11px;">
				<center><br>Ahli Waris<br>*)Suami, Istri, Orag Tua, Anak<br><br><br><br><br>(<u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)</u></center>
			</td>
		</tr>
	</table>
</div>