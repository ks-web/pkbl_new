<?php
$app1 = 'Persetujuan Peminjaman'; // nama aplikasi
$module1 = 'kemitraan';
$appLink1 = 'Persetujuan_pinjaman'; // controller
$idField1  = 'id_persetujuan'; //field key table
?>

<script>
	var url1;
	var app1 = "<?=$app1?>";
	var appLink1 = '<?=$appLink1?>';
	var module1 = '<?=$module1?>';
	var idField1 = '<?=$idField1?>';

	
	$(document).ready(function(){
		// $.get('<?php echo base_url('kemitraan/persetujuan_pinjaman/session');?>',function(data){
		// 	//3 kadis
		// 	if(data.group_id == 3){
		// 		$('#dg1').datagrid('showColumn', 'diketahui');
		// 	}else{
		// 		$('#dg1').datagrid('hideColumn', 'diketahui');
		// 	}
		// });
		$("#row_ket_ditolak").hide();
	});

    function add1(){
        $('#dlg1').dialog('open').dialog('setTitle','Tambah ' + app1);
		$('#fm1').form('clear');
	    $('#tbl1').html('Save1');
		url1 = '<?=base_url($module1 . '/' . $appLink1 . '/create')?>/';
    }
    function edit1(){
        var row = $('#dg1').datagrid('getSelected');
		if (row){
			$('#tbl1').html('Simpan');
			$('#dlg1').dialog('open').dialog('setTitle','Edit '+ app1);
			$('#fm1').form('load',row);

			url1 = '<?=base_url($module1 . '/' . $appLink1 . '/update')?>/'+row[idField1];
	    }
    }
    function hapus1(){
        var row = $('#dg1').datagrid('getSelected');		
	    if (row){
			$.messager.confirm('Confirm','Apakah Anda Yakin Akan Menghapus ini?',function(r){
				if (r){
					$.post('<?=base_url($module1 . '/' . $appLink1 . '/delete')?>/'+row[idField1],function(result){
						if (result.success){
							$('#dg1').datagrid('reload');	// reload the user data
							$('#dg1').datagrid('reload');
						} else {
							$.messager.show({	// show error message
								title: 'Error',
								msg:result.msg
							});
						}
					},'json');
				}
			});
		}
    }
    function save1(){
	   $('#fm1').form('submit',{
	    	url: url1,
	    	onSubmit: function(){
	    		return $(this).form('validate');
	    	},
	    	success: function(result){
	    		var result = eval('('+result+')');
	    		if (result.success){
	    			$('#dlg1').dialog('close');		
	    			$('#dg1').datagrid('reload');
	                
	    		} else {
	             $.messager.alert('Error Message',result.msg,'error');
	    		}
	    	}
	    });
	}
	function doSearch1(value){
	    $('#dg1').datagrid('load',{    
	    	q:value  
	    });
	}
	function reload1(value){
		$('#dg1').datagrid({    
	    	url: '<?=base_url($module1 . '/' . $appLink1 . '/read/')?>/'+idParent  
	    });
	}
	function action(row){
		// console.log(row);
		$.messager.confirm('Konfirmasi','Yakin Melakukan Proses ini?',function(r){
		    if (r){
		        $.post('<?php echo base_url('kemitraan/persetujuan_pinjaman/diketahui'); ?>/'+row, function(data){
					if(data){
						$('#dg1').datagrid('reload');
					}
				});
		    }
		});
	}
	function diketahui(val,row){
		var icon = '';
		var cek = '';
		var session = '<?php echo $this->session->userdata('group_id');?>';
		console.log(session);
		if(val){
			icon = "<i style='color:blue' class='icon-ok icon-sm'></i>";
		}else{
			icon = "<i style='color:red' class='icon-remove icon-sm'></i>";
		}

		if(session == '3'){
			cek = "<button class='btn btn-default' onclick='action(\""+row.id_mitra+"\")'>"+
				  icon+
				  "</button>";
		}else{
			cek = "<center>"+icon+"</center>";
		}
		
		return cek;
	}
</script>
 
<div class="tabs-container">                
	<table id="dg1" class="easyui-datagrid" 
	data-options="  
	    url: '<?=base_url($module1 . '/' . $appLink1 . '/read/')?>',
		singleSelect:'true', 
	    title:'<?=$app1?>',
	    toolbar:'#toolbar1',
	    iconCls:'icon-cog',
	    rownumbers:'true',  
	    idField:'<?=$idField1?>', 
	    pagination:'true',
	    fitColumns:'true',
	    pageList: [10,20,30]
	    "
	>
	    <thead>
	    	<!-- <th field="id_asset" width=100" sortable="true">ID Asset</th> -->
	        <th field="id_persetujuan" hidden="true" width="100" sortable="true"></th>
	        <th field="id_mitra" width="100" sortable="true"> ID Mitra</th>
			<!-- <th field="" width="100" sortable="true">Tanggal Daftar</th> -->
			<th field="nama" width="100" sortable="true">Nama Pemohon</th>
			<th field="nama_perusahaan" width="100" sortable="true">Nama Perusahaan </th>
			<th field="nilai_pengajuan" width="100" sortable="true" formatter="format_numberdisp">Nilai Pengajuan</th>
			<!-- <th field="" width="100" sortable="true">Status</th> -->
			<!-- <th field="" width="100" sortable="true">Tanggal Survey</th> -->
			<th field="angsuran_pokok" width="100" sortable="true" formatter="format_numberdisp">Nilai Rekomendasi</th>
			<!--<th field="diketahui" width="50" sortable="true"  formatter="diketahui">Diketahui</th>-->
			<!-- <th field="nilai_rekomendasi_survey" width="100" sortable="true">Rekomendasi Penilaian</th> -->

	    </thead>
	</table>

	<!-- Model Start -->
	<div id="dlg1" class="easyui-dialog" style="width:520px; height:640px; padding:6px" closed="true" buttons="#t_dlg_dis-buttons1" >
	    <form id="fm1" method="post" enctype="multipart/form-data" action="">
	       <table width="100%" align="center" border="0">
	            <tr style="padding-top: 3px;padding-bottom: 3px;color:red;">
	            	<td colspan="3" align="Left"><b>Identitas Data Pegajuan Mitra</b></td>
	            </tr>
	            <tr>
	                <td>Tanggal Disetujui</td>
	                <td>:</td>
	                <td><input type="text" name="tanggal_disetujui" id="tanggal_disetujui" class="easyui-datebox" style="width: 100px;">
	                </td>
	            </tr>
	            <tr>
	                <td>Nama Instansi</td>
	                <td>:</td>
	                <td>
	                    <select name="id_mitra" id="id_mitra" class="easyui-combogrid" style="width:300px;" data-options="
                        required:true,
                        panelWidth:300,
                        value:'',
                        idField:'id_mitra',
                        textField:'nama_perusahaan',
                        mode:'remote',
                        url:'<?=base_url($module1 . '/' . $appLink1 . '/getDataMitra') ?>',
                        onSelect :function(indek,row)
						{
							$('#nama').val(row.nama);
							$('#no_ktp').val(row.no_ktp);
							$('#alamat').val(row.alamat);
							$('#kota').val(row.kota);
							$('#sektor_usaha').val(row.sektor_usaha);
							$('#sektor_usaha1').val(row.nama_sektor);
							$('#tanggal_survey').datebox('setValue',row.tanggal_survey);
							$('#max_pinjaman').numberbox('setValue', row.max_pinjaman);
							$('#nilai_dibulatkan').numberbox('setValue', row.angsuran_pokok);


							$('#nilai_pengajuan').val(row.nilai_pengajuan);
							$('#nilai_pengajuanval').numberbox('setValue', row.nilai_pengajuan);
						},
                        columns:[[
                        {field:'id_mitra',title:'ID Instansi',width:150},
                        {field:'nama_perusahaan',title:'Nama Perusahaan',width:150}
                        ]]
                        "></select>
	                </td>
	            </tr>
	            <tr>
	                <td>Nama Pemohon</td>
	                <td>:</td>
	                <td>
	                    <input type="text" name="nama" id="nama" value="1" style="width: 150px;">
	                </td>
	            </tr>
	            <tr>
	                <td>No KTP</td>
	                <td>:</td>
	                <td>
	                    <input type="text" name="no_ktp" id="no_ktp" value="1" readonly="true">
	                </td>
	            </tr>
	             <tr>
	                <td>Alamat</td>
	                <td>:</td>
	                <td>
	                    <textarea name="alamat" id="alamat"></textarea>
	                </td>
	            </tr>
	             <tr>
	                <td>Kota</td>
	                <td>:</td>
	                <td><input type="text" name="kota" id="kota"></td>
	            </tr>
	            <tr>
	                <td>Tgl Survey</td>
	                <td>:</td>
	                <td><input type="text" name="tanggal_survey" id="tanggal_survey" class="easyui-datebox" readonly></td>
	            </tr>
	            <tr>
	            	<td>Sektor Usaha</td>
	            	<td>:</td>
	            	<td><input type="text" id="sektor_usaha1" value="1" readonly>
	            	<input type="hidden" name="sektor_usaha" id="sektor_usaha" value="1"></td>
	            </tr>
	            <tr>
	            	<td>Bentuk Kegiatan</td>
	            	<td>:</td>
	            	<td><input type="text" name="" id=""></td>
	            </tr>
	            <tr style="padding-top: 5px;padding-bottom: 5px;color:red;">
	            	<td colspan="3" align="left" ><b>Ketentuan Dan Keterangan</b></td>
	            </tr>
	            <tr>
	            	<td>Nilai Pengajuan Mitra</td>
	            	<td>:</td>
	            	<td><input class="easyui-numberbox" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%',disabled:'true'" name="nilai_pengajuan" id="nilai_pengajuanval"></td>
	            </tr>
	            <tr>
	            	<td>Nilai Rekomendasi Evaluator</td>
	            	<td>:</td>
	            	<td><input class="easyui-numberbox" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%',disabled:'true'" name="nilai_dibulatkan" id="nilai_dibulatkan"></td>
	            </tr>
	            <tr>
	            	<td>Nilai Max Pinjaman</td>
	            	<td>:</td>
	            	<td><input class="easyui-numberbox" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%',disabled:'true'" name="max_pinjaman" id="max_pinjaman"></td>
	            </tr>
	            <!--<tr>
	            	<td>Nilai Disetujui</td>
	            	<td>:</td>
	            	<td><input class="easyui-numberbox" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%'" name="nilai_disetujui" id="nilai_disetujui" required style="width: 250px;"></td>
	            </tr>-->
	            <tr>
	            	<td>Keputusan</td>
	            	<td>:</td>
			        <td><select  class="easyui-combobox" type="text" name="keputusan" id="keputusan" required style="width: 100px;" data-options="onSelect: function(rec){
						if(rec.value == 'Tidak Disetujui'){
							$('#row_ket_ditolak').show();
						} else {
							$('#row_ket_ditolak').hide();
						}
        			}">
			                    	<option value="Disetujui" style="height: 100px;">Disetujui</option>
			                    	<option value="Tidak Disetujui" style="height: 100px;">Tidak Disetujui</option>
			                    </select></td>
	            </tr>
	            <tr id="row_ket_ditolak">
	            	<td>Ket Ditolak</td>
	            	<td>:</td>
	            	<td><textarea name="ket_ditolak" id="ket_ditolak"></textarea></td>
	            </tr>
	            <!-- <tr>
	            	<td>Dana Tersedia</td>
	            	<td>:</td>
	            	<td><select name="id_sdana" id="id_sdana" class="easyui-combogrid" style="width:250px;" data-options="
                        required:true,
                        panelWidth:250,
                        idField:'id_sdana',
                        textField:'nama_sdana',
                        mode:'remote',
                        url:'<?=base_url('kemitraan/Persetujuan_pinjaman/dana')?>',
                        onSelect :function(indek,row)
                        {
                        },
                        columns:[[
                        {field:'id_sdana',title:'ID Dana',width:100},
                        {field:'nama_sdana',title:'Ket Dana',width:150}
                        ]]
                        "></select></td>
	            </tr> -->
	            <tr>
	            	<td>Ket Pemberian Dana</td>
	            	<td>:</td>
	            	<td><textarea name="keteranganpemberian" id="keteranganpemberian"></textarea></td>
	            </tr>
	        </table>
	    </form>
	   
	   	<!-- Tombol Add, Edit, Hapus Datagrid -->
       	<div id="toolbar1">  
            <table align="center" style="padding: 0px; width: 99%;">
                <tr>
                    <td>
                        <a href="javascript:void(0)" class="btn btn-small btn-success" onclick="javascript:add1()"><i class="icon-plus-sign icon-large"></i>&nbsp;Add</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:edit1()"><i class="icon-edit"></i>&nbsp;Edit</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-danger" onclick="javascript:hapus1()"><i class="icon-trash icon-large"></i>&nbsp;Delete</a>
                    </td>
                    <td>&nbsp;</td>
                    <td align="right">
                    	<input class="easyui-searchbox" prompt="Silahkan Input Field" searcher="doSearch1"  style="width:300px"/>
                    </td>
                </tr>
            </table>  
        </div>  
        <!-- end tombol datagrid -->
        <!-- Tombol simpan & Batal Form -->
        <div id="t_dlg_dis-buttons1">
            <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:save1()"><i class="icon-save icon-large"></i>&nbsp;Simpan</a>
            <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:$('#dlg1').dialog('close')"><i class="icon-remove icon-large"></i>&nbsp;Batal</a>      
        </div>
	</div>
	<!-- Model end -->
</div>