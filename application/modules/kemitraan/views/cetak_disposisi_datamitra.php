<?php
	$indeks = '<br />';
	$kode = 'Program Kemitraan';
	$tanggal_masuk = $data_head['tanggal_create'] == null? '<br />':tanggal_display($data_head['tanggal_create']);
	$no = $data_head['id_mitra'];
	$asal = $data_head['nama'].'<br/>'.$data_head['nama_perusahaan'].' (Perusahaan)';
	$no_surat = '<br />';
	$tanggal_surat = $tanggal_masuk;
	$perihal = $data_head['keterangan'].'<br />';
	$tittleManager = managerTitle;
	$namemanager = managerName;
	$url_img = url_img;
?>


		<style>
			table {
				border-spacing: 0px;
				border-collapse: separate;
				font-size: 12px;
				page-break-inside: avoid;
			}

			table, th, td {
				border: 0px solid black;
			}
			html {
				margin: 30px 50px
			}
			.bordered, .bordered td {
				border: 1px solid black;
				font-size: 12px;
			}
			
			.bordered-catat{
				border: 1px solid black;
				font-size: 12px;
			}
			td.no_border_bot {
				border-top: 0px solid black;
				border-bottom: 0px solid black;
			}
			.test td {
				border: 1px solid black;
			}

			.pull-right {
				text-align: right;
			}
			.center {
				text-align: center;
			}

			.bold {
				font-weight: bold;
			}
			.corporate {
				color: red;
				font-size: 24px;
			}
			.corporate_sub {
				font-size: 12px;
			}
			.red {
				color: red;
			}
		</style>
		<table style="width: 100%;">
			<!-- <tbody> -->
				<tr>
					<td>
					<table style="width: 100%;">
						<tr>
							<td width="70px"><img  src='<?= $url_img.'assets/images/logo_KS.png' ?>' style="width: 50%;"></td>
							<td><span class="corporate bold">PT. KRAKATAU STEEL</span>
							<br />
							<span class="corporate_sub bold">DIVISI COMMUNITY DEVELOPMENT</span></td>
							<td width="250px"><span class="bold"></span>
							<br />
							<span class="bold">&nbsp;</span><br />
							<span class="bold">&nbsp;</span></td>
						</tr>
					</table>
					<center>
						<h2 style="margin-top: 10px;">LEMBAR DISPOSISI</h2>
					</center>
					
					<table width="100%" class="bordered">
						<tr class="bold">
							<td class="center" width="150px">Indeks</td>
							<td class="center">Kode</td>
							<td class="center" width="150px">Tanggal Masuk</td>
							<td class="center" width="220px">No</td>
						</tr>
						<tr class="">
							<td class="" width="150px"><?=$indeks?></td>
							<td class=""><?=$kode?></td>
							<td class="" width="150px"><?=$tanggal_masuk?></td>
							<td class="" width="220px"><?=$no?></td>
						</tr>
						<tr class="">
							<td class="" colspan="3" rowspan="4" style="vertical-align: top;"><span class="bold">Asal</span> :<br /><?=$asal?></td>
							<td class="bold" width="220px">No. Surat</td>
						</tr>
						<tr class="">
							<td class="" width="220px"><?=$no_surat?></td>
						</tr>
						<tr class="">
							<td class="bold" width="220px">Tanggal Surat</td>
						</tr>
						<tr class="">
							<td class="" width="220px"><?=$tanggal_surat?></td>
						</tr>
						<tr class="">
							<td class="" colspan="4"><span class="bold">Perihal</span> :<br /><?=$perihal?><br /></td>
						</tr>
					</table>
					<table width="100%" class="bordered">
						<tr class="bold">
							<td class="center" colspan="2">Dari Manager Community Development untuk :</td>
							<td class="center" width="80px">Initial</td>
							<td class="center" width="100px">Tanggal</td>
							<td class="center" width="220px">Pesan</td>
						</tr>
						<?php
						$couter = 1;
						if($data_detail){
							foreach ($data_detail as $row) {
								$tanggal_disposisi = $row['tanggal_survey'] == null? '<br />':tanggal_display($row['tanggal_survey']);
								?>
						<tr class="">
							<td class="center" width="30px" style="vertical-align: top;"><?=$couter?>.</td>
							<td class="" style="vertical-align: top;"><?=$row['jabatan']?></td>
							<td class="center" width="80px" style="vertical-align: top;"><?=$row['penerima']?></td>
							<td class="center" width="100px" style="vertical-align: top;"><?=$tanggal_disposisi?></td>
							<!-- <td class="" width="220px" style="vertical-align: top;"><?=$row['pesan_disposisi']?><br />(*) <?=$row['catatan_disposisi']?></td> -->
							<td class="" width="220px" style="vertical-align: top;"><?=$row['pesan_disposisi']?><br />(*)</td>
						</tr>
								<?php
								$couter++;
							}
						}
						?>
						<tr class="">
							<td class="center" width="30px"><br /></td>
							<td class=""><br /></td>
							<td class="center" width="80px"><br /></td>
							<td class="center" width="100px"><br /></td>
							<td class="" width="220px"><br /></td>
						</tr>
					</table>
					</td>
				</tr>
			<!-- </tbody> -->
		</table>

