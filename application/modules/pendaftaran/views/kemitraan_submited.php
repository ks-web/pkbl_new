<div class="dialog" style="width: 900px; margin-top: 0px;">
	<div class="block">
		<p class="block-heading">
			Pendaftaran Kemitraan
		</p>
		<div class="block-body">
			<p>
				Pendaftaran berhasil silahkan cek email anda. Halaman ini akan kembali otomatis ke halaman utama dalam <span id="counter">5</span> detik.
			</p>
			<script type="text/javascript">
				function countdown () {
					var i = document.getElementById('counter');
					if (parseInt(i.innerHTML) <= 1) {
						location.href = '<?=base_url('')?>';
					}
					i.innerHTML = parseInt(i.innerHTML) - 1;
				}

				setInterval(function () {
					countdown();
				}, 1000);
			</script>
		</div>
	</div>
</div>