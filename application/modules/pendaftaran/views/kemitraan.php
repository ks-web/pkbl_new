<script type="text/javascript">
function pengajuan(status_pengajuan) {
    var url_temp='<?=base_url('pendaftaran/kemitraan/get_provinsi')?>';
    if (status_pengajuan.value=='KM') {
    	// console.log('km');
    	$('#kabupaten').combobox('clear');
		$('#kecamatan').combobox('clear');
		$('#kelurahan').combobox('clear');
		$('#provinsi').combobox('clear');
    	$('#provinsi').combobox('reload', url_temp+'_km');
    }else{
    	$('#kabupaten').combobox('clear');
		$('#kecamatan').combobox('clear');
		$('#kelurahan').combobox('clear');
		$('#provinsi').combobox('clear');
    	$('#provinsi').combobox('reload', url_temp);
    	// console.log('bl');
    }
}
</script>
<div class="dialog" style="width: 900px; margin-top: 0px;">
	<div class="block">
		<p class="block-heading">Pendaftaran Kemitraan</p>
		<div class="block-body">
			
			<?php 
				if(isset($error)) {
					?>
					<div id="infoMessage" class="alert alert-error">Silakan lengkapi dan isi dengan benar data dengan tanda (<span style="color: red;">*</span>)</div>
					<?php
				}
				if(isset($error_username)) {
					?>
					<div id="infoMessage" class="alert alert-error">Silahkan coba username lain</div>
					<?php
				}
				if(isset($error_email)) {
					?>
					<div id="infoMessage" class="alert alert-error">Silahkan coba email lain</div>
					<?php
				}
				if(isset($error_npwp)) {
					?>
					<div id="infoMessage" class="alert alert-error">Silahkan coba npwp lain</div>
					<?php
				}
				if(isset($error_ktp)) {
					?>
					<div id="infoMessage" class="alert alert-error">Silahkan coba KTP lain</div>
					<?php
				}
			?>
			<?php echo form_open_multipart('pendaftaran/kemitraan/submit', array('id' => 'form-1'));?>
				<table width="100%" cellpadding="5">
				<tr>
						<td width="200px;"><label for="" style="text-align: right;">Status Pangajuan <span style="color: red;">*</span> :</label></td>
						<td>
							<input type="radio" id="status_pengajuan" name="status_pengajuan" onchange="pengajuan(this);" value="KM" style="width: 13px;" <?=set_radio('status_pengajuan', 'KM', TRUE) ?>/> Kemitraan 
							<input type="radio" id="status_pengajuan" name="status_pengajuan" onchange="pengajuan(this);" value="BL" style="width: 13px;" <?=set_radio('status_pengajuan', 'BL') ?> /> Bina Lingkungan 
						</td>
					</tr>
					<tr>
						<td width="200px;"><label for="" style="text-align: right;">Username<span style="color: red;">*</span> :</label></td>
						<td><input type="text" id="username" name="username" style="border-left-width: 3px; width: 100px;" value="<?=set_value('username')?>"></td>
					</tr>
					<tr>
						<td width="200px;"><label for="" style="text-align: right;">Nama Mitra <span style="color: red;">*</span> :</label></td>
						<td><input type="text" id="nama_vendor" name="nama_vendor" style="border-left-width: 3px; width: 400px;" placeholder="Contoh: Nama Perusahaan, PT" value="<?=set_value('nama_vendor')?>"></td>
					</tr>
					<tr>
						<td width="200px;"><label for="" style="text-align: right;">Provinsi <span style="color: red;">*</span> :</label></td>
						<td>
							<select class="easyui-combobox" id="provinsi" name="provinsi" style="width: 220px" panelHeight="100" url="<?=base_url('pendaftaran/kemitraan/get_provinsi_km')?>" valueField="id" textField="name" mode="remote"
							data-options="onSelect: function(rec){
								$('#kabupaten').combobox('clear');
								$('#kecamatan').combobox('clear');
								$('#kelurahan').combobox('clear');
								
								var url_temp = '<?=base_url('pendaftaran/kemitraan/get_kabupaten')?>/' + rec.id;
								$('#kabupaten').combobox('reload', url_temp);
							},onLoadSuccess: function(){
								var provinsi = '<?=set_value('provinsi')?>';
								if(provinsi != '') {
									$('#provinsi').combobox('setValue', provinsi);
									
									var url_temp = '<?=base_url('pendaftaran/kemitraan/get_kabupaten')?>/' + provinsi;
									$('#kabupaten').combobox('reload', url_temp);
								}
							}">
					        </select>
						</td>
					</tr>
					<tr>
						<td width="200px;"><label for="" style="text-align: right;">Kabupaten/Kota <span style="color: red;">*</span> :</label></td>
						<td>
							<select class="easyui-combobox" id="kabupaten" name="kabupaten" style="width: 220px" panelHeight="100" valueField="id" textField="name" mode="remote"
							data-options="onSelect: function(rec){
								$('#kecamatan').combobox('clear');
								$('#kelurahan').combobox('clear');
								
								var url_temp = '<?=base_url('pendaftaran/kemitraan/get_kecamatan')?>/' + rec.id;
								$('#kecamatan').combobox('reload', url_temp);
							},onLoadSuccess: function(){
								var kabupaten = '<?=set_value('kabupaten')?>';
								if(kabupaten != '') {
									$('#kabupaten').combobox('setValue', kabupaten);
									
									var url_temp = '<?=base_url('pendaftaran/kemitraan/get_kecamatan')?>/' + kabupaten;
									$('#kecamatan').combobox('reload', url_temp);
								}
							}">
					        </select>
						</td>
					</tr>
					<tr>
						<td width="200px;"><label for="" style="text-align: right;">Kecamatan <span style="color: red;">*</span> :</label></td>
						<td>
							<select class="easyui-combobox" id="kecamatan" name="kecamatan" style="width: 220px" panelHeight="100" valueField="id" textField="name" mode="remote"
							data-options="onSelect: function(rec){
								$('#kelurahan').combobox('clear');
								
								var url_temp = '<?=base_url('pendaftaran/kemitraan/get_kelurahan')?>/' + rec.id;
								$('#kelurahan').combobox('reload', url_temp);
							},onLoadSuccess: function(){
								var kecamatan = '<?=set_value('kecamatan')?>';
								if(kecamatan != '') {
									$('#kecamatan').combobox('setValue', kecamatan);
									
									var url_temp = '<?=base_url('pendaftaran/kemitraan/get_kelurahan')?>/' + kecamatan;
									$('#kelurahan').combobox('reload', url_temp);
								}
							}">
					        </select>
						</td>
					</tr>
					<tr>
						<td width="200px;"><label for="" style="text-align: right;">Kelurahan <span style="color: red;">*</span> :</label></td>
						<td>
							<select class="easyui-combobox" id="kelurahan" name="kelurahan" style="width: 220px" panelHeight="100" valueField="id" textField="name" mode="remote"
							data-options="onLoadSuccess: function(){
								var kelurahan = '<?=set_value('kelurahan')?>';
								if(kelurahan != '') {
									$('#kelurahan').combobox('setValue', kelurahan);
								}
							}">
					        </select>
						</td>
					</tr>
					<tr>
						<td width="200px;"><label for="" style="text-align: right;">Alamat <span style="color: red;">*</span> :</label></td>
						<td><textarea rows="3" id="alamat" name="alamat" style="border-left-width: 3px; width: 400px;"><?=set_value('alamat')?></textarea></td>
					</tr>
					<tr>
						<td width="200px;"><label for="" style="text-align: right;">No. KTP <span style="color: red;">*</span> :</label></td>
						<td><input type="text" id="ktp" name="ktp" style="border-left-width: 3px;" value="<?=set_value('ktp')?>"></td>
					</tr>
					<tr>
						<td width="200px;"><label for="" style="text-align: right;">NPWP <span style="color: red;">*</span> :</label></td>
						<td><input type="text" id="Npwp" name="Npwp" style="border-left-width: 3px;" value="<?=set_value('Npwp')?>"></td>
					</tr>
					<tr>
						<td width="200px;"><label for="" style="text-align: right;">No Telp <span style="color: red;">*</span> :</label></td>
						<td><input type="text" id="no_telp" name="no_telp" style="border-left-width: 3px;" value="<?=set_value('no_telp')?>"></td>
					</tr>
					
					<tr>
						<td width="200px;"><label for="" style="text-align: right;">Email <span style="color: red;">*</span> :</label></td>
						<td><input type="email" id="email" name="email" style="border-left-width: 3px;" value="<?=set_value('email')?>"></td>
					</tr>
					<tr>
						<td width="200px;"><label for="" style="text-align: right;">Kualifikasi <span style="color: red;">*</span> :</label></td>
						<td>
							<select class="easyui-combobox" id="Kualifikasi" name="Kualifikasi" style="width: 220px" panelHeight="100" url="<?=base_url('pendaftaran/kemitraan/get_kualifikasi')?>" valueField="id" textField="kualifikasi"
								data-options="onLoadSuccess: function(){
								$('#Kualifikasi').combobox('setValue', '<?=set_value('Kualifikasi')?>');
							}">
					        </select>
						</td>
					</tr>
					<tr>
						<td width="200px;"><label for="" style="text-align: right;">Tipe Perusahaan <span style="color: red;">*</span> :</label></td>
						<td>
							<select class="easyui-combobox" id="tipe_perusahaan" name="tipe_perusahaan" style="width: 220px" panelHeight="100" url="<?=base_url('pendaftaran/kemitraan/get_tipe_perusahaan')?>" valueField="id" textField="tipe_perusahaan"
								data-options="onLoadSuccess: function(){
								$('#tipe_perusahaan').combobox('setValue', '<?=set_value('tipe_perusahaan')?>');
							}">
					        </select>
						</td>
					</tr>
					<tr>
						<td width="200px;"></td>
						<td>Note <span style="color: red;">*</span> : harus diisi.</td>
							
					</tr>
					<tr>
						<td colspan="2"><button class="btn btn-primary pull-right" type="submit" style="height: 30;margin-left:-3px;"><i class="icon-ok-sign"></i> Simpan</button></td>
					</tr>
				</table>
				
				<div>
					
				</div>
			</form>
		</div>
	</div>
</div>