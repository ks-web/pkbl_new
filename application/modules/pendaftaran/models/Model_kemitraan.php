<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');
	Class Model_kemitraan extends CI_Model {
		function __construct() {
			parent::__construct();
		}

		// public function get_vendor_code($status_pengajuan){
		// 	$car=$$status_pengajuan;
  //       $q = $this->db->query("SELECT LPAD(RIGHT(vendor_code,3)+1,3,0) as vendor_code FROM mas_vendor WHERE vendor_code LIKE DATE_FORMAT(NOW(),'".$car."%Y%m%') ORDER BY vendor_code DESC limit 1");
  //       return $q;
  //       }

		public function get_jenis_ijin_usaha(){
			$this->db->select('id_jenis_ijin, description');
			$this->db->from('mas_jenis_ijin');
			return $this->db->get()->result_array();
		}
		
		public function get_provinsi() {
			$this->db->select('id, name');
			$this->db->from('provinces');
			
			$q = $this->input->post('q');
			if($q){
				$this->db->like('name', $q);
			}
			
			return $this->db->get()->result_array();
		}
		public function get_provinsi_km() {
			$this->db->select('id, name');
			$this->db->where('id','32');
			$this->db->or_where('id','36');
			$this->db->from('provinces');
			
			$q = $this->input->post('q');
			if($q){
				$this->db->like('name', $q);
			}
			
			return $this->db->get()->result_array();
		}
				
		public function get_kabupaten($provinsi) {
			$this->db->select('id, name');
			$this->db->from('regencies');
			$this->db->where('province_id', $provinsi);
			
			$q = $this->input->post('q');
			if($q){
				$this->db->like('name', $q);
			}
			
			return $this->db->get()->result_array();
		}
		
		public function get_kecamatan($kabupaten) {
			$this->db->select('id, name');
			$this->db->from('districts');
			$this->db->where('regency_id', $kabupaten);
			
			$q = $this->input->post('q');
			if($q){
				$this->db->like('name', $q);
			}
			
			return $this->db->get()->result_array();
		}
		
		public function get_kelurahan($kecamatan) {
			$this->db->select('id, name');
			$this->db->from('villages');
			$this->db->where('district_id', $kecamatan);
			
			$q = $this->input->post('q');
			if($q){
				$this->db->like('name', $q);
			}
			
			return $this->db->get()->result_array();
		}
		
		public function get_kualifikasi() {
			$this->db->select('id, kualifikasi');
			$this->db->from('mas_kualifikasi');
			
			$q = $this->input->post('q');
			if($q){
				$this->db->like('kualifikasi', $q);
			}
			
			return $this->db->get()->result_array();
		}
		
		public function get_tipe_perusahaan() {
			$this->db->select('id, tipe_perusahaan');
			$this->db->from('mas_tipe_perusahaan');
			
			$q = $this->input->post('q');
			if($q){
				$this->db->like('tipe_perusahaan', $q);
			}
			
			return $this->db->get()->result_array();
		}
		
		public function check_username($username) {
			$this->db->select('username');
			$this->db->from('sys_user');
			$this->db->where('username', $username);
			return $this->db->get()->result_array();
		}
		
		public function check_email($email) {
			$this->db->select('email');
			$this->db->from('sys_user');
			$this->db->where('email', $email);
			return $this->db->get()->result_array();
		}
		
		public function check_npwp($npwp) {
			$this->db->select('Npwp');
			$this->db->from('mas_vendor');
			$this->db->where('Npwp', $npwp);
			return $this->db->get()->result_array();
		}
		public function check_ktp($ktp) {
			$this->db->select('ktp');
			$this->db->from('mas_vendor');
			$this->db->where('ktp', $ktp);
			return $this->db->get()->result_array();
		}
		
		public function insert_vendor($data){
			$this->db->insert('mas_vendor', $data); 
		}
		
		public function insert_user($data){
			$this->db->insert('sys_user', $data); 
		}        

		public function getsektormitra($filter)
		{
			$this->db->select('A.*');
			$this->db->from('sektor_kemitraan A');
			//$this->db->where($filter);

			$query = $this->db->get();

			return $query->result_array();
		}

	}
