<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');
	class Kemitraan extends CI_Controller {

		function __construct() {
			parent::__construct();
			$this->load->helper('cookie');
			$this->load->model('Model_kemitraan', 'kemitraan');
		}

		public function index() {
			$this->template->load('single', 'kemitraan', array());
		}

		public function submit() {
			$this->load->library('form_validation');
			$this->form_validation->set_rules('nama_vendor', 'Nama Vendor', 'required');
			$this->form_validation->set_rules('provinsi', 'Provinsi', 'required');
			$this->form_validation->set_rules('kabupaten', 'Kabupaten', 'required');
			$this->form_validation->set_rules('kecamatan', 'Kecamatan', 'required');
			$this->form_validation->set_rules('kelurahan', 'Kelurahan', 'required');
			$this->form_validation->set_rules('alamat', 'Alamat', 'required');
			$this->form_validation->set_rules('ktp', 'ktp', 'required');
			$this->form_validation->set_rules('Npwp', 'NPWP', 'required');
			$this->form_validation->set_rules('no_telp', 'No Telp', 'required');
			$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
			$this->form_validation->set_rules('Kualifikasi', 'Kualifikasi', 'required');
			$this->form_validation->set_rules('tipe_perusahaan', 'Tipe Perusahaan', 'required');
			$this->form_validation->set_rules('status_pengajuan', 'status_pengajuan', 'required');

			if ($this->form_validation->run() == FALSE) {
				$this->template->load('single', 'kemitraan', array('error' => true));
			} else {
				// check username if available
				$username = strtolower($this->input->post('username'));
				$q_check = $this->kemitraan->check_username($username);
				if (count($q_check) > 0) {
					$this->template->load('single', 'kemitraan', array('error_username' => true));
					return false;
				}
				// Check email is availailable
				$email = strtolower($this->input->post('email'));
				$q_check = $this->kemitraan->check_email($email);
				if (count($q_check) > 0) {
					$this->template->load('single', 'kemitraan', array('error_email' => true));
					return false;
				}
				// Check npwp duplikat
				$npwp = $this->input->post('Npwp');
				$q_check = $this->kemitraan->check_npwp($npwp);
				if (count($q_check) > 0) {
					$this->template->load('single', 'kemitraan', array('error_npwp' => true));
					return false;
				}

				// Check ktp duplikat
				$ktp = $this->input->post('ktp');
				$q_check = $this->kemitraan->check_ktp($ktp);
				if (count($q_check) > 0) {
					$this->template->load('single', 'kemitraan', array('error_ktp' => true));
					return false;
				}
				// collect data
				$nama_vendor=strtoupper($this->input->post('nama_vendor'));
				$data_vendor = array(
					'vendor_code' => $this->get_vendor_code($this->input->post('status_pengajuan')),
					'nama_vendor' => $nama_vendor,
					'provinsi' => $this->input->post('provinsi'),
					'kabupaten' => $this->input->post('kabupaten'),
					'kecamatan' => $this->input->post('kecamatan'),
					'kelurahan' => $this->input->post('kelurahan'),
					'alamat' => $this->input->post('alamat'),
					// 'Kota' => $this->input->post('Kota'),
					'no_telp' => $this->input->post('no_telp'),
					// 'no_fax' => $this->input->post('no_fax'),
					'email' => $email,
					// 'Status_perusahaan' => $this->input->post('Status_perusahaan'),
					'ktp' => $ktp,
					'Npwp' => $npwp,
					'Kualifikasi' => $this->input->post('Kualifikasi'),
					'status_aktif' => '1',
					'tipe_perusahaan' => $this->input->post('tipe_perusahaan'),
					'status_pengajuan' => $this->input->post('status_pengajuan')
				);

				// insert data vendor
				$this->kemitraan->insert_vendor($data_vendor);

				// collect data user
				$vendor_code = $data_vendor['vendor_code'];
				$pass = $this->random_character();

				$data_user = array(
					'username' => $username,
					'email' => $email,
					'userpass' => password_hash($pass, PASSWORD_DEFAULT),
					'group_id' => USER_LEVEL_VENDOR_DEFAULT,
					'vendor_code' => $vendor_code,
					'aktif' => '1'
				);

				//insert user
				$this->kemitraan->insert_user($data_user);

				// Sending email
				$content_mail = $this->load->view('mail_info_pendaftaran', array(
					'username' => $username,
					'password' => $pass
				), TRUE);

				$this->load->library('pmailer');
				$mailer = $this->pmailer->mail_sent(array($email), 'Informasi Pendaftaran kemitraan', $content_mail);

				// $mailer = $this->mail_sent(array($email), 'Informasi Pendaftaran kemitraan',
				// $content_mail);

				$this->template->load('single', 'kemitraan_submited', array());
			}
		}

		public function get_jenis_ijin_usaha() {
			$ret = $this->kemitraan->get_jenis_ijin_usaha();

			if (sizeof($ret) > 0) {
				$ret[0]['selected'] = true;
			}

			echo json_encode($ret);
		}

		public function get_provinsi() {
			$ret = $this->kemitraan->get_provinsi();

			echo json_encode($ret);
		}
		public function get_provinsi_km(){
			$ret = $this->kemitraan->get_provinsi_km();

			echo json_encode($ret);
		}

		public function get_kabupaten($provinsi) {
			$ret = $this->kemitraan->get_kabupaten($provinsi);

			echo json_encode($ret);
		}

		public function get_kecamatan($kabupaten) {
			$ret = $this->kemitraan->get_kecamatan($kabupaten);

			echo json_encode($ret);
		}

		public function get_kelurahan($kecamatan) {
			$ret = $this->kemitraan->get_kelurahan($kecamatan);

			echo json_encode($ret);
		}

		public function get_kualifikasi() {
			$ret = $this->kemitraan->get_kualifikasi();

			echo json_encode($ret);
		}

		public function get_tipe_perusahaan() {
			$ret = $this->kemitraan->get_tipe_perusahaan();

			echo json_encode($ret);
		}

		public function mail_sent($mailTo = array(), $subj = '', $content = '') {
			require_once (APPPATH . 'libraries/PHPMailerAutoload.php');

			$mail = new PHPMailer();

			$mail->IsSMTP();
			$mail->Host = mail_Host;
			$mail->SMTPAuth = true;
			$mail->Username = mail_Username;
			$mail->Password = mail_Password;
			$mail->SMTPSecure = mail_SMTPSecure;
			$mail->Port = mail_Port;
			// $mail->SMTPDebug = 4;

			$mail->SetFrom(mail_Username, mail_AppName . PERUSAHAAN);
			$mail->Subject = $subj;
			$mail->IsHTML(TRUE);

			$mail->MsgHTML($content);

			if (sizeof($mailTo) > 0) {
				for ($i = 0; $i < sizeof($mailTo); $i++) {
					$mail->AddAddress($mailTo[$i]);
				}
			}

			if ($mail->Send()) {
				$data["status"] = true;
				$data["message"] = "Message sent correctly!";
			} else {
				$data["status"] = false;
				$data["message"] = "Error: " . $mail->ErrorInfo;
			}
			return $data;
		}

		private function random_character($length = 6) {
			$char = user_PasswordPool;
			$string = '';

			for ($i = 0; $i < $length; $i++) {
				$pos = rand(0, strlen($char) - 1);
				$string .= $char{$pos};
			}

			return $string;
		}
		private function get_vendor_code($nama) {
			$char = strtoupper(substr($nama, 0, 2));
			// $date=date("Ym");
			$seq = '0000';

			$this->db->select('vendor_code as last_vendor_code', FALSE);
			$this->db->like('vendor_code', $char, 'after');
			$query = $this->db->get('mas_vendor');
			$row = $query->row_array();


			$sec = substr($row['last_vendor_code'], 8,11)+1;
			$long_temp = strlen($seq);
    		$long_sec  = strlen($sec);

			return $char.substr($seq, 0,$long_temp-$long_sec).$sec;  		

		}

	}
