<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends MY_Controller {
     function __construct()
    {
        parent::__construct();
        $path_file = './assets/captcha/';
        delete_files($path_file);
        
        $this->load->model('home/model_home');
        $this->load->model('model_menu');
        $this->load->model('model_dashboard');
    }
    function index()
    {
      /** $data = array('idProyek'=>'1',
                      'getDueDateIssue'=>$this->getDueDateIssue(date("Y-m-d"),1),
                      'getDueDateTomorrowIssue'=>$this->getDueDateIssue(date("Y-m-d", time() + 60 * 60 * 24),1),
                      'getIssueUpdate'=>$this->getIssueUpdate(date("Y-m-d"),1),
                      'getIssueYesterdayUpdate'=>$this->getIssueUpdate(date("Y-m-d", time() - 60 * 60 * 24),1)
        );*/
        // $query = $this->model_home->getGroupMenuTombol($this->session->userdata('group_id'));
          // $row = $query->row();
         // $data   = array(
                        // 'add'        => $row->add,
                        // 'edit'       => $row->edit,
                        // 'delete'     => $row->delete,
                        // 'approve'    => $row->approve
                      // );
        // $this->session->set_userdata($data);
        $data['menu']   = $this->model_menu->getAllMenu();
        $this->template->load('template','view_dashboard',$data);
    }

    
   
}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */