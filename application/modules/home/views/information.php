<script type="text/javascript">
	$('#tabs-info').tabs({
		width : '100%'
	}); 
	
	function detail_tender_act(idx){
		$('#dg_tender_public').datagrid('selectRow', idx);
		var row = $('#dg_tender_public').datagrid('getSelected');
		if(row) {
			$('#dlg_tender_public').dialog('open').dialog('setTitle', 'Detail');
			$('#fm').form('load', row);
			$('#isi').html(row.isi);
		}
	}
	
	
	function detail_tender(value,row,index){
		return '<button class="btn btn-warning btn-small" onclick="detail_tender_act('+index+')">Detail</button>';
	}
</script>
<div>
	<div id="tabs-info" class="easyui-tabs" style="height:450px;">
		<div title="Selamat Datang" style="padding:10px">
			<center><strong><h4>SELAMAT DATANG DI APLIKASI PKBL</h4></strong></center>
			<p>PKBL adalah bentuk tanggung jawab Badan Usaha Milik Negara (BUMN) kepada masyarakat. PKBL dilaksanakan dengan dasar UU No.19 tahun 2003 ttg BUMN serta Peraturan Menteri BUMN No. Per-05/MBU/2007 yang menyatakan maksud dan tujuan pendirian BUMN tidak hanya mengejar keuntungan melainkan turut aktif memberikan bimbingan dan bantuan kepada pengusaha golongan ekonomi lemah, koperasi dan masyarakat.</p>
			<p>PKBL merupakan Program Pembinaan Usaha Kecil dan pemberdayaan kondisi lingkungan oleh BUMN melalui pemanfaatan dana dari bagian laba BUMN. Jumlah penyisihan laba untuk pendanaan program maksimal sebesar 2% (dua persen) dari laba bersih untuk Program Kemitraan dan maksimal 2% (dua persen) dari laba bersih untuk Program Bina Lingkungan.</p>
			<p>Program Kemitraan BUMN dengan Usaha Kecil, yang selanjutnya disebut Program Kemitraan, yaitu program untuk meningkatkan kemampuan usaha kecil agar menjadi tangguh dan mandiri melalui pemanfaatan dana dari bagian laba BUMN</p>
			<p>Tujuan program Kemitraan adalah untuk meningkatkan kemampuan para pengusaha kecil agar menjadi tangguh dan mandiri sekaligus pemberdayaan kondisi sosial masyarakat.</p>
			<p>Sedangkan Program Bina Lingkungan, yang selanjutnya disebut Program BL, yaitu program untuk membentuk calon Mitra Binaan baru dan pemberdayaan kondisi sosial masyarakat oleh BUMN melalui pemanfaatan dana dari bagian laba BUMN. Program BL ini bersifat bantuan (Korban Bencana Alam, Bantuan Pendidikan dan/atau Pelatihan, Bantuan Peningkatan Kesehatan, Bantuan Pengembangan Sarana dan/atau Prasarana dan Bantuan Sarana Ibadah).</p>
		</div>
	</div>
</div>