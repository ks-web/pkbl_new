<script type="text/javascript">
	function cekuser() {
		var user = $('#user').val();
		$.post('<?php echo base_url("home/cekuser") ?>',{user:user},function(result){
			if(result.success){
				$('#form-forgot').hide();
				$('.alert-danger').hide();
				$('#success').html('Link reset password (valid selama 2 jam) sudah dikirim ke alamat '+result.email);
				$('.alert-success').slideDown();
			}else{
				$('.alert-success').hide();
				$('#danger').html(result.msg);
				$('.alert-danger').slideDown();
			}
		},'json');
	}
</script>
<div class="dialog">
	<div class="block">
		<p class="block-heading"><i class="icon-lock icon-large"></i> Reset Password</p>
		<div class="block-body">
			<?php
			if(isset($link_invalid)) {
				?>
			<div class="alert alert-danger">
				<button type="button" class="close" onclick="$(this).parent().slideUp()"><span title="Close">&times;</span></button>
				<p>Link sudah kadaluarsa/tidak valid, silahkan kirim permintaan reset password kembali.</p>
			</div>
				<?php
			}
			?>
			<div class="alert alert-info">
				<p>Masukan username atau email, agar kami bisa mengirimkan email untuk mereset password anda.</p>
			</div>
			<div class="alert alert-success" style="display:none;">
			  <button type="button" class="close" onclick="$(this).parent().slideUp()"><span title="Close">&times;</span></button>
			  <span id="success"></span>
			</div>
			<div class="alert alert-danger" style="display:none;">
			  <button type="button" class="close" onclick="$(this).parent().slideUp()"><span title="Close">&times;</span></button>
			  <strong>Warning!</strong>&nbsp;&nbsp;&nbsp;<span id="danger"></span>
			</div>

			<div id="form-forgot">
				<label for="user">Username or E-mail :</label>
				<input type="text" id="user" name="user" required="true" style="width:69%;height: 25px; font-size: large;">
				<button class="btn btn-primary pull-right" type="submit" onclick="cekuser()" style="height: 30;margin-left:-3px;"><i class="icon-ok-sign"></i> Send Email</button>
			</div>

		</div>
	</div>
</div>