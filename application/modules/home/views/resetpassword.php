<script type="text/javascript">
	function resetpassword() {
		$('.alert-danger').slideUp();
		
		var userpass = $('#userpass').val();
		var re_userpass = $('#re-userpass').val();
		
		if(userpass === re_userpass){
			if(userpass.length < 6){
				$('#danger').html('Password minimal 6 karakter.');
				$('.alert-danger').slideDown();
			} else {
				$.post('<?php echo base_url("home/resetpassword/".$code) ?>',{userpass:userpass},function(result){
					if(result.success){
						$('.alert-danger').hide();
						$('#userpass').val('');
						$('#success').html('Ganti password berhasil silahkan kembali login <a href="<?php echo base_url(); ?>">Klik disini</a>');
						$('.alert-success').slideDown();
						$('#form-reset').hide();
					}else{
						$('.alert-success').hide();
						$('#danger').html(result.msg);
						$('.alert-danger').slideDown();
					}
				},'json');
			}
		} else {
			$('#danger').html('Password tidak sama.');
			$('.alert-danger').slideDown();
		}
		
	}
</script>
<div class="dialog">
	<?php
	if (!$cek)
	{
	?>
		<h1>Page Not Found</h1>
	<?php
	}else{
	?>
	<div class="block">
		<p class="block-heading"><i class="icon-lock icon-large"></i> Reset Password</p>
		<div class="block-body">
			<div class="alert alert-success" style="display:none;">
			  <button type="button" class="close" onclick="$(this).parent().slideUp()"><span title="Close">&times;</span></button>
			  <span id="success"></span>
			</div>
			<div class="alert alert-danger" style="display:none;">
			  <button type="button" class="close" onclick="$(this).parent().slideUp()"><span title="Close">&times;</span></button>
			  <span id="danger"></span>
			</div>
			<div id="form-reset">
				<div>
					<label for="userpass">New Password :</label>
					<input type="password" id="userpass" name="userpass" required="true" style="width:57%;height: 25px; font-size: large;">
				</div>
				<div>
					<label for="userpass">Re-Type Password :</label>
					<input type="password" id="re-userpass" required="true" style="width:57%;height: 25px; font-size: large;">
					<button class="btn btn-primary pull-right" type="submit" onclick="resetpassword()" style="height: 30;margin-left:-3px;"><i class="icon-ok-sign"></i> Change Password</button>
				</div>
			</div>
		</div>
	</div>
	<?php
	}
	?>
</div>