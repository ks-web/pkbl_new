<script type="text/javascript">
    function getCookie(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for(var i=0; i<ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0)==' ') c = c.substring(1);
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }
    $(document).ready(function(){
        var us = getCookie('us');
        $('#identity').val(us);
        var pa = getCookie('pa');
        $('#password').val(pa);
    });
</script>
<div class="">
    <div class="">
            <div class="">
            <?php
                if($message<>""){
                    echo '<div id="infoMessage" class="alert alert-info">'.$message.'</div>';
                }
            ?>
    		
            <?php 
            $attributs=array( 'class'=>'form-signin'); 
            echo form_open(base_url('home/validate_login'), $attributs); 
            ?>
            <div style="padding: 20px;">
            <table>
            <tr>
            <td colspan="2">
                <label for="identity">Nama Pengguna :</label>
				<?php echo form_input($username, '', 'class="easyui-validatebox" tipPosition="left" required="true" style="width: 100%; height: 25px; font-size: large;"');?> 
            </td>
            </tr>
            <tr>
            <td colspan="2">
                <label for="password">Kata Sandi :</label>
			     <?php echo form_input($password, '', 'class="easyui-validatebox" tipPosition="left" required="true" style="width: 100%; height: 25px; font-size: large;"');?>
             </td>
             </tr>
             <tr>
                <td colspan="2">
                <label for="captcha">Captcha :</label>
                <div style="width: 50%;">
                	<?php echo $captcha; ?>
                </div>
                <?php echo form_input("captcha", '', 'class="easyui-validatebox" tipPosition="left" required="true" style="width: 100%; height: 25px; font-size: large;"');?>
                </td>
                </tr>
                <!-- <tr>
                <td colspan="2">
                Remember Me&nbsp;:
                <?php echo form_checkbox( 'remember', '1', FALSE, 'id="remember" style="width: 13px"');?>
                </td>
                </tr> -->
                </table>
                <br />
				<a href="<?=base_url('home/resetpass')?>">Lupa Kata Sandi ?</a>  
                <br />
                <button class="btn btn-primary pull-right" value="Login" type="submit"><i class="icon-ok-sign"></i> Masuk</button>
            </div>
            <div>&nbsp;</div>
            <br />
            <?php echo form_close();?>
            </div>
        </div><h5>
            Bagi yang belum menjadi rekanan silahkan <a href="<?=base_url('pendaftaran/kemitraan')?>" class="btn btn-primary btn-small">Pendaftaran Kemitraan</a><!-- <a href="<?=base_url('registrasi/rekanan')?>">registrasi rekanan</a> --> terlebih dahulu.
            </h5>
            </div>