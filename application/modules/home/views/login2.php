﻿<?php  

if( $message != '')
{   
   
$style = '<div id="message-error"  class="message message-error">
		<div class="image">
			<img src="'.base_url('asset/images/icons/error.png').'" alt="Error" height="32" />
		</div>
		<div class="text">
			<h6>Error Message</h6>
			<span>'.$message.'</span>
		</div>
		<div class="dismiss">
			<a href="#message-error"></a>
		</div>
	</div>';
          
}else{
    $style='';
}

?>
<div style="font-size:16px;color: red;">
    <?php echo $this->session->flashdata('message');?><!-- Tangkap flash message -->  
</div>
<div class="inner">
	<?php echo form_open("home/validate_login");?>
	<div class="form">
		<!-- fields -->
		<div class="fields">
			<div class="field">
				<div class="label">
					<label for="username">Username:</label>
				</div>
				<div class="input">
					<?php echo form_input('username','','');?>
				</div>
			</div>
			<div class="field">
				<div class="label">
					<label for="password">Password:</label>
				</div>
				<div class="input">
					<?php echo form_password('password');?>
				</div>
			</div>
			<div class="field">
				<div class="checkbox">
					<?php echo form_checkbox('remember', '1', FALSE, 'id="remember"');?>
					<label for="remember">Remember me</label>
				</div>
			</div>
			<div class="buttons">
				<?php echo form_submit('submit', 'Login');?>
			</div>
            <?php echo form_close();?>            
		</div>
		<!-- end fields -->
		<!-- links -->
		<div class="links">
			<a href="forgot_password">Forgot your password?</a>
		</div>
		<!-- end links -->
	</div>
	</form>
</div>
<!-- end login -->