<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');
	class Home extends CI_Controller {

		function __construct() {
			parent::__construct();
			$this->load->model('model_home');
			$this->load->model('model_captcha', 'captcha');
			$this->load->helper('cookie');
		}

		public function index() {
			$this->data['title'] = "Login";
			$data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			$data['username'] = array(
				'name' => 'username',
				'id' => 'username',
				'type' => 'text',
				'value' => $this->form_validation->set_value('username'),
			);
			$data['password'] = array(
				'name' => 'password',
				'id' => 'password',
				'type' => 'password',
			);

			$data['captcha'] = $this->captcha->setCaptcha();

			if ($this->session->userdata('id') == '') {
				$this->template->load2('single_login', 'home/login', 'home/information', $data);
			} else {
				redirect(base_url('dashboard/index'));
			}
		}

		function waktu() {

			$query = $this->db->query("SELECT DATE_FORMAT(NOW(), '%T') AS waktu");
			$row = $query->row();

			if (isset($row)) {
				$waktu = explode(':', $row->waktu);
				$this->output->set_content_type('application/json')->set_output(json_encode(array(
					'status' => true,
					'jam' => $waktu[0],
					'menit' => $waktu[1],
					'detik' => $waktu[2]
				)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('status' => false)));
			}

		}

		function validate_login() {
			$data = array(
				'username' => $this->input->post('username'),
				'userpass' => $this->input->post('password'),
				'captcha' => $this->input->post('captcha')
			);

			$query = $this->model_home->validate($data);
		}

		function get_public_tender() {
			$order = $this->input->post('order') ? $this->input->post('order') : 'desc';
			$sort = $this->input->post('sort') ? $this->input->post('sort') : 'tgl';
			$page = $this->input->post('page') ? intval($this->input->post('page')) : 1;
			$rows = $this->input->post('rows') ? intval($this->input->post('rows')) : 10;
			$offset = (($page - 1) * $rows);

			$json['total'] = $this->db->get('berita_public_vd')->num_rows();

			$this->db->order_by($sort, $order);
			$this->db->limit($rows, $offset);
			$json['rows'] = $this->db->get("berita_public_vd")->result_array();

			$this->output->set_output(json_encode($json));
		}

		function logout() {
			$this->session->sess_destroy();
			delete_cookie('group_id');

			redirect('');
		}

		function resetpass($code = false) {
			if (!$code) {
				$this->template->load('single', 'resetpass');
			} else {
				$cek = $this->model_home->cekcode($code);
				if ($cek->num_rows() > 0) {
					$this->template->load('single', 'resetpassword', array(
						'cek' => $cek,
						'idUser' => $idUser,
						'code' => $code
					));
				} else {
					$this->template->load('single', 'resetpass', array('link_invalid' => true));
				}

			}
		}

		function resetpassword($code) {
			$cek = $this->model_home->cekcode($code);
			if ($cek->num_rows() > 0) {
				$row = $cek->row();
				$data = array(
					'forgot_pass_code' => $row->forgot_pass_code,
					'forgot_pass_time' => $row->forgot_pass_time,
					'userpass' => password_hash($this->input->post('userpass'), PASSWORD_DEFAULT)
				);
				$up = $this->model_home->resetpassword($data);
				if ($up) {
					$this->output->set_output(json_encode(array('success' => true)));
				} else {
					$this->output->set_output(json_encode(array('msg' => 'Update Failed')));
				}
			} else {
				$this->output->set_output(json_encode(array('msg' => 'Sorry Can Not Change Your Password')));
			}
		}

		function cekuser() {
			header("Content-Type: application/json");
			$user = $this->input->post('user');
			$get = $this->model_home->cekuser($user);
			if ($get->num_rows() > 0) {
				$row = $get->row();

				date_default_timezone_set("Asia/Jakarta");
				$time = time();

				$this->load->library('token');
				$code = md5($row->id . $this->token->getToken());

				$data = array(
					'id' => $row->id,
					'forgot_pass_code' => $code,
					'forgot_pass_time' => $time
				);

				$res = $this->db->update('sys_user', $data, array('id' => $row->id));

				// Sending email
				$content_mail = $this->load->view('mail_forgot_password', array('code' => $code), TRUE);
				$this->load->library('pmailer');
				$mailer = $this->pmailer->mail_sent(array($row->email), 'Reset Password', $content_mail);
				// $mailer = $this->mail_sent(array($row->email), 'Reset Password',
				// $content_mail);

				if ($mailer['status']) {
					$this->output->set_output(json_encode(array(
						'success' => true,
						'email' => $row->email
					)));
				} else {
					$this->output->set_output(json_encode(array('msg' => $mailer['message'])));
				}

			} else {
				$this->output->set_output(json_encode(array('msg' => 'User Not Found')));
			}
		}

		function set_homepage() {
			header("Content-Type: application/json");

			$url = $this->input->post('url');
			$this->model_home->set_homepage($url);
		}

		private function get_jml_data_vendor($table = null) {

			$vendor_code = $this->session->userdata('vendor_code');
			if ($table && $vendor_code) {
				if ($this->db->table_exists($table)) {
					$this->db->where('vendor_code', $vendor_code);
					$this->db->from($table);
					return $this->db->count_all_results();
				} else {
					return null;
				}
			}
		}

		private function dokumen_exp() {

			$vendor_code = $this->session->userdata('vendor_code');
			if ($vendor_code) {
				if ($this->db->table_exists('dokumen_exp_count_vd')) {
					$this->db->where('vendor_code', $vendor_code);
					return $this->db->get('dokumen_exp_count_vd')->row_array();
				} else {
					return null;
				}
			}
		}

		function ijin_usaha_exp() {
			$vendor_code = $this->session->userdata('vendor_code');
			$this->db->where('vendor_code', $vendor_code);
			$data = $this->db->get('dokumen_exp_ijin_usaha_vd')->result_array();
			echo json_encode($data);
		}

		function ijin_pajak_bulanan_exp() {
			$vendor_code = $this->session->userdata('vendor_code');
			$this->db->where('vendor_code', $vendor_code);
			$data = $this->db->get('dokumen_exp_pajak_bulanan_vd')->result_array();
			echo json_encode($data);
		}

		function vendor_komodity_exp() {
			$vendor_code = $this->session->userdata('vendor_code');
			$this->db->where('vendor_code', $vendor_code);
			$data = $this->db->get('dokumen_exp_vendor_komodity_vd')->result_array();
			echo json_encode($data);
		}

		function tes_mail() {

			// echo !extension_loaded('openssl')?"Not Available":"Available"; die();
			$this->load->library('pmailer');
			$mail = $this->pmailer->mail_sent(array('demo1@cahaya-abadi.net'), 'Fajar8', 'test email');
			echo $mail['message'];
		}

		function file_delete() {
			$file = 'assets/upload/rekanan/C001/ijinusaha/test.pdf';
			$this->load->library('common');
			$this->common->delete_file($file);
		}

		public function test_rest() {
			echo time();
			die();
			$data = array('id' => 1);

			$response = \Httpful\Request::delete('http://[::1]/rest_server/index.php/api/example/users')->expectsJson()->sendsJson()->body(json_encode($data))->send();
			echo var_dump($response->body);
		}

		public function test_rest_get() {
			$this->load->library('restcli');
			$opt = array(
				'method' => 'get',
				'url' => 'http://localhost/kes/rest_server/index.php/eproc/pegawai/list'
			);
			$response = $this->restcli->call($opt);
			$data = $response->body;
			echo var_dump($data->data[0]->nama_pegawai);
			die();

			echo var_dump($data->data->Document_Number);
		}

		public function test_rest_post() {
			$data = array(
				'nama' => 'fajar ISnandio',
				'sex' => 'L'
			);

			$response = \Httpful\Request::post('http://[::1]/rest_server/index.php/api/example/users')->expectsJson()->sendsJson()->body(json_encode($data))->send();
			echo var_dump($response->body);
		}

		function queue_mail() {
			$data = $this->model_home->queue_mail();
			$id = $data['id'];
			$email = $data['email_to'];
			$judul = $data['subject'];
			$isi = $data['content'];
			$lampiran = $data['attach_url'];

			$this->load->library('pmailer');
			$mail = $this->pmailer->mail_sent_queue(array($email), $judul, $isi, array($lampiran));
			if ($mail['status']) {
				$data = array(
					'status' => '1',
					'msg' => $mail['message']
				);

				$this->db->where('id', $id);
				$this->db->update('email_queue', $data);
			} else {
				$data = array(
					'status' => '2',
					'msg' => $mail['message']
				);

				$this->db->where('id', $id);
				$this->db->update('email_queue', $data);
			}
		}

		function queue_mail_ForError() {

			$count = $this->model_home->queue_mail_ForError();
			$i = sizeof($count);
			$ke = rand(0, $i - 1);
			$this->db->limit(1, $ke);
			$data = $this->model_home->queue_mail_ForError();
			echo var_dump($i);
			echo var_dump($data);
			die();

			$id = $data['id'];
			$email = $data['email_to'];
			$judul = $data['subject'];
			$isi = $data['content'];
			$lampiran = $data['attach_url'];

			$this->load->library('pmailer');
			$mail = $this->pmailer->mail_sent_queue(array($email), $judul, $isi, array($lampiran));
			if ($mail['status']) {
				$data = array(
					'status' => '1',
					'msg' => $mail['message']
				);

				$this->db->where('id', $id);
				$this->db->update('email_queue', $data);
			} else {
				$data = array(
					'status' => '2',
					'msg' => $mail['message']
				);

				$this->db->where('id', $id);
				$this->db->update('email_queue', $data);
			}
		}

	}
