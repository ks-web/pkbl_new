<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');
	Class Model_Home extends CI_Model {
		function __construct() {
			parent::__construct();
		}

		function validate($data) {
			$query_c = $this->valid_captcha();
			if ($query_c) {

				$this->db->select('a.*, c.group_name, d.linkaddress, a.vendor_code', false);
				$this->db->from('sys_user as a');
				$this->db->join('sys_group as c', 'a.group_id = c.group_id');
				$this->db->join('sys_menu as d', 'd.node=a.homepage', 'left');
				$this->db->where('a.username', $data['username']);
				$this->db->where('aktif', '1');

				$query = $this->db->get();

				if ($query->num_rows() == 1) {
					$row_user = $query->row();
					$this->set_session_login($row_user, $data);

				} else {
					// check with email
					$this->db->select('a.*, c.group_name, d.linkaddress', false);
					$this->db->from('sys_user as a');
					$this->db->join('sys_group as c', 'a.group_id = c.group_id');
					$this->db->join('sys_menu as d', 'd.node=a.homepage', 'left');
					$this->db->where('a.email', $data['username']);
					$this->db->where('aktif', '1');

					$query = $this->db->get();

					if ($query->num_rows() == 1) {
						$row_user = $query->row();
						$this->set_session_login($row_user, $data);

					} else {
						$this->session->set_flashdata('message', 'Username & Password Anda Salah !');
						//Kirim message ke form login $this->db->last_query()
						redirect('home/index/');
					}
				}

			} else {

				$this->session->set_flashdata('message', 'Kode Captcha Anda Tidak Sesuai !');
				//Kirim message ke form login
				redirect('home/index/');

			}

		}

		private function set_session_login($row_user, $data) {
			print_r($row_user);
			if (password_verify($data['userpass'], $row_user->userpass)) {

				$this->db->where('id', $row_user->id);
				$this->db->update('sys_user', array(
					'forgot_pass_code' => NULL,
					'forgot_pass_time' => NULL
				));

				$data = array(
					'username' => $row_user->username,
					'email' => $row_user->email,
					'id' => $row_user->id,
					'group_id' => $row_user->group_id,
					'logged_in' => TRUE,
					'group_name' => $row_user->group_name
				);

				$data['name_hello'] = 'Administrator';

				// check if vendor
				if ($row_user->vendor_code) {

					$data['vendor_code'] = $row_user->vendor_code;

					$this->db->select('nama_vendor');
					$this->db->from('mas_vendor');
					$this->db->where('vendor_code', $row_user->vendor_code);
					//$this->db->where('id_vendor', $row_user->vendor_code);
					$row_vendor = $this->db->get()->row();
					$data['name_hello'] = $row_vendor->nama_vendor;
				}

				// check if karyawan
				if ($row_user->idPegawai) {
					$data['idPegawai'] = $row_user->idPegawai;

					$this->db->select('nama_pegawai');
					$this->db->from('mas_pegawai');
					$this->db->where('idPegawai', $row_user->idPegawai);
					$row_pegawai = $this->db->get()->row();
					
					$data['name_hello'] = $row_pegawai->nama_pegawai;
					
					// load data form sikti
					/*$this->load->library('restcli');
					$opt = array(
						'method' => 'get',
						'url' => urlSikti . 'pegawai/list/nip/' . $data['idPegawai'],
					);
					$response = $this->restcli->call($opt);
					$data['name_hello'] = $response->body->data[0]->nama_pegawai;*/
				}

				$this->session->set_userdata($data);
				$exp = time() + $this->config->item('sess_expiration');
				$this->input->set_cookie(array(
					'name' => 'group_id',
					'value' => $this->session->userdata('group_id'),
					'expire' => $exp
				));

				if ($row_user->homepage == 0) {
					redirect(base_url('dashboard'));
				} else {
					redirect(base_url($row_user->linkaddress));
				}
			} else {
				$this->session->set_flashdata('message', 'Username & Password Anda Salah !');
				//Kirim message ke form login $this->db->last_query()
				redirect('home/index/');
			}
		}

		function valid_captcha() {
			$expiration = time() - 60;
			// 1 minutes
			$this->db->query("DELETE FROM captcha WHERE captcha_time < " . $expiration);
			$path_file = './assets/captcha/';

			// Then see if a captcha exists:
			$sql = "SELECT COUNT(*) AS count FROM captcha WHERE word = ? AND ip_address = ? AND captcha_time > ?";
			$binds = array(
				$_POST['captcha'],
				$this->input->ip_address(),
				$expiration
			);
			// echo var_dump($binds);
			// die();
			$query = $this->db->query($sql, $binds);
			$row = $query->row();
			if ($row->count == 0) {
				return false;
			} else {
				delete_files($path_file);
				return true;
			}
		}

		function change_pass($idUser, $pwd_lama, $data) {
			$this->db->where('idUser', $idUser);
			$this->db->where('userpass', $pwd_lama);
			$get = $this->db->get('sys_user');
			if ($get->num_rows() > 0) {
				$this->db->where('idUser', $idUser);
				$this->db->where('userpass', $pwd_lama);
				$up = $this->db->update('sys_user', $data);
				if ($up) {
					$this->output->set_output(json_encode(array('success' => true)));
				}
			} else {
				$this->output->set_output(json_encode(array('msg' => 'Your password incorrect')));
			}
		}

		function getGroupMenuTombol($level) {
			$this->db->where('group_id', $level);
			$query = $this->db->get('sys_group');

			return $query;
		}

		function cekuser($user) {
			$this->db->where('username', $user);
			$this->db->or_where('email', $user);
			$get = $this->db->get('sys_user');

			return $get;
		}

		function cekcode($code) {
			date_default_timezone_set("Asia/Jakarta");

			$time_valid = time() - 7200;
			// valid for 2 hours
			$this->db->where('forgot_pass_code', $code);
			$this->db->where('forgot_pass_time >', $time_valid);
			$get = $this->db->get('sys_user');

			return $get;
		}

		function resetpassword($data) {
			$forgot_pass_code = $data['forgot_pass_code'];
			$forgot_pass_time = $data['forgot_pass_time'];
			$data['forgot_pass_code'] = NULL;
			$data['forgot_pass_time'] = NULL;

			$this->db->where('forgot_pass_code', $forgot_pass_code);
			$this->db->where('forgot_pass_time', $forgot_pass_time);
			$up = $this->db->update('sys_user', $data);

			return $up;
		}

		function set_homepage($url) {
			$idUser = $this->session->userdata('id');
			$sys_menu = $this->db->get('sys_menu')->result();
			$data = false;
			foreach ($sys_menu as $row) {
				if (strpos($url, $row->linkaddress)) {
					$data = array('homepage' => $row->node);
				}
			}
			if ($data) {
				$this->db->where('id', $idUser);
				$set = $this->db->update('sys_user', $data);
				if ($set) {
					$this->output->set_output(json_encode(array('success' => true)));
				} else {
					$this->output->set_output(json_encode(array('msg' => 'Failed To Set Homepage')));
				}
			} else {
				$this->output->set_output(json_encode(array('msg' => 'URL Not Found')));
			}
		}

		function queue_mail() {
			$this->db->select('*');
			$this->db->where('status = ', '0');
			$this->db->or_where('status = ', null);
			$this->db->limit(1);
			$this->db->order_by('id','ASC');
			$data = $this->db->get('email_queue_vd')->row_array();
			return $data;
		}
		function queue_mail_ForError() {
			$this->db->select('*');
			$this->db->where('status = ', '2');
			$this->db->order_by('id','ASC');
			$data = $this->db->get('email_queue_vd')->result_array();
			return $data;
		}

	}
