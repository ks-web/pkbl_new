<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');
	class Keyperson extends MY_Controller {
		public $models = array('keyperson');

		function __construct() {
			parent::__construct();
			$this->load->model('model_menu');
		}

		public function index() {

			$data = array(
				// 'title' => 'Administrasi User Key Person',
				// 'fullname' => $this->session->userdata('fullname'),
				// 'idPerusahaan' => $this->session->userdata('idPerusahaan'),
				'menu' => $this->model_menu->getAllMenu(),
				// 'group_nya' => $this->session->userdata('group_name')
			);
			$this->template->load('template', 'administrasi/view_keyperson', $data);
		}

		public function read() {
			$this->where_add['id <>'] = 1;
			// $this->where_add['group_id ='] = USER_LEVEL_PEGAWAI_DEFAULT;
			$this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read($this->where_add));
		}

		public function read_all() {
			$this->where_add['id <>'] = 1;
			// $this->where_add['group_id ='] = USER_LEVEL_PEGAWAI_DEFAULT;
			$this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read_all($this->where_add));
		}

		public function create() {
			// additional block
			$user_type = $this->input->post('user_type');
			if ($user_type == 'Pegawai') {
				$check = json_decode($this->{$this->models[0]}->read_one(array('idPegawai' => $this->input->post('idPegawai'))));
				if (!$check) {
					NULL;
				} else {
					$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'User untuk pegawai ini sudah dibuat.')));
					return false;
				}
			} else {
				$check = json_decode($this->{$this->models[0]}->read_one(array('vendor_code' => $this->input->post('vendor_code'))));
				if (!$check) {
					NULL;
				} else {
					$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'User untuk vendor ini sudah dibuat.')));
					return false;
				}
			}

			$check = json_decode($this->{$this->models[0]}->read_one(array('email' => $this->input->post('email'))));
			if (!$check) {
				$check = json_decode($this->{$this->models[0]}->read_one(array('username' => $this->input->post('username'))));
				if (!$check) {
					// collect data user
					$username = strtolower($this->input->post('username'));
					$email = strtolower($this->input->post('email'));
					$pass = $this->random_character();
					$userpass = password_hash($pass, PASSWORD_DEFAULT);
					$idPegawai = $this->input->post('idPegawai');
					$vendor_code = $this->input->post('vendor_code');
					$group_id = $this->input->post('group_id');

					$data_user = array(
						'username' => $username,
						'email' => $email,
						'userpass' => $userpass,
						'group_id' => $group_id,
						'aktif' => $this->input->post('aktif')
					);
					if ($user_type == 'Pegawai') {
						$data_user['idPegawai'] = $idPegawai;
					} else if ($user_type == 'Vendor') {
						$data_user['vendor_code'] = $vendor_code;
					}

					//insert user
					$ret = $this->{$this->models[0]}->insert_user($data_user);

					$content_mail = $this->load->view('mail_info_user', array(
						'username' => $username,
						'password' => $pass
					), TRUE);

					$this->load->library('pmailer');
					$mailer = $this->pmailer->mail_sent(array($email), 'Informasi User Login', $content_mail);
					// $mailer = $this->mail_sent(array($email), 'Informasi User Login',
					// $content_mail);
					if ($ret) {
						$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
					} else {
						$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => $this->db->_error_message())));

					}
				} else {
					$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'username sudah dipakai, coba username lain.')));
					return false;
				}

			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'email sudah dipakai, coba email lain.')));
				return false;
			}
		}

		public function update() {
			// die();
			// additional block

			// addtional data
			$this->data_add['email'] = $this->input->post('email');
			$this->data_add['group_id'] = $this->input->post('group_id');

			// additional where

			$this->where_add['id'] = $this->uri->segment(4);

			$result = $this->keyperson->update($this->where_add, $this->data_add);
			if ($result == 1) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'Data Gagal Di Update !')));
			}
		}

		public function delete() {
			// additional where
			$this->where_add['id'] = $this->uri->segment(4);

			$result = $this->keyperson->delete($this->where_add);
			if ($result == 1) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'Data Gagal Di Hapus !')));
			}
		}

		// additional function
		function getJsonpeg() {
			header("Content-Type: application/json");
			$search = $this->input->post('q');

			// load data form sikti
			$this->load->library('restcli');
			$opt = array(
				'method' => 'get',
				'url' => urlSikti . 'pegawai/list/search/' . $search,
			);
			$response = $this->restcli->call($opt);

			// $this->{$this->models[0]}->getJsonpeg();
			$this->output->set_output(json_encode($response->body->data));
		}

		function getJsonven() {
			header("Content-Type: application/json");
			$this->{$this->models[0]}->getJsonven();
		}

		function getGroup($class = null) {
			if ($class) {
				header("Content-Type: application/json");
				$this->{$this->models[0]}->getGroup($class);
			} else {
				show_404();
			}
		}

		function mail_sent($mailTo = array(), $subj = '', $content = '') {
			require_once (APPPATH . 'libraries/PHPMailerAutoload.php');
			$mail = new PHPMailer();
			$mail->IsSMTP();
			$mail->SMTPAuth = true;
			$mail->SMTPSecure = mail_SMTPSecure;
			$mail->Port = mail_Port;
			$mail->Host = mail_Host;
			$mail->Username = mail_Username;
			$mail->Password = mail_Password;
			$mail->SetFrom(mail_Username, mail_AppName . PERUSAHAAN);
			$mail->Subject = $subj;
			$mail->IsHTML(TRUE);

			$mail->MsgHTML($content);

			if (sizeof($mailTo) > 0) {
				for ($i = 0; $i < sizeof($mailTo); $i++) {
					$mail->AddAddress($mailTo[$i]);
				}
			}

			if ($mail->Send()) {
				$data["success"] = true;
				$data["message"] = "Message sent correctly!";
			} else {
				$data["success"] = true;
				$data["message"] = "Error: " . $mail->ErrorInfo;
			}
			return $data;
		}

		private function random_character($length = 8) {
			$char = user_PasswordPool;
			$string = '';

			for ($i = 0; $i < $length; $i++) {
				$pos = rand(0, strlen($char) - 1);
				$string .= $char{$pos};
			}

			return $string;
		}

		public function create1() {
			$this->load->library('form_validation');
			$this->form_validation->set_error_delimiters('<div id="infoMessage" class="alert alert-error">', '</div>');
			$this->form_validation->set_rules('email', 'Email', 'required|valid_email', array(
				'required' => '%s harus diisi.',
				'valid_email' => 'Format %s salah!'
			));

			if ($this->form_validation->run() == FALSE) {
				$this->template->load('single', 'rekanan', array());
			} else {

				// collect data
				$data_pegawai = array(
					'username' => $this->input->post('username'),
					'email' => $this->input->post('email'),
					'idPegawai' => $this->input->post('idPegawai'),
					// 'jenis_ijinusaha' => $this->input->post('jenis_ijinusaha'),
					// 'no_ijinusaha' => $this->input->post('no_ijinusaha'),
					// 'nm_pimpinan' => $this->input->post('nm_pimpinan'),
					// 'jabatan_pimpinan' => $this->input->post('jabatan_pimpinan'),
					// 'no_akte_prshn' => $this->input->post('no_akte_prshn'),
					// 'tgl_akte_prshn' => $this->input->post('tgl_akte_prshn'),
					// 'no_surat_kuasa' => $this->input->post('no_surat_kuasa'),
					// 'berlaku_srt_kuasa' => $this->input->post('berlaku_srt_kuasa') == '' ? null :
					// $this->input->post('berlaku_srt_kuasa'),
					// 'nama_notaris_akta' => $this->input->post('nama_notaris_akta'),
					'aktif' => '1'
				);

				// Upload file
				// $config['upload_path'] = 'assets/upload/rekanan/';
				// $config['file_name'] = strtolower($data_vendor['vendor_code']).'_akta';
				// $this->upload->initialize($config);
				// $this->upload->set_allowed_types('*');
				//
				// if (!$this->upload->do_upload('file_akta_pendirian')) {
				// $data_vendor['file_akta_pendirian'] = '';
				// } else {
				// $file = $this->upload->data();
				// $data_vendor['file_akta_pendirian'] = $config['upload_path'] .
				// $file['file_name'];
				// }

				// insert data vendor
				$this->keyperson->insert_user($data_pegawai);

				// collect data user
				$username = strtolower($data_pegawai['username']);
				$email = strtolower($data_pegawai['email']);
				$pass = $this->random_character();
				$idPegawai = $this->input->post('idPegawai');
				$data_user = array(
					'username' => $username,
					'email' => $email,
					'userpass' => password_hash($pass, PASSWORD_DEFAULT),
					'group_id' => USER_LEVEL_VENDOR_DEFAULT,
					'aktif' => '1',
					'idPegawai' => $idPegawai
				);

				//insert user
				$this->keyperson->insert_user($data_user);

				// Sending email
				// $content_mail = "Pendaftaran keyperson Berhasil<br />" . "Username anda
				// :<strong>" . $username . "</strong><br />" . "Password anda :<strong>" . $pass
				// . "</strong><br />" . "Silahkan melengkapi data-data perusahaan anda.<br />" .
				// "Terima kasih telah mendaftar.";
				$content_mail = $this->load->view('mail_info_pegawai', array(
					'username' => $username,
					'password' => $pass
				), TRUE);

				$mailer = $this->mail_sent(array($email), 'Informasi Pendaftaran keyperson', $content_mail);

				$this->template->load('template', 'administrasi/view_keyperson');
			}
		}

	}
