<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');
class ubahpassword extends CI_Controller {
	function __construct() {
		parent::__construct();
		if (!$this->session->userdata('logged_in') == 1) {
			redirect(base_url('home'));
		}
		$this->load->model('model_ubahpassword');
		$this->load->model('model_menu');
	}

	public function index() {

		$data = array(
			// 'fullname' => $this->session->userdata('fullname'),
			'menu' => $this->model_menu->getAllMenu(),
			// 'group_nya' => $this->session->userdata('group_name')
		);
		$this->template->load('template', 'administrasi/view_ubahpassword', $data);
	}

	function update() {

		if (trim($this->input->post('userpass')) <> trim($this->input->post('userpass2'))) {
			$this->output->set_output(json_encode(array('msg' => 'Password 1 & Password 2 Tidak Sama')));
			return false;
		}
		$pass = $this->input->post('userpass');
		if(strlen($pass) < 6){
			$this->output->set_output(json_encode(array('msg' => 'Password minimal 6 karakter.')));
			return false;
		}

		$data = array(
			'old' => $this->input->post('old'),
			'userpass' => $this->input->post('userpass'),
			'userpass2' => $this->input->post('userpass2'),
			'tgl_modifier' => date('Y-m-d h:i:s'),
			'modifier' => $this->session->userdata('idUser')
		);

		$this->model_ubahpassword->getSave($data);
	}

}
