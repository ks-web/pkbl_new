<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');
	class AdminMenu extends MY_Controller {
		public $models = array('adminmenu');
		
		public function __construct() {
			parent::__construct();
		}

		public function index() {
			$data = array();
			$data['menu'] = $this->model_menu->getAllMenu();

			$this->template->load('template', 'view_adminmenu', $data);
		}

		public function read() {
			$this->output->set_content_type('application/json')->set_output($this->adminmenu->read());
		}

		public function read_all() {
			$this->output->set_content_type('application/json')->set_output($this->adminmenu->read_all());
		}

		public function create() {
			// additional block
			if ($this->input->post('newparent')) {
				$parentnode = $this->input->post('newparent');
			} else {
				$parentnode = $this->input->post('parentnode');
				$cekCC = $this->adminmenu->cekCC('node', $parentnode, 'sys_menu');
				if (trim($cekCC) == 0) {
					$this->output->set_output(json_encode(array('msg' => ' Parent Node Tidak Terdaftar')));
					return false;
				}
			}
			
			// addtional get
			$this->data_add['parentnode'] = $parentnode;
			
			$result = $this->adminmenu->insert($this->data_add);
			if ($result == 1) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => $this->db->_error_message())));
			}
		}

		public function update() {
			// additional block
			if ($this->input->post('newparent')) {
				$parentnode = $this->input->post('newparent');
			} elseif ($this->input->post('parentnode') == 1) {
				$parentnode = 1;
			} else {
				$parentnode = $this->input->post('parentnode');
				$cekCC = $this->adminmenu->cekCC('node', $parentnode, 'sys_menu');
				if (trim($cekCC) == 0) {
					$this->output->set_output(json_encode(array('msg' => ' Parent Node Tidak Terdaftar')));
					return false;
				}
			}
			
			// addtional data
			$this->data_add['parentnode'] = $this->input->post('parentnode');
			
			// additional where
			$this->where_add['node'] = $this->uri->segment(4);
			
			$result = $this->adminmenu->update($this->where_add, $this->data_add);
			if ($result == 1) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'Data Gagal Di Update !')));
			}
		}

		public function delete() {
			// additional where
			$this->where_add['node'] = $this->uri->segment(4);
			
			$result = $this->adminmenu->delete($this->where_add);
			if ($result == 1) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'Data Gagal Di Hapus !')));
			}
		}

		// additional function
		function getParentMenu() {
			$cari = $this->input->post('q') ? $this->input->post('q') : '';
			header("Content-Type: application/json");
			$this->adminmenu->getParentMenu($cari);
		}

	}
