<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');
	class Group_Menu extends MY_Controller {
		function __construct() {
			parent::__construct();
			$this->load->model('model_menu');
			$this->load->model('home/model_home');
			$this->load->model('model_group_menu');
			$this->load->model('administrasi/model_adminmenu');

		}

		public function index() {
			$level = $this->uri->segment(4) ? $this->uri->segment(4) : $this->session->userdata('group_id');
			$data = array(
				'fullname' => $this->session->userdata('fullname'),
				'menu' => $this->model_menu->getAllMenu(),
			);

			// $data['Otorisasi'] = $this->model_home->getGroupMenuTombol($level);
			$data['combo_Group'] = $this->model_adminmenu->getGroupMenu();
			// $data['menu_parent'] = $this->model_group_menu->getMenuByLevel($level);
			$data['level'] = $level;

			$this->template->load('template', 'view_group_menu', $data);

		}

		function update() {
			// $id = $this->input->post('group_id');
			// foreach ($this->input->post(NULL, TRUE) as $a => $b) {
			// $arr = explode('_', $a);
			// if ($arr[0] == 'node') {
			// $data[] = array(
			// 'group_id' => $id,
			// 'node' => $arr[1],
			// 'read' => $this->get_role_val($b, 'read'),
			// 'add' => $this->get_role_val($b, 'add'),
			// 'edit' => $this->get_role_val($b, 'edit'),
			// 'delete' => $this->get_role_val($b, 'delete'),
			// );
			//
			// }
			// }

			$this->add_group_role(false);

			// $otorisasi = array(
			// 'add' => $this->input->post('add'),
			// 'edit' => $this->input->post('edit'),
			// 'delete' => $this->input->post('delete'),
			// 'approve' => $this->input->post('approve')
			// );
			//
			// $this->model_group_menu->editOtorisasi($id, $otorisasi);

		}

		function getGroup() {
			$this->model_group_menu->getGroup();
		}

		function menu_read() {
			$level = $this->uri->segment(4) ? $this->uri->segment(4) : $this->session->userdata('group_id');
			$this->model_group_menu->getMenuTree($level);
		}

		private function get_role_val($arr = array(), $role) {
			$ret = '0';
			if (in_array($role, $arr, true)) {
				$ret = '1';
			}
			return $ret;
		}

		private function add_group_role($mode = true) {
			$data_ins = array();
			$group_id = $this->input->post('group_id');
			$data_row = $this->model_group_menu->getMenu(array('linkaddress !=' => '#'));

			foreach ($data_row as $row) {
				if ($mode) {
					// section jika terjadi penambahan group user langsung insert group link agar
					// masuk ke akses denied
				} else {
					$data_post = $this->input->post('node_' . $row['node']);
					if ($data_post) {
						$data_ins[] = array(
							'group_id' => $group_id,
							'node' => $row['node'],
							'read' => $this->get_role_val($data_post, 'read'),
							'add' => $this->get_role_val($data_post, 'add'),
							'edit' => $this->get_role_val($data_post, 'edit'),
							'delete' => $this->get_role_val($data_post, 'delete'),
						);
					} else {
						$data_ins[] = array(
							'group_id' => $group_id,
							'node' => $row['node'],
							'read' => '0',
							'add' => '0',
							'edit' => '0',
							'delete' => '0'
						);
					}

				}
			}
			$this->model_group_menu->editgroup_menu($group_id, $data_ins);
		}

		function test($level, $node) {
			echo var_dump($this->model_group_menu->getMenuTreeChildRole($level, $node));
		}

	}
