<script>
	function add() {
		$('#dlg').dialog('open').dialog('setTitle', 'Tambah Group');
		$('#fm').form('clear');
		$('.edit').hide();
		$('#tbl').html('Save');
		url = '<?=base_url('administrasi/group_user/create')?>';
	}
	
	function edit() {
		var row = $('#dg').datagrid('getSelected');
		if (row) {
			$('#tbl').html('Edit');
			$('#dlg').dialog('open').dialog('setTitle', 'Edit Group');
			$('.edit').hide();
			
			$('#fm').form('load', row);
			url = '<?=base_url('administrasi/group_user/update')?>/' + row.group_id;
		} else {
			$.messager.alert('Error Message', 'Anda belum memilih data!', 'error');
		}
	}
	
	function hapus() {
		var row = $('#dg').datagrid('getSelected');
		if (row) {
			$.messager.confirm('Confirm', 'Apakah Anda Yakin Akan Menghapus ini?', function(r) {
				if (r) {
					$.post('<?=base_url('administrasi/group_user/delete')?>/' + row.group_id,
						function(result) {
							if (result.success) {
								$('#dg').datagrid('reload'); // reload the user data
							} else {
	
								$.messager.alert('Error Message', result.msg, 'error');
							}
						}, 'json');
				}
			});
		} else {
			$.messager.alert('Error Message', 'Anda belum memilih data!', 'error');
		}
	}
	
	function save() {
		$('#fm').form('submit', {
			url: url,
			onSubmit: function() {
				return $(this).form('validate');
			},
			success: function(result) {
				var result = eval('(' + result + ')');
				if (result.success) {
					$('#dlg').dialog('close');
					$('#dg').datagrid('reload');
					//$('#dg').treegrid('reload');
	
				} else {
					//$('#dlg').dialog('close');
					$.messager.alert('Error Message', result.msg, 'error');
				}
			}
		});
	}
	
	function doSearch(value) {
		$('#dg').datagrid('load', {
			q: value
		});
	}
</script>

<div class="tabs-container">
	<table id="dg" class="easyui-datagrid"
	url= "<?=base_url("administrasi/group_user/read") ?>"
	singleSelect="true"
	iconCls="icon-cogs"
	rownumbers="true"
	idField="group_id"
	pagination="true"
	fitColumns="true"
	pageList= [10,20,30]
	toolbar="#toolbar"
	title="Group User"
	>
		<thead>
			<th field="group_name" width="200" sortable="true">Group Name</th>
			<th field="group_class" sortable="true" width="75" >Group Class</th>
		</thead>
	</table>

	<!-- Model Start -->
	<div id="dlg" class="easyui-dialog" style="width:450px; height:350px; padding:10px" closed="true" buttons="#t_dlg_dis-buttons" >
		<fieldset style="padding: 10px;">
			<form id="fm" method="post" enctype="multipart/form-data" action="">

				<table width="100%" border="0" cellpadding="5px">
					<tr>
						<td>Group Name</td>
						<td>:</td>
						<td>
						<input id="group_name" name="group_name" style="width:250px;"/>
						</td>
					</tr>
					<tr>
						<td>Group Class</td>
						<td>:</td>
						<td>
							<select class="easyui-combobox" id="group_class" name="group_class" style="width:100%;">
				                <option value="Pegawai">Pegawai</option>
				                <option value="Mitra">Mitra</option>
				            </select>
						</td>
					</tr>
				</table>
			</form>
			<div id="toolbar">
				<table align="center" style="padding: 0px; width: 99%;">
					<tr>
						<td>
							<a href="javascript:void(0)" class="btn btn-small btn-success" onclick="javascript:add()"><i class="icon-plus-sign icon-large"></i>&nbsp;Add</a>
							<a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:edit()"><i class="icon-edit"></i>&nbsp;Edit</a>
							<a href="javascript:void(0)" class="btn btn-small btn-danger" onclick="javascript:hapus()"><i class="icon-trash icon-large"></i>&nbsp;Delete</a></td>
						<td>&nbsp;</td>
						<td align="right">
						<input class="easyui-searchbox" prompt="Silahkan Input Field" searcher="doSearch"  style="width:300px"/>
						</td>
					</tr>
				</table>
			</div>
			<div id="t_dlg_dis-buttons">
				<a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:save()"><i class="icon-save icon-large"></i>&nbsp;Simpan</a>
				<a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:$('#dlg').dialog('close')"><i class="icon-remove icon-large"></i>&nbsp;Batal</a>
			</div>
		</fieldset>
	</div>
	<!-- Model end -->
</div>