<script type="text/javascript">
    function save_password(){
        $('#fm').form('submit',{
            url: '<?=base_url('administrasi/ubahpassword/update')?>',
            onSubmit: function(){
                return $(this).form('validate');
            },
            success: function(result){
            var result = eval('('+result+')');
            if (result.success){
                $.messager.alert('Info Message',result.success,'info');
                $('#fm').form('reset');
            } else {
                $.messager.alert('Error Message',result.msg,'error');
                }
            }
        });
    }
</script>

<div class="container">
<fieldset style="padding: 5px;" >  
<form id="fm" method="post">
    <table  border="0" cellpadding="5px">
    <tr>
        <td>&nbsp;</td>
    </tr>
       <tr> 
            <td width="150px">Password Lama</td>
            
            <td>: <input type="password" name="old" id="old" style="width: 195px;" class="easyui-validatebox" required="true"/></td>
       </tr>
       <tr> 
           <td>Password Baru</td>
           
           <td>: <input type="password" name="userpass" id="userpass1" style="width: 195px;" class="easyui-validatebox" required="true"/></td>
       </tr>
       <tr> 
            <td>Re-Password Baru</td>
           
            <td>: <input type="password" name="userpass2" id="userpass2" style="width: 195px;" class="easyui-validatebox" required="true"/></td>  
       </tr>
       <tr>
            <td>&nbsp;</td>
            <td>
                <div id="t_dlg_dis-buttons">
                        <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:$('#dlg').dialog('close')"><i class="icon-remove icon-large"></i>&nbsp;Batal</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-success" onclick="javascript:save_password()"><i class="icon-save icon-large"></i>&nbsp;Simpan</a>      
                </div>
            </td>
       </tr>
                
    </table>  
    
    
</fieldset> 
</div>