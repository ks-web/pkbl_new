<script>

    $('#1').attr('selected', true);

    function add(){
        $('#dlg').dialog('open').dialog('setTitle','Tambah Menu Aplikasi');
    	$('#fm').form('clear');
        $('#tbl').html('Save');
    	url = '<?=base_url('administrasi/adminmenu/create')?>';
    }
    
    function edit(){
        var row = $('#dg').datagrid('getSelected');
    	if (row){
            $('#tbl').html('Edit');
            $('#dlg').dialog('open').dialog('setTitle','Edit Menu Aplikasi');
            $('#parentnode').combogrid('clear');
            $('#fm').form('load',row);
    		url = '<?=base_url('administrasi/adminmenu/update')?>/'+row.node;
    	}else{
    	   $.messager.alert('Error Message','Anda belum memilih data yang di Edit !','error');
    	}
    }
    
    function hapus(){
        var row = $('#dg').datagrid('getSelected');		
        if (row){
			$.messager.confirm('Confirm','Apakah Anda Yakin Akan Menghapus ini?',function(r){
				if (r){
					$.post('<?=base_url('administrasi/adminmenu/delete')?>/'+row.node,function(result){
						if (result.success){
							$('#dg').datagrid('reload');	// reload the user data
						} else {
							
                            $.messager.alert('Error Message',result.msg,'error');
						}
					},'json');
				}
			});
		}
    }
    function save(){
	    $('#fm').form('submit',{
	    	url: url,
	    	onSubmit: function(){
	    		return $(this).form('validate');
	    	},
	    	success: function(result){
	    		var result = eval('('+result+')');
	    		if (result.success){
	    			$('#dlg').dialog('close');		
	    			$('#dg').datagrid('reload');
	                //$('#dg').treegrid('reload');
	                
	    		} else {
	    		 //$('#dlg').dialog('close');
	             $.messager.alert('Error Message',result.msg,'error');
	    		}
	    	}
	    });
	}
	function doSearch(value){
	    $('#dg').datagrid('load',{    
	    	q:value  
	    });   
	}
    
</script>
 
<div class="tabs-container">                
<table id="dg" class="easyui-datagrid" 
    url= "<?=base_url("administrasi/adminmenu/read")?>"
	singleSelect="true" 
    iconCls="icon-cogs" 
    rownumbers="true"  
    idField="node" 
    pagination="true"
    fitColumns="true"
    pageList= [10,20,30]
    toolbar="#toolbar"
    title="Administrasi Menu"
>
        <thead>
            <th field="node" width="75" sortable="true">ID Menu</th>
            <th field="parentnode" sortable="true" width="75" >Parent</th>
            <th field="sortid" width="75" sortable="true" >Urutan</th>
            <th field="displaytext" sortable="true" width="300" >Nama Menu</th>
            <th field="linkaddress" width="500" sortable="true">Link Address</th>
            <th field="stat" width="100" formatter="status" align="center">Status</th>
        </thead>
 </table>
 
<!-- Model Start -->
<div id="dlg" class="easyui-dialog" style="width:450px; height:300px; padding:10px" closed="true" buttons="#t_dlg_dis-buttons" >
    <fieldset style="padding: 10px;">
    <form id="fm" method="post" enctype="multipart/form-data" action="">
        
        <table width="100%" border="0" cellpadding="5px">
            <tr>
                <td>Parent Menu </td>
                <td>:</td>
                <td align="left">
                <input class="easyui-combogrid" style="width:250px" id="parentnode"
    			name="parentnode"
    			data-options="
                panelWidth: 500,
                idField: 'node',
                mode:'remote',
                textField: 'displaytext',
    			url:'<?=base_url('administrasi/adminmenu/getParentMenu')?>',
                columns: [[
                	{field:'node',title:'Node',width:30},
                	{field:'displaytext',title:'Nama Menu',width:120}
                ]],
                fitColumns: true
    			">
                <br>
                <input style="width:auto;" type="checkbox" name="newparent" id="newparent" value="1">New Parent
                </td>
            </tr>
            <tr>
                <td> Nama Menu</td>
                <td>:</td>
                <td><input id="displaytext" name="displaytext" style="width:250px;"/></td>
            </tr>
            <tr>
                <td> Urutan </td>
                <td>:</td>
                <td><input id="sortid" name="sortid" style="width:100px;"/></td>
            </tr>
            <tr>
                <td> Link Address </td>
                <td>:</td>
                <td><input id="linkaddress" name="linkaddress" style="width:250px;"/></td>
            </tr>
            <tr>
                <td> Status Aktif</td>
                <td>:</td>
                <td><input type="checkbox" id="stat" name="stat" value="1" style="width:auto;" /></td>
            </tr>
        </table>
    </form>
    <div id="toolbar">  
            <table align="center" style="padding: 0px; width: 99%;">
                <tr>
                    <td>
                        <a href="javascript:void(0)" class="btn btn-small btn-success" onclick="javascript:add()"><i class="icon-plus-sign icon-large"></i>&nbsp;Add</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:edit()"><i class="icon-edit"></i>&nbsp;Edit</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-danger" onclick="javascript:hapus()"><i class="icon-trash icon-large"></i>&nbsp;Delete</a>
                    </td>
                    <td>&nbsp;</td>
                    <td align="right">
                    	<input class="easyui-searchbox" prompt="Silahkan Input Field" searcher="doSearch"  style="width:300px"/>
                    </td>
                </tr>
            </table>  
        </div>  
         <div id="t_dlg_dis-buttons">
            <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:save()"><i class="icon-save icon-large"></i>&nbsp;Simpan</a>
            <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:$('#dlg').dialog('close')"><i class="icon-remove icon-large"></i>&nbsp;Batal</a>      
        </div>
    </fieldset>
</div>
<!-- Model end -->  
</div>