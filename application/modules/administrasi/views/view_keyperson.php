<script>
    function add(){
        $('#dlg').dialog('open').dialog('setTitle','Tambah User');
        $('#fm').form('clear');
        $('#user_type_peg').prop('checked', true);
        $('#tbl').html('Save');
        $('.add').show();
        $('.Vendor').hide();
        url = '<?=base_url('administrasi/keyperson/create')?>';
    }
    
    function edit(){
        var row = $('#dg').datagrid('getSelected');
        if (row){
            $('#dlg').dialog('open').dialog('setTitle','Edit User');
            $('#group_id').combobox('reload', '<?=base_url('administrasi/keyperson/getGroup')?>/'+row.group_class);
            $('#fm').form('load',{email: row.email, group_id: row.group_id, aktif: row.aktif});
            $('.add').hide();
            url = '<?=base_url('administrasi/keyperson/update')?>/'+row.id;
        }
    }
    
    function hapus(){
        var row = $('#dg').datagrid('getSelected');     
            if (row){
                $.messager.confirm('Confirm','Apakah Anda Yakin Akan Menghapus ini?',function(r){
                    if (r){
                        $.post('<?=base_url('administrasi/keyperson/delete')?>/'+row.id,function(result){
                            if (result.success){
                                $('#dg').datagrid('reload');    // reload the user data
                            } else {
                                $.messager.show({   // show error message
                                    title: 'Error',
                                    msg:result.msg
                                });
                            }
                        },'json');
                    }
                });
            }
    }
    
    function doSearch(value){
        $('#dg').datagrid('load',{    
        q:value  
        });   
    }
    
    
</script>
 
<script type="text/javascript">
var url;
function save(){
    $('#fm').form('submit',{
        url: url,
        onSubmit: function(){
            return $(this).form('validate');
        },
        success: function(result){
            var result = eval('('+result+')');
            if (result.success){
                $('#dlg').dialog('close');      
                $('#dg').datagrid('reload');
                
            } else {
             $('#dlg').dialog('close');
                $.messager.alert('Error Message',result.msg,'error');
            }
        }
    });
}
function usertipe(obj){
	$('.'+$(obj).val()).show();
	if($(obj).val() == 'Vendor') {
		$('.Pegawai').hide();
	} else {
		$('.Vendor').hide();
	}
	$('#group_id').combobox('reload', '<?=base_url('administrasi/keyperson/getGroup')?>/'+$(obj).val()).combobox('clear');
}

</script>

<div class="container_s">                
<table id="dg" class="easyui-datagrid" 
    url= "<?=base_url("administrasi/keyperson/read")?>"
    singleSelect="true" 
    iconCls="icon-cogs" 
    rownumbers="true"  
    idField="id" 
    pagination="true"
    fitColumns="true"
    pageList= [10,20,30]
    toolbar="#toolbar"
    title="Daftar Key Person"
>
        <thead>
            <th field="username" width="100" >User Name</th>
            <th field="email" width="100" >Email</th>
            <th field="group_name" width="150">Group User</th>
            <th field="group_class" width="150">Group Class</th>
            <th field="aktif" width="50"  formatter="status" align="center">Status</th>
        </thead>
 </table>
 
<!-- Model Start -->
<div id="dlg" class="easyui-dialog" style="width:550px; height:300px; padding:10px" closed="true" buttons="#t_dlg_dis-buttons" >
    <fieldset style="padding: 7px;">
    <form id="fm" method="post" enctype="multipart/form-data" action="">
    
        <table width="100%" align="center" border="0">
        	<tr class="add">
                <td>User Type</td>
                <td>: <input type="radio" id="user_type_peg" name="user_type" value="Pegawai" onchange="usertipe(this)" style="width: 10px;"/> Pegawai
                	<input type="radio" id="user_type_ven" name="user_type"  value="Vendor" onchange="usertipe(this)" style="width: 10px;"/> Vendor
                </td>
            </tr>
            <tr class="Pegawai add">
                <td> Nama Pegawai</td>
                <td>: <select name="idPegawai" id="Pegawai" class="easyui-combogrid" style="width:300px;" data-options="
                        panelWidth:300,
                        value:'idPegawai',
                        idField:'idPegawai',
                        textField:'nama_pegawai',
                    	mode:'remote',
                        url:'<?=base_url('administrasi/keyperson/getJsonpeg')?>',
                        onSelect :function(indek,row)
                        {
                            $('#email').val(row.email);
                          },
                        columns:[[
                        {field:'idPegawai',title:'ID Pegawai',width:100},
                        {field:'nama_pegawai',title:'Nama Pegawai',width:100},
                        {field:'Divisi',title:'Divisi',width:195}
                        ]]
                        "></select>
                </td>
            </tr>
            <tr class="Vendor add" style="display: none;">
                <td> Nama Vendor</td>
                <td>: <select name="vendor_code" id="vendor_code" class="easyui-combogrid" style="width:300px;" data-options="
                        panelWidth:300,
                        value:'vendor_code',
                        idField:'vendor_code',
                        textField:'nama_vendor',
                     	mode:'remote',
                        url:'<?=base_url('administrasi/keyperson/getJsonven')?>',
                        onSelect :function(indek,row)
                        {
                            $('#email').val(row.email);
                          },
                        columns:[[
                        {field:'vendor_code',title:'Vendor Code',width:100},
                        {field:'nama_vendor',title:'Nama Vendor',width:200}
                        ]]
                        "></select>
                </td>
            </tr>
            <tr>
            <td> Group Level User</td>
                <td>: <input id="group_id" class="easyui-combobox" 
                name="group_id"
                data-options="
                        url:'<?=base_url('administrasi/keyperson/getGroup/Pegawai')?>',
                        valueField:'group_id',
                        textField:'group_name',
                        panelHeight:'auto'
                ">
                
            </tr>
            <tr class="add">
                <td>User Name</td>
                <td>: <input type="text"  name="username" id="username" ></td>
            </tr>
            <tr >
            <td>Email</td>
                <td>: <input type="text" id="email" name="email" value="1" style="width:auto;"  />
                </td>
            </tr>
            <!-- <tr class="pwd">
                <td> Password</td>
                <td>: <input type="password" id="userpass" name="userpass1" style="width:250px;" /></td>
            </tr>
            <tr class="pwd">
                <td> Re Password</td>
                <td>: <input type="password" id="userrepass" name="userpass2" style="width:250px;"/></td>
            </tr> -->
            <tr >
            <td> Status Aktif</td>
                <td>: <input type="checkbox" id="aktif" name="aktif" value="1" style="width:auto;" /></td>
            </tr>
            
            </table>
            </fieldset>
    </form>
    <?=$this->load->view('toolbar')?>
</div>
<!-- Model end -->   



</div>