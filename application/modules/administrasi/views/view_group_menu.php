<style>
input[type='checkbox']{
    width: 10px;
}
</style>
<script>
$(document).ready(function () {
    $("#btn_menu").click(function(){
    	
      $.ajax({
        type: 'POST',
        url: '<?=base_url('administrasi/group_menu/update')?>',
        cache: false,
        data: $("#fm").serializeArray(),
        success: function (result) {
            var result = eval('('+result+')');
            if (result.success){
                $.messager.alert('Info Message','Data Berhasil Disimpan','info');
            }
            else {
                $.messager.alert('Error Message',result.msg,'error');
            }
          }
        });
    });
    
    $('#tree_menu').tree({
    	url: '<?=base_url('administrasi/group_menu/menu_read/'.$level)?>',
    	onlyLeafCheck: true,
    	lines:true,
    	method:'get',
    	animate:true,
    	checkbox:true,
		formatter:function(node){
			var data_node = node.id.split('_');
			if  (node.attributes.input == '2'){
				var data_node = node.id.split('_');
				var checked_text = '';
				if(node.checked){
					checked_text = 'checked';
				}
				return node.text + ' <input style="display:none;" type="checkbox" id="' + node.id + '" name="node_' + data_node[1] + '[]" value="' + data_node[0] + '" ' + checked_text + '/>';
			}
			else if  (node.attributes.input == '3'){
				var data_node = node.id.split('_');
				return node.text + ' <input style="display:none;" type="checkbox" id="' + node.id + '" name="node_' + data_node[1] + '[]" value="' + data_node[0] + '"/>';
			}
			else {
				return node.text;
			}
		},
		
		onCheck: function(node, checked){
			if(checked) {
				$('#'+node.id).prop('checked', true);
			}
			else {
				$('#'+node.id).prop('checked', false);
			}
		}
	});
});
function batal(){
    location.href = '<?=base_url('administrasi/group_menu/index')?>/'+$('#group_id').combobox('getValue');
};
</script>
<div class="tabs-container">
<form id="fm" method="post">
<fieldset style="padding: 5px;background: white;">
<table cellpadding="5px">
    <tr>
        <td width='120px'><i class="icon-group"></i> Group Level </td>
        <td width='1px'>:</td>
        <td>
        <select id="group_id" class="easyui-combobox" name="group_id" data-options="
            editable:false,
            onSelect: function(rec){
                    location.href = '<?=base_url('administrasi/group_menu/index')?>/'+$('#group_id').combobox('getValue');
                    }"
            >
        <?php
            foreach($combo_Group as $row){
                $level    = $this->uri->segment(4)?$this->uri->segment(4):$this->session->userdata('group_id');
                if(trim($level)==$row->group_id){$selected ='selected';}else{$selected ='';}
                echo "<option value='".$row->group_id."' ".$selected.">".$row->group_name."</option>";
            }
        ?>
             
        </select>
        </td>
    </tr>
</table>

<?php
    // $row=$Otorisasi->row();
    // $add = ($row->add =='1') ? 'checked':'';
    // $edit = ($row->edit =='1') ? 'checked':'';
    // $delete = ($row->delete =='1') ? 'checked':'';
    // $approve = ($row->approve =='1') ? 'checked':'';
?>
<table cellpadding="5px">
    <!-- <tr>
        <td width='120px'>Tombol Otorisasi&nbsp;</td>
        <td width='1px'>:</td>
        <td><input type="checkbox" <?=$add?> id="add" name="add" value="1"></td><td>Tambah</td>
        <td><input type="checkbox" <?=$edit?> id="edit" name="edit" value="1" style="width: 10px;"></td><td>Edit</td>
        <td><input type="checkbox" <?=$delete?>  id="delete" name="delete" value="1" style="width: 10px;"></td><td>Hapus</td>
        <td><input type="checkbox" <?=$approve?>  id="approve" name="approve" value="1" style="width: 10px;"></td><td>Approve</td>
    </tr> -->
    <tr>
        <td colspan="2">&nbsp;</td>
        
        <td colspan="6">
            <a href="#" class="btn btn-small btn-success" id="btn_menu"><i class="icon-save icon-large"></i> Simpan</a>
            <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:batal()"><i class="icon-remove icon-large"></i>&nbsp;Batal</a>
        </td>
        
    </tr>
</table>
    
</fieldset>
<br />
<fieldset style="padding: 5px;background: white;">
    <ul id="tree_menu" class="easyui-tree"></ul>
</fieldset>
</form>
</div>