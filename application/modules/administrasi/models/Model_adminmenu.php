<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	Class Model_adminmenu extends MY_Model {
		function __construct() {
			parent::__construct();
			$this->_table = "sys_menu";
			// $this->_view = "sys_menu";
			$this->_order = 'asc';
			$this->_sort = 'node';
			$this->_page = 1;
			$this->_rows = 10;

			$this->_create = true;
			$this->_update = true;

			if ($this->uri->segment(4)) {
				$this->_filter = array('node' => $this->uri->segment(4));
			}
			if ($this->input->post('q')) {
				$this->_like = array(
					'displaytext' => $this->input->post('q'),
					'parentnode' => $this->input->post('q'),
					'linkaddress' => $this->input->post('q')
				);
			}

			$this->_param = array('node' => $this->input->post('node'));

			$this->_data = array(
				'sortid' => $this->input->post('sortid'),
				'displaytext' => $this->input->post('displaytext'),
				'linkaddress' => $this->input->post('linkaddress'),
				'modifier' => $this->session->userdata('nip'),
				'tgl_modifier' => date('Y-m-d', time()),
				'stat' => $this->input->post('stat')
			);
		}

		// additional function
		function getParentMenu($cari) {
			$this->db->like('CONCAT(node,displaytext)', $cari);
			$this->db->where('parentnode', '1');
			$jsonevent = array();
			$jsonevent['rows'] = $this->db->get('sys_menu')->result();
			$this->output->set_output(json_encode($jsonevent));
		}

		function cekCC($id_field, $pid, $table) {
			$this->db->where($id_field, $pid);
			$query = $this->db->get($table)->num_rows();
			return $query;
		}

		function getGroupMenu() {
			$level = $this->session->userdata('group_id');
			$this->db->where('group_id >=', $level);
			$this->db->order_by('group_id', 'asc');
			$query = $this->db->get('sys_group')->result();

			return $query;
		}

	}
