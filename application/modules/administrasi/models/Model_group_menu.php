<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	Class Model_Group_Menu extends CI_Model {
		function __construct() {
			parent::__construct();

		}

		function getMenuByLevel($id) {
			$this->db->where('stat', '1');
			$this->db->where('parentnode', '1');
			$query = $this->db->get('sys_menu');

			if ($query->num_rows() > 0) {
				$hsl = "<table border='0'  cellpadding='5'>
                        <tr valign='top'>";
				foreach ($query->result() as $row) {
					$hsl .= " 
                            <td style='border-right:0px;'><input type='checkbox' checked disabled='disabled' name='checkbox[]' " . $this->cekMenu($id, $row->node) . " value=" . $row->node . " ></td>
                            <td style='border-left:0px; border-right:0px'><b>" . $row->displaytext . " </b>" . $this->getMenuChild($id, $row->node) . "</td>
                        ";
				}
				$hsl .= "</tr></table>";
				return $hsl;
			} else {
				return array();
			}

		}

		function getMenuChild($id, $node) {
			$this->db->where('stat', '1');
			$this->db->where('parentnode', $node);
			$query = $this->db->get('sys_menu');

			if ($query->num_rows() > 0) {
				$hsl = "<table cellpadding='4px'>";
				foreach ($query->result() as $row) {
					$hsl .= " <tr>
                            <td width='12px'><input type='checkbox' name='checkbox[]' " . $this->cekMenu($id, $row->node) . " value=" . $row->node . " ></td>
                            <td>" . $row->displaytext . "</td>
                        </tr>";
				}
				$hsl .= "</table>";
				return $hsl;
			} else {
				return "";
			}

		}

		function cekMenu($level, $node) {
			$this->db->where('group_id', $level);
			$this->db->where('node', $node);
			$query = $this->db->get('sys_gr_link');

			if ($query->num_rows() > 0) {
				$row = 'checked';
			} else {
				$row = '';
			}

			return $row;
		}

		function editgroup_menu($id, $data) {
			$this->db->where('group_id', $id);
			$query = $this->db->delete('sys_gr_link');

			$this->db->insert_batch('sys_gr_link', $data);
			if ($this->db->affected_rows() > 0) {
				$this->output->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_output(json_encode(array('msg' => 'Data Gagal Di Update !')));
			}

		}

		function editOtorisasi($id, $otorisasi) {
			$this->db->where('group_id', $id);
			$update = $this->db->update('sys_group', $otorisasi);

			if ($update) {
				$this->output->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_output(json_encode(array('msg' => 'Data Gagal Di Update !')));
			}
		}

		function getGroup() {
			$level = $this->session->userdata('group_id');
			$this->db->where('group_id >=', $level);
			$this->db->order_by('group_id', 'asc');
			$query = $this->db->get('sys_group')->result();

			$this->output->set_output(json_encode($query));
		}

		function getMenuTree($level) {
			$this->db->where('stat', '1');
			$this->db->where('parentnode', '1');
			$query = $this->db->get('sys_menu');

			$tree = array();

			if ($query->num_rows() > 0) {
				foreach ($query->result() as $row) {
					$temp = array(
						'id' => $row->node,
						'text' => $row->displaytext,
						'attributes' => array('input' => '0')
					);
					$temp_child = $this->getMenuTreeChild($level, $row->node);
					if (sizeof($temp_child) > 0) {
						$temp['children'] = $temp_child;
						$temp['state'] = 'closed';
						array_push($tree, $temp);
					}
				}
			}

			$this->output->set_output(json_encode($tree));
		}

		function getMenuTreeChild($level, $node) {
			$this->db->where('stat', '1');
			$this->db->where('parentnode', $node);
			$this->db->order_by('sortid', 'ASC');
			$query = $this->db->get('sys_menu') ;

			$child = array();
			if ($query->num_rows() > 0) {
				foreach ($query->result() as $row) {
					$temp = array(
						'id' => $row->node,
						'text' => $row->displaytext,
						'attributes' => array('input' => '1')
					);
					$temp_role = $this->getMenuTreeChildRole($level, $row->node);
					if (sizeof($temp_role) > 0) {
						$temp['children'] = $temp_role;
						$temp['state'] = 'closed';
					}
					array_push($child, $temp);
				}
			}
			return $child;
		}

		function getMenuTreeChildRole($level, $node) {
			$this->db->where('group_id', $level);
			$this->db->where('node', $node);
			$query = $this->db->get('sys_gr_link');

			$role = array();

			if ($query->num_rows() > 0) {
				foreach ($query->result() as $row) {
					$temp = array(
						array(
							'id' => 'read_' . $row->node,
							'text' => 'Read',
							'checked' => $this->getMenuTreeChildChecked($row->read),
							'attributes' => array('input' => '2')
						),
						array(
							'id' => 'add_' . $row->node,
							'text' => 'Add',
							'checked' => $this->getMenuTreeChildChecked($row->add),
							'attributes' => array('input' => '2')
						),
						array(
							'id' => 'edit_' . $row->node,
							'text' => 'Edit',
							'checked' => $this->getMenuTreeChildChecked($row->edit),
							'attributes' => array('input' => '2')
						),
						array(
							'id' => 'delete_' . $row->node,
							'text' => 'Delete',
							'checked' => $this->getMenuTreeChildChecked($row->delete),
							'attributes' => array('input' => '2')
						)
					);

				}
			} else {
				$temp = array(
					array(
						'id' => 'read_' . $node,
						'text' => 'Read',
						'checked' => false,
						'attributes' => array('input' => '3')
					),
					array(
						'id' => 'add_' . $node,
						'text' => 'Add',
						'checked' => false,
						'attributes' => array('input' => '3')
					),
					array(
						'id' => 'edit_' . $node,
						'text' => 'Edit',
						'checked' => false,
						'attributes' => array('input' => '3')
					),
					array(
						'id' => 'delete_' . $node,
						'text' => 'Delete',
						'checked' => false,
						'attributes' => array('input' => '3')
					)
				);
			}
			$role = $temp;

			return $role;
		}

		function getMenuTreeChildChecked($parm) {
			$checked = false;

			if ($parm == 1) {
				$checked = true;
			}
			return $checked;
		}

		function getMenu($where = array()) {
			if (sizeof($where) > 0) {
				$this->db->where($where);
			}

			return $this->db->get('sys_menu')->result_array();
		}

	}
