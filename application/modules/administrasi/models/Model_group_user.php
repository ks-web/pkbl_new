<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	Class Model_group_user extends MY_Model {
		function __construct() {
			parent::__construct();
			$this->_table = "sys_group";
			$this->_view = "sys_group_vd";
			$this->_order = 'asc';
			$this->_sort = 'group_name';
			$this->_page = 1;
			$this->_rows = 10;

			$this->_create = true;
			$this->_update = true;

			// if ($this->uri->segment(4)) {
			// $this->_filter = array('no_ijin' => $this->uri->segment(4));
			// }
			if ($this->input->post('q')) {
				$this->_like = array(
					'group_name' => $this->input->post('q')
				);
			}

			// $this->_param = array('group_id' => $this->input->post('group_id'));

			$this->_data = array(
				'group_name' => $this->input->post('group_name'),
				'group_class' => $this->input->post('group_class')
			);
		}

		// additional function

	}
