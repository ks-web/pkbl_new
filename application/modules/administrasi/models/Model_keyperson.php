<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	Class Model_keyperson extends MY_Model {
		function __construct() {
			parent::__construct();
			$this->_table = "sys_user";
			$this->_view = "sys_user_vd";
			$this->_order = 'asc';
			$this->_sort = 'id';
			$this->_page = 1;
			$this->_rows = 10;

			$this->_create = true;
			$this->_update = true;

			if ($this->uri->segment(4)) {
				$this->_filter = array('id' => $this->uri->segment(4));
			}
			if ($this->input->post('q')) {
				$this->_like = array(
					'username' => $this->input->post('q'),
					'email' => $this->input->post('q')
				);
			}

			$this->_param = array('id' => $this->input->post('id'));

			$this->_data = array(
				'aktif' => $this->input->post('aktif')
			);
		}

		// additional function
		function getJsonpeg() {
			$this->db->select('idPegawai,nama_pegawai,email,Divisi');
			$data = $this->db->get('pegawai')->result();
			$this->output->set_output(json_encode($data));
		}

		function getJsonven() {
			$this->db->select('vendor_code,nama_vendor,email');
			$data = $this->db->get('mas_vendor')->result();
			$this->output->set_output(json_encode($data));
		}

		function getGroup($class) {
			$this->db->where('group_class', $class);
			$data = $this->db->get('sys_group')->result();
			$this->output->set_output(json_encode($data));
		}

		public function insert_user($data_user) {
			return $this->db->insert('sys_user', $data_user);
		}

	}
