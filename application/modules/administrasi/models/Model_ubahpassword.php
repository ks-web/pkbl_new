<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	Class Model_ubahpassword extends CI_Model {
		function __construct() {
			parent::__construct();

		}

		function getSave($data) {

			/*$sql = "SELECT idKaryawan
			 FROM sys_user
			 WHERE idKaryawan='".$this->session->userdata('idKaryawan')."' AND
			 userpass='".$data['old']."'
			 ";

			 $query = $this->db->query($sql);
			 */
			$this->db->where('id', $this->session->userdata('id'));
			// $this->db->where('userpass',$data['old']);
			$query = $this->db->get('sys_user');
			$row_user = $query->row();

			if ($query->num_rows() > 0) {
				if (password_verify($data['old'], $row_user->userpass)) {
					$this->db->set('userpass', password_hash($data['userpass'], PASSWORD_DEFAULT));
					$this->db->where('id', $this->session->userdata('id'));
					$update = $this->db->update('sys_user');

					$this->output->set_output(json_encode(array('success' => 'Password Berhasil Diganti')));

					// $this->session->sess_destroy();
					//redirect(base_url('home'));
				} else {
					$this->output->set_output(json_encode(array('msg' => 'Password Lama Tidak Sesuai')));
				}

			} else {

				$this->output->set_output(json_encode(array('msg' => 'Password Lama Tidak Sesuai')));
			}

		}

	}
