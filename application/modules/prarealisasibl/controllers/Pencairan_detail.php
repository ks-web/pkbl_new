<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');
	class Pencairan_detail extends MY_Controller {
		public $models = array('pencairan_detail');

		public function __construct() {
			parent::__construct();
		}

		public function index() {
			$data = array();
			$data['menu'] = $this->model_menu->getAllMenu();

			$this->template->load('template', 'view_pencairan_detail', $data);
		}

		public function read($id = null) {
			$this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read(array('id_cd_um' => $id)));
		}

		public function read_all($id = null) {
			$this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read_all(array('id_cd_um' => $id)));
		}

		public function create() {
			// additional block
			$id_cd = $this->input->post('id_cd_um');
			// addtional get
			$result = $this->{$this->models[0]}->insert($this->data_add);
			if ($result == 1) {
				$this->{$this->models[0]}->balanced_check($id_cd);
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => $this->db->_error_message())));
			}
		}

		public function update() {
			$param = $this->uri->segment(4);
			// parameter key

			// additional block
			$id_cd = $this->input->post('id_cd_um');
			// addtional data

			// additional where
			$this->where_add['id_akun'] = $param;

			$result = $this->{$this->models[0]}->update($this->where_add, $this->data_add);
			if ($result == 1) {
				$this->{$this->models[0]}->balanced_check($id_cd);
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'Data Gagal Di Update !')));
			}
		}

		public function delete() {
			$param = $this->uri->segment(4);
			$param2 = $this->uri->segment(5);
			// parameter key

			// additional block
			$id_cd = $param2;
			// additional where
			$this->where_add['id_akun'] = $param;

			$result = $this->{$this->models[0]}->delete($this->where_add);
			if ($result == 1) {
				$this->{$this->models[0]}->balanced_check($id_cd);
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'Data Gagal Di Hapus !')));
			}
		}

		public function posting()
		{
			$param = $this->uri->segment(4); 
			$filter = array('A.id_cd_um' => $param, 'A.status' => 'BL', 'A.transaksi' => 'CD');
			$balance = $this->{$this->models[0]}->getStatusBalance($filter);
			//echo $balance;
			if($balance['status'] != 0){
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => $balance['msg'])));
			} else {
				$filter = array('A.id_cd' => $param);
				$data = array('A.status_posted' => 1, 'A.tgl_posting' => date('Y-m-d'));
				$this->output->set_content_type('application/json')->set_output(json_encode($this->{$this->models[0]}->setStatusPosting($filter, $data)));
				//continue to trigger update on table tcd_all
			}
		}

		public function getStatusPosting()
		{
			$param = $this->uri->segment(4); 
			$filter = array('A.id_cd' => $param);
			$this->output->set_content_type('application/json')->set_output(json_encode($this->{$this->models[0]}->getStatusPosting($filter)));
		}

		public function getakun() {
			$q = $this->input->post('q');
			$ret = $this->{$this->models[0]}->getakun($q);
			echo json_encode($ret);
		}

	}
