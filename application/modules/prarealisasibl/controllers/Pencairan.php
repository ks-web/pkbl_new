<?php
if (! defined('BASEPATH'))
    exit('No direct script access allowed');

class Pencairan extends MY_Controller
{

    public $models = array(
        'pencairan'
    );

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $data = array();
        $data['menu'] = $this->model_menu->getAllMenu();
        
        $this->template->load('template', 'view_pencairan', $data);
    }

    public function read()
    {
        $this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read());
    }

    public function read_all()
    {
        $this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read_all());
    }

    public function create()
    {
        // $param = $this->input->post('id_bl'); // parametter key
        
        // additional block
        $tgl_cd = explode('-', $this->input->post('tanggal_cd'));
        $thn_bln = $tgl_cd[0] . '-' . $tgl_cd[1];
        
        $id = $this->get_no($thn_bln);
        $this->data_add['id_cd'] = $id;
        
        // $id_cd = $this->get_idcd();
        // $this->data_add['id_cd'] = $id_cd;
        $idbl = $this->input->post('id_bl');
        $data = array(
            'status' => "7"
        );
        $this->data_add['creator'] = $this->session->userdata('id');
        $this->data_add['tanggal_create'] = date("Y-m-d H:i:s");
        // addtional get
        $result = $this->{$this->models[0]}->insert($this->data_add);
        if ($result == 1) {
            $this->{$this->models[0]}->editbl($idbl, $data);
            // insert_default_account
            // $id_cd = $this->data_add['id_cd'];
            // $this->{$this->models[0]}->insert_default_account($idbl, $id_cd);
            $this->{$this->models[0]}->createAccount($id);
            $this->output->set_content_type('application/json')->set_output(json_encode(array(
                'success' => true
            )));
        } else {
            $this->output->set_content_type('application/json')->set_output(json_encode(array(
                'msg' => $this->db->_error_message()
            )));
        }
    }

    public function update($param)
    {
        // $param = $this->uri->segment(4); // parameter key
        
        // additional block
        
        // addtional data
        $this->data_add['modifier'] = $this->session->userdata('id');
        $this->data_add['tanggal_modified'] = date("Y-m-d H:i:s");
        
        // additional where
        $this->where_add['no'] = $param;
        
        $result = $this->{$this->models[0]}->update($this->where_add, $this->data_add);
        if ($result == 1) {
            $this->output->set_content_type('application/json')->set_output(json_encode(array(
                'success' => true
            )));
        } else {
            $this->output->set_content_type('application/json')->set_output(json_encode(array(
                'msg' => 'Data Gagal Di Update !'
            )));
        }
    }

    public function delete()
    {
        $param = $this->uri->segment(4);
        // parameter key
        
        // additional block
        
        // additional where
        $this->where_add['no'] = $param;
        
        $this->{$this->models[0]}->deleteAccount($param);
        $result = $this->{$this->models[0]}->delete($this->where_add);
        if ($result == 1) {
            $data = array(
                'status' => "6"
            );
            $this->{$this->models[0]}->editbl($param, $data);
            $this->output->set_content_type('application/json')->set_output(json_encode(array(
                'success' => true
            )));
        } else {
            $this->output->set_content_type('application/json')->set_output(json_encode(array(
                'msg' => 'Data Gagal Di Hapus !'
            )));
        }
    }

    function get_idcd()
    {
        $maxbl = $this->{$this->models[0]}->get_idcd();
        if ($maxbl->num_rows() > 0) {
            $q = $maxbl->result_array();
            $a = $q[0]['id_cd'];
        } else {
            $a = "0001";
        }
        $hasil = date('Ym') . $a;
        return $hasil;
    }

    private function get_no($thn_bln)
    {
        $thn_bln_arr = explode('-', $thn_bln);
        
        $no = '000';
        
        $sql = "
				SELECT
					RIGHT(max(a.id_cd),4)*1 AS last_seq
				FROM
					tcd_all a
				WHERE
					DATE_FORMAT(a.tanggal_cd, '%Y-%m') = ? AND code = 'BL'
			";
        $query = $this->db->query($sql, array(
            $thn_bln
        ));
        $row = $query->row_array();
        
        $last_seq = (int) $row['last_seq'];
        $next_seq = $last_seq + 1;
        
        $leng_temp_no = strlen($next_seq);
        $no = 'CD' . implode('', $thn_bln_arr) . substr($no, $leng_temp_no) . $next_seq;
        
        return $no;
    }

    function sektor_bl()
    {
        header("Content-Type: application/json");
        $cari = $this->input->post('q') ? $this->input->post('q') : '';
        $this->{$this->models[0]}->sektor_bl($cari);
    }

    function getBL($param)
    {
        header("Content-Type: application/json");
        $cari = $this->input->post('q') ? $this->input->post('q') : '';
        $this->{$this->models[0]}->getBL($cari, $param);
    }

    function previewpencairandana($idbl)
    { // load dompdf
                                           
        // load content html
        $data['pencairan'] = $this->{$this->models[0]}->pencairan()->row_array();
        $data['akun_bl'] = $this->{$this->models[0]}->getakun($idbl)->result_array();
        $data['sign'] = $this->{$this->models[0]}->getsign()->row_array();
        
        // $html = $this->load->view('sph3.php', $data, true);
        $html = $this->load->view('tcd_bl.php', $data, true);
        // echo $html;
        // create pdf using dompdf
        $this->load->library('dompdf_gen');
        // Load library
        $this->dompdf->set_paper('A4', 'portrait');
        // Setting Paper
        // Convert to PDF
        $this->dompdf->load_html($html);
        $this->dompdf->render();
        $this->dompdf->stream("Pencairan Dana Bina Lingkungan.pdf");
    }

    function getakunbl()
    {
        header("Content-Type: application/json");
        $cari = $this->input->post('q') ? $this->input->post('q') : '';
        $this->{$this->models[0]}->getakunbl($cari);
    }

    function getdataakun($idcd)
    {
        header("Content-Type: application/json");
        // $cari = $this->input->post('q') ? $this->input->post('q') : '';
        $this->{$this->models[0]}->getdataakun($idcd);
    }

    function save_akun($idcd)
    {
        $data = array(
            'id_cd_um' => $idcd,
            'account_no' => $this->input->post('account_no'),
            'nama_akun' => $this->input->post('nama_akun'),
            'debet' => $this->input->post('debet'),
            'kredit' => $this->input->post('kredit'),
            'status' => "BL"
        );
        
        $this->{$this->models[0]}->save_akun($data);
    }

    function update_akunbl($idakun)
    {
        $data = array(
            'account_no' => $this->input->post('account_no'),
            'nama_akun' => $this->input->post('nama_akun'),
            'debet' => $this->input->post('debet'),
            'kredit' => $this->input->post('kredit')
        );
        
        $this->{$this->models[0]}->update_akunbl($data, $idakun);
    }

    function hapus_akun($idakun)
    {
        $this->{$this->models[0]}->hapus_akun($idakun);
    }

    function cetak($idbl)
    { // load dompdf
                           
        // load content html
        $data['pencairan'] = $this->{$this->models[0]}->pencairan()->row_array();
        $data['akun_bl'] = $this->{$this->models[0]}->getakun($idbl)->result_array();
        
        // $html = $this->load->view('sph3.php', $data, true);
        $html = $this->load->view('cetak_pencairandana_bl.php', $data, true);
        // create pdf using dompdf
        $this->load->library('dompdf_gen');
        // Load library
        $this->dompdf->set_paper('A4', 'portrait');
        // Setting Paper
        // Convert to PDF
        $this->dompdf->load_html($html);
        $this->dompdf->render();
        $this->dompdf->stream("Pencairan Dana Bina Lingkungan.pdf");
    }

    function cek($idbl)
    { // load dompdf
                         
        // load content html
        $data['pencairan'] = $this->{$this->models[0]}->pencairan()->row_array();
        $data['akun_bl'] = $this->{$this->models[0]}->getakun($idbl)->result_array();
        
        // $html = $this->load->view('sph3.php', $data, true);
        $html = $this->load->view('cetak_cek.php', $data, true);
        // create pdf using dompdf
        $this->load->library('dompdf_gen');
        // Load library
        $this->dompdf->set_paper('A4', 'portrait');
        // Setting Paper
        // Convert to PDF
        $this->dompdf->load_html($html);
        $this->dompdf->render();
        $this->dompdf->stream("Pencairan Dana Bina Lingkungan.pdf");
    }

    public function account()
    {
        $this->{$this->models[0]}->createAccount(2017100002);
    }

    public function batal_cd()
    {
        // parameter key
        $id_cd = $this->input->post('id_cd');
        $code = $this->input->post('code');
        
        // additional block
        
        // addtional data
        $this->data_add['status_progress'] = '1';
        
        // additional where
        $this->where_add['id_cd'] = $id_cd;
        $this->where_add['code'] = $code;
        
        $result = $this->{$this->models[0]}->updateOtherData('tcd_all', $this->where_add, $this->data_add);
        if ($result == 1) {
            $this->output->set_content_type('application/json')->set_output(json_encode(array(
                'success' => true
            )));
        } else {
            $this->output->set_content_type('application/json')->set_output(json_encode(array(
                'msg' => 'Data Gagal Di Update !'
            )));
        }
    }
}
