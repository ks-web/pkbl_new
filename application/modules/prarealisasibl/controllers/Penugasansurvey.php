<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');
	class Penugasansurvey extends MY_Controller {
		public $models = array('survey','penilaian');
		
		public function __construct() {
			parent::__construct();
		}

		public function index() {
			$data = array();
			$data['menu'] = $this->model_menu->getAllMenu();

			$this->template->load('template', 'view_survey', $data);
		}

		public function read() {

			$id_pegawai = $this->session->userdata('idPegawai');
			$where = array('petugas_survey' => $id_pegawai );
			$data = $this->{$this->models[0]}->read($where);
			$json = json_decode($data);

			if ($json->total == 0){
				$this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read());
			}else{
				$this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read($where));
			}
		}

		public function read_all() {
			$this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read_all());
		}

		public function create() {
			//$param = $this->input->post('id_bl'); // parametter key

			// additional block
            $idbl=$this->input->post('nama_bl');
            $data = array(
                    'status'         =>"3"
                    );
			// addtional get
			$result = $this->{$this->models[0]}->insert($this->data_add);
			if ($result == 1) {
                $this->{$this->models[1]}->editbl($idbl,$data);
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => $this->db->_error_message())));
			}
		}

		public function update() {
			$param = $this->uri->segment(4); // parameter key

			// additional block

			// addtional data

			// additional where
			$this->where_add['param'] = $param;

			$result = $this->{$this->models[0]}->update($this->where_add, $this->data_add);
			if ($result == 1) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'Data Gagal Di Update !')));
			}
		}

		public function delete() {
			$param = $this->uri->segment(4); // parameter key

			// additional block

			// additional where
			$this->where_add['id_bl'] = $param;

			$result = $this->{$this->models[0]}->delete($this->where_add);
			if ($result == 1) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'Data Gagal Di Hapus !')));
			}
		}

		public function data_bl() {
			$cari = $this->input->post('q') ? $this->input->post('q'):'';

			$this->db->select('*');
			$this->db->from('tblingkungan');
			$this->db->like('id_bl', $cari);
			$this->db->or_like('instansi', $cari);
			$data = $this->db->get()->result_array();

			echo json_encode($data);
		}
		public function petugas() {
			$this->db->select('*');
			$this->db->from('mas_pegawai');
			$this->db->where('id_jabatan',7);
			$data = $this->db->get()->result_array();

			echo json_encode($data);
		}
        function download_form($idbl){    // load dompdf
    
		    //load content html
		    $data['survey'] = $this->{$this->models[0]}->download_form($idbl)->row_array();
            $this->load->helper('tanggal_indo');
		    $html = $this->load->view('form_survey.php', $data,true);
		    // create pdf using dompdf
  			$this->load->library('dompdf_gen'); // Load library
			$this->dompdf->set_paper('A4', 'portrait'); // Setting Paper
			// Convert to PDF
			$this->dompdf->load_html($html);
			$this->dompdf->render();
			$this->dompdf->stream("Form Survey.pdf");
        }

	}
