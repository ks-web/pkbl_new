<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');
	class Evaluasi extends MY_Controller {
		public $models = array('evaluasi');
		
		public function __construct() {
			parent::__construct();
		}

		public function index() {
			$data = array();
			$data['menu'] = $this->model_menu->getAllMenu();

			$this->template->load('template', 'view_evaluasi', $data);
		}

		public function read() {
			$this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read());
		}

		public function read_all() {
			$this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read_all());
		}

		public function create() {
			//$param = $this->input->post('id_bl'); // parametter key

			// additional block
            $idbl=$this->input->post('id_bl');
            $data = array(
                    'status'         =>"5"
                    );
			// addtional get
			//$result = $this->{$this->models[0]}->insert($this->data_add);
            $this->where_add['id_bl'] = $idbl;
            $result = $this->{$this->models[0]}->update($this->where_add, $this->data_add);
			if ($result == 1) {
			    $this->{$this->models[0]}->editbl($idbl,$data);
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => $this->db->_error_message())));
			}
		}

		public function update() {
			$param = $this->uri->segment(4); // parameter key

			// additional block

			// addtional data

			// additional where
			$this->where_add['id_bling'] = $param;
            

			$result = $this->{$this->models[0]}->update($this->where_add, $this->data_add);
			if ($result == 1) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
                
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'Data Gagal Di Update !')));
			}
		}

		public function delete() {
			$param = $this->uri->segment(4); // parameter key

			// additional block
            $data = array(
                'nilai_rekomendasi' => "",
                'keterangan_evaluator' => "",
                'tanggal_evaluator' => ""
			);
			// additional where
			$this->where_add['id_bling'] = $param;
            $idbling= $param;
			//$result = $this->{$this->models[0]}->delete($this->where_add);
            $result = $this->{$this->models[0]}->hapus($idbling, $data);
			if ($result == 1) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'Data Gagal Di Hapus !'.$result)));
			}
		}

		public function validasi() {
			$param = $this->uri->segment(4); // parameter key
            
            $filter = array('id_bl' => $param);
            $data = array('status' => 5);

			$result = $this->{$this->models[0]}->validasi($filter, $data);
			if ($result == 1) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
                
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'Data Gagal Di Update !')));
			}
		}

        function sektor_bl() {
			header("Content-Type: application/json");
			$cari = $this->input->post('q') ? $this->input->post('q') : '';
			$this->{$this->models[0]}->sektor_bl($cari);
		}
        function getBL($param) {
			header("Content-Type: application/json");
			$cari = $this->input->post('q') ? $this->input->post('q') : '';
			$this->{$this->models[0]}->getBL($cari,$param);
		}
         function usulan(){    // load dompdf
    
		    //load content html
		    $data['vendor'] = $this->{$this->models[0]}->usulan()->row_array();
		    // $html = $this->load->view('sph3.php', $data, true);
            $this->load->helper('tanggal_indo');
			$this->load->helper('terbilang');
			
		    $html = $this->load->view('usulan_evaluator.php', $data,true);
		    // create pdf using dompdf
  			$this->load->library('dompdf_gen'); // Load library
			$this->dompdf->set_paper('A4', 'portrait'); // Setting Paper
			// Convert to PDF
			$this->dompdf->load_html($html);
			$this->dompdf->render();
			$this->dompdf->stream("Usulan Evaluator Bina Lingkungan.pdf");
		}

	}
