<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');
        /* @property phpword_model $phpword_model */
        include_once(APPPATH."third_party/PhpWord/Autoloader.php");
        //include_once(APPPATH."core/Front_end.php");
        
        use PhpOffice\PhpWord\Autoloader;
        use PhpOffice\PhpWord\Settings;
        Autoloader::register();
        Settings::loadConfig();
	class Kontrak extends MY_Controller {
		public $models = array('kontrak');
        
		public function __construct() {
			parent::__construct();
            
           
		}

		public function index() {
			$data = array();
			$data['menu'] = $this->model_menu->getAllMenu();

			$this->template->load('template', 'view_kontrak', $data);
		}

		public function read() {

			$this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read());
		}

		public function read_all() {
			$this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read_all());
		}

		public function createkontrak() {
			// additional block
			$idbl = $this->input->post('id_bl');
			//$param=$this->input->post('id_persetujuan');
			$databl = array('status' => '6');
			$this->where_add['id_bl'] = $idbl;
			$result = $this->{$this->models[0]}->update($this->where_add, $this->data_add);
			if ($result == 1) {
				$this->{$this->models[0]}->editbl($idbl, $databl);
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'Data Gagal Di Update !')));
			}
		}

		public function update($param) {

			$this->where_add['id_bl'] = $param;

			$result = $this->{$this->models[0]}->update($this->where_add, $this->data_add);
			if ($result == 1) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));

			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'Data Gagal Di Update !')));
			}
		}

		public function delete() {
			$param = $this->uri->segment(4);
			// parameter key

			// additional block

			// additional where
			$this->where_add['id_kontrak'] = $param;

			$result = $this->{$this->models[0]}->delete($this->where_add);
			if ($result == 1) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'Data Gagal Di Hapus !')));
			}
		}

		function sektor_bl() {
			header("Content-Type: application/json");
			$cari = $this->input->post('q') ? $this->input->post('q') : '';
			$this->{$this->models[0]}->sektor_bl($cari);
		}

		function getBL() {
			header("Content-Type: application/json");
			$cari = $this->input->post('q') ? $this->input->post('q') : '';
			$this->{$this->models[0]}->getBL($cari);
		}

		function previewkontrak($id) {// load dompdf

			//load content html
			/*$data['kontrak'] = $this->{$this->models[0]}->kontrak()->row_array();
			// $html = $this->load->view('sph3.php', $data, true);
			$html = $this->load->view('Kontrak_bl.php', $data, true);
			// create pdf using dompdf
			$this->load->library('dompdf_gen');
			// Load library
			$this->dompdf->set_paper('A4', 'portrait');
			// Setting Paper
			// Convert to PDF
			$this->dompdf->load_html($html);
			$this->dompdf->render();
			$this->dompdf->stream("Kontrak Kerjasama Bina Lingkungan.");*/
            //$news = $this->phpword_model->get_news();

    		//  create new file and remove Compatibility mode from word title
            $this->load->helper('tanggal_indo');
            $this->load->helper('terbilang_helper');
            $kontrak= $this->{$this->models[0]}->kontrak($id)->row_array();
            $isikontrak= $this->{$this->models[0]}->isikontrak($id)->result_array();
    		$phpWord = new \PhpOffice\PhpWord\PhpWord();
    		$phpWord->getCompatibility()->setOoxmlVersion(14);
    		$phpWord->getCompatibility()->setOoxmlVersion(15);
    
    		$targetFile = "./global/uploads/headkti.png";
    		$filename = 'Kontrak.docx';
            
            $section = $phpWord->addSection();
            $section->addText("PERJANJIAN", array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center', 'spaceAfter' => 10));
            $section->addText("ANTARA", array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center', 'spaceAfter' => 10));
            $section->addText("PT.KRAKATAU STEEL(PERSERO)Tbk", array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center', 'spaceAfter' => 10));
            $section->addText("DENGAN", array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center', 'spaceAfter' => 10));
            $section->addText(strtoupper($kontrak['kontraktor']), array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center', 'spaceAfter' => 10));
            $section->addText("TENTANG", array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center', 'spaceAfter' => 10));
            $section->addText(strtoupper($kontrak['jenis_pekerjaan']), array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center', 'spaceAfter' => 40));
            $section->addTextBreak(5);
            $section->addText(date("Y"), array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center', 'spaceAfter' => 10));
            
            $section = $phpWord->addSection();//ganti halaman
            $section->addImage($targetFile, array('align' => 'center','width'=>300, 'height'=>120));
            $section->addText("PERJANJIAN", array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center', 'spaceAfter' => 10));
            $section->addText("ANTARA", array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center', 'spaceAfter' => 10));
            $section->addText("PT.KRAKATAU STEEL(PERSERO)Tbk", array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center', 'spaceAfter' => 10));
            $section->addText("DENGAN", array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center', 'spaceAfter' => 10));
            $section->addText(strtoupper($kontrak['kontraktor']), array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center', 'spaceAfter' => 10));
            $section->addText("TENTANG", array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center', 'spaceAfter' => 10));
            $section->addText(strtoupper($kontrak['jenis_pekerjaan']), array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center', 'spaceAfter' => 40));
            $section->addTextBreak(1);
            $section->addText("PIHAK PERTAMA : ".strtoupper($kontrak['no_kontrak']), array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center', 'spaceAfter' => 10));
            $section->addText("PIHAK KEDUA :".strtoupper($kontrak['no_proposal']), array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center', 'spaceAfter' => 10));
            $section->addTextBreak(1);
            $lineStyle = array('weight' => 1, 'width' => 600, 'color' => '000000','height' => 0,);
            $section->addLine($lineStyle);
            $this->load->helper('tanggal_indo');
            $bulan=date('m',strtotime(date("Y-m-d")));
            $isi ="Perjanjian ini ditandantangani di Cilegon, pada hari ".nama_hari(date("Y-m-d"),4).", tanggal ".date("d").", bulan ".nama_bulan($bulan,4).", tahun ".terbilang_display(date("Y"))."  (".date("d-m-Y")."), oleh dan antara :";
            $section->addText($isi, array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify', 'spaceAfter' => 10));
            $section->addTextBreak(1);
            //pihak pertama
            $table = $section->addTable();
            $table->addRow();
            // Add cells
            $table->addCell()->addText("1. ", array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center'));
            $table->addCell(4000)->addText("PT.KRAKATAU STEEL (PERSERO)Tbk", array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table->addCell()->addText(":", array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center'));
            $isipihak1="Berkedudukan di Cilegon, Jalan Industri Nomor 5 Cilegon, dalam hal ini diwakili oleh AGUS NIZAR VIDIANSYAH, General Manager Security Dan GA, yang bertindak mewakili Perseroan berdasarkan SK Nomor : 151/DU-KS/Kpts tanggal 01 Desember 2014, dari dan oleh sebab itu berwenang untuk bertindak untuk dan atas nama PT.KRAKATAU STEEL(PERSERO)Tbk, selanjutnya disebut PIHAK PERTAMA.";
            
            $table->addCell(5000)->addText($isipihak1, array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify'));
            //pihak kedua
            $table->addRow();
            // Add cells
            $table->addCell()->addText("2. ", array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center'));
            $table->addCell(4000)->addText($kontrak['kontraktor'], array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'left'));
            $table->addCell()->addText(":", array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center'));
            $isipihak2="Berkedudukan di ".$kontrak['alamat_perusahaan'].", dalam hal ini diwakili oleh ".$kontrak['nama_pimpinan'].", Direktur, yang bertindak mewakili ".$kontrak['kontraktor']." berdasarkan Akta No. ".$kontrak['akta_nomor']." tanggal ".tanggal_display($kontrak['tanggal_akta']).", Notaris di ".$kontrak['alamat_notaris']." dari dan oleh sebab itu berwenang untuk bertindak untuk dan atas nama ".$kontrak['kontraktor']." selanjutya disebut PIHAK KEDUA.";
            
            $table->addCell(5000)->addText($isipihak2, array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify'));
            $section->addTextBreak(1);
            $isi2="PIHAK PERTAMA dan PIHAK KEDUA selanjutnya disebut PARA PIHAK";
            $section->addText($isi2, array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify', 'spaceAfter' => 10));
            $section->addText("PARA PIHAK terlebih dahulu menerangkan hal-hal sebagai berikut :", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify', 'spaceAfter' => 10));
            $section->addTextBreak(1);
            $pertama="1. Bahwa untuk menciptakan sinergi antara program Bina Lingkungan (Corporate Social Responsibility) Perusahaan dengan program prioritas Pemerintah Kota Serang, maka PIHAK PERTAMA bermaksud untuk menyalurkan dana program bina lingkungan melalui program ".$kontrak['jenis_pekerjaan'].".";
            $section->addText($pertama, array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify', 'spaceAfter' => 10));
            $kedua="2. Bahwa PIHAK PERTAMA dengan ini menetapkan PIHAK KEDUA sebagai mitra kerja untuk melaksanakan ".$kontrak['keterangan']." dan PIHAK KEDUA menyetujui hal tersebut dan akan melaksanakan pekerjaan dengan baik.";
            $section->addText($kedua, array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify', 'spaceAfter' => 10));
            $section->addTextBreak(1);
            $ketiga="Berdasarkan hal-hal tersebut di atas, maka PARA PIHAK sepakat untuk mengatur lebih lanjut dalam suatu Kerjasama Pelaksanaan Program Bina Lingkungan Badan Usaha Milik Negara yang dituangkan dalam Perjanjian Pengadaan Air Bersih (selanjutnya disebut Perjanjian) dengan syarat-syarat dan ketentuan sebagai berikut :";
            $section->addText($ketiga, array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'justify', 'spaceAfter' => 10));
            // ganti halaman
            $section->addTextBreak(1);
            //isi pasal kontrak
            foreach($isikontrak as $n){
                $section->addText($n['pasal'], array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center', 'spaceAfter' => 10));
                $section->addText($n['judul_pasal'], array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center', 'spaceAfter' => 10));
                $section->addTextBreak(1);
                $html="";
                $html.=$n['deskripsi_pasal'];
                $xhtml= new \PhpOffice\PhpWord\Shared\Html();
                $xhtml->addHtml($section, $html);
                $section->addTextBreak(1);
            }
            // end isi pasal kontrak
            //$section = $phpWord->addSection();//ganti halaman
            $table = $section->addTable();
            $table->addRow();
            // Add cells
            $table->addCell(5000)->addText("PIHAK KEDUA", array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center'));
            $table->addCell(5000)->addText("PIHAK PERTAMA", array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center'));
            $table->addRow(1500);
            // Add cells
            $table->addCell(5000)->addText($kontrak['kontraktor'], array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center'));
            $table->addCell(5000)->addText("PT.KRAKATAU STEEL(PERSERO)Tbk", array('bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center'));
            
            $table->addRow();
            // Add cells
            $table->addCell()->addText($kontrak['nama_pimpinan'], array('underline' => 'single','bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center'));
            $table->addCell()->addText("Syarif Rahman", array('underline' => 'single','bold' => true,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center'));
            $table->addRow();
            // Add cells
            $table->addCell()->addText("Direktur", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center'));
            $table->addCell()->addText("General Manager", array('bold' => false,'name'=> 'Tahoma','size' => 11,'color' =>'black'),array('align' => 'center'));
            $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
    		$objWriter->save($filename);
    		// send results to browser to download
    		header('Content-Description: File Transfer');
    		header('Content-Type: application/octet-stream');
    		header('Content-Disposition: attachment; filename='.$filename);
    		header('Content-Transfer-Encoding: binary');
    		header('Expires: 0');
    		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
    		header('Pragma: public');
    		header('Content-Length: ' . filesize($filename));
    		flush();
    		readfile($filename);
    		unlink($filename); // deletes the temporary file
    		exit;
           
		}
        function getpasal() {
			header("Content-Type: application/json");
			$cari = $this->input->post('q') ? $this->input->post('q') : '';
			$this->{$this->models[0]}->getpasal($cari);
		}

		function getkontraktor() {
			header("Content-Type: application/json");
			$cari = $this->input->post('q') ? $this->input->post('q') : '';
			$this->{$this->models[0]}->getkontraktor($cari);
		}
        function getisikontrak($idpersetujuan) {
			header("Content-Type: application/json");
			//$cari = $this->input->post('q') ? $this->input->post('q') : '';
			$this->{$this->models[0]}->getisikontrak($idpersetujuan);
		}
		function getdataalih($idpersetujuan) {
			header("Content-Type: application/json");
			//$cari = $this->input->post('q') ? $this->input->post('q') : '';
			$this->{$this->models[0]}->getdataalih($idpersetujuan);
		}
        function save_pasal($idpersetujuan,$nokontrak) {
			$data = array(
				'id_persetujuan' => $idpersetujuan,
				'no_kontrak' => $nokontrak,
				'pasal' => $this->input->post('pasal'),
               	'judul_pasal' => $this->input->post('judul_pasal'),
				'deskripsi_pasal' => $this->input->post('deskripsi_pasal'),
				'creator' => $this->session->userdata('id'),
				'tanggal_create' => date("Y-m-d H:i:s")
			);

			$this->{$this->models[0]}->save_pasal($data);
		}
		function save_alih($idpersetujuan,$idbl) {
			$data = array(
				'id_persetujuan' => $idpersetujuan,
				'id_kontraktor' => $this->input->post('id_kontraktor'),
				'jenis_pekerjaan' => $this->input->post('jenis_pekerjaan'),
               	'tanggal_kontrak' => date('Y-m-d'),
				'no_kontrak' => date('Y') . $idbl,
				'nilai_pekerjaan' => $this->input->post('nilai_pekerjaan'),
				'keterangan' => $this->input->post('keterangan'),
				'tanggal_mulai' => $this->input->post('tanggal_mulai'),
				'tanggal_selesai' => $this->input->post('tanggal_selesai')
			);

			$this->{$this->models[0]}->save_alih($data);
		}
        function update_isi($idkontrak) {
				$data = array(
				'pasal' => $this->input->post('pasal'),
               	'judul_pasal' => $this->input->post('judul_pasal'),
				'deskripsi_pasal' => $this->input->post('deskripsi_pasal'),
				'modifier' => $this->session->userdata('id'),
				'tanggal_modifed' => date("Y-m-d H:i:s")
			);

			$this->{$this->models[0]}->update_isi($data, $idkontrak);
		}
		function update_alih($idpersetujuan, $idkontraktor) {
			$data = array(
				'id_kontraktor' => $this->input->post('id_kontraktor'),
				'jenis_pekerjaan' => $this->input->post('jenis_pekerjaan'),
				'nilai_pekerjaan' => $this->input->post('nilai_pekerjaan'),
				'keterangan' => $this->input->post('keterangan'),
				'tanggal_mulai' => $this->input->post('tanggal_mulai'),
				'tanggal_selesai' => $this->input->post('tanggal_selesai')
			);

			$this->{$this->models[0]}->update_alih($data, $idpersetujuan, $idkontraktor);
		}
        function hapus_isi($idkontrak) {
			$this->{$this->models[0]}->hapus_isi($idkontrak);
		}

		function hapus_alih($idpersetujuan, $idkontraktor) {
			$this->{$this->models[0]}->hapus_alih($idpersetujuan, $idkontraktor);
		}

		function preview_bast_barang_dana($id_bl = null) {// load dompdf
			if ($id_bl) {
				//load content html
				$this->load->helper('terbilang');
				$this->load->helper('tanggal_indo');
				$data['data'] = $this->{$this->models[0]}->kontrak_bl_bast($id_bl)->row_array();
				$data['data_login'] = $this->{$this->models[0]}->kontrak_bl_bast_user($this->session->userdata('idPegawai'))->row_array();
				$html = $this->load->view('bast_barang_dana_print', $data, true);

				$this->load->library('dompdf_gen');
				$this->dompdf->set_paper('A4', 'portrait');

				$this->dompdf->load_html($html);
				$this->dompdf->render();
				$this->dompdf->stream("BAST_Barang-Dana_" . $id_bl . ".pdf");
			}
		}

	}
