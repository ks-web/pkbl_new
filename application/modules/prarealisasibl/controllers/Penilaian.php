<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');
	class Penilaian extends MY_Controller {
		public $models = array('penilaian');
		
		public function __construct() {
			parent::__construct();
		}

		public function index() {
			$data = array();
			$data['menu'] = $this->model_menu->getAllMenu();

			$this->template->load('template', 'view_penilaian', $data);
		}

		public function read() {
			$this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read());
		}

		public function read_all() {
			$this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read_all());
		}

		public function create() {
			//$param = $this->input->post('id_bl'); // parametter key

			// additional block
            $idbl=$this->input->post('id_bl');
            $data = array(
                    'status'         =>"4"
                    );
			// addtional get
			$result = $this->{$this->models[0]}->insert($this->data_add);
			if ($result == 1) {
			    $this->{$this->models[0]}->editbl($idbl,$data);
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => $this->db->_error_message())));
			}
		}

		public function update() {
			$param = $this->uri->segment(4); // parameter key

			// additional block

			// addtional data

			// additional where
			$this->where_add['id_bling'] = $param;
            

			$result = $this->{$this->models[0]}->update($this->where_add, $this->data_add);
			if ($result == 1) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
                
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'Data Gagal Di Update !')));
			}
		}

		public function delete() {
			$param = $this->uri->segment(4); // parameter key

			// additional block

			// additional where
			$this->where_add['id_bling'] = $param;

			$result = $this->{$this->models[0]}->delete($this->where_add);
			if ($result == 1) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'Data Gagal Di Hapus !')));
			}
		}
        function sektor_bl() {
			header("Content-Type: application/json");
			$cari = $this->input->post('q') ? $this->input->post('q') : '';
			$this->{$this->models[0]}->sektor_bl($cari);
		}
        function getBL($param) {
			header("Content-Type: application/json");
			$cari = $this->input->post('q') ? $this->input->post('q') : '';
			$this->{$this->models[0]}->getBL($cari,$param);
		}
        function contoh(){
            return $a;
        }
        
        function upload_lampiran_all($idbl){
            $param=$idbl;
            //$this->where_add['id_bl'] = $param;
            // Upload ktp
            $bl_dir = 'assets/upload/Survey/' . $param.'/file';
            if (!is_dir($bl_dir)) {
                mkdir($bl_dir, folderUploadPermission, TRUE);
            }
            $dir = 'assets/upload/Survey/' . $param . '/file';
            if (!is_dir($dir)) {
                mkdir($dir, folderUploadPermission, TRUE);
            }
            
            $config['upload_path'] = $dir . '/';
            $config['allowed_types'] = implode('|', json_decode(fileUploadTipe5));
            $config['max_size'] = fileUploadTipe5;
            $config['encrypt_name'] = true;
            $this->upload->initialize($config);
            
            if (!$this->upload->do_upload('file_survey')) {
                //$this->data_add['lampiran_ktp'] = '';
            } else {
                $file = $this->upload->data();
                //$this->data_add['lampiran_ktp'] = $config['upload_path'] . $file['file_name'];
                $data=array('file_survey'=>$config['upload_path'] . $file['file_name']);
            }
            
            $result = $this->{$this->models[0]}->update_file($param, $data);
            if ($result == 1) {
                $this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
            } else {
                $this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => $result)));
            }
        }
        

	}
