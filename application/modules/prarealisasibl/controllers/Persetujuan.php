<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');
	class Persetujuan extends MY_Controller {
		public $models = array('persetujuan','penilaian','evaluasi');
		
		public function __construct() {
			parent::__construct();
		}

		public function index() {
			$data = array();
			$data['menu'] = $this->model_menu->getAllMenu();

			$this->template->load('template', 'view_persetujuan', $data);
		}

		public function read() {
			$this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read());
		}

		public function read_all() {
			$this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read_all());
		}

		public function create() {
			if($this->input->post('keputusan') == 'ditolak'){
				$filter = array('id_bl' => $this->input->post('id_bl'));
				$data = array('status' => 4, 'tgl_ditolak' => $this->input->post('tanggal_disetujui'), 'ket_ditolak' => $this->input->post('ket_ditolak'));
				$result = $this->{$this->models[0]}->update_bl($filter, $data);
				if ($result == 1) {
					$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true, 'msg' => 'Persetujuan Ditolak, data kembali ke proses evaluasi penilaian')));
				} else {
					$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'Data Gagal Di Update !')));
				}
			} else {
				// additional block
	            $idbl=$this->input->post('id_bl');
	            $data = array(
	                    'status'         =>"6"
	                    );
				// addtional get
				$result = $this->{$this->models[0]}->insert($this->data_add);
				if ($result == 1) {
				    $this->{$this->models[1]}->editbl($idbl,$data);
					$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
				} else {
					$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => $this->db->_error_message())));
				}
			}
		}

		public function update() {
			$param = $this->uri->segment(4); // parameter key

			// additional block

			// addtional data

			// additional where
			$this->where_add['id_persetujuan'] = $param;

			$result = $this->{$this->models[0]}->update($this->where_add, $this->data_add);
			if ($result == 1) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'Data Gagal Di Update !')));
			}
			
		}

		public function delete() {
			$param = $this->uri->segment(4); // parameter key

			// update bl.status ke 4
			$idbl = $this->uri->segment(5);
			$data = array(
                    'status'         =>"5"
                    );

			// additional block

			// additional where
			$this->where_add['id_persetujuan'] = $param;

			$result = $this->{$this->models[0]}->delete($this->where_add);
			if ($result == 1) {
				$this->{$this->models[1]}->editbl($idbl,$data);
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'Data Gagal Di Hapus !')));
			}
		}
        function sektor_bl() {
			header("Content-Type: application/json");
			$cari = $this->input->post('q') ? $this->input->post('q') : '';
			$this->{$this->models[0]}->sektor_bl($cari);
		}
        function getBL($param) {
			header("Content-Type: application/json");
			$cari = $this->input->post('q') ? $this->input->post('q') : '';
			$this->{$this->models[0]}->getBL($cari,$param);
		}

		function getsdana()
		{
			header("Content-Type: application/json");
			$cari = $this->input->post('q') ? $this->input->post('q') : '';
			$this->{$this->models[0]}->getsdana($cari);
		}
        function get_anggaran($idbl){
            $getidanggaran=$this->{$this->models[0]}->get_anggaran($idbl);
            $idanggaran=$getidanggaran->id_sektor_bl;
            $getsisa=$this->{$this->models[0]}->get_sisa($idanggaran);
            $terpakai=$getsisa->anggarandipakai;
            $total=$getsisa->totalanggaran;
            $sisa=$total-$terpakai;
            $totalsemua=$getsisa->totalsemua;
            $terpakaisemua=$getsisa->terpakaisemua;
            $sisasemua=$totalsemua-$terpakaisemua;
            $this->output->set_output(json_encode(array('sisa'=>$sisa,'sisasemua'=>$sisasemua,'idanggaran'=>$idanggaran)));
        }
        function usulan(){    // load dompdf
    
		    //load content html
		    $data['vendor'] = $this->{$this->models[0]}->usulan()->row_array();
		    // $html = $this->load->view('sph3.php', $data, true);
		    $this->load->helper('tanggal_indo');
		    $this->load->helper('terbilang');
		    $html = $this->load->view('usulan_evaluator.php', $data,true);
		    // create pdf using dompdf
  			$this->load->library('dompdf_gen'); // Load library
			$this->dompdf->set_paper('A4', 'portrait'); // Setting Paper
			// Convert to PDF
			$this->dompdf->load_html($html);
			$this->dompdf->render();
			$this->dompdf->stream("Usulan Persetujuan Bina Lingkungan.pdf");
		}

		public function diketahui($id_bl){

			$session = $this->session->userdata('username');

			$data = array('diketahui' => $session);
			
			$res = $this->db->update('tpersetujuan_bl', $data, array('id_bl' => $id_bl));
			$this->output->set_content_type('application/json')->set_output(json_encode($res));

		}

	}
