<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	Class Model_persetujuan extends MY_Model {
		function __construct() {
			parent::__construct();
			$this->_table = "tpersetujuan_bl"; // for insert, update, delete
			$this->_view = "tpersetujuan_bl_vd"; // for call view
			$this->_order = 'desc';
			$this->_sort = 'tanggal_evaluator';
			$this->_page = 1;
			$this->_rows = 10;

			$this->_create = true;
			$this->_update = true;

			if ($this->uri->segment(4)) {
				$this->_filter = array('id_persetujuan' => $this->uri->segment(4));
			}

			//parameter from post/get - search function
			if ($this->input->post('q')) {
				$this->_like = array(
					'id_persetujuan' => $this->input->post('q'),
					'id_bl' => $this->input->post('id_bl'),
					'tanggal_disetujui' => $this->input->post('tanggal_disetujui'),
					'nilai_rekomendasi' => $this->input->post('nilai_rekomendasi'),
					'nilai_disetujui' => $this->input->post('nilai_disetujui'),
					'keputusan' => $this->input->post('keputusan'),
					'id_sdana' => $this->input->post('idanggaran'),
					'keteranganpemberian' => $this->input->post('keteranganpemberian')
				);
			}

			$this->_param = array('id_persetujuan' => $this->input->post('id_persetujuan'));

			//data array for input to database
			$this->_data = array(
				'id_bl' => $this->input->post('id_bl'),
				'tanggal_disetujui' => $this->input->post('tanggal_disetujui'),
				'pengalihan_kontrak' => $this->input->post('pengalihan_kontrak'),
				'nilai_disetujui' => $this->input->post('nilai_disetujui'),
				'keputusan' => $this->input->post('keputusan'),
		     	'id_sdana' => $this->input->post('idanggaran'),
				'keteranganpemberian' => $this->input->post('keteranganpemberian')
			);
			
		}
        function getBL($cari,$param) {
			//$this->db->select('id_bl,tanggal_input,no_ktp,nama,instansi,nilai,kota,alamat');
            $this->db->select('a.*,c.nilai_rekomendasi,c.nilai_disetujui,d.nama_bl,e.nama_sdana,f.rekomendasi_penilaian,b.tanggal_survey,f.nilai_rekomendasi,f.keterangan_evaluator');
            $this->db->join('tpenugasan_survey_bl b', 'b.id_bl=a.id_bl', 'left');
            $this->db->join('tpersetujuan_bl c', 'c.id_bl=a.id_bl', 'left');
            $this->db->join('sektor_bl d', 'd.id_sektor_bl=a.jenis_bl', 'left');
           // $this->db->join('mas_bidangbantuanbl g', 'g.id_sektor_bl=d.id_sektor_bl', 'left');
            $this->db->join('mas_sdana e', 'e.id_sdana=c.id_sdana', 'left');
            $this->db->join('tpenilaian_survey_bl f', 'f.id_bl=a.id_bl', 'left');
            $this->db->where ( 'a.status',$param);
			$this->db->like('CONCAT(a.id_bl,a.tanggal_input,a.no_ktp,a.nama,a.instansi,a.nilai,a.kota,a.alamat)', $cari);
			$data = $this->db->get('tblingkungan a')->result();
			$this->output->set_output(json_encode($data));
            //echo $this->db->last_query();
		}
        /*function getBL($cari) {
        	$field = array(
        				'tblingkungan' => array('id_bl', 'tanggal_input', 'no_ktp', 'nama', 'instansi', 'nilai', 'kota', 'alamat', 'jenis_bl', 'kegiatan'),
        				'tpenugasan_survey_bl' => array('tanggal_survey')
        				);
        	$select = '';
        	$like = '';
        	foreach ($field as $tbl => $kolom) {
        		foreach ($kolom as $index => $kolom) {
        			$select .=  $tbl.'.'.$kolom.', ';
        			$like .=  'IFNULL('.$tbl.'.'.$kolom.', ""), ';
        		}
        	}
        	$select = substr($select, 0, -2);
        	$like = 'CONCAT('.substr($like, 0, -2).')';

        	$this->db->select('id_bl');
			$this->db->from('tpersetujuan_bl');
			$where_clause = $this->db->get_compiled_select();

			$this->db->select($select);
			$this->db->where('tblingkungan.id_bl NOT IN ('.$where_clause.')', NULL, FALSE);
			$this->db->like($like, $cari);
			$this->db->join('tpenugasan_survey_bl', 'tpenugasan_survey_bl.id_bl = tblingkungan.id_bl');
			$this->db->join('tpenilaian_survey_bl', 'tpenilaian_survey_bl.id_bl = tblingkungan.id_bl AND tpenilaian_survey_bl.rekomendasi_penilaian="layak"');
			$data = $this->db->get('tblingkungan')->result();

			$this->output->set_output(json_encode($data));
		}*/
        function sektor_bl($cari) {
			$this->db->select('id_sektor_bl, nama_bl, keterangan');
			$this->db->like('CONCAT(id_sektor_bl,nama_bl)', $cari);
			$data = $this->db->get('sektor_bl')->result();

			$this->output->set_output(json_encode($data));
		}
        function getsdana($cari) {
			$this->db->select('id_sdana, nama_sdana, keterangan');
			$this->db->like('CONCAT(IFNULL(id_sdana,""), IFNULL(nama_sdana,""), IFNULL(keterangan,""))', $cari);
			$data = $this->db->get('mas_sdana')->result();

			$this->output->set_output(json_encode($data));
		}
        // function get_anggaran($idbl){
        //     $this->db->select('b.id_anggaran_bl');
        //     $this->db->where('a.id_bl',$idbl);
        //     $this->db->join('mas_sektor_bl b','b.id_sektor_bl = a.jenis_bl AND b.STATUS =1 ','left');
        //     $query= $this->db->get('tblingkungan a');
        //     if($query->num_rows()>0){
        //         return $query->row();
        //     }else{
        //         return array();
        //     }
        //     }
        // function get_sisa($idanggaran){
        //     //$this->db->select('b.id_anggaran_bl');
        //    // $this->db->where('a.id_bl',$idbl);
        //    // $this->db->join('mas_sektor_bl b','b.id_sektor_bl = a.jenis_bl AND b. STATUS =1 ','left');
        //     $sql= "SELECT sum(a.nilai_disetujui)anggarandipakai,
        //     (SELECT anggaran FROM mas_sektor_bl WHERE id_anggaran_bl='$idanggaran' and `status`=1)totalanggaran,
        //     (SELECT sum(anggaran) FROM mas_sektor_bl WHERE `status`=1)totalsemua,
        //     (SELECT sum(nilai_disetujui) FROM tpersetujuan_bl 
        //      WHERE id_sdana in(SELECT id_anggaran_bl from mas_sektor_bl WHERE `status`=1))terpakaisemua
        //      FROM tpersetujuan_bl a WHERE id_sdana='$idanggaran'";
        //     $query = $this->db->query($sql);
        //     if($query->num_rows()>0){
        //         return $query->row();
        //     }else{
        //         return array();
        //     }
        //     }
        
        // sumiyati 16 09 17
        function get_anggaran($idbl){
            $this->db->select('b.id_sektor_bl');
            $this->db->where('a.id_bl',$idbl);
            $this->db->join('sektor_bl_anggaran b','b.id_sektor_bl = a.jenis_bl AND b.STATUS =1 ','left');
            $query= $this->db->get('tblingkungan a');
            if($query->num_rows()>0){
                return $query->row();
            }else{
                return array();
            }
            }

            function get_sisa($idanggaran){
            //$this->db->select('b.id_anggaran_bl');
           // $this->db->where('a.id_bl',$idbl);
           // $this->db->join('mas_sektor_bl b','b.id_sektor_bl = a.jenis_bl AND b. STATUS =1 ','left');
            $sql= "SELECT sum(a.nilai_disetujui)anggarandipakai,
            (SELECT anggaran FROM sektor_bl_anggaran WHERE id_sektor_bl='$idanggaran' and `status`=1)totalanggaran,
            (SELECT sum(anggaran) FROM sektor_bl_anggaran WHERE `status`=1)totalsemua,
            (SELECT sum(nilai_disetujui) FROM tpersetujuan_bl 
             WHERE id_sdana in(SELECT id_sektor_bl from sektor_bl_anggaran WHERE `status`=1))terpakaisemua
             FROM tpersetujuan_bl a WHERE id_sdana='$idanggaran'";
            $query = $this->db->query($sql);
            if($query->num_rows()>0){
                return $query->row();
            }else{
                return array();
            }
            }


         public function usulan(){
			$param = $this->uri->segment(4);
			$this->db->where("id_bl", $param);
			$vendor = $this->db->get("penilaian_survey_bl_vd");
			return $vendor;
		}

		public function update_bl($filter, $data)
		{
			$this->db->where($filter);
			$update = $this->db->update('tblingkungan', $data);

			if($update){
				$result = 1;
			} else {
				$result = 0;
			}

			return $result;
		}


	}
