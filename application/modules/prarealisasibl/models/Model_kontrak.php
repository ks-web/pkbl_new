<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	Class Model_kontrak extends MY_Model {
		function __construct() {
			parent::__construct();
			$this->_table = "tpersetujuan_bl";
			// for insert, update, delete
			$this->_view = "kontrak_bl_vd";
			// for call view
			$this->_order = 'desc';
			$this->_sort = 'tanggal_disetujui';
			$this->_page = 1;
			$this->_rows = 10;

			$this->_create = true;
			$this->_update = true;

			if ($this->uri->segment(4)) {
				$this->_filter = array('id_persetujuan' => $this->uri->segment(4));
			}

			//parameter from post/get - search function
			if ($this->input->post('q')) {
				$this->_like = array(
					'id_bl' => $this->input->post('q'),
					'instansi' => $this->input->post('q'),
					'nama' => $this->input->post('q')
				);
			}

			$this->_param = array('id_persetujuan' => $this->input->post('id_persetujuan'));

			//data array for input to database

			$tgl_kontrak = strtotime($this->input->post('tgl_kontrak'));
			$y = date('Y', $tgl_kontrak) + 1;
			$satu_thn = $y . date('-m-d', $tgl_kontrak);

			$this->_data = array(
				'tgl_kontrak' => $this->input->post('tgl_kontrak'),
				'no_kontrak' => date('Y') . $this->input->post('id_bl'),
				'tgl_expired_kontrak' => $satu_thn
			);

		}

		function getBL($cari) {
			//$this->db->select('id_bl,tanggal_input,no_ktp,nama,instansi,nilai,kota,alamat');
			$this->db->select('a.*,b.*,c.*,d.*,e.nama_sdana');
			$this->db->join('tpenilaian_survey_bl b', 'b.id_bl=a.id_bl', 'left');
			$this->db->join('tpersetujuan_bl c', 'c.id_bl=a.id_bl', 'left');
			$this->db->join('sektor_bl d', 'd.id_sektor_bl=a.jenis_bl', 'left');
			$this->db->join('mas_sdana e', 'e.id_sdana=c.id_sdana', 'left');
			$this->db->where('a.status', 'Proses Persetujuan');
			$this->db->where('c.keputusan', 'Disetujui');
			$this->db->like('CONCAT(a.id_bl,a.tanggal_input,a.no_ktp,a.nama,a.instansi,a.nilai,a.kota,a.alamat)', $cari);
			$data = $this->db->get('tblingkungan a')->result();
			$this->output->set_output(json_encode($data));
			//echo $this->db->last_query();
		}

		function sektor_bl($cari) {
			$this->db->select('id_sektor_bl, nama_bl, keterangan');
			$this->db->like('CONCAT(id_sektor_bl,nama_bl)', $cari);
			$data = $this->db->get('sektor_bl')->result();

			$this->output->set_output(json_encode($data));
		}

		function editbl($id, $databl) {
			$this->db->where('id_bl', $id);
			//$this->db->where('idPerusahaan',$this->session->userdata('idPerusahaan'));
			$update = $this->db->update('tblingkungan', $databl);

		}

		function editpersetujuandana($id, $data) {
			$this->db->where('id_bl', $id);
			//$this->db->where('idPerusahaan',$this->session->userdata('idPerusahaan'));
			$update = $this->db->update('tpersetujuan_bl', $data);

		}

		public function kontrak($id) {
			//$param = $this->uri->segment(4);
			$this->db->where("id_persetujuan", $id);
			$kontrak = $this->db->get("detail_persetujuan_kontrakbl_vd");
			return $kontrak;
		}
        function getpasal($cari) {
			$this->db->select('pasal, nama_pasal,deskripsi_pasal');
			$this->db->like('CONCAT(pasal,nama_pasal,deskripsi_pasal)', $cari);
			$data = $this->db->get('master_pasal_bl')->result();

			$this->output->set_output(json_encode($data));
		}
		function getkontraktor($cari) {
			$this->db->select('id_kontraktor, kontraktor');
			$this->db->like('CONCAT(id_kontraktor,kontraktor)', $cari);
			$data = $this->db->get('mas_kontraktor')->result();

			$this->output->set_output(json_encode($data));
		}
        function getisikontrak($idpersetujuan) {
			//$this->db->join('mas_kontraktor b', 'b.id_kontraktor=a.id_kontraktor', 'left');
			$this->db->where("id_persetujuan", $idpersetujuan);
			$data = $this->db->get('tkontrak_bl')->result();
			$this->output->set_output(json_encode($data));
		}
		function getdataalih($idpersetujuan) {
			$this->db->join('mas_kontraktor b', 'b.id_kontraktor=a.id_kontraktor', 'left');
			$this->db->where("a.id_persetujuan", $idpersetujuan);
			$data = $this->db->get('detail_persetujuan_kontrak a')->result();
			$this->output->set_output(json_encode($data));
		}
        function save_pasal($data) {

			$tambah = $this->db->insert('tkontrak_bl', $data);
			if ($tambah) {
				$this->output->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_output(json_encode(array('msg' => $this->db->_error_message())));
			}
		}
		function save_alih($data) {

			$tambah = $this->db->insert('detail_persetujuan_kontrak', $data);
			if ($tambah) {
				$this->output->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_output(json_encode(array('msg' => $this->db->_error_message())));
			}
		}
        function update_isi($data, $idkontrak) {
			$this->db->where('id_t_kontrak', $idkontrak);
			$update = $this->db->update('tkontrak_bl', $data);

			if ($update) {
				$this->output->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_output(json_encode(array('msg' => 'Data Gagal Di Update !')));
			}

		}
		function update_alih($data, $idpersetujuan, $idkontraktor) {
			$this->db->where('id_persetujuan', $idpersetujuan);
			$this->db->where('id_kontraktor', $idkontraktor);
			$update = $this->db->update('detail_persetujuan_kontrak', $data);

			if ($update) {
				$this->output->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_output(json_encode(array('msg' => 'Data Gagal Di Update !')));
			}

		}
        function hapus_isi($idkontrak) {
			$this->db->where('id_t_kontrak', $idkontrak);
			$hapus = $this->db->delete('tkontrak_bl');
			if ($hapus) {
				$this->output->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_output(json_encode(array('msg' => 'Data Gagal Di Hapus !')));
			}

		}
		function hapus_alih($idpersetujuan, $idkontraktor) {
			$this->db->where('id_persetujuan', $idpersetujuan);
			$this->db->where('id_kontraktor', $idkontraktor);
			$hapus = $this->db->delete('detail_persetujuan_kontrak');
			if ($hapus) {
				$this->output->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_output(json_encode(array('msg' => 'Data Gagal Di Hapus !')));
			}

		}

		public function kontrak_bl_bast($id_bl) {
			$this->db->where("id_bl", $id_bl);
			$query = $this->db->get("kontrak_bl_bast_vd");
			return $query;
		}

		public function kontrak_bl_bast_user($id_bl) {
			$this->db->where("idPegawai", $id_bl);
			$query = $this->db->get("mas_pegawai");
			return $query;
		}
        function isikontrak($id) {
		
			//$this->db->select('');
			$this->db->where("id_persetujuan", $id);
			$isikontrak = $this->db->get("tkontrak_bl");
			return $isikontrak;
		}

	}
