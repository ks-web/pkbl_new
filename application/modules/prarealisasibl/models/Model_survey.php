<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	Class Model_survey extends MY_Model {
		function __construct() {
			parent::__construct();
			$this->_table = "tpenugasan_survey_bl"; // for insert, update, delete
			$this->_view = "penugasan_bl_vd"; // for call view
			$this->_order = 'desc';
			$this->_sort = 'tanggal_survey';
			$this->_page = 1;
			$this->_rows = 10;

			$this->_create = true;
			$this->_update = true;

			if ($this->uri->segment(4)) {
				$this->_filter = array('id_bl' => $this->uri->segment(4));
			}

			//parameter from post/get - search function
			if ($this->input->post('q')) {
				$this->_like = array(
					'id_bl' => $this->input->post('q'),
					'tanggal_input' => $this->input->post('q'),
					'nama' => $this->input->post('q')
				);
			}

			$this->_param = array('id_bl' => $this->input->post('nama_bl'));

			//data array for input to database
			$this->_data = array(
				'id_bl' => $this->input->post('nama_bl'),
                'tanggal_survey' => $this->input->post('tanggal_input'),
                'petugas_survey' => $this->input->post('petugas'),
                'pemberi_tugas' => $this->session->userdata('id')
			);
			
		}
        public function download_form($idbl){
			//$param = $this->uri->segment(4);
			$this->db->where("id_bl", $idbl);
			$survey = $this->db->get("penugasan_bl_vd");
			return $survey;
		}

	}
