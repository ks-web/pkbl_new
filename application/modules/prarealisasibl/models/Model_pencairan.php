<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	Class Model_pencairan extends MY_Model {
		function __construct() {
			parent::__construct();
			$this->_table = "tcd_all";
			// for insert, update, delete
			$this->_view = "tcd_bl_vd";
			// for call view
			$this->_order = 'desc';
			$this->_sort = 'id_bl';
			$this->_page = 1;
			$this->_rows = 10;

			$this->_create = true;
			$this->_update = true;

			if ($this->uri->segment(4)) {
				$this->_filter = array('id_persetujuan' => $this->uri->segment(4));
			}

			//parameter from post/get - search function
			if ($this->input->post('q')) {
				$this->_like = array(
					'id_bl' => $this->input->post('q'),
					'instansi' => $this->input->post('q'),
					'nama' => $this->input->post('q')
				);
			}

			$this->_param = array('no' => $this->input->post('id_bl'));

			//data array for input to database
			$this->_data = array(
				'no' => $this->input->post('id_bl'),
				'tanggal_cd' => $this->input->post('tanggal_cd'),
				'kode_bayar' => $this->input->post('kode_bayar'),
				'code' => "BL"
			);

		}

		function get_idcd() {
			$q = $this->db->query("SELECT LPAD(RIGHT(id_cd,4)+1,4,0) as id_cd FROM tcd_all WHERE id_cd LIKE DATE_FORMAT(NOW(),'%Y%m%') ORDER BY id_cd DESC limit 1");
			return $q;
		}

		function editbl($id, $databl) {
			$this->db->where('id_bl', $id);
			//$this->db->where('idPerusahaan',$this->session->userdata('idPerusahaan'));
			$update = $this->db->update('tblingkungan', $databl);

		}

		function getakun($idbl) {
			$sql = "SELECT * FROM tcd_all WHERE no='$idbl'";
			$query = $this->db->query($sql);
			$row = $query->row();
			$idcd = $row->id_cd;
			$this->db->select('*,(SELECT SUM(debet) FROM detail_akun_transaksi_all WHERE status="BL" AND transaksi="CD" AND id_cd_um="' . $idcd . '")totaldebet,(SELECT SUM(kredit) FROM detail_akun_transaksi_all WHERE status="BL" AND transaksi="CD" AND id_cd_um="' . $idcd . '")totalkredit');
			$this->db->from('detail_akun_transaksi_all');
			$this->db->join('mas_account', 'mas_account.kode_account = detail_akun_transaksi_all.account_no');
			$this->db->where("id_cd_um", $idcd);
			$this->db->where('status', 'BL');
			$this->db->where('transaksi', 'CD');
			$this->db->order_by('debet DESC');
			$dataakun = $this->db->get();
			
			return $dataakun;
		}

		public function pencairan() {
			$param = $this->uri->segment(4);
			$this->db->where("id_bl", $param);
			$vendor = $this->db->get("tcd_bl_vd");
			return $vendor;
		}

		public function getsign() {
			$sign = $this->db->get("sign_vd");
			return $sign;

		}

		function getakunbl($cari) {
			$this->db->select('account, uraian');
			$this->db->like('CONCAT(account,uraian)', $cari);
			$data = $this->db->get('account_bl')->result();

			$this->output->set_output(json_encode($data));
		}

		function getdataakun($idcd) {
			//	$this->db->select('account, uraian');
			$this->db->where("id_cd_um", $idcd);
			$data = $this->db->get('detail_akun_transaksi_all')->result();
			$this->output->set_output(json_encode($data));
		}

		function save_akun($data) {

			$tambah = $this->db->insert('detail_akun_transaksi_all', $data);
			if ($tambah) {
				$this->output->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_output(json_encode(array('msg' => $this->db->_error_message())));
			}
		}

		function update_akunbl($data, $idakun) {
			$this->db->where('id_akun', $idakun);
			$update = $this->db->update('detail_akun_transaksi_all', $data);

			if ($update) {
				$this->output->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_output(json_encode(array('msg' => 'Data Gagal Di Update !')));
			}

		}

		function hapus_akun($idakun) {
			$this->db->where('id_akun', $idakun);
			$hapus = $this->db->delete('detail_akun_transaksi_all');
			if ($hapus) {
				$this->output->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_output(json_encode(array('msg' => 'Data Gagal Di Hapus !')));
			}

		}

		function getBL($cari, $param) {
			//$this->db->select('id_bl,tanggal_input,no_ktp,nama,instansi,nilai,kota,alamat');
			$this->db->select('a.*,c.nilai_rekomendasi,c.nilai_disetujui,d.nama_bl,e.nama_sdana,f.rekomendasi_penilaian,b.tanggal_survey,f.nilai_rekomendasi,c.pengalihan_kontrak');
			$this->db->join('tpenugasan_survey_bl b', 'b.id_bl=a.id_bl', 'left');
			$this->db->join('tpersetujuan_bl c', 'c.id_bl=a.id_bl', 'left');
			$this->db->join('sektor_bl d', 'd.id_sektor_bl=a.jenis_bl', 'left');
			// $this->db->join('mas_bidangbantuanbl g', 'g.id_sektor_bl=d.id_sektor_bl',
			// 'left');
			$this->db->join('mas_sdana e', 'e.id_sdana=c.id_sdana', 'left');
			$this->db->join('tpenilaian_survey_bl f', 'f.id_bl=a.id_bl', 'left');
			$this->db->where('a.status', $param);
			$this->db->where('c.keputusan', 'disetujui');
			$this->db->like('CONCAT(a.id_bl,a.tanggal_input,a.no_ktp,a.nama,a.instansi,a.nilai,a.kota,a.alamat)', $cari);
			$data = $this->db->get('tblingkungan a')->result();
			$this->output->set_output(json_encode($data));
			//echo $this->db->last_query();
		}

		function insert_default_account($idbl, $id_cd) {
			// cari jenis bl
			$this->db->select('tblingkungan.id_bl, tblingkungan.jenis_bl, tpenilaian_survey_bl.nilai_rekomendasi, sektor_bl.account_debit, sektor_bl.account_kredit');
			$this->db->from('tblingkungan');
			//$this->db->join('tpersetujuan_bl', 'tpersetujuan_bl.id_bl = tblingkungan.id_bl');
			$this->db->join('tpenilaian_survey_bl', 'tpenilaian_survey_bl.id_bl = tblingkungan.id_bl');
			$this->db->join('sektor_bl', 'sektor_bl.id_sektor_bl = tblingkungan.jenis_bl');
			$this->db->where('tblingkungan.id_bl', $idbl);
			$q_bl = $this->db->get();
			if ($q_bl->num_rows() > 0) {
				$databl = $q_bl->row_array();
				$data_akun_ins = array(
					'id_cd_um' => $id_cd,
					'account_no' => $databl['account_debit'],
					'debet' => $databl['nilai_rekomendasi'],
					'kredit' => 0,
					'status' => 'BL',
					'transaksi' => 'CD'
				);
				$this->db->insert('detail_akun_transaksi_all', $data_akun_ins);
				$data_akun_ins = array(
					'id_cd_um' => $id_cd,
					'account_no' => $databl['account_kredit'],
					'debet' => 0,
					'kredit' => $databl['nilai_rekomendasi'],
					'status' => 'BL',
					'transaksi' => 'CD'
				);
				$this->db->insert('detail_akun_transaksi_all', $data_akun_ins);
				$this->db->update('tcd_all', array('balanced' => '1'), array('id_cd' => $id_cd));
			}
		}

		public function createAccount($id){
        	//$this->db->select('sektor_usaha, nilai_disetujui');
        	//$this->db->from('tcd_mitra_vc');
        	$this->db->select('sektor_usaha, nilai_rekomendasi AS nilai_disetujui');
        	$this->db->from('tcd_bl_vd');
        	$this->db->where('id_cd', $id);
        	$query = $this->db->get();

        	$result = $query->row();

        	$this->db->select('A.*, B.uraian');
        	$this->db->from('mas_account_sektor A');
        	$this->db->join('mas_account B', 'B.kode_account = A.account_no');
        	$this->db->where(array('status' => 'BL', 'transaksi' => 'CD', 'id_sektor' => $result->sektor_usaha));
        	$query2 = $this->db->get();
        	$result2 = $query2->result_array();
        	//print_r($result2);
        	foreach ($result2 as $key) {
        		if($key['posisi'] == 'C'){
        			$credit = $result->nilai_disetujui;
        			$debet = 0;
        		} else {
        			$debet = $result->nilai_disetujui;
        			$credit = 0;
        		}
        		$data = array('id_cd_um' => $id, 'account_no' => $key['account_no'], 'nama_akun' => $key['uraian'], 'kredit' => $credit, 'debet' => $debet, 'status' => 'BL', 'transaksi' => 'CD');
        		//print_r($data);
        		//echo "<br>";
        		$this->db->insert('detail_akun_temp', $data);
        	}
        }

		public function deleteAccount($id)
        {
        	$this->db->select('id_cd');
        	$this->db->from('tcd_all');
        	$this->db->where('no', $id);
        	$query = $this->db->get();
        	$data = $query->row_array();

        	$this->db->where('id_cd_um', $data['id_cd']);
        	$this->db->where('status', 'BL');
        	$this->db->where('transaksi', 'CD');
        	$this->db->delete('detail_akun_transaksi_all');
        }


	}
