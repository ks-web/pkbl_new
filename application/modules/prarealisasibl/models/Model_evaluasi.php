<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	Class Model_evaluasi extends MY_Model {
		function __construct() {
			parent::__construct();
			$this->_table = "tpenilaian_survey_bl"; // for insert, update, delete
			$this->_view = "evaluator_vd"; // for call view
			$this->_order = 'desc';
			$this->_sort = 'tanggal_evaluator';
			$this->_page = 1;
			$this->_rows = 10;

			$this->_create = true;
			$this->_update = true;

			if ($this->uri->segment(4)) {
				$this->_filter = array('id_bling' => $this->uri->segment(4));
			}

			//parameter from post/get - search function
			if ($this->input->post('q')) {
				$this->_like = array(
					'id_bling' => $this->input->post('q'),
					'instansi' => $this->input->post('q'),
					'nama' => $this->input->post('q')
				);
			}

			$this->_param = array('id_bling' => $this->input->post('id_bling'));

			//data array for input to database
			$this->_data = array(
                'nilai_rekomendasi' => $this->input->post('nilai_rekomendasi'),
                'nilai_akhir' => $this->input->post('nilai_akhir'),
                'nilai_rekomendasi_barang'=> $this->input->post('nilai_rekomendasi_barang'),
                'keterangan_evaluator' => $this->input->post('keterangan_evaluator'),
                'tanggal_evaluator' => date("Y-m-d")
			);
			
		}
        function getBL($cari,$param) {
			//$this->db->select('id_bl,tanggal_input,no_ktp,nama,instansi,nilai,kota,alamat');
            //$where1 = "(f.keterangan_evaluator is null or f.keterangan_evaluator='')";
            //$where2="(f.tanggal_evaluator = '0000-00-00' or f.tanggal_evaluator is null)";
            //$where3="(f.nilai_rekomendasi is null or f.nilai_rekomendasi=0)";
            $this->db->select('a.*,c.nilai_rekomendasi,c.nilai_disetujui,d.nama_bl,e.nama_sdana,f.rekomendasi_penilaian,b.tanggal_survey,f.nilai_rekomendasi,f.keterangan_hasil');
            $this->db->join('tpenugasan_survey_bl b', 'b.id_bl=a.id_bl', 'left');
            $this->db->join('tpersetujuan_bl c', 'c.id_bl=a.id_bl', 'left');
            $this->db->join('sektor_bl d', 'd.id_sektor_bl=a.jenis_bl', 'left');
           // $this->db->join('mas_bidangbantuanbl g', 'g.id_sektor_bl=d.id_sektor_bl', 'left');
            $this->db->join('mas_sdana e', 'e.id_sdana=c.id_sdana', 'left');
            $this->db->join('tpenilaian_survey_bl f', 'f.id_bl=a.id_bl', 'left');
            $this->db->where ( 'a.status',$param);
            $this->db->where ( 'f.rekomendasi_penilaian','Layak');
            //$this->db->where($where1);
            //$this->db->where($where2);
            //$this->db->where($where3);
			$this->db->like('CONCAT(a.id_bl,a.tanggal_input,a.no_ktp,a.nama,a.instansi,a.nilai,a.kota,a.alamat)', $cari);
			$data = $this->db->get('tblingkungan a')->result();
			$this->output->set_output(json_encode($data));
            //echo $this->db->last_query();
		}
        function sektor_bl($cari) {
			$this->db->select('id_sektor_bl, nama_bl, keterangan');
			$this->db->like('CONCAT(id_sektor_bl,nama_bl)', $cari);
			$data = $this->db->get('sektor_bl')->result();

			$this->output->set_output(json_encode($data));
		}
        function editbl($id,$data)
	    {
	        $this->db->where('id_bl',$id);
	        //$this->db->where('idPerusahaan',$this->session->userdata('idPerusahaan'));
	        $update = $this->db->update('tblingkungan',$data);
	       
	        
	    }
         function hapus($id,$data)
	    {
	        $this->db->where('id_bling',$id);
	        $update = $this->db->update('tpenilaian_survey_bl',$data);
	        if($update){
	            $result="1";
	        }else{
	            $result="0";
	        }
	       
	        return $result;
	    }
	    public function usulan(){
				$param = $this->uri->segment(4);
				$this->db->where("id_bling", $param);
				$vendor = $this->db->get("penilaian_survey_bl_vd");
				return $vendor;
		}

		public function validasi($filter, $data)
		{
			$this->db->where($filter);
	        $update = $this->db->update('tblingkungan', $data);
	        if($update){
	            $result = 1;
	        } else {
	            $result = 0;
	        }
	       
	        return $result;
		}

	}

	
