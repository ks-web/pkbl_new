<?php
  date_default_timezone_set("Asia/Jakarta");
    
?>
<!DOCTYPE html>
<html>
<head>
	<title>Penugasan Survey Bina Lingkungan<br>PT Krakatau Steel Tbk</title>
	<style>

	u {    
    border-bottom: 1px dotted #000;
    text-decoration: none;
}
		div, .hr{

		    background-color: white;
		    /*width: 100%;*/
		    border: 0px solid black;
		    padding: 25px;
		    margin: 1px;
		    font-size: 10;
			padding-bottom:2px;
			padding-top: 0px;

		}
		hr{
		    background-color: white;
		    /*width: 100%;*/
		    border: 0px solid black;
		    padding: 0px;
		    margin: 0px;
		}
		h3, .title{
			text-align: center;
		}	
		table{
		    /*width: 100%;*/
		    border: 0px solid black;
		    border-collapse: collapse;
		    padding: 5px;
		    /*padding-bottom: 5px;*/
		}
		table,td,tr{
		    border: 0px solid black;
		    border-collapse: collapse;
		    padding: 1px;
			/*padding-bottom:2px;*/
        
		}	
    .topleft {
    position: absolute;
    top: 8px;
    left: 16px;
    font-size: 18px;	
    }	
	</style>
</head>
<body>
<div style="padding-top: 5px;padding-bottom: 0px;" align="right">
    <table  style=" padding-top: 10px;" width="100%" height="40%" border="0" >
        <tr>
            <td><center><img  src='<?=base_url('assets/images/logo2.png')?>' style="width:60px; height:80px;"></center></td>
            <td><strong><font style=" font-size: 20px;">DIVISI CORPORATE SOCIAL RESPONSIBILITY (CSR)</font></strong></td>
        </tr>
        <tr>
            <td colspan="2" align="center"><strong><font style=" font-size: 22px;">FORMULIR HASIL SURVEY</font></strong></td>
        </tr>
    </table>
    <table  style=" padding-top: 10px;" width="100%" height="40%" border="1" >
        <tr>
            <th>NAMA PENGURUS</th><th>NAMA LEMBAGA</th><th>ALAMAT LEMBAGA</th>
        </tr>
        <tr align="center">
            <td><?php echo $survey['nama'];?></td><td><?php echo $survey['instansi'];?></td><td><?php echo $survey['alamat'];?></td>
        </tr>
    </table><br /><br /><br />
    <table  style=" padding-top: 10px;" width="100%" height="40%" border="1" >
        <tr>
            <th>KETERANGAN HASIL SURVEY</th>
        </tr>
        <tr >
            <td style="height: 400px;padding-left: 5px;padding-top: 5px;" valign="top"><?php echo $survey['keterangan_hasil'];?></td>
        </tr>
    </table>
    <table  style=" padding-top: 10px;" width="100%" height="40%" border="1" >
        <tr align="center">
            <td>DIPERIKSA,<br /><br /><br /><br /><?php echo $survey['pemberi_tugas'];?><br /><b>Superintendent Bina Lingkungan Kemasyarakatan</b></td>
            <td>TGL SURVEY, <?php echo tanggal_display($survey['tanggal_survey']);?><br /><br /><br /><br /><?php echo $survey['petugas_survey'];?><br /><b>Surveyor</b></td>
        </tr>
    </table>
</div>

</body>
</html>
<?php
/*function Terbilang($x)
{
  $abil = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
  if ($x < 12)
    return " " . $abil[$x];
  elseif ($x < 20)
    return Terbilang($x - 10) . "belas";
  elseif ($x < 100)
    return Terbilang($x / 10) . " puluh" . Terbilang($x % 10);
  elseif ($x < 200)
    return " seratus" . Terbilang($x - 100);
  elseif ($x < 1000)
    return Terbilang($x / 100) . " ratus" . Terbilang($x % 100);
  elseif ($x < 2000)
    return " seribu" . Terbilang($x - 1000);
  elseif ($x < 1000000)
    return Terbilang($x / 1000) . " ribu" . Terbilang($x % 1000);
  elseif ($x < 1000000000)
    return Terbilang($x / 1000000) . " juta" . Terbilang($x % 1000000);
}*/

function tanggal_indo($tanggal)
{
	$bulan = array (1 =>   'Januari',
				'Februari',
				'Maret',
				'April',
				'Mei',
				'Juni',
				'Juli',
				'Agustus',
				'September',
				'Oktober',
				'November',
				'Desember'
			);
	$split = explode('-', $tanggal);
	return $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];
}
?>