<?php
$app = 'Persetujuan BL'; // nama aplikasi
$module = 'prarealisasibl';
$appLink = 'persetujuan'; // controller
$idField  = 'id_persetujuan'; //field key table
$id_bl  = 'id_bl'; //field key table
// $moduleref = 'kemitraan';
// $appLinkref = 'datamitra'; // controller
$appLinkpenilaian = 'penilaian'; // controller
?>

<script>
	var url;
	var app = "<?=$app?>";
	var appLink = '<?=$appLink?>';
	var module = '<?=$module?>';
	var idField = '<?=$idField?>';
	var id_bl = '<?=$id_bl?>';

	$(document).ready(function(){
		$("#row_ket_ditolak").hide();
	});

	function aksi(){
	   return a='Layak';
	}
    function add(){
        $('#dlg').dialog('open').dialog('setTitle','Tambah ' + app);
		$('#fm').form('clear');
        $('#tanggal_disetujui').datebox('setValue','<?=date("Y-m-d")?>');
        $('#nilai_disetujui').numberbox('setValue', 0);
	    $('#tbl').html('Save');
		url = '<?=base_url($module . '/' . $appLink . '/create')?>';
       
    }
    function edit(){
        var row = $('#dg').datagrid('getSelected');
		if (row){
			$('#tbl').html('Simpan');
			$('#dlg').dialog('open').dialog('setTitle','Edit '+ app);
			$('#fm').form('load',row);
			url = '<?=base_url($module . '/' . $appLink . '/update')?>/'+row[idField];
	    }
    }
    function hapus(){
        var row = $('#dg').datagrid('getSelected');		
	    if (row){
			$.messager.confirm('Confirm','Apakah Anda Yakin Akan Menghapus ini?',function(r){
				if (r){
					$.post('<?=base_url($module . '/' . $appLink . '/delete')?>/'+row[idField]+'/'+row[id_bl],function(result){
						if (result.success){
							$('#dg').datagrid('reload');	// reload the user data
							$('#id_bl').combogrid('grid').datagrid('reload');
						} else {
							$.messager.show({	// show error message
								title: 'Error',
								msg:result.msg
							});
						}
					},'json');
				}
			});
		}
    }
    function save(){
	   $('#fm').form('submit',{
	    	url: url,
	    	onSubmit: function(){
	    		return $(this).form('validate');
	    	},
	    	success: function(result){
	    		var result = eval('('+result+')');
	    		if (result.success){
	    			$('#dlg').dialog('close');		
	    			$('#dg').datagrid('reload');
	                $("#id_bl").combogrid({url:'<?=base_url($module . "/". $appLinkpenilaian."/getBL/5")?>'});
	    		} else {
	             $.messager.alert('Error Message',result.msg,'error');
	    		}
	    	}
	    });
	}
	function doSearch(value){
	    $('#dg').datagrid('load',{    
	    	q:value  
	    });
	}
    function get_anggaran(idbl){
        $.get('<?=base_url($module . '/' . $appLink . '/get_anggaran')?>/'+idbl,function(result){
            $('#anggaran').numberbox('setValue',result.sisa);
            $('#anggarantotal').numberbox('setValue',result.sisasemua);
            $('#idanggaran').val(result.idanggaran);
            },'json');
    }
    function preview(){
	    var row = $('#dg').datagrid('getSelected');
	    //alert(row.id_bl);
	    url  = '<?=base_url($module . '/' . $appLink . '/usulan')?>/'+row.id_bl;
	    if (row)
	    {
	        window.open(url,'_blank');
	    }
    }

    function action(row){
		// console.log(row);
		$.messager.confirm('Konfirmasi','Yakin Melakukan Proses ini?',function(r){
		    if (r){
		        $.post('<?php echo base_url('prarealisasibl/persetujuan/diketahui'); ?>/'+row, function(data){
					if(data){
						$('#dg').datagrid('reload');
					}
				});
		    }
		});
	}

	function diketahui(val,row){
		var icon = '';
		var cek = '';
		var session = '<?php echo $this->session->userdata('group_id');?>';
		console.log(row);
		if(val){
			icon = "<i style='color:blue' class='icon-ok icon-sm'></i>";
		}else{
			icon = "<i style='color:red' class='icon-remove icon-sm'></i>";
		}

		if(session == '3'){
			cek = "<center><button class='btn btn-default' onclick='action(\""+row.id_bl+"\")'>"+
				  icon+
				  "</button></center>";
		}else{
			cek = "<center>"+icon+"</center>";
		}
		
		return cek;
	}
</script>
 
<div class="tabs-container">                
	<table id="dg" class="easyui-datagrid" 
	data-options="  
	    url: '<?=base_url($module . '/' . $appLink . '/read')?>',
		singleSelect:'true', 
	    title:'<?=$app?>',
	    toolbar:'#toolbar',
	    iconCls:'icon-cog',
	    rownumbers:'true',  
	    idField:'<?=$idField?>', 
	    pagination:'true',
	    //fitColumns:'true',
	    pageList: [10,20,30]
	    "
	>
		<thead frozen="true">
			<tr>
				<th field="id_bl" width="100" sortable="true">ID</th>
		        <th field="tanggal_input" width="100" sortable="true">Tanggal<br>Daftar</th>
	            <th field="nama" width="200" sortable="true">Nama<br>Pemohon</th>
			</tr>
		</thead>
	    <thead>
	    	<tr>
	    		<th field="instansi" width="250" sortable="true">Nama<br>Instansi</th>
	            <th align="right" field="nilai" width="100" sortable="true" formatter="format_numberdisp">Nilai<br>Pengajuan</th>
	            <th field="nama_status" width="150" sortable="true">Status</th>
	            <th field="tanggal_survey" width="100" sortable="true">Tanggal<br>Survey</th>
	            <th field="keputusan" width="100" sortable="true">Rekomendasi<br>Persetujuan</th>
	            <th field="diketahui" width="80" sortable="true" formatter="diketahui">Diketahui</th>
	    	</tr>
	    </thead>
	</table>
	
	<!-- Model Start -->
	<div id="dlg" class="easyui-dialog" style="width:500px; height:500px; padding:10px" closed="true" buttons="#t_dlg_dis-buttons" >
	    <form id="fm" method="post" enctype="multipart/form-data" action="">
	        <fieldset>
	        	<legend>Identitas Data Pengajuan BL</legend>
	        	<table width="100%" align="center" border="0">
                    <input type="hidden" id="idanggaran" name="idanggaran" />
	                <tr>
		                <td>Tanggal Disetujui</td>
		                <td>:</td>
		                <td>
		                    <input id="tanggal_disetujui" name="tanggal_disetujui" required="true" class="easyui-datebox" style="width:100px;"/></td>
		                </td>
		            </tr>
		            <tr>
		                <td>Nama Mitra</td>
		                <td>:</td>
		                <td>
	                    <select name="id_bl" id="id_bl" class="easyui-combogrid" style="width:300px;" data-options="
	                        required:true,
	                        panelWidth:300,
	                        value:'id_bl',
	                        idField:'id_bl',
	                        textField:'instansi',
	                        mode:'remote',
	                        url:'<?=base_url($module . '/' . $appLink. '/getBL/5')?>',
	                        columns:[[
	                        {field:'id_bl',title:'ID',width:100},
	                        {field:'tanggal_input',title:'Tanggal Daftar',width:100},
	                        {field:'nama',title:'Nama Pemohon',width:100},
	                        {field:'no_ktp',title:'KTP Pemohon',width:100},
	                        {field:'instansi',title:'Nama Instansi',width:100},
	                        {field:'nilai',title:'Nilai Pengajuan',width:100},
	                        {field:'kota',title:'Kota',width:100}
	                        ]],
	                        onSelect: function(no,row){
	                           get_anggaran(row.id_bl);
	                           $('#nama').val(row.nama);
	                           $('#no_ktp').val(row.no_ktp);
	                           $('#alamat').val(row.alamat);
	                           $('#kota').val(row.kota);
                               //$('#tanggal_disetujui').datebox('setValue',new Date());
	                           //$('#jenis_bl').combogrid('setValue', row.jenis_bl);
                               $('#nama_bl').val(row.nama_bl);
	                           $('#kegiatan').val(row.kegiatan);
                               $('#nilai').val(row.nilai);
                               $('#anggaran').val(row.anggaran);
                               $('#nilai').numberbox('setValue', row.nilai);
                               $('#nilai_rekomendasi').numberbox('setValue', row.nilai_rekomendasi);
                               $('#keterangan_evaluator').val(row.keterangan_evaluator);
                               $('#barang').val(row.barang); 
                               
	                        }
	                        "></select>
		            </tr>
	                <tr>
		                <td>Nama Pemohon</td>
		                <td>:</td>
		                <td>
		                    <input type="text" name="nama" id="nama" style="width: 250px;">
		                </td>
		            </tr>
	                <tr>
		                <td>Nomor KTP</td>
		                <td>:</td>
		                <td>
		                    <input type="text" name="no_ktp" id="no_ktp" style="width: 250px;">
		                </td>
		            </tr>
	                <tr valign="top">
		                <td>Alamat</td>
		                <td>:</td>
		                <td>
		                    <textarea id="alamat" name="alamat" style="width: 300px ;"></textarea>
		                </td>
		            </tr>
	                <tr>
		                <td>Kota</td>
		                <td>:</td>
		                <td>
		                    <input type="text" name="kota" id="kota" style="width: 250px;">
		                </td>
		            </tr>
		            <!--tr>
		                <td>Tanggal Survey</td>
		                <td>:</td>
		                <td>
		                    <input id="tanggal_survey" name="tanggal_survey" class="easyui-textbox" style="width:100px;" readonly="true"/></td>
		                </td>
		            </tr-->
		            <tr>
		                <td>Nilai Pengajuan</td>
		                <td>:</td>
		                <td>
		                    <input class="easyui-numberbox" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%'" name="nilai" id="nilai" readonly style="width: 250px;">
		                </td>
		            </tr>

                    <tr>
		                <td>Jenis Bina Lingkungan</td>
		                <td>:</td>
		                <td>
		                    <input id="nama_bl" name="nama_bl" class="easyui-textbox" style="width:300px;" readonly="true"/></td>
		                </td>
		            </tr>
		            <!--tr>
		                <td>Jenis Bina Lingkungan</td>
		                <td>:</td>
		                <td>
	                    <select name="jenis_bl" id="jenis_bl" class="easyui-combogrid" style="width:300px;" data-options="
	                        panelWidth:300,
	                        value:'',
	                        idField:'id_sektor_bl',
	                        textField:'nama_bl',
	                        mode:'remote',
	                        url:'<?=base_url($module . '/' . $appLink . '/sektor_bl')?>',
	                        columns:[[
	                        {field:'id_sektor_bl',title:'ID',width:100},
	                        {field:'nama_bl',title:'Nama Bina Lingkungan',width:195}
	                        ]]
	                        "></select>
		                </td>
		            </tr-->
	                <tr>
		                <td>Bentuk Kegiatan</td>
		                <td>:</td>
		                <td>
		                    <input type="text" name="kegiatan" id="kegiatan" style="width: 300px;">
		                </td>
		            </tr>
                    <tr>
		                <td>Nilai Rekomendasi</td>
		                <td>:</td>
		                <td>
		                    <input class="easyui-numberbox" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%'" name="nilai_rekomendasi" id="nilai_rekomendasi" readonly style="width: 250px;">
		                </td>
		            </tr>
                    <tr>
	                <td>Keterangan Rekomendasi</td>
	                <td>:</td>
	                <td>
	                    <textarea id="keterangan_evaluator" name="keterangan_evaluator" style="width: 300px ;" readonly="true"></textarea>
	                </td>
	            </tr>
				</table>
	        </fieldset>
	        <fieldset>
	        	<legend>Ketentuan Dan Persetujuan Hasil</legend>
	        	<table width="100%" align="center" border="0">
	                <!--tr>
		                <td>Nilai Rekomendasi</td>
		                <td>:</td>
		                <td>
		                    <input class="easyui-textbox" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%'" name="nilai_rekomendasi" id="nilai_rekomendasi" required style="width: 250px;" readonly="true">
		                </td>
		            </tr-->
		            <tr>
		                <td>Sisa Anggaran Bidang</td>
		                <td>:</td>
		                <td>
		                    <input class="easyui-numberbox" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%'" name="anggaran" id="anggaran" readonly style="width: 250px;">
		                </td>
		            </tr>
                    <tr>
		                <td>Sisa Anggaran Keseluruhan</td>
		                <td>:</td>
		                <td>
		                    <input class="easyui-numberbox" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%'" name="anggarantotal" id="anggarantotal" readonly style="width: 250px;">
		                </td>
		            </tr>
		            <!--<tr>
		                <td>Nilai Disetujui</td>
		                <td>:</td>
		                <td>
		                    <input class="easyui-numberbox" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%'" name="nilai_disetujui" id="nilai_disetujui" required style="width: 250px;">
		                </td>
		            </tr>-->
                    <tr>
	                <td>Barang</td>
	                <td>:</td>
	                <td>
                        <input id="barang" name="barang" style="width: 300px ;" readonly="true">
	                </td>
	                </tr>
		            <tr>
		                <td>Keputusan</td>
		                <td>:</td>
		                <td>
		                    <select class="easyui-combobox" type="text" name="keputusan" id="keputusan" required style="width: 100px;" data-options="panelHeight:'auto',
		                    onSelect: function(rec){
								if(rec.value == 'ditolak'){
									$('#row_ket_ditolak').show();
								} else {
									$('#row_ket_ditolak').hide();
								}
		        			}">
		                    	<option value="disetujui">Disetujui</option>
		                    	<option value="ditolak">Ditolak</option>
		                    </select>
		                </td>
		            </tr>
		            <tr id="row_ket_ditolak">
		            	<td>Ket Ditolak</td>
		            	<td>:</td>
		            	<td><textarea name="ket_ditolak" id="ket_ditolak"></textarea></td>
		            </tr>
		            <!--tr>
		                <td>Sumber Dana</td>
		                <td>:</td>
		                <td>
		                    <select name="id_sdana" id="id_sdana" class="easyui-combogrid" style="width:300px;" data-options="
	                        panelWidth:300,
	                        value:'',
	                        idField:'id_sdana',
	                        textField:'nama_sdana',
	                        mode:'remote',
	                        url:'<?=base_url($module . '/' . $appLink . '/getsdana')?>',
	                        columns:[[
	                        {field:'id_sdana',title:'ID',width:100},
	                        {field:'nama_sdana',title:'Sumber Dana',width:195}
	                        ]]
	                        "></select>
		                </td>
		            </tr-->
	                <tr valign="top">
		                <td>Keterangan Pemberian Dana</td>
		                <td>:</td>
		                <td>
		                    <textarea id="keteranganpemberian" name="keteranganpemberian" style="width: 300px ;"></textarea>
		                </td>
		            </tr>
                    <tr>
                        <td>Buat Kontrak</td>
                        <td>:</td>
                        <td><input type="checkbox" id="pengalihan_kontrak" name="pengalihan_kontrak" value="1" style="width: 20px;" /> <!--select id="pengalihan_kontrak" name="pengalihan_kontrak">
                            <option value="1">YA</option>
                            <option value="0">TIDAK</option>
                        </select--></td>
                    </tr>
				</table>
	        </fieldset>
	    </form>
	   
	   	<!-- Tombol Add, Edit, Hapus Datagrid -->
       	<div id="toolbar">  
            <table align="center" style="padding: 0px; width: 99%;">
                <tr>
                    <td>
                        <a href="javascript:void(0)" class="btn btn-small btn-success" onclick="javascript:add()"><i class="icon-plus-sign icon-large"></i>&nbsp;Add</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:edit()"><i class="icon-edit"></i>&nbsp;Edit</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-danger" onclick="javascript:hapus()"><i class="icon-trash icon-large"></i>&nbsp;Delete</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-warning" iconCls: 'icon-search' onclick="javascript:preview()"><i class="icon-download-alt"></i>&nbsp;Cetak Usulan</a>
                    </td>
                    <td>&nbsp;</td>
                    <td align="right">
                    	<input class="easyui-searchbox" prompt="Silahkan Input Field" searcher="doSearch"  style="width:300px"/>
                    </td>
                </tr>
            </table>  
        </div>  
        <!-- end tombol datagrid -->
        <!-- Tombol simpan & Batal Form -->
        <div id="t_dlg_dis-buttons">
            <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:save()"><i class="icon-save icon-large"></i>&nbsp;Simpan</a>
            <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:$('#dlg').dialog('close')"><i class="icon-remove icon-large"></i>&nbsp;Batal</a>      
        </div>
	</div>
	<!-- Model end -->
</div>