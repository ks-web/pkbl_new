<?php
	$tgl = date("d-m-Y");
	$tgl_indo = tanggal_display();

	$tgl_arr = explode(' ', $tgl_indo);
?>
<!DOCTYPE html>
<html>
<head>
	<title>BAST Barang/Dana<br>PT Krakatau Steel Tbk</title>
	<style>
		.judul {
			font-family: Times;
			font-size: 18px;
			font-style: normal;
			font-weight: bold;
			text-decoration: underline;
			text-transform: none;
			color: #000000;
			background-color: #ffffff;
		}

		.dibawah_judul {
			font-family: Arial;
			font-size: 16px;
			font-style: normal;
			font-weight: normal;
			text-decoration: none;
			text-transform: none;
			color: #000000;
			background-color: #ffffff;
		}

		.teks {
			font-family: Arial;
			font-size: 14px;
			font-style: normal;
			font-weight: normal;
			text-decoration: none;
			text-transform: none;
			color: #000000;
			background-color: #ffffff;
		}

		u {
			border-bottom: 1px dotted #000;
			text-decoration: none;
		}
		div, .hr {

			background-color: white;
			/*width: 100%;*/
			border: 0px solid black;
			padding: 25px;
			margin: 1px;
			font-size: 10;
			padding-bottom: 2px;
			padding-top: 0px;
		}
		hr {
			background-color: white;
			/*width: 100%;*/
			border: 0px solid black;
			padding: 0px;
			margin: 0px;
		}
		h3, .title {
			text-align: center;
		}
		table {
			/*width: 100%;*/
			border: 0px solid black;
			border-collapse: collapse;
			padding: 5px;
			/*padding-bottom: 5px;*/
		}
		table, td, tr {
			border: 0px solid black;
			border-collapse: collapse;
			padding: 1px;
			/*padding-bottom:2px;*/

		}

	</style>
</head>
<body>
<center><img  src='<?=base_url('assets/images/headkti.png') ?>' style="width:20%; height:20%;"><center><br><br>
<div style="padding-top: 5px;padding-bottom: 0px;">
    <table style=" padding-top: 0px;" width="100%" height:40%>
        <tr>
            <td align="center" colspan="2" style="border-top:1px;border-bottom: 1px; padding-left: 5px;vertical-align: justify;padding-top: 5px;"><div class="judul">BERITA ACARA SERAH TERIMA BARANG/DANA</div></td>
        </tr>
        <tr>
            <td align="center" colspan="2" style="border-top:1px;border-bottom: 1px; padding-left: 5px;vertical-align: justify;padding-top: 5px;"><div class="judul">DARI PT KRAKATAU STEEL (PERSERO) Tbk.</div></td>
        </tr>
        <tr>
            <td align="center" colspan="2" style="border-top:1px;border-bottom: 1px; padding-left: 5px;vertical-align: justify;padding-top: 5px;"><div class="dibawah_judul">Nomor : <?=$data['id_bl'] ?></div></td>
        </tr>
        
        <tr>
            <td colspan="2" style="border-top:1px;border-bottom: 1px; padding-left: 5px;vertical-align: justify;padding-top: 5px;">
				Pada hari ini <?=nama_hari() ?> tanggal <strong><?=terbilang_display($tgl_arr[0],3) ?></strong> 
				bulan <strong><?=$tgl_arr[1] ?></strong> 
				tahun <strong><?=terbilang_display($tgl_arr[2],3) ?></strong> <strong>(<?=$tgl ?>)</strong> di Cilegon telah dilaksnakan serah terima dana bantuan 
				<?=$data['nama_bl'] ?> dari Divisi Community Development PT Krakatau Steel (Persero) Tbk :
			</td>
        </tr>
        <tr>
			<td align="center" colspan="2" style="border-top:1px;border-bottom: 1px; padding-left: 5px;vertical-align: justify;padding-top: 5px;">
				<br>
				<table class="tg" style="table-layout: fixed; width: 450px">
					<colgroup>
						<col style="width: 23.2px">
						<col style="width: 153.2px">
						<col style="width: 21.2px">
						<col style="width: 124.2px">
					</colgroup>
					<tr>
						<td class="tg-031e"><br /></th>
						<td class="tg-yw4l" style="vertical-align: top;">Kepada</th>
						<td class="tg-lqy6" style="vertical-align: top;">:</th>
						<td class="tg-yw4l"><?=$data['nama'] ?><br /><br /></th>
					</tr>
					<tr>
						<td class="tg-yw4l"></td>
						<td class="tg-yw4l" style="vertical-align: top;">Alamat</td>
						<td class="tg-lqy6" style="vertical-align: top;">:</td>
						<td class="tg-yw4l"><?=$data['alamat'] ?>, <?=strtolower($data['kota']) ?>, <?=strtolower($data['kecamatan']) ?>, <?=strtolower($data['kelurahan']) ?><br /><br /></td>
					</tr>
					<tr>
						<td class="tg-yw4l"></td>
						<td class="tg-yw4l" style="vertical-align: top;">Berupa</td>
						<td class="tg-lqy6" style="vertical-align: top;">:</td>
						<td class="tg-yw4l"><?=$data['nama_bl'] ?><br /><br /></td>
					</tr>
					<tr>
						<td class="tg-yw4l"></td>
						<td class="tg-yw4l" style="vertical-align: top;">Sebanyak/Senilai</td>
						<td class="tg-lqy6" style="vertical-align: top;">:</td>
						<td class="tg-yw4l">Rp. <?=number_format($data['nilai_disetujui'], 0, ",", ".") ?><br />
							(<?=terbilang_display($data['nilai_disetujui']) ?> Rupiah)<br /><br />
						</td>
					</tr>
				</table>
				<br><br>
			</td>
		</tr>
		<tr>
            <td colspan="2" style="border-top:1px;border-bottom: 1px; padding-left: 5px;vertical-align: justify;padding-top: 5px;">
				Demikian Berita Acara ini dibuat untuk dipergunakan sebagaimana mestiya.
			</td>
        </tr>
		<tr>
				<td  style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;vertical-align:top;text-align:right;">
					<center>
					<br>YANG MENERIMA<br><br><br><br><br><font style="text-decoration: underline;"><?=$data['nama'] ?></font>
					</center>
				</td>
				<td  style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;vertical-align:top;text-align:right;">
					<center>
					<br>YANG MENYERAHKAN<br><br><br><br><br><font style="text-decoration: underline;"><?=$data_login['nama_pegawai'] ?></font>
					</center>
				</td>
			</tr>
		<tr>
            <td colspan="2" style="border-top:1px;border-bottom: 1px; padding-left: 5px;vertical-align: justify;padding-top: 5px;">
				<center>
					<br>MENGETAHUI<br />DIVISI COMMUNITY DEVELOPMENT<br><br><br><br><br><font style="text-decoration: underline;"><strong><?=managerName ?></strong><br /><strong>Manager</strong></font>
				</center>
			</td>
        </tr>
		
    </table>
	<br>
</div>
</body>
</html>