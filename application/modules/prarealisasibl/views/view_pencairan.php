<?php
$app = 'Pencairan Dana'; // nama aplikasi
$module = 'prarealisasibl';
$appLink = 'pencairan'; // controller
$idField  = 'id_bl'; //field key table
$moduleref = 'vendorbl';
$appLinkref = 'databl'; // controller
$appLinkpenilaian = 'penilaian'; // controller
?>

<script>
    $("#instansi").hide();
	var url;
	var app = "<?=$app?>";
	var appLink = '<?=$appLink?>';
	var module = '<?=$module?>';
	var idField = '<?=$idField?>';
	var idParent;
	
	function aksi(){
	   return a='Layak';
	}
    function add(){
        $("#instansi").hide();
        $("#namabl").show();
        $('#dlg').dialog('open').dialog('setTitle','Tambah ' + app);
		$('#fm').form('clear');
        $('#tanggal_cd').datebox('setValue','<?=date("Y-m-d")?>');
	    $('#tbl').html('Save');
		url = '<?=base_url($module . '/' . $appLink . '/create')?>';
    }
    function editakun(){
        var row = $('#dg_akunbl').datagrid('getSelected');
        if (row){
            
			$('#fm_akunbl').form('load',row);
			url = '<?=base_url($module . '/' . $appLink . '/update_akunbl')?>/'+row.id_akun;
            
            }
    }
    function edit(){
        
        var row = $('#dg').datagrid('getSelected');
		if (row){
		    $("#namabl").hide();
            $("#instansi").show();
			$('#tbl').html('Simpan');
			$('#dlg').dialog('open').dialog('setTitle','Edit '+ app);
			$('#fm').form('load',row);
			url = '<?=base_url($module . '/' . $appLink . '/update')?>/'+row[idField];
            
	    }
    }
    
     function hapusakun(){
        var row = $('#dg_akunbl').datagrid('getSelected');		
	    if (row){
			$.messager.confirm('Confirm','Apakah Anda Yakin Akan Menghapus ini?',function(r){
				if (r){
					$.post('<?=base_url($module . '/' . $appLink . '/hapus_akun')?>/'+row.id_akun,function(result){
						if (result.success){
							$('#dg_akunbl').datagrid('reload');	// reload the user data
						} else {
							$.messager.show({	// show error message
								title: 'Error',
								msg:result.msg
							});
						}
					},'json');
				}
			});
		}
    }
    function hapus(){
        var row = $('#dg').datagrid('getSelected');		
	    if (row){
			$.messager.confirm('Confirm','Apakah Anda Yakin Akan Menghapus ini?',function(r){
				if (r){
					$.post('<?=base_url($module . '/' . $appLink . '/delete')?>/'+row[idField],function(result){
						if (result.success){
							$('#dg').datagrid('reload');	// reload the user data
                            $("#id_bl").combogrid({url:'<?=base_url($module . "/". $appLinkpenilaian."/getBL/6")?>'});
						} else {
							$.messager.show({	// show error message
								title: 'Error',
								msg:result.msg
							});
						}
					},'json');
				}
			});
		}
    }
    function preview(){
	    var row = $('#dg').datagrid('getSelected');
	    url  = '<?=base_url($module . '/' . $appLink . '/previewpencairandana')?>/'+row[idField];
	    if (row)
	    {
	        window.open(url,'_blank');
	    }
    }
    function save(){
	   $('#fm').form('submit',{
	    	url: url,
	    	onSubmit: function(){
	    		return $(this).form('validate');
	    	},
	    	success: function(result){
	    		var result = eval('('+result+')');
	    		if (result.success){
	    			$('#dlg').dialog('close');		
	    			$('#dg').datagrid('reload');
	                $("#id_bl").combogrid({url:'<?=base_url($module . "/". $appLinkpenilaian."/getBL/6")?>'});
	    		} else {
	    		// alert(result.success);
	            $.messager.alert('Error Message',result.msg,'error');
	    		}
	    	}
	    });
	}
	function doSearch(value){
	    $('#dg').datagrid('load',{    
	    	q:value  
	    });
	}
    
    function akun(value,row,index){
        console.log(row);
        var result = '<a href="javascript:void(0)" title="Daftar Akun BL" class="btn btn-small btn-default" onclick="javascript:akun_detail('+index+')"><i class="icon-tasks icon-large"></i></a>';
        if(row.status_progress != '1') {
 			result += '&nbsp;<a href="javascript:batal_cd(\''+row.id_cd+'\',\''+row.code+'\')" title="Batal CD" class="btn btn-small btn-default easyui-tooltip" ><i class="icon-remove icon-large" style=""></i></a>';
			
		} else {
			result += '&nbsp;<span style="color: red">Batal</span>';
		}
		return result;
	}

    function batal_cd(id_cd, code){
		$.messager.confirm('Confirm','Apakah Anda Yakin Akan Membatalkan CD ini?',function(r){
			if (r){
				$.post('<?=base_url($module . '/' . $appLink . '/batal_cd')?>',{id_cd: id_cd, code: 'BL'},function(result){
					if (result.success){
						$('#dg').datagrid('reload');	// reload the user data
					} else {
						$.messager.show({	// show error message
							title: 'Error',
							msg:result.msg
						});
					}
				},'json');
			}
		});
	}
	
	function akun_detail(idx){
		var row = $('#dg').datagrid('getRows')[idx];
		idParent = row.id_cd;
		$('#dlg-detail').dialog('open').dialog('setTitle','Edit ' + app);
		reload1() //dg1();
		// $('#dg-detail').datagrid({url: '<?=base_url($module . '/' . $appLink . '/read_detail')?>' + '/' + row.id_um});
	}
	function cetak(){
		var row = $('#dg').datagrid('getSelected');
	    url  = '<?=base_url($module . '/' . $appLink . '/cetak')?>/'+row[idField];
	    if (row)
	    {
	        window.open(url,'_blank');
	    }
    
    }
    function cek(){
		var row = $('#dg').datagrid('getSelected');
	    url  = '<?=base_url($module . '/' . $appLink . '/cek')?>/'+row[idField];
	    if (row)
	    {
	        window.open(url,'_blank');
	    }
    
    }
</script>
 
<div class="tabs-container">                
	<table id="dg" class="easyui-datagrid" 
	data-options="  
	    url: '<?=base_url($module . '/' . $appLink . '/read')?>',
		singleSelect:'true', 
	    title:'<?=$app?>',
	    toolbar:'#toolbar',
	    iconCls:'icon-cog',
	    rownumbers:'true',  
	    idField:'<?=$idField?>', 
	    pagination:'true',
	    fitColumns:'true',
	    pageList: [10,20,30]
	    "
	>
	    <thead>
	        <th field="id_bl" width="100" sortable="true">ID</th>
	        <th field="tanggal_cd" width="100" sortable="true">Tanggal<br>CD</th>
            <th field="nama" width="100" sortable="true">Nama<br>Pemohon</th>
            <th field="instansi" width="100" sortable="true">Nama<br>Instansi</th>
            <th field="nilai" width="100" sortable="true" formatter="format_numberdisp">Nilai<br>Pengajuan</th>
            <th field="nilai_rekomendasi" width="100" sortable="true" formatter="format_numberdisp">Nilai<br>Rekomendasi</th>            
            <th field="kode_bayar" width="100" sortable="true">Kode<br>Bayar</th>
            <th field="action" formatter="akun" width="100">action</th>
            <!--th field="nilai_disetujui" width="100" sortable="true">Nilai<br>Disetujui</th>
            <th field="nama_status" width="100" sortable="true">Status</th>
            <th field="tanggal_survey" width="100" sortable="true">Tanggal<br>Survey</th>
            <th field="keputusan" width="100" sortable="true">Rekomendasi<br>Persetujuan</th>
            <th field="tgl_kontrak" width="100" sortable="true">Tanggal<br>Kontrak</th>
            <th field="no_kontrak" width="100" sortable="true">Nomor<br>Kontrak</th-->
	    </thead>
	</table>
	
	<!-- Model Start -->
	<div id="dlg" class="easyui-dialog" style="width:500px; height:500px; padding:10px" closed="true" buttons="#t_dlg_dis-buttons" >
	    <form id="fm" method="post" enctype="multipart/form-data" action="">
	        <table width="100%" align="center" border="0">
                <tr>
	                <td>Tanggal CD</td>
	                <td>:</td>
	                <td>
	                    <input id="tanggal_cd" name="tanggal_cd"  class="easyui-datebox" style="width:100px;" /></td>
	                </td>
	            </tr>
	            <tr id="namabl">
	                <td>Nama Bina Lingkungan</td>
	                <td>:</td>
	                <td>
                    <select name="id_bl" id="id_bl" class="easyui-combogrid" style="width:300px;" data-options="
                        required:true,
                        panelWidth:300,
                        value:'id_bl',
                        idField:'id_bl',
                        textField:'instansi',
                        mode:'remote',
                        url:'<?=base_url($module . '/' . $appLink . '/getBL/6')?>',
                        columns:[[
                        {field:'id_bl',title:'ID',width:100},
                        {field:'tanggal_input',title:'Tanggal Daftar',width:100},
                        {field:'nama',title:'Nama Pemohon',width:100},
                        {field:'instansi',title:'Nama Instansi',width:100},
                        {field:'nilai',title:'Nilai Pengajuan',width:100},
                        {field:'kota',title:'Kota',width:100},
                        {field:'tanggal_survey',title:'Tanggal Survey',width:100}
                        ]],
                        onSelect: function(no,row){
                           $('#nama').val(row.nama);
                           $('#no_ktp').val(row.no_ktp);
                           $('#alamat').val(row.alamat);
                           $('#nilai').numberbox('setValue', row.nilai);
                           $('#nilai_rekomendasi').numberbox('setValue', row.nilai_rekomendasi);
                           $('#nilai_disetujui').numberbox('setValue', row.nilai_disetujui);
                           
                           $('#nama_sdana').val(row.nama_sdana);
                           $('#rekomendasi_penilaian').val(row.rekomendasi_penilaian);
                           
                           $('#nama_bl').val(row.nama_bl);
                           $('#tanggal_survey').val(row.tanggal_survey);
                           
                        }
                        "></select>
	            </tr>
                <tr id="instansi">
	                <td>Nama Instansi</td>
	                <td>:</td>
	                <td>
	                    <input type="text" name="instansi" id="instansi" required style="width: 250px;" readonly="true">
	                </td>
	            </tr>
                <tr>
	                <td>Nama Pemohon</td>
	                <td>:</td>
	                <td>
	                    <input type="text" name="nama" id="nama" required style="width: 250px;" readonly="true">
	                </td>
	            </tr>
                <tr>
	                <td>Nomor KTP</td>
	                <td>:</td>
	                <td>
	                    <input type="text" name="no_ktp" id="no_ktp" required style="width: 250px;" readonly="true">
	                </td>
	            </tr>
                <tr>
	                <td>Alamat</td>
	                <td>:</td>
	                <td>
	                    <textarea id="alamat" name="alamat" style="width: 300px ;" required readonly="true"></textarea>
	                </td>
	            </tr>
                <tr>
	                <td>Nilai Pengajuan</td>
	                <td>:</td>
	                <td>
                        <input class="easyui-numberbox" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%'" name="nilai" id="nilai" readonly style="width: 250px;">
	                </td>
	            </tr>
                <!--tr>
	                <td>Tanggal Survey</td>
	                <td>:</td>
	                <td>
	                    <input id="tanggal_survey" name="tanggal_survey"  class="easyui-datebox" style="width:100px;" readonly="true"/></td>
	                </td>
	            </tr-->
                <tr>
	                <td>Nilai Rekomendasi</td>
	                <td>:</td>
	                <td>
                        <input class="easyui-numberbox" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%'" name="nilai_rekomendasi" id="nilai_rekomendasi" style="width: 250px;" readonly="true">
	                </td>
	            </tr>
                <!--<tr>
	                <td>Nilai Yang Disetujui</td>
	                <td>:</td>
	                <td>
                        <input class="easyui-numberbox" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%'" name="nilai_disetujui" id="nilai_disetujui" style="width: 250px;" readonly="true">
	                </td>
	            </tr>-->
                <tr>
	                <td>Jenis Bina Lingkungan</td>
	                <td>:</td>
	                <td>
	                    <input type="text" name="nama_bl" id="nama_bl" required style="width: 250px;" readonly="true">
	                </td>
	            </tr>
              <!--   <tr>
	                <td>Sumber Dana</td>
	                <td>:</td>
	                <td>
	                    <input type="text" name="nama_sdana" id="nama_sdana" required style="width: 250px;" readonly="true">
	                </td>
	            </tr> -->
                <tr>
	                <td>Rekomendasi Penilaian</td>
	                <td>:</td>
	                <td>
	                    <input type="text" name="rekomendasi_penilaian" id="rekomendasi_penilaian" required style="width: 250px;" readonly="true">
	                </td>
	            </tr>
                <tr>
	                <td>Kode Bayar</td>
	                <td>:</td>
	                <td>
	                    <select  class="easyui-combobox" type="text" name="kode_bayar" id="kode_bayar" required style="width: 200px;">
	                    	<option value="Transfer">Transfer</option>
	                    	<option value="Cheque">Cheque</option>
	                    </select>
	                </td>
	            </tr>
	        </table>
	    </form>
	   
	   	<!-- Tombol Add, Edit, Hapus Datagrid -->
       	<div id="toolbar">  
            <table align="center" style="padding: 0px; width: 99%;">
                <tr>
                    <td>
                        <a href="javascript:void(0)" class="btn btn-small btn-success" onclick="javascript:add()"><i class="icon-plus-sign icon-large"></i>&nbsp;Add</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:edit()"><i class="icon-edit"></i>&nbsp;Edit</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-danger" onclick="javascript:hapus()"><i class="icon-trash icon-large"></i>&nbsp;Delete</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-warning" iconCls: 'icon-search' onclick="javascript:preview()"><i class="icon-search icon-large"></i>&nbsp;Preview</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-warning" iconCls: 'icon-print' onclick="javascript:cetak()"><i class="icon-print icon-large"></i>&nbsp;Cetak</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-warning" iconCls: 'icon-print' onclick="javascript:cek()"><i class="icon-print icon-small"></i>&nbsp;Cetak Kwitansi</a>
                    </td>
                    <td>&nbsp;</td>
                    <td align="right">
                    	<input class="easyui-searchbox" prompt="Silahkan Input Field" searcher="doSearch"  style="width:300px"/>
                    </td>
                </tr>
            </table>  
        </div>  
        <!-- Tombol simpan & Batal Form -->
        <div id="t_dlg_dis-buttons">
            <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:save()"><i class="icon-save icon-large"></i>&nbsp;Simpan</a>
            <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:$('#dlg').dialog('close')"><i class="icon-remove icon-large"></i>&nbsp;Batal</a>      
        </div>
	</div>
    
    
	<!-- Model end -->
	<div id="dlg-detail" class="easyui-dialog" style="width:800px; height:auto; padding:10px" closed="true" buttons="#t_dlg_dis-buttonsx">
		<?php $this->load->view('view_pencairan_detail'); ?>
		<div id="t_dlg_dis-buttonsx">
            <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:$('#dlg-detail').dialog('close')"><i class="icon-remove icon-large"></i>&nbsp;Batal</a>      
        </div>
	</div>
</div>