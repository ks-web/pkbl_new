<?php
$app = 'Data Kontrak'; // nama aplikasi
$module = 'prarealisasibl';
$appLink = 'kontrak'; // controller
$idField  = 'id_bl'; //field key table
$moduleref = 'vendorbl';
$appLinkref = 'databl'; // controller
$appLinkpenilaian = 'penilaian'; // controller
?>

<script>
    
    $("#instansi").hide();
	var url;
	var app = "<?=$app?>";
	var appLink = '<?=$appLink?>';
	var module = '<?=$module?>';
	var idField = '<?=$idField?>';
	function aksi(){
	   return a='Layak';
	}
    function add(){
        $("#instansi").hide();
        $("#namabl").show();
        $('#dlg').dialog('open').dialog('setTitle','Tambah ' + app);
		$('#fm').form('clear');
	    $('#tbl').html('Save');
		url = '<?=base_url($module . '/' . $appLink . '/createkontrak')?>';
    }
    function edit(){
        
        var row = $('#dg').datagrid('getSelected');
		if (row){
		    $("#namabl").hide();
            $("#instansi").show();
			$('#tbl').html('Simpan');
			$('#dlg').dialog('open').dialog('setTitle','Edit '+ app);
			$('#fm').form('load',row);
			url = '<?=base_url($module . '/' . $appLink . '/update')?>/'+row[idField];
            
	    }
    }
    function hapus(){
        var row = $('#dg').datagrid('getSelected');		
	    if (row){
			$.messager.confirm('Confirm','Apakah Anda Yakin Akan Menghapus ini?',function(r){
				if (r){
					$.post('<?=base_url($module . '/' . $appLink . '/delete')?>/'+row[idField],function(result){
						if (result.success){
							$('#dg').datagrid('reload');	// reload the user data
						} else {
							$.messager.show({	// show error message
								title: 'Error',
								msg:result.msg
							});
						}
					},'json');
				}
			});
		}
    }
    function preview(){
	    var row = $('#dg_alihkontrak').datagrid('getSelected');
	    url  = '<?=base_url($module . '/' . $appLink . '/previewkontrak')?>/'+row.id_persetujuan;
	    if (row)
	    {
	        
	        window.open(url,'_blank');
	    }
    }
    
    function preview_bast(){
	    var row = $('#dg').datagrid('getSelected');
	    url  = '<?=base_url($module . '/' . $appLink . '/preview_bast_barang_dana')?>/'+row[idField];
	    if (row)
	    {
	        window.open(url,'_blank');
	    }
    }
    function save(){
	   $('#fm').form('submit',{
	    	url: url,
	    	onSubmit: function(){
	    		return $(this).form('validate');
	    	},
	    	success: function(result){
	    		var result = eval('('+result+')');
	    		if (result.success){
	    			$('#dlg').dialog('close');		
	    			$('#dg').datagrid('reload');
	                $("#id_bl").combogrid({url:'<?=base_url($module . "/". $appLinkpenilaian."/getBL/5")?>'});
	    		} else {
	    		// alert(result.success);
	            $.messager.alert('Error Message',result.msg,'error');
	    		}
	    	}
	    });
	}
	function doSearch(value){
	    $('#dg').datagrid('load',{    
	    	q:value  
	    });
	}
    function alih_kontrak(index,value) {
        list_kontrak = '<center> <a href="javascript:kontraktor_view()" title="Buat Kontrak" class="btn btn-small btn-default easyui-tooltip" ><i class="icon-tasks icon-large"></i></a> </center>';
        return list_kontrak;
    }
    function isi_kontrak(index,value) {
        content_kontrak = '<center> <a href="javascript:isi_kontrak_view()" title="Isi Kontrak" class="btn btn-small btn-default easyui-tooltip" ><i class="icon-tasks icon-large"></i></a> </center>';
        return content_kontrak;
    }
    function isi_kontrak_view(){
        var row = $('#dg_alihkontrak').datagrid('getSelected');
        if(row){
            
            $('#fm_isikontrak').form('clear');	
            $('#dlg_isi_kontrak').dialog('open').dialog('setTitle','Tambah Isi Kontrak');
            $(".hidepasal").hide();
            $("#dg_isikontrak").datagrid({url:'<?=base_url($module . "/". $appLink."/getisikontrak")?>/'+row.id_persetujuan});
            url ='<?=base_url($module . '/' . $appLink . '/save_pasal')?>/'+row.id_persetujuan+'/'+row.no_kontrak;
            var IS = CKEDITOR.instances['deskripsi_pasal'];
        if (IS) { 
            IS.destroy(true); 
        };
        CKEDITOR.replace('deskripsi_pasal');
                }
    }
    function kontraktor_view(){
        var row = $('#dg').datagrid('getSelected');
        if(row){
            
            $('#dlg-kontraktor').dialog('open').dialog('setTitle','Tambah Kontraktor');
            $('#fm_alihkontrak').form('clear');
            $("#dg_alihkontrak").datagrid({url:'<?=base_url($module . "/". $appLink."/getdataalih")?>/'+row.id_persetujuan});
            url ='<?=base_url($module . '/' . $appLink . '/save_alih')?>/'+row.id_persetujuan+'/'+row.id_bl;
        }
        
    }
     function save_pasal(){
	   $('#fm_isikontrak').form('submit',{
	    	url: url,
	    	onSubmit: function(){
	    		return $(this).form('validate');
	    	},
	    	success: function(result){
	    		var result = eval('('+result+')');
	    		if (result.success){
	    			$('#fm_isikontrak').form('clear');	
	    			$('#dg_isikontrak').datagrid('reload');
	    		} else {
	    		// alert(result.success);
	            $.messager.alert('Error Message',result.msg,'error');
	    		}
	    	}
	    });
	}
     function save_alih(){
	   $('#fm_alihkontrak').form('submit',{
	    	url: url,
	    	onSubmit: function(){
	    		return $(this).form('validate');
	    	},
	    	success: function(result){
	    		var result = eval('('+result+')');
	    		if (result.success){
	    			$('#fm_alihkontrak').form('clear');	
	    			$('#dg_alihkontrak').datagrid('reload');
	    		} else {
	    		// alert(result.success);
	            $.messager.alert('Error Message',result.msg,'error');
	    		}
	    	}
	    });
	}
    function editisi(){
        var row = $('#dg_isikontrak').datagrid('getSelected');
        if (row){
            $("#pasal").val(row.pasal);
            $("#judul_pasal").val(row.judul_pasal);
            CKEDITOR.instances.deskripsi_pasal.setData(row.deskripsi_pasal);
			//$('#fm_isikontrak').form('load',row);
			url = '<?=base_url($module . '/' . $appLink . '/update_isi')?>/'+row.id_t_kontrak;
            
            }
    }
    function editalih(){
        var row = $('#dg_alihkontrak').datagrid('getSelected');
        if (row){
            
			$('#fm_alihkontrak').form('load',row);
			url = '<?=base_url($module . '/' . $appLink . '/update_alih')?>/'+row.id_persetujuan+"/"+row.id_kontraktor;
            
            }
    }
    function hapusisi(){
        var row = $('#dg_isikontrak').datagrid('getSelected');		
	    if (row){
			$.messager.confirm('Confirm','Apakah Anda Yakin Akan Menghapus ini?',function(r){
				if (r){
					$.post('<?=base_url($module . '/' . $appLink . '/hapus_isi')?>/'+row.id_t_kontrak,function(result){
						if (result.success){
							$('#dg_isikontrak').datagrid('reload');	// reload the user data
						} else {
							$.messager.show({	// show error message
								title: 'Error',
								msg:result.msg
							});
						}
					},'json');
				}
			});
		}
    }
    function hapusalih(){
        var row = $('#dg_alihkontrak').datagrid('getSelected');		
	    if (row){
			$.messager.confirm('Confirm','Apakah Anda Yakin Akan Menghapus ini?',function(r){
				if (r){
					$.post('<?=base_url($module . '/' . $appLink . '/hapus_alih')?>/'+row.id_persetujuan+"/"+row.id_kontraktor,function(result){
						if (result.success){
							$('#dg_alihkontrak').datagrid('reload');	// reload the user data
						} else {
							$.messager.show({	// show error message
								title: 'Error',
								msg:result.msg
							});
						}
					},'json');
				}
			});
		}
    }
    
</script>
 
<div class="tabs-container">                
	<table id="dg" class="easyui-datagrid" 
	data-options="  
	    url: '<?=base_url($module . '/' . $appLink . '/read')?>',
		singleSelect:'true', 
	    title:'<?=$app?>',
	    toolbar:'#toolbar',
	    iconCls:'icon-cog',
	    rownumbers:'true',  
        nowrap:false, 
	    idField:'<?=$idField?>', 
	    pagination:'true',
	    pageList: [10,20,30]
	    "
	>
        <thead  data-options="frozen:true">  
            <tr>  	
                <th data-options="field:'id_bl',width:100">ID BL</th>
                <th data-options="field:'tanggal_input',width:100">Tanggal<br>Daftar</th>
                <th data-options="field:'nama',width:100">Nama<br>Pemohon</th>
            </tr>
        </thead>
	    <thead>
            <tr>
                <th data-options="field:'instansi',width:200">Nama<br>Instansi</th>
                <th data-options="field:'nilai',width:100">Nilai<br>Pengajuan</th>
                <th data-options="field:'nilai_disetujui',width:100">Nilai<br>Disetujui</th>
                <!--th data-options="field:'nama_status',width:150">Status</th-->
                <th data-options="field:'tanggal_survey',width:130">Tanggal<br>Survey</th>
                <th data-options="field:'tanggal_disetujui',width:130">Tanggal<br>Disetujui</th>
                <th data-options="field:'keputusan',width:100">Keputusan</th>
                <!--th data-options="field:'tgl_kontrak',width:100">Tanggal<br>Kontrak</th>
                <th data-options="field:'msg_expired',width:100">Message<br/>Expired</th-->
                <th data-options="field:'pengalihan_kontrak',width:80" formatter="alih_kontrak">Kontraktor</th>

            </tr>  
	    </thead>
	</table>
	
	<!-- Model Start -->
	<div id="dlg" class="easyui-dialog" style="width:500px; height:500px; padding:10px" closed="true" buttons="#t_dlg_dis-buttons" >
	    <form id="fm" method="post" enctype="multipart/form-data" action="">
	        <table width="100%" align="center" border="0">
                <tr>
	                <td>Tanggal Kontrak</td>
	                <td>:</td>
	                <td>
	                    <input id="tgl_kontrak" name="tgl_kontrak"  class="easyui-datebox" style="width:100px;" /></td>
	                </td>
	            </tr>
                <!--tr>
	                <td>Nomor Kontrak</td>
	                <td>:</td>
	                <td>
	                    <input type="text" name="no_kontrak" id="no_kontrak" required style="width: 250px;" >
	                </td>
	            </tr-->
	            <tr id="namabl">
	                <td>Nama Bina Lingkungan</td>
	                <td>:</td>
	                <td>
                    <select name="id_bl" id="id_bl" class="easyui-combogrid" style="width:300px;" data-options="
                        required:true,
                        panelWidth:300,
                        value:'instansi',
                        idField:'id_bl',
                        textField:'instansi',
                        mode:'remote',
                        url:'<?=base_url($module . '/' . $appLinkpenilaian . '/getBL/5')?>',
                        columns:[[
                        {field:'id_bl',title:'ID',width:30},
                        {field:'tanggal_input',title:'Tanggal Daftar',width:100},
                        {field:'nama',title:'Nama Pemohon',width:100},
                        {field:'instansi',title:'Nama Instansi',width:100},
                        {field:'nilai',title:'Nilai Pengajuan',width:100},
                        {field:'kota',title:'Kota',width:100},
                        {field:'tanggal_survey',title:'Tanggal Survey',width:100}
                        ]],
                        onSelect: function(no,row){
                           //$('#no_kontrak').val(row.id_bl);
                           $('#nama').val(row.nama);
                           $('#no_ktp').val(row.no_ktp);
                           $('#alamat').val(row.alamat);
                           $('#nilai').val(row.nilai);
                           //$('#nilai_rekomendasi').val(row.nilai_rekomendasi);
                           $('#nilai_disetujui').val(row.nilai_disetujui);
                           //$('#nama_sdana').val(row.nama_sdana);
                           $('#rekomendasi_penilaian').val(row.rekomendasi_penilaian);
                           
                           $('#nama_bl').val(row.nama_bl);
                           $('#tanggal_survey').val(row.tanggal_survey);
                           
                        }
                        "></select>
	            </tr>
                <tr id="instansi">
	                <td>Nama Instansi</td>
	                <td>:</td>
	                <td>
	                    <input type="text" name="instansi" id="instansi" required style="width: 250px;" readonly="true">
	                </td>
	            </tr>
                <tr>
	                <td>Nama Pemohon</td>
	                <td>:</td>
	                <td>
	                    <input type="text" name="nama" id="nama" required style="width: 250px;" readonly="true">
	                </td>
	            </tr>
                <tr>
	                <td>Nomor KTP</td>
	                <td>:</td>
	                <td>
	                    <input type="text" name="no_ktp" id="no_ktp" required style="width: 250px;" readonly="true">
	                </td>
	            </tr>
                <tr>
	                <td>Alamat</td>
	                <td>:</td>
	                <td>
	                    <textarea id="alamat" name="alamat" style="width: 300px ;" required readonly="true"></textarea>
	                </td>
	            </tr>
                <tr>
	                <td>Nilai Pengajuan</td>
	                <td>:</td>
	                <td>
                        <input class="easyui-numberbox" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%'" name="nilai" id="nilai"  style="width: 250px;" readonly="true">
	                </td>
	            </tr>
                <!--tr>
	                <td>Tanggal Survey</td>
	                <td>:</td>
	                <td>
	                    <input id="tanggal_survey" name="tanggal_survey"  class="easyui-datebox" style="width:100px;" readonly="true"/></td>
	                </td>
	            </tr-->
                <!--tr>
	                <td>Nilai Rekomendasi</td>
	                <td>:</td>
	                <td>
                        <input class="easyui-numberbox" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%'" name="nilai_rekomendasi" id="nilai_rekomendasi" style="width: 250px;" readonly="true">
	                </td>
	            </tr-->
                <tr>
	                <td>Nilai Yang Disetujui</td>
	                <td>:</td>
	                <td>
                        <input class="easyui-numberbox" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%'" name="nilai_disetujui" id="nilai_disetujui" style="width: 250px;" readonly="true">
	                </td>
	            </tr>
                <tr>
	                <td>Jenis Bina Lingkungan</td>
	                <td>:</td>
	                <td>
	                    <input type="text" name="nama_bl" id="nama_bl" required style="width: 250px;" readonly="true">
	                </td>
	            </tr>
                <!--tr>
	                <td>Sumber Dana</td>
	                <td>:</td>
	                <td>
	                    <input type="text" name="nama_sdana" id="nama_sdana" required style="width: 250px;" readonly="true">
	                </td>
	            </tr-->
                <tr>
	                <td>Rekomendasi Penilaian</td>
	                <td>:</td>
	                <td>
	                    <input type="text" name="rekomendasi_penilaian" id="rekomendasi_penilaian" required style="width: 250px;" readonly="true">
	                </td>
	            </tr>
                <tr>
                    <td>Pengalihan Kontrak</td>
                    <td>:</td>
                    <td>
                    <select id="pengalihan_kontrak" name="pengalihan_kontrak">
                        <option value="1">Ya</option>
                        <option value="0">Tidak</option>
                    </select>
                    </td>
                </tr>
                <!-- <tr>
	                <td>Tanggal Expired</td>
	                <td>:</td>
	                <td>
	                    <input id="tgl_exp" name="tgl_exp"  class="easyui-datebox" style="width:100px;" /></td>
	                </td>
	            </tr> -->
	        </table>
	    </form>
	   
	   	<!-- Tombol Add, Edit, Hapus Datagrid -->
       	<div id="toolbar">  
            <table align="center" style="padding: 0px; width: 99%;">
                <tr>
                    <td>
                        <!--a href="javascript:void(0)" class="btn btn-small btn-success" onclick="javascript:add()"><i class="icon-plus-sign icon-large"></i>&nbsp;Add</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:edit()"><i class="icon-edit"></i>&nbsp;Edit</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-danger" onclick="javascript:hapus()"><i class="icon-trash icon-large"></i>&nbsp;Delete</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-warning" iconCls: 'icon-search' onclick="javascript:preview()"><i class="icon-trash icon-large"></i>&nbsp;Preview</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-warning" iconCls: 'icon-search' onclick="javascript:preview_bast()">&nbsp;BAST</a-->
                    </td>
                    <td>&nbsp;</td>
                    <td align="right">
                    	<input class="easyui-searchbox" prompt="Silahkan Input Field" searcher="doSearch"  style="width:300px"/>
                    </td>
                </tr>
            </table>  
        </div>  
        <!-- end tombol datagrid -->
        <!-- Tombol simpan & Batal Form -->
        <div id="t_dlg_dis-buttons">
            <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:save()"><i class="icon-save icon-large"></i>&nbsp;Simpan</a>
            <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:$('#dlg').dialog('close')"><i class="icon-remove icon-large"></i>&nbsp;Batal</a>      
        </div>
	</div>
	<!-- Model end -->
</div>
<!--tambah isi kontrak -->
<div id="dlg_isi_kontrak" class="easyui-dialog" style="width:800px; height:600px; padding:10px" closed="true" buttons="#dlgpasal_view-buttons" >
    <form id="fm_isikontrak" method="post" enctype="multipart/form-data" action="">
        <table>
            <tr>
            <td>Pasal Kontrak</td>
            <td><select id="pasalx" name="pasal" class="easyui-combogrid" style="width:400px;"
                        data-options="
                        url:'<?=base_url($module . "/". $appLink."/getpasal")?>',
                        panelWidth:700,
                        idField:'pasal',
                        textField:'pasal',
                        mode:'remote',
                        nowrap:false, 
                        columns:[[
                        {field:'pasal',title:'Pasal',width:100},
                        {field:'nama_pasal',title:'Judul',width:200,valign:'top'},
                        {field:'deskripsi_pasal',title:'Deskripsi',width:400}
                        ]],
                        onSelect: function(no,row){
                           $('.hidepasal').show();
                           $('#pasal').val(row.pasal);
                           $('#judul_pasal').val(row.nama_pasal);
                           CKEDITOR.instances.deskripsi_pasal.setData(row.deskripsi_pasal);
                        }
                        "></select>
                        <!-- {field:'username',title:'username',width:130}, -->
                </td>
            </tr>
            <tr class="hidepasal">
                <td>Pasal</td>
                <td>
                    <input type="text" name="pasal" id="pasal" required style="width: 700px;">
                </td>
            </tr>
            <tr class="hidepasal">
                <td>Judul Pasal</td>
                <td>
                    <input type="text" name="judul_pasal" id="judul_pasal" required style="width: 700px;">
                </td>
            </tr>
             <tr class="hidepasal">
                <td>Deskripsi Pasal</td>
                <td><textarea id="deskripsi_pasal" name="deskripsi_pasal" style="width: 700px ;height: 300px;" rows="20" required ></textarea></td>
            </tr>
            <tr>
            <td></td>
            <td align="right">
                <a href="#" class="btn btn-small btn-primary" onclick="javascript:save_pasal()"><i class="icon-plus icon-large"></i>&nbsp;Add</a>
            </td>
            </tr>
        </table>
    </form>   
    <table id="dg_isikontrak" class="easyui-datagrid" style="height:400px;"
            data-options="
                singleSelect:'true', 
                toolbar:'#toolbaralisi',
                title:'Daftar Isi Kontrak',
                iconCls:'icon-tasks',
                rownumbers:'true',
                nowrap:false, 
                "
            >
            <thead> 
                <tr>
                    <!--th field="id_kontraktor" width="50">ID</th-->
                    <th field="pasal" width="100">Pasal</th>
                    <th field="judul_pasal" width="200">Judul Pasal</th>
                    <th field="deskripsi_pasal" width="400">Deskripsi Pasal</th>
                </tr>
            </thead>
        </table>
        <div id="dlgpasal_view-buttons">
            <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:preview()"><i class="icon-print icon-large"></i>&nbsp;Cetak</a>
            <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:$('#dlg_isi_kontrak').dialog('close')"><i class="icon-remove icon-large"></i>&nbsp;Batal</a>      
        </div>
</div>
<!--end tambah isi kontrak -->
<div id="dlg-kontraktor" class="easyui-dialog" style="width:530px; height:530px; padding:10px" closed="true" buttons="#dlgkontraktor_view-buttons" >
    <form id="fm_alihkontrak" method="post" enctype="multipart/form-data" action="">
        <table>
            <tr>
            <td>Kontraktor</td>
            <td><select id="id_kontraktor" name="id_kontraktor" class="easyui-combogrid" style="width:400px;"
                        data-options="
                        url:'<?=base_url($module . "/". $appLink."/getkontraktor")?>',
                        panelWidth:400,
                        idField:'id_kontraktor',
                        textField:'kontraktor',
                        mode:'remote',
                        columns:[[
                        {field:'id_kontraktor',title:'ID',width:100},
                        {field:'kontraktor',title:'Kontraktor',width:230}
                        ]]
                        "></select>
                        <!-- {field:'username',title:'username',width:130}, -->
                </td>
            </tr>
            <tr>
                <td>Jenis Pekerjaan</td>
                <td>
                    <input type="text" name="jenis_pekerjaan" id="jenis_pekerjaan" required style="width: 400px;">
                </td>
            </tr>
            <tr>
                <td>Nilai</td>
                <td>
                     <input class="easyui-numberbox" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%'" name="nilai_pekerjaan" id="nilai_pekerjaan" style="width: 300px;" >
                </td>
            </tr>
            <tr>
                <td>Keterangan</td>
                <td><textarea id="keterangan" name="keterangan" style="width: 300px ;" required ></textarea></td>
            </tr>
            <tr>
                <td>Tanggal Mulai</td>
                <td><input id="tanggal_mulai" name="tanggal_mulai"  class="easyui-datebox" style="width:100px;" /></td>
            </tr>
            <tr>
                <td>Tanggal Selesai</td>
                <td><input id="tanggal_selesai" name="tanggal_selesai"  class="easyui-datebox" style="width:100px;" /></td>
            </tr>
            <tr>
            <td></td>
            <td align="right">
                <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:save_alih()"><i class="icon-plus icon-large"></i>&nbsp;Add</a>
            </td>
            </tr>
        </table>
    </form>
    <table id="dg_alihkontrak" class="easyui-datagrid" style="height:200px;"
            data-options="
                singleSelect:'true', 
                toolbar:'#toolbaralih',
                title:'Daftar Alih Kontrak',
                iconCls:'icon-tasks',
                rownumbers:'true'
                "
            >
            <thead  data-options="frozen:true"> 
                <tr>
                    <!--th field="id_kontraktor" width="50">ID</th-->
                    <th field="no_kontrak" width="130">Nomor Kontrak</th>
                    <th field="kontraktor" width="130">Nama Kontraktor</th>
                    
                </tr>
            </thead>
            <thead>
                <tr>
                    <th field="jenis_pekerjaan" width="100">Pekerjaan</th>
                    <th field="nilai_pekerjaan" width="100">Nilai</th>
                    <th field="tanggal_mulai" width="100">Tgl Mulai</th>
                    <th field="tanggal_selesai" width="100">Tgl Selesai</th>
                    <th field="id_bl" width="130" formatter="isi_kontrak">Isi Kontrak</th>
                </tr>
                
            </thead>
        </table>
</div>
<div id="toolbaralisi">  
    <table align="center" style="padding: 0px; width: 99%;">
        <tr>
            <td>
                <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:editisi()"><i class="icon-edit"></i>&nbsp;Edit</a>
                <a href="javascript:void(0)" class="btn btn-small btn-danger" onclick="javascript:hapusisi()"><i class="icon-trash icon-large"></i>&nbsp;Delete</a>
                
            </td>
           
        </tr>
    </table>  
</div>
<div id="toolbaralih">  
    <table align="center" style="padding: 0px; width: 99%;">
        <tr>
            <td>
                <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:editalih()"><i class="icon-edit"></i>&nbsp;Edit</a>
                <a href="javascript:void(0)" class="btn btn-small btn-danger" onclick="javascript:hapusalih()"><i class="icon-trash icon-large"></i>&nbsp;Delete</a>
            </td>
           
        </tr>
    </table>  
</div>  