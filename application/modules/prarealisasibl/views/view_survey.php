<?php
$app = 'Penugasan Survey'; // nama aplikasi
$module = 'prarealisasibl';
$appLink = 'penugasansurvey'; // controller
$idField  = 'id_bl'; //field key table
$appLinkpenilaian = 'penilaian'; // controller
?>

<script>
	var url;
	var app = "<?=$app?>";
	var appLink = '<?=$appLink?>';
	var module = '<?=$module?>';
	var idField = '<?=$idField?>';
	function aksi(){
	   return a='Download';
	}
    function add(){
        $('#dlg').dialog('open').dialog('setTitle','Tambah ' + app);
		$('#fm').form('clear');
	    $('#tbl').html('Save');
		url = '<?=base_url($module . '/' . $appLink . '/create')?>';
    }
    function edit(){
        var row = $('#dg').datagrid('getSelected');
		if (row){
			$('#tbl').html('Simpan');
			$('#dlg').dialog('open').dialog('setTitle','Edit '+ app);
			$('#fm').form('load',{tanggal_input:row.tanggal_survey});

			$('#nama_bl').combogrid({
				value:row.id_bl,
				idField:'id_bl',
				textField:'instansi',
			});

			$('#petugas').combogrid({
				value:row.idPegawai,
				idField:'idPegawai',
				textField:'nama_pegawai',
			});

			url = '<?=base_url($module . '/' . $appLink . '/update')?>/'+row[idField];
	    }
    }
    function hapus(){
        var row = $('#dg').datagrid('getSelected');		
	    if (row){
			$.messager.confirm('Confirm','Apakah Anda Yakin Akan Menghapus ini?',function(r){
				if (r){
					$.post('<?=base_url($module . '/' . $appLink . '/delete')?>/'+row.id_bl,function(result){
						if (result.success){
							$('#dg').datagrid('reload');	// reload the user data
						} else {
							$.messager.show({	// show error message
								title: 'Error',
								msg:result.msg
							});
						}
					},'json');
				}
			});
		}
    }
    function save(){
	   $('#fm').form('submit',{
	    	url: url,
	    	onSubmit: function(){
	    		return $(this).form('validate');
	    	},
	    	success: function(result){
	    		var result = eval('('+result+')');
	    		if (result.success){
	    			$('#dlg').dialog('close');		
	    			$('#dg').datagrid('reload');
	                $("#nama_bl").combogrid({url:'<?=base_url($module . "/". $appLinkpenilaian."/getBL/2")?>'});
	    		} else {
	             $.messager.alert('Error Message',result.msg,'error');
	    		}
	    	}
	    });
	}
	function doSearch(value){
	    $('#dg').datagrid('load',{    
	    	q:value  
	    });
	}
    function download_form(){
        var row = $('#dg').datagrid('getSelected');
        url  = '<?=base_url($module . '/' . $appLink . '/download_form')?>/'+row[idField];
        if (row)
        {
            window.open(url,'_blank');
        }
    }
	$(document).ready(function(){
		/*$('#tanggal_input').datebox().datebox('calendar').calendar({
	        validator: function(date){
	            var now = new Date();
	            var d1 = new Date(now.getFullYear(), now.getMonth(), now.getDate());
	            var d2 = new Date(now.getFullYear(), now.getMonth(), now.getDate()+10);
	            return d1<=date && date<=d2;
	        }
	    });*/
	})
	function ceklist(value,index,row){
		var icon = ""
		if (value == 1){
			icon = "icon-ok";
		}
		return "<span style='color: red'><i class="+icon+"></i></span>";
	}
</script>
 
<div class="tabs-container">                
	<table id="dg" class="easyui-datagrid" 
	data-options="  
	    url: '<?=base_url($module . '/' . $appLink . '/read')?>',
		singleSelect:'true', 
	    title:'<?=$app?>',
	    toolbar:'#toolbar',
	    iconCls:'icon-cog',
	    rownumbers:'true',  
	    idField:'<?=$idField?>', 
	    pagination:'true',
        nowrap:false, 
	    //fitColumns:'true',
	    pageList: [10,20,30],
	    "
	>
		<thead data-options="frozen:true" >
			<th field="id_bl" width="100" sortable="true">ID BL</th>
	        <th field="tanggal_input" width="100" sortable="true">Tanggal Daftar</th>
            <th field="nama" width="100" sortable="true">Nama Pemohon</th>
		</thead>
	    <thead>
	        
            <th field="instansi" width="300" sortable="true">Nama Instansi</th>
            <th align="right" field="nilai" width="100" sortable="true" formatter="format_numberdisp">Nilai Pengajuan</th>
            <!-- <th field="status" width="50" sortable="true">Status</th> -->
            <th field="tanggal_survey" width="100" sortable="true">tanggal_survey</th>
            <th field="petugas_survey" width="100" sortable="true">petugas_survey</th>
            <th field="pemberi_tugas" width="100" sortable="true">pemberi_tugas</th>
            <th field="status" width="100" sortable="true">Status</th>
            <th field="ceklis" width="40" sortable="true" formatter="ceklist">CK</th>
	    </thead>
	</table>
	
	<!-- Model Start -->
	<div id="dlg" class="easyui-dialog" style="width:500px; height:250px; padding:10px" closed="true" buttons="#t_dlg_dis-buttons" >
	    <form id="fm" method="post" enctype="multipart/form-data" action="">
	        <table width="100%" align="center" border="0">
	            <tr style="height: 40px">
	                <td>Nama Bina Lingkungan</td>
	                <td style="width: 20px">:</td>
	                <td>
	                    <select id="nama_bl" class="easyui-combogrid" name="nama_bl" style="width:250px;"
				        data-options="
				        	mode: 'remote',
				        	method:'post',	
				            panelWidth:600,
				            idField:'id_bl',
				            textField:'instansi',
				            url:'<?=base_url($module . "/". $appLinkpenilaian."/getBL/2")?>',
				            nowrap:false,
				            columns:[[
				                {field:'id_bl',title:'ID BL',width:80},
				                {field:'tanggal_input',title:'Tanggal Daftar',width:85},
				                {field:'nama',title:'Nama Pemohon',width:100},
				                {field:'no_ktp',title:'KTP Pemohon',width:100},
				                {field:'instansi',title:'Nama Instansi',width:150},
				                {field:'nilai',title:'Nilai Pengajuan',width:100},
				                {field:'kota',title:'Kota',width:70}

				            ]]
				        "></select>
	                </td>
	            </tr>
	            <tr style="height: 40px">
	                <td>Tanggal Survey</td>
	                <td style="width: 20px" >:</td>
	                <td>
	                    <input id="tanggal_input" name="tanggal_input" required class="easyui-datebox" style="width:100px;"/>
	                </td>
	            </tr>
                <tr style="height: 40px">
	                <td>Petugas Survey</td>
	                <td style="width: 20px">:</td>
	                <td>
	                    <select id="petugas" class="easyui-combogrid" name="petugas" style="width:250px;"
				        data-options="
				            panelWidth:500,
				            idField:'idPegawai',
				            textField:'nama_pegawai',
				            url:'<?=base_url($module . "/". $appLink	."/petugas")?>',
				            nowrap:false,
				            columns:[[
				                {field:'idPegawai',title:'ID Pegawai',width:80},
				                {field:'nip',title:'NIP',width:100},
				                {field:'nama_pegawai',title:'Nama Pegawai',width:200}
				            ]]
				        "></select>
	                </td>
	            </tr>
	        </table>
	    </form>
	   
	   	<!-- Tombol Add, Edit, Hapus Datagrid -->
       	<div id="toolbar">  
            <table align="center" style="padding: 0px; width: 99%;">
                <tr>
                    <td>
                        <!--a href="javascript:void(0)" class="btn btn-small btn-success" onclick="javascript:add()"><i class="icon-plus-sign icon-large"></i>&nbsp;Add</a-->
                        <!--a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:edit()"><i class="icon-edit"></i>&nbsp;Edit</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-danger" onclick="javascript:hapus()"><i class="icon-trash icon-large"></i>&nbsp;Delete</a-->
                        <a href="javascript:void(0)" class="btn btn-small btn-warning" iconCls: 'icon-search' onclick="javascript:download_form()"><i class="icon-download-alt"></i>&nbsp;Download Form</a>
                    </td>
                    <td>&nbsp;</td>
                    <td align="right">
                    	<input class="easyui-searchbox" prompt="Silahkan Input Field" searcher="doSearch"  style="width:300px"/>
                    </td>
                </tr>
            </table>  
        </div>  
        <!-- end tombol datagrid -->
        <!-- Tombol simpan & Batal Form -->
        <div id="t_dlg_dis-buttons">
            <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:save()"><i class="icon-save icon-large"></i>&nbsp;Simpan</a>
            <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:$('#dlg').dialog('close')"><i class="icon-remove icon-large"></i>&nbsp;Batal</a>      
        </div>
	</div>
	<!-- Model end -->
</div>