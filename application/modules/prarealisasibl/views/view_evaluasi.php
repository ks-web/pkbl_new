<?php
$app = 'Data Evaluator'; // nama aplikasi
$module = 'prarealisasibl';
$appLink = 'evaluasi'; // controller
$idField  = 'id_bling'; //field key table
$moduleref = 'vendorbl';
$appLinkref = 'databl'; // controller
?>

<script>
    $("#instansi").hide();
	var url;
	var app = "<?=$app?>";
	var appLink = '<?=$appLink?>';
	var module = '<?=$module?>';
	var idField = '<?=$idField?>';
	function aksi(){
	   return a='Layak';
	}
    function add(){
        $("#instansi").hide();
        $("#namabl").show();
        $('#dlg').dialog('open').dialog('setTitle','Tambah ' + app);
		$('#fm').form('clear');
        $('#nilai_rekomendasi').numberbox('setValue', 0);
	    $('#tbl').html('Save');
		url = '<?=base_url($module . '/' . $appLink . '/create')?>';
    }
    function edit(){
        
        var row = $('#dg').datagrid('getSelected');
		if (row){
			$('#fm').form('clear');
		    $("#namabl").hide();
            $("#instansi").show();
			$('#tbl').html('Simpan');
			$('#dlg').dialog('open').dialog('setTitle','Edit '+ app);
			$('#fm').form('load',row);
			url = '<?=base_url($module . '/' . $appLink . '/update')?>/'+row[idField];
            
	    }
    }
    function hapus(){
        var row = $('#dg').datagrid('getSelected');		
	    if (row){
			$.messager.confirm('Confirm','Apakah Anda Yakin Akan Menghapus ini?',function(r){
				if (r){
					$.post('<?=base_url($module . '/' . $appLink . '/delete')?>/'+row[idField],function(result){
						if (result.success){
							$('#dg').datagrid('reload');	// reload the user data
                            $("#id_bl").combogrid({url:'<?=base_url($module . "/". $appLink."/getBL/4")?>'});
						} else {
							$.messager.show({	// show error message
								title: 'Error',
								msg:result.msg
							});
						}
					},'json');
				}
			});
		}
    }
    function save(){
	   $('#fm').form('submit',{
	    	url: url,
	    	onSubmit: function(){
	    		return $(this).form('validate');
	    	},
	    	success: function(result){
	    		var result = eval('('+result+')');
	    		if (result.success){

	    			$('#dlg').dialog('close');		
	    			$('#dg').datagrid('reload');
	                $("#id_bl").combogrid({url:'<?=base_url($module . "/". $appLink."/getBL/4")?>'});
	    		} else {
	             $.messager.alert('Error Message',result.msg,'error');
	    		}
	    	}
	    });
	}
	function doSearch(value){
	    $('#dg').datagrid('load',{    
	    	q:value  
	    });
	}
    function preview(){
   		var row = $('#dg').datagrid('getSelected');
    
	    if (row) {
	    	url  = '<?=base_url($module . '/' . $appLink . '/usulan')?>/'+row.id_bling;
	        window.open(url,'_blank');
	    }
    }
    function get_anggaran(idbl){
        $.get('<?=base_url($module . '/persetujuan/get_anggaran')?>/'+idbl,function(result){
            $('#anggaran').numberbox('setValue',result.sisa);
            $('#anggarantotal').numberbox('setValue',result.sisasemua);
            //$('#idanggaran').val(result.idanggaran);
            },'json');
    }

    function pengajuan(val){
        // alert(val);
        if (val=='1') {
            $('.uang').show();
            $('.barang').hide();
        }else{
            $('.barang').show();
            $('.uang').hide();
        }
    }

    function formatValidasi(value,row,index){
    	
    	// console.log(value);
    	// rowSelected = value;
    if(row.status== 4){
        disposisi = '<center> <a onclick="javascript:validasi('+index+')" title="Validasi" class="btn btn-small btn-default easyui-tooltip" style="color:red"><i class="icon-check icon-large"></i></a> </center>';
    	}else{
    		disposisi = '<center> <a  title="Sudah tervalidasi" class="btn btn-small btn-default easyui-tooltip" style="color:green"><i class="icon-check icon-large"></i></a> </center>';
    		
    	}
        return disposisi;

    }

    function validasi(idx){
		var row = $('#dg').datagrid('getRows')[idx];
		//var row = $('#dg').datagrid('checkRow', index);
		// row = rowSelected;
	 	//console.log(row);		
	    if (row){
			$.messager.confirm('Confirm','Apakah Anda Yakin Akan Memvalidasi data ini?',function(r){
				if (r){
					$.post('<?=base_url($module . '/' . $appLink . '/validasi')?>/'+row['id_bl'],function(result){
						if (result.success){
							$('#dg').datagrid('reload');	// reload the user data
						} else {
							$.messager.show({	// show error message
								title: 'Error',
								msg:result.msg
							});
						}
					},'json');
				}
			});
		}
	}
</script>
 
<div class="tabs-container">                
	<table id="dg" class="easyui-datagrid" 
	data-options="  
	    url: '<?=base_url($module . '/' . $appLink . '/read')?>',
		singleSelect:'true', 
	    title:'<?=$app?>',
	    toolbar:'#toolbar',
	    iconCls:'icon-cog',
	    rownumbers:'true',  
	    idField:'<?=$idField?>', 
	    pagination:'true',
        nowrap:false, 
	    pageList: [10,20,30]
	    "
	>
        <thead data-options="frozen:true">
            <tr>
                <th field="id_bl" width="100" sortable="true">ID</th>
                <th field="nama_bl" width="100" sortable="true" hidden>nama bl</th>
                <th field="instansi" width="160" sortable="true">Instansi Pemohon</th>
            </tr>
        </thead>
	    <thead>
            <tr>
	        <th field="tanggal_input" width="90" sortable="true">Tanggal Daftar</th>
            <th field="nama" width="140" sortable="true">Nama Pemohon</th>
            <th align="right" field="nilai" width="100" sortable="true" formatter="format_numberdisp">Nilai<br /> Pengajuan</th>
            <!-- <th field="nilai_rekomendasi" width="250" sortable="true">Nilai Rekomendasi</th> -->
            <th field="status" width="100" sortable="true" hidden>Status</th>
            <th field="tanggal_survey" width="80" sortable="true">Tanggal<br /> Survey</th>
            <th field="rekomendasi_penilaian" width="90" sortable="true">Rekomendasi</th>
            <th field="nilai_rekomendasi" width="90" sortable="true" formatter="format_numberdisp">Nilai <br /> Rekomendasi</th>
            <th field="keterangan_evaluator" width="80" sortable="true">Keterangan <br /> Evaluator</th>
            <th field="tanggal_evaluator" width="80" sortable="true">Tanggal <br /> Evaluator</th>
            <th field="action" width="100" formatter="formatValidasi" sortable="true">Action</th>
            </tr>
	        
	    </thead>
	</table>
	
	<!-- Model Start -->
	<div id="dlg" class="easyui-dialog" style="width:500px; height:500px; padding:10px" closed="true" buttons="#t_dlg_dis-buttons" >
	    <form id="fm" method="post" enctype="multipart/form-data" action="">
	        <table width="100%" align="center" border="0">
	            <tr id="namabl">
	                <td>Nama Bina Lingkungan</td>
	                <td>:</td>
	                <td>
                    <select name="id_bl" id="id_bl" class="easyui-combogrid" style="width:300px;" data-options="
                        required:true,
                        panelWidth:300,
                        value:'instansi',
                        idField:'id_bl',
                        textField:'instansi',
                        mode:'remote',
                        url:'<?=base_url($module . '/' . $appLink . '/getBL/4')?>',
                        columns:[[
                        {field:'id_bl',title:'ID',width:100},
                        {field:'tanggal_input',title:'Tanggal Daftar',width:100},
                        {field:'nama',title:'Nama Pemohon',width:100},
                        {field:'instansi',title:'Nama Instansi',width:100},
                        {field:'nilai',title:'Nilai Pengajuan',width:100},
                        {field:'kota',title:'Kota',width:100}
                        ]],
                        onSelect: function(no,row){
                            $('#nama').val(row.nama);
                            $('#no_ktp').val(row.no_ktp);
                            $('#alamat').val(row.alamat);
                            $('#nama_bl').val(row.nama_bl); 
                            $('#nilai').numberbox('setValue', row.nilai);
                            get_anggaran(row.id_bl);
                            $('#keterangan_hasil').val(row.keterangan_hasil); 
                            $('#rekomendasi_penilaian').val(row.rekomendasi_penilaian); 
                            $('#barang').val(row.barang);
                           if(row.jenis_pengajuan==1){
                            $('#jenis_pengajuan').val('Uang');
                            	}else{
                            	$('#jenis_pengajuan').val('Barang');
                            	}
                            if(row.jenis_pengajuan==1){
                            $('.uang1').show();
           					$('.barang1').hide();
                            }else{
                            $('.uang1').hide();
           					$('.barang1').show();
                            }

                            }
                        "></select>
	            </tr>
                <tr id="instansi">
	                <td>Nama Instansi</td>
	                <td>:</td>
	                <td>
	                    <input type="text" name="instansi" id="instansi" required style="width: 250px;" readonly="true">
	                </td>
	            </tr>
                <tr>
	                <td>Nama Pemohon</td>
	                <td>:</td>
	                <td>
	                    <input type="text" name="nama" id="nama" required style="width: 250px;" readonly="true">
	                </td>
	            </tr>
                <tr>
	                <td>Nomor KTP</td>
	                <td>:</td>
	                <td>
	                    <input type="text" name="no_ktp" id="no_ktp" required style="width: 250px;" readonly="true">
	                </td>
	            </tr>
                <tr>
	                <td>Alamat</td>
	                <td>:</td>
	                <td>
	                    <textarea id="alamat" name="alamat" style="width: 300px ;" class="easyui-textbox" required readonly="true"></textarea>
	                </td>
	            </tr>
	            <tr>
	                <td>Bidang Bantuan</td>
	                <td>:</td>
	                <td>
	                    <input id="nama_bl" name="nama_bl" style="width: 300px ;" readonly="true">
	                </td>
	            </tr>
	            <tr>
	            	<td>Jenis Pengajuan</td>
	            	<td>:</td>
	            	<td><input id="jenis_pengajuan" name="jenis_pengajuan" readonly></td>
	            </tr>
	            <tr class="barang1">
	            	<td>Deskrip Barang</td>
	            	<td>:</td>
	            	<td><input  name="barang" id="barang" readonly="true"></td>
	            </tr>
                <tr class="uang1" style="display: none;">
	                <td>Nilai Pengajuan</td>
	                <td>:</td>
	                <td>
                        <input class="easyui-numberbox" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%'" name="nilai" id="nilai"  style="width: 250px;" readonly="true">
	                </td>
	            </tr>
                <tr>
		                <td>Sisa Anggaran Bidang</td>
		                <td>:</td>
		                <td>
		                    <input class="easyui-numberbox" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%'" name="anggaran" id="anggaran" readonly style="width: 250px;">
		                </td>
		            </tr>
                    <tr>
		                <td>Sisa Anggaran Keseluruhan</td>
		                <td>:</td>
		                <td>
		                    <input class="easyui-numberbox" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%'" name="anggarantotal" id="anggarantotal" readonly style="width: 250px;">
		                </td>
		            </tr>
                <!--tr>
	                <td>Tanggal Survey</td>
	                <td>:</td>
	                <td>
	                    <input id="tanggal_survey" name="tanggal_survey"  class="easyui-datebox" style="width:100px;" readonly="true"/></td>
	                </td>
	            </tr-->
                
                <tr>
	                <td>Keterangan Hasil Survey</td>
	                <td>:</td>
	                <td>
	                    <textarea id="keterangan_hasil" name="keterangan_hasil" style="width: 300px ;" readonly="true"></textarea>
	                </td>
	            </tr>
                <tr>
	                <td>Penilaian Survey</td>
	                <td>:</td>
	                <td>
	                    <input id="rekomendasi_penilaian" name="rekomendasi_penilaian" style="width: 300px ;" readonly="true">
	                </td>
	            </tr>
	            <!-- sumiyati 03-08-17 -->
	            <tr>
                            <td>Nilai Akhir</td>
                            <td>:</td>
                            <td>
                            <input class="easyui-combobox"  style="width: 100px;" name="nilai_akhir" id="nilai_akhir" data-options="
                            	required:'true',
                                valueField: 'value',
                                textField: 'label',
                                data: [{
                                    label: 'Uang',
                                    value: '1'
                                },{
                                    label: 'Barang',
                                    value: '0'
                                }],
                                onSelect:function(param){
                                
                                pengajuan(param.value);

                                }
                                " />
                            </td>
                        </tr>
                        <tr class="barang" style="display: none;">
                            <td>Deskripsi Barang</td>
                            <td>:</td>
                            <td><input class="easyui-textbox" name="nilai_rekomendasi_barang" id="nilai_rekomendasi_barang"></td>
                        </tr>
	            <!--  -->
                <tr class="uang" style="display: none;">
	                <td>Nilai Rekomendasi</td>
	                <td>:</td>
	                <td>
                        <input class="easyui-numberbox" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%'" name="nilai_rekomendasi" id="nilai_rekomendasi" required style="width: 250px;">
	                </td>
	            </tr>
                <!--tr>
	                <td>Penilaian Survey</td>
	                <td>:</td>
	                <td>
	                    <select  class="easyui-combobox" type="text" name="rekomendasi_penilaian" id="rekomendasi_penilaian" required style="width: 250px;">
	                    	<option value="Layak">Layak</option>
	                    	<option value="Tidak Layak">Tidak Layak</option>
	                    </select>
	                </td>
	            </tr-->
                <tr>
	                <td>Keterangan Evaluator</td>
	                <td>:</td>
	                <td>
	                    <textarea id="keterangan_evaluator" name="keterangan_evaluator" style="width: 300px ;" required></textarea>
	                </td>
	            </tr>
	        </table>
	    </form>
	   
	   	<!-- Tombol Add, Edit, Hapus Datagrid -->
       	<div id="toolbar">  
            <table align="center" style="padding: 0px; width: 99%;">
                <tr>
                    <td>
                        <a href="javascript:void(0)" class="btn btn-small btn-success" onclick="javascript:add()"><i class="icon-plus-sign icon-large"></i>&nbsp;Add</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:edit()"><i class="icon-edit"></i>&nbsp;Edit</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-danger" onclick="javascript:hapus()"><i class="icon-trash icon-large"></i>&nbsp;Delete</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-warning" iconCls: 'icon-search' onclick="javascript:preview()"><i class="icon-download-alt"></i>&nbsp;Preview</a>
                    </td>
                    <td>&nbsp;</td>
                    <td align="right">
                    	<input class="easyui-searchbox" prompt="Silahkan Input Field" searcher="doSearch"  style="width:300px"/>
                    </td>
                </tr>
            </table>  
        </div>  
        <!-- end tombol datagrid -->
        <!-- Tombol simpan & Batal Form -->
        <div id="t_dlg_dis-buttons">
            <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:save()"><i class="icon-save icon-large"></i>&nbsp;Simpan</a>
            <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:$('#dlg').dialog('close')"><i class="icon-remove icon-large"></i>&nbsp;Batal</a>      
        </div>
	</div>
	<!-- Model end -->
</div>