<?php
    date_default_timezone_set("Asia/Jakarta");
    $tanggal = date('d F Y');
    $tgl=$pencairan['tanggal_cd'];
    // $kec=$pencairan['kecamatan'];
    // $kel=$pencairan['kelurahan'];
    // $kot=$pencairan['kota'];
    // $kelurahan=ucwords(strtolower($kel));
    // $kecamatan = ucwords(strtolower($kec));
    // $kota=ucwords(strtolower($kot));
    
    // $kredit=$pencairan['kredit'];
    // $hari=date('d',strtotime($tgl));
    // $hari=date('d',strtotime($tanggal));
    $hari=date('d',strtotime($tgl));
    // $bulan=date('m',strtotime($tgl));
    $bulan=date('m',strtotime($tgl));
    // echo $bulan;
    // $year=date('Y',strtotime($tgl));
    $year=date('Y',strtotime($tgl));
    $day = date('D', strtotime($tgl));
    $dayList = array(
        'Sun' => 'Minggu',
        'Mon' => 'Senin',
        'Tue' => 'Selasa',
        'Wed' => 'Rabu',
        'Thu' => 'Kamis',
        'Fri' => 'Jumat',
        'Sat' => 'Sabtu'
    );
    $bulanList = array('01' => 'Januari', '02' => 'Februari', '03' => 'Maret', '04' => 'April', '05' => 'Mei', '06' => 'Juni', '07' => 'Juli', '08' => 'Augustus', '09' => 'September', '10' => 'October.', '11' => 'Nopember', '12' => 'Desember');
   

function kekata($x) {
        $x = abs($x);
        $angka = array("", "Satu", "Dua", "Tiga", "Empat", "Lima",
        "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas");
        $temp = "";
        if ($x <12) {
            $temp = " ". $angka[$x];
        } else if ($x <20) {
            $temp = kekata($x - 10). " Belas";
        } else if ($x <100) {
            $temp = kekata($x/10)." Puluh". kekata($x % 10);
        } else if ($x <200) {
            $temp = " seratus" . kekata($x - 100);
        } else if ($x <1000) {
            $temp = kekata($x/100) . " Ratus" . kekata($x % 100);
        } else if ($x <2000) {
            $temp = " seribu" . kekata($x - 1000);
        } else if ($x <1000000) {
            $temp = kekata($x/1000) . " Ribu" . kekata($x % 1000);
        } else if ($x <1000000000) {
            $temp = kekata($x/1000000) . " Juta" . kekata($x % 1000000);
        } else if ($x <1000000000000) {
            $temp = kekata($x/1000000000) . " Milyar" . kekata(fmod($x,1000000000));
        } else if ($x <1000000000000000) {
            $temp = kekata($x/1000000000000) . " Trilyun" . kekata(fmod($x,1000000000000));
        }     
            return $temp;
    }
    function terbilang($x, $style=4) {
        if($x<0) {
            $hasil = "minus ". trim(kekata($x));
        } else {
            $hasil = trim(kekata($x));
        }     
        switch ($style) {
            case 1:
                $hasil = strtoupper($hasil);
                break;
            case 2:
                $hasil = strtolower($hasil);
                break;
            case 3:
                $hasil = ucwords($hasil);
                break;
            default:
                $hasil = ucfirst($hasil);
                break;
        }     
        return $hasil;
    }
    $terbilang = number_format($pencairan['nilai_rekomendasi']);
    // $harga_penawaran_vendor = number_format($pencairan['harga_penawaran_vendor']);
?>
<!DOCTYPE html>
<html>
<head>
	<title>Pecairan Dana</title>
	<style>

	
		div, .hr{

		    background-color: white;
            width: 100%;
            /*height: 50%;*/
            
            padding: 25px;
            padding-left: 30px;
            margin: 0px;
            font-size: 12;
            padding-bottom:2px;
            padding-top: 0px;
            /*letter-spacing: 4px;*/
            line-height: 90%;
		}
		hr{
		    background-color: white;
		    width: 100%;
		    /*border: 1px solid black;*/
		    padding: 0px;
		    margin: 0px;
		}
		h3, .title{
			text-align: center;
		}	
		table{
		    width: 100%;
		    /*border: 1px solid black;*/
		    border-collapse: collapse;
		    padding: 5px;
		    padding-bottom: 5px;
		}
        /*td{
            height: 5px;
        }*/
		table,td,tr{
		    /*border: 1px solid black;*/
		    border-collapse: collapse;
		    padding: 2px;
			padding-bottom:2px;

		}	
					
	</style>
</head>
<body>
<div style="padding-top: 1px;padding-bottom: 0px;padding-left: 5px;padding-right: 5px;border:3px solid black;border-style: double;">
<table  style="padding-top: 0px;padding-right: 5px;" width="90%" height="90%">

 
		
		<thead>
		<!-- <tr>
			<td rowspan="7" style="">DIVISI COMMUNITY DEVELOPMENT<br>PT KRAKATAU STEEL (PERSERO) Tbk.<br>Jl.Industri N0.3 Cilegon<br>Telp (0254) 372326, 372298, 372280</td>
		</tr> -->
        <tr>
            <td colspan="3"><center><img src='<?=base_url('assets/images/headkti.jpg')?>' style="width:30%; height:30%;padding-top: 0px;padding-bottom:0px;"></center></td></td>
        </tr>
        <tr >
            <td colspan="3" style="text-align: bottom center;padding-top: 1px;"><b>No.<u><?php echo $pencairan['id_bl'];?></u></b></td>
            

        </tr>
        <tr style="height: 5px;">
            <td style="width: 150px;height: 5px;">Telah Diterima Dari </td>
            <td colspan="2" style="border-bottom: 1px solid black;text-align: left;">:<b>&nbsp;PT KRAKATAU STEEL (PERSERO)Tbk.</b></td>
        </tr>
        <tr style="height: 5px;">
            <td style="height: 5px;">Uang Sejumlah</td>
            <td colspan="2" style=" border-bottom: 1px solid black;text-align: left;">: &nbsp;<b><i><?php echo terbilang($pencairan['nilai_rekomendasi']);?> Rupiah</i></b></td>
        </tr>
        <tr >
            <td style="height: 5px;">Untuk Pembayaran</td>
            <td colspan="2" style="border-bottom:  1px solid black;text-align: left;">:&nbsp;<?php echo $pencairan['keterangan'];?></td>
        </tr>
        <tr >
            <td style="border-bottom: 1px solid black; height: 5px;"></td>
            <td style="width: 20px;">Periode :</td>
            <td style="border-bottom:  1px solid black;"></td>

        </tr>
        <tr style="height: 5px;">
            <td colspan="3" style="border-bottom:  1px solid black;">&nbsp;</td>
            

        </tr>
        <tr style="height: 5px;">
            <td colspan="2" style="border-right:  2px solid black;border-left:   2px solid black;border-top:  2px solid black;border-bottom:  2px solid black;text-align: center;height: 2px;"><b><?php echo $pencairan['instansi'];?></td>
            <td></td>
            
        </tr>
        <tr style="height: 5px;">
            <td colspan="2" style="border-right:  2px solid black;border-left:   2px solid black;border-top:  2px solid black;border-bottom:  2px solid black;text-align: center;height: 2px;"><b><?php echo $pencairan['kota'];?></b></td>
            <td style="text-align: center;"><b>Cilegon, <?php echo $hari;?> <?php echo $bulanList[$bulan];?> <?php echo $year;?> </b></td>
           
            
        </tr>
        <tr >
            <td colspan="3"></td>
        </tr>
        <tr style="height: 5px;">
            <td colspan="3"></td>
        </tr>
        <tr style="height: 5px;">
            <td colspan="3"></td>
        </tr>
        <tr style="height: 5px;">
            <td  style="border-left:   1px solid black;border-top: 1px;border-bottom: 1px solid black;border-right: 1px solid black;border-top: 1px solid black;height: 5px;height: 2px;">Rp. &nbsp;&nbsp;<?php echo number_format($pencairan['nilai_rekomendasi']);?></td>
            <td colspan="2"></td>
        </tr>
        <tr>
            <td colspan="3"></td>
        </tr>
        <tr>
            <td colspan="3"></td>
        </tr>
        <tr>
            <td colspan="2"></td>

            <td style="text-align: center;"><u >Penerima</u></td>
        </tr>
		
		</thead>
		
		</table>
	</div>
	</body>
</html>