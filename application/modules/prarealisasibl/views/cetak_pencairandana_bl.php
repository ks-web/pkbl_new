<?php
    date_default_timezone_set("Asia/Jakarta");
    $tanggal = date('d F Y');
    $kec=$pencairan['kecamatan'];
    $kel=$pencairan['kelurahan'];
    $kot=$pencairan['kota'];
    $kelurahan=ucwords(strtolower($kel));
    $kecamatan = ucwords(strtolower($kec));
    $kota=ucwords(strtolower($kot));
    
    // $kredit=$pencairan['kredit'];
    // $hari=date('d',strtotime($tgl));
    $hari=date('d',strtotime($tanggal));
    // $bulan=date('m',strtotime($tgl));
    $bulan=date('m',strtotime($tanggal));
    // echo $bulan;
    // $year=date('Y',strtotime($tgl));
    $year=date('Y',strtotime($tanggal));
    $day = date('D', strtotime($tanggal));
    $dayList = array(
        'Sun' => 'Minggu',
        'Mon' => 'Senin',
        'Tue' => 'Selasa',
        'Wed' => 'Rabu',
        'Thu' => 'Kamis',
        'Fri' => 'Jumat',
        'Sat' => 'Sabtu'
    );
    $bulanList = array('01' => 'Januari', '02' => 'Februari', '03' => 'Maret', '04' => 'April', '05' => 'Mei', '06' => 'Juni', '07' => 'Juli', '08' => 'Augustus', '09' => 'September', '10' => 'October.', '11' => 'Nopember', '12' => 'Desember');
   

function kekata($x) {
        $x = abs($x);
        $angka = array("", "Satu", "Dua", "Tiga", "Empat", "Lima",
        "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas");
        $temp = "";
        if ($x <12) {
            $temp = " ". $angka[$x];
        } else if ($x <20) {
            $temp = kekata($x - 10). " Belas";
        } else if ($x <100) {
            $temp = kekata($x/10)." Puluh". kekata($x % 10);
        } else if ($x <200) {
            $temp = " seratus" . kekata($x - 100);
        } else if ($x <1000) {
            $temp = kekata($x/100) . " Ratus" . kekata($x % 100);
        } else if ($x <2000) {
            $temp = " seribu" . kekata($x - 1000);
        } else if ($x <1000000) {
            $temp = kekata($x/1000) . " Ribu" . kekata($x % 1000);
        } else if ($x <1000000000) {
            $temp = kekata($x/1000000) . " Juta" . kekata($x % 1000000);
        } else if ($x <1000000000000) {
            $temp = kekata($x/1000000000) . " Milyar" . kekata(fmod($x,1000000000));
        } else if ($x <1000000000000000) {
            $temp = kekata($x/1000000000000) . " Trilyun" . kekata(fmod($x,1000000000000));
        }     
            return $temp;
    }
    function terbilang($x, $style=4) {
        if($x<0) {
            $hasil = "minus ". trim(kekata($x));
        } else {
            $hasil = trim(kekata($x));
        }     
        switch ($style) {
            case 1:
                $hasil = strtoupper($hasil);
                break;
            case 2:
                $hasil = strtolower($hasil);
                break;
            case 3:
                $hasil = ucwords($hasil);
                break;
            default:
                $hasil = ucfirst($hasil);
                break;
        }     
        return $hasil;
    }
    $terbilang = number_format($pencairan['nilai_rekomendasi']);
    // $harga_penawaran_vendor = number_format($pencairan['harga_penawaran_vendor']);
?>
<!DOCTYPE html>
<html>
<head>
	<title>Pecairan Dana</title>
	<style>

	
		div, .hr{

		    background-color: white;
		    width: 100%;
		    
		    padding: 25px;
		    padding-left: 30px;
		    margin: 0px;
		    font-size: 12;
			padding-bottom:2px;
			padding-top: 0px;

		}
		hr{
		    background-color: white;
		    width: 100%;
		    /*border: 0px solid black;*/
		    padding: 0px;
		    margin: 0px;
		}
		h3, .title{
			text-align: center;
		}	
		table{
		    width: 100%;
		    /*border: 0px; solid black;*/
		    border-collapse: collapse;
		    padding: 5px;
		    /*padding-bottom: 5px;*/
		}
		table,td,tr{
		    /*border: 0px; solid black;*/
		    border-collapse: collapse;
		    padding: 2px;
			/*padding-bottom:2px;*/

		}	
					
	</style>
</head>
<body>
<div style="padding-top: 5px;padding-bottom: 0px;">
<table  style="padding-top: 0px;" width="100%" height="100%">
<thead>
	<tr>
		<td colspan="7" style="padding: 10px 15px 0px 20px;">
			<center><img src='<?=base_url('assets/images/headkti.png')?>' style="width:100%; height:100%;"></center>
		</td>
	</tr>
	<br><br>
	<tr>
		<td colspan="7" >
			&nbsp;
		</td>
	</tr>
	<tr>
		<td colspan="7" style="padding: 10px 15px 0px 20px;text-align: center;" ><CENTER><b>BERITA ACARA SERAH TERIMA BANTUAN BARANG / DANA<br><font style="text-decoration: underline;text-align: center;">DARI PT KRAKATAU STEEL (PERSERO) Tbk.</font></b><br>Nomor : <?php echo $pencairan['id_bl'];?>
			&nbsp;</CENTER>
		</td>
	</tr>
	</thead>
 <tr><br><br><br><br>
		<td></td>
		<br><br><br><br><br><br><td colspan="6" style="text-align: right;"><!-- Cilegon, <?php echo $hari;?> &nbsp;<?php echo $bulanList[$bulan];?>&nbsp;<?php echo $year;?> --></td>
	</tr>
	<tr>
		<td colspan="7" style="padding: 10px 15px 0px 30px;text-align: justify;"><p >Pada Hari ini &nbsp;&nbsp;<?php echo $dayList[$day];?>&nbsp;&nbsp; Tanggal&nbsp;&nbsp; <?php echo $hari;?>&nbsp;&nbsp;, bulan&nbsp;&nbsp; <?php echo $bulanList[$bulan];?>&nbsp;&nbsp; tahun &nbsp;&nbsp;<?php echo terbilang($year);?> (<?php echo $hari;?> -&nbsp;<?php echo $bulan;?> -&nbsp;<?php echo $year;?>) dicilegon, telah dilaksanakan serah terima bantuan barang/dana dari Divisi Community Development PT Krakatau Steel (Persero) Tbk.:</p></td>
		
	</tr>
		<tr>
			<td  style="padding-left: 35px;">Kepada</td>
			<td>:</td>
			<td colspan="5" style="border-bottom:  1px solid black;"><?php echo $pencairan['nama'];?> </td>
		</tr>
		<tr>
			<td  style="padding-left: 35px;"></td>
			<td style="width: 5px;">:</td>
			<td colspan="5" style="border-bottom:  1px solid black;"><?php echo $pencairan['instansi'];?></td>
		</tr>
		<tr>
			<td  style="padding-left: 35px;">Alamat</td>
			<td>:</td>
			<td colspan="5" style="border-bottom:  1px solid black;"><?php echo $pencairan['alamat']; ?>,Kel.<?php echo $kelurahan;?>, Kec.<?php echo $kecamatan;?>, <?php echo $kota;?></td>
		</tr>
		<tr>
			<td  style="padding-left: 35px;">Berupa</td>
			<td>:</td>
			<td colspan="5" style="border-bottom:  1px solid black;" ><?php echo $pencairan['keterangan'];?></td>
		</tr>
	
		<tr style="padding:1px;">
			<td  style="padding-left: 35px;">Sebanyak</td>
			<td>:</td>
			<td colspan="5" style="border-bottom:  1px solid black;">Rp <?php echo number_format($pencairan['nilai_rekomendasi']);?></td>
		</tr>
		<tr>
			<td  style="padding-left: 35px;"></td>
			<td>:</td>
			<td colspan="5"> &nbsp;&nbsp;<i><?php echo ucwords(Terbilang($pencairan['nilai_rekomendasi']));?> Rupiah</i></td>
		</tr>
		
		
		
		<tr>
			<td colspan="7" style="padding-right: 15px;padding-left: 20px;" ><p>Demikian Berita Acara ini dibuat untuk dipergunakan sebagaimana mestinya.</p><br></td>
		</tr>
		
		<thead>
		
		<tr>
				
				<td colspan="3" style="font-family: font-size:14px;padding:5px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;vertical-align:top;text-align:left;">
					<center>
					<br>Yanng Menerima<br><br><br><br></center>
					<center></center>
					<center><font style="text-decoration: underline;"><?php echo $pencairan['nama'];?></font><br>
					</center>
				</td>
				
				
				<td style="font-family:font-size:14px;padding:5px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;vertical-align:top;text-align:right;"></td>
				<td colspan="3" style="font-family:font-size:14px;padding:5px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;vertical-align:top;text-align:right;">
					<center>
					</strong><br>Yang Menyerahkan<br><br><br><br><font style="text-decoration: underline;" >Aris Santoso</font><br>		</center>
				</td>
			</tr>
			</thead> 
			<thead>
				<tr>
					<td colspan="7" style="font-family: font-size:14px;">
					<center>
					<br>Mengetahui,<br>Divisi Community Development<br><br><br><br><br></center>
					<center></center>
					<center><font style="text-decoration: underline;">Syarif Rahman</font><br>Manager
					</center>
				</td>
				</tr>
			</thead>
		
		</table>
	</div>
	</body>
</html>