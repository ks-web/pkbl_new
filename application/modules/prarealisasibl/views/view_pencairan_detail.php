<?php
$app1 = 'Akun Pencairan Dana'; // nama aplikasi
$module1 = 'prarealisasibl';
$appLink1 = 'pencairan_detail'; // controller
$idField1  = 'id_akun'; //field key table
?>

<script>
	var url1;
	var app1 = "<?=$app1?>";
	var appLink1 = '<?=$appLink1?>';
	var module1 = '<?=$module1?>';
	var idField1 = '<?=$idField1?>';
	var flag;
	
    function add1(){
        $('#dlg1').dialog('open').dialog('setTitle','Tambah ' + app1);
		$('#fm1').form('clear');
	    $('#tbl1').html('Save');
		url1 = '<?=base_url($module1 . '/' . $appLink1 . '/create')?>';
		$('#idParent').val(idParent);
		flag = true;
    }
    function edit1(){
        var row = $('#dg1').datagrid('getSelected');
		if (row){
			flag = false;
			$('#tbl1').html('Simpan');
			$('#dlg1').dialog('open').dialog('setTitle','Edit '+ app1);
			$('#fm1').form('load',row);
			url1 = '<?=base_url($module1 . '/' . $appLink1 . '/update')?>/'+row[idField1];
			$('#idParent').val(idParent);
			flag = true;
	    }
    }
    function hapus1(){
        var row = $('#dg1').datagrid('getSelected');		
	    if (row){
			$.messager.confirm('Confirm','Apakah Anda Yakin Akan Menghapus ini?',function(r){
				if (r){
					$.post('<?=base_url($module1 . '/' . $appLink1 . '/delete')?>/'+row[idField1] + '/' + idParent,function(result){
						if (result.success){
							$('#dg1').datagrid('reload');	// reload the user data
						} else {
							$.messager.show({	// show error message
								title: 'Error',
								msg:result.msg
							});
						}
					},'json');
				}
			});
		}
    }
    function save1(){
	   $('#fm1').form('submit',{
	    	url: url1,
	    	onSubmit: function(){
	    		return $(this).form('validate');
	    	},
	    	success: function(result){
	    		var result = eval('('+result+')');
	    		if (result.success){
	    			$('#dlg1').dialog('close');		
	    			$('#dg1').datagrid('reload');
	                
	    		} else {
	             $.messager.alert('Error Message',result.msg,'error');
	    		}
	    	}
	    });
	}
	function posting1(){
    	$.messager.confirm('Confirm','Apakah Anda Yakin Akan Melakukan Posting?',function(r){
			if (r){
				$.post('<?=base_url($module1 . '/' . $appLink1 . '/posting')?>/'+idParent,function(result){
					if (result.success){
						$('.btnOptions1').hide();
						$('.btnOptions2').show();
						$('#dg').datagrid('reload');	// reload the user data
						//$('#dg1').datagrid('reload');
					} else {
						$.messager.show({	// show error message
							title: 'Error',
							msg:result.msg
						});
					}
				},'json');
			}
		});
    }
	function doSearch1(value){
	    $('#dg1').datagrid('load',{    
	    	q:value  
	    });
	}
	/*function dg1(){
		$('#dg1').datagrid({url: '<?=base_url($module1 . '/' . $appLink1 . '/read')?>/' + idParent})
	}*/

	function reload1(){
		$.post( '<?=base_url($module1 . '/' . $appLink1 . '/getStatusPosting/')?>/'+idParent )
  		.done(function( data ) {
  			console.log(data[0]['status_posted']);
    		if(data[0]['status_posted'] == 1){
				$('.btnOptions1').hide();
				$('.btnOptions2').show();
			} else {
				$('.btnOptions1').show();
				$('.btnOptions2').hide();
			}
    		$('#dg1').datagrid({    
		    	url: '<?=base_url($module1 . '/' . $appLink1 . '/read/')?>/'+idParent  
		    });
  		});
	}
</script>
 
<div class="tabs-container">                
	<table id="dg1" class="easyui-datagrid" 
	data-options="
		singleSelect:'true', 
	    title:'<?=$app1?>',
	    toolbar:'#toolbar1',
	    iconCls:'icon-cog',
	    rownumbers:'true',  
	    idField:'<?=$idField1?>', 
	    pagination:'true',
	    fitColumns:'true',
	    pageList: [10,20,30]
	    "
	>
	    <thead>
	        <th field="account_no" width="100">No Akun</th>
	        <th field="uraian" width="200">Nama Akun</th>
            <th field="debet" width="120" formatter="format_numberdisp" align="right">Debet</th>
            <th field="kredit" width="120" formatter="format_numberdisp" align="right">Kredit</th>
	    </thead>
	</table>
	
	<!-- Model Start -->
	<div id="dlg1" class="easyui-dialog" style="width:400px; height:auto; padding:10px" closed="true" buttons="#t_dlg_dis-buttons1" >
	    <form id="fm1" method="post" enctype="multipart/form-data" action="">
	        <table width="100%" align="center" border="0">
	            <tr>
	                <td>Account No</td>
	                <td>:</td>
	                <td>
	                    <select id="account_no" name="account_no" class="easyui-combogrid" style="width:100px;"
                        data-options="
                        url:'<?=base_url($module1 . "/" . $appLink1 . "/getakun")?>',
                        panelWidth:400,
                        idField:'kode_account',
                        textField:'kode_account',
                        mode:'remote',
                        columns:[[
                        {field:'kode_account',title:'Nomor Akun',width:100},
                        {field:'uraian',title:'Uraian',width:230}
                        ]],
                        onSelect: function(no,row){
                        	$('#uraian').val(row.uraian);
                        }
                        "></select>
	                    <input type="hidden" id="idParent" name="id_cd_um" />
	                    <input type="hidden" name="status" value="BL" />
	                </td>
	            </tr>
	            <tr>
	                <td>Nama Akun</td>
	                <td>:</td>
	                <td>
	                    <input type="text" name="uraian" id="uraian" required style="width: 200px;">
	                </td>
	            </tr>
	            <tr>
	                <td>Debet</td>
	                <td>:</td>
	                <td>
	                    <input class="easyui-numberbox" data-options="onChange: function(){if(flag){flag = false; $('#kredit').numberbox('setValue', 0); flag = true; }},precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%'" name="debet" id="debet" required style="width: 120px;">
	                </td>
	            </tr>
	            <tr>
	                <td>Kredit</td>
	                <td>:</td>
	                <td>
	                    <input class="easyui-numberbox" data-options="onChange: function(){if(flag){flag = false; $('#debet').numberbox('setValue', 0); flag = true;}},precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%'" name="kredit" id="kredit" required style="width: 120px;">
	                </td>
	            </tr>
	        </table>
	    </form>
	   
	   	<!-- Tombol Add, Edit, Hapus Datagrid -->
       	<div id="toolbar1">  
            <table align="center" style="padding: 0px; width: 99%;">
                <tr>
                    <td>
                        <a href="javascript:void(0)" class="btn btn-small btn-success btnOptions1" onclick="javascript:add1()"><i class="icon-plus-sign icon-large"></i>&nbsp;Add</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-primary btnOptions1" onclick="javascript:edit1()"><i class="icon-edit"></i>&nbsp;Edit</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-danger btnOptions1" onclick="javascript:hapus1()"><i class="icon-trash icon-large"></i>&nbsp;Delete</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-primary btnOptions1" onclick="javascript:posting1()"><i class="icon-check icon-large"></i>&nbsp;Posting</span></a>
                        <a href="javascript:void(0)" class="btn btn-small btn-success btnOptions2"><i class="icon-check icon-large" data-options="disabled:true"></i>&nbsp;Posted</span></a>
                    </td>
                    <td>&nbsp;</td>
                    <td align="right">
                    	<input class="easyui-searchbox" prompt="Silahkan Input Field" searcher="doSearch1"  style="width:300px"/>
                    </td>
                </tr>
            </table>  
        </div>  
        <!-- end tombol datagrid -->
        <!-- Tombol simpan & Batal Form -->
        <div id="t_dlg_dis-buttons1">
            <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:save1()"><i class="icon-save icon-large"></i>&nbsp;Simpan</a>
            <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:$('#dlg1').dialog('close')"><i class="icon-remove icon-large"></i>&nbsp;Batal</a>      
        </div>
	</div>
	<!-- Model end -->
</div>