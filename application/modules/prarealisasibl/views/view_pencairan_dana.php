<?php
$app = 'Pencairan Dana'; // nama aplikasi
$module = 'prarealisasibl';
$appLink = 'pencairan_dana'; // controller
$idField  = 'id_um'; //field key table
?>

<script>
	var url;
	var app = "<?=$app?>";
	var appLink = '<?=$appLink?>';
	var module = '<?=$module?>';
	var idField = '<?=$idField?>';
	var idParent = '';
	
    function add(){
        $('#dlg').dialog('open').dialog('setTitle','Tambah ' + app);
		$('#fm').form('clear');
	    $('#tbl').html('Save');
		url = '<?=base_url($module . '/' . $appLink . '/create')?>';
		$("#akun").hide();
    }
    function edit(){
        var row = $('#dg').datagrid('getSelected');
		if (row){
			$('#tbl').html('Simpan');
			$('#dlg').dialog('open').dialog('setTitle','Edit '+ app);
			$('#fm').form('load',row);
			url = '<?=base_url($module . '/' . $appLink . '/update')?>/'+row[idField];
			idParent = row.id_um;
			$("#akun").show();
			reload1();
	    }
    }
    function hapus(){
        var row = $('#dg').datagrid('getSelected');		
	    if (row){
			$.messager.confirm('Confirm','Apakah Anda Yakin Akan Menghapus ini?',function(r){
				if (r){
					$.post('<?=base_url($module . '/' . $appLink . '/delete')?>/'+row[idField],function(result){
						if (result.success){
							$('#dg').datagrid('reload');	// reload the user data
						} else {
							$.messager.show({	// show error message
								title: 'Error',
								msg:result.msg
							});
						}
					},'json');
				}
			});
		}
    }
    function preview(){
    var row = $('#dg').datagrid('getSelected');
    url  = '<?=base_url($module . '/' . $appLink . '/permohonan')?>/'+row[idField];
    if (row)
    {
        window.open(url,'_blank');
    }
}
    function save(){
	   $('#fm').form('submit',{
	    	url: url,
	    	onSubmit: function(){
	    		return $(this).form('validate');
	    	},
	    	success: function(result){
	    		var result = eval('('+result+')');
	    		if (result.success){
	    			//$('#dlg').dialog('close');		
	    			$('#dg').datagrid('reload');
	    			console.log(result);
	                idParent = result.id;
					$("#akun").show();
					reload1();
	    		} else {
	             $.messager.alert('Error Message',result.msg,'error');
	    		}
	    	}
	    });
	}
	function doSearch(value){
	    $('#dg').datagrid('load',{    
	    	q:value  
	    });
	}
	function hitungAngsuran(){
		var bunga = $('#bunga').numberbox('getValue');
		var tahun = $('#tahun').numberbox('getValue');
		var nilai_pengajuan = $('#nilai_pengajuan').numberbox('getValue');

		if(bunga != '' && tahun != '' && nilai_pengajuan != ''){
			tahun = tahun*12;

			var total_bunga = nilai_pengajuan * (bunga/100);
			var angsuran_bunga = total_bunga/tahun;
			var angsuran_pokok = nilai_pengajuan/tahun;

			$('#total_bunga').numberbox('setValue', total_bunga);
			$('#angsuran_bunga').numberbox('setValue', angsuran_bunga);
			$('#angsuran_pokok').numberbox('setValue', angsuran_pokok);
			$('#total_pinjaman_bunga').numberbox('setValue', (parseInt(nilai_pengajuan)+parseInt(total_bunga)));
			$('#angsuran_bulanan').numberbox('setValue', (angsuran_pokok+angsuran_bunga));
		}
	}
</script>
 
<div class="tabs-container">                
	<table id="dg" class="easyui-datagrid" 
	data-options="  
	    url: '<?=base_url($module . '/' . $appLink . '/read')?>',
		singleSelect:'true', 
	    title:'<?=$app?>',
	    toolbar:'#toolbar',
	    iconCls:'icon-cog',
	    rownumbers:'true',  
	    idField:'<?=$idField?>', 
	    pagination:'true',
	    fitColumns:'true',
	    pageList: [10,20,30]
	    "
	>
	    <thead>
	        <th field="id_um" width="100" sortable="true">ID UM</th>
	        <th field="tanggal_uangmuka" width="200" sortable="true">Tanggal</th>
            <th field="keperluan" width="200" sortable="true">Keperluan</th>
            <th field="nilai" width="200" sortable="true">Nilai</th>
            <th field="jwaktu1" width="200" sortable="true">Jangka Waktu</th>
            <th field="jwaktu2" width="200" sortable="true">Sampai Dengan</th>
            <th field="keterangan" width="200" sortable="true">Keterangan</th>
            <th field="nama_pegawai" width="200" sortable="true">Pembuat</th>
	    </thead>
	</table>
	
	<div id="dlg" class="easyui-dialog" style="width:800px; height:440px; padding:10px" closed="true" buttons="#t_dlg_dis-buttons" >
		<table width="100%" align="center" border="0">
			<tr>
				<td width="50%">
					<form id="fm" method="post" enctype="multipart/form-data" action="">						
						<table width="100%" align="center" border="0">
				          	<tr>
				                <td colspan="3"><h4>Data Uang Muka</h4></td>
				            </tr>
				            <tr>
				                <td >ID BL</td>
				                <td>:</td>
				                <td>
									<input name="id_bl" id="id_bl"  class="easyui-combogrid" style="width: 203px"
									data-options="panelHeight:200, panelWidth: 500,
									valueField:'',
									idField:'id_bl',
									textField:'nama',
									mode:'remote',
									url:'<?=base_url($module . '/' . $appLink . '/getmitra') ?>',
									columns:[[
			                       		{field:'id_bl',title:'ID BL',width:80},
			                        	{field:'nama',title:'Nama Pemohon',width:150},
			                        	{field:'instansi',title:'Instansi',width:100},
			                        	{field:'kota',title:'Kota',width:200}
			                        ]]

									" >
				                </td>
				            </tr>
				            <tr>
				                <td >Tanggal UM</td>
				                <td>:</td>
				                <td>
									<input class="easyui-datebox" label="Start Date:" labelPosition="top" name="tanggal_uangmuka" id="tanggal_uangmuka" required style="width: 120px;">
				                </td>
				            </tr>
				            <tr>
				                <td >Keperluan</td>
				                <td>:</td>
				                <td>
				                    <textarea id="keperluan" name="keperluan" class="easyui-textbox"  style="width:200px" required>
				                    	
				                    </textarea>
				                </td>
								
				            </tr>
							<tr>
				                <td >Nilai</td>
				                <td>:</td>
				                <td>
									<input class="easyui-numberbox" data-options="label:'Rp.',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:' Rp.',width:'100%'" name="nilai" id="nilai" required style="width: 200px;">
				                </td>
								
				            </tr>
				            <tr>
				                <td>Jangka Waktu</td>
				                <td>:</td>
				                <td>
				                    <input class="easyui-datebox" label="Start Date:" labelPosition="top" name="jwaktu1" id="jwaktu1" required style="width: 120px;">
				                </td>
								
				            </tr>
							<tr>
				                <td>Sampai Dengan</td>
				                <td>:</td>
				                <td>
				                    <input class="easyui-datebox" label="Start Date:" labelPosition="top" name="jwaktu2" id="jwaktu2" required style="width: 120px;">
				                </td>
				            </tr>
							<tr>
				                <td>Keterangan</td>
				                <td>:</td>
				                <td>
									<textarea id="keterangan" name="keterangan" class="easyui-textbox"  style="width:200px" required>
				                    	
				                    </textarea>
				                </td>
				            </tr>
				        </table>
				    </form>
				</td>
				<td width="50%">
					<h4>Identitas Akun</h4>
					<div id="akun" style="display: none"><?php $this->load->view('view_pencairan_dana_akun'); ?></div>
				</td>
			</tr>
		</table>
	  
	   	<!-- Tombol Add, Edit, Hapus Datagrid -->
       	<div id="toolbar">  
            <table align="center" style="padding: 0px; width: 99%;">
                <tr>
                    <td>
                        <a href="javascript:void(0)" onclick="javascript:add()" class="btn btn-small btn-success"><i class="icon-plus-sign icon-large"></i>&nbsp;Add</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:edit()"><i class="icon-edit"></i>&nbsp;Edit</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-danger" onclick="javascript:hapus()"><i class="icon-trash icon-large"></i>&nbsp;Delete</a>
						<!--<a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:detail_jaminan()"><i class="icon-file"></i>&nbsp;Detail</a>
						<a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:view_report()"><i class="icon-print"></i>&nbsp;Report</a>-->
                    </td>
                    <td>&nbsp;</td>
                    <td align="right">
                    	<input class="easyui-searchbox" prompt="Silahkan Input Field" searcher="doSearch"  style="width:300px"/>
                    </td>
                </tr>
            </table>  
        </div>  
        <!-- end tombol datagrid -->
        <!-- Tombol simpan & Batal Form -->
        <div id="t_dlg_dis-buttons">
            <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:save()"><i class="icon-save icon-large"></i>&nbsp;Simpan</a>
            <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:$('#dlg').dialog('close')"><i class="icon-remove icon-large"></i>&nbsp;Batal</a>      
        </div>
	</div>
	<!-- Model end -->

</div>