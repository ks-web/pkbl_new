<?php
$app1 = 'Pencairan Dana'; // nama aplikasi
$module1 = 'prarealisasibl';
$appLink1 = 'pencairan_dana_akun'; // controller
$idField1  = 'id_akun'; //field key table
?>

<script>
	var url1;
	var app1 = "<?=$app1?>";
	var appLink1 = '<?=$appLink1?>';
	var module1 = '<?=$module1?>';
	var idField1 = '<?=$idField1?>';
	//var idParent = 1;
	
    function add1(){
        $('#dlg1').dialog('open').dialog('setTitle','Tambah ' + app1);
		$('#fm1').form('clear');
	    $('#tbl1').html('Save');
		url1 = '<?=base_url($module1 . '/' . $appLink1 . '/create')?>/'+idParent;
    }
    function edit1(){
        var row = $('#dg1').datagrid('getSelected');
		if (row){
			$('#tbl1').html('Simpan');
			$('#dlg1').dialog('open').dialog('setTitle','Edit '+ app1);
			$('#fm1').form('load',row);
			url1 = '<?=base_url($module1 . '/' . $appLink1 . '/update')?>/'+row[idField1];
	    }
    }
    function hapus1(){
        var row = $('#dg1').datagrid('getSelected');		
	    if (row){
			$.messager.confirm('Confirm','Apakah Anda Yakin Akan Menghapus ini?',function(r){
				if (r){
					$.post('<?=base_url($module1 . '/' . $appLink1 . '/delete')?>/'+row[idField1],function(result){
						if (result.success){
							$('#dg1').datagrid('reload');	// reload the user data
							$('#dg1').datagrid('reload');
						} else {
							$.messager.show({	// show error message
								title: 'Error',
								msg:result.msg
							});
						}
					},'json');
				}
			});
		}
    }
    function save1(){
    	//alert(idParent);
	   $('#fm1').form('submit',{
	    	url: url1,
	    	onSubmit: function(){
	    		return $(this).form('validate');
	    	},
	    	success: function(result){
	    		var result = eval('('+result+')');
	    		if (result.success){
	    			$('#dlg1').dialog('close');		
	    			reload1();
	    			//$('#dg1').datagrid('reload');
	                
	    		} else {
	             $.messager.alert('Error Message',result.msg,'error');
	    		}
	    	}
	    });
	}
	function doSearch1(value){
	    $('#dg1').datagrid('load',{    
	    	q:value  
	    });
	}
	function reload1(){
		$('#dg1').datagrid({    
	    	url: '<?=base_url($module1 . '/' . $appLink1 . '/read/')?>/'+idParent  
	    });
	}
</script>
 
<div class="tabs-container">                
	<table id="dg1" class="easyui-datagrid" 
	data-options="  
	    url: '<?=base_url($module1 . '/' . $appLink1 . '/read/')?>',
		singleSelect:'true', 
	    title:'<?=$app1?>',
	    toolbar:'#toolbar1',
	    iconCls:'icon-cog',
	    rownumbers:'true',  
	    idField:'<?=$idField1?>', 
	    pagination:'true',
	    fitColumns:'true',
	    pageList: [10,20,30]
	    "
	>
	    <thead>
	    	<!-- <th field="id_asset" width=100" sortable="true">ID Asset</th> -->
	        <th field="account_no" width=100" sortable="true">Account No</th>
	        <th field="nama_akun" width=100" sortable="true">Nama Akun</th>
			<th field="debet" width="100" sortable="true" formatter="format_numberdisp">Debet</th>
			<th field="kredit" width="100" sortable="true" formatter="format_numberdisp">Credit</th>
	    </thead>
	</table>
	
	<!-- Model Start -->
	<div id="dlg1" class="easyui-dialog" style="width:500px; height:auto; padding:10px" closed="true" buttons="#t_dlg_dis-buttons1" >
	    <form id="fm1" method="post" enctype="multipart/form-data" action="">
	       <table width="100%" align="center" border="0">
	            <tr>
	                <td>Account</td>
	                <td>:</td>
	                <td><input name="account_no" id="account_no"  class="easyui-combogrid" required style="width: 203px"
						data-options="panelHeight:200, panelWidth: 400,
						valueField:'',
						idField:'account',
						textField:'account',
						mode:'remote',
						url:'<?=base_url($module1 . '/' . $appLink1 . '/getAccount') ?>',
						onSelect :function(indek,row)
						{
						},
						columns:[[
                       		{field:'account',title:'Account',width:80},
                        	{field:'uraian',title:'Uraian',width:150}
                        ]]

						" >
	                </td>
	            </tr>
	            <tr>
	                <td>Debet</td>
	                <td>:</td>
	                <td>
	                    <input class="easyui-numberbox" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%'" name="debet" id="debet" required style="width: 200px;">
	                </td>
	            </tr>
	             <tr>
	                <td>Credit</td>
	                <td>:</td>
	                <td>
	                    <input class="easyui-numberbox" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%'" name="kredit" id="kredit" required style="width: 200px;">
	                </td>
	            </tr>	            
	        </table>
	    </form>
	   
	   	<!-- Tombol Add, Edit, Hapus Datagrid -->
       	<div id="toolbar1">  
            <table align="center" style="padding: 0px; width: 99%;">
                <tr>
                    <td>
                        <a href="javascript:void(0)" class="btn btn-small btn-success" onclick="javascript:add1()"><i class="icon-plus-sign icon-large"></i>&nbsp;Add</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:edit1()"><i class="icon-edit"></i>&nbsp;Edit</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-danger" onclick="javascript:hapus1()"><i class="icon-trash icon-large"></i>&nbsp;Delete</a>
                    </td>
                    <td>&nbsp;</td>
                    <td align="right">
                    	<input class="easyui-searchbox" prompt="Silahkan Input Field" searcher="doSearch1"  style="width:100px"/>
                    </td>
                </tr>
            </table>  
        </div>  
        <!-- end tombol datagrid -->
        <!-- Tombol simpan & Batal Form -->
        <div id="t_dlg_dis-buttons1">
            <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:save1()"><i class="icon-save icon-large"></i>&nbsp;Simpan</a>
            <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:$('#dlg1').dialog('close')"><i class="icon-remove icon-large"></i>&nbsp;Batal</a>      
        </div>
	</div>
	<!-- Model end -->
</div>