<?php
$app = 'Data Penilaian'; // nama aplikasi
$module = 'prarealisasibl';
$appLink = 'penilaian'; // controller
$idField  = 'id_bling'; //field key table
$moduleref = 'vendorbl';
$appLinkref = 'databl'; // controller
?>

<script>
    $("#instansi").hide();
	var url;
	var app = "<?=$app?>";
	var appLink = '<?=$appLink?>';
	var module = '<?=$module?>';
	var idField = '<?=$idField?>';
	var file_type = JSON.parse('<?=fileUploadTipe5?>');
	var file_upload = true;

	function validate_file(obj){
        var file_name = $(obj).val().replace('C:\\fakepath\\', '');
        var file_name_attr = file_name.split('.');
        file_name_attr[2] = obj.files[0].size/1024;


        if(file_type.indexOf(file_name_attr[1]) == -1 || (file_name_attr[2] > <?=fileUploadSize5?>)){
        	$.messager.alert('Error Message', 'File upload harus (' + file_type.join('|') + ') dan size dibawah <?=fileUploadSize5?>KB', 'error');
        	$(obj).wrap('<form>').closest('form').get(0).reset();
        	$(obj).unwrap();
        }
    }
	
	function aksi(){
	   return a='Layak';
	}
    function add(){
        $("#instansi").hide();
        $("#namabl").show();
        $('#dlg').dialog('open').dialog('setTitle','Tambah ' + app);
		$('#fm').form('clear');
	    $('#tbl').html('Save');
		url = '<?=base_url($module . '/' . $appLink . '/create')?>';
    }
    function edit(){
        
        var row = $('#dg').datagrid('getSelected');
		if (row){
			$('#fm').form('clear');
		    $("#namabl").hide();
            $("#instansi").show();
			$('#tbl').html('Simpan');
			$('#dlg').dialog('open').dialog('setTitle','Edit '+ app);
			$('#fm').form('load',row);
			url = '<?=base_url($module . '/' . $appLink . '/update')?>/'+row[idField];
            
	    }
    }
    function hapus(){
        var row = $('#dg').datagrid('getSelected');		
	    if (row){
			$.messager.confirm('Confirm','Apakah Anda Yakin Akan Menghapus ini?',function(r){
				if (r){
					$.post('<?=base_url($module . '/' . $appLink . '/delete')?>/'+row[idField],function(result){
						if (result.success){
							$('#dg').datagrid('reload');	// reload the user data
						} else {
							$.messager.show({	// show error message
								title: 'Error',
								msg:result.msg
							});
						}
					},'json');
				}
			});
		}
    }
    function save(){
	   $('#fm').form('submit',{
	    	url: url,
	    	onSubmit: function(){
	    		return $(this).form('validate');
	    	},
	    	success: function(result){
	    		var result = eval('('+result+')');
	    		if (result.success){

	    			$('#dlg').dialog('close');		
	    			$('#dg').datagrid('reload');
	                $("#id_bl").combogrid({url:'<?=base_url($module . "/". $appLink."/getBL/3")?>'});
	    		} else {
	             $.messager.alert('Error Message',result.msg,'error');
	    		}
	    	}
	    });
	}
	function doSearch(value){
	    $('#dg').datagrid('load',{    
	    	q:value  
	    });
	}
    function download_form(){
        var row = $('#dg').datagrid('getSelected');
        url  = '<?=base_url($module . '/penugasansurvey/download_form')?>/'+row.id_bl;
        if (row)
        {
            window.open(url,'_blank');
        }
    }

    function upload_file_all(){
        var row = $('#dg').datagrid('getSelected');
		if (row){
		  $('#dlg_upload_all').dialog('open').dialog('setTitle','Upload File');
          $('.edit').hide();
          file_upload=false;
          
            //lampiran file
            if(row.file_survey != ''){
            	var file_survey = '<?=base_url()?>'+row.file_survey;
                $('#current_file').attr('href', file_survey).show();
                $('#current_file_inp').attr('value', row.file_survey);
            } 
            
            url = '<?=base_url($module . '/' . $appLink . '/upload_lampiran_all')?>/'+row.id_bl;
          }
    }

    function upload_lampiran_all(){
        var row = $('#dg').datagrid('getSelected');
        if(row){
            
            $('#fm_upload_file_all').form('submit',{
                url: url,
                success: function(result){
                    var result = eval('('+result+')');
                    if (result.success){
                        $('#dlg_upload_all').dialog('close');
                        $('#dg').datagrid('reload');
                        
                    } else {
                        $.messager.alert('Error Message',result.msg,'error');
                    }
                }
            });
        }
         
    }
    function lam_file(value,row,index){
        var ktp = ''
        if(value != '') {
            ktp = '<a onclick="$(\'#frem\').dialog(\'open\');$(\'#frem iframe\').attr(\'src\',\'<?=base_url()?>'+row.file_survey+'\')" class="btn btn-small btn-default"><i class="icon-search"></i></a>';
        }

        return ktp;
    }
</script>
 
<div class="tabs-container">                
	<table id="dg" class="easyui-datagrid" 
	data-options="  
	    url: '<?=base_url($module . '/' . $appLink . '/read')?>',
		singleSelect:'true', 
	    title:'<?=$app?>',
	    toolbar:'#toolbar',
	    iconCls:'icon-cog',
	    rownumbers:'true',  
	    idField:'<?=$idField?>', 
	    pagination:'true',
        nowrap:false, 
	    fitColumns:'true',
	    pageList: [10,20,30]
	    "
	>
	    <thead>
	        <th field="id_bl" width="80" sortable="true">ID</th>
	        <th field="tanggal_input" width="70" sortable="true">Tanggal Daftar</th>
            <th field="nama" width="140" sortable="true">Nama Pemohon</th>
            <th field="instansi" width="160" sortable="true">Instansi Pemohon</th>
            <th align="right" field="nilai" width="100" sortable="true" formatter="format_numberdisp">Nilai Pengajuan</th>
            <!-- <th field="nilai_rekomendasi" width="250" sortable="true">Nilai Rekomendasi</th> -->
            <th field="status" width="100" sortable="true" hidden>Status</th>
            <th field="tanggal_survey" width="80" sortable="true">Tanggal<br /> Survey</th>
            <th field="rekomendasi_penilaian" width="80" sortable="true">Rekomendasi</th>
            <th field="nama_bl" width="100" sortable="true" hidden>nama bl</th>
            <th data-options="field:'file_survey',width:60" formatter="lam_file">Berkas<br />Survey</th>
	    </thead>
	</table>
	
	<!-- Model Start -->
	<div id="dlg" class="easyui-dialog" style="width:500px; height:500px; padding:10px" closed="true" buttons="#t_dlg_dis-buttons" >
	    <form id="fm" method="post" enctype="multipart/form-data" action="">
	        <table width="100%" align="center" border="0">
	            <tr id="namabl">
	                <td>Nama Bina Lingkungan</td>
	                <td>:</td>
	                <td>
                    <select name="id_bl" id="id_bl" class="easyui-combogrid" style="width:300px;" data-options="
                        required:true,
                        panelWidth:300,
                        value:'instansi',
                        idField:'id_bl',
                        textField:'instansi',
                        mode:'remote',
                        url:'<?=base_url($module . '/' . $appLink . '/getBL/3')?>',
                        columns:[[
                        {field:'id_bl',title:'ID',width:100},
                        {field:'tanggal_input',title:'Tanggal Daftar',width:100},
                        {field:'nama',title:'Nama Pemohon',width:100},
                        {field:'instansi',title:'Nama Instansi',width:100},
                        {field:'nilai',title:'Nilai Pengajuan',width:100},
                        {field:'kota',title:'Kota',width:100}
                        ]],
                        onSelect: function(no,row){
                           $('#nama').val(row.nama);
                           $('#no_ktp').val(row.no_ktp);
                           $('#alamat').val(row.alamat);
                           //$('#nilai').val(row.nilai);
                           $('#tanggal_survey').val(row.tanggal_survey);
                           $('#nama_bl').val(row.nama_bl); 
                           $('#barang').val(row.barang); 
                           $('#nilai').numberbox('setValue', row.nilai);
                        }
                        "></select>
	            </tr>
                <tr id="instansi">
	                <td>Nama Instansi</td>
	                <td>:</td>
	                <td>
	                    <input type="text" name="instansi" id="instansi" required style="width: 250px;" readonly="true">
	                </td>
	            </tr>
                <tr>
	                <td>Nama Pemohon</td>
	                <td>:</td>
	                <td>
	                    <input type="text" name="nama" id="nama" required style="width: 250px;" readonly="true">
	                </td>
	            </tr>
                <tr>
	                <td>Nomor KTP</td>
	                <td>:</td>
	                <td>
	                    <input type="text" name="no_ktp" id="no_ktp" required style="width: 250px;" readonly="true">
	                </td>
	            </tr>
                <tr>
	                <td>Alamat</td>
	                <td>:</td>
	                <td>
	                    <textarea id="alamat" name="alamat" style="width: 300px ;" class="easyui-textbox" required readonly="true"></textarea>
	                </td>
	            </tr>
	            <tr>
	                <td>Bidang Bantuan</td>
	                <td>:</td>
	                <td>
	                    <input id="nama_bl" name="nama_bl" style="width: 300px ;" readonly="true">
	                </td>
	            </tr>
                <tr>
	                <td>Nilai Pengajuan</td>
	                <td>:</td>
	                <td>
                        <input class="easyui-numberbox" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%'" name="nilai" id="nilai" readonly style="width: 250px;">
	                </td>
	            </tr>
                <tr>
	                <td>Barang</td>
	                <td>:</td>
	                <td>
                        <input id="barang" name="barang" style="width: 300px ;" readonly="true">
	                </td>
	            </tr>
                <!--tr>
	                <td>Tanggal Survey</td>
	                <td>:</td>
	                <td>
	                    <input id="tanggal_survey" name="tanggal_survey"  class="easyui-datebox" style="width:100px;" readonly="true"/></td>
	                </td>
	            </tr-->
                <!--tr>
	                <td>Nilai Rekomendasi</td>
	                <td>:</td>
	                <td>
                        <input class="easyui-numberbox" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%'" name="nilai_rekomendasi" id="nilai_rekomendasi" required style="width: 250px;">
	                </td>
	            </tr-->
                <tr>
	                <td>Keterangan Hasil Survey</td>
	                <td>:</td>
	                <td>
	                    <textarea id="keterangan_hasil" name="keterangan_hasil" style="width: 300px ;" required></textarea>
	                </td>
	            </tr>
                <tr>
	                <td>Penilaian Survey</td>
	                <td>:</td>
	                <td>
	                    <select  class="easyui-combobox" type="text" name="rekomendasi_penilaian" id="rekomendasi_penilaian" required style="width: 250px;">
	                    	<option value="Layak">Layak</option>
	                    	<option value="Tidak Layak">Tidak Layak</option>
	                    </select>
	                </td>
	            </tr>
                <!--tr>
	                <td>Keterangan</td>
	                <td>:</td>
	                <td>
	                    <textarea id="keterangan" name="keterangan" style="width: 300px ;" required></textarea>
	                </td>
	            </tr-->
	        </table>
	    </form>
	   
	   	<!-- Tombol Add, Edit, Hapus Datagrid -->
       	<div id="toolbar">  
            <table align="center" style="padding: 0px; width: 99%;">
                <tr>
                    <td>
                        <a href="javascript:void(0)" class="btn btn-small btn-success" onclick="javascript:add()"><i class="icon-plus-sign icon-large"></i>&nbsp;Add</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:edit()"><i class="icon-edit"></i>&nbsp;Edit</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-danger" onclick="javascript:hapus()"><i class="icon-trash icon-large"></i>&nbsp;Delete</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-warning" iconCls: 'icon-search' onclick="javascript:download_form()"><i class="icon-download-alt"></i>&nbsp;Download Form</a>
                        <a id="upload_file" href="javascript:void(0)" class="btn btn-small btn-success" iconCls: 'icon-search' onclick="javascript:upload_file_all()"><i class="icon-arrow-up icon-large"></i>&nbsp;Uplod File</a>
                    </td>
                    <td>&nbsp;</td>
                    <td align="right">
                    	<input class="easyui-searchbox" prompt="Silahkan Input Field" searcher="doSearch"  style="width:300px"/>
                    </td>
                </tr>
            </table>  
        </div>  
        <!-- end tombol datagrid -->
        <!-- Tombol simpan & Batal Form -->
        <div id="t_dlg_dis-buttons">
            <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:save()"><i class="icon-save icon-large"></i>&nbsp;Simpan</a>
            <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:$('#dlg').dialog('close')"><i class="icon-remove icon-large"></i>&nbsp;Batal</a>      
        </div>
	</div>
	<!-- Model end -->
	
	<!--Dialog upload file-->
	<div id="dlg_upload_all" class="easyui-dialog" style="width:500px; height:200px; padding:10px" closed="true" buttons="#t_dlg_upload_view-buttons" >
        <form id="fm_upload_file_all" method="POST" enctype="multipart/form-data">
            <table>
                
                <tr>
    				<td>File Survey</td>
    				<td>:</td>
    				<td><a href="" id="current_file" class="edit">File</a><input type="hidden" name="current_file" id="current_file_inp" />
    				<input type="file" id="file_survey" name="file_survey" size="20" onchange="validate_file(this)" />
    				</td>
    		   </tr>
            </table>
        </form>
        <!--Tombol upload file all-->
        <div id="t_dlg_upload_view">
            <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:upload_lampiran_all()"><i class="icon-arrow-up icon-large"></i>&nbsp;Upload</a>
            <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:$('#dlg_upload_all').dialog('close')"><i class="icon-remove icon-large"></i>&nbsp;Batal</a>      
        </div>
        <!--End Tombol upload file all-->
	</div>
<!--End Dialog upload file-->
	<div class="easyui-dialog" id="frem" style="width: 500px; height: 400px;" closed="true">
     <iframe src="" style="width: 100%;height: 100%">
         
     </iframe>
    </div>
</div>