<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	Class Model_pencairan_dana_umum extends MY_Model {
		function __construct() {
			parent::__construct();
			$this->_table = "tcd_all_umum"; // for insert, update, delete
			$this->_view = "tcd_all_umum_vd"; // for call view
			$this->_order = 'asc';
			$this->_sort = 'id_cd_umum';
			$this->_page = 1;
			$this->_rows = 10;

			$this->_create = true;
			$this->_update = true;

			if ($this->uri->segment(4)) {
				$this->_filter = array('id_cd_umum' => $this->uri->segment(4));
			}

			//parameter from post/get - search function
			if ($this->input->post('q')) {
				$this->_like = array(
					'no_trans' => $this->input->post('q')
				);
			}

// 			$this->_param = array('node' => $this->input->post('node'));

			//data array for input to database
			$this->_data = array(
			    'transaksi' => 'UM',
			    'no_trans' => $this->input->post('no_trans'),
			    'pemilik' => $this->input->post('pemilik'),
			    'alamat' => $this->input->post('alamat'),
			    'kode_bayar' => $this->input->post('kode_bayar')
			);
			
		}
		
		function search_no($no_trans){
		    $this->db->where('id_um', $no_trans);
		    $res = $this->db->get('pencairan_dana_umum_no_vd');
		    return $res->row_array();
		}

		public function getAccountPencairan($filter){
			$this->db->select('A.*');
			$this->db->from('detail_akun_transaksi_all_mitra_vd A');
			$this->db->where($filter);

			$query = $this->db->get();

			return $query->result_array();
		}
        public function getPencairan($filter){
			$this->db->select('A.*');
			$this->db->from('tcd_all_umum_preview_vd A');
			$this->db->where($filter);

			$query = $this->db->get();

			return $query->result_array();
		}
        public function getsign(){
            $sign = $this->db->get("sign_vd");
            return $sign;
            
        }

        public function createDefaultAccount($filter)
		{
			$this->db->select('A.*');
			$this->db->from('tuang_muka A');
			$this->db->where($filter);

			$query = $this->db->get();
			$tuang_muka = $query->row();

			$this->db->select('SUM(jumlah_harga) total');
			$this->db->from('detail_tuang_muka A');
			$this->db->where($filter);

			$query = $this->db->get();
			$detail_tuang_muka = $query->row();

			$filter2 = array('status' => $tuang_muka->tipe_um, 'transaksi' => 'UM');
			//print_r($filter2);
			$this->db->select('*');
			$this->db->from('mas_account_sektor');
			$this->db->where($filter2);

			$query = $this->db->get();

			//echo $this->db->last_query();

			foreach ($query->result_array() as $key) {
				if($key['posisi'] == 'D'){
					$debet = $detail_tuang_muka->total;
					$credit = 0;
				} else {
					$debet = 0;
					$credit = $detail_tuang_muka->total;
				}
				$data = array('id_cd_um' => $this->data_add['id_cd_umum'], 'account_no' => $key['account_no'], 'debet' => $debet, 'kredit' => $credit, 'status' => 'UM', 'transaksi' => 'CD');
				//print_r($data);
				$this->db->insert('detail_akun_temp', $data);
			}

			/*mas_account_sektor
			status
			transaksi

			tuang_muka
			id_um
			tipe_um*/
		}


	}
