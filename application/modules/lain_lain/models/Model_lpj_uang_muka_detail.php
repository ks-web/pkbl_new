<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	Class Model_lpj_uang_muka_detail extends MY_Model {
		function __construct() {
			parent::__construct();
			$this->_table = "detail_lpj_uang_muka"; // for insert, update, delete
			$this->_view = "detail_lpj_uang_muka_vd"; // for call view
			$this->_order = 'asc';
			$this->_sort = 'id_detail_lpj';
			$this->_page = 1;
			$this->_rows = 10;

			$this->_create = true;
			$this->_update = true;

			// if ($this->uri->segment(4)) {
				// $this->_filter = array('id_detail_lpj' => $this->uri->segment(4));
			// }

			//parameter from post/get - search function
			if ($this->input->post('q')) {
				$this->_like = array(
					'keterangan' => $this->input->post('q')
				);
			}

			$this->_param = array('id_detail_lpj' => $this->input->post('id_detail_lpj'));

			//data array for input to database
			$this->_data = array(
				'jumlah_satuan_realisasi' => $this->input->post('jumlah_satuan_realisasi'),
				'harga_satuan_realisasi' => $this->input->post('harga_satuan_realisasi'),
				'jumlah_harga_realisasi' => $this->input->post('jumlah_harga_realisasi'),
				'selisih_harga' => $this->input->post('selisih_harga')
			);
			
		}

	}
