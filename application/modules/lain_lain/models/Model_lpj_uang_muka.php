<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	Class Model_lpj_uang_muka extends MY_Model {
		function __construct() {
			parent::__construct();
			$this->_table = "tlpj_uang_muka"; // for insert, update, delete
			$this->_view = "tlpj_uang_muka_vd"; // for call view
			$this->_order = 'desc';
			$this->_sort = 'id_lpj';
			$this->_page = 1;
			$this->_rows = 10;

			$this->_create = true;
			$this->_update = true;

			if ($this->uri->segment(4)) {
				$this->_filter = array('id_lpj' => $this->uri->segment(4));
			}

			//parameter from post/get - search function
			if ($this->input->post('q')) {
				$this->_like = array(
					'id_lpj' => $this->input->post('q'),
					'id_um' => $this->input->post('q'),
					'keterangan_digunakan' => $this->input->post('q')
				);
			}

			$this->_param = array('id_lpj' => $this->input->post('node'));

			//data array for input to database
			$this->_data = array(
				'id_um' => $this->input->post('id_um'),
				'keterangan_digunakan' => $this->input->post('keterangan_digunakan')
			);
			
		}
		
		public function get_uang_muka($like) {
			$not_in = array();
			$this->db->select('id_um');
			$this->db->from('tlpj_uang_muka');
			$data = $this->db->get()->result_array();
			
			foreach ($data as $row) {
				$not_in[] = $row['id_um'];
			}
			
			$this->db->select('id_um, no_bl, keperluan, nilai, jwaktu1_disp, jwaktu2_disp');
			$this->db->from('proses_uang_muka_vd');
			if(count($not_in) >0){
				$this->db->where_not_in('id_um', $not_in);
			}
			
			$this->db->like('id_um', $like);
			
			$query = $this->db->get();
			return $query->result_array();
		}

	}
