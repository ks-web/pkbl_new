<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	Class Model_pencairan_dana extends MY_Model {
		function __construct() {
			parent::__construct();
			$this->_table = "tuang_muka"; // for insert, update, delete
			$this->_view = "tuang_muka_vd"; // for call view
			$this->_order = 'asc';
			$this->_sort = 'id_um';
			$this->_page = 1;
			$this->_rows = 10;

			$this->_create = true;
			$this->_update = true;

			if ($this->uri->segment(4)) {
				$this->_filter = array('id_um' => $this->uri->segment(4));
			}

			//parameter from post/get - search function
			if ($this->input->post('q')) {
				$this->_like = array(
					'id_um' => $this->input->post('q'),
					'tanggal_uangmuka' => $this->input->post('q'),
					'keperluan' => $this->input->post('q')
				);
			}

			$this->_param = array('id_um' => $this->input->post('id_um'));

			//data array for input to database
			$this->_data = array(
				'id_bl' => $this->input->post('id_bl'),
				'tanggal_create' => $this->input->post('tanggal_create'),
				'tanggal_uangmuka' => $this->input->post('tanggal_uangmuka'),
				'keperluan' => $this->input->post('keperluan'),
				'nilai' => $this->input->post('nilai'),
				'jwaktu1' => $this->input->post('jwaktu1'),
				'jwaktu2' => $this->input->post('jwaktu2'),
				'keterangan' => $this->input->post('keterangan')
			);
			
		}

		public function getMitra($filter)
		{
			$this->db->select('A.*');
			$this->db->from('tblingkungan A');
			//$this->db->join('tpersetujuan_bl B', 'A.id_bl = B.id_bl');
			//$this->db->join('tcd_mitra C', 'C.id_mitra = A.id_mitra', 'LEFT');
			//$this->db->where($filter);

			$query = $this->db->get();

			return $query->result_array();
		}

		public function getLastDataMitra()
		{
			$this->db->select('A.*');
			$this->db->from('tcd_mitra A');
			$this->db->order_by('id_cd', 'DESC');
			$this->db->limit(1);
			
			$query = $this->db->get();
			$result = $query->result_array();

			return $result[0]['id_cd'];
		}

	}
