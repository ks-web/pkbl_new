<?php
if (! defined('BASEPATH'))
    exit('No direct script access allowed');

class Model_detail_akun_jk_history extends MY_Model
{

    function __construct()
    {
        parent::__construct();
        $this->_table = "detail_akun_transaksi_all"; // for insert, update, delete
        $this->_view = "read_akun_jk_history_vd"; // for call view
        $this->_order = 'asc';
        $this->_sort = 'id_akun';
        $this->_page = 1;
        $this->_rows = 10;
        
        $this->_create = true;
        $this->_update = true;
        
        // if ($this->uri->segment(4)) {
        // $this->_filter = array('id_cd_um' => $this->uri->segment(4));
        // }
        
        // parameter from post/get - search function
        if ($this->input->post('q')) {
            $this->_like = array();
        }
        
        // $this->_param = array('id_akun' => $this->input->post('id_akun'));
        
        // data array for input to database
        $this->_data = array();
    }

    function read_history($transaksi, $status, $id_cd_um)
    {
        $this->_view = $this->_view ? $this->_view : $this->_table;
        
        $statuss = array(
            'JK',
            $status
        );
        $this->db->where('id_cd_um', $id_cd_um);
        $this->db->where('transaksi', $transaksi);
        $this->db->where_in('status', $statuss);
        
        $result = $this->db->get($this->_view);
        return json_encode($result->result_array());
    }
}
