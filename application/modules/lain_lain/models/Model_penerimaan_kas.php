<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	Class Model_penerimaan_kas extends MY_Model {
		function __construct() {
			parent::__construct();
			$this->_table = "tpenerimaan_kas"; // for insert, update, delete
			$this->_view = "tpenerimaan_kas_vd"; // for call view
			$this->_order = 'asc';
			$this->_sort = 'id_penerimaan_kas';
			$this->_page = 1;
			$this->_rows = 10;

			$this->_create = true;
			$this->_update = true;

			if ($this->uri->segment(4)) {
				$this->_filter = array('id_penerimaan_kas' => $this->uri->segment(4));
			}

			//parameter from post/get - search function
			if ($this->input->post('q')) {
				$this->_like = array(
					'id_penerimaan_kas' => $this->input->post('q'),
					'id_mitra' => $this->input->post('q'),
					'nama' => $this->input->post('q'),
					'type' => $this->input->post('q'),
					'nama_perusahaan' => $this->input->post('q')
				);
			}

			//$this->_param = array('id_cd' => $this->input->post('id_cd'));

			//data array for input to database
			$this->_data = array(
				'nama' => $this->input->post('nama_mitra'),
				'tanggal_create' => $this->input->post('tanggal'),
				'deskripsi' => $this->input->post('deskripsi'),
				'type' => $this->input->post('type'),
				'jumlah' => $this->input->post('jumlah'),
				'date_modified' => date('Y-m-d')
			);
			
		}

		public function getMitra($filter)
		{
			$this->db->select('A.*');
			$this->db->from('tmitra A');
			//$this->db->join('tpersetujuan_mitra B', 'A.id_mitra = B.id_mitra');
			//$this->db->join('tcd_mitra C', 'C.id_mitra = A.id_mitra', 'LEFT');
			$this->db->where($filter);

			$query = $this->db->get();

			return $query->result_array();
		}

	}
