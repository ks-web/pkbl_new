<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	Class Model_proses_uang_muka_detail extends MY_Model {
		function __construct() {
			parent::__construct();
			$this->_table = "detail_tuang_muka"; // for insert, update, delete
			$this->_view = "proses_uang_muka_detail_vd"; // for call view
			$this->_order = 'asc';
			$this->_sort = 'id_detail_um';
			$this->_page = 1;
			$this->_rows = 10;

			$this->_create = true;
			$this->_update = true;

			// if ($this->uri->segment(4)) {
				// $this->_filter = array('id_detail_um' => $this->uri->segment(4));
			// }

			//parameter from post/get - search function
			if ($this->input->post('q')) {
				$this->_like = array(
					'keterangan' => $this->input->post('q')
				);
			}

			$this->_param = array('id_detail_um' => $this->input->post('id_detail_um'));

			//data array for input to database
			$this->_data = array(
				'id_um' => $this->input->post('id_um'),
				'keterangan' => $this->input->post('keterangan'),
				'jumlah_satuan' => $this->input->post('jumlah_satuan'),
				'harga_satuan' => $this->input->post('harga_satuan'),
				'jumlah_harga' => $this->input->post('jumlah_harga')
			);
			
		}

	}
