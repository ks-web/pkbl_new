<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	Class Model_detail_akun_jk extends MY_Model {
		function __construct() {
			parent::__construct();
			$this->_table = "detail_akun_temp"; // for insert, update, delete
			$this->_view = "read_akun_jk_vd"; // for call view
			$this->_order = 'asc';
			$this->_sort = 'id_akun';
			$this->_page = 1;
			$this->_rows = 10;

			$this->_create = true;
			$this->_update = true;

			if ($this->uri->segment(4)) {
				$this->_filter = array('id_cd_um' => $this->uri->segment(4), );
			}

			//parameter from post/get - search function
			if ($this->input->post('q')) {
				$this->_like = array(
					'id_akun' => $this->input->post('q'),
					'id_cd_um' => $this->input->post('q'),
					'account_no' => $this->input->post('q')
				);
			}

			$this->_param = array('id_akun' => $this->input->post('id_akun'));

			//data array for input to database
			$this->_data = array(
				// 'id_bbbantu' => $this->input->post('id_bbbantu'),
				// 'id_mitra' => $this->input->post('id_mitra'),
				'account_no'=> $this->input->post('account_no'),
				'nama_akun'=> $this->input->post('nama_akun'),
				'debet'=> $this->input->post('debet'),
				'kredit'=> $this->input->post('kredit')
			);
			
		}

		public function getStatusBalance($filter)
		{
			$this->db->select('SUM(A.debet)-SUM(A.kredit) balance, SUM(A.debet) total');
			$this->db->from('detail_akun_temp A');
			$this->db->where($filter);

			$query = $this->db->get();
			$balance = $query->row();
			//echo $this->db->last_query();
			//print_r($balance);
			if($balance->balance != 0){
				return array('status' => 1, 'msg' => 'Gagal Posting, Jumlah Nilai Akun Tidak Balance');
			} else {
				$filter2 = array('A.id_jurnal_koreksi' => $filter['A.id_cd_um']);

				$this->db->select('A.nilai');
				$this->db->from('tjurnal_koreksi A');
				//$this->db->join('mas_account B', 'B.kode_account = A.account_no AND B.no_rek IS NOT NULL');
				$this->db->where($filter2);

				$query = $this->db->get();
				$jurnal = $query->row();

				if($balance->total != $jurnal->nilai){
					return array('status' => 1, 'msg' => 'Gagal Posting, Jumlah Nilai Akun Tidak Sama Dengan Nilai Koreksi Awal');
				} else {
					return array('status' => 0, 'msg' => 'Posting Berhasil');
				}
			}

			//return $query->row()->balance;
		}

		public function getStatusPosting($filter)
		{
			$this->db->select('A.*');
			$this->db->from('tjurnal_koreksi A');
			$this->db->where($filter);

			$query = $this->db->get();

			return $query->result_array();
		}

		public function setStatusPosting($filter, $data)
		{
			$this->db->where($filter);
			$this->db->update('tjurnal_koreksi A', $data);

			return array('success' => true);
		}

		public function getAccount($filter, $like)
		{
			$this->db->select('A.*');
			$this->db->from('mas_account A');
			$this->db->or_like($like);
			$this->db->where($filter);

			$query = $this->db->get();

			return $query->result_array();
		}

		public function getTypeTrans($filter)
		{
			$this->db->select('A.jenis_transaksi, A.jurnal');
			$this->db->from('tjurnal_koreksi A');
			$this->db->where($filter);

			$query = $this->db->get();

			return $query->row();
		}

	}
