<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	Class Model_penanaman_pohon extends MY_Model {
		function __construct() {
			parent::__construct();
			$this->_table 	= "t_penanaman_pohon";
			$this->_view 	= "t_penanaman_pohon_vd";
			$this->_order 	= 'asc';
			$this->_sort 	= 'id_tpohon';
			$this->_page 	= 1;
			$this->_rows 	= 10;

			$this->_update = true;
			$this->_create = true;

			if ($this->uri->segment(4)) {
				$this->_filter = array('id_tpohon' => $this->uri->segment(4));
			}
			if ($this->input->post('q')) {
				$this->_like = array(	
					'id_tpohon' 	=> $this->input->post('q'),
					'nama_sektor' => $this->input->post('q'),
					'keterangan' => $this->input->post('q')
				);
			}

			$this->_param = array('id_tpohon' => $this->input->post('id_tpohon'));

			$this->_data = array(
				'id_tpohon' 		=> $this->input->post('id_tpohon'),
				'id_pohon' 			=> $this->input->post('id_pohon'),
				'kabupaten' 		=> $this->input->post('kabupaten'),
				'kecamatan'			=> $this->input->post('kecamatan'),
				'kelurahan'			=> $this->input->post('kelurahan'),
				'jumlah'			=> $this->input->post('jumlah'),
				'tanggal_create' 	=> date('Y-m-d'),
				'creator' 			=> $this->session->userdata('id')
			);
		}

		
		public function get_pohon()
		{
			echo json_encode($this->db->get('mas_pohon')->result_array());
		}
		
		public function get_id()
		{
			$this->db->select_max('id_tpohon');
			$q 		= $this->db->get('t_penanaman_pohon')->row();			
			$tahun 	= substr($q->id_tpohon,2,4);
			$seq	= substr($q->id_tpohon,8,3);
			if($q->id_tpohon == NULL)
				return 'PH'.date('Ym').'001';
			else
			{
				if($tahun < date('Y'))
					return 'PH'.date('Ym').'001';
					
				else
				{
					$hasil = $seq + 1;
					$nomor = sprintf("%03s", $hasil);
					return 'PH'.date('Ym').$nomor;
				}
			}		
		}

	}
