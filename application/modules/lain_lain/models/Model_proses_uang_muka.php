<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	Class Model_proses_uang_muka extends MY_Model {
		function __construct() {
			parent::__construct();
			$this->_table = "tuang_muka";
			// for insert, update, delete
			$this->_view = "proses_uang_muka_vd";
			// for call view
			$this->_order = 'asc';
			$this->_sort = 'id_um';
			$this->_page = 1;
			$this->_rows = 10;

			$this->_create = true;
			$this->_update = true;

			if ($this->uri->segment(4)) {
				$this->_filter = array('id_um' => $this->uri->segment(4));
			}

			//parameter from post/get - search function
			if ($this->input->post('q')) {
				$this->_like = array(
					'id_um' => $this->input->post('q'),
					'transaksi' => $this->input->post('q'),
					'keperluan' => $this->input->post('q')
				);
			}

			$this->_param = array('id_um' => $this->input->post('id_um'));

			//data array for input to database
			$this->_data = array(
				'no_bl' => $this->input->post('no_bl'),
			    'tipe_um' => $this->input->post('tipe_um'),
				'keperluan' => $this->input->post('keperluan'),
				'nilai' => $this->input->post('nilai'),
				'jwaktu1' => $this->input->post('jwaktu1'),
				'jwaktu2' => $this->input->post('jwaktu2')
			);

		}

		public function get_no_bl($like = null) {
			$this->db->select('id_bl, nama, instansi');
			$this->db->from('proses_uang_muka_bl_vd');

			$this->db->or_like(array(
				'id_bl' => $like,
				'nama' => $like
			));
			
			$query = $this->db->get();
			return $query->result_array();
		}
        public function getAccount($filter, $like)
		{
			$this->db->select('A.*');
			$this->db->from('mas_account A');
			$this->db->or_like($like);
			$this->db->where($filter);

			$query = $this->db->get();

			return $query->result_array();
		}

	}
