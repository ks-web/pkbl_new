<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	Class Model_pencairan_dana_akun extends MY_Model {
		function __construct() {
			parent::__construct();
			$this->_table = "tuang_muka_akun"; // for insert, update, delete
			$this->_view = "tuang_muka_akun_vd"; // for call view
			$this->_order = 'asc';
			$this->_sort = 'id_akun';
			$this->_page = 1;
			$this->_rows = 10;

			$this->_create = true;
			$this->_update = true;

			if ($this->uri->segment(4)) {
				$this->_filter = array('id_um' => $this->uri->segment(4));
			}

			//parameter from post/get - search function
			if ($this->input->post('q')) {
				$this->_like = array(
					'id_akun' => $this->input->post('q'),
					'account_no' => $this->input->post('q'),
					'uraian' => $this->input->post('q')
				);
			}

			$this->_param = array('id_akun' => $this->input->post('id_akun'));

			//data array for input to database
			$this->_data = array(
				'account_no' => $this->input->post('account_no'),
				'debet' => $this->input->post('debet'),
				'kredit' => $this->input->post('kredit')
			);
			
		}

		public function getAccount($filter)
		{
			$this->db->select('A.*');
			$this->db->from('account_pk A');
			//$this->db->where($filter);

			$query = $this->db->get();

			return $query->result_array();
		}

	}
