<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	Class Model_jurnal_koreksi extends MY_Model {
		function __construct() {
			parent::__construct();
			$this->_table 	= "tjurnal_koreksi"; // for insert, update, delete
			$this->_view 	= "tjurnal_koreksi_vd"; // for call view
			$this->_order 	= 'desc';
			$this->_sort 	= 'tanggal_create';
			$this->_page 	= 1;
			$this->_rows 	= 10;

			$this->_create = true;
			$this->_update = true;

			if ($this->uri->segment(4)) {
				$this->_filter = array('id_jurnal_transaksi' => $this->uri->segment(4));
			}
			
			//parameter from post/get - search function
			if ($this->input->post('q')) {
				$this->_like = array(
					'' => $this->input->post('q'),
					'' => $this->input->post('q'),
					'' => $this->input->post('q')
				);
			}

			$this->_param = array('id_jurnal_koreksi' => $this->input->post('id_jurnal_koreksi'));

			//data array for input to database
			$this->_data = array(
				'jenis_transaksi' => $this->input->post('jenis_transaksi'),
				'no_transaksi' => $this->input->post('nomor'),
				'keterangan_koreksi' => $this->input->post('keterangan_koreksi'),
				'nilai' => $this->input->post('nilai'),
				'nilai_koreksi' => $this->input->post('nilai_koreksi')
			);
		}
		
		function get_nomor($tipe, $jenis_transaksi,$no)
		{
			if($tipe == 2 AND $jenis_transaksi == 1) // KM CR
			{
				$this->db->select('cd.id_cd as nomor, km.nama as nama_pemohon, (ang.jumlah + ang.biaya) as nilai_usulan');
				$this->db->where('cd.code', 'KM');
				$this->db->where('cd.status_posted', '1');
				$this->db->where('cd.id_cd', $no);
				$this->db->join('tmitra km', 'cd.no = km.id_mitra', 'left');
				$this->db->join('tbl_angsuran ang', 'cd.no = ang.id_mitra', 'left');
				$q = $this->db->get('tcd_all cd');
				return json_encode($q->row_array());
			}
			
			else if($tipe == 1 AND $jenis_transaksi == 1) //KM CD
			{
				
				$this->db->select('cd.id_cd as nomor, km.nama as nama_pemohon, pes.angsuran_pokok as nilai_usulan');
				$this->db->where('cd.code','KM');
				$this->db->where('cd.status_posted', '1');
				$this->db->where('cd.id_cd', $no);
				$this->db->join('tmitra km', 'cd.no = km.id_mitra', 'left');
				$this->db->join('tpenilaian_survey_pengusaha_evaluator pes', 'cd.no = pes.id_mitra');
				$q	= $this->db->get('tcd_all cd');
				return json_encode($q->row_array());
			}
			
			else if($tipe == 1 AND $jenis_transaksi == 2) //BL CD
			{
				$this->db->select('cd.id_cd as nomor, bl.nama as nama_pemohon, stj.nilai_rekomendasi as nilai_usulan');
				$this->db->where('cd.code','BL');
				$this->db->where('cd.status_posted', '1');
				$this->db->where('cd.id_cd', $no);
				$this->db->join('tblingkungan bl','cd.no = bl.id_bl');
				$this->db->join('tpenilaian_survey_bl stj', 'cd.no = stj.id_bl');
				$q	= $this->db->get('tcd_all cd');
				return json_encode($q->row_array());
			}
			else if($tipe == 0 AND $jenis_transaksi == 4) //Jurnal Umum
			{
			    $this->db->select('a.id_penerimaan_kas as nomor, a.nama as nama_pemohon, a.jumlah as nilai_usulan');
			    $this->db->where('a.status_posted', '1');
			    $this->db->where('a.id_penerimaan_kas', $no);
			    $q	= $this->db->get('tpenerimaan_kas a');
			    return json_encode($q->row_array());
			}
		}
		
		function get_account($id_cd_um)
		{
			$this->db->select('a.account_no, a.nama_akun, a.debet, a.kredit, a.id_cd_um, a.status');
			$this->db->where('id_cd_um',$id_cd_um);
			$q	= $this->db->get('detail_akun_transaksi_all a');
			return json_encode($q->result_array());
		}
		
		function save_detail($data)
		{
			$insert = $this->db->insert('detail_akun_transaksi_all', $data);
			
			if($insert)
            {   
                $this->output->set_output(json_encode(array('success'=>true)));
            }else
            {
                $this->output->set_output(json_encode(array('msg'=>$this->db->_error_message())));
            }
		}
        public function get_jurnal_umum(){
            $data = $this->db->get('tpenerimaan_kas')->result_array();
            return $data;
        }

        public function getStatusPosting($filter)
		{
			$this->db->select('A.*');
			$this->db->from('tjurnal_koreksi A');
			$this->db->where($filter);

			$query = $this->db->get();

			return $query->result_array();
		}

		public function clearAccount($filter)
		{
			$this->db->where($filter);
			$this->db->delete('detail_akun_temp');

			//only for jurnal koreksi
			$filter2 = array('A.id_jurnal_koreksi' => $filter['id_cd_um']);
			$this->db->select('A.*');
			$this->db->from('tjurnal_koreksi A');
			$this->db->where($filter2);

			$query = $this->db->get();

			$filter3 = array('id_cd_um' => $query->row()->no_transaksi, 'status' => 'JK', 'trasaksi' => $query->row()->jurnal);
			//print_r($filter3);
			$this->db->where($filter3);
			$this->db->delete('detail_akun_transaksi_all');
		}

	}
