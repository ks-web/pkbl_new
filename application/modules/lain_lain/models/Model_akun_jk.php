<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	Class Model_akun_jk extends MY_Model {
		function __construct() {
			parent::__construct();
			$this->_table = "detail_akun_transaksi_all"; // for insert, update, delete
			$this->_view = "read_akun_jk_vd"; // for call view
			$this->_order = 'asc';
			$this->_sort = 'id_akun';
			$this->_page = 1;
			$this->_rows = 10;

			$this->_create = true;
			$this->_update = true;

			if ($this->uri->segment(4)) {
				$this->_filter = array('id_cd_um' => $this->uri->segment(4));
			}

			//parameter from post/get - search function
			if ($this->input->post('q')) {
				$this->_like = array(
					'id_akun' => $this->input->post('q'),
					'id_cd_um' => $this->input->post('q'),
					'account_no' => $this->input->post('q')
				);
			}

			$this->_param = array('id_akun' => $this->input->post('id_akun'));

			//data array for input to database
			$this->_data = array(
				// 'id_bbbantu' => $this->input->post('id_bbbantu'),
				// 'id_mitra' => $this->input->post('id_mitra'),
				'account_no'=> $this->input->post('account_no'),
				'nama_akun'=> $this->input->post('nama_akun'),
				'debet'=> $this->input->post('debet'),
				'kredit'=> $this->input->post('kredit')
			);
			
		}

		public function getAccount($filter)
		{
			$this->db->select('A.*');
			$this->db->from('account_pk A');
			//$this->db->where($filter);

			$query = $this->db->get();

			return $query->result_array();
		}

	}
