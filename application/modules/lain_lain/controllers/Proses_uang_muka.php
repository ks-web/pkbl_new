<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');
	class Proses_uang_muka extends MY_Controller {
		public $models = array('proses_uang_muka');

		public function __construct() {
			parent::__construct();
		}

		public function index() {
			$data = array();
			$data['menu'] = $this->model_menu->getAllMenu();

			$this->template->load('template', 'view_proses_uang_muka', $data);
		}

		public function read() {
			$this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read());
		}

		public function read_all() {
			$this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read_all());
		}

		public function create() {
			// parametter key

			// additional block
			$thn_bln = date("Y-m");
			$this->data_add['id_um'] = $this->get_no($thn_bln);
			$this->data_add['creator'] = $this->session->userdata('id');
			$this->data_add['tanggal_create'] = date("Y-m-d H:i:s");

			// addtional get
			$result = $this->{$this->models[0]}->insert($this->data_add);
			if ($result == 1) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => $this->db->_error_message())));
			}
		}

		public function update() {
			$param = $this->uri->segment(4);
			// parameter key

			// additional block
			$this->data_add['modifier'] = $this->session->userdata('id');
			$this->data_add['tanggal_modified'] = date("Y-m-d H:i:s");
			// addtional data

			// additional where
			$this->where_add['id_um'] = $param;

			$result = $this->{$this->models[0]}->update($this->where_add, $this->data_add);
			if ($result == 1) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'Data Gagal Di Update !')));
			}
		}

		public function delete() {
			$param = $this->uri->segment(4);
			// parameter key

			// additional block

			// additional where
			$this->where_add['id_um'] = $param;

			$result = $this->{$this->models[0]}->delete($this->where_add);
			if ($result == 1) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'Data Gagal Di Hapus !')));
			}
		}

		public function get_no_bl() {
			$q = $this->input->post('q');

			$ret = $this->{$this->models[0]}->get_no_bl($q);

			echo json_encode($ret);
		}

		private function get_no($thn_bln) {
			$thn_bln_arr = explode('-', $thn_bln);

			$no = '0000';

			$sql = "
				SELECT
					RIGHT(max(a.id_um),4)*1 AS last_seq
				FROM
					tuang_muka a
				WHERE
					DATE_FORMAT(a.tanggal_create, '%Y-%m') = ?
			";
			$query = $this->db->query($sql, array($thn_bln));
			$row = $query->row_array();

			$last_seq = (int)$row['last_seq'];
			$next_seq = $last_seq + 1;

			$leng_temp_no = strlen($next_seq);
			$no = 'UM' . implode('', $thn_bln_arr) . substr($no, $leng_temp_no) . $next_seq;

			return $no;

		}
		
		public function read_preview($id = null) {
			$this->db->where('id_um', $id);
			$check = $this->db->get('proses_uang_muka_vd');
			if ($check->num_rows() > 0) {

				$data['data_head'] = $check->row_array();
				$data['data_detail'] = null;
				
				$this->db->where('id_um', $id);
				$q = $this->db->get('proses_uang_muka_detail_vd');
				if($q->num_rows() > 0){
					$data['data_detail'] = $q->result_array();
				}

				// kota tanggal
				$data['kota'] = 'Cilegon';
				$this->load->helper('tanggal_indo');
				$this->load->helper('terbilang');
				$data['tanggal'] = tanggal_display();

				$html = $this->load->view('view_proses_uang_muka_preview', $data, true);

				$this->load->library('dompdf_gen');
				// Load library
				$this->dompdf->set_paper('A4', 'potrait');
				// Setting Paper
				// // Convert to PDF
				$this->dompdf->load_html($html);
				$this->dompdf->render();
				$this->dompdf->stream("Preview_Uang_Muka-" . $id . ".pdf");
			} else {
				show_404();
			}
		}
        public function getAccount()
		{
			$param = $this->input->post('q');
			$filter = array();
			$like = array('A.kode_account' => $param, 'A.uraian' => $param);
			$this->output->set_content_type('application/json')->set_output(json_encode($this->{$this->models[0]}->getAccount($filter, $like)));
		}
        public function save_akun(){
            $id_cd_um = $this->input->post('id_cd_um');
            $account_no = $this->input->post('account_no');
            $nama_akun = $this->input->post('nama_akun');
            $debet = $this->input->post('debet');
            $kredit = $this->input->post('kredit');
            
            $data_add = array(
                'id_cd_um' => $id_cd_um,
                'account_no' => $account_no,
                'nama_akun' => $nama_akun,
                'debet' => $debet,
                'kredit' => $kredit,
                'status' => 'UM',
                'transaksi' => 'CD'
            );
            $res = $this->db->insert('detail_akun_transaksi_all',$data_add);
            if($res){
                $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode(array('success' => true)));
            }else{
                $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode(array('msg' => 'Data Gagal Di Tambah !')));
            }
            
        }
        public function update_akun(){
            $id_cd_um = $this->input->post('id_cd_um');
            $id_akun = $this->input->post('id_akun');
            $account_no = $this->input->post('account_no');
            $nama_akun = $this->input->post('nama_akun');
            $debet = $this->input->post('debet');
            $kredit = $this->input->post('kredit');
            
            $data_add = array(
                'id_cd_um' => $id_cd_um,
                'account_no' => $account_no,
                'nama_akun' => $nama_akun,
                'debet' => $debet,
                'kredit' => $kredit,
                'status' => 'UM',
                'transaksi' => 'CD'
            );
            $data_where = array(
                'id_cd_um' => $id_cd_um,
                'id_akun' => $id_akun
            );
            $res = $this->db->update('detail_akun_transaksi_all',$data_add,$data_where);
            if($res){
                $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode(array('success' => true)));
            }else{
                $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode(array('msg' => 'Data Gagal Di Tambah !')));
            }
            
        }
        public function delete_akun($id_akun,$id_cd_um){
            
            $res = $this->db
                            ->where('id_akun',$id_akun)
                            ->where('id_cd_um',$id_cd_um)
                            ->delete('detail_akun_transaksi_all');
            if($res){
                $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode(array('success' => true)));
            }else{
                $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode(array('msg' => 'Data Gagal Di Tambah !')));
            }
            
        }
        public function get_dataAkun($id_cd_um){
            $data = $this->db->where('id_cd_um',$id_cd_um)->get('detail_akun_transaksi_all')->result_array();
            $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode($data));
        } 

	}
