<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');
	class Penanaman_pohon extends MY_Controller {
		public $models = array('penanaman_pohon');
		
		public function __construct() {
			parent::__construct();
		}

		public function index() {
			$data = array();
			$data['menu'] = $this->model_menu->getAllMenu();

			$this->template->load('template', 'view_penanaman_pohon', $data);
		}

		public function read() {
			$this->output->set_content_type('application/json')->set_output($this->penanaman_pohon->read());
		}

		public function read_all() {
			$this->output->set_content_type('application/json')->set_output($this->penanaman_pohon->read_all());
		}

		public function create() {
			
			//$this->data_add['parentnode'] = $parentnode;
			$this->data_add['id_tpohon'] = $this->get_id();		
			$result = $this->penanaman_pohon->insert($this->data_add);
			if ($result == 1) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => $this->db->_error_message())));
			}
		}

		public function update() {
			$param = $this->uri->segment(4);
	
			$this->where_add['id_tpohon'] = $param;			
			$result = $this->{$this->models[0]}->update($this->where_add, $this->data_add);
			
			if ($result == 1) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'Data Gagal Di Update !')));
			}
		}

		public function delete() {
			// additional where
			$this->where_add['id_tpohon'] = $this->uri->segment(4);
			
			$result = $this->penanaman_pohon->delete($this->where_add);
			if ($result == 1) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'Data Gagal Di Hapus !')));
			}
		}
		
		public function get_pohon()
		{
			return $this->penanaman_pohon->get_pohon();
		}
		
		public function get_id()
		{
			return $this->penanaman_pohon->get_id();
		}


	}
