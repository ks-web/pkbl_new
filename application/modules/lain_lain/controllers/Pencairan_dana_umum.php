<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');
	class Pencairan_dana_umum extends MY_Controller {
		public $models = array('pencairan_dana_umum');
		
		public function __construct() {
			parent::__construct();
		}

		public function index() {
			$data = array();
			$data['menu'] = $this->model_menu->getAllMenu();

			$this->template->load('template', 'view_pencairan_dana_umum', $data);
		}

		public function read() {
			$this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read());
		}

		public function read_all() {
			$this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read_all());
		}

		public function create() {
			// additional block
			$id = $this->get_no(date('Y-m'));
			$this->data_add['id_cd_umum'] = $id;
			$this->data_add['creator'] = $this->session->userdata('id');
			$this->data_add['tanggal_create'] = date('Y-m-d H:i:s');

			$no_trans = $this->input->post('no_trans');
			
			// addtional get
			$result = $this->{$this->models[0]}->insert($this->data_add);
			if ($result == 1) {
				$this->{$this->models[0]}->createDefaultAccount(array('A.id_um' => $no_trans));
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => $this->db->_error_message())));
			}
		}

		public function update() {
			$param = $this->uri->segment(4); // parameter key

			// additional block

			// addtional data

			// additional where
			$this->where_add['param'] = $param;

			$result = $this->{$this->models[0]}->update($this->where_add, $this->data_add);
			if ($result == 1) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'Data Gagal Di Update !')));
			}
		}

		public function delete() {
			$param = $this->uri->segment(4); // parameter key

			// additional block

			// additional where
			$this->where_add['param'] = $param;

			$result = $this->{$this->models[0]}->delete($this->where_add);
			if ($result == 1) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'Data Gagal Di Hapus !')));
			}
		}

		function previewPencairanDana($idbl){    // load dompdf
    		$param = $this->uri->segment(4); // parameter key
			$filter = array('A.id_cd_umum' => $param);
			$filter2 = array('A.id_cd_um' => $param);

		    //load content html
		    //$data['mitra'] = $this->{$this->models[0]}->kartuPiutangMitra($filter);
		    $data['pencairan'] = $this->{$this->models[0]}->getPencairan($filter);
            $data['account'] = $this->{$this->models[0]}->getAccountPencairan($filter2);
            $data['sign']=$this->{$this->models[0]}->getsign()->row_array();
		    // $html = $this->load->view('sph3.php', $data, true);
		    //$html = $this->load->view('view_pencairan_dana_cd', $data,true);
		    $html = $this->load->view('view_pencairan_dana_umum_cd', $data, true);
		    // create pdf using dompdf
  			$this->load->library('dompdf_gen'); // Load library
			$this->dompdf->set_paper('A4', 'portrait'); // Setting Paper
			// Convert to PDF
			$this->dompdf->load_html($html);
			$this->dompdf->render();
			$this->dompdf->stream("Pencairan_Dana_KM_".$idbl.".pdf");
        }
		
		public function search_no(){
		    $no_trans = $this->input->post('no_trans');
		    $result = $this->{$this->models[0]}->search_no($no_trans);
		    
		    $this->output->set_content_type('application/json')->set_output(json_encode($result));
		}
		
		private function get_no($thn_bln) {
		    $thn_bln_arr = explode('-', $thn_bln);
		    
		    $no = '000';
		    
		    $sql = "
				SELECT
					RIGHT(max(a.id_jurnal_koreksi),4)*1 AS last_seq
				FROM
					tjurnal_koreksi a
				WHERE
					DATE_FORMAT(a.tanggal_create, '%Y-%m') = ?
			";
		    $query = $this->db->query($sql, array($thn_bln));
		    $row = $query->row_array();
		    
		    $last_seq = (int)$row['last_seq'];
		    $next_seq = $last_seq + 1;
		    
		    $leng_temp_no = strlen($next_seq);
		    $no = 'UM'.implode('', $thn_bln_arr) . substr($no, $leng_temp_no) . $next_seq;
		    
		    return $no;
		    
		}

	}
