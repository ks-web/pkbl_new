<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');
	class Jurnal_koreksi extends MY_Controller {
		public $models = array('jurnal_koreksi');

		public function __construct() {
			parent::__construct();
			$this->load->library('disposisi');
		}
		
		public function index() 
		{
			$data = array();
			$data['menu'] = $this->model_menu->getAllMenu();

			$this->template->load('template', 'view_' . $this->models[0], $data);
		}

		public function read() 
		{
			$this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read());
		}

		public function read_all() 
		{
			$this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read_all());
		}
		
		public function create() 
		{
		    if($this->input->post('jenis_transaksi') <= 2){
		        if($this->input->post('tipe') == 1){
		            $this->data_add['jurnal'] = 'CD';
		        }else if($this->input->post('tipe') == 2){
		            $this->data_add['jurnal'] = 'CR';
		        }
		    } else {
		        if($this->input->post('jenis_transaksi') == '4') {
		            $this->data_add['jurnal'] = 'CR';
		        }
		    }
		    
		    $this->data_add['tanggal_create'] = date('Y-m-d');
		    $this->data_add['creator'] = $this->session->userdata('id');
		    $thn_bln = date("Y-m");
		    $this->data_add['id_jurnal_koreksi'] =  $this->get_no($thn_bln);
						
			$result = $this->{$this->models[0]}->insert($this->data_add);
			if ($result) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
				
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => $this->db->_error_message())));
			}
		}
		
		public function delete() {
		    $param = $this->uri->segment(4); // parameter key
		    
		    // additional block
		    
		    // additional where
		    $this->where_add['id_jurnal_koreksi'] = $param;

		    $filter = array('A.id_jurnal_koreksi' => $param);
		    $statusPosting = $this->{$this->models[0]}->getStatusPosting($filter);

		    if($statusPosting[0]['status_posted'] == 1){
		    	$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'Data Gagal Di Hapus ! Data Sudah di Posting')));
		    } else {
		    	$this->{$this->models[0]}->clearAccount(array('id_cd_um' => $param));
		    	$result = $this->{$this->models[0]}->delete($this->where_add);
			    if ($result == 1) {
			        $this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			    } else {
			        $this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'Data Gagal Di Hapus !')));
			    }
		    }
		}
		
		function get_nomor($tipe, $jenis_transaksi,$no)
		{
			$this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->get_nomor($tipe, $jenis_transaksi,$no));
		}
		
		function get_account($id_cd_um)
		{
			$this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->get_account($id_cd_um));
		}
		
		function read_detail($no_transaksi)
		{
			$this->db->where('id_cd_um',$no_transaksi);
			$q	= $this->db->get('detail_akun_transaksi_all a');
			echo json_encode($q->result_array());
		}
		
		function save_detail()
		{
			$data = array (
				'id_cd_um' => $this->input->post('id_cd_um'),
				'account_no' => $this->input->post('no_akun'),
				'nama_akun' => $this->input->post('nama_akun'),
				'debet' => $this->input->post('debet'),
				'kredit' => $this->input->post('kredit'),
				'status' => $this->input->post('status_akun')
			);
			
			return $this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->save_detail($data));
			//echo json_encode($data);
		}
        public function get_jurnal_umum(){
            $data = $this->{$this->models[0]}->get_jurnal_umum();
            $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode( $data ));
        }
        
        private function get_no($thn_bln) {
            $thn_bln_arr = explode('-', $thn_bln);
            
            $no = '000';
            
            $sql = "
				SELECT
                    RIGHT(max(a.id_jurnal_koreksi),4)*1 AS last_seq
				FROM
					tjurnal_koreksi a
				WHERE
					DATE_FORMAT(a.tanggal_create, '%Y-%m') = ?
			";
            $query = $this->db->query($sql, array($thn_bln));
            $row = $query->row_array();
            
            $last_seq = (int)$row['last_seq'];
            $next_seq = $last_seq + 1;
            
            $leng_temp_no = strlen($next_seq);
            $no = 'JK'.implode('', $thn_bln_arr) . substr($no, $leng_temp_no) . $next_seq;
            
            return $no;
            
        }
	}