<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');
	class Detail_akun_jk extends MY_Controller {
		public $models = array('detail_akun_jk');
		
		public function __construct() {
			parent::__construct();
		}

		public function index() {
			$data = array();
			$data['menu'] = $this->model_menu->getAllMenu();

			$this->template->load('template', 'view_detailakun_jk', $data);
		}

		public function read() {
			$trans = $this->{$this->models[0]}->getTypeTrans(array('A.id_jurnal_koreksi' => $this->uri->segment(4)));
			$transaksi = 'JK';
			/*$jenis_transaksi = $trans->jenis_transaksi;
			if($jenis_transaksi == 1){
				$transaksi = 'KM';
			} else if($jenis_transaksi == 2){
				$transaksi = 'BL';
			} else if($jenis_transaksi == 4){
				$transaksi = 'JK';
			}*/
			/*$this->where_add['status'] = $transaksi;
			$this->where_add['transaksi'] = $trans->jurnal;*/
			$filter2 = array('status' => $transaksi, 'transaksi' => $trans->jurnal);
			$this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read($filter2));
			//echo $this->db->last_query();
		}

		public function read_all() {
			$this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read_all());
		}

		public function create() {
			// additional block
			
			// $jenis=$this->uri->segment(5); 
			$this->data_add['id_cd_um']= $this->uri->segment(4); 
			//$this->data_add['status']='JK';

			$trans = $this->{$this->models[0]}->getTypeTrans(array('A.id_jurnal_koreksi' => $this->uri->segment(4)));
			$transaksi = 'JK';
			/*$jenis_transaksi = $trans->jenis_transaksi;
			if($jenis_transaksi == 1){
				$transaksi = 'KM';
			} else if($jenis_transaksi == 2){
				$transaksi = 'BL';
			} else if($jenis_transaksi == 4){
				$transaksi = 'JK';
			}*/
			$this->data_add['status'] = $transaksi;
			//$this->data_add['status'] = $trans->jenis_transaksi;
			$this->data_add['transaksi'] = $trans->jurnal;
			// addtional get
			$result = $this->{$this->models[0]}->insert($this->data_add);
			if ($result == 1) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => $this->db->_error_message())));
			}
		}

		public function update() { 
			// additional block

			// addtional data

			// additional where
			$this->where_add['id_akun']= $this->uri->segment(4); 

			$result = $this->{$this->models[0]}->update($this->where_add, $this->data_add);
			// echo $this->db->last_query();
			if ($result == 1) {
				// echo $this->db->last_query();

				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'Data Gagal Di Update !')));
			}
		}

		public function delete() {
			// additional block

			// additional where
			$this->where_add['id_akun']= $this->uri->segment(4);

			$result = $this->{$this->models[0]}->delete($this->where_add);
			
			if ($result == 1) {

				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'Data Gagal Di Hapus !')));
			}
		}

		public function posting()
		{
			$param = $this->uri->segment(4); 
			$trans = $this->{$this->models[0]}->getTypeTrans(array('A.id_jurnal_koreksi' => $this->uri->segment(4)));
			//$filter = array('A.id_cd_um' => $param, 'A.status' => $trans->jenis_transaksi);
			$filter = array('A.id_cd_um' => $param, 'A.status' => 'JK');
			$balance = $this->{$this->models[0]}->getStatusBalance($filter);
			//echo $balance;
			if($balance['status'] != 0){
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => $balance['msg'])));
			} else {
				$filter = array('A.id_jurnal_koreksi' => $param);
				$data = array('A.status_posted' => 1);
				$this->output->set_content_type('application/json')->set_output(json_encode($this->{$this->models[0]}->setStatusPosting($filter, $data)));
				//continue to trigger update on table tcd_all
			}
		}

		public function getStatusPosting()
		{
			$param = $this->uri->segment(4); 
			$filter = array('A.id_jurnal_koreksi' => $param);
			$this->output->set_content_type('application/json')->set_output(json_encode($this->{$this->models[0]}->getStatusPosting($filter)));
		}

		public function getAccount()
		{
			$param = $this->input->post('q');
			$filter = array();
			$like = array('A.kode_account' => $param, 'A.uraian' => $param);
			$this->output->set_content_type('application/json')->set_output(json_encode($this->{$this->models[0]}->getAccount($filter, $like)));
		}

		/*public function getAccount()
		{
			$filter = array();
			$this->output->set_content_type('application/json')->set_output(json_encode($this->{$this->models[0]}->getAccount($filter)));
		}*/

	}
