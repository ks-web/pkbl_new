<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');
	class Lpj_uang_muka_detail extends MY_Controller {
		public $models = array('lpj_uang_muka_detail');
		
		public function __construct() {
			parent::__construct();
		}

		public function index() {
			$data = array();
			$data['menu'] = $this->model_menu->getAllMenu();

			$this->template->load('template', 'view_lpj_uang_muka_detail', $data);
		}

		public function read($id = null) {
			$this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read(array('id_lpj' => $id)));
		}

		public function read_all($id = null) {
			$this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read_all(array('id_lpj' => $id)));
		}

		public function create() {
			// $param = $this->input->post('param'); // parametter key
// 
			// // additional block
// 
			// // addtional get
			// $result = $this->{$this->models[0]}->insert($this->data_add);
			// if ($result == 1) {
				// $this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			// } else {
				// $this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => $this->db->_error_message())));
			// }
		}

		public function update() {
			$param = $this->uri->segment(4); // parameter key

			// additional block

			// addtional data
			$jumlah_um = $this->input->post('jumlah_harga');
			$jumlah_realisasi = $this->input->post('jumlah_harga_realisasi');
			$this->data_add['selisih_harga'] = $jumlah_um - $jumlah_realisasi;

			// additional where
			$this->where_add['id_detail_lpj'] = $param;

			$result = $this->{$this->models[0]}->update($this->where_add, $this->data_add);
			if ($result == 1) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'Data Gagal Di Update !')));
			}
		}

		public function delete() {
			// $param = $this->uri->segment(4); // parameter key
// 
			// // additional block
// 
			// // additional where
			// $this->where_add['param'] = $param;
// 
			// $result = $this->{$this->models[0]}->delete($this->where_add);
			// if ($result == 1) {
				// $this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			// } else {
				// $this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'Data Gagal Di Hapus !')));
			// }
		}

	}
