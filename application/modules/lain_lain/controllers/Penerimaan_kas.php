<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');
	class Penerimaan_kas extends MY_Controller {
		public $models = array('penerimaan_kas');
		
		public function __construct() {
			parent::__construct();
		}

		public function index() {
			$data = array();
			$data['menu'] = $this->model_menu->getAllMenu();

			$this->template->load('template', 'view_penerimaan_kas', $data);
		}

		public function read() {
			$this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read());
		}

		public function read_all() {
			$this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read_all());
		}

		public function create() {
			//$param = $this->input->post('param'); // parametter key

			// additional block
			$this->data_add['creator'] = $this->session->userdata('idPegawai');
			
			$thn_bln = date("Y-m");
			$this->data_add['id_penerimaan_kas'] =  $this->get_no($thn_bln);

			// addtional get
			$result = $this->{$this->models[0]}->insert($this->data_add);
			if ($result == 1) {
				//$id = $this->{$this->models[0]}->getLastDataMitra();
				//print_r($id);
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => $this->db->_error_message())));
			}
		}

		public function update() {
			$param = $this->uri->segment(4); // parameter key

			// additional block

			// addtional data

			// additional where
			$this->where_add['id_penerimaan_kas'] = $param;

			$result = $this->{$this->models[0]}->update($this->where_add, $this->data_add);
			if ($result == 1) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'Data Gagal Di Update !')));
			}
		}

		public function delete() {
			$param = $this->uri->segment(4); // parameter key

			// additional block

			// additional where
			$this->where_add['id_penerimaan_kas'] = $param;

			$result = $this->{$this->models[0]}->delete($this->where_add);
			if ($result == 1) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'Data Gagal Di Hapus !')));
			}
		}

		public function getMitra()
		{
			$filter = array('A.status' => '6');
			$this->output->set_content_type('application/json')->set_output(json_encode($this->{$this->models[0]}->getMitra($filter)));
		}
		
		private function get_no($thn_bln) {
		    $thn_bln_arr = explode('-', $thn_bln);
		    
		    $no = '000';
		    
		    $sql = "
				SELECT
					RIGHT(max(a.id_penerimaan_kas),4)*1 AS last_seq
				FROM
					tpenerimaan_kas a
				WHERE
					DATE_FORMAT(a.tanggal_create, '%Y-%m') = ?
			";
		    $query = $this->db->query($sql, array($thn_bln));
		    $row = $query->row_array();
		    
		    $last_seq = (int)$row['last_seq'];
		    $next_seq = $last_seq + 1;
		    
		    $leng_temp_no = strlen($next_seq);
		    $no = 'JU'.implode('', $thn_bln_arr) . substr($no, $leng_temp_no) . $next_seq;
		    
		    return $no;
		    
		}

	}
