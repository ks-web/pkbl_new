<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');
	class Lpj_uang_muka extends MY_Controller {
		public $models = array('lpj_uang_muka');

		public function __construct() {
			parent::__construct();
		}

		public function index() {
			$data = array();
			$data['menu'] = $this->model_menu->getAllMenu();

			$this->template->load('template', 'view_lpj_uang_muka', $data);
		}

		public function read() {
			$this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read());
		}

		public function read_all() {
			$this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read_all());
		}

		public function create() {
			// additional block
			$thn_bln = date("Y-m");
			$this->data_add['id_lpj'] = $this->get_no($thn_bln);
			$this->data_add['creator'] = $this->session->userdata('id');
			$this->data_add['tanggal_create'] = date("Y-m-d H:i:s");

			// addtional get
			$result = $this->{$this->models[0]}->insert($this->data_add);
			if ($result == 1) {
				$this->db->select('id_detail_um');
				$this->db->where('id_um', $this->input->post('id_um'));
				$detail = $this->db->get('detail_tuang_muka')->result_array();

				foreach ($detail as $row) {
					$detail_in = array(
						'id_lpj' => $this->data_add['id_lpj'],
						'id_detail_um' => $row['id_detail_um']
					);
					$this->{$this->models[0]}->insertOtherData('detail_lpj_uang_muka', $detail_in);
				}

				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => $this->db->_error_message())));
			}
		}

		public function update() {
			$param = $this->uri->segment(4);
			// parameter key

			// additional block

			// addtional data
			$this->data_add['modifier'] = $this->session->userdata('id');
			$this->data_add['tanggal_modified'] = date("Y-m-d H:i:s");

			// additional where
			$this->where_add['id_lpj'] = $param;

			$result = $this->{$this->models[0]}->update($this->where_add, $this->data_add);
			if ($result == 1) {
				$where = array('id_lpj' => $param);
				$this->{$this->models[0]}->deleteOtherData('detail_lpj_uang_muka', $where);
				
				$this->db->select('id_detail_um');
				$this->db->where('id_um', $this->input->post('id_um'));
				$detail = $this->db->get('detail_tuang_muka')->result_array();

				foreach ($detail as $row) {
					$detail_in = array(
						'id_lpj' => $param,
						'id_detail_um' => $row['id_detail_um']
					);
					$this->{$this->models[0]}->insertOtherData('detail_lpj_uang_muka', $detail_in);
				}
				
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'Data Gagal Di Update !')));
			}
		}

		public function delete() {
			$param = $this->uri->segment(4);
			// parameter key

			// additional block

			// additional where
			$this->where_add['id_lpj'] = $param;

			$result = $this->{$this->models[0]}->delete($this->where_add);
			if ($result == 1) {
				$where = array('id_lpj' => $param);
				$this->{$this->models[0]}->deleteOtherData('detail_lpj_uang_muka', $where);

				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'Data Gagal Di Hapus !')));
			}
		}

		private function get_no($thn_bln) {
			$thn_bln_arr = explode('-', $thn_bln);

			$no = '0000';

			$sql = "
				SELECT
                    RIGHT(max(a.id_lpj),4)*1 AS last_seq
				FROM
					tlpj_uang_muka a
				WHERE
					DATE_FORMAT(a.tanggal_create, '%Y-%m') = ?
			";
			$query = $this->db->query($sql, array($thn_bln));
			$row = $query->row_array();

			$last_seq = (int)$row['last_seq'];
			$next_seq = $last_seq + 1;

			$leng_temp_no = strlen($next_seq);
			$no = 'LPJ' . implode('', $thn_bln_arr) . substr($no, $leng_temp_no) . $next_seq;

			return $no;

		}

		public function get_uang_muka() {
			$q = $this->input->post('q');

			$ret = $this->{$this->models[0]}->get_uang_muka($q);

			echo json_encode($ret);
		}
		
		public function read_preview($id = null) {
			$this->db->where('id_lpj', $id);
			$check = $this->db->get('tlpj_uang_muka_vd');
			if ($check->num_rows() > 0) {

				$data['data_head'] = $check->row_array();
				$data['data_detail'] = null;
				
				$this->db->where('id_lpj', $id);
				$q = $this->db->get('detail_lpj_uang_muka_vd');
				if($q->num_rows() > 0){
					$data['data_detail'] = $q->result_array();
				}

				// kota tanggal
				$data['kota'] = 'Cilegon';
				$this->load->helper('tanggal_indo');
				$this->load->helper('terbilang');
				$data['tanggal'] = tanggal_display();

				$html = $this->load->view('view_lpj_uang_muka_preview', $data, true);

				$this->load->library('dompdf_gen');
				// Load library
				$this->dompdf->set_paper('A4', 'potrait');
				// Setting Paper
				// // Convert to PDF
				$this->dompdf->load_html($html);
				$this->dompdf->render();
				$this->dompdf->stream("Preview_LPJ_Uang_Muka-" . $id . ".pdf");
			} else {
				show_404();
			}
		}

	}
