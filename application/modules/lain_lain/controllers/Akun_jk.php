<?php
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');
	class Akun_jk extends MY_Controller {
		public $models = array('akun_jk');
		
		public function __construct() {
			parent::__construct();
		}

		public function index() {
			$data = array();
			$data['menu'] = $this->model_menu->getAllMenu();

			$this->template->load('template', 'view_akun_jk', $data);
		}

		public function read() {
			$this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read());
		}

		public function read_all() {
			$this->output->set_content_type('application/json')->set_output($this->{$this->models[0]}->read_all());
		}

		public function create() {
			// additional block
			
			// $jenis=$this->uri->segment(5); 
			$this->data_add['id_cd_um']= $this->uri->segment(4); 
			$this->data_add['status']=$this->uri->segment(5);
			// addtional get
			$result = $this->{$this->models[0]}->insert($this->data_add);
			if ($result == 1) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => $this->db->_error_message())));
			}
		}

		public function update() { 
			// additional block

			// addtional data

			// additional where
			$this->where_add['id_akun']= $this->uri->segment(4); 

			$result = $this->{$this->models[0]}->update($this->where_add, $this->data_add);
			// echo $this->db->last_query();
			if ($result == 1) {
				// echo $this->db->last_query();

				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'Data Gagal Di Update !')));
			}
		}

		public function delete() {
			// additional block

			// additional where
			$this->where_add['id_akun']= $this->uri->segment(4);

			$result = $this->{$this->models[0]}->delete($this->where_add);
			
			if ($result == 1) {

				$this->output->set_content_type('application/json')->set_output(json_encode(array('success' => true)));
			} else {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('msg' => 'Data Gagal Di Hapus !')));
			}
		}

		public function getAccount()
		{
			$filter = array();
			$this->output->set_content_type('application/json')->set_output(json_encode($this->{$this->models[0]}->getAccount($filter)));
		}

	}
