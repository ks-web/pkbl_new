<?php
$app = 'Pencairan Dana Umum'; // nama aplikasi
$module = 'lain_lain';
$appLink = 'pencairan_dana_umum'; // controller
$idField  = 'id_cd_umum'; //field key table
?>

<script>
	var url;
	var app = "<?=$app?>";
	var appLink = '<?=$appLink?>';
	var module = '<?=$module?>';
	var idField = '<?=$idField?>';
	
    function add(){
        $('#dlg').dialog('open').dialog('setTitle','Tambah ' + app);
		$('#fm').form('clear');
	    $('#tbl').html('Save');
		url = '<?=base_url($module . '/' . $appLink . '/create')?>';
    }
    function edit(){
        var row = $('#dg').datagrid('getSelected');
		if (row){
			$('#tbl').html('Simpan');
			$('#dlg').dialog('open').dialog('setTitle','Edit '+ app);
			$('#fm').form('load',row);
			url = '<?=base_url($module . '/' . $appLink . '/update')?>/'+row[idField];
	    }
    }
    function hapus(){
        var row = $('#dg').datagrid('getSelected');		
	    if (row){
			$.messager.confirm('Confirm','Apakah Anda Yakin Akan Menghapus ini?',function(r){
				if (r){
					$.post('<?=base_url($module . '/' . $appLink . '/delete')?>/'+row[idField],function(result){
						if (result.success){
							$('#dg').datagrid('reload');	// reload the user data
						} else {
							$.messager.show({	// show error message
								title: 'Error',
								msg:result.msg
							});
						}
					},'json');
				}
			});
		}
    }
    function save(){
	   $('#fm').form('submit',{
	    	url: url,
	    	onSubmit: function(){
	    		return $(this).form('validate');
	    	},
	    	success: function(result){
	    		var result = eval('('+result+')');
	    		if (result.success){
	    			$('#dlg').dialog('close');		
	    			$('#dg').datagrid('reload');
	                
	    		} else {
	             $.messager.alert('Error Message',result.msg,'error');
	    		}
	    	}
	    });
	}
	function doSearch(value){
	    $('#dg').datagrid('load',{    
	    	q:value  
	    });
	}

	function reset_form(){
		$('#nama').val('');
        $('#jumlah').numberbox('reset');
	}

	function repopulate_form(obj){
		console.log(obj);
		$('#nama').val(obj.nama_pegawai);
        $('#jumlah').numberbox('setValue',obj.jumlah);
	}

	function noChange(){
		reset_form();
	}

	function cariExec(){
		var no_trans = $('#no_trans').val();

		$.ajax({
			type : 'POST',
			url : '<?=base_url($module . '/' . $appLink . '/search_no')?>',
			data: {no_trans: no_trans},
			cache : false,
			success : function(result) {
				if(result){
					repopulate_form(result);
                }
				else {
					$.messager.alert('Not Found','Data tidak ditemukan','info');
					reset_form();
				}
			}
		});
	}

	function action(value,row,index){
		var result = '<a href="javascript:void(0)" title="Daftar Akun" class="btn btn-small btn-default" onclick="javascript:editAccount(\''+row.id_cd_umum+'\')"><i class="icon-tasks icon-large"></i></a> ';

		return result;
	}
	
	function editAccount(id){
		$('#dlgx').dialog('open').dialog('setTitle','Edit '+ app);
			idParent = id;
			reload1();
	}
	function preview(){
	    var row = $('#dg').datagrid('getSelected');
	    url  = '<?=base_url($module . '/' . $appLink . '/previewPencairanDana')?>/'+row[idField];
	    if (row)
	    {
	        window.open(url,'_blank');
	    }
	}
</script>
 
<div class="tabs-container">                
	<table id="dg" class="easyui-datagrid" 
	data-options="  
	    url: '<?=base_url($module . '/' . $appLink . '/read')?>',
		singleSelect:'true', 
	    title:'<?=$app?>',
	    toolbar:'#toolbar',
	    iconCls:'icon-cog',
	    rownumbers:'true',  
	    idField:'<?=$idField?>', 
	    pagination:'true',
	    fitColumns:'true',
	    pageList: [10,20,30]"
	>
	    <thead>
	        <th field="id_cd_umum" width="100" sortable="true">No Transaksi</th>
	        <th field="nama_pegawai" width="200" sortable="true">Nama</th>
            <th field="pemilik" width="200" sortable="true">Pemilik</th>
            <th field="kode_bayar" width="200" sortable="true">Kode Bayar</th>
            <th field="action" width="200" sortable="true" formatter="action">Action</th>
	    </thead>
	</table>
	
	<!-- Model Start -->
	<div id="dlg" class="easyui-dialog" style="width:400px; height:auto; padding:10px" closed="true" buttons="#t_dlg_dis-buttons" >
	    <form id="fm" method="post" enctype="multipart/form-data" action="">
	        <table width="100%" align="center" border="0">
	            <tr>
	                <td>Nomor Transaksi</td>
	                <td>:</td>
	                <td>
	                	<input type="hidden" name="id_cd_umum" id="id_cd_umum">
	                    <input type="text" name="no_trans" id="no_trans" required style="width: 120px;" onchange="noChange()">
	                    <a href="#" id="btn-cari" class="easyui-linkbutton" onclick="cariExec()">Cari</a>
	                </td>
	            </tr>
	            <tr>
	                <td>Nama</td>
	                <td>:</td>
	                <td>
	                    <input id="nama" name="nama" type="text" style="width: 200px;" readonly>
	                </td>
	            </tr>
	            <tr>
	                <td>Jumlah</td>
	                <td>:</td>
	                <td>
	                    <input class="easyui-numberbox" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. '" name="jumlah" id="jumlah" style="width: 200px;" readonly>
	                </td>
	            </tr>
	            <tr>
	                <td colspan="3"><hr/></td>
	            </tr>
	            <tr>
	                <td>Pemilik</td>
	                <td>:</td>
	                <td>
	                    <input class="easyui-textbox" id="pemilik" name="pemilik" style="width:200px" required>
	                </td>
	            </tr>
	            <tr>
	                <td>Alamat</td>
	                <td>:</td>
	                <td>
	                    <textarea name="alamat" id="alamat" style="width:200px;height: 70px;" required></textarea>
	                </td>
	            </tr>
	            <tr>
	                <td>Kode Bayar</td>
	                <td>:</td>
	                <td>
	                    <select  class="easyui-combobox" type="text" name="kode_bayar" id="kode_bayar" height="auto" required data-options="panelHeight: 'auto'">
	                    	<option value="Transfer">Transfer</option>
	                    	<!--<option value="Cheque">Cheque</option>-->
	                    </select>
	                </td>
	            </tr>
	        </table>
	    </form>
	   
	   	<!-- Tombol Add, Edit, Hapus Datagrid -->
       	<div id="toolbar">  
            <table align="center" style="padding: 0px; width: 99%;">
                <tr>
                    <td>
                        <a href="javascript:void(0)" class="btn btn-small btn-success" onclick="javascript:add()"><i class="icon-plus-sign icon-large"></i>&nbsp;Add</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:edit()"><i class="icon-edit"></i>&nbsp;Edit</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-danger" onclick="javascript:hapus()"><i class="icon-trash icon-large"></i>&nbsp;Delete</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-warning" iconCls: 'icon-search' onclick="javascript:preview()"><i class="icon-search icon-large"></i>&nbsp;Preview</a>
                    </td>
                    <td>&nbsp;</td>
                    <td align="right">
                    	<input class="easyui-searchbox" prompt="Silahkan Input Field" searcher="doSearch"  style="width:300px"/>
                    </td>
                </tr>
            </table>  
        </div>  
        <!-- end tombol datagrid -->
        <!-- Tombol simpan & Batal Form -->
        <div id="t_dlg_dis-buttons">
            <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:save()"><i class="icon-save icon-large"></i>&nbsp;Simpan</a>
            <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:$('#dlg').dialog('close')"><i class="icon-remove icon-large"></i>&nbsp;Batal</a>      
        </div>
	</div>
	<!-- Model end -->
	
	<div id="dlgx" class="easyui-dialog" style="width:800px; height:auto; padding:10px" closed="true" buttons="#t_dlg_dis-buttonsx" >
		<?php $this->load->view('view_pencairan_dana_umum_detail'); ?>
		<!-- Tombol simpan & Batal Form -->
        <div id="t_dlg_dis-buttonsx">
            <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:$('#dlgx').dialog('close')"><i class="icon-remove icon-large"></i>&nbsp;Batal</a>      
        </div>
	</div>
	<!-- Model end -->
</div>