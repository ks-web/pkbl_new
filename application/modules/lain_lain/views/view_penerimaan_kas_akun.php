<?php
$app1 = 'Penerimaan Kas Akun'; // nama aplikasi
$module1 = 'lain_lain';
$appLink1 = 'penerimaan_kas_akun'; // controller
$idField1  = 'id_akun'; //field key table
?>

<script>
	var url1;
	var app1 = "<?=$app1?>";
	var appLink1 = '<?=$appLink1?>';
	var module1 = '<?=$module1?>';
	var idField1 = '<?=$idField1?>';
	//var idParent = 1;
	
    function add1(){
        $('#dlg1').dialog('open').dialog('setTitle','Tambah ' + app1);
		$('#fm1').form('clear');
	    $('#tbl1').html('Save');
		url1 = '<?=base_url($module1 . '/' . $appLink1 . '/create')?>/'+idParent;
    }
    function edit1(){
        var row = $('#dg1').datagrid('getSelected');
		if (row){
			$('#tbl1').html('Simpan');
			$('#dlg1').dialog('open').dialog('setTitle','Edit '+ app1);
			$('#fm1').form('load',row);
			url1 = '<?=base_url($module1 . '/' . $appLink1 . '/update')?>/'+row[idField1];
	    }
    }
    function hapus1(){
        var row = $('#dg1').datagrid('getSelected');		
	    if (row){
			$.messager.confirm('Confirm','Apakah Anda Yakin Akan Menghapus ini?',function(r){
				if (r){
					$.post('<?=base_url($module1 . '/' . $appLink1 . '/delete')?>/'+row[idField1],function(result){
						if (result.success){
							$('#dg1').datagrid('reload');	// reload the user data
							$('#dg1').datagrid('reload');
						} else {
							$.messager.show({	// show error message
								title: 'Error',
								msg:result.msg
							});
						}
					},'json');
				}
			});
		}
    }
    function save1(){
    	//alert(idParent);
    	console.log($("#fm1").serialize());
	   $('#fm1').form('submit',{
	    	url: url1,
	    	onSubmit: function(){
	    		return $(this).form('validate');
	    	},
	    	success: function(result){
	    		var result = eval('('+result+')');
	    		if (result.success){
	    			$('#dlg1').dialog('close');		
	    			reload1();
	    			//$('#dg1').datagrid('reload');
	                
	    		} else {
	             $.messager.alert('Error Message',result.msg,'error');
	    		}
	    	}
	    });
	}
	function posting1(){
		$('#dlg-date-posted').dialog('open');
    }
    
    function posting2(){
        var date_posted = $('#date_posted').datebox('getValue');
        if(date_posted){
        	$.messager.confirm('Confirm','Apakah Anda Yakin Akan Melakukan Posting?',function(r){
        		if (r){
        			$.post('<?=base_url($module1 . '/' . $appLink1 . '/posting')?>/'+idParent,{date_posted: date_posted},function(result){
        				if (result.success){
        					$('.btnOptions1').hide();
        					$('.btnOptions2').show();
        					$('#dlg-date-posted').dialog('close');
        					$('#dg').datagrid('reload');	// reload the user data
        					//$('#dg1').datagrid('reload');
        				} else {
        					$.messager.show({	// show error message
        						title: 'Error',
        						msg:result.msg
        					});
        				}
        			},'json');
        		}
        	}); 
        }
        else {
        	 $.messager.alert('Info','Tolong pilih tanggal!','info');
        }
    	
    }
    
	function doSearch1(value){
	    $('#dg1').datagrid('load',{    
	    	q:value  
	    });
	}
	function reload1(){
		$.post( '<?=base_url($module1 . '/' . $appLink1 . '/getStatusPosting/')?>/'+idParent )
  		.done(function( data ) {
  			console.log(data[0]['status_posted']);
    		if(data[0]['status_posted'] == 1){
				$('.btnOptions1').hide();
				$('.btnOptions2').show();
			} else {
				$('.btnOptions1').show();
				$('.btnOptions2').hide();
			}
    		$('#dg1').datagrid({    
		    	url: '<?=base_url($module1 . '/' . $appLink1 . '/read/')?>/'+idParent  
		    });
  		});
	}
	/*function reload1(){
		$('#dg1').datagrid({    
	    	url: '<?=base_url($module1 . '/' . $appLink1 . '/read/')?>/'+idParent  
	    });
	}*/
</script>
 
<div class="tabs-container">                
	<table id="dg1" class="easyui-datagrid" 
	data-options="  
	    url: '<?=base_url($module1 . '/' . $appLink1 . '/read/')?>',
		singleSelect:'true', 
	    title:'<?=$app1?>',
	    toolbar:'#toolbar1',
	    iconCls:'icon-cog',
	    rownumbers:'true',  
	    idField:'<?=$idField1?>', 
	    pagination:'true',
	    fitColumns:'true',
	    pageList: [10,20,30]
	    "
	>
	    <thead>
	    	<!-- <th field="id_asset" width=100" sortable="true">ID Asset</th> -->
	        <th field="account_no" width=100" sortable="true">Account No</th>
			<th field="nama_akun" width="100" sortable="true">Nama Akun</th>
			<th field="debet" width="100" sortable="true" formatter="format_numberdisp" align="right">Debet</th>
			<th field="kredit" width=100" sortable="true" formatter="format_numberdisp" align="right">Credit</th>
	    </thead>
	</table>
	
	<!-- Model Start -->
	<div id="dlg1" class="easyui-dialog" style="width:500px; height:auto; padding:10px" closed="true" buttons="#t_dlg_dis-buttons1" >
	    <form id="fm1" method="post" enctype="multipart/form-data" action="">
	       <table width="100%" align="center" border="0">
	            <tr>
	                <td>Account</td>
	                <td>:</td>
	                <td><input name="account_no" id="account_no"  class="easyui-combogrid" required style="width: 203px"
						data-options="panelHeight:200, panelWidth: 700,
						valueField:'',
						idField:'kode_account',
						textField:'kode_account',
						mode:'remote',
						url:'<?=base_url($module1 . '/' . $appLink1 . '/getAccount') ?>',
						onSelect :function(indek,row)
						{
							$('#nama_akun').val(row.uraian);
						},
						columns:[[
                       		{field:'kode_account',title:'Account',width:80},
                        	{field:'uraian',title:'Uraian',width:150}
                        ]]
						" >
	                </td>
	            </tr>
	            <tr>
	                <td>Nama Akun</td>
	                <td>:</td>
	                <td>
	                    <input type="text" name="nama_akun" class="easyui-textbox" id="nama_akun" readonly="readonly" required>
	                </td>
	            </tr>
	            <tr>
	                <td>Debet</td>
	                <td>:</td>
	                <td>
	                    <input class="easyui-numberbox" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%',onChange:function(a, b){ if(a != 0){$('#kredit').numberbox('setValue', 0)} }" name="debet" id="debet" required style="width: 200px;">
	                </td>
	            </tr>
	             <tr>
	                <td>Credit</td>
	                <td>:</td>
	                <td>
	                    <input class="easyui-numberbox" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%',onChange:function(a, b){ if(a != 0){$('#debet').numberbox('setValue', 0)} }" name="kredit" id="kredit" required style="width: 200px;">
	                </td>
	            </tr>            
	        </table>
	    </form>
	   
	   	<!-- Tombol Add, Edit, Hapus Datagrid -->
       	<div id="toolbar1">  
            <table align="center" style="padding: 0px; width: 99%;">
                <tr>
                    <td>
                        <a href="javascript:void(0)" class="btn btn-small btn-success btnOptions1" onclick="javascript:add1()"><i class="icon-plus-sign icon-large"></i>&nbsp;Add</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-primary btnOptions1" onclick="javascript:edit1()"><i class="icon-edit"></i>&nbsp;Edit</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-danger btnOptions1" onclick="javascript:hapus1()"><i class="icon-trash icon-large"></i>&nbsp;Delete</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-primary btnOptions1" onclick="javascript:posting1()"><i class="icon-check icon-large"></i>&nbsp;Posting</span></a>
                        <a href="javascript:void(0)" class="btn btn-small btn-success btnOptions2"><i class="icon-check icon-large" data-options="disabled:true"></i>&nbsp;Posted</span></a>
                    </td>
                    <td>&nbsp;</td>
                    <td align="right">
                    	<input class="easyui-searchbox" prompt="Silahkan Input Field" searcher="doSearch1"  style="width:100px"/>
                    </td>
                </tr>
            </table>  
        </div>
        <div id="dlg-date-posted" class="easyui-dialog" title="Posting Dialog" closed="true" data-options="
                iconCls: 'icon-save',
                buttons: '#dlg-buttons-date-posted'
            " style="width:400px;height:200px;padding:10px">
            Post date : <input id="date_posted" type="text" class="easyui-datebox">
        </div>
        <div id="dlg-buttons-date-posted">
            <a href="javascript:void(0)" class="easyui-linkbutton" onclick="javascript:posting2()">Post</a>
            <a href="javascript:void(0)" class="easyui-linkbutton" onclick="javascript:$('#dlg-date-posted').dialog('close')">Close</a>
        </div>
        <!-- end tombol datagrid -->
        <!-- Tombol simpan & Batal Form -->
        <div id="t_dlg_dis-buttons1">
            <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:save1()"><i class="icon-save icon-large"></i>&nbsp;Simpan</a>
            <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:$('#dlg1').dialog('close')"><i class="icon-remove icon-large"></i>&nbsp;Batal</a>      
        </div>
	</div>
	<!-- Model end -->
</div>