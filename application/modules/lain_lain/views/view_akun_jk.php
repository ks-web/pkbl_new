<?php
$app5 = 'Tambah Akun Jurnal Koreksi'; // nama aplikasi
$module5 = 'lain_lain';
$appLink5= 'Akun_jk'; // controller
$idField5  = 'id_akun'; //field key table
?>

<script>
	var url5;
	var app5 = "<?=$app5?>";
	var appLink5 = '<?=$appLink5?>';
	var module5 = '<?=$module5?>';
	var idField5 = '<?=$idField5?>';
	
	
    function add5(){
        $('#dlg5').dialog('open').dialog('setTitle','Tambah ' + app5);
		$('#fm5').form('clear');
	    $('#tbl5').html('Save5');
		url5 = '<?=base_url($module5 . '/' . $appLink5 . '/create')?>/'+idParent+'/'+idParent2;
    }
    function edit5(){
        var row = $('#dg5').datagrid('getSelected');
		if (row){
			$('#tbl5').html('Simpan5');
			$('#dlg5').dialog('open').dialog('setTitle','Edit '+ app5);
			$('#fm5').form('load',row);
			url5 = '<?=base_url($module5 . '/' . $appLink5 . '/update')?>/'+row[idField5];
	    }
    }
    function hapus5(){
        var row = $('#dg5').datagrid('getSelected');		
	    if (row){
			$.messager.confirm('Confirm','Apakah Anda Yakin Akan Menghapus ini?',function(r){
				if (r){
					$.post('<?=base_url($module5 . '/' . $appLink5 . '/delete')?>/'+row[idField5],function(result){
						if (result.success){
							$('#dg5').datagrid('reload');	// reload the user data
							$('#dg5').datagrid('reload');
						} else {
							$.messager.show({	// show error message
								title: 'Error',
								msg:result.msg
							});
						}
					},'json');
				}
			});
		}
    }
    function save5(){
	   // $('#fm5').form('submit',{
	   //  	url: url5,
	   //  	onSubmit: function(){
	   //  		return $(this).form('validate');
	   //  	},
	   //  	success: function(result){
	   //  		var result = eval('('+result+')');
	   //  		if (result.success){
	   //  			$('#dlg5').dialog('close');		
	   //  			$('#dg5').datagrid('reload');
	                
	   //  		} else {
	   //           $.messager.alert('Error Message',result.msg,'error');
	   //  		}
	   //  	}
	   //  });
	   console.log($('#fm5x').serialize());
	   $.post(url5,$('#fm5x').serialize())
	   .done(function(data){
	   	$('#dlg5').dialog('close');		
	    $('#dg5').datagrid('reload');

	   });
	}
	function doSearch5(value){
	    $('#dg5').datagrid('load',{    
	    	q:value  
	    });
	}
	function reload5(value){
		$('#dg5').datagrid({    
	    	url: '<?=base_url($module5 . '/' . $appLink5 . '/read/')?>/'+idParent  
	    });
	}
</script>
 
<div class="tabs-container">                
	<table id="dg5" class="easyui-datagrid" 
	data-options="  
	    url: '<?=base_url($module5 . '/' . $appLink5 . '/read')?>',
		singleSelect:'true', 
	    title:'<?=$app5?>',
	    toolbar:'#toolbar5',
	    iconCls:'icon-cog',
	    rownumbers:'true',  
	    idField:'<?=$idField5?>', 
	    pagination:'true',
	    fitColumns:'true',
	    pageList: [10,20,30]
	    "
	>
	    <thead>
	    <tr>
	    	<th field="account_no" width=100" sortable="true">Account No</th>
			<th field="debet" width="100" sortable="true">Debet</th>
			<th field="kredit" width=100" sortable="true">Credit</th>
		</tr>
		</thead>
	</table>
	
	<!-- Model Start -->
	<div id="dlg5" class="easyui-dialog" style="width:500px; height:auto; padding:10px" closed="true" buttons="#t_dlg_dis-buttons5" >
	    <form id="fm5x" method="post" enctype="multipart/form-data" action="">
	        <table width="100%" align="center" border="0">
	            <tr>
	               <td>Account</td>
	                <td>:</td>
	                <td><input name="account_no" id="account_no"  class="easyui-combogrid" required style="width: 203px"
						data-options="panelHeight:200, panelWidth: 700,
						valueField:'',
						idField:'account',
						textField:'account',
						mode:'remote',
						url:'<?=base_url($module5 . '/' . $appLink5 . '/getAccount') ?>',
						onSelect :function(indek,row)

						{
						
						$('#nama_akunx').val(row.uraian);
						alert(row.uraian);
						},
						columns:[[
                       		{field:'account',title:'Account',width:80},
                        	{field:'uraian',title:'Uraian',width:150}
                        ]]

						" >
	                </td>
	            </tr>
	            <tr>
	            	<td>Nama Akun</td>
	            	<td>:</td>
	            	<td><input type="text" name="nama_akun" id="nama_akunx"></td>
	            </tr>
	           <tr>
	               <td>Debet</td>
	                <td>:</td>
	                <td>
	                    <input class="easyui-numberbox" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%',onChange:function(a, b){ if(a != 0){$('#kredit').numberbox('setValue', 0)} }" name="debet" id="debet" required style="width: 200px;">
	                </td>
	            </tr>
	           <tr>
	               <td>Credit</td>
	                <td>:</td>
	                <td>
	                    <input class="easyui-numberbox" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%',onChange:function(a, b){ if(a != 0){$('#debet').numberbox('setValue', 0)} }" name="kredit" id="kredit" required style="width: 200px;">
	                </td>
	            </tr>
	               
	        </table>
	    </form>
	   
	   	<!-- Tombol Add, Edit, Hapus Datagrid -->
       	<div id="toolbar5">  
            <table align="center" style="padding: 0px; width: 99%;">
                <tr>
                    <td>
                        <a href="javascript:void(0)" class="btn btn-small btn-success" onclick="javascript:add5()"><i class="icon-plus-sign icon-large"></i>&nbsp;Tambah</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:edit5()"><i class="icon-edit"></i>&nbsp;Ubah</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-danger" onclick="javascript:hapus5()"><i class="icon-trash icon-large"></i>&nbsp;Hapus</a>
                    </td>
                    <td>&nbsp;</td>
                    <td align="right">
                    	<input class="easyui-searchbox" prompt="Silahkan Input Field" searcher="doSearch5"  style="width:300px"/>
                    </td>
                </tr>
            </table>  
        </div>  
        <!-- end tombol datagrid -->
        <!-- Tombol simpan & Batal Form -->
        <div id="t_dlg_dis-buttons5">
            <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:save5()"><i class="icon-save icon-large"></i>&nbsp;Simpan</a>
            <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:$('#dlg5').dialog('close')"><i class="icon-remove icon-large"></i>&nbsp;Batal</a>      
        </div>
	</div>
	<!-- Model end -->
</div>