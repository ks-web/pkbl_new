<?php
$app = 'LPJ Uang Muka'; // nama aplikasi
$module = 'lain_lain';
$appLink = 'lpj_uang_muka'; // controller
$idField  = 'id_lpj'; //field key table
?>

<script>
	var url;
	var app = "<?=$app?>";
	var appLink = '<?=$appLink?>';
	var module = '<?=$module?>';
	var idField = '<?=$idField?>';
	var idParent;
	
    function add(){
        $('#dlg').dialog('open').dialog('setTitle','Tambah ' + app);
		$('#fm').form('clear');
	    $('#tbl').html('Save');
		url = '<?=base_url($module . '/' . $appLink . '/create')?>';
    }
    function edit(){
        var row = $('#dg').datagrid('getSelected');
		if (row){
			$('#tbl').html('Simpan');
			$('#dlg').dialog('open').dialog('setTitle','Edit '+ app);
			$('#fm').form('load',row);
			url = '<?=base_url($module . '/' . $appLink . '/update')?>/'+row[idField];
	    }
    }
    function hapus(){
        var row = $('#dg').datagrid('getSelected');		
	    if (row){
			$.messager.confirm('Confirm','Apakah Anda Yakin Akan Menghapus ini?',function(r){
				if (r){
					$.post('<?=base_url($module . '/' . $appLink . '/delete')?>/'+row[idField],function(result){
						if (result.success){
							$('#dg').datagrid('reload');	// reload the user data
							$('#id_um').combogrid('grid').datagrid('reload');
						} else {
							$.messager.show({	// show error message
								title: 'Error',
								msg:result.msg
							});
						}
					},'json');
				}
			});
		}
    }
    function save(){
	   $('#fm').form('submit',{
	    	url: url,
	    	onSubmit: function(){
	    		return $(this).form('validate');
	    	},
	    	success: function(result){
	    		var result = eval('('+result+')');
	    		if (result.success){
	    			$('#dlg').dialog('close');		
	    			$('#dg').datagrid('reload');
	    			$('#id_um').combogrid('grid').datagrid('reload');
	                
	    		} else {
	             $.messager.alert('Error Message',result.msg,'error');
	    		}
	    	}
	    });
	}
	function doSearch(value){
	    $('#dg').datagrid('load',{    
	    	q:value  
	    });
	}
	function action(value,row,index){
		return '<a href="javascript:void(0)" class="btn btn-small btn-success" onclick="javascript:action_detail('+index+')">&nbsp;Detail</a>'
	}
	
	function action_detail(idx){
		var row = $('#dg').datagrid('getRows')[idx];
		idParent = row.id_lpj;
		// console.log(row);
		$('#dlg-detail').dialog('open').dialog('setTitle','Detail ' + app);
		dg1();
	}
	
	function preview(){
		var row = $('#dg').datagrid('getSelected');
		if (row){
			window.open('<?=base_url($module . '/' . $appLink . '/read_preview')?>/'+row[idField], '_blank');
	    }
    }
</script>
 
<div class="tabs-container">                
	<table id="dg" class="easyui-datagrid" 
	data-options="  
	    url: '<?=base_url($module . '/' . $appLink . '/read')?>',
		singleSelect:'true', 
	    title:'<?=$app?>',
	    toolbar:'#toolbar',
	    iconCls:'icon-cog',
	    rownumbers:'true',  
	    idField:'<?=$idField?>', 
	    pagination:'true',
	    fitColumns:'true',
	    pageList: [10,20,30]
	    "
	>
	    <thead>
	        <th field="id_lpj" width="170" sortable="true">No LPJ Uang Muka</th>
	        <th field="id_um" width="150" sortable="true">No Uang Muka</th>
            <th field="tanggal_create_disp" width="150" sortable="true">Tanggal LPJ</th>
            <th field="nilai_uang_muka" formatter="format_numberdisp" align="right" width="200" sortable="true">Nilai Uang Muka</th>
            <th field="nilai_lpj" formatter="format_numberdisp" align="right" width="200" sortable="true">Nilai LPJ</th>
            <th field="nilai_selisih" formatter="format_numberdisp" align="right" width="200" sortable="true">Selisih Nilai</th>
            <th field="action" width="100" formatter="action">Action</th>
	    </thead>
	</table>
	
	<!-- Model Start -->
	<div id="dlg" class="easyui-dialog" style="width:400px; height:350px; padding:10px" closed="true" buttons="#t_dlg_dis-buttons" >
	    <form id="fm" method="post" enctype="multipart/form-data" action="">
	        <table width="100%" align="center" border="0">
	            <tr>
	                <td>No Uang Muka</td>
	                <td>:</td>
	                <td>
	                	<input name="id_um" id="id_um"  class="easyui-combogrid" required style="width: 200px"
						data-options="panelHeight:200, panelWidth: 400,
						url: '<?=base_url($module . '/' . $appLink . '/get_uang_muka') ?>',
						valueField:'',
						idField:'id_um',
						textField:'id_um',
						mode:'remote',
						onSelect :function(indek,row)
						{
							$('#keperluan').val(row.keperluan);
							$('#nilai').val(row.nilai);
							$('#jwaktu1').val(row.jwaktu1_disp);
							$('#jwaktu2').val(row.jwaktu2_disp);
						},
						columns:[[
                       		{field:'id_um',title:'No Uang Muka',width:100},
                        	{field:'nilai',title:'Nilai',width:100},
                        	{field:'no_bl',title:'No BL',width:150}
                        ]]"/>
	                </td>
	            </tr>
	            <tr>
	                <td>Keperluan</td>
	                <td>:</td>
	                <td>
	                    <input type="text" id="keperluan" style="width: 250px;" readonly>
	                </td>
	            </tr>
	            <tr>
	                <td>Nilai</td>
	                <td>:</td>
	                <td>
	                    <input type="text" id="nilai" style="width: 250px;" readonly>
	                </td>
	            </tr>
	            <tr>
	                <td>Jangka Waktu</td>
	                <td>:</td>
	                <td>
	                    <input type="text" id="jwaktu1" style="width: 100px;" readonly>
	                </td>
	            </tr>
	            <tr>
	                <td>Sampai Dengan</td>
	                <td>:</td>
	                <td>
	                    <input type="text" id="jwaktu2" style="width: 100px;" readonly>
	                </td>
	            </tr>
	            <tr>
	            	<td colspan="3"><hr /></td>
	            </tr>
	            <tr>
	                <td>Telah Digunakan</td>
	                <td>:</td>
	                <td>
	                    <input type="text" id="keterangan_digunakan" name="keterangan_digunakan" required style="width: 250px;">
	                </td>
	            </tr>
	        </table>
	    </form>
	   
	   	<!-- Tombol Add, Edit, Hapus Datagrid -->
       	<div id="toolbar">  
            <table align="center" style="padding: 0px; width: 99%;">
                <tr>
                    <td>
                        <a href="javascript:void(0)" class="btn btn-small btn-success" onclick="javascript:add()"><i class="icon-plus-sign icon-large"></i>&nbsp;Add</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:edit()"><i class="icon-edit"></i>&nbsp;Edit</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-danger" onclick="javascript:hapus()"><i class="icon-trash icon-large"></i>&nbsp;Delete</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:preview()">&nbsp;Preview</a>
                    </td>
                    <td>&nbsp;</td>
                    <td align="right">
                    	<input class="easyui-searchbox" prompt="Silahkan Input Field" searcher="doSearch"  style="width:300px"/>
                    </td>
                </tr>
            </table>  
        </div>  
        <!-- end tombol datagrid -->
        <!-- Tombol simpan & Batal Form -->
        <div id="t_dlg_dis-buttons">
            <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:save()"><i class="icon-save icon-large"></i>&nbsp;Simpan</a>
            <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:$('#dlg').dialog('close')"><i class="icon-remove icon-large"></i>&nbsp;Batal</a>      
        </div>
        
        <div id="dlg-detail" class="easyui-dialog" style="width:600px; height:400px; padding:10px" closed="true">
		<?php $this->load->view('view_lpj_uang_muka_detail'); ?>
	</div>
	</div>
	<!-- Model end -->
</div>