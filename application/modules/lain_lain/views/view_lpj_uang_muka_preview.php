<!DOCTYPE html>
<html>
	<head>
		<style>
			table {
				border-spacing: 0px;
				border-collapse: separate;
				font-size: 12px;
				page-break-inside: avoid;
			}

			table, th, td {
				border: 0px solid black;
			}
			html {
				margin: 30px 50px
			}
			.bordered, .bordered td {
				border: 1px solid black;
				font-size: 12px;
			}
			
			.bordered-catat{
				border: 1px solid black;
				font-size: 12px;
			}
			td.no_border_bot {
				border-top: 0px solid black;
				border-bottom: 0px solid black;
			}
			.test td {
				border: 1px solid black;
			}

			.pull-right {
				text-align: right;
			}
			.center {
				text-align: center;
			}

			.bold {
				font-weight: bold;
			}
			.corporate {
				color: red;
				font-size: 24px;
			}
			.corporate_sub {
				font-size: 12px;
			}
			.red {
				color: red;
			}
		</style>
	</head>
	<body>
		<table style="width: 100%;">
			<tbody>
				<tr>
					<td>
					<table style="width: 100%;">
						<tr>
							<td width="70px"><img  src='<?=base_url('assets/images/logo_KS.png') ?>' style="width: 50%;"></td>
							<td><span class="corporate bold">PT. KRAKATAU STEEL</span>
							<br />
							<span class="corporate_sub bold">DIVISI COMMUNITY DEVELOPMENT</span></td>
							<td width="250px"><span class="bold"><u>PERTANGGUNG JAWABAN UANG MUKA</u></span>
							<br />
							<span class="bold">NOMOR PERT. JWB &nbsp;&nbsp;&nbsp;: <?=$data_head['id_lpj']?></span><br />
							<span class="bold">NOMOR PERMT. U/M : &nbsp;<?=$data_head['id_um']?></span></td>
						</tr>
					</table>
					<br />
					<br />
					<table style="width: 100%;">
						<tr>
							<td width="240px">Uang muka telah di pergunakan untuk keperluan</td>
							<td width="5px">:</td>
							<td class="bold"><?=$data_head['keterangan_digunakan']?></td>
						</tr>
					</table>
					<br />
					<br />
					Perincian pertanggung jawaban sbb :
					<br />
					<table width="100%" class="bordered">
						<tr class="bold">
							<td class="center" rowspan="2" width="30px">No.</td>
							<td rowspan="2">Keteragan / Uraian</td>
							<td class="center" width="120px">Pertanggung Jawaban<br />Uang Muka</td>
							<td class="center" width="120px">Data Permintaan<br />Uang Muka</td>
							<td class="center" width="120px">Selisih<br />Lebih/Kurang</td>
						</tr>
						<tr class="bold">
							<td class="center">Rupiah</td>
							<td class="center">Rupiah</td>
							<td class="center">Rupiah</td>
						</tr>
						<?php
							$no = 1;
							$total_jumlah_harga_realisasi = 0;
							$total_jumlah_harga = 0;
							$total_selisih_harga = 0;
							
							foreach ($data_detail as $row) {
								$total_jumlah_harga_realisasi += $row['jumlah_harga_realisasi'];
								$total_jumlah_harga += $row['jumlah_harga'];
								$total_selisih_harga += $row['selisih_harga'];
								?>
						<tr >
							<td class="center no_border_bot"><?=$no?></td>
							<td class="no_border_bot"><?=$row['keterangan']?></td>
							<td class="pull-right no_border_bot"><?=number_format($row['jumlah_harga_realisasi'], 0,",",".")?></td>
							<td class="pull-right no_border_bot"><?=number_format($row['jumlah_harga'], 0,",",".")?></td>
							<td class="pull-right no_border_bot"><?=number_format($row['selisih_harga'], 0,",",".")?></td>
						</tr>
								<?php
								$no++;
							}
						?>
						<tr >
							<td class="center no_border_bot"><br /></td>
							<td class="no_border_bot"></td>
							<td class="pull-right no_border_bot"></td>
							<td class="pull-right no_border_bot"></td>
							<td class="pull-right no_border_bot"></td>
						</tr>
						<tr class="bold">
							<td class="pull-right" colspan="2">Jumlah Rp. </td>
							<td class="pull-right "><?=number_format($total_jumlah_harga_realisasi, 0,",",".")?></td>
							<td class="pull-right "><?=number_format($total_jumlah_harga, 0,",",".")?></td>
							<td class="pull-right "><?=number_format($total_selisih_harga, 0,",",".")?></td>
						</tr>
					</table>
					<br />
					<table width="100%">
						<tr class="bold">
							<td width="100px"><br /></td>
							<td width="300px">Total nilai pertanggung jawaban</td>
							<td width="10px">:</td>
							<td>Rp. <?=number_format($data_head['nilai_lpj'], 0,",",".")?></td>
						</tr>
						<tr class="bold">
							<td width="100px"><br /></td>
							<td width="300px">Total permintaan uang muka</td>
							<td width="10px">:</td>
							<td>Rp. <?=number_format($data_head['nilai_uang_muka'], 0,",",".")?></td>
						</tr>
						<tr class="bold">
							<td width="100px"><br /></td>
							<td width="300px">Sisa / (Kurang)</td>
							<td width="10px">:</td>
							<td>Rp. <?=number_format($data_head['nilai_selisih'], 0,",",".")?></td>
						</tr>
					</table>
					<br />
					<hr  style="border: 1px solid black;"/>
					<table style="width: 100%;">
						<tbody>
							<tr>
								<td class="center bold"><u>KEUANGAN PKBL</u></td>
								<td width="200px" class="center"></td>
								<td width="200px" class="center"><strong><?=$kota ?>,
								<?=tanggal_display()?></strong></td>
							</tr>
							<tr>
								<td rowspan="4">
									<table width="100%" class="test">
										<tr class="bold">
											<td class="center">VERIVIED.<br /><br /><br /></td>
											<td class="center">APPROVED.<br /><br /><br /></td>
										</tr>
									</table>
								</td>
								<td width="200px" class="center bold">MENYETUJUI</td>
								<td width="200px" class="center bold">PEMOHON</td>
							</tr>
							<tr>
								<td width="200px" class="center">
								<br />
								<br />
								</td>
								<td width="200px" class="center">
								<br />
								<br />
								</td>
							</tr>
							<tr>
								<td width="200px" class="center"><strong><u><?=managerName?></u></strong></td>
								<td width="200px" class="center"><strong><u><?=$data_head['creator_name']?></u></strong></td>
							</tr>
							<tr>
								<td width="200px" class="center bold">Manager</td>
								<td width="200px" class="center"></td>
							</tr>
						</tbody>
					</table></td>
				</tr>
			</tbody>
		</table>
	</body>
</html>
