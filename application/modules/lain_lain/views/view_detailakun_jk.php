<?php
$app6 = 'Tambah Akun Jurnal Koreksi'; // nama aplikasi
$module6 = 'lain_lain';
$appLink6= 'Detail_akun_jk'; // controller
$idField6  = 'id_akun'; //field key table
?>

<script>
	var url6;
	var app6 = "<?=$app6?>";
	var appLink6 = '<?=$appLink6?>';
	var module6 = '<?=$module6?>';
	var idField6 = '<?=$idField6?>';
	
	
    function add6(){
        $('#dlg6').dialog('open').dialog('setTitle','Tambah ' + app6);
		$('#fm6x').form('clear');
	    $('#tbl6').html('Save6');
		url6 = '<?=base_url($module6 . '/' . $appLink6 . '/create')?>/'+idParentX;
    }
    function edit6(){
        var row = $('#dg6').datagrid('getSelected');
		if (row){
			$('#tbl6').html('Simpan6');
			$('#dlg6').dialog('open').dialog('setTitle','Edit '+ app6);
			$('#fm6x').form('load',row);
			url6 = '<?=base_url($module6 . '/' . $appLink6 . '/update')?>/'+row[idField6];
	    }
    }
    function hapus6(){
        var row = $('#dg6').datagrid('getSelected');		
	    if (row){
			$.messager.confirm('Confirm','Apakah Anda Yakin Akan Menghapus ini?',function(r){
				if (r){
					$.post('<?=base_url($module6 . '/' . $appLink6 . '/delete')?>/'+row[idField6],function(result){
						if (result.success){
							$('#dg6').datagrid('reload');	// reload the user data
							$('#dg6').datagrid('reload');
						} else {
							$.messager.show({	// show error message
								title: 'Error',
								msg:result.msg
							});
						}
					},'json');
				}
			});
		}
    }
    function save6(){
	   // $('#fm6').form('submit',{
	   //  	url: url6,
	   //  	onSubmit: function(){
	   //  		return $(this).form('validate');
	   //  	},
	   //  	success: function(result){
	   //  		var result = eval('('+result+')');
	   //  		if (result.success){
	   //  			$('#dlg6').dialog('close');		
	   //  			$('#dg6').datagrid('reload');
	                
	   //  		} else {
	   //           $.messager.alert('Error Message',result.msg,'error');
	   //  		}
	   //  	}
	   //  });
	   console.log($('#fm6x').serialize());
	   $.post(url6,$('#fm6x').serialize())
	   .done(function(data){
	   	$('#dlg6').dialog('close');		
	    $('#dg6').datagrid('reload');

	   });
	}
	function posting6(){
    	$.messager.confirm('Confirm','Apakah Anda Yakin Akan Melakukan Posting?',function(r){
			if (r){
				$.post('<?=base_url($module6 . '/' . $appLink6 . '/posting')?>/'+idParentX,function(result){
					if (result.success){
						$('.btnOptions1').hide();
						$('.btnOptions2').show();
						//$('#dg').datagrid('reload');	// reload the user data
						//$('#dg1').datagrid('reload');
					} else {
						$.messager.show({	// show error message
							title: 'Error',
							msg:result.msg
						});
					}
				},'json');
			}
		});
    }
	function doSearch6(value){
	    $('#dg6').datagrid('load',{    
	    	q:value  
	    });
	}
	function reload6(){
		$.post( '<?=base_url($module6 . '/' . $appLink6 . '/getStatusPosting/')?>/'+idParentX )
  		.done(function( data ) {
  			console.log(data[0]['status_posted']);
    		if(data[0]['status_posted'] == 1){
				$('.btnOptions1').hide();
				$('.btnOptions2').show();
			} else {
				$('.btnOptions1').show();
				$('.btnOptions2').hide();
			}
    		$('#dg6').datagrid({    
		    	url: '<?=base_url($module6 . '/' . $appLink6 . '/read/')?>/'+idParentX  
		    });
  		});
	}
	/*function reload6(value){
		$('#dg6').datagrid({    
	    	url: '<?=base_url($module6 . '/' . $appLink6 . '/read/')?>/'+idParent3  
	    });
	}*/
</script>
 
<div class="tabs-container">                
	<table id="dg6" class="easyui-datagrid" 
	data-options="  
	    url: '<?=base_url($module6 . '/' . $appLink6 . '/read')?>',
		singleSelect:'true', 
	    title:'<?=$app6?>',
	    toolbar:'#toolbar6',
	    iconCls:'icon-cog',
	    rownumbers:'true',  
	    idField:'<?=$idField6?>', 
	    pagination:'true',
	    fitColumns:'true',
	    pageList: [10,20,30]
	    "
	>
	    <thead>
	    <th field="account_no" width=100" sortable="true">Account No</th>
			<th field="nama_akun" width="100" sortable="true">Nama Akun</th>
			<th field="debet" width="100" sortable="true" formatter="format_numberdisp" align="right">Debet</th>
			<th field="kredit" width=100" sortable="true" formatter="format_numberdisp" align="right">Credit</th>
		</thead>
	</table>
	
	<!-- Model Start -->
	<div id="dlg6" class="easyui-dialog" style="width:500px; height:auto; padding:10px" closed="true" buttons="#t_dlg_dis-buttons6" >
	    <form id="fm6x" method="post" enctype="multipart/form-data" action="">
	        <table width="100%" align="center" border="0">
	            <tr>
	               <td>Account</td>
	                <td>:</td>
	                <td><input name="account_no" id="account_no"  class="easyui-combogrid" required style="width: 203px"
						data-options="panelHeight:200, panelWidth: 700,
						valueField:'',
						idField:'kode_account',
						textField:'kode_account',
						mode:'remote',
						url:'<?=base_url($module6 . '/' . $appLink6 . '/getAccount') ?>',
						onSelect :function(indek,row)
						{
							$('#nama_akuny').val(row.uraian);
						},
						columns:[[
                       		{field:'kode_account',title:'Account',width:80},
                        	{field:'uraian',title:'Uraian',width:150}
                        ]]
						" >
	                </td>
	            </tr>
	            <tr>
	            	<td>Nama Akun</td>
	            	<td>:</td>
	            	<td><input style="width: 100px;" type="text" name="nama_akun" id="nama_akuny"></td>
	            </tr>
	           <tr>
	               <td>Debet</td>
	                <td>:</td>
	                <td>
	                    <input class="easyui-numberbox" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%',onChange:function(a, b){ if(a != 0){$('#kredit').numberbox('setValue', 0)} }" name="debet" id="debet" required style="width: 200px;">
	                </td>
	            </tr>
	           <tr>
	               <td>Credit</td>
	                <td>:</td>
	                <td>
	                    <input class="easyui-numberbox" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%',onChange:function(a, b){ if(a != 0){$('#debet').numberbox('setValue', 0)} }" name="kredit" id="kredit" required style="width: 200px;">
	                </td>
	            </tr>
	               
	        </table>
	    </form>
	   
	   	<!-- Tombol Add, Edit, Hapus Datagrid -->
       	<div id="toolbar6">  
            <table align="center" style="padding: 0px; width: 99%;">
                <tr>
                    <td>
                        <a href="javascript:void(0)" class="btn btn-small btn-success btnOptions1" onclick="javascript:add6()"><i class="icon-plus-sign icon-large"></i>&nbsp;Add</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-primary btnOptions1" onclick="javascript:edit6()"><i class="icon-edit"></i>&nbsp;Edit</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-danger btnOptions1" onclick="javascript:hapus6()"><i class="icon-trash icon-large"></i>&nbsp;Delete</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-primary btnOptions1" onclick="javascript:posting6()"><i class="icon-check icon-large"></i>&nbsp;Posting</span></a>
                        <a href="javascript:void(0)" class="btn btn-small btn-success btnOptions2"><i class="icon-check icon-large" data-options="disabled:true"></i>&nbsp;Posted</span></a>
                    </td>
                    <td>&nbsp;</td>
                    <td align="right">
                    	<input class="easyui-searchbox" prompt="Silahkan Input Field" searcher="doSearch6"  style="width:150px"/>
                    </td>
                </tr>
            </table>  
        </div>  
        <!-- end tombol datagrid -->
        <!-- Tombol simpan & Batal Form -->
        <div id="t_dlg_dis-buttons6">
            <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:save6()"><i class="icon-save icon-large"></i>&nbsp;Simpan</a>
            <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:$('#dlg6').dialog('close')"><i class="icon-remove icon-large"></i>&nbsp;Batal</a>      
        </div>
	</div>
	<!-- Model end -->
</div>