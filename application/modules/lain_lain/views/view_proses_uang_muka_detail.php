<?php
$app1 = 'Proses Uang Muka Detail'; // nama aplikasi
$module1 = 'lain_lain';
$appLink1 = 'proses_uang_muka_detail'; // controller
$idField1  = 'id_detail_um'; //field key table
?>

<script>
	var url1;
	var app1 = "<?=$app1?>";
	var appLink1 = '<?=$appLink1?>';
	var module1 = '<?=$module1?>';
	var idField1 = '<?=$idField1?>';
	
    function add1(){
        $('#dlg1').dialog('open').dialog('setTitle','Tambah ' + app1);
		$('#fm1').form('clear');
	    $('#tbl1').html('Save');
		url1 = '<?=base_url($module1 . '/' . $appLink1 . '/create')?>';
		$('#idParent').val(idParent);
    }
    function edit1(){
        var row = $('#dg1').datagrid('getSelected');
		if (row){
			$('#tbl1').html('Simpan');
			$('#dlg1').dialog('open').dialog('setTitle','Edit '+ app1);
			$('#fm1').form('load',row);
			url1 = '<?=base_url($module1 . '/' . $appLink1 . '/update')?>/'+row[idField1];
			$('#idParent').val(idParent);
	    }
    }
    function hapus1(){
        var row = $('#dg1').datagrid('getSelected');		
	    if (row){
			$.messager.confirm('Confirm','Apakah Anda Yakin Akan Menghapus ini?',function(r){
				if (r){
					$.post('<?=base_url($module1 . '/' . $appLink1 . '/delete')?>/'+row[idField1],function(result){
						if (result.success){
							$('#dg1').datagrid('reload');	// reload the user data
						} else {
							$.messager.show({	// show error message
								title: 'Error',
								msg:result.msg
							});
						}
					},'json');
				}
			});
		}
    }
    function save1(){
	   $('#fm1').form('submit',{
	    	url: url1,
	    	onSubmit: function(){
	    		return $(this).form('validate');
	    	},
	    	success: function(result){
	    		var result = eval('('+result+')');
	    		if (result.success){
	    			$('#dlg1').dialog('close');		
	    			$('#dg1').datagrid('reload');
	                
	    		} else {
	             $.messager.alert('Error Message',result.msg,'error');
	    		}
	    	}
	    });
	}
	function doSearch1(value){
	    $('#dg1').datagrid('load',{    
	    	q:value  
	    });
	}
	function calc(){
		var jumlah_satuan = $('#jumlah_satuan').numberbox('getValue');
		var harga_satuan = $('#harga_satuan').numberbox('getValue');
		
		var total = jumlah_satuan * harga_satuan;
		$('#jumlah_harga').numberbox('setValue', total);
	}
	function dg1(){
		$('#dg1').datagrid({url: '<?=base_url($module1 . '/' . $appLink1 . '/read')?>/' + idParent})
	}
</script>
 
<div class="tabs-container">                
	<table id="dg1" class="easyui-datagrid" 
	data-options="
		singleSelect:'true', 
	    title:'<?=$app1?>',
	    toolbar:'#toolbar1',
	    iconCls:'icon-cog',
	    rownumbers:'true',  
	    idField:'<?=$idField1?>', 
	    pagination:'true',
	    fitColumns:'true',
	    pageList: [10,20,30]
	    "
	>
	    <thead>
	        <th field="keterangan" width="200" sortable="true">Keterangan</th>
	        <th field="jumlah_satuan" width="120" formatter="format_numberdisp" align="right" sortable="true">Jumlah Satuan</th>
            <th field="harga_satuan" width="120" formatter="format_numberdisp" align="right" sortable="true">Harga Satuan</th>
            <th field="jumlah_harga" width="120" formatter="format_numberdisp" align="right" sortable="true">Jumlah Harga</th>
	    </thead>
	</table>
	
	<!-- Model Start -->
	<div id="dlg1" class="easyui-dialog" style="width:400px; height:250px; padding:10px" closed="true" buttons="#t_dlg_dis-buttons1" >
	    <form id="fm1" method="post" enctype="multipart/form-data" action="">
	        <table width="100%" align="center" border="0">
	            <tr>
	                <td>Keterangan</td>
	                <td>:</td>
	                <td>
	                    <textarea name="keterangan" id="keterangan"></textarea>
	                    <input type="hidden" id="idParent" name="id_um" />
	                </td>
	            </tr>
	            <tr>
	                <td>Jumlah Satuan</td>
	                <td>:</td>
	                <td>
	                    <input class="easyui-numberbox" data-options="onChange: function(){calc()},precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'',width:'100%'" name="jumlah_satuan" id="jumlah_satuan" required style="width: 120px;">
	                </td>
	            </tr>
	            <tr>
	                <td>Harga Satuan</td>
	                <td>:</td>
	                <td>
	                    <input class="easyui-numberbox" data-options="onChange: function(){calc()},precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%'" name="harga_satuan" id="harga_satuan" required style="width: 120px;">
	                </td>
	            </tr>
	            <tr>
	                <td>Jumlah Harga</td>
	                <td>:</td>
	                <td>
	                    <input class="easyui-numberbox" data-options="precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%'" name="jumlah_harga" id="jumlah_harga" required style="width: 120px;" readonly>
	                </td>
	            </tr>
	        </table>
	    </form>
	   
	   	<!-- Tombol Add, Edit, Hapus Datagrid -->
       	<div id="toolbar1">  
            <table align="center" style="padding: 0px; width: 99%;">
                <tr>
                    <td>
                        <a href="javascript:void(0)" class="btn btn-small btn-success" onclick="javascript:add1()"><i class="icon-plus-sign icon-large"></i>&nbsp;Add</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:edit1()"><i class="icon-edit"></i>&nbsp;Edit</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-danger" onclick="javascript:hapus1()"><i class="icon-trash icon-large"></i>&nbsp;Delete</a>
                    </td>
                    <td>&nbsp;</td>
                    <td align="right">
                    	<input class="easyui-searchbox" prompt="Silahkan Input Field" searcher="doSearch1"  style="width:300px"/>
                    </td>
                </tr>
            </table>  
        </div>  
        <!-- end tombol datagrid -->
        <!-- Tombol simpan & Batal Form -->
        <div id="t_dlg_dis-buttons1">
            <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:save1()"><i class="icon-save icon-large"></i>&nbsp;Simpan</a>
            <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:$('#dlg1').dialog('close')"><i class="icon-remove icon-large"></i>&nbsp;Batal</a>      
        </div>
	</div>
	<!-- Model end -->
</div>