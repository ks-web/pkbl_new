<!DOCTYPE html>
<html>
	<head>
		<style>
			table {
				border-spacing: 0px;
				border-collapse: separate;
				font-size: 12px;
				page-break-inside: avoid;
			}

			table, th, td {
				border: 0px solid black;
			}
			html {
				margin: 30px 50px
			}
			.bordered, .bordered td {
				border: 1px solid black;
				font-size: 12px;
			}
			
			.bordered-catat{
				border: 1px solid black;
				font-size: 12px;
			}
			td.no_border_bot {
				border-top: 0px solid black;
				border-bottom: 0px solid black;
			}

			.pull-right {
				text-align: right;
			}
			.center {
				text-align: center;
			}

			.bold {
				font-weight: bold;
			}
			.corporate {
				color: red;
				font-size: 24px;
			}
			.corporate_sub {
				font-size: 12px;
			}
			.red {
				color: red;
			}
		</style>
	</head>
	<body>
		<table style="width: 100%;">
			<tbody>
				<tr>
					<td>
					<table style="width: 100%;">
						<tr>
							<td width="70px"><img  src='<?=base_url('assets/images/logo_KS.png') ?>' style="width: 50%;"></td>
							<td><span class="corporate bold">PT. KRAKATAU STEEL</span>
							<br />
							<span class="corporate_sub bold">DIVISI COMMUNITY DEVELOPMENT</span></td>
							<td width="200px"><span class="bold"><u>PERMINTAAN UANG MUKA</u></span>
							<br />
							<span class="bold">NOMOR: <?=$data_head['id_um']?></span></td>
						</tr>
					</table>
					<br />
					<br />
					<table style="width: 100%;">
						<tr>
							<td>Harap dapat diberikan uang muka sebesar : <span class="bold">Rp. <?=number_format($data_head['nilai'], 0,",",".")?></span></td>
						</tr>
					</table>
					<br />
					<table style="width: 100%;">
						<tr>
							<td width="120px"><strong>Terbilang</strong> : </td>
							<td class="background-dotted" style="height: 40px;"><span class="bold" style="font-size: 16px;"><?=terbilang_display($data_head['nilai'],3) ?> Rupiah</span></td>
						</tr>
						<tr>
							<td width="120px">Untuk keperluan : </td>
							<td><?=$data_head['keperluan']?></td>
						</tr>
					</table>
					<br />
					<br />
					<table style="width: 100%;">
						<tr>
							<td rowspan="3" width="280px" style="vertical-align: top;"><span class="red">Uang muka ini ditempuh karena hal-hal sbb : </span>
							<br />
							<span class="red">( Sesuai SK No. 316/KMK.016/1994 )</span></td>
							<td>1. ................................................................</td>
						</tr>
						<tr>
							<td>2. ................................................................</td>
						</tr>
						<tr>
							<td>3. ................................................................</td>
						</tr>
					</table>
					<br />
					Periode pemakaian uang muka : Tgl. <span class="bold"><?=tanggal_display($data_head['jwaktu1'])?> s/d <?=tanggal_display($data_head['jwaktu2'])?></span>
					<br />
					Perincian Permintaan uang muka sbb:
					<br />
					<table width="100%" class="bordered">
						<tr class="bold">
							<td class="center" width="30px">No.</td>
							<td>Keteragan/Uraian</td>
							<td class="center" width="70px">Banyaknya Satuan</td>
							<td class="center" width="120px">Harga Satuan
							<br />
							(Rp.)</td>
							<td class="center" width="120px">Jumlah Harga
							<br />
							(Rp.)</td>
						</tr>
						<?php
						if(count($data_detail) >0){
							$no = 1;
							foreach ($data_detail as $row) {
								?>
						<tr >
							<td class="center no_border_bot"><?=$no?></td>
							<td class="no_border_bot"><?=$row['keterangan']?></td>
							<td class="center no_border_bot"><?=number_format($row['jumlah_satuan'], 0,",",".")?></td>
							<td class="pull-right no_border_bot"><?=number_format($row['harga_satuan'], 0,",",".")?></td>
							<td class="pull-right no_border_bot"><?=number_format($row['jumlah_harga'], 0,",",".")?></td>
						</tr>
								<?php
								$no++;
							}}
						?>
						<tr >
							<td class="center no_border_bot"><br /></td>
							<td class="no_border_bot"></td>
							<td class="center no_border_bot"></td>
							<td class="pull-right no_border_bot"></td>
							<td class="pull-right no_border_bot"></td>
						</tr>
					</table>
					<br />
					<table width="100%">
						<tr class="bold">
							<td class="pull-right">Total = </td><td class="pull-right" width="120px"><?=number_format($data_head['nilai'], 0,",",".")?></td>
						</tr>
					</table>
					<br />
					<hr  style="border: 1px solid black;"/>
					<table style="width: 100%;">
						<tbody>
							<tr>
								<td width="200px" class="center"></td>
								<td></td>
								<td width="200px" class="center"><strong><?=$kota ?>,
								<?=tanggal_display()?></strong></td>
							</tr>
							<tr>
								<td width="200px" class="center bold">MENYETUJUI</td>
								<td rowspan="4">
									<!-- <table style="width: 100%;" class="bordered-catat">
										<tr><td><br />
											Catatan.<br />
											1. Persetujuan uang muka hanya ditempuh jika satu-satunya jalan
											2. kolom-kolom diatas harus
										</td></tr>
									</table> -->
								</td>
								<td width="200px" class="center bold">PEMOHON</td>
							</tr>
							<tr>
								<td width="200px" class="center">
								<br />
								<br />
								</td>
								<td width="200px" class="center">
								<br />
								<br />
								</td>
							</tr>
							<tr>
								<td width="200px" class="center"><strong><u><?=managerName?></u></strong></td>
								<td width="200px" class="center"><strong><u><?=$data_head['creator_name']?></u></strong></td>
							</tr>
							<tr>
								<td width="200px" class="center bold">Manager</td>
								<td width="200px" class="center"></td>
							</tr>
						</tbody>
					</table></td>
				</tr>
			</tbody>
		</table>
	</body>
</html>
