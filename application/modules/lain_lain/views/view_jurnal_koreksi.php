<?php
$app 		= 'Jurnal Koreksi'; // nama aplikasi
$module 	= 'lain_lain';
$appLink 	= 'jurnal_koreksi'; // controller
$idField  	= 'id_jurnal_koreksi'; //field key table
?>

<script>
	var url;
	var app 	= '<?=$app?>';
	var appLink = '<?=$appLink?>';
	var module 	= '<?=$module?>';
	var idField = '<?=$idField?>';
	var idParent;
	var idParentX;
	
    function add(){
        $('#dlg').dialog('open').dialog('setTitle','Tambah ' + app);
		$('#fm').form('clear');
	    $('#tbl').html('Save');
		url = '<?=base_url($module . '/' . $appLink . '/create')?>';
    }
    function edit(){
        var row = $('#dg').datagrid('getSelected');
		if (row){
			$('#tbl').html('Simpan');
			$('#dlg').dialog('open').dialog('setTitle','Edit '+ app);
			$('#fm').form('load',row);
			url = '<?=base_url($module . '/' . $appLink . '/update')?>/'+row[idField];
	    }
    }
    function hapus(){
        var row = $('#dg').datagrid('getSelected');		
	    if (row){
			$.messager.confirm('Confirm','Apakah Anda Yakin Akan Menghapus ini?',function(r){
				if (r){
					$.post('<?=base_url($module . '/' . $appLink . '/delete')?>/'+row[idField],function(result){
						if (result.success){
							$('#dg').datagrid('reload');	// reload the user data
							$('#id_um').combogrid('grid').datagrid('reload');
						} else {
							$.messager.show({	// show error message
								title: 'Error',
								msg:result.msg
							});
						}
					},'json');
				}
			});
		}
    }
    function save(){
	   $('#fm').form('submit',{
	    	url: url,
	    	onSubmit: function(){
	    		return $(this).form('validate');
	    	},
	    	success: function(result){
	    		var result = eval('('+result+')');
	    		if (result.success){
	    			$('#dlg').dialog('close');		
	    			$('#dg').datagrid('reload');
	    			$('#id_um').combogrid('grid').datagrid('reload');
	                
	    		} else {
	             $.messager.alert('Error Message',result.msg,'error');
	    		}
	    	}
	    });
	}
	
	function action(value,row,index)
	{
		return '<center><a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:detailX('+index+')">Detail</a></center>';
	}
	
	function detailX(index)
	{
		$('#dg').datagrid('selectRow', index);
		var row = $('#dg').datagrid('getSelected');
		

		if(row){
			idParentX = row.id_jurnal_koreksi;
		
    		$('#dlg-detail').dialog('open').dialog('setTitle','Tambah Detail '+ app);
    		$('#fm-detail').form('load',row);
    		idParent3=row.no_transaksi;
    		reload6();
    		reload7(row);
		}
	}

	function doSearch(value){
	    $('#dg').datagrid('load',{
	    	q:value  
	    });
	}

	function cariExec(){
		var jenis_transaksi = $('#jenis_transaksi').combobox('getValue');
		var tipe = $('#tipe_disp').combobox('getValue');
		var no = $('#nomor').val();

		if(jenis_transaksi == 4){
			tipe = '0';
		}

		$.ajax({
			type : 'POST',
			url : '<?=base_url($module . '/' . $appLink . '/get_nomor')?>/'+tipe+'/'+jenis_transaksi+'/'+no,
			cache : false,
			success : function(result) {
				if(result){
				 $('#nama_pemohon').val(result.nama_pemohon);
                 $('#nilai').numberbox('setValue',result.nilai_usulan);
                }
				else {
					$.messager.alert('Not Found','Data tidak ditemukan','info');
					$('#nama_pemohon').val('');
	                $('#nilai').numberbox('reset');
				}
			}
		});
	}	

	function noChange(){
		$('#nama_pemohon').val('');
        $('#nilai').numberbox('reset');
	}

	function jenis_transaksi_format(value,row,index)
	{
		var ret = '';
		if(value == '1'){
			ret = "Kemitraan";
		}
		else if(value == '2'){
			ret = "Bina Lingkungan";
		} 
		else if(value == '4'){
			ret = "Jurnal Umum";
		}
		return ret;
	}
</script>
 
<div class="tabs-container">                
	<table id="dg" class="easyui-datagrid" 
	data-options="
	    url: '<?=base_url($module . '/' . $appLink . '/read')?>',
		singleSelect:'true',
	    title:'<?=$app?>',
	    toolbar:'#toolbar',
	    iconCls:'icon-cog',
	    rownumbers:'true',
	    idField:'<?=$idField?>',
	    pagination:'true',
	    fitColumns:'true',
	    pageList: [10,20,30]
	    ">
			
	    <thead>
	        <th field="no_transaksi" width="170" sortable="true">No Jurnal Koreksi</th>
	        <th field="tanggal_create" width="150" sortable="true">Tanggal Koreksi</th>
            <th field="nilai" width="150" sortable="true" formatter="format_numberdisp" align="right">Nilai</th>
            <th field="jenis_transaksi"  width="200" sortable="true" formatter="jenis_transaksi_format">Transaksi</th>
            <!-- <th field="jurnal"  width="200" sortable="true">Jurnal</th> -->
            <th field="keterangan_koreksi"   width="200" sortable="true">Keterangan Koreksi</th>
            <th field="action" width="100" formatter="action" >Action</th>
	    </thead>
	</table>
	
	<!-- Model Start -->
	<div id="dlg" class="easyui-dialog" style="width:500px; height:400px; padding:10px" closed="true" buttons="#t_dlg_dis-buttons">
	    <form id="fm" method="post" enctype="multipart/form-data" action="">
	        <table width="100%" align="center" border="0">
	            <tr>
					<td>Transaksi</td>
					<td>:</td>
					<td >
						<select class="easyui-combobox"  name="jenis_transaksi" id="jenis_transaksi" label="sektor" style="width:254px" data-options="
							valueField : 'id',
							textField : 'text',
							onChange: function(new_val,old_val){                                                
                                if(new_val == 4){
                                    $('.is_not_jurnal_umum').hide();
                                }else{
                                    $('.is_not_jurnal_umum').show();
                                }
                            }
							
						">
							<option value="1">Kemitraan</option>
							<option value="2">Bina Lingkungan</option>
							<option value="4">Jurnal Umum</option>
						</select>
					</td>
				</tr>
				<tr class="is_not_jurnal_umum" style="display: none;">
					<td>Tipe</td>
					<td>:</td>
					<td>
						<input class="easyui-textbox"  name="id_realisasi" id="tipe" style="width: 246px;" hidden readonly>
						<select class="easyui-combobox" name="tipe" id="tipe_disp" label="tipe" style="width:254px">
							<option value="1">CD</option>
							<option value="2">CR</option>
						</select>
					</td>
				</tr>
				<tr class="">
					<td>No</td>
					<td>:</td>
					<td>
						<input class="easyui-textbox"  name="id_realisasi" id="id_realisasi" style="width: 246px;" hidden readonly>
						<input class="easyui-textbox"  name="nomor" id="nomor" style="width: 246px;" onchange="noChange()">
						<a href="#" id="btn-cari" class="easyui-linkbutton" onclick="cariExec()">Cari</a>
					</td>
				</tr>
	            <tr class="">
	                <td>Nama Pemohon</td>
	                <td>:</td>
	                <td>
	                    <input type="text" id="nama_pemohon" style="width: 250px;" >
	                </td>
	            </tr>
				<tr class="">
	                <td>Nilai</td>
	                <td>:</td>
	                <td>
	                    <input class="easyui-numberbox" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%'"  id="nilai" name="nilai" style="width: 250px;" >
	                </td>
	            </tr>
	            <tr>
	            	<td colspan="3"><hr></td>
	            </tr>
	            <!--  <tr>
	                <td>Nilai Koreksi</td>
	                <td>:</td>
	                <td>
	                    <input class="easyui-numberbox" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%'" id="nilai_koreksi" name="nilai_koreksi" required style="width: 250px;" >
	                </td>
	            </tr>-->
	            <tr>
	                <td>Keterangan</td>
	                <td>:</td>
	                <td>
	                    <textarea id="keterangan_koreksi" name="keterangan_koreksi" rows="4" cols="50" required></textarea>
	                </td>
	            </tr>
	           
	        </table>
	    </form>
	   
	   	<!-- Tombol Add, Edit, Hapus Datagrid -->
       		<div id="toolbar">  
            <table align="center" style="padding: 0px; width: 99%;">
                <tr>
                    <td>
                        <a href="javascript:void(0)" class="btn btn-small btn-success" onclick="javascript:add()"><i class="icon-plus-sign icon-large"></i>&nbsp;Add</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:edit()"><i class="icon-edit"></i>&nbsp;Edit</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-danger" onclick="javascript:hapus()"><i class="icon-trash icon-large"></i>&nbsp;Delete</a>
                       
                    </td>
                    <td>&nbsp;</td>
                    <td align="right">
                    	<input class="easyui-searchbox" prompt="Silahkan Input Field" searcher="doSearch"  style="width:300px"/>
                    </td>
                </tr>
            </table>  
        </div>  
        <!-- end tombol datagrid -->
        <!-- Tombol simpan & Batal Form -->
        <div id="t_dlg_dis-buttons">
            <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:save()"><i class="icon-save icon-large"></i>&nbsp;Simpan</a>
            <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:$('#dlg').dialog('close')"><i class="icon-remove icon-large"></i>&nbsp;Batal</a>      
        </div>
	</div>
	
	<div id="dlg-detail" class="easyui-dialog" style="width:600px; height:500px; padding:10px" closed="true" buttons="#t_dlg_dis-buttons" >
		<form id="fm-detail" method="post" enctype="multipart/form-data" action="">
			
			<table width="100%" align="center" border="0">
				Nilai koreksi : <input type="text" class="easyui-numberbox" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%',disabled:'true'" id="nilai" name="nilai" style="width: 150px;" >
			</table>
			
		</form>
		<?php $this->load->view('view_detailakun_jk_history'); ?>
		<hr />
		<?php $this->load->view('view_detailakun_jk'); ?>
		
		
    
	<div id="t_dlg_dis-buttons">
        <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:$('#dlg-detail').dialog('close')"><i class="icon-remove icon-large"></i>&nbsp;Batal</a>
    </div>
	</div> 
</div>