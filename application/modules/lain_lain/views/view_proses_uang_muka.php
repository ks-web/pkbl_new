<?php
$app = 'Proses Uang Muka'; // nama aplikasi
$module = 'lain_lain';
$appLink = 'proses_uang_muka'; // controller
$idField  = 'id_um'; //field key table
?>

<script>
	var url;
	var app = "<?=$app?>";
	var appLink = '<?=$appLink?>';
	var module = '<?=$module?>';
	var idField = '<?=$idField?>';
	var idParent;
	
    function add(){
        $('#dlg').dialog('open').dialog('setTitle','Tambah ' + app);
		$('#fm').form('clear');
	    $('#tbl').html('Save');
		url = '<?=base_url($module . '/' . $appLink . '/create')?>';
    }
    function edit(){
        var row = $('#dg').datagrid('getSelected');
		if (row){
			$('#tbl').html('Simpan');
			$('#dlg').dialog('open').dialog('setTitle','Edit '+ app);
			$('#fm').form('load',row);
			url = '<?=base_url($module . '/' . $appLink . '/update')?>/'+row[idField];
	    }
    }
    function hapus(){
        var row = $('#dg').datagrid('getSelected');		
	    if (row){
			$.messager.confirm('Confirm','Apakah Anda Yakin Akan Menghapus ini?',function(r){
				if (r){
					$.post('<?=base_url($module . '/' . $appLink . '/delete')?>/'+row[idField],function(result){
						if (result.success){
							$('#dg').datagrid('reload');	// reload the user data
						} else {
							$.messager.show({	// show error message
								title: 'Error',
								msg:result.msg
							});
						}
					},'json');
				}
			});
		}
    }
    function save(){
	   $('#fm').form('submit',{
	    	url: url,
	    	onSubmit: function(){
	    		return $(this).form('validate');
	    	},
	    	success: function(result){
	    		var result = eval('('+result+')');
	    		if (result.success){
	    			$('#dlg').dialog('close');		
	    			$('#dg').datagrid('reload');
	                
	    		} else {
	             $.messager.alert('Error Message',result.msg,'error');
	    		}
	    	}
	    });
	}
	function doSearch(value){
	    $('#dg').datagrid('load',{    
	    	q:value  
	    });
	}
	function doSearch_detail(value){
	    $('#dg-detail').datagrid('load',{    
	    	q:value  
	    });
	}
	
	function action(value,row,index){
        let add_akun = `<a href="javascript:void(0)" class="btn btn-small btn-default"
                            onclick="javascript:action_addAkun('${row.id_um}')">
                            <i class="icon-tasks icon-large"></i>
                        </a>`;
        let detail = `<a href="javascript:void(0)" class="btn btn-small btn-success"     
                        onclick="javascript:action_detail(${index})">
                        &nbsp;Detail
                    </a>`;
        
		return  `${detail}`;
	}
	function action_addAkun(row){
        $('#dlg_addakun').dialog('open').dialog('setTitle','Detail ');
        $('#id_cd_um').val(row);
        $('#dg2').datagrid({
            url: "<?=base_url('lain_lain/proses_uang_muka/get_dataAkun/')?>"+row
        });
        
    }
	function action_detail(idx){
		var row = $('#dg').datagrid('getRows')[idx];
		idParent = row.id_um;
		// console.log(row);
		dg1();
		$('#dlg-detail').dialog('open').dialog('setTitle','Detail ' + app);
	}
	
	function preview(){
		var row = $('#dg').datagrid('getSelected');
		if (row){
			window.open('<?=base_url($module . '/' . $appLink . '/read_preview')?>/'+row[idField], '_blank');
	    }
    }
    
    function add_akun(){
        $('#dlg_akun').dialog('open').dialog('setTitle','Detail ');
        $('#fm_akun').form('reset');
        var url = '<?=base_url($module . '/' . $appLink . '/save_akun')?>';
        $('#btn_save_akun').click(function(){
            save_akun(url);
        });
        
    }
    function save_akun(url){
        $('#fm_akun').form('submit',{
	    	url: url,
	    	onSubmit: function(){
	    		return $(this).form('validate');
	    	},
	    	success: function(result){
	    		var result = eval('('+result+')');
	    		if (result.success){
	    			$('#dlg_akun').dialog('close');		
	    			$('#dg2').datagrid('reload');	                
	    		} else {
	             $.messager.alert('Error Message',result.msg,'error');
	    		}
	    	}
	    });
    }
    function edit_akun(){
        var row = $('#dg2').datagrid('getSelected');
        if(row){
            $('#dlg_akun').dialog('open').dialog('setTitle','Detail ');
            $('#fm_akun').form('load',row);
            var url = '<?=base_url($module . '/' . $appLink . '/update_akun')?>';
            $('#btn_save_akun').click(function(){
                save_akun(url);
            });
        }else{
            $.messager.alert('Error Message','Pilih satu data','error');
        }
        
    }
    function delete_akun(){
        var row = $('#dg2').datagrid('getSelected');
        if (row){
            var url = '<?=base_url($module . '/' . $appLink . '/delete_akun')?>/'+row.id_akun+'/'+row.id_cd_um;
			$.messager.confirm('Confirm','Apakah Anda Yakin Akan Menghapus ini?',function(r){
				if (r){
					$.post(url,function(data){
						if (data.success){
							$('#dlg_akun').dialog('close');		
	    			        $('#dg2').datagrid('reload');
						} else {
							$.messager.show({	// show error message
								title: 'Error',
								msg:result.msg
							});
						}
					},'json');
				}
			});
		}
    }
</script>
 
<div class="tabs-container">                
	<table id="dg" class="easyui-datagrid" 
	data-options="  
	    url: '<?=base_url($module . '/' . $appLink . '/read')?>',
		singleSelect:'true', 
	    title:'<?=$app?>',
	    toolbar:'#toolbar',
	    iconCls:'icon-cog',
	    rownumbers:'true',  
	    idField:'<?=$idField?>', 
	    pagination:'true',
	    fitColumns:'true',
	    pageList: [10,20,30]
	    "
	>
	    <thead>
	        <th field="id_um" width="100" sortable="true">No Uang Muka</th>
	        <th field="tanggal_create_disp" width="200" sortable="true">Tanggal</th>
            <th field="no_bl" width="200" sortable="true">No BL</th>
            <th field="keperluan" width="200" sortable="true">Keperluan</th>
            <th field="nilai" formatter="format_numberdisp" align="right" width="200" sortable="true">Nilai</th>
            <th field="jwaktu1_disp" width="200" sortable="true">Jangka Waktu</th>
            <th field="jwaktu2_disp" width="200" sortable="true">Sampai Dengan</th>
            <th field="action" width="200" formatter="action">Action</th>
	    </thead>
	</table>
	
	<!-- Model Start -->
	<div id="dlg" class="easyui-dialog" style="width:400px; height:auto; padding:10px" closed="true" buttons="#t_dlg_dis-buttons" >
	    <form id="fm" method="post" enctype="multipart/form-data" action="">
	        <table width="100%" align="center" border="0">
	            <tr>
	                <td>No Transaksi</td>
	                <td>:</td>
	                <td>
	                	<input class="easyui-textbox" type="text" name="no_bl" id="no_bl" required style="width: 150px;">
	                    <!-- <input name="no_bl" id="no_bl"  class="easyui-combogrid" required style="width: 200px"
						data-options="panelHeight:200, panelWidth: 400,
						valueField:'',
						url:'<?=base_url($module . '/' . $appLink . '/get_no_bl')?>',
						idField:'id_bl',
						textField:'id_bl',
						mode:'remote',
						onSelect :function(indek,row)
						{},
						columns:[[
                       		{field:'id_bl',title:'No BL',width:100},
                        	{field:'nama',title:'Nama',width:120},
                        	{field:'instansi',title:'instansi',width:150}
                        ]]"/> -->
	                </td>
	            </tr>
	            <tr>
	                <td>Tipe Uang Muka</td>
	                <td>:</td>
	                <td>
	                	<select id="tipe_um" class="easyui-combobox" name="tipe_um" style="width:70px;" data-options="panelHeight: 'auto'">
                            <option value="PK">PK</option>
                            <option value="BL">BL</option>
                        </select>
	                </td>
	            </tr>
	            <tr>
	                <td>Keperluan</td>
	                <td>:</td>
	                <td>
	                	<input class="easyui-textbox" type="text" name="keperluan" id="keperluan" required style="width: 120px;">
	                </td>
	            </tr>
	            <tr>
	                <td>Jangka Waktu</td>
	                <td>:</td>
	                <td>
	                	<input class="easyui-datebox" type="text" name="jwaktu1" id="jwaktu1" required style="width: 120px;">
	                </td>
	            </tr>
	            <tr>
	                <td>Sampai Dengan</td>
	                <td>:</td>
	                <td>
	                	<input class="easyui-datebox" type="text" name="jwaktu2" id="jwaktu2" required style="width: 120px;">
	                </td>
	            </tr>
	        </table>
	    </form>
	    <div id="t_dlg_dis-buttons">
            <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:save()"><i class="icon-save icon-large"></i>&nbsp;Simpan</a>
            <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:$('#dlg_addakun').dialog('close')"><i class="icon-remove icon-large"></i>&nbsp;Batal</a>
        </div>
        
	   	<!-- Tombol Add, Edit, Hapus Datagrid -->
       	<div id="toolbar">  
            <table align="center" style="padding: 0px; width: 99%;">
                <tr>
                    <td>
                        <a href="javascript:void(0)" class="btn btn-small btn-success" onclick="javascript:add()"><i class="icon-plus-sign icon-large"></i>&nbsp;Add</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:edit()"><i class="icon-edit"></i>&nbsp;Edit</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-danger" onclick="javascript:hapus()"><i class="icon-trash icon-large"></i>&nbsp;Delete</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:preview()">&nbsp;Preview</a>
                    </td>
                    <td>&nbsp;</td>
                    <td align="right">
                    	<input class="easyui-searchbox" prompt="Silahkan Input Field" searcher="doSearch"  style="width:300px"/>
                    </td>
                </tr>
            </table>  
        </div>  
        <!-- end tombol datagrid -->
        <!-- Tombol simpan & Batal Form -->
        
	</div>
	<!-- Model end -->
	
	<div id="dlg-detail" class="easyui-dialog" style="width:600px; height:400px; padding:10px" closed="true">
		<?php $this->load->view('view_proses_uang_muka_detail'); ?>
	</div>
</div>


<!-- Model Start -->
<div id="dlg_addakun" class="easyui-dialog" style="width:600px; height:auto; padding:10px" closed="true">
    <table id="dg2" class="easyui-datagrid" 
	data-options="  
	    toolbar:'#toolbar2',
	    title:'Detail Akun',
		singleSelect:'true', 
	    iconCls:'icon-cog',
	    rownumbers:'true',  
	    pagination:'true',
	    fitColumns:'true',
	    pageList: [10,20,30]
	    "
	>
	   <thead>
	        <th field="account_no" width="100" sortable="true">Account No</th>
			<th field="nama_akun" width="200" sortable="true">Nama Akun</th>
			<th field="debet" width="70" sortable="true" formatter="format_numberdisp">Debet</th>
			<th field="kredit" width="70" formatter="format_numberdisp" sortable="true">Credit</th>
	   </thead>
	</table>
    <!-- Tombol Add, Edit, Hapus Datagrid -->
    <div id="toolbar2">  
        <table align="center" style="padding: 0px; width: 99%;">
            <tr>
                <td>
                    <a href="javascript:void(0)" class="btn btn-small btn-success" onclick="javascript:add_akun()"><i class="icon-plus-sign icon-large"></i>&nbsp;Add</a>
                    <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:edit_akun()"><i class="icon-edit"></i>&nbsp;Edit</a>
                    <a href="javascript:void(0)" class="btn btn-small btn-danger" onclick="javascript:delete_akun()"><i class="icon-trash icon-large"></i>&nbsp;Delete</a>
                </td>
                <td>&nbsp;</td>
                <td align="right">
                    <input class="easyui-searchbox" prompt="Silahkan Input Field" searcher="doSearch1"  style="width:100px"/>
                </td>
            </tr>
        </table>  
    </div>  
    <!-- end tombol datagrid -->
</div>
<!-- Model end -->




<div id="dlg_akun" class="easyui-dialog" style="width:500px; height:auto; padding:10px" closed="true" buttons="#t_dlg_dis-buttons2">
    <form id="fm_akun" method="post" enctype="multipart/form-data" action="">
        <table width="100%" align="center" border="0">
            <tr>
                <input type="hidden" id="id_cd_um" name="id_cd_um" />
                <input type="hidden" id="id_akun" name="id_akun" />
                <td>Account</td>
                <td>:</td>
                <td>
                    <input name="account_no" id="account_no" class="easyui-combogrid" required style="width: 203px" data-options="panelHeight:200, panelWidth: 330,
						valueField:'',
						idField:'kode_account',
						textField:'kode_account',
						mode:'remote',
						url:'<?=base_url($module . '/' . $appLink . '/getAccount') ?>',
						onSelect :function(indek,row)
						{
							$('#nama_akun').val(row.uraian);
						},
						columns:[[
                       		{field:'kode_account',title:'Account',width:100},
                        	{field:'uraian',title:'Uraian',width:200}
                        ]]
						">
                </td>
            </tr>
            <tr>
                <td>Nama Akun</td>
                <td>:</td>
                <td>
                    <input type="text" name="nama_akun" class="easyui-textbox" id="nama_akun" readonly="readonly" required>
                </td>
            </tr>
            <tr>
                <td>Debet</td>
                <td>:</td>
                <td>
                    <input class="easyui-numberbox" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%',onChange:function(a, b){ if(a != 0){$('#kredit').numberbox('setValue', 0)} }" name="debet" id="debet" required style="width: 200px;">
                </td>
            </tr>
            <tr>
                <td>Credit</td>
                <td>:</td>
                <td>
                    <input class="easyui-numberbox" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%',onChange:function(a, b){ if(a != 0){$('#debet').numberbox('setValue', 0)} }" name="kredit" id="kredit" required style="width: 200px;">
                </td>
            </tr>
        </table>
    </form>
    <!-- Tombol simpan & Batal Form -->
    <div id="t_dlg_dis-buttons2">
        <a id="btn_save_akun" href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:save_akun()"><i class="icon-save icon-large"></i>&nbsp;Simpan</a>
        <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:$('#dlg_akun').dialog('close')"><i class="icon-remove icon-large"></i>&nbsp;Batal</a>
    </div>
</div>