<?php
$app = 'Penanaman_pohon';
$appLink = 'penanaman_pohon';

?>

<script>
var app = "<?=$app?>";
var appLink = '<?=$appLink?>';

    function add(){
	$('#dlg').dialog('open').dialog('setTitle','Tambah ' + app);
	$('#fm').form('clear');
    $('#tbl').html('Save');
	url = '<?=base_url('lain_lain/' . $appLink . '/create')?>';
    $('#email').val('');
}
function edit(){
	var row = $('#dg').datagrid('getSelected');
	if (row){
	   $('#tbl').html('Simpan');
		$('#dlg').dialog('open').dialog('setTitle','Edit '+ app);
		$('#fm').form('load',row);
        url = '<?=base_url('lain_lain/' . $appLink . '/update')?>/'+row.id_tpohon;
	    }
}
    
function hapus(){
	var row = $('#dg').datagrid('getSelected');		
    if (row){
		$.messager.confirm('Confirm','Apakah Anda Yakin Akan Menghapus ini?',function(r){
			if (r){
				$.post('<?=base_url("lain_lain/" . $appLink . "/delete")?>/'+row.id_tpohon,function(result){
					if (result.success){
						$('#dg').datagrid('reload');	// reload the user data
					} else {
						$.messager.show({	// show error message
							title: 'Error',
							msg:result.msg
						});
					}
				},'json');
			}
		});
	}
}
    
function doSearch(value){
    $('#dg').datagrid('load',{    
		q:value  
    });   
}
    
</script>
 
<script type="text/javascript">
var url;
function save(){
    $('#fm').form('submit',{
        url: url,
        onSubmit: function(){
            return $(this).form('validate');
        },
        success: function(result){
            var result = eval('('+result+')');
            if (result.success){
                $('#dlg').dialog('close');      
                $('#dg').datagrid('reload');
                
            } else {
             $('#dlg').dialog('close');
                $.messager.alert('Error Message',result.msg,'error');
            }
        }
    });
}
</script>

<div class="container_s">                
<table id="dg" class="easyui-datagrid" 
    url= "<?=base_url("lain_lain/penanaman_pohon/read_all")?>"
    singleSelect="true" 
    iconCls="icon-cogs" 
    rownumbers="true"  
    idField="id" 
    pagination="true"
    fitColumns="true"
    pageList= [10,20,30]
    toolbar="#toolbar"
    title="Daftar Program Penanaman pohon"
>
        <thead>
			<th field="id_tpohon" width="100" >No Transaksi</th>
            <th field="tanggal_create" width="100" >Tanggal</th>
            <th field="nama_pohon" width="100" >Nama Pohon</th>
			<th field="nama_latin" width="100" >Nama Ilmiah</th>
			<th field="kabupaten" width="100" >Kebupaten</th>
			<th field="kecamatan" width="100" >Kecamatan</th>
			<th field="kelurahan" width="100" >Kelurahan</th>
			<th field="jumlah" width="100" >Jumlah Pohon</th>
            <!-- <th field="aktif" width="50"  formatter="status" align="center">Status</th> -->
        </thead>
 </table>
 
<!-- Model Start -->
<div id="dlg" class="easyui-dialog" style="width:550px; height:270px; padding:10px" closed="true" buttons="#t_dlg_dis-buttons" >
    <form id="fm" method="post" enctype="multipart/form-data" action="">
		<table width="100%" align="center" border="0">
		    <!--
			<tr >
				<td>No Transaksi</td>
				<td>:</td>
			    <td><input class="easyui-textbox"  name="id_tpohon" id="id_tpohon" style="width: 247px;" readonly="true"></td>
			</tr>
			-->
			<input class="easyui-textbox"  name="id_tpohon" id="id_tpohon" style="width: 247px;" readonly="true" hidden="true">
			<tr>
			    <td>Nama Pohon</td>
				<td>:</td>
			    <td>
			        <input class="easyui-combobox" name="id_pohon" id="id_pohon" required style="width: 250px;" data-options="
				        url:'<?=base_url('lain_lain/penanaman_pohon/get_pohon')?>',
				        method:'get',
				        valueField:'id_pohon',
				        textField:'nama_pohon',
				        panelHeight:'auto',
				        label: 'Language:',
				        labelPosition: 'top',
						onSelect: function(param){
							$('#nama_latin').val(param.nama_latin);
				        }
				    ">
			    </td>
			</tr>
			<tr>
				<td>Nama Latin</td>
				<td>:</td>
			    <td><input class="easyui-textbox"  name="nama_latin" id="nama_latin" style="width: 247px;" readonly="true"></td>
			</tr>
				
			<tr>
			    <td>Kota/Kabupaten</td>
				<td>:</td>
			    <td>
			        <input class="easyui-combobox" name="kabupaten" id="kabupaten" required style="width: 250px;" data-options="
				        url:'<?=base_url('kemitraan/datamitra/getKota/36')?>',
				        method:'get',
				                    valueField:'name',
				                    textField:'name',
				                    panelHeight:'auto',
				                    label: 'Language:',
				                    labelPosition: 'top',
				                    onSelect: function(param){
				                    	$('#kecamatan').combobox('clear');
				                    	$('#kecamatan').combobox('reload','<?=base_url('kemitraan/datamitra/getKecamatan/')?>/'+param.id);
				                    }
				                ">
			                </td>
			</tr>
							<tr>
			                <td>Kecamatan</td>
			                <td>:</td>
			                <td>
			                    <input class="easyui-combobox" name="kecamatan" id="kecamatan" required style="width: 250px;" data-options="
				                    url:'<?=base_url('kemitraan/datamitra/getKecamatan')?>',
				                    method:'get',
				                    valueField:'name',
				                    textField:'name',
				                    panelHeight:'auto',
				                    label: 'Language:',
				                    labelPosition: 'top',
				                    onSelect: function(param){
				                    	$('#kelurahan').combobox('clear');
				                    	$('#kelurahan').combobox('reload','<?=base_url('kemitraan/datamitra/getKelurahan/')?>/'+param.id);
				                    }
				                ">
			                </td>
			            </tr>
			            <tr>
			                <td>Kelurahan</td>
			                <td>:</td>
			                <td>
			                    <input class="easyui-combobox" name="kelurahan" id="kelurahan" required style="width: 250px;" data-options="
				                    url:'<?=base_url('kemitraan/datamitra/getKelurahan')?>',
				                    method:'get',
				                    valueField:'name',
				                    textField:'name',
				                    panelHeight:'auto',
				                    label: 'Language:',
				                    labelPosition: 'top'
				                ">
			                </td>
			</tr>
			<tr>
				<td>Jumlah</td>
				<td>:</td>
			    <td><input class="easyui-textbox"  name="jumlah" id="jumlah" style="width: 247px;" ></td>
			</tr>
		</table>
    </form>
    <?=$this->load->view('toolbar')?>
</div>
<!-- Model end -->   



</div>