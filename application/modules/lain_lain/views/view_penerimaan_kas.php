<?php
$app = 'Jurnal Umum'; // nama aplikasi
$module = 'lain_lain';
$appLink = 'penerimaan_kas'; // controller
$idField  = 'id_penerimaan_kas'; //field key table
?>

<script>
	var url;
	var app = "<?=$app?>";
	var appLink = '<?=$appLink?>';
	var module = '<?=$module?>';
	var idField = '<?=$idField?>';
	var idParent = '';
	
    function add(){
        $('#dlg').dialog('open').dialog('setTitle','Tambah ' + app);
		$('#fm').form('clear');
	    $('#tbl').html('Save');
		url = '<?=base_url($module . '/' . $appLink . '/create')?>';

		var d = new Date();
		var strDate = d.getFullYear() + "-" + (d.getMonth()+1) + "-" + d.getDate();

		$('#tanggal').datebox('setValue', strDate);
    }
    function edit(){
        var row = $('#dg').datagrid('getSelected');
		if (row){
			$('#tbl').html('Simpan');
			$('#dlg').dialog('open').dialog('setTitle','Edit '+ app);
			$('#fm').form('load',row);
			url = '<?=base_url($module . '/' . $appLink . '/update')?>/'+row[idField];
	    }
    }
    function hapus(){
        var row = $('#dg').datagrid('getSelected');		
	    if (row){
			$.messager.confirm('Confirm','Apakah Anda Yakin Akan Menghapus ini?',function(r){
				if (r){
					$.post('<?=base_url($module . '/' . $appLink . '/delete')?>/'+row[idField],function(result){
						if (result.success){
							$('#dg').datagrid('reload');	// reload the user data
						} else {
							$.messager.show({	// show error message
								title: 'Error',
								msg:result.msg
							});
						}
					},'json');
				}
			});
		}
    }
    function save(){
	   $('#fm').form('submit',{
	    	url: url,
	    	onSubmit: function(){
	    		return $(this).form('validate');
	    	},
	    	success: function(result){
	    		var result = eval('('+result+')');
	    		if (result.success){
	    			$('#dlg').dialog('close');		
	    			$('#dg').datagrid('reload');
	    			//console.log(result);
	                //idParent = result.id;
					//$("#akun").show();
					//reload1();
	    		} else {
	             $.messager.alert('Error Message',result.msg,'error');
	    		}
	    	}
	    });
	}
	function doSearch(value){
	    $('#dg').datagrid('load',{    
	    	q:value  
	    });
	}
	function account(index){
		$('#dg').datagrid('selectRow', index);
		var row = $('#dg').datagrid('getSelected');
		if (row){
			//$('#tbl').html('Simpan');
			idParent = row.id_penerimaan_kas;
			reload1();
			$('#dlg_akun').dialog('open').dialog('setTitle','Akun '+ app);

			//$('#fm').form('load',row);
			//url = '<?=base_url($module . '/' . $appLink . '/update')?>/'+row[idField];
	    }
	}
	function accountlist(val, row, index){
		var result = '<a href="javascript:void(0)" title="Daftar Akun" class="btn btn-small btn-default" onclick="javascript:account('+index+')"><i class="icon-tasks icon-large"></i></a> ';

		return result;
	}
</script>
 
<div class="tabs-container">                
	<table id="dg" class="easyui-datagrid" 
	data-options="  
	    url: '<?=base_url($module . '/' . $appLink . '/read')?>',
		singleSelect:'true', 
	    title:'<?=$app?>',
	    toolbar:'#toolbar',
	    iconCls:'icon-cog',
	    rownumbers:'true',  
	    idField:'<?=$idField?>', 
	    pagination:'true',
	    fitColumns:'true',
	    pageList: [10,20,30]
	    "
	>
	    <thead>
	        <th field="id_penerimaan_kas" width="50" sortable="true">ID Jurnal</th>
            <th field="nama" width="200" sortable="true">Nama Pemohon</th>
            <th field="tanggal_create" width="60" sortable="true">Tgl Create</th>
             <th field="nama_creator" width="100" sortable="true">Creator</th>
            <th align="right" field="jumlah" width="100" sortable="true" formatter="format_numberdisp">Jumlah</th>
            <th field="deskripsi" width="200" sortable="true">Deskripsi</th>
            <th field="type" width="50" sortable="true">Jenis</th>
            <th field="date_modified" width="50" sortable="true" formatter="accountlist">Akun</th>
	    </thead>
	</table>
	
	<div id="dlg" class="easyui-dialog" style="width:500px; height:auto; padding:10px" closed="true" buttons="#t_dlg_dis-buttons" >
		<table width="100%" align="center" border="0">
			<tr>
				<td>
					<form id="fm" method="post" enctype="multipart/form-data" action="">
				        <table width="100%" align="center" border="0">
				        	<tr>
				            	<td>Tanggal</td>
				            	<td> : </td>
				            	<td> <input type="date" name="tanggal" id="tanggal" class="easyui-datebox" style="width: 150px"> </td>
				            </tr>
				            <tr>
				                <td>Nama Mitra</td>
				                <td> : </td>
				                <td> <input type="text" name="nama_mitra" class="easyui-textbox" id="nama_mitra" required> </td>							
				            </tr>
							<tr>
				                <td>Jumlah</td>
				                <td>:</td>
				                <td>
				                	<input class="easyui-numberbox" data-options="label:'Currency:Rp. ',labelPosition:'top',precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%'" name="jumlah" id="jumlah" required style="width: 150px;">
				                </td>
				            </tr>
				            <tr>
				                <td>Jenis</td>
				                <td>:</td>
				                <td>
				                	<select  class="easyui-combobox" type="text" name="type" id="type" required style="width: 200px;">
				                    	<option value="CR">CR</option>
				                    	<option value="GV">GV</option>
				                    </select>
				                </td>
				            </tr>
				            
				            <tr>
				            	<td>Deskripsi</td>
				            	<td> : </td>
				            	<td> <textarea name="deskripsi"></textarea> </td>
				            </tr>
							
				        </table>
				    </form>
				</td>
			</tr>
		</table>
	  
	   	<!-- Tombol Add, Edit, Hapus Datagrid -->
       	<div id="toolbar">  
            <table align="center" style="padding: 0px; width: 99%;">
                <tr>
                    <td>
                        <a href="javascript:void(0)" onclick="javascript:add()" class="btn btn-small btn-success"><i class="icon-plus-sign icon-large"></i>&nbsp;Add</a>
                        <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:edit()"><i class="icon-edit"></i>&nbsp;Edit</a>
                        <!--<a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:account()"><i class="icon-edit"></i>&nbsp;Akun</a>-->
                        <a href="javascript:void(0)" class="btn btn-small btn-danger" onclick="javascript:hapus()"><i class="icon-trash icon-large"></i>&nbsp;Delete</a>
						<!--<a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:detail_jaminan()"><i class="icon-file"></i>&nbsp;Detail</a>
						<a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:view_report()"><i class="icon-print"></i>&nbsp;Report</a>-->
                    </td>
                    <td>&nbsp;</td>
                    <td align="right">
                    	<input class="easyui-searchbox" prompt="Silahkan Input Field" searcher="doSearch"  style="width:300px"/>
                    </td>
                </tr>
            </table>  
        </div>  
        <!-- end tombol datagrid -->
        <!-- Tombol simpan & Batal Form -->
        <div id="t_dlg_dis-buttons">
            <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:save()"><i class="icon-save icon-large"></i>&nbsp;Simpan</a>
            <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:$('#dlg').dialog('close')"><i class="icon-remove icon-large"></i>&nbsp;Batal</a>      
        </div>
	</div>
	<!-- Model end -->

	<div id="dlg_akun" class="easyui-dialog" style="width:800px; height:auto; padding:10px" closed="true" buttons="#t_dlg_dis-buttons_akun" >
		<?php $this->load->view('view_penerimaan_kas_akun'); ?>
		<div id="t_dlg_dis-buttons_akun">
            <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:$('#dlg_akun').dialog('close')"><i class="icon-remove icon-large"></i>&nbsp;Batal</a>      
        </div>
	</div>

</div>