<?php
$app1 = 'LPJ Uang Muka Detail'; // nama aplikasi
$module1 = 'lain_lain';
$appLink1 = 'lpj_uang_muka_detail'; // controller
$idField1  = 'id_detail_lpj'; //field key table
?>

<script>
	var url1;
	var app1 = "<?=$app1?>";
	var appLink1 = '<?=$appLink1?>';
	var module1 = '<?=$module1?>';
	var idField1 = '<?=$idField1?>';
	
    function add1(){
        $('#dlg1').dialog('open').dialog('setTitle','Tambah ' + app1);
		$('#fm1').form('clear');
	    $('#tbl1').html('Save');
		url1 = '<?=base_url($module1 . '/' . $appLink1 . '/create')?>';
		$('#idParent').val(idParent);
    }
    function edit1(){
        var row = $('#dg1').datagrid('getSelected');
		if (row){
			$('#tbl1').html('Simpan');
			$('#dlg1').dialog('open').dialog('setTitle','Edit '+ app1);
			$('#fm1').form('load',row);
			url1 = '<?=base_url($module1 . '/' . $appLink1 . '/update')?>/'+row[idField1];
			$('#idParent').val(idParent);
	    }
    }
    function hapus1(){
        var row = $('#dg1').datagrid('getSelected');		
	    if (row){
			$.messager.confirm('Confirm','Apakah Anda Yakin Akan Menghapus ini?',function(r){
				if (r){
					$.post('<?=base_url($module1 . '/' . $appLink1 . '/delete')?>/'+row[idField1],function(result){
						if (result.success){
							$('#dg1').datagrid('reload');	// reload the user data
						} else {
							$.messager.show({	// show error message
								title: 'Error',
								msg:result.msg
							});
						}
					},'json');
				}
			});
		}
    }
    function save1(){
	   $('#fm1').form('submit',{
	    	url: url1,
	    	onSubmit: function(){
	    		return $(this).form('validate');
	    	},
	    	success: function(result){
	    		var result = eval('('+result+')');
	    		if (result.success){
	    			$('#dlg1').dialog('close');		
	    			$('#dg1').datagrid('reload');
	                
	    		} else {
	             $.messager.alert('Error Message',result.msg,'error');
	    		}
	    	}
	    });
	}
	function doSearch1(value){
	    $('#dg1').datagrid('load',{    
	    	q:value  
	    });
	}
	function calc(){
		var jumlah_satuan = $('#jumlah_satuan_realisasi').numberbox('getValue');
		var harga_satuan = $('#harga_satuan_realisasi').numberbox('getValue');
		
		var total = jumlah_satuan * harga_satuan;
		$('#jumlah_harga_realisasi').numberbox('setValue', total);
	}
	function dg1(){
		$('#dg1').datagrid({url: '<?=base_url($module1 . '/' . $appLink1 . '/read')?>/' + idParent})
	}
</script>
 
<div class="tabs-container">                
	<table id="dg1" class="easyui-datagrid" 
	data-options="
		singleSelect:'true', 
	    title:'<?=$app1?>',
	    toolbar:'#toolbar1',
	    iconCls:'icon-cog',
	    rownumbers:'true',  
	    idField:'<?=$idField1?>', 
	    pagination:'true',
	    fitColumns:'true',
	    pageList: [10,20,30]
	    "
	>
	    <thead>
	        <th field="keterangan" width="200" sortable="true">Keterangan</th>
	        <th field="jumlah_satuan_realisasi" formatter="format_numberdisp" align="right" width="120" sortable="true">Satuan Realisasi</th>
            <th field="harga_satuan_realisasi" formatter="format_numberdisp" align="right" width="120" sortable="true">Harga Satuan Realisasi</th>
            <th field="jumlah_harga_realisasi" formatter="format_numberdisp" align="right" width="120" sortable="true">Jumlah Harga Realisasi</th>
	    </thead>
	</table>
	
	<!-- Model Start -->
	<div id="dlg1" class="easyui-dialog" style="width:400px; height:350px; padding:10px" closed="true" buttons="#t_dlg_dis-buttons1" >
	    <form id="fm1" method="post" enctype="multipart/form-data" action="">
	        <table width="100%" align="center" border="0">
	            <tr>
	                <td>Keterangan</td>
	                <td>:</td>
	                <td>
	                    <textarea name="keterangan" id="keterangan" readonly></textarea>
	                </td>
	            </tr>
	            <tr>
	                <td>Satuan</td>
	                <td>:</td>
	                <td>
	                    <input class="easyui-numberbox" data-options="precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'',width:'100%'" id="jumlah_satuan" name="jumlah_satuan" readonly style="width: 120px;">
	                </td>
	            </tr>
	            <tr>
	                <td>Harga Satuan</td>
	                <td>:</td>
	                <td>
	                    <input class="easyui-numberbox" data-options="precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%'" id="harga_satuan" name="harga_satuan" readonly style="width: 120px;">
	                </td>
	            </tr>
	            <tr>
	                <td>Jumlah Harga</td>
	                <td>:</td>
	                <td>
	                    <input class="easyui-numberbox" data-options="precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%'" id="jumlah_harga" name="jumlah_harga" readonly style="width: 120px;">
	                </td>
	            </tr>
	            <tr>
	            	<td colspan="3"><hr /></td>
	            </tr>
	            <tr>
	                <td>Satuan Realisasi</td>
	                <td>:</td>
	                <td>
	                    <input class="easyui-numberbox" data-options="onChange: function(){calc()},precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'',width:'100%'" name="jumlah_satuan_realisasi" id="jumlah_satuan_realisasi" required style="width: 120px;">
	                </td>
	            </tr>
	            <tr>
	                <td>Harga Satuan Realisasi</td>
	                <td>:</td>
	                <td>
	                    <input class="easyui-numberbox" data-options="onChange: function(){calc()},precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%'" name="harga_satuan_realisasi" id="harga_satuan_realisasi" required style="width: 120px;">
	                </td>
	            </tr>
	            <tr>
	                <td>Jumlah Harga Realisasi</td>
	                <td>:</td>
	                <td>
	                    <input class="easyui-numberbox" data-options="precision:0,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. ',width:'100%'" name="jumlah_harga_realisasi" id="jumlah_harga_realisasi" required style="width: 120px;" readonly>
	                </td>
	            </tr>
	        </table>
	    </form>
	   
	   	<!-- Tombol Add, Edit, Hapus Datagrid -->
       	<div id="toolbar1">  
            <table align="center" style="padding: 0px; width: 99%;">
                <tr>
                    <td>
                        <!-- <a href="javascript:void(0)" class="btn btn-small btn-success" onclick="javascript:add1()"><i class="icon-plus-sign icon-large"></i>&nbsp;Add</a> -->
                        <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:edit1()"><i class="icon-edit"></i>&nbsp;Edit</a>
                        <!-- <a href="javascript:void(0)" class="btn btn-small btn-danger" onclick="javascript:hapus1()"><i class="icon-trash icon-large"></i>&nbsp;Delete</a> -->
                    </td>
                    <td>&nbsp;</td>
                    <td align="right">
                    	<input class="easyui-searchbox" prompt="Silahkan Input Field" searcher="doSearch1"  style="width:300px"/>
                    </td>
                </tr>
            </table>  
        </div>  
        <!-- end tombol datagrid -->
        <!-- Tombol simpan & Batal Form -->
        <div id="t_dlg_dis-buttons1">
            <a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="javascript:save1()"><i class="icon-save icon-large"></i>&nbsp;Simpan</a>
            <a href="javascript:void(0)" class="btn btn-small btn-warning" onclick="javascript:$('#dlg1').dialog('close')"><i class="icon-remove icon-large"></i>&nbsp;Batal</a>      
        </div>
	</div>
	<!-- Model end -->
</div>