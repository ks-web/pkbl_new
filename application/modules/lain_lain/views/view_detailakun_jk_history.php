<?php
$app7 = 'History Jurnal'; // nama aplikasi
$module7 = 'lain_lain';
$appLink7= 'detail_akun_jk_history'; // controller
$idField7  = 'id_akun'; //field key table
?>

<script>
	var url7;
	var app7 = "<?=$app7?>";
	var appLink7 = '<?=$appLink7?>';
	var module7 = '<?=$module7?>';
	var idField7 = '<?=$idField7?>';

	function reload7(row){
		var transaksi = row.jurnal;
		var id_cd_um = row.no_transaksi;
		var status;
		if ( row.status_kode == '1') {
			status = 'KM';
		} else if ( row.status_kode == '2') {
			status = 'BL'
		} else if ( row.status_kode == '4') {
			status = 'KU';
		} else {
			status = '';
		}
		
		$('#dg7').datagrid({    
	    	url: '<?=base_url($module7 . '/' . $appLink7 . '/read_history/')?>/'+transaksi+'/'+status + '/' + id_cd_um  
	    });
	}
	
</script>
 
<div class="tabs-container">                
	<table id="dg7" class="easyui-datagrid" 
	data-options="
		singleSelect:'true', 
	    title:'<?=$app7?>',
	    iconCls:'icon-cog',
	    rownumbers:'true',  
	    idField:'<?=$idField7?>',
	    fitColumns:'true'
	    "
	>
	    <thead>
	    <tr>
	    	<th field="account_no">Akun</th>
	    	<th field="uraian">Nama Akun</th>
			<th field="debet" formatter="format_numberdisp">Debet</th>
			<th field="kredit" formatter="format_numberdisp">Kredit</th>
		</tr>
		</thead>
	</table>
</div>