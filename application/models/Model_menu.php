<?php
	class model_Menu extends CI_Model {
		function __construct() {
			parent::__construct();
		}

		function getAllMenu() {
			$sql = "SELECT * 
                   FROM sys_menu 
                   WHERE stat='1' AND 
                   parentnode = '1' 
                   ORDER BY sortid
                   ";

			$query = $this->db->query($sql);

			if ($query->num_rows() > 0) {
				$hsl = '';
				$no = 1;
				//$icon =
				// array('icon-file','icon-file','icon-cogs','icon-edit','icon-hdd','icon-money','icon-gift');
				foreach ($query->result() as $row) {
					$uri = uri_string();
					$cek = $this->cekSelected($row->node, $uri);
					$sel = '';
					if ($cek == true) {
						$sel = 'selected:true,';
					} else {
						$sel = '';
					}
					$hsl .= '<div " title="' . $no . '. ' . $row->displaytext . '" id="' . $no . '" onclick="#"  data-options="' . $sel . 'iconCls:&apos;' . $row->icon . '&apos;"  style="overflow:auto;padding:10px;">
                <ul class="easyui-tree" animate="true" data-options="onClick:function(node){location.href=$(node.text).attr(\'href\');}">';
					$hsl .= $this->getChild($row->node, $no);
					$hsl .= '</ul>  
                        </div> ';
					$no++;
				}
				return $hsl;

			} else {
				return array();
			}

		}

		function getChild($parent, $no) {
			$uri = uri_string();
			$sql = "SELECT * 
                   FROM sys_menu 
                   WHERE stat='1' AND 
                   parentnode = " . $parent . " AND 
                   node in (select node from sys_gr_link where group_id='" . $this->session->userdata('group_id') . "' and sys_gr_link.read = '1')  
                   ORDER BY sortid
                   ";
			$query = $this->db->query($sql);

			$hsl1 = '';

			if ($query->num_rows() > 0) {
				$i = 1;
				foreach ($query->result() as $row) {
					if (!$this->session->userdata('logged_in') == 1) {
						$hsl1 .= '';
					} else {
						$cek = (strtolower($uri) === strtolower($row->linkaddress)) ? "color:red;" : "";
						$hsl1 .= '<li><a style="' . $cek . '" href="' . base_url($row->linkaddress) . '">&nbsp;' . $no . '.' . $i . ' &nbsp;' . $row->displaytext . '</a></li>';
					}
					$i++;
				}

			} else {
				#return array();
				$hsl1 .= '';
			}
			return $hsl1;
		}

		function cekSelected($parent, $uri) {
			$sql = "SELECT * 
                   FROM sys_menu 
                   WHERE stat='1' AND 
                   parentnode = " . $parent . " AND 
                   node in (select node from sys_gr_link where group_id='" . $this->session->userdata('group_id') . "')  
                   ORDER BY sortid
                   ";
			$query = $this->db->query($sql);

			if ($query->num_rows() > 0) {
				$cil = $query->result();
				foreach ($cil as $row) {
					if (strtolower($uri) === strtolower($row->linkaddress)) {
						return true;
					}
				}

			} else {
				return false;
			}
		}

		function cekOtoritas($type) {
			if ($this->session->userdata($type) != 1) {
				$this->output->set_output(json_encode(array('msg' => 'Maaf anda tidak diperbolehkan melakukan action ini')));
				return false;
			}
		}

	}
