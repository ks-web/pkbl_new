<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	/*
	 | -------------------------------------------------------------------------
	 | URI ROUTING
	 | -------------------------------------------------------------------------
	 | This file lets you re-map URI requests to specific controller functions.
	 |
	 | Typically there is a one-to-one relationship between a URL string
	 | and its corresponding controller class/method. The segments in a
	 | URL normally follow this pattern:
	 |
	 |	example.com/class/method/id/
	 |
	 | In some instances, however, you may want to remap this relationship
	 | so that a different class/function is called than the one
	 | corresponding to the URL.
	 |
	 | Please see the user guide for complete details:
	 |
	 |	https://codeigniter.com/user_guide/general/routing.html
	 |
	 | -------------------------------------------------------------------------
	 | RESERVED ROUTES
	 | -------------------------------------------------------------------------
	 |
	 | There are three reserved routes:
	 |
	 |	$route['default_controller'] = 'welcome';
	 |
	 | This route indicates which controller class should be loaded if the
	 | URI contains no data. In the above example, the "welcome" class
	 | would be loaded.
	 |
	 |	$route['404_override'] = 'errors/page_missing';
	 |
	 | This route will tell the Router which controller/method to use if those
	 | provided in the URL cannot be matched to a valid route.
	 |
	 |	$route['translate_uri_dashes'] = FALSE;
	 |
	 | This is not exactly a route, but allows you to automatically route
	 | controller and method names that contain dashes. '-' isn't a valid
	 | class or method name character, so it requires translation.
	 | When you set this option to TRUE, it will replace ALL dashes in the
	 | controller and method URI segments.
	 |
	 | Examples:	my-controller/index	-> my_controller/index
	 |		my-controller/my-method	-> my_controller/my_method
	 */
	$route['default_controller'] = 'home';
	$route['404_override'] = '';
	$route['translate_uri_dashes'] = FALSE;

	require_once (BASEPATH . 'database/DB' . EXT);
	$db = &DB();

	$db->distinct();
	$db->select('sys_menu.linkaddress, sys_menu.stat');
	$db->select('sys_gr_link.read, sys_gr_link.add, sys_gr_link.edit, sys_gr_link.delete');
	$db->from('sys_menu');
	$db->join('sys_gr_link', 'sys_menu.node = sys_gr_link.node');
	
	if (isset($_COOKIE['group_id'])) {
		$db->where('sys_gr_link.group_id', $_COOKIE['group_id']);
		// die();
	}
	$query = $db->get();
	$result = $query->result();
	foreach ($result as $row) {

		if ($row->read == 0 || $row->stat != '1') {
			$route[$row->linkaddress . '/(:any)'] = 'error/permission';
			$route[$row->linkaddress] = 'error/permission';
		} else {
			if ($row->add == 0) {
				$route[$row->linkaddress . '/create/(:any)'] = 'error/permission';
			}
			if ($row->edit == 0) {
				$route[$row->linkaddress . '/update/(:any)'] = 'error/permission';
			}
			if ($row->delete == 0) {
				$route[$row->linkaddress . '/delete/(:any)'] = 'error/permission';
			}
		}
	}

	// die();
