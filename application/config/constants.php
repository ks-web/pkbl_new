<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

/*
	Eproc General Settings
*/
defined('PERUSAHAAN')  OR define('PERUSAHAAN', 'PT. Krakatau Steel');
defined('USER_LEVEL_VENDOR_DEFAULT')  OR define('USER_LEVEL_VENDOR_DEFAULT', '3');
defined('USER_LEVEL_PEGAWAI_DEFAULT')  OR define('USER_LEVEL_PEGAWAI_DEFAULT', '2');

/*
	User Password Random Pool
*/
defined('user_PasswordPool')  OR define('user_PasswordPool', '1234567890');

/*
	Email Settings
*/
defined('mail_SMTPSecure')  OR define('mail_SMTPSecure', 'ssl');
defined('mail_Port')  OR define('mail_Port', 465);
defined('mail_Host')  OR define('mail_Host', 'smtp.gmail.com');
defined('mail_Username')  OR define('mail_Username', 'krakataukit@gmail.com');
defined('mail_Password')  OR define('mail_Password', '11102016');
defined('mail_db')  OR define('mail_db', TRUE);
defined('mail_AppName')  OR define('mail_AppName', 'PKBL ');
defined('mail_Footer')  OR define('mail_Footer', '<br /><br />Admin PKBL<br />' . '<strong>' . PERUSAHAAN . '</strong>');

/*
	File Upload Settings
*/
// fileUploadTipe1 / fileUploadSize1 for document upload
defined('fileUploadTipe1')  OR define('fileUploadTipe1', json_encode(array('pdf')));
defined('fileUploadSize1')  OR define('fileUploadSize1', 500);

// fileUploadTipe2 / fileUploadSize2 for image upload
defined('fileUploadTipe2')  OR define('fileUploadTipe2', json_encode(array('png','jpg')));
defined('fileUploadSize2')  OR define('fileUploadSize2', 100);

// fileUploadTipe3 / fileUploadSize3 for image upload
defined('fileUploadTipe3')  OR define('fileUploadTipe3', json_encode(array('PNG','png','jpg', 'pdf', 'jpeg')));
defined('fileUploadSize3')  OR define('fileUploadSize3', 100);

// fileUploadTipe4 / fileUploadSize4 for image upload
defined('fileUploadTipe4')  OR define('fileUploadTipe4', json_encode(array('pdf')));
defined('fileUploadSize4')  OR define('fileUploadSize4', 600);

// fileUploadTipe5 / fileUploadSize5 for image upload
defined('fileUploadTipe5')  OR define('fileUploadTipe5', json_encode(array('PNG','png','jpg', 'pdf', 'jpeg')));
defined('fileUploadSize5')  OR define('fileUploadSize5', 300);
/*
	Folder Permission 
*/
defined('folderUploadPermission')  OR define('folderUploadPermission', 0755);

defined('managerName')  or define('managerName', 'Ridwan');
defined('managerTitle')  or define('managerTitle', 'Manager General Affair');
defined('divisiTitle')  or define('divisiTitle', 'Divisi General Affair');
defined('sptTitle')  or define('sptTitle', 'Muhammad Aminudin Azis');
defined('sptJabatan')  or define('sptJabatan', 'Superintendent Community Development');
defined('url_img')  or define('url_img', '/var/www/pkbl/');
